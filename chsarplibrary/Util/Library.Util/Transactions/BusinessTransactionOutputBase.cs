﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Util.Transactions
{
    public abstract class BusinessTransactionOutputBase
    {
        public Boolean AccessDenied { get; set; }

        public Boolean DoesNotMeetRequirements { get; set; }
    }
}
