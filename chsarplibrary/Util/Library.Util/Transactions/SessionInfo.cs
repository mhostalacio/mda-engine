﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Util.Transactions
{
    public class SessionInfo
    {
        public string RemoteHostAddress { get; set; }
        public string FrontEndAddress { get; set; }
        public string SessionId { get; set; }
        public string TimeZoneCode { get; set; }
        public string CultureCode { get; set; }
        private string _languageCode { get; set; }
        public string UserCode { get; set; }
        public string Name { get; set; }
        //public string LoginGUID { get; set; }
        public string PicturePath { get; set; }
        public string AccessToken { get; set; }
        public Boolean IsFirstLogin { get; set; }
        public string CompanyName { get; set; }


        private string _companyCode = "TEST";

        public string CompanyCode 
        { 
            get
            {
                return _companyCode;
            }
            set
            {
                _companyCode = value;
            }
        }

        public string LanguageCode
        {
            get
            {
                if (_languageCode == null)
                    return CommonSettings.DefaultLanguageCode;
                return _languageCode;
            }
            set
            {
                _languageCode = value;
            }
        }

        private Dictionary<String, Object> _objects = new Dictionary<string, Object>();
        public Dictionary<String, Object> Objects
        {
            get
            {
                return _objects;
            }
        }
    }
}
