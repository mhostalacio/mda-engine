﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using Library.Util.Text;
using System.Web;
using Library.Util.Logging;

namespace Library.Util.Transactions
{
    public class BusinessTransaction : IDisposable
    {
        public const string USER_CONTEXT_SESSION_KEY = "UserContext";
        public const string USER_CONTEXT_EMPTY = "Anonymous";
        private List<IMessage> _messages = new List<IMessage>();
        private CultureInfo _currentCulture;
        private Dictionary<Guid, EnlistedTransaction> _enlistedTransactions = new Dictionary<Guid, EnlistedTransaction>();

        [ThreadStatic]
        private static BusinessTransaction _currentContext = null;

        public void Dispose()
        {
            if (this.TransactionName != null)
                LogManager.Instance.AddLog(this);
        }

        public SessionInfo SessionInfo { get; private set; }

        public DateTime ClientApplicationDateTime { get; private set; }

        public BusinessTransaction(SessionInfo sessionInfo, DateTime currentDateTime)
        {
            SessionInfo = sessionInfo;
            ClientApplicationDateTime = currentDateTime;
            if (!BusinessTransaction.HasCurrentContext)
            {
                // Sets this instance as the current transaction 
                _currentContext = this;
            }
        }

        public static BusinessTransaction CurrentContext 
        {
            get
            {
                return _currentContext;
            }
        }

        public static void SetCurrentContext(BusinessTransaction ctx)
        {
            _currentContext = ctx;
        }

        /// <summary>
        /// Indicates if there is a current transaction executing.
        /// </summary>
        public static bool HasCurrentContext
        {
            get { return _currentContext != null; }
        }

        /// <summary>
        /// Executes the transaction.
        /// </summary>
        public void Execute()
        {
            Guid tranID = Guid.NewGuid();
            Library.Util.Transactions.BusinessTransaction.CurrentContext.EnlistTransaction(tranID, this.TransactionName);
            if (!HasAccess())
            {
                SetAccessDenied();
                Library.Util.Transactions.BusinessTransaction.CurrentContext.FinishEnlistedTransaction(tranID, false, "Access Denied");
            }
            else if (!MeetsRequirements())
            {
                Library.Util.Transactions.BusinessTransaction.CurrentContext.FinishEnlistedTransaction(tranID, false, "Failed meeting requirements");
            }
            else
            {
                try
                {
                    ExecuteInternal();
                }
                catch (Exception ex)
                {
                    Library.Util.Transactions.BusinessTransaction.CurrentContext.FinishEnlistedTransaction(tranID, false, ex.Message + ex.StackTrace);
                    throw;
                }
                Library.Util.Transactions.BusinessTransaction.CurrentContext.FinishEnlistedTransaction(tranID);
            }
        }

        public virtual String TransactionName { get { return null; } }

        protected virtual Boolean HasAccess()
        {
            return true;
        }

        protected virtual Boolean MeetsRequirements()
        {
            return true;
        }

        protected virtual void SetAccessDenied()
        {}

        /// <summary>
        /// Derived transactions should implement their execution logic here.
        /// </summary>
        protected virtual void ExecuteInternal()
        {
        }

        public List<IMessage> Messages
        {
            get
            {
                return _messages;
            }
        }

        public Boolean HasErrorMessages()
        {
            return _messages != null && _messages.FirstOrDefault(o => o.MessageType == MessageTypeEnum.ERROR) != null;
        }

        public void ClearMessages()
        {
            _messages = new List<IMessage>();
        }

        public int AccessLevel { get; set; }

        public static BusinessTransaction InitializeContext(string sessionId)
        {
            if (String.IsNullOrEmpty(sessionId))
                throw new Exception("sessionId is mandatory");
            SessionInfo sessionInfo = new SessionInfo();
            sessionInfo.SessionId = sessionId;
            //sessionInfo.LanguageCode = CultureInfo.CreateSpecificCulture("pt-PT").IetfLanguageTag;
            //sessionInfo.CultureCode = CultureInfo.CreateSpecificCulture("pt-PT").IetfLanguageTag;
            sessionInfo.RemoteHostAddress = System.Net.Dns.GetHostName();
            //sessionInfo.TimeZoneCode = TimeZoneInfo.Utc.Id;
            sessionInfo.UserCode = USER_CONTEXT_EMPTY;
            sessionInfo.Name = USER_CONTEXT_EMPTY;
            sessionInfo.FrontEndAddress = "";
            BusinessTransaction ctx = new BusinessTransaction(sessionInfo, DateTime.UtcNow);
            return ctx;
        }

        public static void ReloadContext(String usercode, String username, String pic, int accessLevel, String accessToken)
        {
            CurrentContext.SessionInfo.UserCode = usercode;
            CurrentContext.SessionInfo.Name = username;
            CurrentContext.SessionInfo.PicturePath = pic;
            CurrentContext.AccessLevel = accessLevel;
            CurrentContext.SessionInfo.AccessToken = accessToken;
        }

        public static void ReloadContext(String usercode, String username, String pic, String companyCode, int accessLevel, String accessToken)
        {
            CurrentContext.SessionInfo.UserCode = usercode;
            CurrentContext.SessionInfo.Name = username;
            CurrentContext.SessionInfo.PicturePath = pic;
            CurrentContext.SessionInfo.CompanyCode = companyCode;
            CurrentContext.AccessLevel = accessLevel;
            CurrentContext.SessionInfo.AccessToken = accessToken;
        }

        public static void ClearContext(HttpSessionStateBase session)
        {
            //_currentContext = null;
            session.Clear();
            session.Abandon();
            InitializeContext(session.SessionID);
        }

        public string GetSessionId()
        {
            return SessionInfo.SessionId;
        }

        public CultureInfo CurrentCulture
        {
            get
            {
                if (_currentCulture == null && !String.IsNullOrEmpty(CurrentContext.SessionInfo.CultureCode))
                    _currentCulture = CultureInfo.CreateSpecificCulture(CurrentContext.SessionInfo.CultureCode);
                else
                    _currentCulture = CultureInfo.InvariantCulture;
                return _currentCulture;
            }
        }

        public CultureInfo CurrentLanguage
        {
            get
            {
                if (!String.IsNullOrEmpty(CurrentContext.SessionInfo.LanguageCode))
                    return CultureInfo.CreateSpecificCulture(CurrentContext.SessionInfo.LanguageCode);
                else
                    return CultureInfo.InvariantCulture;
            }
            set
            {
                CurrentContext.SessionInfo.LanguageCode = value.IetfLanguageTag;
            }
        }

        public static Boolean IsLoggedIn()
        {
            return BusinessTransaction.CurrentContext.SessionInfo.UserCode != USER_CONTEXT_EMPTY;
        }

        public static Boolean IsLoggedIn(HttpContextBase ctx)
        {
            return BusinessTransaction.CurrentContext.SessionInfo.UserCode != USER_CONTEXT_EMPTY
                && BusinessTransaction.CurrentContext.SessionInfo.SessionId == ctx.Session.SessionID;
        }

        public virtual Dictionary<String, Object> GetExposedAttributes()
        {
            return null;
        }

        public virtual String GetTranslation(String keyType, String key)
        {
            return Library.Util.AOP.ServiceLocator.Instance.GetService<Library.Util.Localization.ITranslationProvider>().GetTranslation(Library.Util.Transactions.BusinessTransaction.CurrentContext.CurrentLanguage, keyType, key);
        }

        public void EnlistTransaction(Guid identifier, string transactionName)
        {
            Guid? parent = null;
            if (_enlistedTransactions.Count > 0)
            {
                parent = _enlistedTransactions.First().Key;
            }
            _enlistedTransactions.Add(identifier, new EnlistedTransaction(identifier, transactionName, GetSessionId(), SessionInfo.UserCode, parent));
        }

        public void FinishEnlistedTransaction(Guid identifier, Boolean success = true, String errorMessage = null)
        {
            _enlistedTransactions[identifier].EndTransaction();
            _enlistedTransactions.Remove(identifier);
        }
    }


    public class BusinessTransaction<INPUT, OUTPUT> : BusinessTransaction
        where INPUT : BusinessTransactionInputBase
        where OUTPUT: BusinessTransactionOutputBase
    {

        public BusinessTransaction(SessionInfo sessionInfo, DateTime currentDateTime)
            : base(sessionInfo, currentDateTime)
        {}

        public virtual INPUT Input { get; set; }

        public virtual OUTPUT Output { get; set; }

        protected override void SetAccessDenied()
        {
            base.SetAccessDenied();

            Output.AccessDenied = true;
        }

    }
    
}
