﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Util.Transactions
{
    public class EnlistedTransaction
    {
        public Guid Identifier { get; private set; }

        public String TransactionId { get; private set; }

        public DateTime StartTime { get; private set; }

        public DateTime EndTime { get; private set; }

        public Boolean Success { get; private set; }

        public String ErrorMessage { get; private set; }

        public String LoginGuid { get; private set; }

        public String UserCode { get; private set; }

        public Guid? ParentIdentifier { get; private set; }

        public EnlistedTransaction(Guid identifier, String transactionId, String loginGuid, String userCode, Guid? parentIdentifier)
        {
            Identifier = identifier;
            TransactionId = transactionId;
            StartTime = DateTime.UtcNow;
            LoginGuid = loginGuid;
            UserCode = userCode;
            ParentIdentifier = parentIdentifier;
        }

        public void EndTransaction(Boolean success = true, String errorMessage = null)
        {
            Success = success;
            ErrorMessage = errorMessage;
            EndTime = DateTime.UtcNow;
        }
        
    }
}
