﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Util.Cache
{
    public interface ICache<K, V> 
    {
        V Get(K key);

        Dictionary<K, V> GetAll();

        int CacheHit { get; }

        int CacheMiss { get; }

        int Count { get; }

        string ToString();

        void ClearCache();

        void WarmUp();

        Dictionary<String, String> GetFullInfo();

        void RemoveSingleEntry(String key);
    }
}
