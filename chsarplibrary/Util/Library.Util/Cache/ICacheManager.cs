﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Util.Cache
{
    /// <summary>
    /// Base contract for cache managers.
    /// </summary>
    /// <typeparam name="T">The type of objects this cache manager holds.</typeparam>
    public interface ICacheManager<T>
    {
        void Add(String key, T obj);

        void Add(String key, T obj, TimeSpan timeToLive);

        T Get(String key);

        void Update(String key, T obj);
    }
}
