﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Util.Cache
{
    /// <summary>
    /// Provides a in-memory cache store.
    /// </summary>
    /// <typeparam name="K">Key type.</typeparam>
    /// <typeparam name="V">Value type.</typeparam>
    public class InProcessCacheStore<K, V> : ICacheStore<K, V>
    {
        private object _lockObject = new object();
        private Dictionary<K, V> _dictionary;


        public InProcessCacheStore()
        {
            _dictionary = new Dictionary<K, V>();
        }

        public bool Get(K key, out V value)
        {
            return _dictionary.TryGetValue(key, out value);
        }

        public void Add(K key, V value)
        {
            lock (_lockObject)
            {
                if (!_dictionary.ContainsKey(key))
                {
                    _dictionary.Add(key, value);
                }
                else
                {
                    _dictionary[key] = value;
                }
            }
        }

        public void Remove(K key)
        {
            lock (_lockObject)
            {
                if (_dictionary.ContainsKey(key))
                {
                    _dictionary.Remove(key);
                }
            }
        }

        public void BulkAdd(IEnumerable<KeyValuePair<K, V>> values)
        {
            lock (_lockObject)
            {
                foreach (var value in values)
                {
                    if (!_dictionary.ContainsKey(value.Key))
                    {
                        _dictionary.Add(value.Key, value.Value);
                    }
                    else
                    {
                        _dictionary[value.Key] = value.Value;
                    }
                }
            }
        }

        public void Clear()
        {
            _dictionary.Clear();
        }


        public bool ContainsKey(K key)
        {
            return _dictionary.ContainsKey(key);
        }

        public int Count
        {
            get
            {
                return _dictionary.Count;
            }
        }

        internal Dictionary<K, V> GetAll()
        {
            return _dictionary;
        }
    }
}
