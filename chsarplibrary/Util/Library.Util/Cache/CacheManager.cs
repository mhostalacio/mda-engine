﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Util.Cache
{
    /// <summary>
    /// Manages all registered caches.
    /// </summary>
    public class CacheManager
    {
        private ICacheConfiguration _configuration;

        public static readonly CacheManager INSTANCE = new CacheManager();

        public CacheManager() :
            this(new DefaultCacheConfiguration())
        {
        }

        public CacheManager(ICacheConfiguration config)
        {
            if (config == null)
                throw new ArgumentNullException("config");

            _configuration = config;
        }

        /// <summary>
        /// The ICacheConfiguration instance that this manager uses.
        /// </summary>
        public ICacheConfiguration Configuration
        {
            get { return _configuration; }
            set { _configuration = value; }
        }
    }
}
