﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Util.Cache
{
    /// <summary>
    /// Contract for cache store implements. It serves as a repository for the cache managerfalhafa.
    /// </summary>
    public interface ICacheStore<K, V>
    {
        /// <summary>
        /// Returns the value with the supplied key.
        /// </summary>
        /// <param name="key">Unique key that identifies an object in the store.</param>
        /// <param name="value">A variable that will be setted with the store value.</param>
        /// <returns>A boolean value if the value was found in the store.</returns>
        bool Get(K key, out V value);

        /// <summary>
        /// Adds the supplied list of values.
        /// </summary>
        /// <param name="values">List of values.</param>
        void BulkAdd(IEnumerable<KeyValuePair<K, V>> values);

        /// <summary>
        /// Adds the supplied value using the supplied key as a unique identifier.
        /// </summary>
        /// <param name="key">Unique key that identifies an object in the store.</param>
        /// <param name="value">Value to store.</param>
        void Add(K key, V value);

        /// <summary>
        /// Removes all the stored values.
        /// </summary>
        void Clear();

        /// <summary>
        /// Removes the value associated with supplied key.
        /// </summary>
        /// <param name="key">Unique key that identifies an object in the store.</param>
        void Remove(K key);

        /// <summary>
        /// Validate If Key exists in Store.
        /// </summary>
        /// <param name="key">Unique key that identifies an object in the store.</param>
        bool ContainsKey(K key);
    }
}
