﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Util.Cache
{
    public class DefaultCacheConfiguration : ICacheConfiguration
    {
        private Dictionary<Object, Object> _stores = new Dictionary<Object, object>();

        #region ICacheConfiguration Members

        public ICacheStore<K, V>[] GetStores<K, V>(ICache<K, V> cache)
        {
            if (cache == null)
                throw new ArgumentNullException("cache");

            Object stores = null;

            if (!_stores.TryGetValue(cache, out stores))
            {
                lock (_stores)
                {
                    if (!_stores.TryGetValue(cache, out stores))
                    {
                        stores = new[] { new InProcessCacheStore<K, V>() };
                        _stores.Add(cache, stores);
                    }
                }
            }

            return (ICacheStore<K, V>[])stores;
        }

        #endregion
    }
}
