﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Util.Cache
{
    public abstract class CacheBase<K, V> : ICache<K, V>
    {
        private ICacheStore<K, V>[] _stores = null;
        private int _cacheHit = 0;
        private int _cacheMiss = 0;
        private object _lockObject = new object();

        public int CacheHit { get { return _cacheHit; } }

        public int CacheMiss { get { return _cacheMiss; } }

        public int Count
        {
            get
            {
                var count = 0;
                foreach (ICacheStore<K, V> store in _stores)
                {
                    if (store is InProcessCacheStore<K, V>)
                    {
                        count += (store as InProcessCacheStore<K, V>).Count;
                    }
                    //FOR OTHER STORES IT IS NOT POSSIBLE YET !!
                }
                return count;
            }
        }

        public override string ToString()
        {
            return string.Format("{0} Items | {1} Hit | {2} Miss", Count, CacheHit, CacheMiss);
        }

        public CacheBase()
        {
            _stores = CacheManager.INSTANCE.Configuration.GetStores(this);

            if (_stores == null || _stores.Length == 0)
                throw new InvalidOperationException("There is no store configured for the cache \"" + GetType().Name + "\"!");
        }

        public CacheBase(IEnumerable<KeyValuePair<K, V>> values)
            : this()
        {
            AddRange(values);
        }

        /// <summary>
        /// Adds a value to the cache with the supplied key.
        /// </summary>
        /// <param name="key">Unique Key.</param>
        /// <param name="value">Value to add.</param>
        public virtual void Add(K key, V value)
        {
            foreach (ICacheStore<K, V> store in _stores)
            {
                if (key != null && !store.ContainsKey(key))
                {
                    store.Add(key, value);
                }
            }
        }

        /// <summary>
        /// Adds a range of values to the cache with the supplied keys.
        /// </summary>
        /// <param name="values">KeyValuePairs to add.</param>
        public virtual void AddRange(IEnumerable<KeyValuePair<K, V>> values)
        {
            if (values == null)
                throw new ArgumentNullException("values");

            foreach (ICacheStore<K, V> store in _stores)
            {
                store.BulkAdd(values);
            }
        }

        /// <summary>
        /// Clears the cache in current application
        /// Use CacheInvalidationManager if you want to invalidate cache of multiple application
        /// </summary>
        public virtual void ClearCache()
        {
            // Clears all the stores sequentially
            foreach (ICacheStore<K, V> store in _stores)
            {
                lock (store)
                {
                    store.Clear();
                }
            }
        }

        /// <summary>
        /// Returns if a given key is present in current cache content
        /// </summary>
        /// <param name="key">Key to search.</param>
        public virtual bool ContainsKey(K key)
        {
            foreach (ICacheStore<K, V> store in _stores)
            {
                if (store.ContainsKey(key))
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Removes the supplied key from the cache.
        /// </summary>
        /// <param name="key">Key to remove.</param>
        protected virtual void Remove(K key)
        {
            // Removes the value from all the stores sequentially
            foreach (ICacheStore<K, V> store in _stores)
            {
                lock (store)
                {
                    store.Remove(key);
                }
            }
        }

        protected bool SearchInCache(K key, out int i, out ICacheStore<K, V> store, out V value)
        {
            i = 0;
            store = null;

            // Search for the value on all configured stores sequentially
            do
            {
                store = _stores[i];

                if (store.Get(key, out value))
                {
                    ++_cacheHit;
                    return true;
                }
                else
                    i++;
            }
            while (store != null && i < _stores.Length);

            value = default(V);
            return false;
        }

        /// <summary>
        /// Returns the value stored with the supplied key.
        /// </summary>
        /// <param name="key">Key.</param>
        /// <returns>Value stored.</returns>
        public virtual V Get(K key)
        {

            V value;
            ICacheStore<K, V> store;
            int i;

            //Get value by calling SearchInCache() OR AddInternal()
            if (!SearchInCache(key, out i, out store, out value))
            {
                lock (_lockObject)
                {
                    if (!SearchInCache(key, out i, out store, out value))
                    {
                        value = AddInternal(key);

                        ++_cacheMiss;

                        i = _stores.Length;
                    }
                }
            }

            //Add value to cache, if it is not in cache yet
            if (i > 0)
            {
                for (int j = i - 1; j >= 0; j--)
                {
                    store = _stores[j];

                    store.Add(key, value);
                }
            }

            return value;

        }

        /// <summary>
        /// Method called when a given key is not found on the dictionary.
        /// </summary>
        /// <param name="key">Supplied key.</param>
        /// <returns>The value that corresponds to the supplied key.</returns>
        protected abstract V AddInternal(K key);

        /// <summary>
        /// Returns all entries in current cache instance
        /// </summary>
        public virtual Dictionary<K, V> GetAll()
        {
            foreach (ICacheStore<K, V> store in _stores)
            {
                if (store is InProcessCacheStore<K, V>)
                {
                    if (store is InProcessCacheStore<K, V>)
                    {
                        var inProcessCacheStore = (InProcessCacheStore<K, V>)store;
                        return inProcessCacheStore.GetAll();
                    }
                    //FOR OTHER STORES IT IS NOT POSSIBLE YET !!
                }
            }
            return null;
        }

        /// <summary>
        /// Warms up cache content 
        /// </summary>
        public virtual void WarmUp()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// For debug information purposes only - returns cache content basic info
        /// </summary>
        public virtual Dictionary<String, String> GetFullInfo()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Removes the supplied key from the cache.
        /// </summary>
        /// <param name="key">Key to remove.</param>
        public virtual void RemoveSingleEntry(string key)
        {
            throw new NotImplementedException();
        }

    }
}
