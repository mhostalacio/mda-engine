﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Util.Cache
{
    /// <summary>
    /// Defines the contract for cache configuration classes.
    /// </summary>
    public interface ICacheConfiguration
    {
        /// <summary>
        /// Returns the list of <see cref="ICacheStore"/> instances configured to the supplied cache manager.
        /// </summary>
        /// <param name="manager">Cache manager instance.</param>
        /// <returns>List of <see cref="ICacheStore"/> instances.</returns>
        ICacheStore<K, V>[] GetStores<K, V>(ICache<K, V> manager);
    }
}
