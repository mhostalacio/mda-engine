﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Transactions.Configuration;
using System.Configuration;
using System.Text.RegularExpressions;
using System.IO;
using Library.Util.AOP;
using Library.Util.FileStorage;

namespace Library.Util
{
    public static class CommonSettings
    {
        public static long TotalDatabaseHit { get; set; }
        private static TransactionOptions? _defaultTransactionOptions;
        private static TimeSpan _defaultTransactionTimeSpan = new TimeSpan(0, 30, 0);
        private static String _databaseName = ConfigurationManager.AppSettings["DatabaseName"];
        public const String PUBLIC_LOGIN_GUID = "PUBLIC";
        public static string FilePath = ConfigurationManager.AppSettings["FilePath"];
        public static string ImagePath = ConfigurationManager.AppSettings["ImagePath"];
        public static string DefaultTimezoneCode = ConfigurationManager.AppSettings["DefaultTimezoneCode"];
        public static string ServiceLocatorInstanceTypeName = ConfigurationManager.AppSettings["ServiceLocator.InstanceTypeName"];
        public const string MODEL_KEY_HIDDEN_FIELD_NAME = "mkey";
 
        public const string POST_ACTION_NAME_HIDDEN_FIELD_NAME = "Post_Back_Action_Name_Hidden";
        public const string POST_ARGUMENTS_HIDDEN_FIELD_NAME = "Post_Back_Arguments_Hidden";
        public const string IS_ON_REQUEST_HIDDEN_FIELD_NAME = "hdnIsOnRequest";
        public static string NoReplyEmail = ConfigurationManager.AppSettings["NoReplyEmail"];
        public static Boolean PersistLogs = ConfigurationManager.AppSettings["PersistLogs"] != null ? Boolean.Parse(ConfigurationManager.AppSettings["PersistLogs"]) : false;
        public static string ApplicationName = ConfigurationManager.AppSettings["ApplicationName"];
        public static string DefaultCurrencyCode = ConfigurationManager.AppSettings["DefaultCurrencyCode"];

  

        public static TransactionOptions DefaultTransactionOptions
        {
            get
            {
                if (_defaultTransactionOptions == null)
                {
                    lock (typeof(CommonSettings))
                    {
                        if (_defaultTransactionOptions == null)
                        {
                            TransactionOptions opts = new TransactionOptions();
                            opts.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;

                            DefaultSettingsSection defSetts = (DefaultSettingsSection)ConfigurationManager.GetSection("system.transactions/defaultSettings");

                            if (defSetts != null)
                            {
                                opts.Timeout = defSetts.Timeout;
                            }
                            else
                                opts.Timeout = _defaultTransactionTimeSpan;

                            _defaultTransactionOptions = opts;
                        }
                    }
                }

                return _defaultTransactionOptions.Value;
            }
        }

        public static string CommonDataBaseName { get; set; }

        public static string DefaultLanguageCode 
        { 
            get 
            {
                return ConfigurationManager.AppSettings["DefaultLanguageCode"]; 
            }
        }

        public static String DataBaseName
        {
            get
            {
                return _databaseName;
            }
        }

        public static bool UseLazyLoading = Boolean.Parse(ConfigurationManager.AppSettings["UseLazyLoading"]); //TODO: get from config

        public static bool CascadeModelPersisterInstrumentationEnabled = false;

        public static TimeSpan WCFPooledServiceChannelTimeout { get; set; }

        
    }
}
