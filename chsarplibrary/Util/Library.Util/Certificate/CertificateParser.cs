﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Util.Certificate
{
    public static class CertificateParser
    {
        //KeyUsage ::= BIT STRING {

        //  digitalSignature        (0),
        //  nonRepudiation          (1),
        //  keyEncipherment         (2),
        //  dataEncipherment        (3),
        //  keyAgreement            (4),
        //  keyCertSign             (5),
        //  cRLSign                 (6),
        //  encipherOnly            (7),
        //  decipherOnly            (8) }


        public static Boolean IsRecognizedCertificate(string keyUsage)
        {
            string[] usage = keyUsage.Split(',');
            return (usage.Length > 0 && usage[1].Equals("true", StringComparison.InvariantCultureIgnoreCase)) ? true : false;
        }

        /// <summary>
        /// Recursively searches the supplied AD string for all groups.
        /// </summary>
        /// <param name="data">The string returned from AD to parse for a group.</param>
        /// <param name="delimiter">The string to use as the seperator for the data. ex. ","</param>
        /// <returns>null if no groups were found -OR- data is null or empty.</returns>
        public static String Parse(string data, string delimiter)
        {
            if (data == null) return null;

            //data = data.ToUpper(); // why did i add this?
            if (!data.Contains(delimiter)) return null;


            int start = data.IndexOf(delimiter) + 3;
            int length = data.IndexOf(',', start) - start;
            if (length == 0) return null; //the group is empty
            if (length > 0)
            {
                return data.Substring(start, length);
            }
            else //no comma found after current group so just use the whole remaining string
            {
                return data.Substring(start);
            }
        }
    }
}
