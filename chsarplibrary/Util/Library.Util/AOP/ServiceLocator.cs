﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Util.AOP
{
    /// <summary>
    /// Provides methods that locates dependencies.
    /// </summary>
    public static class ServiceLocator
    {
        private static IServiceLocator _serviceLocator = null;

        static ServiceLocator()
        {
            ResetInstance();
        }

        /// <summary>
        /// The singleton IServiceLocator instance used to retrieve services.
        /// </summary>
        public static IServiceLocator Instance
        {
            get
            {
                return _serviceLocator;
            }
            set
            {
                _serviceLocator = value;
            }
        }

        /// <summary>
        /// Gets the service from the supplied Type and throws an error if not found.
        /// </summary>
        /// <typeparam name="T">Service type.</typeparam>
        /// <returns>Service implementation</returns>
        public static T GetService<T>()
        {
            return GetService<T>(true);
        }

        /// <summary>
        /// Gets the service from the supplied Type and depending on the value supplied to the  throws an error if not found or returns null.
        /// </summary>
        /// <param name="throwErrorIfNotFound">Indicates if it should throw an error if the Service implementation cannot be found.</param>
        /// <typeparam name="T">Service type.</typeparam>
        /// <returns>Service implementation</returns>
        public static T GetService<T>(bool throwErrorIfNotFound)
        {
            T svc = default(T);

            if (_serviceLocator == null)
            {
                if (throwErrorIfNotFound)
                    throw new NullReferenceException("The ServiceLocator.Instance property is null! Please check if the CommonSettings.ServiceLocatorInstanceTypeName was setted with a valid IServiceLocator implementation.");
            }
            else
            {
                //try
                //{
                    svc = _serviceLocator.GetService<T>();
                //}
                //catch (Exception)
                //{
                //    if (throwErrorIfNotFound)
                //    {
                //        throw;
                //    }
                //}
                /*
                if (svc == null && throwErrorIfNotFound)
                {
                    throw new ArgumentException(String.Format("The service \"{0}\" implementation was not located!", typeof(T).FullName));
                }
                */
            }

            return svc;
        }

        /// <summary>
        /// Sets the Instance value with the an instance of the type supplied on the App.config "ServiceLocator.Instance" appsettings key.
        /// </summary>
        public static void ResetInstance()
        {
            if (!String.IsNullOrEmpty(CommonSettings.ServiceLocatorInstanceTypeName))
            {
                Type svcLocType = Type.GetType(CommonSettings.ServiceLocatorInstanceTypeName, true);

                _serviceLocator = (IServiceLocator)Activator.CreateInstance(svcLocType);
            }
            else
            {
                _serviceLocator = null;
            }
        }
    }
}
