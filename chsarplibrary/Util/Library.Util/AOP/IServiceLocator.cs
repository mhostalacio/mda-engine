﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Util.AOP
{
    public interface IServiceLocator
    {
        /// <summary>
        /// Gets the service from the supplied Type.
        /// </summary>
        /// <typeparam name="T">Service type.</typeparam>
        /// <returns>Service implementation<.wwww/returns>
        T GetService<T>();

        /// <summary>
        /// Gets the service from the supplied Type and Argument.
        /// </summary>
        /// <typeparam name="T">Service type.</typeparam>
        /// <param name="param">Parameter to initialize type</param>
        /// <returns>Service implementation<.wwww/returns>
        T GetService<T>(String param);
    }
}
