﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spring.Context;
using Spring.Context.Support;

namespace Library.Util.AOP
{
    public class SpringServiceLocator : IServiceLocator
    {
        public static String ROOT_CTX_NAME = "spring.root";

        #region IServiceLocator Members

        public T GetService<T>()
        {
            T svc = default(T);

            IApplicationContext ctx = null;

            //try
            //{
                
            ctx = ContextRegistry.GetContext();
            //}
            //catch (ApplicationContextException ex)
            //{
            //    // Hides exception because the context may not be registered and the IsContextRegistered method
            //    // does not initialize the declared contexts
            //    Console.WriteLine(ex.Message + ex.StackTrace);
            //}

            if (ctx != null)
            {
                Type serviceType = typeof(T);

                String serviceName = serviceType.FullName;

                if (ctx.ContainsObject(serviceName))
                {
                    Object obj = ctx.GetObject(serviceName);

                    Type actualType = obj.GetType();

                    if (serviceType.IsAssignableFrom(actualType))
                        svc = (T)obj;
                    else
                        throw new ArgumentException(String.Format("The service \"{0}\" actual implementation ({1}) does not implements or inherits from \"{2}\"!", serviceName, actualType.AssemblyQualifiedName, serviceType.AssemblyQualifiedName));
                }
            }

            return svc;
        }

        public T GetService<T>(string param)
        {
            T svc = default(T);

            IApplicationContext ctx = null;

            //try
            //{

            ctx = ContextRegistry.GetContext();
            //}
            //catch (ApplicationContextException ex)
            //{
            //    // Hides exception because the context may not be registered and the IsContextRegistered method
            //    // does not initialize the declared contexts
            //    Console.WriteLine(ex.Message + ex.StackTrace);
            //}

            if (ctx != null)
            {
                Type serviceType = typeof(T);

                String serviceName = serviceType.FullName;

                if (ctx.ContainsObject(serviceName))
                {
                    Object obj = ctx.GetObject(serviceName, new Object[] { param });

                    Type actualType = obj.GetType();

                    if (serviceType.IsAssignableFrom(actualType))
                        svc = (T)obj;
                    else
                        throw new ArgumentException(String.Format("The service \"{0}\" actual implementation ({1}) does not implements or inherits from \"{2}\"!", serviceName, actualType.AssemblyQualifiedName, serviceType.AssemblyQualifiedName));
                }
            }

            return svc;
        }

        #endregion


       
    }
}
