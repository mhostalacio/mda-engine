﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Util.Attributes
{
    /// <summary>
    /// Base class for attributes that keep track whenever their value changes.   
    /// </summary>
    public abstract class PersistableAttribute
    {
        // TODO: Create EventHandler specific for BecameDirty event (value, culture) 
        protected bool _isDirty = false;

        /// <summary>
        /// This event its triggered when the attribute becomes dirty.
        /// </summary>
        public event EventHandler BecameDirty;

        /// <summary>
        /// Checks if the attribute value have been changed.
        /// </summary>
        public bool IsDirty
        {
            get { return _isDirty; }
        }

        /// <summary>
        /// Triggers the BecameDirty event.
        /// </summary>
        protected virtual void OnBecameDirty()
        {
            if (BecameDirty != null)
                BecameDirty(this, new EventArgs());
        }

    }
}
