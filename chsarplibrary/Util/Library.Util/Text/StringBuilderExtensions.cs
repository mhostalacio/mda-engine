﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Util.Text
{
    public static class StringBuilderExtensions
    {
        public static void AppendDelimiter(this StringBuilder sb, string value, string delimiter, bool isLast)
        {
            sb.Append(value);

            if (!isLast)
                sb.Append(delimiter);
        }
    }
}
