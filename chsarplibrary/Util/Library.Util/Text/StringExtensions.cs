﻿using System.Collections.Generic;
using System.Text;

namespace Library.Util.Text
{
    public static class StringExtensions
    {
        #region Methods


        /// <summary>
        /// Splits the string using the indicated separator and returns the part in the supplied position 
        /// </summary>
        /// <param name="str">The string from which the part will be returned</param>
        /// <param name="separator">The string separator</param>
        /// <param name="position">The position of the parte to be returned</param>
        /// <returns></returns>
        public static string Part(this string str, char separator, int position)
        {
            if (string.IsNullOrEmpty(str))
                return str;

            string[] splitted = str.Split(separator);

            if (splitted.Length >= position)
            {
                return splitted[position - 1];
            }

            return str;
        }

        public static string Truncate(this string str, int maxLength)
        {
            if (string.IsNullOrEmpty(str))
                return str;

            if (str.Length > maxLength)
                return str.Substring(0, maxLength);
            else
                return str;
        }

        public static string TruncateWithEllipsis(this string str, int maxLength)
        {
            if (string.IsNullOrEmpty(str))
                return str;

            if (str.Length > maxLength)
                return string.Format("{0}...", Truncate(str, maxLength - 3));
            else
                return str;
        }

        /// <summary>
        /// Removes all diacritic from the provided string
        /// </summary>
        /// <param name="str">The string to be processed</param>
        /// <returns>string</returns>
        public static string RemoveDiacritics(this string str)
        {
            Encoding destEncoding = Encoding.GetEncoding("iso-8859-8");

            return destEncoding.GetString(Encoding.Convert(Encoding.UTF8, destEncoding, Encoding.UTF8.GetBytes(str)));
        }

        /// <summary>
        /// Remove Spaces
        /// </summary>
        /// <param name="str">The string to be processed</param>
        /// <returns>string</returns>
        public static string RemoveSpacesAndPunctuation(this string str)
        {
            return str.Replace(" ", "").Replace(".", "").Replace(",", "").Replace("'", "").Replace("\"", "").Replace("!", "").Replace("?", "");
        }

        /// <summary>
        /// Recursively searches the supplied AD string for all groups.
        /// </summary>
        /// <param name="data">The string returned from AD to parse for a group.</param>
        /// <param name="delimiter">The string to use as the seperator for the data. ex. ","</param>
        /// <returns>null if no groups were found -OR- data is null or empty.</returns>
        public static List<string> Parse(this string data, string delimiter)
        {
            if (data == null) return null;

            if (!delimiter.EndsWith("=")) delimiter = delimiter + "=";

            //data = data.ToUpper(); // why did i add this?
            if (!data.Contains(delimiter)) return null;

            //base case
            var result = new List<string>();
            int start = data.IndexOf(delimiter) + 3;
            int length = data.IndexOf(',', start) - start;
            if (length == 0) return null; //the group is empty
            if (length > 0)
            {
                result.Add(data.Substring(start, length));

                //only need to recurse when the comma was found, because there could be more groups
                var rec = Parse(data.Substring(start + length), delimiter);
                if (rec != null) result.AddRange(rec); //can't pass null into AddRange() :(
            }
            else //no comma found after current group so just use the whole remaining string
            {
                result.Add(data.Substring(start));
            }

            return result;
        }

        #endregion
    }
}
