﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Util.Text
{
    public interface IMultiLanguageAttribute
    {
        object CurrentValue { get; }
    }
}
