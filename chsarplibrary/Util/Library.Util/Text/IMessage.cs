﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Util.Text
{
    public interface IMessage
    {
        String Text { get; }

        MessageTypeEnum MessageType { get; }

        String AssociatedFieldClientID { get; }
    }

    public enum MessageTypeEnum
    {
        ERROR,
        WARNING,
        INFO,
        SUCCESS
    }
}
