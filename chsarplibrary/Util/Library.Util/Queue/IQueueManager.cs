﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Util.Queue
{
    public interface IQueueManager
    {
        void Enqueue(IQueueItem item);

        void Enqueue(IList<IQueueItem> items);

        IList<IQueueItem> GetItemsToConsume(int quantity);

        Boolean ProcessItem(IQueueItem item);
    }
}
