﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Util.Queue
{
    public interface IQueueConsumer<T> 
        where T : IQueueItem
    {
        Boolean ProcessItem(T item);
    }
}
