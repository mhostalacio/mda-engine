﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Util.Transactions;

namespace Library.Util.Logging
{
    public interface ILogPersister
    {
        void AddLog(BusinessTransaction log);

        void SaveAndClearLogs();
    }
}
