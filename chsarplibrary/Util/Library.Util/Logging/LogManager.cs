﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Util.AOP;
using Library.Util.Transactions;

namespace Library.Util.Logging
{
    public class LogManager 
    {
        private static LogManager _instance = null;
        private int _logCount = 0;
        private const long _logMax = 10;
 

        private LogManager()
        {
            _logCount = 0;
        }

        public static LogManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (typeof(LogManager))
                    {
                        if (_instance == null)
                        {
                            Initialize();
                        }
                    }
                }
                return _instance;
            }
        }

        public static LogManager Initialize()
        {
            _instance = new LogManager();
            return _instance;
        }

        public void AddLog(BusinessTransaction log)
        {
            _logCount++;
            ILogPersister persister = null;
            if (CommonSettings.PersistLogs)
            {
                persister = ServiceLocator.Instance.GetService<ILogPersister>();
                persister.AddLog(log);
            }
            

            if (_logCount >= _logMax)
            {
                if (CommonSettings.PersistLogs)
                {
                    persister.SaveAndClearLogs();
                }
                _logCount = 0;
            }
        }



    }
}
