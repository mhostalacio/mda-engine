﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Util.Collections;

namespace Library.Util.Localization
{
    public static class LanguageSettings
    {
        private static PagedList<CultureInfo> _activeLanguageList = new PagedList<CultureInfo>();

        public static PagedList<CultureInfo> ActiveLanguageList
        {
            get
            {
                if (_activeLanguageList.Count == 0)
                {
                    lock (_activeLanguageList)
                    {
                        if (_activeLanguageList.Count == 0)
                        {
                            _activeLanguageList = GetActiveLanguages();
                        }
                    }
                }

                return _activeLanguageList;
            }
        }


        private static PagedList<CultureInfo> GetActiveLanguages()
        {
            _activeLanguageList = new PagedList<CultureInfo>();
            CultureInfo info = CultureInfo.CreateSpecificCulture("pt-PT");
            _activeLanguageList.Add(info);
            info = CultureInfo.CreateSpecificCulture("en-US");
            _activeLanguageList.Add(info);
            return _activeLanguageList;
        }
    }
}
