﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Library.Util.Localization
{
    public interface ITranslationProvider
    {

        String GetTranslation(CultureInfo culture, String sentenceType, String sentenceCode);

    }
}
