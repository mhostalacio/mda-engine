﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Library.Util.FileStorage
{
    public interface IFileStorageProvider
    {
        /// <summary>Store file</summary>
        /// <param name="postedFile">File to be stored</param>
        /// <param name="storageKey">Folder or Container of the storage to use</param>
        /// <param name="fileUniqueName">Name of the physical file on disk (should be a GUID)</param>
        /// <returns>Strorage Key</returns>
        String StoreFile(System.Web.HttpPostedFileBase postedFile, String storageKey, String fileUniqueName);

        /// <summary>Store file</summary>
        /// <param name="file">File to be stored</param>
        /// <param name="storageKey">Folder or Container of the storage to use</param>
        /// <param name="fileUniqueName">Name of the physical file on disk (should be a GUID)</param>
        /// <returns>Strorage Key</returns>
        String StoreFile(System.IO.Stream file, String storageKey, String fileUniqueName);

        /// <summary>Store file</summary>
        /// <param name="file">File to be stored</param>
        /// <param name="storageKey">Folder or Container of the storage to use</param>
        /// <param name="fileUniqueName">Name of the physical file on disk (should be a GUID)</param>
        /// <returns>Strorage Key</returns>
        String StoreFile(byte[] file, String storageKey, String fileUniqueName);

        /// <summary>Retrieve file</summary>
        /// <param name="fileUniqueName">Name of the physical file to be retrieved</param>
        /// <param name="storageKey">Folder or Container of the storage to use</param>
        /// <returns>Physical file</returns>
        byte[] GetFile(String fileUniqueName, String storageKey);

        /// <summary>Get url of the storage</summary>
        /// <param name="storageKey">Folder or Container of the storage to use</param>
        /// <returns>URL</returns>
        String GetDirectPath(String storageKey);


        /// <summary>
        /// Delete file
        /// </summary>
        /// <param name="storageKey">Folder or Container of the storage to use</param>
        /// <param name="filename">Name of the physical file to be deleted</param>
        void DeleteFile(String storageKey, String filename);

    }
}
