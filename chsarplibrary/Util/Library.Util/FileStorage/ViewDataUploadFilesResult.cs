﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Util.FileStorage
{
    public class ViewDataUploadFilesResult
    {
        public string name { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public string url { get; set; }
        public string delete_url { get; set; }
        public string thumbnail_url { get; set; }
        public string delete_type { get; set; }
        public string status { get; set; }
        public string error { get; set; }
    }
}
