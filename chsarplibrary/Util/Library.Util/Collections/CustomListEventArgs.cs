﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Util.Collections
{
    public class CustomListEventArgs<T> : EventArgs
    {
        public T Item { get; set; }

        public int Index { get; set; }
    }

    public class CustomListReplaceItemEventArgs<T> : EventArgs
    {
        public T ReplacingItem { get; set; }

        public T ReplacedItem { get; set; }

        public int Index { get; set; }
    }  
}
