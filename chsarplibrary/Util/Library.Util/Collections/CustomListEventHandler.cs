﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Util.Collections
{
    public delegate void CustomListEventHandler<T>(CustomListEventArgs<T> args);

    public delegate void CustomListReplaceItemEventHandler<T>(CustomListReplaceItemEventArgs<T> args);
}
