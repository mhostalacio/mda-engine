﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Server;
using System.Data;
using Library.Util.Text;

namespace Library.Util.Collections
{
    public static class CollectionExtensions
    {
        public static bool IsNullOrEmpty<T>(this ICollection<T> instance)
        {
            if (instance != null)
            {
                return (instance.Count == 0);
            }
            return true;
        }

        public static void AddIfNew<T>(this ICollection<T> instance, T item)
        {
            if (instance == null)
                throw new ArgumentNullException("instance");

            if (!instance.Contains(item))
            {
                instance.Add(item);
            }
        }

        public static void AddIfNew<T>(this ICollection<T> instance, IEnumerable<T> items)
        {
            if (instance == null)
                throw new ArgumentNullException("instance");

            foreach (T item in items)
            {
                if (!instance.Contains(item))
                {
                    instance.Add(item);
                }
            }
        }

        public static ListT Converge<T, ListT>(this IEnumerable<ListT> listOfLists)
            where ListT : IList<T>, new()
        {
            if (listOfLists == null)
                throw new ArgumentNullException("listOfLists");

            ListT allListsTogether = new ListT();

            foreach (ListT list in listOfLists)
            {
                foreach (T item in list)
                {
                    allListsTogether.Add(item);
                }
            }

            return allListsTogether;
        }


        public static IList<T> Converge<T>(this IEnumerable<IList<T>> listOfLists)
        {
            if (listOfLists == null)
                throw new ArgumentNullException("listOfLists");

            IList<T> allListsTogether = new List<T>();

            foreach (IEnumerable<T> list in listOfLists)
            {
                foreach (T item in list)
                {
                    allListsTogether.Add(item);
                }
            }

            return allListsTogether;
        }

        /// <summary>
        /// Returns the previous item from the supplied item.
        /// </summary>
        /// <typeparam name="T">List item type.</typeparam>
        /// <param name="items">List of items.</param>
        /// <param name="item">Item.</param>
        /// <returns>Previous item.</returns>
        public static T PreviousItem<T>(this IList<T> items, T item)
        {
            T previousItem = default(T);

            int idxItem = items.IndexOf(item);

            if (idxItem > 0)
            {
                previousItem = items[idxItem - 1];
            }

            return previousItem;
        }

        /// <summary>
        /// Returns the list of items that are before the supplied item.
        /// </summary>
        /// <typeparam name="T">List item type.</typeparam>
        /// <param name="items">List of items.</param>
        /// <param name="item">Item.</param>
        /// <returns>List of items that are before the supplied item.</returns>
        public static IList<T> PreviousItems<T>(this IList<T> items, T item)
        {
            int idxItem = items.IndexOf(item);

            IList<T> previousItems = new List<T>(idxItem);

            for (int i = 0; i < idxItem; i++)
            {
                previousItems.Add(items[i]);
            }

            return previousItems;
        }

        /// <summary>
        /// Returns the list of items that are after the supplied item.
        /// </summary>
        /// <typeparam name="T">List item type.</typeparam>
        /// <param name="items">List.</param>
        /// <param name="item">Item.</param>
        /// <returns>List of items.</returns>
        public static IList<T> Foward<T>(this IList<T> items, T item)
        {
            int idxItem = items.IndexOf(item);

            IList<T> fowardItems = new List<T>(items.Count - idxItem);

            for (int i = idxItem + 1; i < items.Count; i++)
            {
                fowardItems.Add(items[i]);
            }

            return fowardItems;
        }

        public static bool IsLastItem<T>(this IList<T> itens, T item)
        {
            if (item == null)
                throw new ArgumentNullException("item");

            return itens.IndexOf(item) == itens.Count - 1;
        }

        public static string ToDelimitedString<T>(this IList<T> list, string delimiter)
        {
            return ToDelimitedString(list, null, delimiter);
        }

        public static string ToDelimitedString<T>(this IList<T> list, Func<T, string> toString, string delimiter)
        {
            if (list == null)
                throw new ArgumentNullException("list");

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < list.Count; i++)
            {
                T item = list[i];

                string strVersion = string.Empty;

                if (item != null)
                    strVersion = (toString == null) ? item.ToString() : toString(item);

                sb.AppendDelimiter(strVersion, delimiter, i == list.Count - 1);
            }

            return sb.ToString();
        }

        public static void Each<T>(this IEnumerable<T> instance, Action<T> action)
        {
            foreach (T local in instance)
            {
                action(local);
            }
        }

        public static void Each<T>(this IEnumerable<T> instance, Action<T, int> action)
        {
            int num = 0;
            foreach (T local in instance)
            {
                action(local, num++);
            }
        }

        public static CustomList<String> ToStringList<T>(this IList<T> list)
        {
            CustomList<String> strList = new CustomList<string>(list.Count);

            foreach (T item in list)
            {
                strList.Add(item.ToString());
            }

            return strList;
        }

        public static IList<IList<T>> DivideInChunk<T>(this IList<T> list, int chunkSize)
        {
            var result = new List<IList<T>>();
            int currentChunkIndex = 0;
            int currentChunckItemsCount = 0;
            List<T> currentChunk = null;

            if (chunkSize == 0)
            {
                result.Add(list);
                return result;
            }

            foreach (T item in list)
            {
                //INITIALIZE CHUNK IF NECESSARY
                if (currentChunckItemsCount == 0)
                {
                    currentChunk = new List<T>();
                    result.Add(currentChunk);
                }

                currentChunk.Add(item);
                ++currentChunckItemsCount;

                //FINALIZE CHUNK IF NECESSARY
                if (currentChunckItemsCount == chunkSize)
                {
                    currentChunckItemsCount = 0;
                    ++currentChunkIndex;
                }
            }

            return result;
        }

        //TODO: Remove
        public static IEnumerable<SqlDataRecord> ToIntTable(this IEnumerable<int> ids)
        {
            var sdr = new SqlDataRecord(new SqlMetaData("Id", SqlDbType.Int));

            foreach (int id in ids)
            {
                sdr.SetInt32(0, id);

                yield return sdr;
            }
        }

        public static IEnumerable<SqlDataRecord> ToIntListTable(this IEnumerable<int> ints)
        {
            var sdr = new SqlDataRecord(new SqlMetaData("Value", SqlDbType.Int));

            foreach (int id in ints)
            {
                sdr.SetInt32(0, id);

                yield return sdr;
            }
        }

        public static IEnumerable<SqlDataRecord> ToHashCodeListTable<T>(this IEnumerable<T> enumValues)
        {
            var sdr = new SqlDataRecord(new SqlMetaData("Value", SqlDbType.Int));

            foreach (T id in enumValues)
            {
                sdr.SetInt32(0, id.GetHashCode());

                yield return sdr;
            }
        }

        public static IEnumerable<SqlDataRecord> ToSqlDataRecord(this IEnumerable<long> ids)
        {
            var sdr = new SqlDataRecord(new SqlMetaData("Id", SqlDbType.BigInt));

            foreach (long id in ids)
            {
                sdr.SetInt64(0, id);

                yield return sdr;
            }
        }

        
    }
}
