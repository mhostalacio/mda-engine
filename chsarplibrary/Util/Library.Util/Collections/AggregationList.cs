﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Collections;
using Microsoft.SqlServer.Server;
using System.Data;

namespace Library.Util.Collections
{
    [Serializable]
    public class AggregationList : CustomList<Int64>, IEquatable<AggregationList>, IList, IEnumerable<SqlDataRecord>
    {
        private List<long> _removedIds = null;
        private List<long> _addedIds = null;

        #region Constructors

        public AggregationList()
            : base()
        {

        }

        public AggregationList(int count)
            : base(count)
        {

        }

        public AggregationList(IEnumerable<long> items)
            : base(items)
        {
        }

        public AggregationList(IEnumerable<long> addedItems, IEnumerable<long> existingItems, IEnumerable<long> removedItems)
            : base(existingItems == null ? new List<long>() : existingItems)
        {
            this._addedIds = addedItems == null ? null : new List<long>(addedItems);
            this._removedIds = removedItems == null ? null : new List<long>(removedItems);
        }

        #endregion

        protected override void OnAddedItem(long item, int idx)
        {
            base.OnAddedItem(item, idx);

            //if (item > 0)
            //{
            // Removes the once removed item
            if (_removedIds != null && _removedIds.Contains(item))
            {
                _removedIds.Remove(item);
                return;
            }

            if (_addedIds == null)
            {
                _addedIds = new List<long>();
            }

            _addedIds.Add(item);
            //}            
        }

        protected override void OnRemovedItem(long item, int idx)
        {
            base.OnRemovedItem(item, idx);

            //if (item > 0)
            //{
            if (_addedIds != null && _addedIds.Contains(item))
            {
                _addedIds.Remove(item);
                return;
            }

            if (_removedIds == null)
                _removedIds = new List<long>();

            _removedIds.Add(item);
            //}
        }

        /// <summary>
        /// List of removed ids. (It can be null)
        /// </summary>
        public List<long> RemovedIds
        {
            get { return _removedIds; }
        }

        /// <summary>
        /// List of added ids. (It can be null)
        /// </summary>
        public List<long> AddedIds
        {
            get { return _addedIds; }
        }

        /// <summary>
        /// Aggregation List indexer 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public override long this[int index]
        {
            get
            {
                return base[index];
            }
            set
            {
                long oldValue = base[index];
                base[index] = value;
                if (this.AddedIds.Contains(oldValue))
                {
                    this.AddedIds.Insert(this.AddedIds.IndexOf(oldValue), value);
                    this.AddedIds.Remove(oldValue);
                }
                else
                {
                    this.RemovedIds.Add(oldValue);
                    this.AddedIds.Add(value);
                }

            }
        }

        #region IEquatable<AggregationList> Members

        public bool Equals(AggregationList other)
        {
            bool equals = false;

            if (other != null)
            {
                if (Count == other.Count)
                {
                    equals = true;

                    // Compares the list of Ids without taking account the order
                    int i = 0;

                    while (i < Count && equals)
                    {
                        equals = other.Contains(this[i]);
                        i++;
                    }
                }
            }

            return equals;
        }

        public override bool Equals(object obj)
        {
            if (obj is AggregationList)
                return Equals((AggregationList)obj);
            else
                return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Writes this aggregation list to a Xml writer.
        /// </summary>
        /// <param name="writer">XmlWriter to write to.</param>
        public void ToXml(XmlWriter writer)
        {
            if (writer == null)
                throw new ArgumentNullException("writer");

            ToXml(writer, "AggregationList");
        }

        /// <summary>
        /// Writes this aggregation list to a Xml writer.
        /// </summary>
        /// <param name="writer">XmlWriter to write to.</param>
        /// <param name="elementName">The name of Xml element representing this aggregation list.</param>
        public void ToXml(XmlWriter writer, string elementName)
        {
            if (writer == null)
                throw new ArgumentNullException("writer");

            if (String.IsNullOrEmpty(elementName))
                throw new ArgumentException("Argument required!", "elementName");

            writer.WriteStartElement(elementName);

            if (Count > 0)
            {
                writer.WriteStartElement("Existing");

                foreach (long id in this)
                {
                    writer.WriteElementString("Id", id.ToString());
                }

                writer.WriteEndElement();
            }

            if (_addedIds != null && _addedIds.Count > 0)
            {
                writer.WriteStartElement("Added");

                foreach (long id in _addedIds)
                {
                    writer.WriteElementString("Id", id.ToString());
                }

                writer.WriteEndElement();
            }

            if (_removedIds != null && _removedIds.Count > 0)
            {
                writer.WriteStartElement("Removed");

                foreach (long id in _removedIds)
                {
                    writer.WriteElementString("Id", id.ToString());
                }

                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        #endregion

        public void MarkAsSaved()
        {
            if (this.RemovedIds != null)
            {
                this.RemovedIds.Clear();
                this._removedIds = null;
            }         

            if (this.AddedIds != null)
            {
                this.AddedIds.Clear();
                this._addedIds = null;
            }
        }

        IEnumerator<SqlDataRecord> IEnumerable<SqlDataRecord>.GetEnumerator()
        {
            var sdr = new SqlDataRecord(
                new SqlMetaData("AggregatedEntityId", SqlDbType.BigInt));


            foreach (long item in this)
            {
                // AggregatedEntityId
                sdr.SetInt64(0, item);

                yield return sdr;
            }
        }
    }
}
