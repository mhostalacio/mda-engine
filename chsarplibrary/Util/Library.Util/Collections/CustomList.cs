﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices;
using System.Threading;
using System.Linq;
using System.Linq.Expressions;

namespace Library.Util.Collections
{
    [Serializable]
    public class CustomList<T> : ICustomList<T>, ICollection<T>, IEnumerable<T>, IList, ICollection, IEnumerable
    {
        #region Events

        public event CustomListEventHandler<T> AddingItem;

        public event CustomListEventHandler<T> RemovingItem;

        public event CustomListReplaceItemEventHandler<T> ReplacingItem;

        public event CustomListEventHandler<T> AddedItem;

        public event CustomListEventHandler<T> RemovedItem;

        public event CustomListReplaceItemEventHandler<T> ReplacedItem;

        #endregion

        #region Constants

        private const int _defaultCapacity = 4;
        public const string GREATER_THAN_OR_EQUAL_TO_COUNT_EXCEPTION_MESSAGE = "Cannot be lower than zero!";
        public const string LOWER_THAN_ZERO_ARGUMENT_EXCEPTION_MESSAGE = "Cannot be lower than zero!";

        #endregion

        #region Fields

        private static T[] _emptyArray;
        protected T[] _items;
        private int _size;

        [NonSerialized]
        private object _syncRoot;

        private int _version;

        #endregion

        #region Properties

        public bool HasMoreEntities
        {
            get;
            set;
        }

        public int Capacity
        {
            get { return _items.Length; }
            set
            {
                if (value != _items.Length)
                {
                    if (value < _size)
                    {
                        throw new ArgumentOutOfRangeException("capacity");
                    }
                    if (value > 0)
                    {
                        T[] destinationArray = new T[value];
                        if (_size > 0)
                        {
                            Array.Copy(_items, 0, destinationArray, 0, _size);
                        }
                        _items = destinationArray;
                    }
                    else
                    {
                        _items = _emptyArray;
                    }
                }
            }
        }

        #endregion

        // Methods

        #region Constructors

        static CustomList()
        {
            _emptyArray = new T[0];
        }

        public CustomList()
        {
            _items = _emptyArray;
        }

        public CustomList(IEnumerable<T> collection)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }

            ICollection<T> is2 = collection as ICollection<T>;
            if (is2 != null)
            {
                int count = is2.Count;
                _items = new T[count];
                is2.CopyTo(_items, 0);
                _size = count;
            }
            else
            {
                _size = 0;
                _items = new T[4];
                using (IEnumerator<T> enumerator = collection.GetEnumerator())
                {
                    while (enumerator.MoveNext())
                    {
                        Add(enumerator.Current);
                    }
                }
            }
        }

        public CustomList(int capacity)
        {
            if (capacity < 0)
            {
                throw new ArgumentOutOfRangeException("Capacity cannot be lower than zero!");
            }
            _items = new T[capacity];
        }

        #endregion

        #region Methods

        public virtual void AddRange(IEnumerable<T> collection)
        {
            InsertRange(_size, collection);
        }

        public ReadOnlyCollection<T> AsReadOnly()
        {
            return new ReadOnlyCollection<T>(this);
        }

        public int BinarySearch(T item)
        {
            return BinarySearch(0, Count, item, null);
        }

        public int BinarySearch(T item, IComparer<T> comparer)
        {
            return BinarySearch(0, Count, item, comparer);
        }

        public int BinarySearch(int index, int count, T item, IComparer<T> comparer)
        {
            if ((index < 0) || (count < 0))
            {
                throw new ArgumentOutOfRangeException((index < 0) ? "Index cannot be lower than zero" : "Index out of range!");
            }
            if ((_size - index) < count)
            {
                throw new ArgumentException("index");
            }
            return Array.BinarySearch<T>(_items, index, count, item, comparer);
        }

        public CustomList<TOutput> ConvertAll<TOutput>(Converter<T, TOutput> converter)
        {
            if (converter == null)
            {
                throw new ArgumentNullException("converter");
            }
            CustomList<TOutput> customList = new CustomList<TOutput>(_size);
            for (int i = 0; i < _size; i++)
            {
                customList._items[i] = converter(_items[i]);
            }
            customList._size = _size;
            return customList;
        }

        public void CopyTo(T[] array)
        {
            CopyTo(array, 0);
        }

        public void CopyTo(int index, T[] array, int arrayIndex, int count)
        {
            if ((_size - index) < count)
            {
                throw new ArgumentException("Index out of range!");
            }
            Array.Copy(_items, index, array, arrayIndex, count);
        }

        private void EnsureCapacity(int min)
        {
            if (_items.Length < min)
            {
                int num = (_items.Length == 0) ? 4 : (_items.Length * 2);
                if (num < min)
                {
                    num = min;
                }
                Capacity = num;
            }
        }

        public bool Exists(Predicate<T> match)
        {
            return (FindIndex(match) != -1);
        }

        public T Find(Predicate<T> match)
        {
            if (match == null)
            {
                throw new ArgumentNullException("match");
            }
            for (int i = 0; i < _size; i++)
            {
                if (_items[i] == null)
                    throw new ArgumentNullException(String.Format("item {0}", i));

                if (match(_items[i]))
                {
                    return _items[i];
                }
            }
            return default(T);
        }

        public CustomList<T> FindAll(Predicate<T> match)
        {
            if (match == null)
            {
                throw new ArgumentNullException("match");
            }
            CustomList<T> customList = new CustomList<T>();
            for (int i = 0; i < _size; i++)
            {
                if (match(_items[i]))
                {
                    customList.Add(_items[i]);
                }
            }
            return customList;
        }

        public int FindIndex(Predicate<T> match)
        {
            return FindIndex(0, _size, match);
        }

        public int FindIndex(int startIndex, Predicate<T> match)
        {
            return FindIndex(startIndex, _size - startIndex, match);
        }

        public int FindIndex(int startIndex, int count, Predicate<T> match)
        {
            if (startIndex > _size)
            {
                throw new ArgumentOutOfRangeException("startIndex");
            }
            if ((count < 0) || (startIndex > (_size - count)))
            {
                throw new ArgumentOutOfRangeException("count");
            }
            if (match == null)
            {
                throw new ArgumentNullException("match");
            }
            int num = startIndex + count;
            for (int i = startIndex; i < num; i++)
            {
                if (match(_items[i]))
                {
                    return i;
                }
            }
            return -1;
        }

        public T FindLast(Predicate<T> match)
        {
            if (match == null)
            {
                throw new ArgumentNullException("match");
            }
            for (int i = _size - 1; i >= 0; i--)
            {
                if (match(_items[i]))
                {
                    return _items[i];
                }
            }
            return default(T);
        }

        public int FindLastIndex(Predicate<T> match)
        {
            return FindLastIndex(_size - 1, _size, match);
        }

        public int FindLastIndex(int startIndex, Predicate<T> match)
        {
            return FindLastIndex(startIndex, startIndex + 1, match);
        }

        public int FindLastIndex(int startIndex, int count, Predicate<T> match)
        {
            if (match == null)
            {
                throw new ArgumentNullException("match");
            }
            if (_size == 0)
            {
                if (startIndex != -1)
                {
                    throw new ArgumentOutOfRangeException("startIndex");
                }
            }
            else if (startIndex >= _size)
            {
                throw new ArgumentOutOfRangeException("startIndex");
            }
            if ((count < 0) || (((startIndex - count) + 1) < 0))
            {
                throw new ArgumentOutOfRangeException("count");
            }
            int num = startIndex - count;
            for (int i = startIndex; i > num; i--)
            {
                if (match(_items[i]))
                {
                    return i;
                }
            }
            return -1;
        }

        public void ForEach(Action<T> action)
        {
            if (action == null)
            {
                throw new ArgumentNullException("match");
            }
            for (int i = 0; i < _size; i++)
            {
                action(_items[i]);
            }
        }

        public virtual Enumerator<T> GetEnumerator()
        {
            return new Enumerator<T>((CustomList<T>)this);
        }

        public CustomList<T> GetRange(int index, int count)
        {
            if ((index < 0) || (count < 0))
            {
                throw new ArgumentOutOfRangeException((index < 0) ? "Index cannot be lower than zero" : "Index out of range!");
            }
            if ((_size - index) < count)
            {
                throw new ArgumentException("Invalid Offlength!");
            }
            CustomList<T> customList = new CustomList<T>(count);
            Array.Copy(_items, index, customList._items, 0, count);
            customList._size = count;
            return customList;
        }

        public int IndexOf(T item, int index)
        {
            if (index > _size)
            {
                throw new ArgumentOutOfRangeException("index");
            }
            return Array.IndexOf<T>(_items, item, index, _size - index);
        }

        public int IndexOf(T item, int index, int count)
        {
            if (index > _size)
            {
                throw new ArgumentOutOfRangeException("index");
            }
            if ((count < 0) || (index > (_size - count)))
            {
                throw new ArgumentOutOfRangeException("count");
            }
            return Array.IndexOf<T>(_items, item, index, count);
        }

        public virtual void InsertRange(int index, IEnumerable<T> collection)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }
            if (index > _size)
            {
                throw new ArgumentOutOfRangeException("index");
            }
            ICollection<T> is2 = collection as ICollection<T>;
            if (is2 != null)
            {
                int count = is2.Count;

                for (int i = 0; i < count; i++)
                {
                    OnAddingItem(is2.ElementAt(i), i + index);
                }

                if (count > 0)
                {
                    EnsureCapacity(_size + count);
                    if (index < _size)
                    {
                        Array.Copy(_items, index, _items, index + count, _size - index);
                    }
                    if (this == is2)
                    {
                        Array.Copy(_items, 0, _items, index, index);
                        Array.Copy(_items, (int)(index + count), _items, (int)(index * 2), (int)(_size - index));
                    }
                    else
                    {
                        T[] array = new T[count];
                        is2.CopyTo(array, 0);
                        array.CopyTo(_items, index);
                    }
                    _size += count;
                }

                for (int i = 0; i < count; i++)
                {
                    OnAddedItem(is2.ElementAt(i), i + index);
                }
            }
            else
            {
                using (IEnumerator<T> enumerator = collection.GetEnumerator())
                {
                    while (enumerator.MoveNext())
                    {
                        T item = enumerator.Current;
                        int idx = index++;

                        Insert(idx, item);
                    }
                }
            }
            _version++;
        }

        private static bool IsCompatibleObject(object value)
        {
            if (!(value is T) && ((value != null) || typeof(T).IsValueType))
            {
                return false;
            }
            return true;
        }

        public int LastIndexOf(T item)
        {
            return LastIndexOf(item, _size - 1, _size);
        }

        public int LastIndexOf(T item, int index)
        {
            if (index >= _size)
            {
                throw new ArgumentOutOfRangeException("index");
            }
            return LastIndexOf(item, index, index + 1);
        }

        public int LastIndexOf(T item, int index, int count)
        {
            if (_size == 0)
            {
                return -1;
            }
            if ((index < 0) || (count < 0))
            {
                throw new ArgumentOutOfRangeException((index < 0) ? "Index cannot be lower than zero" : "Index out of range!");
            }
            if ((index >= _size) || (count > (index + 1)))
            {
                throw new ArgumentOutOfRangeException((index >= _size) ? "index" : "count", "Count cannot be bigger than collection!");
            }
            return Array.LastIndexOf<T>(_items, item, index, count);
        }

        public int RemoveAll(Predicate<T> match)
        {
            if (match == null)
            {
                throw new ArgumentNullException("match");
            }
            int index = 0;
            while ((index < _size) && !match(_items[index]))
            {
                index++;
            }
            if (index >= _size)
            {
                return 0;
            }
            int num2 = index + 1;

            Dictionary<int, T> removedItems = new Dictionary<int, T>();

            //Notify removal of first element
            OnRemovingItem(_items[index], index);
            removedItems.Add(index, _items[index]);

            while (num2 < _size)
            {
                while ((num2 < _size) && match(_items[num2]))
                {
                    //Notify removal of subsequent elements
                    OnRemovingItem(_items[num2], num2);
                    removedItems.Add(num2, _items[num2]);

                    num2++;
                }
                if (num2 < _size)
                {
                    _items[index++] = _items[num2++];
                }
            }
            Array.Clear(_items, index, _size - index);
            int num3 = _size - index;
            _size = index;
            _version++;

            foreach (int idx in removedItems.Keys)
                OnRemovedItem(removedItems[idx], idx);

            return num3;
        }

        public virtual void RemoveRange(int index, int count)
        {
            if ((index < 0) || (count < 0))
            {
                throw new ArgumentOutOfRangeException((index < 0) ? "Index cannot be lower than zero" : "Index out of range!");
            }
            if ((_size - index) < count)
            {
                throw new ArgumentException("Invalid Offlength!");
            }
            if (count > 0)
            {
                Dictionary<int, T> removedItems = new Dictionary<int, T>();

                int lastIdx = count + index;

                for (int i = index; i < lastIdx; i++)
                {
                    OnRemovingItem(_items[i], i);
                    removedItems.Add(i, _items[i]);
                }

                //for (int i = index; i < count; i++)
                //{

                //}

                _size -= count;
                if (index < _size)
                {
                    Array.Copy(_items, index + count, _items, index, _size - index);
                }
                Array.Clear(_items, _size, count);
                _version++;

                foreach (int idx in removedItems.Keys)
                    OnRemovedItem(removedItems[idx], idx);
            }
        }

        public void Reverse()
        {
            Reverse(0, Count);
        }

        public void Reverse(int index, int count)
        {
            if ((index < 0) || (count < 0))
            {
                throw new ArgumentOutOfRangeException((index < 0) ? "Index cannot be lower than zero" : "Index out of range!");
            }
            if ((_size - index) < count)
            {
                throw new ArgumentException("Invalid Offlength!");
            }
            Array.Reverse(_items, index, count);
            _version++;
        }

        public void Sort()
        {
            Sort(0, Count, null);
        }

        public void Sort(IComparer<T> comparer)
        {
            Sort(0, Count, comparer);
        }

        /*public void Sort(Comparison<T> comparison)
        {
            if (comparison == null)
            {
                throw new ArgumentNullException("comparison");
            }
            if (this._size > 0)
            {
                IComparer<T> comparer = new Array.FunctorComparer<T>(comparison);
                Array.Sort<T>(this._items, 0, this._size, comparer);
            }
        }*/

        public void Sort(int index, int count, IComparer<T> comparer)
        {
            if ((index < 0) || (count < 0))
            {
                throw new ArgumentOutOfRangeException((index < 0) ? "Index cannot be lower than zero" : "Index out of range!");
            }
            if ((_size - index) < count)
            {
                throw new ArgumentException("Invalid Offlength!");
            }
            Array.Sort<T>(_items, index, count, comparer);
            _version++;
        }

        public T[] ToArray()
        {
            T[] destinationArray = new T[_size];
            Array.Copy(_items, 0, destinationArray, 0, _size);
            return destinationArray;
        }

        public void TrimExcess()
        {
            int num = (int)(_items.Length * 0.9);
            if (_size < num)
            {
                Capacity = _size;
            }
        }

        public bool TrueForAll(Predicate<T> match)
        {
            if (match == null)
            {
                throw new ArgumentNullException("match");
            }
            for (int i = 0; i < _size; i++)
            {
                if (!match(_items[i]))
                {
                    return false;
                }
            }
            return true;
        }

        private static void VerifyValueType(object value)
        {
            if (!IsCompatibleObject(value))
            {
                throw new ArgumentException("Type is not compatible");
            }
        }

        #endregion

        #region ICustomList<T> Members

        /// <summary>
        /// Adds an item to the list.
        /// </summary>
        /// <param name="item">Item to add.</param>
        public virtual void Add(T item)
        {
            if (_size == _items.Length)
            {
                EnsureCapacity(_size + 1);
            }

            int idx = _size;

            OnAddingItem(item, idx);

            _items[_size++] = item;

            _version++;

            OnAddedItem(item, idx);
        }

        /// <summary>
        /// Adds an item to the list without raising any event.
        /// </summary>
        /// <param name="item">Item to add.</param>
        public virtual void AddIncognito(T item)
        {
            if (_size == _items.Length)
            {
                EnsureCapacity(_size + 1);
            }

            int idx = _size;

            _items[_size++] = item;

            _version++;
        }

        public virtual void Clear()
        {
            T[] items = new T[_size];

            // Copies existing items to a new array so we can use them even
            // after the original one is cleared
            for (int i = 0; i < _size; i++)
            {
                items[i] = _items[i];
            }

            // Raises Removing event
            for (int i = items.Length - 1; i >= 0; i--)
            {
                T item = items[i];

                OnRemovingItem(item, i);
            }

            Array.Clear(_items, 0, _size);

            // Raises Removed event
            for (int i = items.Length - 1; i >= 0; i--)
            {
                T item = items[i];

                OnRemovedItem(item, i);
            }

            _size = 0;
            _version++;
        }

        public virtual bool Contains(T item)
        {
            if (item == null)
            {
                for (int i = 0; i < _size; i++)
                {
                    if (_items[i] == null)
                    {
                        return true;
                    }
                }
                return false;
            }
            EqualityComparer<T> comparer = EqualityComparer<T>.Default;
            for (int j = 0; j < _size; j++)
            {
                if (comparer.Equals(_items[j], item))
                {
                    return true;
                }
            }
            return false;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            Array.Copy(_items, 0, array, arrayIndex, _size);
        }

        public int IndexOf(T item)
        {
            return Array.IndexOf<T>(_items, item, 0, _size);
        }

        public virtual void Insert(int index, T item)
        {
            if (index > _size)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            if (_size == _items.Length)
            {
                EnsureCapacity(_size + 1);
            }

            OnAddingItem(item, index);

            if (index < _size)
            {
                Array.Copy(_items, index, _items, index + 1, _size - index);
            }

            _items[index] = item;

            _size++;
            _version++;

            OnAddedItem(item, index);
        }

        public virtual bool Remove(T item)
        {
            int index = IndexOf(item);
            if (index >= 0)
            {
                RemoveAt(index);
                return true;
            }
            return false;
        }

        public void RemoveAt(int index)
        {
            if (index >= _size)
            {
                throw new ArgumentOutOfRangeException("index");
            }
            _size--;

            T itemToRemove = _items[index];

            OnRemovingItem(itemToRemove, index);

            if (index < _size)
            {
                Array.Copy(_items, index + 1, _items, index, _size - index);
            }
            _items[_size] = default(T);
            _version++;

            OnRemovedItem(itemToRemove, index);
        }

        protected virtual void OnAddingItem(T item, int idx)
        {
            if (AddingItem != null)
            {
                CustomListEventArgs<T> args = new CustomListEventArgs<T> { Item = item, Index = idx };

                AddingItem(args);
            }
        }

        protected virtual void OnAddedItem(T item, int idx)
        {
            if (AddedItem != null)
            {
                CustomListEventArgs<T> args = new CustomListEventArgs<T> { Item = item, Index = idx };

                AddedItem(args);
            }
        }

        protected virtual void OnRemovingItem(T item, int idx)
        {
            if (RemovingItem != null)
            {
                CustomListEventArgs<T> args = new CustomListEventArgs<T> { Item = item, Index = idx };

                RemovingItem(args);
            }
        }

        protected virtual void OnRemovedItem(T item, int idx)
        {
            if (RemovedItem != null)
            {
                CustomListEventArgs<T> args = new CustomListEventArgs<T> { Item = item, Index = idx };

                RemovedItem(args);
            }
        }

        protected virtual void OnReplacingItem(T replacingItem, T replacedItem, int idx)
        {
            if (ReplacingItem != null)
            {
                CustomListReplaceItemEventArgs<T> args = new CustomListReplaceItemEventArgs<T> { ReplacingItem = replacingItem, ReplacedItem = replacedItem, Index = idx };

                ReplacingItem(args);
            }
        }

        protected virtual void OnReplacedItem(T replacingItem, T replacedItem, int idx)
        {
            if (ReplacedItem != null)
            {
                CustomListReplaceItemEventArgs<T> args = new CustomListReplaceItemEventArgs<T> { ReplacingItem = replacingItem, ReplacedItem = replacedItem, Index = idx };

                ReplacedItem(args);
            }
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return new Enumerator<T>((CustomList<T>)this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new Enumerator<T>((CustomList<T>)this);
        }

        // Properties

        public int Count
        {
            get { return _size; }
        }

        public int Version
        {
            get { return _version; }
            set { _version = value; }
        }

        public virtual T this[int index]
        {
            get
            {
                if (index >= _size)
                {
                    throw new ArgumentOutOfRangeException("index");
                }
                return _items[index];
            }
            set
            {
                if (index >= _size)
                {
                    throw new ArgumentOutOfRangeException("index");
                }
                T oldItem = _items[index];
                this.OnReplacingItem(value, oldItem, index);
                _items[index] = value;
                _version++;
                this.OnReplacedItem(value, oldItem, index);
            }
        }

        bool ICollection<T>.IsReadOnly
        {
            get { return false; }
        }

        #endregion

        #region IList Members

        void ICollection.CopyTo(Array array, int arrayIndex)
        {
            if ((array != null) && (array.Rank != 1))
            {
                throw new ArgumentOutOfRangeException("Multidimensional array not supported!");
            }
            try
            {
                Array.Copy(_items, 0, array, arrayIndex, _size);
            }
            catch (ArrayTypeMismatchException)
            {
                throw new ArgumentException("Wrong array type!");
            }
        }

        int IList.Add(object item)
        {
            VerifyValueType(item);
            Add((T)item);
            return (Count - 1);
        }

        bool IList.Contains(object item)
        {
            if (IsCompatibleObject(item))
            {
                return Contains((T)item);
            }
            return false;
        }

        int IList.IndexOf(object item)
        {
            if (IsCompatibleObject(item))
            {
                return IndexOf((T)item);
            }
            return -1;
        }

        void IList.Insert(int index, object item)
        {
            VerifyValueType(item);
            Insert(index, (T)item);
        }

        void IList.Remove(object item)
        {
            if (IsCompatibleObject(item))
            {
                Remove((T)item);
            }
        }

        bool ICollection.IsSynchronized
        {
            get { return false; }
        }

        object ICollection.SyncRoot
        {
            get
            {
                if (_syncRoot == null)
                {
                    Interlocked.CompareExchange(ref _syncRoot, new object(), null);
                }
                return _syncRoot;
            }
        }

        bool IList.IsFixedSize
        {
            get { return false; }
        }

        bool IList.IsReadOnly
        {
            get { return false; }
        }

        object IList.this[int index]
        {
            get { return this[index]; }
            set
            {
                VerifyValueType(value);
                this[index] = (T)value;
            }
        }

        #endregion

        // Nested Types
        [Serializable]
        [StructLayout(LayoutKind.Sequential)]
        public struct Enumerator<EnumT> : IEnumerator<EnumT>, IDisposable, IEnumerator
        {
            #region Fields

            private ICustomList<EnumT> _customList;
            private EnumT current;
            private int index;
            private int version;

            #endregion

            #region Constructors

            public Enumerator(ICustomList<EnumT> customList)
            {
                _customList = customList;
                index = 0;
                version = customList.Version;
                current = default(EnumT);
            }

            #endregion

            #region IEnumerator<EnumT> Members

            public void Dispose() { }

            public bool MoveNext()
            {
                if (version != _customList.Version)
                {
                    throw new InvalidOperationException("Wrong enum version!");
                }
                if (index < _customList.Count)
                {
                    current = _customList[index];
                    index++;
                    return true;
                }
                index = _customList.Count + 1;
                current = default(EnumT);
                return false;
            }

            public EnumT Current
            {
                get { return current; }
            }

            object IEnumerator.Current
            {
                get
                {
                    if ((index == 0) || (index == (_customList.Count + 1)))
                    {
                        throw new InvalidOperationException("Cannot retrieve current!");
                    }
                    return Current;
                }
            }

            void IEnumerator.Reset()
            {
                if (version != _customList.Version)
                {
                    throw new InvalidOperationException("Wrong enum version!");
                }
                index = 0;
                current = default(EnumT);
            }

            #endregion
        }


        /// <summary>
        /// Replaces the current items with the supplied items.
        /// </summary>
        public void Replace(T[] items)
        {
            _items = items;

            if (_items != null)
                _size = _items.Length;
            else
                _size = 0;
        }
    }

    public interface ICustomList<T> : IList<T>
    {
        #region Properties

        int Version { get; set; }

        #endregion
    }
}
