﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Util.Collections
{
    public interface IPagedList
    {

        int? TotalCount
        {
            get;
            set;
        }

        int CurrentPage
        {
            get;
            set;
        }

        int PageSize
        {
            get;
            set;
        }

        bool HasPreviousPage
        {
            get;
        }

        bool HasNextPage
        {
            get;
        }
    }

    [Serializable]
    public class PagedList<T> : CustomList<T>, IEnumerable<T>, IPagedList
    {
        private int _startIndex;
        private int _endIndex;

        public PagedList()
        {
            CurrentPage = 0;
            PageSize = int.MaxValue;
        }

        public PagedList(int count)
            : base(count)
        {
        }

        public PagedList(IEnumerable<T> items)
            : base(items)
        {
        }

        public PagedList(IQueryable<T> source, int currentPage, int pageSize)
        {
            this.TotalCount = source.Count();
            this.PageSize = pageSize;
            this.CurrentPage = currentPage;
            this.AddRange(source.Skip(currentPage * pageSize).Take(pageSize).ToList());
        }

        public PagedList(List<T> source, int currentPage, int pageSize)
        {
            this.TotalCount = source.Count();
            this.PageSize = pageSize;
            this.CurrentPage = currentPage;
            this.AddRange(source);
            //this.AddRange(source.Skip(currentPage * pageSize).Take(pageSize).ToList());
        }

        public int StartIndex
        {
            get { return _startIndex; }
            set { _startIndex = value; }
        }

        public int EndIndex
        {
            get { return _endIndex; }
            set { _endIndex = value; }
        }

        public int? TotalCount
        {
            get;
            set;
        }

        public bool HasPreviousEntities
        {
            get { return _startIndex > 0; }
        }

        public int CurrentPage
        {
            get;
            set;
        }

        private int _pageSize = int.MaxValue;
        public int PageSize
        {
            get
            {
                return _pageSize;
            }
            set
            {
                _pageSize = value;
            }
        }

        public bool HasPreviousPage
        {
            get
            {
                return (CurrentPage > 0);
            }
        }

        public bool HasNextPage
        {
            get
            {
                return ((CurrentPage + 1) * PageSize) <= Count || HasMoreEntities;
            }
        }

        public Boolean NeedsMoreEntities()
        {
            if (HasMoreEntities)
            {
                return ((CurrentPage + 1) * PageSize) > Count;
            }
            return false;
        }

        public void SetCurrentPage(int page)
        {
            CurrentPage = page;
        }

        public new void Add(T item)
        {
            base.Add(item);
            if (TotalCount.HasValue)
            {
                TotalCount += 1;
            }
        }

        public new void AddRange(IEnumerable<T> collection)
        {
            base.AddRange(collection);
            if (TotalCount.HasValue && collection != null)
            {
                TotalCount += collection.Count();
            }
        }

        public new void Remove(T item)
        {
            if (base.Remove(item))
            {
                if (TotalCount.HasValue)
                {
                    TotalCount -= 1;
                }
            }
        }

        public new void RemoveAt(int index)
        {
            base.RemoveAt(index);
            if (TotalCount.HasValue)
            {
                TotalCount -= 1;
            }
        }

        public new void RemoveAll(Predicate<T> match)
        {
            base.RemoveAll(match);
            if (TotalCount.HasValue)
            {
                TotalCount = null;
            }
        }


    }

    //public static class Pagination
    //{
    //    public static PagedList<T> ToPagedList<T>(this IQueryable<T> source, int index, int pageSize)
    //    {
    //        return new PagedList<T>(source, index, pageSize);
    //    }

    //    public static PagedList<T> ToPagedList<T>(this IQueryable<T> source, int index)
    //    {
    //        return new PagedList<T>(source, index, 10);
    //    }

    //    public static PagedList<T> ToPagedList<T>(this List<T> source, int index, int pageSize)
    //    {
    //        return new PagedList<T>(source, index, pageSize);
    //    }

    //    public static PagedList<T> ToPagedList<T>(this List<T> source, int index)
    //    {
    //        return new PagedList<T>(source, index, 10);
    //    }
    //}

    //[Serializable]
    //public class InMemoryFilterInfoDictionary<T> : Dictionary<IComparable, PagedList<T>>
    //{
    //    public bool HasMoreEntities { get; set; }
    //}


}
