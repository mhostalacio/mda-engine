﻿using SendGrid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Library.Util.Mail
{
    public class SMTPMailProvider : MailProviderBase, IMailProvider
    {
        public SMTPMailProvider()
        {
        }

        IMailDeliveryStatus IMailProvider.SendMessage(IMailMessage message)
        {
            IMailDeliveryStatus result = new MailDeliveryStatus();

            if (Boolean.Parse(System.Configuration.ConfigurationManager.AppSettings["SendMails"]))
            {
                try
                {
                    // Create the email object first, then add the properties.
                    MailMessage mail = new MailMessage();

                    // Add the message properties.
                    //if (!String.IsNullOrEmpty(message.From.Key))
                    //{
                    //    mail.From = new MailAddress(message.From.Key, message.From.Value);
                    //}
                    //else
                    //{
                        mail.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["EmailFrom"], System.Configuration.ConfigurationManager.AppSettings["EmailFromName"]);
                    //}

                    foreach (String to in message.To.Keys)
                    {
                        mail.To.Add(to);
                    }

                    if (message.CC != null)
                    {
                        foreach (String cc in message.CC.Keys)
                        {
                            mail.CC.Add(cc);
                        }
                    }

                    if (message.BCC != null)
                    {
                        foreach (String bcc in message.BCC.Keys)
                        {
                            mail.Bcc.Add(bcc);
                        }
                    }

                    mail.Subject = message.Subject;

                    mail.Body = message.Body;

                    mail.IsBodyHtml = true;


                    //Create the SMTP client
                    SmtpClient client = new SmtpClient();
                    client.Send(mail);
                }

                catch (AggregateException ae)
                {
                    ae.Handle((x) =>
                    {
                        result.Errors.Add(string.Format("{0} [{1}]{2}", ae.Message, ae.StackTrace));
                        return true;
                    });

                }
                catch (Exception ex)
                {
                    result.Errors.Add(string.Format("{0} [{1}]", ex.Message, ex.StackTrace));
                }
                finally
                {
                }
            }

            return result;
        }
    }
}
