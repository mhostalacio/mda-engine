﻿using Library.Util.AOP;
using Library.Util.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Util.Mail
{
    public static class MailManager
    {
        public static IMailDeliveryStatus SendEmail(IMailMessage message)
        {
            return ServiceLocator.Instance.GetService<IMailProvider>().SendMessage(message);
            
        }
    }
}
