﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Util.Mail
{
    public interface IMailMessage
    {
        String Subject { get; set; }

        String Body { get; set; }

        KeyValuePair<String, String> From { get; set; }

        Dictionary<String, String> To { get; set; }

        Dictionary<String, String> CC { get; set; }

        Dictionary<String, String> BCC { get; set; }

        DateTime? SendingDateTime { get; set; }
    }
}
