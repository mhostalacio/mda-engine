﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Util.Mail
{
    public class MailMessageBase : IMailMessage
    {
        public string Subject
        {
            get;
            set;
        }

        public string Body
        {
            get;
            set;
        }

        public KeyValuePair<string, string> From
        {
            get;
            set;
        }

        public Dictionary<string, string> To
        {
            get;
            set;
        }

        public Dictionary<string, string> CC
        {
            get;
            set;
        }

        public Dictionary<string, string> BCC
        {
            get;
            set;
        }

        public DateTime? SendingDateTime
        {
            get;
            set;
        }
    }
}
