﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Util.Mail
{
    public interface IMailDeliveryStatus
    {
        List<string> Errors { get; set; }

        bool Success { get; }
    }
}
