﻿using Exceptions;
using SendGrid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Library.Util.Mail
{
    public class SendGridMailProvider : MailProviderBase, IMailProvider
    {
        public SendGridMailProvider()
        {
        }

        IMailDeliveryStatus IMailProvider.SendMessage(IMailMessage message)
        {
            IMailDeliveryStatus result = new MailDeliveryStatus();

            if (Boolean.Parse(System.Configuration.ConfigurationManager.AppSettings["SendMails"]))
            {
                try
                {

                    // Create the email object first, then add the properties.
                    SendGridMessage mail = new SendGridMessage();

                    // Add the message properties.
                    if (!String.IsNullOrEmpty(message.From.Key))
                    {
                        mail.From = new MailAddress(message.From.Key, message.From.Value);
                    }
                    else
                    {
                        mail.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["EmailFrom"], System.Configuration.ConfigurationManager.AppSettings["EmailFromName"]);
                    }

                    foreach (String to in message.To.Keys)
                    {
                        mail.AddTo(to);
                    }

                    if (message.CC != null)
                    {
                        foreach (String cc in message.CC.Keys)
                        {
                            mail.AddCc(cc);
                        }
                    }

                    if (message.BCC != null)
                    {
                        foreach (String bcc in message.BCC.Keys)
                        {
                            mail.AddBcc(bcc);
                        }
                    }

                    mail.Subject = message.Subject;

                    //myMessage.EnableClickTracking(true);

                    mail.Html = message.Body;


                    //if (files != null && files.Count > 0)
                    //{

                    //    foreach (CertifiQMembers.Entities.Util.File file in files)
                    //    {
                    //        byte[] fileBytes = FileStorageManager.RetrieveGeneratedDocument(file.Hash, file.StorageKey);

                    //        MemoryStream memoryStream = new MemoryStream(fileBytes);
                    //        openedStreams.Add(memoryStream);
                    //        mail.AddAttachment(memoryStream, file.Name);
                    //    }
                    //}

                    String username = System.Configuration.ConfigurationManager.AppSettings["SENDGRID_USER"];

                    String pswd = System.Configuration.ConfigurationManager.AppSettings["SENDGRID_PASS"];

                    // Create credentials, specifying your user name and password.
                    NetworkCredential credentials = new NetworkCredential(username, pswd);

                    // Create an Web transport for sending email.
                    Web transportWeb = new Web(credentials);

                    // Send the email.
                    transportWeb.DeliverAsync(mail).Start();//.Wait();
                }
                catch (AggregateException ae)
                {
                    ae.Handle((x) =>
                    {
                        if (x is InvalidApiRequestException)
                        {
                            InvalidApiRequestException apiError = (InvalidApiRequestException)x;

                            var detail = new StringBuilder();

                            detail.Append("ResponseStatusCode: " + apiError.ResponseStatusCode + ".   ");

                            for (int i = 0; i < apiError.Errors.Count(); i++)
                            {
                                detail.Append(" -- Error #" + i.ToString() + " : " + apiError.Errors[i]);
                            }

                            result.Errors.Add(string.Format("{0} [{1}]", detail.ToString(), apiError.StackTrace));
                        }
                        else
                        {
                            result.Errors.Add(string.Format("{0} [{1}]{2}", ae.Message, ae.StackTrace));
                        }
                        return true;
                    });
                }
                catch (Exception ex)
                {
                    result.Errors.Add(string.Format("{0} [{1}]", ex.Message, ex.StackTrace));
                }
                finally
                {
                }
            }

            return result;
        }
    }
}
