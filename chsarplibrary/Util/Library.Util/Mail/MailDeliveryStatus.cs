﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Util.Mail
{
    public class MailDeliveryStatus : IMailDeliveryStatus
    {
        public MailDeliveryStatus()
        {
            this.Errors = new List<string>();
        }

        public bool Success
        {
            get
            {
                return this.Errors.Count == 0;
            }
        }

        public List<string> Errors { get; set; }
    }
}
