﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Util.Mail
{
    public interface IMailProvider
    {
        IMailDeliveryStatus SendMessage(IMailMessage message);
    }
}
