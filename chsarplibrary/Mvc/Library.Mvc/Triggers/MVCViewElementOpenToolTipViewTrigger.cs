﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;
using System.Web.Mvc;
using System.Web.UI;

namespace Library.Mvc.Triggers
{
    public class MVCViewElementOpenToolTipViewTrigger : MVCViewActionBaseTrigger
    {
        private HttpVerbs _method = HttpVerbs.Get;

        /// <summary>
        /// Indicates witch http verb to use while calling the action. Default is HttpVerbs.Get.
        /// </summary>
        public HttpVerbs Method
        {
            get { return _method; }
            set { _method = value; }
        }

        public ToolTipHorizontalPosition HorizontalPosition { get; set; }

        public ToolTipVerticalPosition VerticalPosition { get; set; }

        protected override void RenderInternal(MVCViewElement elem, HtmlTextWriter writer, string eventName)
        {
            string modelKey = Controller.ModelKey;


            if (UseAjax)
            {
                switch (Method)
                {
                    case HttpVerbs.Get:
                        TriggerHelper.WriteAjaxGetCall(writer, modelKey, AreaName, ControllerName, ActionName, BlockForm, Arguments);
                        break;
                    case HttpVerbs.Post:
                        TriggerHelper.WriteAjaxPostCall(writer, modelKey, AreaName, ControllerName, ActionName, BlockForm, Arguments);
                        break;
                    default:
                        throw new NotSupportedException(String.Format("The HttpVerb \"{0}\" its not supported!", Method.ToString()));
                }
            }
            else
            {
                writer.Write("window.location.href = ");
                TriggerHelper.WriteUrl(writer, modelKey, AreaName, ControllerName, ActionName, Arguments);
            }
        }


    }

    public enum ToolTipHorizontalPosition
    {
        Right,
        Left,  
    }
    public enum ToolTipVerticalPosition
    {
        Bottom,
        Top,  
    }
}
