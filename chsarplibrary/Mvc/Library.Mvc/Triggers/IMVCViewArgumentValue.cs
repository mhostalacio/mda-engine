﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web.Mvc;

namespace Library.Mvc.Triggers
{
    public interface IMVCViewArgumentValue
    {
        void WriteValue(MVCViewArgument arg, TextWriter writer, bool writeArgumentName, bool isClientSide, HttpVerbs method);

        string GetUrlArgumentValue(string argName, out bool wrappValue);

        string GetWritableValue();
    }
}
