﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;
using System.Web.UI;

namespace Library.Mvc.Triggers
{
    public class MVCViewElementOpenPopupViewTrigger : MVCViewActionBaseTrigger
    {

        private MVCViewArgumentCollection _callbackArguments = null;

        private Boolean _wrapWithScriptTags = false;

        public String CallBackUrl { get; set; }

        public String CompleteUrl { get; set; }

        public String PopupWindowName { get; set; }

        public int? PopupWidth { get; set; }

        public int? PopupHeight { get; set; }

        public bool? ShowScrollBars { get; set; }

        public bool? PopupResizable { get; set; }

        public bool WrapWithScriptTags
        {
            get { return _wrapWithScriptTags; }
            set { _wrapWithScriptTags = value; }
        }

        public MVCViewArgumentCollection CallBackArguments
        {
            get { return _callbackArguments; }
            set { _callbackArguments = value; }
        }

        protected override void RenderInternal(MVCViewElement elem, HtmlTextWriter writer, string eventName)
        {
            if (WrapWithScriptTags)
            {
                writer.Write("<script language=\"javascript\">");
            }
            if (Arguments == null)
            {
                Arguments = new MVCViewArgumentCollection();
            }
            if (Arguments.Find(o => o.Name.Equals("pmkey")) == null)
            {
                Arguments.Add(new MVCViewArgument() { Name = "pmkey", Value = new MVCViewLiteralArgumentValue() { Value = this.Controller.ModelKey } });
            }
            writer.Write("openPopup(");
            if (String.IsNullOrEmpty(CompleteUrl))
            {
                TriggerHelper.WriteUrl(writer, null, AreaName, ControllerName, ActionName, Arguments, true, true, CallBackUrl, null, CallBackArguments);
            }
            else
            {
                writer.Write("'{0}'", CompleteUrl);
            }
            writer.Write(", '{0}'", PopupHeight.HasValue ? PopupHeight.Value.ToString() : "");
            writer.Write(", '{0}'", PopupWidth.HasValue ? PopupWidth.Value.ToString() : "");
            writer.Write(", {0}", ShowScrollBars.HasValue ? ShowScrollBars.Value.ToString() : "false");
            writer.Write(", {0}", PopupResizable.HasValue ? PopupResizable.Value.ToString() : "true");
            writer.Write(", this");//writer.Write(", '{0}'", PopupWindowName);
            writer.Write("); ");

            if (WrapWithScriptTags)
            {
                writer.Write("</script>");
            }
        }
    }
}
