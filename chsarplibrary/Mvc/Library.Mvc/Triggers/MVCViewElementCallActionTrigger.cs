﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Library.Mvc.WebControls.Base;
using System.Web.UI;

namespace Library.Mvc.Triggers
{
    public partial class MVCViewElementCallActionTrigger : MVCViewActionBaseTrigger
    {
        private HttpVerbs _method = HttpVerbs.Get;

        private Boolean _includeSpecifiedModelKey = false;

        /// <summary>
        /// Indicates witch http verb to use while calling the action. Default is HttpVerbs.Get.
        /// </summary>
        public HttpVerbs Method
        {
            get { return _method; }
            set { _method = value; }
        }

        private Boolean _includeModelKey = true;

        public String AjaxWaitActions { get; set; }

        

        public Boolean IncludeModelKey
        {
            get { return _includeModelKey; }
            set { _includeModelKey = value; }
        }

        public Boolean IncludeSpecifiedModelKey
        {
            get { return _includeSpecifiedModelKey; }
            set { _includeSpecifiedModelKey = value; }
        }

 

        protected override void RenderInternal(MVCViewElement elem, HtmlTextWriter writer, string eventName)
        {
            string modelKey = Controller.ModelKey;

            
            if (UseAjax)
            {
                switch (Method)
                {
                    case HttpVerbs.Get:
                        TriggerHelper.WriteAjaxGetCall(writer, modelKey, AreaName, ControllerName, ActionName, BlockForm, Arguments);
                        break;
                    case HttpVerbs.Post:
                        TriggerHelper.WriteAjaxPostCall(writer, modelKey, AreaName, ControllerName, ActionName, BlockForm, Arguments);
                        break;
                    default:
                        throw new NotSupportedException(String.Format("The HttpVerb \"{0}\" its not supported!", Method.ToString()));
                }
            }
            else
            {
                writer.Write("window.location.href = ");
                TriggerHelper.WriteUrl(writer, modelKey, AreaName, ControllerName, ActionName, Arguments);
            }
        }
    }
}
