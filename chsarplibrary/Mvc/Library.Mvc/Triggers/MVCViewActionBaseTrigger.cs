﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Mvc.Triggers
{
    public abstract class MVCViewActionBaseTrigger : MVCViewElementTrigger
    {
        #region Fields

        private MVCViewArgumentCollection _arguments = null;

        #endregion

        #region Properties

        public String ControllerName { get; set; }

        public String ActionName { get; set; }

        public String AreaName { get; set; }

        public Boolean UseAjax { get; set; }

        public Boolean BlockForm { get; set; }

        public String ModelKey { get; set; }

        public MVCViewArgumentCollection Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        #endregion
    }
}
