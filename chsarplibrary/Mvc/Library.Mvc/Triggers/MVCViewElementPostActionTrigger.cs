﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;
using System.Web.UI;
using Library.Util;

namespace Library.Mvc.Triggers
{
    public class MVCViewElementPostActionTrigger : MVCViewElementTrigger
    {
        private String _areaName;
        private String _controllerName;
        private String _actionName;
        private String _commandArguments;
        private MVCViewArgumentCollection _queryStringArguments;
        private MVCViewArgumentCollection _arguments = null;
        /// <summary>
        /// Post back action name.
        /// </summary>
        public String AreaName
        {
            get { return _areaName; }
            set { _areaName = value; }
        }

        /// <summary>
        /// Post back action name.
        /// </summary>
        public String ControllerName
        {
            get { return _controllerName; }
            set { _controllerName = value; }
        }

        /// <summary>
        /// Post back action name.
        /// </summary>
        public String ActionName
        {
            get { return _actionName; }
            set { _actionName = value; }
        }

        public Boolean UseAjax { get; set; }

        public Boolean BlockForm { get; set; }

        /// <summary>
        /// Post back arguments.
        /// </summary>
        public String CommandArguments
        {
            get
            {
                return _commandArguments;
            }
            set
            {
                _commandArguments = value;
            }
        }
        public MVCViewArgumentCollection QueryStringArguments
        {
            get { return _queryStringArguments; }
            set { _queryStringArguments = value; }
        }
        public MVCViewArgumentCollection Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }



        protected override void RenderInternal(MVCViewElement elem, HtmlTextWriter writer, string eventName)
        {
            // Overrides the element to use as post
            //if (elem.PostElement != null)
            //    elem = elem.PostElement;

            string idToUse = elem.ClientId;

            if (UseAjax)
            {
                writer.Write(String.Format("postForm('{0}', '{1}', '{2}', ", idToUse, ActionName, (CommandArguments != null) ? CommandArguments : String.Empty));

                if (QueryStringArguments != null)
                    TriggerHelper.WriteUrl(writer, elem.Controller.ModelKey, AreaName, ControllerName, ActionName, QueryStringArguments);
                else
                    TriggerHelper.WriteUrl(writer, elem.Controller.ModelKey, AreaName, ControllerName, ActionName);

                if (Arguments != null && Arguments.Count > 0)
                {
                    TriggerHelper.WriteAjaxPostCallArguments(writer, Arguments);
                }
                else
                {
                    writer.Write(",null");
                }

                if (BlockForm)
                {
                    writer.Write(",true);");
                }
                else
                {
                    writer.Write(",false);");
                }
            }
            else
            {
                writer.Write("if (window.tinyMCE){tinyMCE.triggerSave();}");
                writer.Write(String.Format("$('#{0}').parents('form:first').children('input[name={1}]:first').val('{2}');", idToUse, CommonSettings.POST_ACTION_NAME_HIDDEN_FIELD_NAME, ActionName));

                if (!String.IsNullOrEmpty(CommandArguments))
                {
                    writer.Write(String.Format("$('#{0}').parents('form:first').children('input[name={1}]:first').val('{2}');", idToUse, CommonSettings.POST_ARGUMENTS_HIDDEN_FIELD_NAME, CommandArguments));
                }

                writer.Write(String.Format("$('#{0}').parents('form:first').attr('action', ", idToUse));

                if (QueryStringArguments != null)
                    TriggerHelper.WriteUrl(writer, elem.Controller.ModelKey, AreaName, ControllerName, ActionName, QueryStringArguments);
                else
                    TriggerHelper.WriteUrl(writer, elem.Controller.ModelKey, AreaName, ControllerName, ActionName);


                writer.Write(");");
                writer.Write(String.Format("$('#{0}').parents('form:first').submit();", idToUse));
            }
        }
    }
}
