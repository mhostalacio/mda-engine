﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;
using System.Web.Mvc;

namespace Library.Mvc.Triggers
{
    public class MVCViewElementJavascriptTrigger : MVCViewElementTrigger
    {
        private MVCViewArgumentCollection _arguments = null;

        public bool WrapWithScriptTags { get; set; }

        public String ActionName { get; set; }

        public MVCViewArgumentCollection Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public Boolean BlockForm { get; set; }

        public String Javascript { get; set; }

        protected override void RenderInternal(MVCViewElement elem, System.Web.UI.HtmlTextWriter writer, string eventName)
        {
            if (WrapWithScriptTags)
                writer.Write("<script>");

            if (!String.IsNullOrEmpty(Javascript))
            {
                writer.Write(Javascript);
                writer.Write(";");
            }
            else if (!String.IsNullOrEmpty(ActionName))
            {
                writer.Write(ActionName);
                writer.Write("(this");
                if (Arguments != null && Arguments.Count > 0)
                {
                    foreach (MVCViewArgument arg in Arguments)
                    {
                        writer.Write(",");
                        arg.AvoidEncode = true;
                        arg.Value.WriteValue(arg, writer, false, true, HttpVerbs.Post);
                    }
                }
                writer.Write(");");
            }

            if (WrapWithScriptTags)
                writer.Write("</script>");
        }
    }
}
