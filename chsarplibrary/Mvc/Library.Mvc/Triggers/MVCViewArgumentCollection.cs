﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Util.Collections;

namespace Library.Mvc.Triggers
{
    public class MVCViewArgumentCollection : CustomList<MVCViewArgument>
    {
        public MVCViewArgumentCollection()
        {
        }

        public MVCViewArgumentCollection(IEnumerable<MVCViewArgument> list)
            : base(list)
        {
        }
    }
}
