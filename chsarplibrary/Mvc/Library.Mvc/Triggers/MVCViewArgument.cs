﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Mvc.Triggers
{
    public class MVCViewArgument
    {
        public string Name { get; set; }

        public IMVCViewArgumentValue Value { get; set; }

        public bool AvoidEncode { get; set; }
    }
}
