﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;
using System.Web.UI;
using Library.Mvc.Controllers;

namespace Library.Mvc.Triggers
{
    public abstract class MVCViewElementTrigger
    {
        public virtual MVCControllerBase Controller { get; set; }

        public String ConfirmationMessage { get; set; }

        public MVCViewElementTrigger ConfirmationCancelTrigger { get; set; }

        public void Render(MVCViewElement elem, HtmlTextWriter writer, string eventName)
        {
            bool hasConfirmation = !String.IsNullOrEmpty(ConfirmationMessage);

            if (hasConfirmation)
            {
                writer.Write("if (confirm('{0}'))", ConfirmationMessage);
                writer.Write("{");
            }

            RenderInternal(elem, writer, eventName);

            if (hasConfirmation)
            {
                writer.Write("}");

                if (ConfirmationCancelTrigger != null)
                {
                    writer.Write(" else {");
                    ConfirmationCancelTrigger.Render(elem, writer, eventName);
                    writer.Write("}");
                }
            }
        }

        protected abstract void RenderInternal(MVCViewElement elem, HtmlTextWriter writer, string eventName);
    }
}
