﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;
using System.Web.UI;

namespace Library.Mvc.Triggers
{
    public class MVCViewElementRedirectTrigger : MVCViewActionBaseTrigger
    {

        protected override void RenderInternal(MVCViewElement elem, HtmlTextWriter writer, string eventName)
        {
            string modelKey = Controller.ModelKey;

            if (UseAjax)
            {
                TriggerHelper.WriteAjaxGetCall(writer, modelKey, AreaName, ControllerName, ActionName, BlockForm, Arguments);
            }
            else
            {
                writer.Write("window.location.href = ");
                TriggerHelper.WriteUrl(writer, null, AreaName, ControllerName, ActionName, Arguments);
            }
        }
    }
}
