﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.IO;
using Library.Util.Transactions;

namespace Library.Mvc.Triggers
{
    public class MVCViewLiteralArgumentValue : IMVCViewArgumentValue
    {
        public MVCViewLiteralArgumentValue()
        {
        }

        public MVCViewLiteralArgumentValue(Object value)
        {
            Value = value;
        }

        public Object Value { get; set; }

        public Boolean AvoidCommas { get; set; }

        #region IMVCViewElementActionArgumentValue Members

        public void WriteValue(MVCViewArgument arg, TextWriter writer, bool writeArgumentName, bool isClientSide, HttpVerbs method)
        {
            if (writeArgumentName)
            {
                TriggerHelper.WriteArgumentName(writer, arg, method, isClientSide);
            }

            if (isClientSide)
            {
                isClientSide = !AvoidCommas;
            }

            TriggerHelper.WriteArgumentValue(writer, arg, GetWritableValue(), isClientSide);
        }

        public string GetWritableValue()
        {
            return Convert.ToString(Value, BusinessTransaction.CurrentContext.CurrentCulture);
        }

        public string GetUrlArgumentValue(string argName, out bool wrappValue)
        {
            wrappValue = true;

            return Convert.ToString(Value, BusinessTransaction.CurrentContext.CurrentCulture);
        }

        #endregion
    }
}
