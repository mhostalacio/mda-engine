﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web.Mvc;
using Library.Util.Text;
using System.Web;
using Library.Mvc.Controllers;
using Library.Util;

namespace Library.Mvc.Triggers
{
    public static class TriggerHelper
    {
        private static readonly string QUESTION_MARK = "?";
        private static readonly string QUESTION_MARK_ENCODED = HttpUtility.UrlEncode(QUESTION_MARK);
        private static readonly string AND = "&";
        private static readonly string AND_ENCODED = HttpUtility.UrlEncode(AND);
        public const string MESSAGES_QUERY_STRING_KEY = "Messages";
        public const string CALLBACK_URL_QUERY_STRING_KEY = "CallBackUrl";
        public const string POPUP_VIEW_FLAG_QUERY_STRING_KEY = "asPopupView";


        public static void WriteAjaxGetCall(TextWriter writer, string modelKey, string areaName, string controllerName, string actionName, MVCViewArgumentCollection args)
        {
            writer.Write("getAction(");

            WriteUrl(writer, modelKey, areaName, controllerName, actionName, args);

            writer.Write(");");
        }

        public static void WriteAjaxGetCall(TextWriter writer, string modelKey, string areaName, string controllerName, string actionName, bool blockForm, MVCViewArgumentCollection args)
        {
            writer.Write("getAction(");

            WriteUrl(writer, modelKey, areaName, controllerName, actionName, args);

            if (blockForm)
            {
                writer.Write(",true);");
            }
            else
            {
                writer.Write(",false);");
            }
        }

        public static void WriteAjaxPostCall(TextWriter writer, string modelKey, string areaName, string controllerName, string actionName, bool blockForm, MVCViewArgumentCollection args)
        {
            writer.Write("postAction(");

            WriteUrl(writer, modelKey, areaName, controllerName, actionName);

            WriteAjaxPostCallArguments(writer, args);

            if (blockForm)
            {
                writer.Write(",true);");
            }
            else
            {
                writer.Write(",false);");
            }
        }

        public static void WriteUrl(TextWriter writer, string modelKey, string areaName, string controllerName, string actionName, bool clientSide = true)
        {
            if (clientSide)
                writer.Write("'");

            WriteApplicationPah(writer);

            WriteFirstPart(writer, areaName, controllerName, actionName);

            WriteModelKey(writer, modelKey, "?");

            if (clientSide)
                writer.Write("'");
        }

        public static void WriteUrl(TextWriter writer, string modelKey, string areaName, string controllerName, string actionName, MVCViewArgumentCollection args, bool isClientSide = true,  bool isPopupView = false, string callbackUrl = null, string applicationName = null, MVCViewArgumentCollection callBackArguments = null)
        {
            if (isClientSide)
                writer.Write("'");

            WriteApplicationPah(writer, applicationName);

            WriteFirstPart(writer, areaName, controllerName, actionName);

            if (isClientSide)
                writer.Write("'");

            WriteArguments(writer, modelKey, args, isClientSide, isPopupView, callbackUrl, HttpVerbs.Get, callBackArguments);
        }

        public static void WriteUrl(TextWriter writer, string modelKey, string areaName, string controllerName, string actionName, Dictionary<string, object> args, bool isClientSide = true, bool encodeUrl = false, bool encodeArgs = true)
        {
            if (isClientSide)
                writer.Write("'");

            WriteApplicationPah(writer);

            WriteFirstPart(writer, areaName, controllerName, actionName);

            WriteArguments(writer, modelKey, args, encodeUrl, encodeArgs);

            if (isClientSide)
                writer.Write("'");
        }

        public static void WriteUrl(TextWriter writer, string modelKey, string url, Dictionary<string, object> args, bool isClientSide = true, bool encodeUrl = false, bool encodeArgs = true)
        {
            if (isClientSide)
                writer.Write("'");

            WriteApplicationPah(writer);

            writer.Write("{0}", url);

            WriteArguments(writer, modelKey, args, encodeUrl, encodeArgs);

            if (isClientSide)
                writer.Write("'");
        }

        public static String CreateUrl(string modelKey, string areaName, string controllerName, string actionName, MVCViewArgumentCollection args = null, bool clientSide = true)
        {
            String url = null;

            using (StringWriter str = new StringWriter())
            {
                WriteUrl(str, modelKey, areaName, controllerName, actionName, args, clientSide);

                url = str.ToString();
            }

            return url;
        }

        public static void WriteAjaxPostCallArguments(TextWriter writer, MVCViewArgumentCollection args)
        {
            if (args != null && args.Count > 0)
            {
                writer.Write(", { ");

                bool isFirst = true;

                foreach (MVCViewArgument arg in args)
                {
                    if (isFirst)
                    {
                        isFirst = false;
                    }
                    else
                    {
                        writer.Write(", ");
                    }

                    arg.Value.WriteValue(arg, writer, true, true, HttpVerbs.Post);
                }

                writer.Write(" }");
            }
        }

        private static void WriteApplicationPah(TextWriter writer, string applicationName = null)
        {
            if (HttpContext.Current != null && HttpContext.Current.Request.ApplicationPath != "/")
            {
                writer.Write(HttpContext.Current.Request.ApplicationPath);
            }

            writer.Write("/");
        }

        private static void WriteFirstPart(TextWriter writer, string areaName, string controllerName, string actionName)
        {
            if (!String.IsNullOrEmpty(areaName))
                writer.Write("{0}/", areaName);

            if (!String.IsNullOrEmpty(controllerName))
                writer.Write("{0}/", controllerName);

            if (!String.IsNullOrEmpty(actionName))
                writer.Write("{0}", actionName);
        }

        public static void WriteArguments(TextWriter writer, string modelKey, MVCViewArgumentCollection args, bool isClientSide, bool isPopupView, string callbackUrl, HttpVerbs method, MVCViewArgumentCollection callBackArguments = null)
        {
            string prefix = QUESTION_MARK;

            if (args != null && args.Count > 0)
            {
                if (isClientSide)
                    writer.Write(" + ");

                bool first = true;

                for (int i = 0; i < args.Count; i++)
                {
                    if (isClientSide)
                        writer.Write("'{0}' + ", prefix);
                    else
                        writer.Write(prefix);

                    MVCViewArgument arg = args[i];

                    if (arg.Value != null)
                        arg.Value.WriteValue(arg, writer, true, isClientSide, method);

                    if (first)
                    {
                        first = false;
                        prefix = AND;
                    }

                    if (isClientSide)
                        writer.Write(" + ");
                }

                if (isClientSide)
                    writer.Write("'");

                WriteModelKey(writer, modelKey, prefix);

                WritePopupViewArgs(writer, isPopupView, callbackUrl, prefix, isClientSide, callBackArguments, method);

                

                if (isClientSide)
                    writer.Write("'");
            }
            else if (!String.IsNullOrEmpty(modelKey))
            {
                if (isClientSide)
                    writer.Write(" + '");

                WriteModelKey(writer, modelKey, prefix);

                prefix = AND;

                WritePopupViewArgs(writer, isPopupView, callbackUrl, prefix, isClientSide, callBackArguments, method);

                

                if (isClientSide)
                    writer.Write("'");
            }
            else
            {
                if (isPopupView)
                {
                    if (isClientSide)
                        writer.Write(" + '");

                    if (isPopupView)
                    {
                        WritePopupViewArgs(writer, isPopupView, callbackUrl, prefix, isClientSide, callBackArguments, method);

                        prefix = AND;
                    }

                    

                    if (isClientSide)
                        writer.Write("'");
                }
            }
        }

        private static void WriteArguments(TextWriter writer, string modelKey, Dictionary<string, object> args, bool encodeUrl, bool encodeArgs)
        {
            if (!String.IsNullOrEmpty(modelKey) || (args != null && args.Count > 0))
            {
                bool first = true;
                string prefix = (encodeUrl) ? QUESTION_MARK_ENCODED : QUESTION_MARK;

                if (args != null)
                {
                    foreach (KeyValuePair<string, object> arg in args)
                    {
                        writer.Write(prefix);

                        if (encodeUrl)
                            writer.Write(HttpUtility.UrlEncode(String.Format("{0}={1}", arg.Key, arg.Value)));
                        else
                            writer.Write("{0}={1}", arg.Key, encodeArgs ? HttpUtility.UrlEncode(arg.Value != null ? arg.Value.ToString() : string.Empty) : arg.Value);

                        if (first)
                        {
                            first = false;

                            if (encodeUrl)
                                prefix = AND_ENCODED;
                            else
                                prefix = AND;
                        }
                    }
                }

                if (!String.IsNullOrEmpty(modelKey))
                {
                    WriteModelKey(writer, modelKey, prefix);

                    if (encodeUrl)
                        prefix = AND_ENCODED;
                    else
                        prefix = AND;
                }

            }
        }

        public static void WriteArgumentName(TextWriter writer, MVCViewArgument arg, HttpVerbs method, bool isClientSide)
        {
            if (isClientSide)
            {
                switch (method)
                {
                    case HttpVerbs.Get:
                        writer.Write("'{0}=' + ", arg.Name);
                        break;
                    case HttpVerbs.Post:
                        writer.Write("{0}: ", arg.Name);
                        break;
                    default:
                        throw new NotSupportedException(method.ToString());
                }
            }
            else
            {
                switch (method)
                {
                    case HttpVerbs.Get:
                        writer.Write("{0}=", arg.Name);
                        break;
                    default:
                        throw new NotSupportedException(method.ToString());
                }
            }
        }

        public static void WriteArgumentValue(TextWriter writer, MVCViewArgument arg, String argValue, bool isClientSide)
        {
            string value = arg.AvoidEncode ? argValue : HttpUtility.UrlEncode(argValue);

            if (isClientSide)
            {
                writer.Write("'{0}'", value);
            }
            else
            {
                writer.Write(value);
            }
        }

        private static void WriteModelKey(TextWriter writer, string modelKey, string prefix)
        {
            if (!String.IsNullOrEmpty(modelKey))
                writer.Write("{0}{1}={2}", prefix, CommonSettings.MODEL_KEY_HIDDEN_FIELD_NAME, modelKey);
        }

        private static void WritePopupViewArgs(TextWriter writer, bool isPopupView, string callbackUrl, string prefix, bool isClientSide = false, MVCViewArgumentCollection callBackArguments = null, HttpVerbs method = HttpVerbs.Get)
        {
            if (isPopupView)
            {
                writer.Write("{0}{1}={2}", prefix, POPUP_VIEW_FLAG_QUERY_STRING_KEY, "true");
                if (!String.IsNullOrEmpty(callbackUrl))
                    writer.Write("&{0}={1}", CALLBACK_URL_QUERY_STRING_KEY, callbackUrl);
            }
            if (callBackArguments != null && callBackArguments.Count > 0)
            {
                if (isClientSide)
                    writer.Write("'");
                prefix = AND;
                foreach (var arg in callBackArguments)
                {
                    if (isClientSide)
                        writer.Write(" + encodeURIComponent('{0}') + ", prefix);
                    if (arg.Value != null)
                        arg.Value.WriteValue(arg, writer, true, isClientSide, method);

                }
                if (isClientSide)
                    writer.Write(" +'");
            }
        }

        
    }
}
