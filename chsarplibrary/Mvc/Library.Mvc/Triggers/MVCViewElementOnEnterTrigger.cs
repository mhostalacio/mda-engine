﻿using Library.Mvc.WebControls.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace Library.Mvc.Triggers
{
    public class MVCViewElementOnEnterTrigger : MVCViewElementTrigger
    {
        #region Fields

        private List<MVCViewElementTrigger> handlers;

        #endregion

        #region Properties

        public List<MVCViewElementTrigger> Handlers
        {
            get 
            {
                if (this.handlers == null)
                {
                    this.handlers = new List<MVCViewElementTrigger>();
                }

                return this.handlers;
            }
        }

        #endregion

        #region Methods

        protected override void RenderInternal(MVCViewElement elem, HtmlTextWriter writer, string eventName)
        {
            writer.Write("if (event.keyCode == 13) {");

            if (this.handlers != null && this.handlers.Count > 0)
            {
                foreach (MVCViewElementTrigger handler in this.handlers)
                {
                    handler.Render(elem, writer, eventName);
                }
            }

            writer.Write("}");
        }

        #endregion
    }
}
