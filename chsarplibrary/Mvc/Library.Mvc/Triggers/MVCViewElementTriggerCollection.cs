﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Util.Collections;

namespace Library.Mvc.Triggers
{
    [Serializable]
    public class MVCViewElementTriggerCollection : CustomList<MVCViewElementTrigger>
    {
    }
}
