﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace Library.Mvc.Triggers
{
    public class MVCViewElementOpenTabViewTrigger : MVCViewActionBaseTrigger
    {

        public String TabID { get; set; }


        protected override void RenderInternal(WebControls.Base.MVCViewElement elem, System.Web.UI.HtmlTextWriter writer, string eventName)
        {
            writer.Write("#" + TabID);
        }
    }
}
