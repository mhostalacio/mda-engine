﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Util.Transactions;

namespace Library.Mvc.Triggers
{
    public class MVCViewModelArgumentValue<T> : IMVCViewArgumentValue
    {
        public void WriteValue(MVCViewArgument arg, System.IO.TextWriter writer, bool writeArgumentName, bool isClientSide, System.Web.Mvc.HttpVerbs method)
        {
            if (writeArgumentName)
            {
                TriggerHelper.WriteArgumentName(writer, arg, method, isClientSide);
            }

            TriggerHelper.WriteArgumentValue(writer, arg, GetWritableValue(), isClientSide);
        }

        public string GetWritableValue()
        {
            T val = Value();
            return Convert.ToString(val, BusinessTransaction.CurrentContext.CurrentCulture);
        }

        public string GetUrlArgumentValue(string argName, out bool wrappValue)
        {
            wrappValue = true;

            return Convert.ToString(Value, BusinessTransaction.CurrentContext.CurrentCulture);
        }

        public Boolean AddEncodeUri { get; set; }

        public Func<T> Value { get; set; }

    }
}
