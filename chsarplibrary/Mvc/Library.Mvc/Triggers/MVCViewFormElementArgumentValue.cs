﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web.Mvc;
using Library.Mvc.WebControls.Base;
using Library.Mvc.WebControls.Controls;

namespace Library.Mvc.Triggers
{
    public class MVCViewFormElementArgumentValue : IMVCViewArgumentValue
    {
        #region Fields

        private Boolean _addEncodeUri = false;
        private String _elementClientId;
        private String _elemGetFormValueJQueryMethod;

        #endregion

        #region Properties

        public Func<MVCViewElement> Element { get; set; }

        public Boolean AddEncodeUri
        {
            get { return _addEncodeUri; }
            set { _addEncodeUri = value; }
        }

        #endregion

        #region Constructor

        public MVCViewFormElementArgumentValue()
        {
        }

        public MVCViewFormElementArgumentValue(String elementClientId, String elemGetFormValueJQueryMethod)
        {
            _elementClientId = elementClientId;
            _elemGetFormValueJQueryMethod = elemGetFormValueJQueryMethod;
        }

        #endregion

        #region IMVCViewArgumentValue Members

        private string GetFormValueAction()
        {
            MVCViewElement elem = Element != null ? Element() : null;

            string formValueId = elem != null ? elem.ClientId : _elementClientId;
            //TODO: change this
            if (elem is IMVCViewCheckBox)
            {
                formValueId = ((IMVCViewCheckBox)elem).OriginalClientId;
            }
            string formValueJQueryMethod = elem != null ? elem.GetFormValueJQueryMethod() : _elemGetFormValueJQueryMethod;

            StringBuilder formValue = new StringBuilder();

            formValue.AppendFormat("$.{0}(", _addEncodeUri ? "getURIVal" : "getVal");
            formValue.AppendFormat("'{0}'", formValueId);

            if (!String.IsNullOrEmpty(formValueJQueryMethod))
                formValue.AppendFormat(",'{0}'", formValueJQueryMethod);

            formValue.Append(")");

            return formValue.ToString();
        }

        public void WriteValue(MVCViewArgument arg, TextWriter writer, bool writeArgumentName, bool isClientSide, HttpVerbs method)
        {
            if (isClientSide)
            {
                if (writeArgumentName)
                    TriggerHelper.WriteArgumentName(writer, arg, method, isClientSide);

                writer.Write(GetFormValueAction());
            }
            else
                throw new NotSupportedException();
        }

        public string GetUrlArgumentValue(string argName, out bool wrappValue)
        {
            wrappValue = true;

            return GetFormValueAction();
        }

        public string GetWritableValue()
        {
            return GetFormValueAction();
        }

        #endregion
    }
}
