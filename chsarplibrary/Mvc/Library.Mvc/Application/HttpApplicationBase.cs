﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;
using Library.Util.Transactions;
using Library.Mvc.Views;
using System.Globalization;
using Library.Util.AOP;
using Library.Mvc.Models;
using Library.Util;
using Library.Util.Logging;

namespace Library.Mvc.Application
{
    public abstract class HttpApplicationBase : System.Web.HttpApplication
    {
        public virtual void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("favicon.ico");
        }

        protected virtual void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterRoutes(RouteTable.Routes);

            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new CustomWebFormViewEngine());
            ServiceLocator.GetService<IModelStorage>().OnApplicationStart();

        }

        protected virtual void Application_End()
        {
            ServiceLocator.GetService<IModelStorage>().OnApplicationEnd();
            if (CommonSettings.PersistLogs)
            {
                ServiceLocator.GetService<ILogPersister>().SaveAndClearLogs();
            }
        }

        protected virtual void Application_PreRequestHandlerExecute(object sender, EventArgs e)
        {
            if (Context.Handler is IRequiresSessionState || Context.Handler is IReadOnlySessionState)
            {
                BusinessTransaction userCtx = (BusinessTransaction)Session[BusinessTransaction.USER_CONTEXT_SESSION_KEY];

                if (userCtx == null)
                {
                    userCtx = BusinessTransaction.InitializeContext(Session.SessionID);
                    Session[BusinessTransaction.USER_CONTEXT_SESSION_KEY] = userCtx;
                }
                else
                {
                    BusinessTransaction.SetCurrentContext(userCtx);
                }
            }
        }

        protected virtual void Session_Start(object sender, EventArgs e)
        {
            if (BusinessTransaction.CurrentContext == null || BusinessTransaction.CurrentContext.GetSessionId() != Session.SessionID)
            {
                BusinessTransaction ctx = BusinessTransaction.InitializeContext(Session.SessionID);
                Session[BusinessTransaction.USER_CONTEXT_SESSION_KEY] = ctx;
            }
            else if (BusinessTransaction.CurrentContext != null && Session[BusinessTransaction.USER_CONTEXT_SESSION_KEY] == null)
            {
                Session[BusinessTransaction.USER_CONTEXT_SESSION_KEY] = BusinessTransaction.CurrentContext;
            }
            BusinessTransaction userCtx = ((BusinessTransaction)Session[BusinessTransaction.USER_CONTEXT_SESSION_KEY]);
            ServiceLocator.GetService<IModelStorage>().OnSessionStart(userCtx.GetSessionId(), Session.SessionID);
            //TODO: move from here when real sign in
            ServiceLocator.GetService<IModelStorage>().OnSignIn(userCtx.GetSessionId(), Session.SessionID, userCtx.SessionInfo.Name, userCtx.SessionInfo.UserCode);
        }

        protected virtual void Session_End(object sender, EventArgs e)
        {
            BusinessTransaction userCtx = ((BusinessTransaction)Session[BusinessTransaction.USER_CONTEXT_SESSION_KEY]);
            if (userCtx != null)
            {
                ServiceLocator.GetService<IModelStorage>().OnSessionEnd(userCtx.GetSessionId(), Session.SessionID);
                //TODO: move from here when real sign out
                ServiceLocator.GetService<IModelStorage>().OnSignOut(userCtx.GetSessionId(), Session.SessionID);
            }
        }
        
        protected virtual void Application_BeginRequest(object sender, EventArgs e)
        {
            ServiceLocator.GetService<IModelStorage>().OnRequestStart();
        }

        protected virtual void Application_EndRequest(object sender, EventArgs e)
        {
            ServiceLocator.GetService<IModelStorage>().OnRequestEnd();
            if (BusinessTransaction.CurrentContext != null)
                BusinessTransaction.CurrentContext.Dispose();
        }

        protected void RegisterArea(RouteCollection routes, String areaName, String controllerName = null, String namespaceStr = null)
        {
            if (controllerName == null && namespaceStr == null)
            {
                Route route = routes.MapRoute(
                    Guid.NewGuid().ToString(), // Route name
                    areaName + "/{controller}/{action}/{args}", //URL with parameters
                    new { controller = "Home", action = "Default", args = UrlParameter.Optional }
                    );
                route.DataTokens["area"] = areaName;
            }
            else if (controllerName != null && namespaceStr != null)
            {
                Route route = routes.MapRoute(
                    Guid.NewGuid().ToString(), // Route name
                    areaName + "/{controller}/{action}/{args}", // URL with parameters
                    new { controller = controllerName, action = "Default", args = UrlParameter.Optional },
                    new[] { namespaceStr }
                );
                route.DataTokens["area"] = areaName;
            }
            else if (controllerName == null && namespaceStr != null)
            {
                Route route = routes.MapRoute(
                    Guid.NewGuid().ToString(), // Route name
                    areaName + "/{controller}/{action}/{args}", //URL with parameters
                    new { controller = "Home", action = "Default", args = UrlParameter.Optional },
                    new[] { namespaceStr }
                    );
                route.DataTokens["area"] = areaName;
            }
        }

        protected void RegisterDefaultPage(RouteCollection routes, String areaName, String controllerName, String actionName, String namespaceStr)
        {
            Route route = routes.MapRoute(
                "Home_Default",// Route name
                "",// URL with parameters
                new { controller = controllerName, action = actionName },
                new[] { namespaceStr }
               );
            route.DataTokens["area"] = areaName;
        }

    }
}
