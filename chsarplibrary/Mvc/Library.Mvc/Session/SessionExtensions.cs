﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Library.Util;
using Library.Util.Transactions;

namespace Library.Mvc.Session
{
    public static class SessionExtensions
    {

        public static string GetSessionId(this HttpSessionStateBase session, HttpResponseBase response, bool abandonIfUnableTOGetSessionId, bool throwIfUnableTOGetSessionId)
        {
            return session.SessionID;
        }

        public static string GetLoginGUID(this HttpSessionStateBase session, HttpResponseBase response, bool abandonIfUnableTOGetSessionId, bool throwIfUnableTOGetSessionId)
        {
            if (session[BusinessTransaction.USER_CONTEXT_SESSION_KEY] != null)
                return ((BusinessTransaction)session[BusinessTransaction.USER_CONTEXT_SESSION_KEY]).GetSessionId();
            return null;
        }

        
    }
}
