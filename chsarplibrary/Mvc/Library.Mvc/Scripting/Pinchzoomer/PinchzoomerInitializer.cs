﻿using Library.Mvc.Scripting.Base;
using Library.Mvc.WebControls.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace Library.Mvc.Scripting.Pinchzoomer
{
    public class PinchzoomerInitializer : JSScriptBlock
    {
        #region Properties

        public string Selector;

        #endregion

        #region Methods

        protected override void RenderInternal(HtmlTextWriter writer)
        {
            writer.Write("$('{0}').pinchzoomer({{", this.Selector);
            writer.Write("imageOptions:{ scaleMode:'full' }");
            writer.Write("});");
        }

        #endregion

        #region Static Methods

        public static void Initialize(MVCViewElement element)
        {
            PinchzoomerInitializer initializer;

            initializer = new PinchzoomerInitializer();
            initializer.Selector = "#" + element.ClientId;

            element.Controller.AddScriptBlock(initializer);
        }

        #endregion
    }
}
