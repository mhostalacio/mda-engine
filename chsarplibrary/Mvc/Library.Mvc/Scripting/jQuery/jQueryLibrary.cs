﻿using Library.Mvc.Controllers;
using Library.Mvc.Scripting.Javascript;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Mvc.Scripting.jQuery
{
    public static class jQueryLibrary
    {
        #region Static Methods

        public static void ShowElements(string selector, MVCControllerBase ctrl)
        {
            CallMethod actionCall;

            actionCall = new CallMethod();
            actionCall.Selector = selector;
            actionCall.MethodName = "show";

            ctrl.AddScriptBlock(actionCall);
        }

        public static void HideElements(string selector, MVCControllerBase ctrl)
        {
            CallMethod actionCall;

            actionCall = new CallMethod();
            actionCall.Selector = selector;
            actionCall.MethodName = "hide";

            ctrl.AddScriptBlock(actionCall);
        }

        #endregion
    }
}
