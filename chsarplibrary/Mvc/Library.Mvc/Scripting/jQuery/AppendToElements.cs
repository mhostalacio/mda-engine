﻿using Library.Mvc.Scripting.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace Library.Mvc.Scripting.jQuery
{
    public class AppendToElements : JSScriptBlock
    {
        #region Fields

        public string AppendToSelector;

        public string SourceSelector;

        #endregion

        #region Methods

        protected override void RenderInternal(HtmlTextWriter writer)
        {
            writer.Write("$('{0}').append($('{1}'));", this.AppendToSelector, this.SourceSelector);
        }

        #endregion
    }
}
