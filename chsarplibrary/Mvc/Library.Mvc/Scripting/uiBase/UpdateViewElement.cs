﻿using Library.Mvc.Scripting.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace Library.Mvc.Scripting.uiBase
{
    public class UpdateViewElement : JSScriptBlock
    {
        #region Properties

        public string ViewElementSelector;
        public string NewContentSelector;

        #endregion

        #region Methods

        protected override void RenderInternal(HtmlTextWriter writer)
        {
            writer.Write("$.updateElement('{0}','{1}');", this.ViewElementSelector, this.NewContentSelector);
        }

        #endregion
    }
}
