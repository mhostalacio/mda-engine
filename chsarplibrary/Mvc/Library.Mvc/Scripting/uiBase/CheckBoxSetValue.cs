﻿using Library.Mvc.Scripting.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace Library.Mvc.Scripting.uiBase
{
    public class CheckBoxSetValue : JSScriptBlock
    {
        #region Properties

        public string CheckboxClientId;
        public string CheckboxOriginalClientId;

        #endregion

        #region Methods

        protected override void RenderInternal(HtmlTextWriter writer)
        {
            writer.Write("$('#{0}').on('change', function(event)", this.CheckboxClientId);
            writer.Write("{");
            writer.Write("$(this).val(event.target.checked);");
            writer.Write("setCheckValue(event, '{0}');", this.CheckboxOriginalClientId);
            writer.Write("});");
        }

        #endregion
    }
}
