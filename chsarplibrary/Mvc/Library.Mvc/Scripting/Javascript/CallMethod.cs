﻿using Library.Mvc.Scripting.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace Library.Mvc.Scripting.Javascript
{
    public class CallMethod : JSScriptBlock
    {
        #region Properties

        public string Selector { get; set; }
        public string MethodName { get; set; }
        public string Arguments { get; set; }
        #endregion

        #region Methods

        protected override void RenderInternal(HtmlTextWriter writer)
        {
            writer.Write("$('{0}').{1}(", this.Selector, this.MethodName);
            // todo : refactor arguments definition & writing
            writer.Write(this.Arguments);
            writer.Write(");");
        }

        #endregion
    }
}
