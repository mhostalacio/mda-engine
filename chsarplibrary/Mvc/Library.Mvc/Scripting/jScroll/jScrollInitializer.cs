﻿using Library.Mvc.Scripting.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace Library.Mvc.Scripting.jScroll
{
    public class jScrollInitializer : JSScriptBlock
    {
        #region Properties

        public string Selector;
        public string NextPageSelector;
        public string LoadingHtml;
        public string CallbackFunc;

        #endregion

        #region Methods

        protected override void RenderInternal(HtmlTextWriter writer)
        {
            writer.Write("$('{0}').jscroll({{", this.Selector);
            if (!string.IsNullOrWhiteSpace(this.CallbackFunc))
            {
                writer.Write("callback: {0},",this.CallbackFunc);
            }
            writer.Write("loadingHtml: '{0}',", this.LoadingHtml);
            writer.Write("nextSelector: '{0}'", this.NextPageSelector);
            writer.Write("});");
        }

        #endregion
    }
}
