﻿using Library.Mvc.WebControls.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace Library.Mvc.Scripting.Base
{
    public abstract class JSScriptBlock : IMVCRenderElement
    {
        #region Methods

        public void Render(HtmlTextWriter writer)
        {
            writer.Write(JSScriptElement.JS_OPEN_TAG);

            this.RenderInternal(writer);

            writer.Write(JSScriptElement.JS_END_TAG);
        }

        public void RenderBlock(HtmlTextWriter writer)
        {
            this.RenderInternal(writer);
        }

        protected abstract void RenderInternal(HtmlTextWriter writer);

        #endregion
    }
}
