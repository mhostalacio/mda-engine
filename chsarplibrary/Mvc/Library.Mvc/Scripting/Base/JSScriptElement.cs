﻿using Library.Mvc.WebControls.Base;
using Library.Util.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI;

namespace Library.Mvc.Scripting.Base
{
    public class JSScriptElement : CustomList<JSScriptBlock>, IMVCRenderElement
    {
        #region Constants

        public const string JS_OPEN_TAG = "<script Type=\"text/javascript\">";
        public const string JS_END_TAG = "</script>";

        #endregion

        #region Methods

        public void Render(HtmlTextWriter writer)
        {
            if (Count > 0)
            {
                writer.WriteLine(JS_OPEN_TAG);

                foreach (JSScriptBlock item in this)
                {
                    item.RenderBlock(writer);
                }

                writer.WriteLine(JS_END_TAG);
            }
        }

        public void Render(HtmlHelper helper)
        {
            using (HtmlTextWriter writer = new HtmlTextWriter(helper.ViewContext.HttpContext.Response.Output))
            {
                Render(writer);
            }
        }

        #endregion
    }
}
