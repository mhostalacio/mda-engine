﻿using Library.Mvc.Scripting.Base;
using Library.Mvc.WebControls.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace Library.Mvc.Scripting.MagnificPopup
{
    public enum CssOverflow
    {
        Auto,
        Hidden,
        Visible,
        Scroll
    }

    public class MagnificPopupInitializer : JSScriptBlock
    {
        #region Properties

        public string Selector;
        public CssOverflow OverflowY = CssOverflow.Scroll;

        #endregion

        #region Methods

        protected override void RenderInternal(HtmlTextWriter writer)
        {
            writer.Write("$.initMagnificPopup('{0}', ", this.Selector);
            writer.Write("{");
            writer.Write("removalDelay: 300,");
            writer.Write("mainClass: 'mfp-fade',");
            writer.Write("overflowY: '{0}',", this.OverflowY.ToString().ToLower());
            //writer.Write("fixedContentPos: false,");
            writer.Write("type: 'iframe'");
            writer.Write("});");
        }

        #endregion

        #region Static Methods

        private static MagnificPopupInitializer InitializeScript(MVCViewElement element)
        {
            MagnificPopupInitializer initializer;

            initializer = new MagnificPopupInitializer();
            initializer.Selector = "#" + element.ClientId;

            return initializer;
        }

        public static void Initialize(MVCViewElement element)
        {
            MagnificPopupInitializer initializer = InitializeScript(element);

            element.Controller.AddScriptBlock(initializer);
        }

        public static void Initialize(MVCViewElement element, CssOverflow overflowY)
        {
            MagnificPopupInitializer initializer;

            initializer = InitializeScript(element);
            initializer.OverflowY = overflowY;

            element.Controller.AddScriptBlock(initializer);
        }

        #endregion
    }
}
