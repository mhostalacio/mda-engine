﻿using Library.Mvc.Scripting.Base;
using Library.Mvc.WebControls.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace Library.Mvc.Scripting.Bootstrap.Typeahead
{
    public class TypeaheadAjaxInitializer : JSScriptBlock
    {
        #region Properties

        public string Selector;
        public MVCUrl ActionUrl;

        public string onSelectItemMethod;
        public string itemTemplateMethod;
        public string menuTemplate;
        public string displayField;
        public string valueField;
        public string preDispatchMethod;
        public string preProcessMethod;

        #endregion

        #region Methods

        protected override void RenderInternal(HtmlTextWriter writer)
        {
            writer.Write("if(typeof {0} == 'function'){{$('{1}').typeahead({{", this.itemTemplateMethod, this.Selector);
            writer.Write("item: {0},", this.itemTemplateMethod);
            writer.Write("menu: '{0}',", this.menuTemplate);
            writer.Write("onSelect: {0},", this.onSelectItemMethod);
            writer.Write("ajax: {");
            writer.Write("url:");
            this.ActionUrl.ClientSide = true;
            this.ActionUrl.Render(writer);
            writer.Write(",");
            writer.Write("timeout: 300,");
            writer.Write("method: 'get',");
            writer.Write("displayField: '{0}',", this.displayField);
            writer.Write("valueField: '{0}',", this.valueField);
            writer.Write("preDispatch: {0},", this.preDispatchMethod);
            writer.Write("preProcess: {0}", this.preProcessMethod);
            writer.Write("}");
            writer.Write("})};");
        }
        
        #endregion
    }
}
