﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Runtime.Serialization.Json;

namespace Library.Mvc.WebControls
{
    public static class Helper
    {
        public static string Nbsp = "&nbsp;";

        public static String NormalizeValue(String value, RenderValueMode mode = RenderValueMode.None)
        {
            String encodedValue;

            if (mode == RenderValueMode.HtmlDecode)
            {
                encodedValue = HttpUtility.HtmlDecode(value);
            }
            else if (mode == RenderValueMode.HtmlEncode)
            {
                encodedValue = HttpUtility.HtmlEncode(value);
            }
            else
            {
                encodedValue = value;
            }

            return encodedValue;
        }

        public static string JsonSerializer<T>(T t)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream();
            ser.WriteObject(ms, t);
            string jsonString = Encoding.UTF8.GetString(ms.ToArray());
            ms.Close();
            return jsonString;
        }

        public static string JsonSerializer<T>(T[] t) 
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T[]));
            MemoryStream ms = new MemoryStream();
            ser.WriteObject(ms, t);
            string jsonString = Encoding.UTF8.GetString(ms.ToArray());
            ms.Close();
            return jsonString;
        }

        public static T JsonDeserialize<T>(string jsonString)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)ser.ReadObject(ms);
            return obj;
        }

        /// <summary>
        /// Encodes a string to be represented as a string literal. The format
        /// is essentially a JSON string.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string EncodeJsString(string s)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("");
            foreach (char c in s)
            {
                switch (c)
                {
                    case '\"':
                        sb.Append("\\\"");
                        break;
                    case '\\':
                        sb.Append("\\\\");
                        break;
                    case '\b':
                        sb.Append("\\b");
                        break;
                    case '\f':
                        sb.Append("\\f");
                        break;
                    case '\n':
                        sb.Append("\\n");
                        break;
                    case '\r':
                        sb.Append("\\r");
                        break;
                    case '\t':
                        sb.Append("\\t");
                        break;
                    default:
                        int i = (int)c;
                        if (i < 32 || i > 127)
                        {
                            sb.AppendFormat("\\u{0:X04}", i);
                        }
                        else
                        {
                            sb.Append(c);
                        }
                        break;
                }
            }
            sb.Append("");

            return sb.ToString();
        }
    }

    public enum RenderValueMode
    {
        None,
        HtmlEncode,
        HtmlDecode,
    }

    public class TreeNode
    {
        public String id { get; set; }
        public String parent { get; set; }
        public String text { get; set; }
        public TreeNodeState state { get; set; }
        public Boolean children { get; set; }
 
    }

    public class TreeNodeState
    {
        public bool opened { get; set; }
        public bool selected { get; set; }
        public bool disabled { get; set; }
    }



}
