﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace Library.Mvc.WebControls.Base
{
    public abstract class MVCViewTextBoxBase<T> : MVCViewInputElement<T> 
    {
        #region Fields

        private Boolean _allowHtmlContent;
        private Boolean? _autocomplete = false;
        
        private T _value;

        #endregion

        #region Properties

        /// <summary>
        /// Indicates if this text box has autocomplete feature
        /// </summary>
        public Boolean Autocomplete
        {
            get { return _autocomplete.HasValue ? _autocomplete.Value : false; }
            set
            {
                _autocomplete = value;
            }
        }

        public virtual int? MaxLength
        {
            get;
            set;
        }

        public Boolean AllowHtmlContent
        {
            get { return _allowHtmlContent; }
            set { _allowHtmlContent = value; }
        }

        public virtual T Value
        {
            get
            {
                if (Bind != null)
                {
                    _value = Bind(false, default(T));
                }
                return _value;
            }
            set
            {
                _value = value;
            }

        }

        #endregion

        #region Methods

        public override void WriteAttributes(HtmlTextWriter writer)
        {
            base.WriteAttributes(writer);

            WriteAutocompleteAttribute(writer);
        }

        protected override void WriteHelpTextAttribute(HtmlTextWriter writer)
        {
            if (HelpText != null && ViewMode != MVCViewElementViewMode.Print)
            {
                WriteAttribute(HtmlTextWriterAttribute.Title, HelpText, writer);
            }
        }

        /// <summary>
        /// Writes the encode uri component attribute
        /// </summary>
        /// <param name="writer">Instance of the HtmlTextWriter</param>
        protected void WriteAllowHtmlContentAttribute(HtmlTextWriter writer)
        {
            if (AllowHtmlContent && IsEnabled && ViewMode != MVCViewElementViewMode.Print)
            {
                WriteAttribute("htmlCtnt", "1", writer);
            }
        }

        /// <summary>
        /// Adds the autocomplete attribute
        /// </summary>
        /// <param name="writer">Instance of the HtmlTextWriter</param>
        public virtual void WriteAutocompleteAttribute(HtmlTextWriter writer)
        {
            if (!Autocomplete)
            {
                WriteAttribute(HtmlTextWriterAttribute.AutoComplete, "off", writer);
            }
        }

        

        #endregion
    }

 
    
}
