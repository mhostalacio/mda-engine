﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Util.Collections;

namespace Library.Mvc.WebControls.Base
{
    [Serializable]
    public class MVCViewElementCollection : CustomList<MVCViewElement>
    {

    }

    [Serializable]
    public class MVCViewElementCollection<T> : CustomList<T> where T : MVCViewElement, IMVCViewElement
    {

    }
}
