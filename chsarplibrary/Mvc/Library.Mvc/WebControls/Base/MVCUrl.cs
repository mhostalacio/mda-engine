﻿using Library.Mvc.Triggers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace Library.Mvc.WebControls.Base
{
    public class MVCUrl
    {
        #region Properties

        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string AreaName { get; set; }
        public bool ClientSide;
        public MVCViewArgumentCollection Arguments { get; set; }

        #endregion

        #region Methods

        public void Render(HtmlTextWriter writer)
        {
            TriggerHelper.WriteUrl(writer, null, this.AreaName, this.ControllerName, this.ActionName, this.Arguments, this.ClientSide);
        }

        #endregion
    }
}
