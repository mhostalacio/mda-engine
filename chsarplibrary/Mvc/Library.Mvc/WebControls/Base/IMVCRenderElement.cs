﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace Library.Mvc.WebControls.Base
{
    public interface IMVCRenderElement
    {
        void Render(HtmlTextWriter writer);
    }
}
