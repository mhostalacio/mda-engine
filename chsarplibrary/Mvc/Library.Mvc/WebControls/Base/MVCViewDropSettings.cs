﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.Triggers;

namespace Library.Mvc.WebControls.Base
{
    public class MVCViewDropSettings
    {
        public String AcceptElementsSelector { get; set; }

        private MVCViewElementTriggerCollection _onItemDropped;
        public MVCViewElementTriggerCollection OnItemDropped
        {
            get
            {
                if (_onItemDropped == null)
                {
                    _onItemDropped = new MVCViewElementTriggerCollection();
                }

                return _onItemDropped;
            }
        }
    }
}
