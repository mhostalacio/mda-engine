﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace Library.Mvc.WebControls.Base
{
    public abstract class MVCViewInputElement<T> : MVCViewElement<T>
    {
        #region Fields

        private String _name = null;
        private Func<Boolean, Boolean, Boolean> _isRequired;
        private Func<Boolean, Boolean, Boolean> _isEnabledValue;
        private Func<Boolean, Boolean, Boolean> _isReadonlyValue;

        #endregion

        #region Properties

        /// <summary>
        /// Name of the element
        /// </summary>
        public String Name
        {
            get
            {
                return (!String.IsNullOrEmpty(_name) ? _name : ClientId);
            }
            set
            {
                _name = value;
            }
        }

        /// <summary>
        /// Indicates if the element is enabled.
        /// </summary>
        public virtual Func<Boolean, Boolean, Boolean> IsEnabledValue
        {
            get
            {
                return _isEnabledValue;
            }
            set
            {
                _isEnabledValue = value;
            }
        }

        private Boolean _isEnabled = true;
        public virtual Boolean IsEnabled
        {
            get
            {
                if (IsEnabledValue != null)
                {
                    _isEnabled = IsEnabledValue(false, false);
                }
                return _isEnabled;
            }
            set
            {
                _isEnabled = value;
            }
        }

        /// <summary>
        /// Indicates if the element is readonly.
        /// </summary>
        public virtual Func<Boolean, Boolean, Boolean> IsReadOnlyValue
        {
            get
            {
                return _isReadonlyValue;
            }
            set
            {
                _isReadonlyValue = value;
            }
        }

        public virtual Boolean IsReadOnly
        {
            get
            {
                return IsReadOnlyValue != null ? IsReadOnlyValue(false, false) : false;
            }
        }

        public virtual Func<Boolean, Boolean, Boolean> IsRequired
        {
            get
            {
                return _isRequired;
            }
            set
            {
                _isRequired = value;
            }
        }

        protected override string TagName
        {
            get { return "input"; }
        }

        public virtual String PlaceHolder { get; set; }

        #endregion

        protected override bool HasChildren
        {
            get
            {
                return false;
            }
        }

        public override void WriteAttributes(HtmlTextWriter writer)
        {
            base.WriteAttributes(writer);

            if (!IsEnabled)
                WriteAttribute(HtmlTextWriterAttribute.Disabled, "disabled", writer);

            if (IsReadOnly)
                WriteAttribute(HtmlTextWriterAttribute.ReadOnly, "readonly", writer);

            if (!String.IsNullOrEmpty(PlaceHolder))
            {
                WriteAttribute("placeholder", PlaceHolder, writer);
            }

            WriteNameAttribute(writer);
            
        }

        protected virtual void WriteNameAttribute(HtmlTextWriter writer)
        {
            WriteAttribute(HtmlTextWriterAttribute.Name, ClientId, writer);
        }

        protected override void RenderInternal(System.Web.UI.HtmlTextWriter writer)
        {
 
            base.RenderInternal(writer);

            if (IsEnabled && IsRequired != null && IsRequired(false, false))
            {
                writer.Write(" <font css=\"RequiredMark\">&nbsp;</font>");   
            }

            AddFieldMapping();
        }

        protected virtual void AddFieldMapping()
        {
            if (IsEnabled && !IsReadOnly && !AvoidValueBinding)
                Controller.AddFieldMapping(this.ClientId, Bind);
        }
    }
}
