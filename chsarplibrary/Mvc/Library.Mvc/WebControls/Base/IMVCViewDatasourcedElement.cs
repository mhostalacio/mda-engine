﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Mvc.WebControls.Base
{
    public interface IMVCViewDatasourcedElement : IMVCViewElement
    {
        int CurrentIndex { get; set; }

        Object CurrentBind { get; set; }
    }
}
