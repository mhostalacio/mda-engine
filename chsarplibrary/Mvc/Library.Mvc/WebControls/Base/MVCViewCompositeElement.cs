﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Mvc.WebControls.Base
{
    public abstract class MVCViewCompositeElement<T> : MVCViewElement
        where T : IMVCViewElement
    {
        protected override bool HasChildren
        {
            get
            {
                return ChildElements != null && ChildElements.Count > 0;
            }
        }

        private List<T> _childElements = new List<T>();
        public List<T> ChildElements 
        {
            get
            {
                return _childElements;
            }
        }

        protected override void WriteChildren(System.Web.UI.HtmlTextWriter writer)
        {
            base.WriteChildren(writer);
            if (ChildElements != null)
            {
                foreach (T element in ChildElements)
                {
                    element.Render(writer);
                }
            }
        }

        public void Add(T element)
        {
            if (element != null)
            {
                this._childElements.Add(element);
            }
        }
    }
}
