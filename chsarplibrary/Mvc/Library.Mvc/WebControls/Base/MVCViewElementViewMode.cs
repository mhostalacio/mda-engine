﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Mvc.WebControls.Base
{
    public enum MVCViewElementViewMode
    {
        Edit,
        Print,
        View
    }
}
