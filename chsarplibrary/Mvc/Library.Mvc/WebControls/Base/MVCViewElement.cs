﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.Triggers;
using Library.Mvc.Controllers;
using System.Web.UI;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using System.IO;

namespace Library.Mvc.WebControls.Base
{
    public abstract class MVCViewElement : IMVCViewElement
    {
        #region Constants

        internal const string SPAN_TAG = "span";

        #endregion

        #region Fields

        private Dictionary<string, string> _attributes = new Dictionary<string, string>();
        private String _clientId;
        private String _helpText;
        private String _id;
        private bool _isVisible = true;
        private MVCViewElementTriggerCollection _onClick;
        private MVCViewElementTriggerCollection _onKeyPress;
        private MVCViewElementTriggerCollection _onKeyUp;
        private MVCViewElementTriggerCollection _onBlur;
        private MVCViewElementTriggerCollection _onFocus;
        private MVCViewElementTriggerCollection _onOutFocus;
        private MVCViewElementTriggerCollection _onKeyDown;
        private MVCViewElementTriggerCollection _onChange;
        private MVCViewElementTriggerCollection _onMouseOver;
        private MVCViewElementViewMode _viewMode;
        private RenderValueMode _renderMode = RenderValueMode.None;

        private Dictionary<object, object> securityTokens;

        #endregion

        #region Properties

        public Dictionary<string, string> Attributes
        {
            get { return _attributes; }
        }

        public Boolean AvoidValueBinding { get; set; }

        public virtual string ClientId
        {
            get { return _clientId; }
            set { _clientId = value; }
        }

        public virtual MVCControllerBase Controller { get; set; }

        public virtual String CssClass { get; set; }

        public virtual String IconCssClass { get; set; }

        public virtual Boolean AlignIconToRight { get; set; }

        public virtual IMVCViewDatasourcedElement ContextElement { get; set; }

        public virtual Boolean Draggable { get; set; }

        public virtual Boolean Droppable { get; set; }

        public virtual MVCViewDropSettings DropSettings { get; set; }

        public virtual String HelpText
        {
            get
            {
                if (HelpTextValue != null)
                {
                    _helpText = HelpTextValue(false, null);
                }
                return _helpText;
            }
            set
            {
                _helpText = value;
            }
        }

        public String BindObjectCode { get; set; }

        public virtual Func<Boolean, String, String> HelpTextValue { get; set; }

        public virtual Unit Height { get; set; }

        public virtual String Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public bool IsVisible
        {
            get
            {
                return _isVisible;
            }
            set
            {
                _isVisible = value;
            }
        }

        public MVCViewElementTriggerCollection OnClick
        {
            get
            {
                if (_onClick == null)
                {
                    _onClick = new MVCViewElementTriggerCollection();
                }

                return _onClick;
            }
        }

        public MVCViewElementTriggerCollection OnKeyPress
        {
            get
            {
                if (_onKeyPress == null)
                {
                    _onKeyPress = new MVCViewElementTriggerCollection();
                }

                return _onKeyPress;
            }
        }

        public MVCViewElementTriggerCollection OnKeyUp
        {
            get
            {
                if (_onKeyUp == null)
                {
                    _onKeyUp = new MVCViewElementTriggerCollection();
                }

                return _onKeyUp;
            }
        }

        public MVCViewElementTriggerCollection OnBlur
        {
            get
            {
                if (_onBlur == null)
                {
                    _onBlur = new MVCViewElementTriggerCollection();
                }

                return _onBlur;
            }
        }

        public MVCViewElementTriggerCollection OnFocus
        {
            get
            {
                if (_onFocus == null)
                {
                    _onFocus = new MVCViewElementTriggerCollection();
                }

                return _onFocus;
            }
        }

        public MVCViewElementTriggerCollection OnOutFocus
        {
            get
            {
                if (_onOutFocus == null)
                {
                    _onOutFocus = new MVCViewElementTriggerCollection();
                }

                return _onOutFocus;
            }
        }

        public MVCViewElementTriggerCollection OnKeyDown
        {
            get
            {
                if (_onKeyDown == null)
                {
                    _onKeyDown = new MVCViewElementTriggerCollection();
                }

                return _onKeyDown;
            }
        }

        public MVCViewElementTriggerCollection OnMouseOver
        {
            get
            {
                if (_onMouseOver == null)
                {
                    _onMouseOver = new MVCViewElementTriggerCollection();
                }

                return _onMouseOver;
            }
        }

        public MVCViewElementTriggerCollection OnChange
        {
            get
            {
                if (_onChange == null)
                {
                    _onChange = new MVCViewElementTriggerCollection();
                }

                return _onChange;
            }
        }

        public IMVCViewElement Parent { get; set; }

        public RenderValueMode RenderMode
        {
            get { return _renderMode; }
            set { _renderMode = value; }
        }

        protected virtual String TagName { get { return null; } }

        public virtual Unit Width { get; set; }

        public MVCViewElementViewMode ViewMode
        {
            get
            {
                return _viewMode;
            }
            set
            {
                _viewMode = value;
            }
        }

        protected virtual MVCViewElementTag TagType
        {
            get { return MVCViewElementTag.WithEndingTag; }
        }

        #endregion

        #region Methods

        public virtual string GetFormValueJQueryMethod()
        {
            return "";
        }

        protected virtual bool HasChildren
        {
            get { return true; }
        }

        public void Render(HtmlHelper helper)
        {
            using (HtmlTextWriter writer = new HtmlTextWriter(helper.ViewContext.HttpContext.Response.Output))
            {
                this.Render(writer);
            }
        }

        public void Render(HtmlTextWriter writer)
        {
            if (this.Controller != null && this.IsVisible)
            {
                this.IsVisible = this.Controller.HasAccessToElement(this);
            }

            if (this.IsVisible)
            {
                this.RenderInternal(writer);
            }
            else
            {
                this.RenderMarker(writer);
            }
        }

        protected virtual void RenderInternal(HtmlTextWriter writer)
        {
            if (this.TagType == MVCViewElementTag.WithEndingTag)
            {
                writer.Write("<{0}", TagName);

                WriteAttributes(writer);

                writer.Write(">");

                WriteIcon(writer);

                if (HasChildren)
                {
                    WriteChildren(writer);
                }

                writer.Write("</{0}>", TagName);
            }
            else
            {
                writer.Write("<{0}", this.TagName);

                WriteAttributes(writer);

                writer.Write("/>");                
            }

            WriteScripts(writer);
        }

        protected virtual void WriteIcon(HtmlTextWriter writer)
        {
            if (!String.IsNullOrEmpty(IconCssClass))
            {
                writer.Write("<i class=\"{0}\"></i>", IconCssClass);
            }
        }

        protected virtual void WriteScripts(HtmlTextWriter writer)
        {
            if (Draggable)
            {
                writer.Write("<script type=\"text/javascript\">applyDraggableSettings(\""+this.ClientId+"\")</script>");
            }
            if (Droppable && DropSettings != null)
            {
                StringWriter stringWriter = new StringWriter();
                using (HtmlTextWriter onItemDroppedWriter = new HtmlTextWriter(stringWriter))
	            {
                    if (DropSettings.OnItemDropped != null && DropSettings.OnItemDropped.Count > 0)
                    {
                        foreach (MVCViewElementTrigger trigger in DropSettings.OnItemDropped)
                        {
                            trigger.Controller = this.Controller;
                            trigger.Render(this, onItemDroppedWriter, "drop");
                        }
                    }
                }

                writer.Write("<script type=\"text/javascript\">applyDroppableSettings(\"" + this.ClientId + "\", \"" + DropSettings.AcceptElementsSelector + "\", \"" + System.Web.HttpUtility.JavaScriptStringEncode(stringWriter.ToString()) + "\")</script>");
            }
        }

        protected virtual void RenderMarker(HtmlTextWriter writer)
        {
            writer.Write("<{0} id=\"{1}\" style=\"display:none\">", TagName, ClientId);

            writer.Write("</{0}>", TagName);
        }

        protected virtual void WriteChildren(HtmlTextWriter writer)
        {

        }

        #region Write Attributes

        public virtual void WriteAttributes(HtmlTextWriter writer)
        {
            if (ViewMode != MVCViewElementViewMode.Print)
            {
                WriteIdAttribute(writer);
                WriteHelpTextAttribute(writer);
                WriteOnClickAttribute(writer);
                WriteOnChangeAttribute(writer);
                WriteOnFocusAttribute(writer);
                WriteOnOutFocusAttribute(writer);
                WriteOnKeyPressAttribute(writer);
                WriteOnKeyDownAttribute(writer);
                WriteOnKeyUpAttribute(writer);
                WriteOnMouseOverAttribute(writer);
            }

            WriteCssAttribute(writer);
            WriteStyleAttribute(writer);

            foreach (KeyValuePair<string, string> attribute in _attributes)
            {
                if (!String.IsNullOrEmpty(attribute.Value))
                {
                    WriteAttribute(attribute.Key.ToString().ToLower(), attribute.Value, writer);
                }
            }
        }

        protected void WriteAttribute(string name, string value, HtmlTextWriter writer)
        {
            writer.Write(String.Format(" {0}=\"{1}\"", name, value));
        }

        protected void WriteAttribute(HtmlTextWriterAttribute name, string value, HtmlTextWriter writer)
        {
            WriteAttribute(name.ToString(), value, writer);
        }

        protected virtual void WriteIdAttribute(HtmlTextWriter writer)
        {
            if (!String.IsNullOrEmpty(ClientId) && ViewMode != MVCViewElementViewMode.Print)
                writer.WriteAttribute("id", ClientId);
        }

        protected virtual void WriteCssAttribute(HtmlTextWriter writer)
        {
            if (!String.IsNullOrEmpty(CssClass))
                writer.WriteAttribute("class", CssClass);
        }

        protected virtual void WriteHelpTextAttribute(HtmlTextWriter writer)
        {
            if (HelpText != null)
            {
                WriteAttribute(HtmlTextWriterAttribute.Alt, HelpText, writer);
                WriteAttribute(HtmlTextWriterAttribute.Title, HelpText, writer);
            }
        }

        protected virtual void WriteOnClickAttribute(HtmlTextWriter writer)
        {
            if (_onClick != null && _onClick.Count > 0)
            {
                writer.Write(" onclick=\"javascript:");

                foreach (MVCViewElementTrigger onClickTrigg in _onClick)
                {
                    onClickTrigg.Controller = this.Controller;
                    onClickTrigg.Render(this, writer, "click");
                }

                writer.Write("\"");
            }
        }

        protected virtual void WriteOnChangeAttribute(HtmlTextWriter writer)
        {
            if (_onChange != null && _onChange.Count > 0)
            {
                writer.Write(" onChange=\"javascript:");

                foreach (MVCViewElementTrigger onChangeTrigg in _onChange)
                {
                    onChangeTrigg.Controller = this.Controller;
                    onChangeTrigg.Render(this, writer, "change");
                }

                writer.Write("\"");
            }
        }

        protected virtual void WriteOnFocusAttribute(HtmlTextWriter writer)
        {
            if (_onFocus != null && _onFocus.Count > 0)
            {
                writer.Write(" onfocus=\"javascript:");

                foreach (MVCViewElementTrigger onFocusTrigg in _onFocus)
                {
                    onFocusTrigg.Controller = this.Controller;
                    onFocusTrigg.Render(this, writer, "focus");
                }

                writer.Write("\"");
            }
        }

        protected virtual void WriteOnOutFocusAttribute(HtmlTextWriter writer)
        {
            if (_onOutFocus != null && _onOutFocus.Count > 0)
            {
                writer.Write(" onblur=\"javascript:");

                foreach (MVCViewElementTrigger onOutFocusTrigg in _onOutFocus)
                {
                    onOutFocusTrigg.Controller = this.Controller;
                    onOutFocusTrigg.Render(this, writer, "blur");
                }

                writer.Write("\"");
            }
        }

        protected virtual void WriteOnKeyPressAttribute(HtmlTextWriter writer)
        {
            if (_onKeyPress != null && _onKeyPress.Count > 0)
            {
                writer.Write(" onkeypress=\"javascript:");

                foreach (MVCViewElementTrigger onKeyPressTrigg in _onKeyPress)
                {
                    onKeyPressTrigg.Controller = this.Controller;
                    onKeyPressTrigg.Render(this, writer, "keypress");
                }

                writer.Write("\"");
            }
        }

        protected virtual void WriteOnKeyDownAttribute(HtmlTextWriter writer)
        {
            if (_onKeyDown != null && _onKeyDown.Count > 0)
            {
                writer.Write(" onkeydown=\"javascript:");

                foreach (MVCViewElementTrigger onKeyDownTrigg in _onKeyDown)
                {
                    onKeyDownTrigg.Controller = this.Controller;
                    onKeyDownTrigg.Render(this, writer, "keydown");
                }

                writer.Write("\"");
            }
        }

        protected virtual void WriteOnKeyUpAttribute(HtmlTextWriter writer)
        {
            if (_onKeyUp != null && _onKeyUp.Count > 0)
            {
                writer.Write(" onkeyup=\"javascript:");

                foreach (MVCViewElementTrigger onKeyUpTrigg in _onKeyUp)
                {
                    onKeyUpTrigg.Controller = this.Controller;
                    onKeyUpTrigg.Render(this, writer, "keyup");
                }

                writer.Write("\"");
            }
        }

        protected virtual void WriteOnMouseOverAttribute(HtmlTextWriter writer)
        {
            if (_onMouseOver != null && _onMouseOver.Count > 0)
            {
                writer.Write(" onmouseover=\"javascript:");

                foreach (MVCViewElementTrigger onMouseOverTrigg in _onMouseOver)
                {
                    onMouseOverTrigg.Controller = this.Controller;
                    onMouseOverTrigg.Render(this, writer, "mouseover");
                }

                writer.Write("\"");
            }
        }

        protected virtual void WriteStyleAttribute(HtmlTextWriter writer)
        {
            StringBuilder style = new StringBuilder();
            if (Width != null && !Width.IsEmpty)
            {
                style.AppendFormat("width:{0}px;", Width.Value);
            }
            if (Height != null && !Height.IsEmpty)
            {
                style.AppendFormat("height:{0}px;", Height.Value);
            }
            if (style.Length > 0)
            {
                writer.WriteAttribute("style", style.ToString());
            }
        }

        #endregion

        public void AddSecurityToken(object key, object value)
        {
            if (this.securityTokens == null)
                this.securityTokens = new Dictionary<object, object>();

            this.securityTokens[key] = value;
        }

        public bool HasSecurityToken(object key)
        {
            return this.securityTokens != null ? this.securityTokens.ContainsKey(key) : false;
        }

        public T GetSecurityToken<T>(object key)
        {
            object value;
            T securityTokenValue = default(T);

            if(this.securityTokens != null && this.securityTokens.TryGetValue(key, out value) && value != null)
            {
                securityTokenValue = (T)value;
            }

            return securityTokenValue;
        }

        #endregion
    }

    public abstract class MVCViewElement<T> : MVCViewElement
    {
        private Func<Boolean, T, T> _bind;

        /// <summary>
        /// Element bind.
        /// </summary>
        public virtual Func<Boolean, T, T> Bind
        {
            get
            {
                return _bind;
            }
            set
            {
                _bind = value;
            }
        }
    }

    public enum MVCViewElementTag
    {
        WithEndingTag,
        NoEndingTag,
    }

    public enum MVCViewUserAgentMode
    {
        Desktop,
        Mobile
    }
}
