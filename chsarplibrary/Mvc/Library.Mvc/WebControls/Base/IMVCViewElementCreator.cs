﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Mvc.WebControls.Base
{
    public interface IMVCViewElementCreator<T> where T : MVCViewElement
    {
        /// <summary>
        /// Adds the elements created to the collection supplied.
        /// </summary>
        /// <param name="coll">Collection to add to.</param>
        void AddElements(MVCViewElementCollection<T> coll);

    }
}
