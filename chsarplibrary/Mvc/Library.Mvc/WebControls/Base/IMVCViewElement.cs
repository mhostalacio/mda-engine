﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.Controllers;
using Library.Mvc.Triggers;

namespace Library.Mvc.WebControls.Base
{
    public interface IMVCViewElement : IMVCRenderElement
    {
        MVCViewElementViewMode ViewMode { get; set; }

        String Id { get; }

        String ClientId { get; set; }

        MVCControllerBase Controller { get; set; }

        MVCViewElementTriggerCollection OnClick { get; }

        IMVCViewDatasourcedElement ContextElement { get; set; }
    }
}
