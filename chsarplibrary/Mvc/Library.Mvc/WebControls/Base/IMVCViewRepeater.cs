﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Mvc.WebControls.Base
{
    public interface IMVCViewRepeater
    {
        IList<R> GetViewElements<R>() where R : IMVCViewElement;
    }
}
