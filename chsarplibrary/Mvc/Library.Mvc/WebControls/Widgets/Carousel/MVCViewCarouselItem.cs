﻿using Library.Mvc.WebControls.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace Library.Mvc.WebControls.Widgets
{
    public abstract class MVCViewCarouselItem : MVCViewCompositeElement<IMVCViewElement>
    {
        #region Methods

        internal void AddContentElements()
        {
            this.AddContentElementsInternal();
        }

        protected abstract void AddContentElementsInternal();

        #endregion
    }
}
