﻿using Library.Mvc.WebControls.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Mvc.WebControls.Widgets
{
    public class MVCViewCarouselBannerItem : MVCViewCarouselItem
    {
        #region Properties

        public string BannerSource { get; set; }

        protected override string TagName
        {
            get { return "div"; }
        }

        #endregion

        #region Methods

        protected override void AddContentElementsInternal()
        {
            MVCViewImage image;

            image = new MVCViewImage();
            image.Controller = this.Controller;
            image.CssClass = "img-thumbnail";

            if (string.IsNullOrEmpty(this.BannerSource))
            {
                image.ConcatAppPathToSource = true;
                image.Source = "/images/avatar_small.png";
            }
            else
            {
                image.Source = this.BannerSource;
            }

            this.ChildElements.Add(image);
        }

        #endregion
    }
}
