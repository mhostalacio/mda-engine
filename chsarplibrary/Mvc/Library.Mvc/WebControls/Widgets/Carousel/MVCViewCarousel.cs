﻿using Library.Mvc.WebControls.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace Library.Mvc.WebControls.Widgets
{
    public class MVCViewCarousel : MVCViewCompositeElement<MVCViewCarouselItem>
    {
        #region Properties

        public int? SlidesToShow { get; set; }
        public int? SlidesToScroll { get; set; }
        public bool InfiniteSlide { get; set; }

        protected override string TagName
        {
            get { return "div"; }
        }

        #endregion

        #region Methods

        public void AddItem(MVCViewCarouselItem item)
        {
            if (item == null)
                throw new ArgumentNullException("item");

            item.AddContentElements();

            this.ChildElements.Add(item);
        }

        protected override void WriteScripts(HtmlTextWriter writer)
        {
            base.WriteScripts(writer);

            writer.Write("<script type=\"text/javascript\">$(document).ready(function(){");
            writer.Write("$('#{0}').slick({{", this.ClientId);
            writer.Write("slidesToShow: {0},", this.SlidesToShow.GetValueOrDefault(3));
            writer.Write("slidesToScroll: {0},", this.SlidesToScroll.GetValueOrDefault(1));
            writer.Write("prevArrow : '<button type=\"button\" class=\"slick-prev\"><i class=\"fa fa-arrow-circle-left\"></button>',");
            writer.Write("nextArrow : '<button type=\"button\" class=\"slick-next\"><i class=\"fa fa-arrow-circle-right\"></button>',");
            writer.Write("infinite: {0}", this.InfiniteSlide.ToString().ToLowerInvariant());
            writer.Write("});");
            writer.Write("});</script>");
            
        }

        #endregion
    }
}
