﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;
using System.Web.UI;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewHidden<T> : MVCViewInputElement<T>
    {
        #region Fields

        private T _value;

        #endregion

        #region Properties

        public virtual T Value
        {
            get
            {
                if (Bind != null)
                {
                    _value = Bind(false, default(T));
                }
                return _value;
            }
            set
            {
                _value = value;
            }

        }

        #endregion

        public override void WriteAttributes(HtmlTextWriter writer)
        {
            base.WriteAttributes(writer);

            WriteAttribute(HtmlTextWriterAttribute.Type, "hidden", writer);

            if (Value != null)
            {
                WriteAttribute(HtmlTextWriterAttribute.Value, Convert.ToString(Value), writer);
            }
        }
    }
}
