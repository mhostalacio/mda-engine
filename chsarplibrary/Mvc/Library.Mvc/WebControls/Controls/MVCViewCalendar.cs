﻿using Library.Mvc.WebControls.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewCalendar<T> : MVCViewElement<T>
    {
        protected override string TagName
        {
            get
            {
                return "div";
            }
        }

        public virtual Func<IList<T>> DataSource { get; set; }

        public virtual Func<T, IMVCViewElement, int, MVCViewCalendarItem> ElementCreator { get; set; }

        protected override void RenderMarker(HtmlTextWriter writer)
        {
            writer.Write("<font id=\"{0}\" style=\"display:none\">", ClientId);

            writer.Write("</font>", TagName);
        }

        protected override void WriteScripts(HtmlTextWriter writer)
        {
            writer.Write("<script type=\"text/javascript\">");
            writer.Write("$('#{0}').fullCalendar(", this.ClientId);
            writer.Write("{");
            writer.Write("  header: {");
			writer.Write("      left: 'prev,next today',");
			writer.Write("      center: 'title',");
			writer.Write("      right: 'month,agendaWeek,agendaDay'");
			writer.Write("  },");
			writer.Write("  defaultDate: '{0}',", DateTime.Today.ToString("yyyy-MM-dd"));
			writer.Write("  editable: true,");
            writer.Write("  eventLimit: true, ");
            writer.Write("  events: [");

            IList<T> list = null;

            if (DataSource != null && ElementCreator != null)
            {
                list = DataSource();
            }
            for (int i = 0; i < list.Count; i++)
            {
                MVCViewCalendarItem elem = ElementCreator(list[i], Parent, i);
                writer.Write("  {");
				writer.Write("  	title: '{0}',", elem.Title);
				writer.Write("  	start: '{0}',", elem.Start.Value.ToString("yyyy-MM-ddTHH:mm:ss"));
                writer.Write("  	end: '{0}',", elem.Start.Value.ToString("yyyy-MM-ddTHH:mm:ss"));
                writer.Write("  	url: '{0}'", elem.Url);
                writer.Write("  },");
            }

            writer.Write("  ]");

            writer.Write("});");
            writer.Write("</script>");
        }
    }

    public class MVCViewCalendarItem
    {
        public string Title { get; set; }

        public DateTime? Start { get; set; }

        public DateTime? End { get; set; }

        public string Url { get; set; }
    }
}
