﻿using Library.Mvc.WebControls.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewB : MVCViewElement<string>
    {
        #region Fields

        private string text;

        #endregion

        #region Properties

        protected override string TagName
        {
            get { return "b"; }
        }

        protected sealed override bool HasChildren
        {
            get { return true; }
        }

        public string Text
        {
            get
            {
                if (this.Bind != null)
                    this.text = this.Bind(false, string.Empty);
                return this.text;
            }
            set
            {
                this.text = value;
            }
        }

        #endregion

        #region Methods

        protected override void WriteChildren(HtmlTextWriter writer)
        {
            base.WriteChildren(writer);

            if (!string.IsNullOrEmpty(this.Text))
            {
                writer.Write(Helper.NormalizeValue(this.Text, RenderValueMode.None));
            }
        }

        #endregion
    }
}
