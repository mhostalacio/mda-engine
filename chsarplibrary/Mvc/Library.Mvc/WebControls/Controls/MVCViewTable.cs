﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewTable : MVCViewCompositeElement<MVCViewTableRow>
    {
        protected override string TagName
        {
            get
            {
                return "table";
            }
        }


        protected override void WriteChildren(System.Web.UI.HtmlTextWriter writer)
        {
            List<MVCViewTableRow> headerRows = this.ChildElements.FindAll(o => o.Type == TableRowType.Header);
            List<MVCViewTableRow> footerRows = this.ChildElements.FindAll(o => o.Type == TableRowType.Footer);
            List<MVCViewTableRow> normalRows = this.ChildElements.FindAll(o => o.Type == TableRowType.Regular);
            
            if (headerRows.Count > 0)
            {
                writer.Write("<thead>");
                foreach (MVCViewTableRow element in headerRows)
                {
                    element.Render(writer);
                }
                writer.Write("</thead>");
            }

            if (footerRows.Count > 0)
            {
                writer.Write("<tfoot>");

                foreach (MVCViewTableRow element in footerRows)
                {
                    element.Render(writer);
                }

                writer.Write("</tfoot>");
            }

            if (normalRows.Count > 0)
            {
                writer.Write("<tbody>");
                foreach (MVCViewTableRow element in normalRows)
                {
                    element.Render(writer);
                }
                writer.Write("</tbody>");
            }
        }
    }

   
}
