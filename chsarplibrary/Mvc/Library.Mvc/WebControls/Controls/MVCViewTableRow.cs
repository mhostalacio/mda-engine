﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewTableRow : MVCViewCompositeElement<MVCViewTableCell>
    {
        public TableRowType Type { get; set; }
        
        protected override string TagName
        {
            get
            {
                return "tr";
            }
        }

        public IList<MVCViewTableCell> Cells
        {
            get
            {
                return ChildElements;
            }
        }
    }

    public enum TableRowType
    {
        Regular,
        Header,
        Footer,
    }
}
