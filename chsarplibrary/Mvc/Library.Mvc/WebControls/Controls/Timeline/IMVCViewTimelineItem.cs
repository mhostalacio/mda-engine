﻿using Library.Mvc.WebControls.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Mvc.WebControls.Controls
{
    public interface IMVCViewTimelineItem : IMVCViewElement
    {
        void AddContentElements();
    }
}
