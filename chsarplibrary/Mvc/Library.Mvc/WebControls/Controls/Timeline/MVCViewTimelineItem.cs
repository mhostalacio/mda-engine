﻿using Library.Mvc.WebControls.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Mvc.WebControls.Controls
{
    public abstract class MVCViewTimelineItem : MVCViewCompositeElement<IMVCViewElement>, IMVCViewTimelineItem
    {
        #region Properties

        protected override string TagName
        {
            get { return "article"; }
        }

        public override string CssClass
        {
            get { return "post"; }
            set { base.CssClass = value; }
        }

        #endregion

        #region Methods

        public void AddContentElements()
        {
            this.AddContentElementsInternal();
        }

        protected abstract void AddContentElementsInternal();

        #endregion
    }

    public abstract class MVCViewTimelineItem<T> : MVCViewTimelineItem
    {
        #region Properties

        public T Model { get; set; }

        #endregion
    }
}
