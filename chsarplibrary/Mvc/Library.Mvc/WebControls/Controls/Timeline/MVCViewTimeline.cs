﻿using Library.Mvc.WebControls.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewTimeline : MVCViewCompositeElement<IMVCViewTimelineItem>
    {
        #region Properties

        protected override string TagName
        {
            get { return "div"; }
        }

        #endregion

        #region Methods

        public void Add(IMVCViewTimelineItem item)
        {
            if (item == null)
                throw new ArgumentNullException("item");

            item.AddContentElements();

            this.ChildElements.Add(item);
        }

        public void AddRange(IEnumerable<IMVCViewTimelineItem> items)
        {
            foreach (IMVCViewTimelineItem item in items)
            {
                item.AddContentElements();
                this.ChildElements.Add(item);
            }
        }

        #endregion
    }
}
