﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;
using Library.Mvc.Controllers;
using System.Web.UI;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewForm : MVCViewCompositeElement<IMVCViewElement>
    {

        protected override string TagName
        {
            get
            {
                return "form";
            }
        }


        public override void WriteAttributes(HtmlTextWriter writer)
        {
            base.WriteAttributes(writer);

            WriteAttribute("method", "post", writer);
            WriteAttribute("action", Controller.Request.Url.LocalPath, writer);
            WriteAttribute("enctype", "multipart/form-data", writer);
        }

  
    }
}
