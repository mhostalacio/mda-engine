﻿using Library.Mvc.WebControls.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewHR : MVCViewElement
    {
        #region Properties

        protected override string TagName
        {
            get { return "hr"; }
        }

        #endregion

        #region Static Methods

        public static MVCViewHR Small()
        {
            return new MVCViewHR() { CssClass = "small" };
        }

        #endregion
    }
}
