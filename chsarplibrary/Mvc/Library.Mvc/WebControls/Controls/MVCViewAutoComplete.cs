﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;
using System.Web.UI;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewAutoComplete<T> : MVCViewElement
    {

        public bool RenderInvisible { get; set; }


        public override void WriteAttributes(HtmlTextWriter writer)
        {
            base.WriteAttributes(writer);

            if (RenderInvisible)
            {
                WriteAttribute(HtmlTextWriterAttribute.Style, "display:none", writer);
            }
        }

        protected override string TagName
        {
            get
            {
                return "div";
            }
        }

        public virtual Func<IList<T>> DataSource { get; set; }

        public Func<T, Dictionary<String, String>> DataKeyValues { get; set; }

        public virtual Func<T, IMVCViewElement, int, IList<IMVCViewElement>> ElementCreator { get; set; }

        protected override void WriteChildren(HtmlTextWriter writer)
        {
            base.WriteChildren(writer);


            writer.AddAttribute(HtmlTextWriterAttribute.Class, "AutoComplete");
            writer.AddAttribute(HtmlTextWriterAttribute.Id, String.Format("{0}_tbl", ClientId));
            writer.RenderBeginTag(HtmlTextWriterTag.Ul);

            if (DataSource != null && ElementCreator != null)
            {
                IList<T> list = DataSource();

                if (list != null && list.Count > 0)
                {
                    for (int i = 0; i < list.Count; i++)
                    {

                        writer.Write("<li class=\"AutoCompleteItem\"");
                        if (DataKeyValues != null)
                        {
                            StringBuilder sb = new StringBuilder();
                            Dictionary<String, String> args = DataKeyValues(list[i]);
                            int j = 0;
                            foreach (string key in args.Keys)
                            {
                                if (j > 0)
                                    sb.Append("|@|");
                                sb.AppendFormat("{0}|=|{1}", key, Microsoft.JScript.GlobalObject.escape(args[key]));
                                j++;
                            }
                            if (sb.Length > 0)
                            {
                                writer.Write(" datakeys=\"" + sb.ToString() + "\" ");
                            }
                        }
                        writer.Write(">");
                       
                        IList<IMVCViewElement> elements = ElementCreator(list[i], Parent, i);
                        foreach (IMVCViewElement element in elements)
                        {
                            element.Render(writer);
                        }
                        writer.Write("</li>");
                    }
                }
            }

            writer.RenderEndTag();
        }


    }
}
