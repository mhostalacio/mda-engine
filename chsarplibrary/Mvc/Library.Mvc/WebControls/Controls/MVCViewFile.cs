﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.Mvc.Triggers;
using System.Web;
using Library.Util;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewFile : MVCViewInputElement<String>
    {
        private int? _maxFileSize;
        public Boolean _allowMultipleUpload = true;

        public Boolean AcceptOnlyImages { get; set; }

        public String UploadActionName { get; set; }

        public Unit ImageWidth { get; set; }

        public Unit ImageHeight { get; set; }

        public Boolean RenderAsSingleButton { get; set; }

        public Boolean AllowMultipleUpload 
        { 
            get
            {
                return _allowMultipleUpload;
            }
            set
            {
                _allowMultipleUpload = value;
            }
        }

        public MVCViewArgumentCollection ExtraArguments
        {
            get;
            set;
        }

        public int MaxFileSize
        {
            get
            {
                if (_maxFileSize.HasValue)
                {
                    return _maxFileSize.Value;
                }
                return 1000000; //1 MB
            }
            set
            {
                _maxFileSize = value;
            }
        }

        protected override void RenderMarker(HtmlTextWriter writer)
        {
            writer.Write("<font id=\"{0}\" style=\"display:none\">", ClientId);

            writer.Write("</font>");
        }

        protected override void RenderInternal(System.Web.UI.HtmlTextWriter writer)
        {
            if (this.IsVisible)
            {
                writer.Write("<div class=\"{1}\" id=\"{0}_Container\">", this.ClientId, RenderAsSingleButton ? "" : "input-control files");

                String url = String.Format("{0}/{1}/{2}?mkey={3}", this.Controller.AreaName, this.Controller.ControllerName, UploadActionName, this.Controller.ModelKey);
                if (ExtraArguments != null)
                {
                    foreach (MVCViewArgument arg in ExtraArguments)
                    {
                        url += "&" + arg.Name + "=" + HttpUtility.UrlEncode(arg.Value.GetWritableValue());
                    }
                }
                url = this.Controller.Request.Url.Scheme + "://" + this.Controller.Request.Url.Host + this.Controller.Request.ApplicationPath + "/" + url;
                writer.WriteLine("<form action=\"{0}\" class=\"dropzone\" drop-zone=\"\" id=\"{1}_Form\">", url, this.ClientId);


                if (RenderAsSingleButton)
                {
                    MVCViewButton but = new MVCViewButton();
                    but.Id = this.Id;
                    but.Controller = this.Controller;
                    but.HelpText = this.HelpText;
                    but.ClientId = this.ClientId;
                    but.Text = this.HelpText;
                    but.CssClass = "dz-clickable";
                    but.IconCssClass = this.IconCssClass;
                    but.Render(writer);

                }
                else
                {
                    writer.WriteLine("<span class=\"btn green fileinput-button dz-clickable\" id=\"{0}_AddFiles\">", this.ClientId);
                    writer.WriteLine("<i class=\"fa fa-upload\"></i>");
                    writer.WriteLine("<span>");
                    writer.Write(Helper.NormalizeValue(HelpText));
                    writer.WriteLine("</span>");
                    writer.WriteLine("</span>");

                    writer.WriteLine("<div id=\"{0}\" class=\"file-row\">", this.ClientId);


                    writer.WriteLine("<div>");
                    writer.WriteLine("  <p class=\"name\" data-dz-name></p>");
                    writer.WriteLine("  <strong class=\"error text-danger\" data-dz-errormessage></strong>");
                    writer.WriteLine("</div>");
                    writer.WriteLine("<div>");
                    writer.WriteLine("  <p class=\"size\" data-dz-size></p>");
                    writer.WriteLine("  <div class=\"progress progress-striped active\" role=\"progressbar\" aria-valuemin=\"0\" aria-valuemax=\"100\" aria-valuenow=\"0\">");
                    writer.WriteLine("      <div class=\"progress-bar progress-bar-success\" style=\"width:0%;\" data-dz-uploadprogress id=\"{0}_ProgressBar\"></div>", this.ClientId);
                    writer.WriteLine("  </div>");
                    writer.WriteLine("</div>");

                    writer.WriteLine("</div>");
                    
                }



                writer.Write("</form>");


                writer.WriteLine("<script type=\"text/javascript\">");
                writer.WriteLine("$('#" + this.ClientId + "').dropzone({");
                writer.WriteLine("      url: '{0}',", url);
                writer.WriteLine("      method: 'post',");
                if (!AllowMultipleUpload)
                    writer.WriteLine("      maxFiles: 1,");
                writer.WriteLine("      parallelUploads: 10,");
                writer.WriteLine("      maxFilesize: {0},", MaxFileSize);
                writer.WriteLine("      autoProcessQueue: true,");
                writer.WriteLine("      paramName: 'files',");
                writer.WriteLine("      maxThumbnailFilesize: 5,");
                writer.WriteLine("      clickable: '#{0}{1}',", this.ClientId, RenderAsSingleButton ? "" : "_AddFiles");
                writer.WriteLine("      init: function() {");
                writer.WriteLine("          this.on('success', function(file, json) {");
                writer.WriteLine("              parseAjaxElementReturn(json);");
                writer.WriteLine("              $.unblockUI();");
                writer.WriteLine("          });");
                writer.WriteLine("          this.on('addedfile', function(file) {");
                writer.WriteLine("          });");
                writer.WriteLine("          this.on('drop', function(file) {");
                writer.WriteLine("          }); ");
                writer.WriteLine("          this.on('maxfilesexceeded', function(file) {");
                writer.WriteLine("              this.removeAllFiles();");
                writer.WriteLine("              this.addFile(file);");
                writer.WriteLine("          }); ");
                writer.WriteLine("          this.on('totaluploadprogress', function(progress) {");
                writer.WriteLine("              document.querySelector(\"#{0}_ProgressBar\").style.width = progress + \"%\";", this.ClientId);
                writer.WriteLine("          }); ");

                writer.WriteLine("      }");
                writer.WriteLine("});");

                writer.WriteLine("</script>");
                writer.Write("</div>");
            }
        }

        public override void WriteAttributes(System.Web.UI.HtmlTextWriter writer)
        {
            base.WriteAttributes(writer);

            WriteAttribute(HtmlTextWriterAttribute.Type, "file", writer);

        }
    }
}
