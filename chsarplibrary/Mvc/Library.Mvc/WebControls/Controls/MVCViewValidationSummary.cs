﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Util.Text;
using System.Web;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewValidationSummary : MVCViewDiv
    {
        public string ErrorCss { get; set; }

        public string InfoCss { get; set; }

        public string WarningCss { get; set; }

        public string SuccessCss { get; set; }

        public List<IMessage> Messages { get; set; }

        protected override bool HasChildren
        {
            get
            {
                return true;
            }
        }

        public string MessagesTitle { get; set; }

        protected override void RenderInternal(System.Web.UI.HtmlTextWriter writer)
        {
            if (Messages != null && Messages.Count > 0)
            {
                this.CssClass = "MessagesPanel";
            }
            base.RenderInternal(writer);
           
        }

        protected override void WriteChildren(System.Web.UI.HtmlTextWriter writer)
        {
            
            if (Messages != null && Messages.Count > 0)
            {
                if (Messages.Find(o => o.MessageType == MessageTypeEnum.ERROR) != null)
                {
                    writer.Write("<div class=\"alert alert-danger fade in \"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>");
                    if (!String.IsNullOrEmpty(MessagesTitle) && Messages != null && Messages.Count > 0)
                    {
                        writer.Write("<h4>");
                        writer.Write(MessagesTitle);
                        writer.Write("</h4>");
                    }
                    foreach (IMessage message in Messages.FindAll(o => o.MessageType == MessageTypeEnum.ERROR))
                    {
                        writer.Write("<p><label");
                        if (!String.IsNullOrEmpty(message.AssociatedFieldClientID))
                        {
                            writer.Write(" for=\"{0}\"", message.AssociatedFieldClientID);
                        }
                        writer.Write(">");
                        writer.Write(message.Text);
                        writer.Write("</label></p>");
                        writer.Write("<script>showToast('" + HttpUtility.JavaScriptStringEncode(message.Text) + "', 'error')</script>");
                    }
                    writer.Write("</div>");
                }

                if (Messages.Find(o => o.MessageType == MessageTypeEnum.WARNING) != null)
                {
                    writer.Write("<div class=\"alert alert-warning fade in \"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>");
                    if (!String.IsNullOrEmpty(MessagesTitle) && Messages != null && Messages.Count > 0)
                    {
                        writer.Write("<h4>");
                        writer.Write(MessagesTitle);
                        writer.Write("</h4>");
                    }
                    foreach (IMessage message in Messages.FindAll(o => o.MessageType == MessageTypeEnum.WARNING))
                    {
                        writer.Write("<p><label");
                        if (!String.IsNullOrEmpty(message.AssociatedFieldClientID))
                        {
                            writer.Write(" for=\"{0}\"", message.AssociatedFieldClientID);
                        }
                        writer.Write(">");
                        writer.Write(message.Text);
                        writer.Write("</label></p>");
                        writer.Write("<script>showToast('" + HttpUtility.JavaScriptStringEncode(message.Text) + "', 'warning')</script>");
                    }
                    writer.Write("</div>");
                }

                if (Messages.Find(o => o.MessageType == MessageTypeEnum.SUCCESS) != null)
                {
                    writer.Write("<div class=\"alert alert-success fade in \"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>");
                    if (!String.IsNullOrEmpty(MessagesTitle) && Messages != null && Messages.Count > 0)
                    {
                        writer.Write("<h4>");
                        writer.Write(MessagesTitle);
                        writer.Write("</h4>");
                    }
                    foreach (IMessage message in Messages.FindAll(o => o.MessageType == MessageTypeEnum.SUCCESS))
                    {
                        writer.Write("<p><label");
                        if (!String.IsNullOrEmpty(message.AssociatedFieldClientID))
                        {
                            writer.Write(" for=\"{0}\"", message.AssociatedFieldClientID);
                        }
                        writer.Write(">");
                        writer.Write(message.Text);
                        writer.Write("</label></p>");
                        writer.Write("<script>showToast('" + HttpUtility.JavaScriptStringEncode(message.Text) + "', 'success')</script>");
                    }
                    writer.Write("</div>");
                }

                if (Messages.Find(o => o.MessageType == MessageTypeEnum.INFO) != null)
                {
                    writer.Write("<div class=\"alert alert-info fade in \"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>");
                    if (!String.IsNullOrEmpty(MessagesTitle) && Messages != null && Messages.Count > 0)
                    {
                        writer.Write("<h4>");
                        writer.Write(MessagesTitle);
                        writer.Write("</h4>");
                    }
                    foreach (IMessage message in Messages.FindAll(o => o.MessageType == MessageTypeEnum.INFO))
                    {
                        writer.Write("<p><label");
                        if (!String.IsNullOrEmpty(message.AssociatedFieldClientID))
                        {
                            writer.Write(" for=\"{0}\"", message.AssociatedFieldClientID);
                        }
                        writer.Write(">");
                        writer.Write(message.Text);
                        writer.Write("</label></p>");
                        writer.Write("<script>showToast('" + HttpUtility.JavaScriptStringEncode(message.Text) + "', 'info')</script>");
                    }
                    writer.Write("</div>");
                }
                
            }
        }
    }
}
