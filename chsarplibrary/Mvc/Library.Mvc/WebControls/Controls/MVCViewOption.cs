﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;
using System.Web.UI;
using Library.Mvc.Triggers;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewOption : MVCViewElement
    {
        #region Fields

        private String _value;
        private String _text;
        private bool? _selected;

        #endregion

        #region Properties

        /// <summary>
        /// Indicates if the option is selected.
        /// </summary>
        public bool Selected
        {
            get
            {
                if (IsSelectedValue != null)
                {
                    _selected = IsSelectedValue(false, false);
                }
                if (_selected.HasValue)
                {
                    return _selected.Value;
                }
                return false;
            }
            set
            {
                _selected = value;
            }
        }

        public virtual Func<Boolean, Boolean, Boolean> IsSelectedValue { get; set; }

        /// <summary>
        /// Option value.
        /// </summary>
        public String Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
            }
        }

        /// <summary>
        /// Option text.
        /// </summary>
        public String Text
        {
            get
            {
                return _text;
            }
            set
            {
                _text = value;
            }
        }

        protected override string TagName
        {
            get
            {
                return "option";
            }
        }

        protected override bool HasChildren
        {
            get
            {
                return true;
            }
        }

        public String ParentDefaultOptionText { get; set; }

    
        #endregion

        #region [Methods]

        public override void WriteAttributes(HtmlTextWriter writer)
        {
            base.WriteAttributes(writer);

            if (Value != null)
                WriteAttribute(HtmlTextWriterAttribute.Value, Value, writer);
            if(Selected)
                WriteAttribute(HtmlTextWriterAttribute.Selected, "selected", writer);
        }

        protected override void WriteChildren(HtmlTextWriter writer)
        {
            base.WriteChildren(writer);
            writer.Write(Helper.NormalizeValue(Text));
        }


        #endregion
    }
}
