﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;
using System.Web.UI;
using System.Globalization;
using Library.Util.Transactions;
using Library.Util;
using Library.Mvc.Views;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewDateBox : MVCViewTextBoxBase<DateTime?>
    {
        private DateTime? _value;
        private String _timeZoneCode;
        private String _cultureCode;
        private DateTime? _maxDate;
        private DateTime? _minDate;
        private DateTime? _defaultDate;
        private string datePickerButtonCssClass = "btn";


        public override DateTime? Value
        {
            get
            {
                if (Bind != null)
                {
                    _value = Bind(false, default(DateTime?));
                }
                return _value;
            }
            set
            {
                if (value.HasValue && value.Value.Kind != DateTimeKind.Utc)
                {
                    throw new Exception("Value must be set in UTC");
                }
                _value = value;
            }
        }

        public virtual DateTime? MaxDate
        {
            get
            {
                return _maxDate;
            }
            set
            {
                if (value.HasValue && value.Value.Kind != DateTimeKind.Utc)
                {
                    throw new Exception("Value must be set in UTC");
                }
                _maxDate = value;
            }
        }

        public virtual DateTime? MinDate
        {
            get
            {
                return _minDate;
            }
            set
            {
                if (value.HasValue && value.Value.Kind != DateTimeKind.Utc)
                {
                    throw new Exception("Value must be set in UTC");
                }
                _minDate = value;
            }
        }

        public virtual DateTime? DefaultDate
        {
            get
            {
                return _defaultDate;
            }
            set
            {
                if (value.HasValue && value.Value.Kind != DateTimeKind.Utc)
                {
                    throw new Exception("Value must be set in UTC");
                }
                _defaultDate = value;
            }
        }

        public virtual String TimeZoneCode
        {
            get
            {
                if (_timeZoneCode == null)
                {
                    if (BusinessTransaction.CurrentContext != null)
                    {
                        _timeZoneCode = BusinessTransaction.CurrentContext.SessionInfo.TimeZoneCode;
                    }
                    else
                    {
                        _timeZoneCode = CommonSettings.DefaultTimezoneCode;
                    }
                }
                return _timeZoneCode;
            }
            set
            {
                _timeZoneCode = value;
            }
        }

        public virtual String CultureCode
        {
            get
            {
                if (_cultureCode == null)
                {
                    if (BusinessTransaction.CurrentContext != null)
                    {
                        _cultureCode = BusinessTransaction.CurrentContext.CurrentCulture.IetfLanguageTag;
                    }
                    else
                    {
                        _cultureCode = CultureInfo.InvariantCulture.IetfLanguageTag;
                    }
                }
                return _cultureCode;
            }
            set
            {
                _cultureCode = value;
            }
        }

        public override string Id
        {
            get
            {
                return base.Id;
            }
            set
            {
                base.Id = value;
            }
        }

        protected override string TagName
        {
            get
            {
                return IsEnabled ? "input" : "span";
            }
        }

        public override string CssClass
        {
            get
            {
                return IsEnabled ? "form-control" : "transparent-input";
            }
            set
            {
                base.CssClass = value;
            }
        }

        public string DatePickerButtonCssClass
        {
            get { return this.datePickerButtonCssClass; }
            set { this.datePickerButtonCssClass = value; }
        }

        protected override void RenderMarker(HtmlTextWriter writer)
        {
            writer.Write("<font id=\"{0}_Container\" style=\"display:none\"></font>", ClientId);
        }

        public override void WriteAttributes(HtmlTextWriter writer)
        {
            base.WriteAttributes(writer);

            if (ViewMode != MVCViewElementViewMode.Print)
            {
                WriteAttribute(HtmlTextWriterAttribute.Type, "text", writer);
            }

            if (IsEnabled && MaxLength.HasValue && ViewMode != MVCViewElementViewMode.Print)
            {
                WriteAttribute(HtmlTextWriterAttribute.Maxlength, MaxLength.Value.ToString(), writer);
            }

            if (Value != null && Value.HasValue)
            {
                DateTime valueToDisplay = Value.Value;

                if (!String.IsNullOrEmpty(TimeZoneCode))
                {
                    TimeZoneInfo timezone = TimeZoneInfo.FindSystemTimeZoneById(TimeZoneCode);
                    valueToDisplay = TimeZoneInfo.ConvertTimeFromUtc(valueToDisplay, timezone);
                }

                WriteAttribute(HtmlTextWriterAttribute.Value, valueToDisplay.ToString(GetDatePattern()), writer);
            }

        }

        protected override bool HasChildren
        {
            get
            {
                return !IsEnabled;
            }
        }

        protected override void WriteChildren(HtmlTextWriter writer)
        {
            base.WriteChildren(writer);

            if (!IsEnabled)
            {
                if (Value != null && Value.HasValue)
                {
                    DateTime valueToDisplay = Value.Value;

                    if (!String.IsNullOrEmpty(TimeZoneCode))
                    {
                        TimeZoneInfo timezone = TimeZoneInfo.FindSystemTimeZoneById(TimeZoneCode);
                        valueToDisplay = TimeZoneInfo.ConvertTimeFromUtc(valueToDisplay, timezone);
                    }

                    writer.Write(valueToDisplay.ToString(GetDatePattern()));
                }
            }
        }

        protected override void RenderInternal(HtmlTextWriter writer)
        {
            RenderBegin(writer);
            base.RenderInternal(writer);
            RenderEnd(writer);
        }

        protected virtual void RenderBegin(HtmlTextWriter writer)
        {
            writer.Write("<div class=\"input-group input-medium {2}\" data-date-format=\"{1}\" id=\"{0}_Container\">", ClientId, GetDatePattern().ToLower(), IsEnabled ? "date date-picker" : "");
        }

        protected virtual void RenderEnd(HtmlTextWriter writer)
        {
            if (IsEnabled)
            {
                writer.Write("<span class=\"input-group-btn\"><button class=\"{0}\" type=\"button\"><i class=\"fa fa-calendar\"></i></button></span>", this.datePickerButtonCssClass);
            }
                
            writer.Write("</div>");
        }

        protected virtual String GetDatePattern()
        {
            String dateFormat = CultureInfo.InvariantCulture.DateTimeFormat.ShortDatePattern;
            if (!String.IsNullOrEmpty(CultureCode))
            {
                CultureInfo culture = CultureInfo.GetCultureInfo(CultureCode);
                dateFormat = culture.DateTimeFormat.ShortDatePattern;
            }
            return dateFormat;
        }
    }
}
