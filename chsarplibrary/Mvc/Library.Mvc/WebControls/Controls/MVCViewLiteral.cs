﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;
using System.Web.UI;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewLiteral<T> : MVCViewElement<T>
    {

        private T _text;

        public T Text
        {
            get
            {
                if (Bind != null)
                    _text = Bind(false, default(T));
                return _text;
            }
            set
            {
                _text = value;
            }
        }

        protected override string TagName
        {
            get
            {
                return "";
            }
        }


        protected override void RenderInternal(HtmlTextWriter writer)
        {
            if (Text != null)
            {
                writer.Write(Text);
            }
            if (!String.IsNullOrEmpty(IconCssClass))
            {
                writer.Write("<i class=\"{0}\"></i>", this.IconCssClass);
            }
        }
    }
}
