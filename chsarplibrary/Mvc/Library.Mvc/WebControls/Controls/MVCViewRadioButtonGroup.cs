﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;
using System.Web.UI;
using Library.Mvc.Triggers;

namespace Library.Mvc.WebControls.Controls
{
    public partial class MVCViewRadioButtonGroup<T, R, K> : MVCViewInputElement<T>
    {
        #region Fields

        private T _selectedValue;

        #endregion

        #region Properties
 
        protected override string TagName
        {
            get { return "div"; }
        }

        public virtual T SelectedValue
        {
            get
            {
                if (Bind != null)
                {
                    _selectedValue = Bind(false, default(T));
                }
                return _selectedValue;
            }
            set { _selectedValue = value; }
        }

        public MVCViewElementDisplay Display { get; set; }

        public MVCViewRadioButtonRenderStyle RenderStyle;

        public virtual Func<IList<R>> DataSource { get; set; }

        public virtual Func<R, K> OptionHiddenValueBind { get; set; }

        public virtual Func<R, String> OptionDisplayValueBind { get; set; }

        protected override bool HasChildren
        {
            get { return true; }
        }

        public bool MultiSelection { get; set; }

        public String OriginalClientId { get; private set; }

        public virtual Func<R, IMVCViewElement, int, IMVCViewElement> ElementCreator { get; set; }

        public override string CssClass
        {
            get { return string.IsNullOrEmpty(base.CssClass) ? "md-radio-list radio-group" : base.CssClass; }
            set { base.CssClass = value; }
        }

        #endregion

        #region Methods

        public override void WriteAttributes(HtmlTextWriter writer)
        {
            base.WriteAttributes(writer);
        }

        protected override void RenderInternal(HtmlTextWriter writer)
        {
            base.RenderInternal(writer);

            if (this.IsEnabled && !this.IsReadOnly && !this.AvoidValueBinding)
            {
                this.Controller.AddFieldMapping(this.Name + "Group", this.Bind);
            }
            
        }

        protected override void WriteChildren(HtmlTextWriter writer)
        {
            base.WriteChildren(writer);

            if (DataSource != null && ElementCreator != null)
            {
                IList<R> list = DataSource();

                if (list != null && list.Count > 0)
                {
                    if (this.RenderStyle == MVCViewRadioButtonRenderStyle.Regular)
                    {
                        this.RenderRegularGroup(list, writer);
                    }
                    else if (this.RenderStyle == MVCViewRadioButtonRenderStyle.ButtonGroup)
                    {
                        this.RenderButtonGroup(list, writer);
                    }
                }
            }
        }

        private void RenderButtonGroup(IList<R> list, HtmlTextWriter writer)
        {
            for (int itemIndex = 0; itemIndex < list.Count; itemIndex++)
            {
                R itemModel = list[itemIndex];
                IMVCViewElement element = this.ElementCreator(itemModel, this.Parent, itemIndex);
                IMVCViewElement elementToRender = element;

                if (element is MVCViewRadio<K>)
                {
                    MVCViewRadio<K> radioItem;

                    radioItem = element as MVCViewRadio<K>;
                    radioItem.Display = Display;
                    radioItem.RenderStyle = MVCViewRadioButtonRenderStyle.ButtonGroup;
                    radioItem.GroupName = this.Name + "Group";
                    radioItem.IsEnabled = this.IsEnabled;
                    radioItem.Id = this.Name + "_" + itemIndex;
                    radioItem.ClientId = this.Name + "_" + itemIndex;
                    radioItem.Controller = this.Controller;

                    if (this.OptionHiddenValueBind != null)
                    {
                        radioItem.Value = this.OptionHiddenValueBind(itemModel);
                    }

                    if (SelectedValue != null && radioItem.Value != null && 
                        SelectedValue.ToString().Equals(radioItem.Value.ToString(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        radioItem.Selected = true;
                    }

                    if (this.OptionDisplayValueBind != null)
                    {
                        MVCViewLabel<string> radioItemLabel;

                        radioItemLabel = new MVCViewLabel<string>();
                        radioItemLabel.CssClass = radioItem.Selected ? "btn btn-info btn-sm active" : "btn btn-info btn-sm";
                        radioItemLabel.For = () => radioItem;
                        radioItemLabel.Text = this.OptionDisplayValueBind(itemModel);
                        radioItemLabel.HelpText = this.OptionDisplayValueBind(itemModel);
                        radioItemLabel.ChildElements.Add(radioItem);
                        radioItemLabel.Controller = this.Controller;

                        elementToRender = radioItemLabel;
                    }
                }

                
                elementToRender.Render(writer);
            }
        }

        private void RenderRegularGroup(IList<R> list, HtmlTextWriter writer)
        {
            for (int i = 0; i < list.Count; i++)
            {
                IMVCViewElement element = ElementCreator(list[i], Parent, i);

                if (element is MVCViewRadio<K>)
                {
                    MVCViewRadio<K> elem = (MVCViewRadio<K>)element;
                    elem.Display = Display;
                    elem.GroupName = this.Name + "Group";
                    elem.IsEnabled = this.IsEnabled;
                    elem.Id = this.Name + "_" + i;
                    elem.ClientId = this.Name + "_" + i;
                    if (OptionHiddenValueBind != null)
                    {
                        elem.Value = (K)(OptionHiddenValueBind(list[i]));
                    }
                    if (OptionDisplayValueBind != null)
                    {
                        elem.Text = OptionDisplayValueBind(list[i]);
                        elem.HelpText = OptionDisplayValueBind(list[i]);
                    }
                    //if (OnClick.Count > 0)
                    //    elem.OnClick.AddRange(OnClick);
                    //if (OnChange.Count > 0)
                    //    elem.OnClick.AddRange(OnChange);
                    if (SelectedValue != null && elem.Value != null && SelectedValue.ToString().Equals(elem.Value.ToString(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        elem.Selected = true;
                    }
                }
                else if (element is MVCViewCheckBox<K>)
                {
                    MVCViewCheckBox<K> elem = (MVCViewCheckBox<K>)element;
                    elem.IsEnabled = this.IsEnabled;
                    elem.Id = this.Name + "_" + i;
                    elem.ClientId = this.Name + "_" + i;
                    if (OptionHiddenValueBind != null)
                    {
                        elem.Value = OptionHiddenValueBind(list[i]);
                    }
                    if (OptionDisplayValueBind != null)
                    {
                        elem.Label = Convert.ToString(OptionDisplayValueBind(list[i]));
                        elem.HelpText = OptionDisplayValueBind(list[i]);
                    }
                    //if (OnClick.Count > 0)
                    //    elem.OnClick.AddRange(OnClick);
                    //if (OnChange.Count > 0)
                    //    elem.OnClick.AddRange(OnChange);
                    if (SelectedValue != null && elem.Value != null && SelectedValue.Equals(elem.Value))
                    {
                        elem.IsChecked = true;
                    }
                }
                //if (Display == MVCViewElementDisplay.Vertical && i > 0)
                //{
                //    writer.Write("<br/>");
                //}
                element.Controller = this.Controller;
                element.Render(writer);
            }
        }

        #endregion
    }

    public enum MVCViewElementDisplay
    {
        Horizontal,
        Vertical,
    }

    public enum MVCViewRadioButtonRenderStyle
    {
        Regular,
        ButtonGroup
    }
}
