﻿using Library.Mvc.Scripting.MagnificPopup;
using Library.Mvc.WebControls.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewLink : MVCViewSpan<String>
    {
        #region Properties

        protected override string TagName
        {
            get { return "a"; }
        }
        
        public MVCUrl Href { get; set; }

        public bool OpenInPopup { get; set; }

        public CssOverflow? PopupOverflowY { get; set; }

        #endregion

        #region Methods

        public override void WriteAttributes(HtmlTextWriter writer)
        {
            base.WriteAttributes(writer);

            if (this.Href != null)
            {
                writer.Write(" href=\"");

                this.Href.Render(writer);

                writer.Write("\"");

                if (this.OpenInPopup)
                {
                    MagnificPopupInitializer.Initialize(this, this.PopupOverflowY.GetValueOrDefault(CssOverflow.Scroll));
                }
            }
        }

        #endregion
    }
}
