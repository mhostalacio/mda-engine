﻿using Library.Mvc.Triggers;
using Library.Mvc.WebControls.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewTreeView<T> : MVCViewElement<T>
    {
        public virtual Func<IList<T>> DataSource { get; set; }

        protected override string TagName
        {
            get
            {
                return "div";
            }
        }

        public String ConfirmButtonID { get; set; }

        public String SearchButtonID { get; set; }

        public String SearchBoxID { get; set; }

        public bool HasMultiSelection { get; set; }

        protected override void WriteScripts(System.Web.UI.HtmlTextWriter writer)
        {
            string getNodeDataActionUrl = TriggerHelper.CreateUrl(this.Controller.ModelKey, this.Controller.AreaName, this.Controller.ControllerName, "GetNodeChildren", null, true);

            string selectTreeNodesActionUrl = TriggerHelper.CreateUrl(this.Controller.ModelKey, this.Controller.AreaName, this.Controller.ControllerName, "SelectTreeNodes", null, true);

            writer.Write("<script type=\"text/javascript\" language=\"javascript\">$(function () {");

            writer.Write("$('#{0}')", ClientId);
            writer.Write(".jstree({ ");
            writer.Write("'plugins' : [ 'wholerow', 'checkbox', 'search'],");
            writer.Write("'core' : {");
            writer.Write("  'themes' : {");
            writer.Write("      'variant' : 'large',");
            writer.Write("      'dots': true,");
            writer.Write("      'icons': false,");
            writer.Write("  },");
            writer.Write("  'multiple' : {0},", this.HasMultiSelection ? "true" : "false");
            writer.Write("  'animation' : 0,");
            writer.Write("  'search' : {");
            writer.Write("      'case_insensitive' : true,");
            writer.Write("      'show_only_matches': true,");
            writer.Write("      'show_matches_children': true,");
            writer.Write("  },");
            writer.Write("  'data' : {");
            writer.Write("      'url' : function (node) {");
            writer.Write("          return " + getNodeDataActionUrl + ";");
            writer.Write("      },");
            writer.Write("      'data' : function (node) { return { 'nodeId' : node.id, 'mkey':'"+this.Controller.ModelKey+"' }; }");
            writer.Write("  }");

            writer.Write("},");
            writer.Write("'checkbox' : {");
            writer.Write("  'keep_selected_style' : false");
            writer.Write("}");
            
            writer.Write("});");


            // Set confirm button click handler
            if (!String.IsNullOrEmpty(ConfirmButtonID))
            {
                writer.Write("$(\"#" + ConfirmButtonID + "\").unbind(\"click\").bind(\"click\",function () {");
                writer.Write("var checkedNodes = $(\"#{0}\").jstree(\"get_checked\");", ClientId);
                writer.Write("var url = " + selectTreeNodesActionUrl.Substring(0, selectTreeNodesActionUrl.Length - 1) + "&checkedNodes=' + checkedNodes.join(',');");
                writer.Write("getAction(url);");
                writer.Write("});");
            }

            // Search Handler
            if (!String.IsNullOrEmpty(SearchButtonID) && !String.IsNullOrEmpty(SearchBoxID))
            {
                writer.Write("$(\"#" + SearchButtonID + "\").unbind(\"click\").bind(\"click\",(function (e) {");
                writer.Write("$(\"#" + ClientId + "\").jstree(\"search\", $(\"#" + SearchBoxID + "\").val());");
                writer.Write("if ($(\"#" + ClientId + "\").has(\".jstree-search\")) {");
                writer.Write("  $('html, body').animate({");
                writer.Write("      scrollTop: $(\".jstree-search:first\").offset().top");
                writer.Write("  }, 2000);");
                writer.Write("}");
                writer.Write("}));");
            }


            writer.Write("});");
            writer.Write("</script>");
        }


        //protected override void WriteScripts(System.Web.UI.HtmlTextWriter writer)
        //{
        //    // Invoke instruction to render tree
        //    string getNodeDataActionUrl = TriggerHelper.CreateUrl(this.Controller.ModelKey, this.Controller.AreaName, this.Controller.ControllerName, "GetNodeChildren", null, true);
        //    string searchTreeNodesActionUrl = TriggerHelper.CreateUrl(this.Controller.ModelKey, this.Controller.AreaName, this.Controller.ControllerName, "SearchTreeNodes", null, true);
        //    string selectTreeNodesActionUrl = TriggerHelper.CreateUrl(this.Controller.ModelKey, this.Controller.AreaName, this.Controller.ControllerName, "SelectTreeNodes", null, true);

        //    string resultsFoundMessageText = "TODO";
        //    string invalidSearchWordLengthMessageText = "TODO";

        //    StringBuilder sb = new StringBuilder();

        //    // ### Main category tree
        //    writer.Write("<script type=\"text/javascript\" language=\"javascript\" >$(function () {");
        //    writer.Write("var MVCViewTreeViewVars = {};");
        //    writer.Write("MVCViewTreeViewVars.totalResultsFound = {};");
        //    writer.Write("MVCViewTreeViewVars.pagination = 1;");
        //    writer.Write("MVCViewTreeViewVars.currentSearchTxt = null;");
        //    writer.Write("MVCViewTreeViewVars.nodeMap = {'data':0,'id':1,'rel':2,'hasChilds':3,'style':4,'selected':5,'active':6};");
        //    writer.Write("MVCViewTreeViewVars.parseNode = function(node) { var myObj = {}; myObj.data = node.v[MVCViewTreeViewVars.nodeMap.id] + ' - ' + node.v[MVCViewTreeViewVars.nodeMap.data]; myObj.state = (node.v[MVCViewTreeViewVars.nodeMap.hasChilds] == '1'? 'closed': 'null') ; myObj.attr = {}; myObj.attr.id = node.v[MVCViewTreeViewVars.nodeMap.id]; myObj.attr.rel = (node.v[MVCViewTreeViewVars.nodeMap.rel] == '1'? 'enabled':'disabled'); myObj.attr.haschilds = (node.v[MVCViewTreeViewVars.nodeMap.hasChilds] == '1'? 'True':'False'); myObj.attr.style = node.v[MVCViewTreeViewVars.nodeMap.style]; if (typeof(node.v[MVCViewTreeViewVars.nodeMap.selected]) != 'undefined') myObj.attr.selected = node.v[MVCViewTreeViewVars.nodeMap.selected]; if (typeof(node.v[MVCViewTreeViewVars.nodeMap.active]) != 'undefined') myObj.attr.active = node.v[MVCViewTreeViewVars.nodeMap.active]; myObj.children=[]; if (node.c != null) { $.each(node.c, function(i,v) { myObj.children.push(MVCViewTreeViewVars.parseNode(v)); }); } return myObj; };");

        //    if (this.Controller.ControllerName.Equals("DocumentClassificationTreeView"))
        //    {
        //        writer.Write("MVCViewTreeViewVars.successFunction = function(data) { if (data == null) { MVCViewTreeViewVars.totalResultsFound[MVCViewTreeViewVars.currentSearchTxt] = true; MVCViewTreeViewVars.pagination=1; if (typeof(window.hasMoreResultsURL) != 'undefined') getAction(window.hasMoreResultsURL.replace('[0]', 'false')); return []; } else { if (typeof(window.hasMoreResultsURL) != 'undefined' && (typeof(MVCViewTreeViewVars.totalResultsFound[MVCViewTreeViewVars.currentSearchTxt]) == 'undefined' || MVCViewTreeViewVars.totalResultsFound[MVCViewTreeViewVars.currentSearchTxt] == false)) getAction(window.hasMoreResultsURL.replace('[0]', 'true')); } };");
        //        writer.Write("MVCViewTreeViewVars.dataFunction = function (searchTxt) { if(searchTxt.length < 3) { alert('" + invalidSearchWordLengthMessageText + "'); } else { if (searchTxt != MVCViewTreeViewVars.currentSearchTxt) { MVCViewTreeViewVars.pagination=1; MVCViewTreeViewVars.currentSearchTxt = searchTxt; }; return { 'searchText': (detectIE() ? searchTxt + '_' + MVCViewTreeViewVars.pagination++ : searchTxt) }; } };");
        //    }
        //    writer.Write("$('#{0}')", ClientId);
        //    writer.Write(".jstree({ ");
        //    writer.Write("themes: { theme: \"classic\", dots: true, icons: true, url: \"" + Controller.GenerateContentUrl("/Scripts/treeview/classic/style.min.css") + "\" }, ");
        //    writer.Write("ui: { }, ");

        //    writer.Write("checkbox : { two_state : true }, ");
            

        //    writer.Write("json_data: { ajax: { url: " + getNodeDataActionUrl + ", progressive_render: true, data: function (n) { return (n == -1 ? { 'nodeId': '0' } : { 'nodeId': n.attr('id') }); }, success: function(data) { myData = []; $.each(data, function(index,value) { var parsedNode = MVCViewTreeViewVars.parseNode(value); myData.push(parsedNode) }); return myData; }, error: function (xhr, status, error) { alert('[Error] ' + status + ' Exception: ' + error); }, cache: false } }, ");

        //    writer.Write("search: { case_insensitive: true, _search_open : function (){return false;}, ajax: { error: function() { alert('error on search'); }, url : " + searchTreeNodesActionUrl + ", data: function (searchTxt) { if(searchTxt.length < 3) { alert('" + invalidSearchWordLengthMessageText + "'); } else { return { 'searchText': searchTxt } } } } },");

        //    writer.Write("\"types\" : { \"types\" : { \"disabled\" : { \"check_node\" : function(n){ return false; }, \"uncheck_node\" : function(n){ return false; }, \"select_node\" : function(n){ return false; }, \"icon\" : { \"image\" : \"" + Controller.GenerateContentUrl("/Scripts/treeview/classic/folder_root.png") + "\" } }, \"enabled\" : { \"check_node\" : true, \"icon\" : { \"image\" : \"" + Controller.GenerateContentUrl("/Scripts/treeview/classic/folder_root.png") + "\" } }, \"default\" : { \"valid_children\" : [ \"default\" ] } } },");

        //    writer.Write("plugins: ['themes', 'json_data', 'search', 'ui', 'checkbox', 'types'], ");
        //    writer.Write("callback: {");
        //    writer.Write(" onload: function (tree) {");
        //    writer.Write(" $('li[selected=true]').each(function () {");
        //    writer.Write(" alert(this);");
        //    //writer.Write(" $.tree.plugins.checkbox.check(this);");
        //    writer.Write(" });}}");
        //    writer.Write("});");

        //    // ### End.Main category tree

        //    // ### Handlers
        //    // Set search button click handler
        //    writer.Write("$(\"#"+SearchButtonID+"\").unbind(\"click\").bind(\"click\",(function () {");
        //    writer.Write("$(\"#" + ClientId + "\").jstree(\"search\", $(\"#" + SearchBoxID + "\").val());");
        //    writer.Write("document.getElementById('MessagesPanel').innerHTML = \"<b>0</b> " + resultsFoundMessageText + "\";");
        //    writer.Write("}));");

        //    if (!HasMultiSelection)
        //    {
        //        // Disable multiple selection
        //        writer.Write("$(\"#{0}\").unbind(\"change_state.jstree\").bind(\"change_state.jstree\", function(e, data)", ClientId);
        //        writer.Write("{ if (data.inst.get_checked().length > 1) {");
        //        writer.Write("data.inst.uncheck_node_single(data.rslt[0]); }");
        //        writer.Write("});");
        //    }
           
        //    writer.Write(" $(\"#" + ClientId + "\").delegate(\"a\", \"click\", function (event, data) { $(event.target).find(\".jstree-checkbox\").click() }); ");

        //    writer.Write("$(\"#{0}\").unbind(\"search.jstree\").bind(\"search.jstree\", function(e, data)", ClientId);

        //    // Show number of results found
        //    writer.Write("{ showSearchResults('" + ClientId.Substring(2) + "', '" + resultsFoundMessageText + "'); });");

        //    // Set clear button click handler
        //    writer.Write("$(\"#lnkClearSearch_" + ClientId.Substring(2) + "\").unbind(\"click\").bind(\"click\",function () {");
        //    writer.Write("$(\"#" + ClientId + "\").jstree(\"clear_search\");");
        //    writer.Write("$(\"#txtQuickSearchText_" + ClientId.Substring(2) + "\").val('');");
        //    writer.Write("document.getElementById('div" + ClientId.Substring(2) + "Info').innerHTML = \"\";");
        //    writer.Write("});");

        //    if (!ClientId.Contains("Secondary"))
        //    {
        //        // Set OK button click handler
        //        writer.Write("$(\"#"+ConfirmButtonID+"\").unbind(\"click\").bind(\"click\",function () {");
        //        writer.Write("var checkedNodes = [];");
        //        writer.Write("$(\"#{0}\").jstree(\"get_checked\").each(", ClientId);
        //        writer.Write("function () { checkedNodes.push(this.id); });");

        //        writer.Write("var url = " + selectTreeNodesActionUrl.Substring(0, selectTreeNodesActionUrl.Length - 1) + "&checkedNodes=' + checkedNodes.join(',');");
        //        writer.Write("getAction(url);");
        //        writer.Write("});");
        //        // ### End.Handlers
        //    }

        //    writer.Write("});");

        //    writer.Write("</script>");
        //}
    }
}
