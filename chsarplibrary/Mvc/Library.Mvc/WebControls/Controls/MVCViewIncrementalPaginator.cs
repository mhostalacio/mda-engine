﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Util.Collections;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewIncrementalPaginator<T> : MVCViewPaginator<T>
    {
        protected override void WriteChildren(System.Web.UI.HtmlTextWriter writer)
        {
            if (DataSource != null)
            {
                writer.Write("<tr class=\"PaginatorIncrementalRow\">");
                PagedList<T> source = DataSource();
                if (source != null)
                {
                    int totaldisplayed = (source.CurrentPage + 1) * source.PageSize;
                    if (source.HasMoreEntities || source.Count > totaldisplayed)
                    {
                        writer.Write("<td class=\"PaginatorNextCell\">");
                        MVCViewButton but = GetPageButton(source.CurrentPage + 1);
                        but.Text = this.Controller.GetTranslation("GlobalSentences", "MORE_PAGES");
                        but.Render(writer);
                        writer.Write("</td>");

                        if (source.CurrentPage > 0)
                        {
                            //writer.Write("<td class=\"PaginatorResetCell\">");
                            //MVCViewButton but = GetPageButton(0);
                            //but.Text = this.Controller.GetTranslation("GlobalSentences", "RESET");
                            //but.Render(writer);
                            //writer.Write("</td>");
                        }
                    }
                }

                writer.Write("</tr>");
            }
        }

    }
}
