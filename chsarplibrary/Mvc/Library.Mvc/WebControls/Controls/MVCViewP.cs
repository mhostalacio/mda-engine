﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;
using System.Web.UI;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewP<T> : MVCViewElement<T>
    {
        private String _text;

        /// <summary>
        /// Span text.
        /// </summary>
        public String Text
        {
            get
            {
                if (Bind != null)
                    _text = Convert.ToString(Bind(false, default(T)));
                return _text;
            }
            set
            {
                _text = value;
            }
        }

        protected override string TagName
        {
            get
            {
                return "p";
            }
        }


        protected override bool HasChildren
        {
            get
            {
                return true;
            }
        }

        private List<IMVCViewElement> _childElements = new List<IMVCViewElement>();
        public List<IMVCViewElement> ChildElements
        {
            get
            {
                return _childElements;
            }
        }

        protected override void WriteChildren(System.Web.UI.HtmlTextWriter writer)
        {
            base.WriteChildren(writer);
            if (Text != null)
            {
                writer.Write(Helper.NormalizeValue(Text, RenderValueMode.None));
            }
            if (ChildElements != null)
            {
                foreach (IMVCViewElement element in ChildElements)
                {
                    element.Render(writer);
                }
            }
        }



    }
}
