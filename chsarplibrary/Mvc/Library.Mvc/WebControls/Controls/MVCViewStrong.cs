﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewStrong<T> : MVCViewSpan<T>
    {
        #region Properties

        protected override string TagName
        {
            get { return "strong"; }
        }

        #endregion
    }
}
