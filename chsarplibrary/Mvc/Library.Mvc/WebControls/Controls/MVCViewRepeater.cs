﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;
using Library.Util.Collections;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewRepeater<T> : MVCViewElement<T>, IMVCViewRepeater, IMVCViewDatasourcedElement
    {
        private String _tagName = "font";
        private int _currentIndex;
        private Object _currentBind;

        protected override string TagName
        {
            get
            {
                if (DataSource != null && ElementCreator != null)
                {
                    IList<T> list = DataSource();

                    if (list != null && list.Count > 0)
                    {
                        return _tagName;
                    }
                }
                return "font";
               
            }
        }

        public String RenderTagName
        {
            set
            {
                _tagName = value;
            }
        }

        public virtual Func<IList<T>> DataSource { get; set; }

        public virtual Func<IMVCViewDatasourcedElement, T, int, IList<IMVCViewElement>> ElementCreator { get; set; }

        protected override bool HasChildren
        {
            get
            {
                return true;
            }
        }

        protected override void WriteChildren(System.Web.UI.HtmlTextWriter writer)
        {
            base.WriteChildren(writer);

            if (DataSource != null && ElementCreator != null)
            {
                IList<T> list = DataSource();

                if (list != null && list.Count > 0)
                {
                    if (list is PagedList<T>)
                    {
                        //must render only what the page brings
                        PagedList<T> pagedList = (PagedList<T>)list;
                        int start = pagedList.CurrentPage * pagedList.PageSize;
                        int end = Math.Min(start + pagedList.PageSize, list.Count);
                        for (int i = start; i < end; i++)
                        {
                            IList<IMVCViewElement> elements = ElementCreator(this, list[i], i);
                            foreach (IMVCViewElement element in elements)
                            {
                                element.Render(writer);
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < list.Count; i++)
                        {
                            IList<IMVCViewElement> elements = ElementCreator(this, list[i], i);
                            foreach (IMVCViewElement element in elements)
                            {
                                element.Render(writer);
                            }
                        }
                    }
                }
            }
        }

        public IList<R> GetViewElements<R>()
            where R : IMVCViewElement
        {
            List<R> toRet = new List<R>();
            if (DataSource != null && ElementCreator != null)
            {
                IList<T> list = DataSource();

                if (list != null && list.Count > 0)
                {

                    if (list is PagedList<T>)
                    {
                        PagedList<T> pagedList = (PagedList<T>)list;
                        int start = pagedList.CurrentPage * pagedList.PageSize;
                        int end = Math.Min(start + pagedList.PageSize, list.Count);
                        for (int i = start; i < end; i++)
                        {
                            CurrentBind = list[i];
                            CurrentIndex = i;
                            IList<IMVCViewElement> elements = ElementCreator(this, list[i], i);
                            foreach (IMVCViewElement elem in elements)
                            {
                                if (elem is IMVCViewRepeater)
                                {
                                    toRet.AddRange(((IMVCViewRepeater)elem).GetViewElements<R>());
                                }
                                else
                                {
                                    toRet.Add((R)elem);
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < list.Count; i++)
                        {
                            CurrentBind = list[i];
                            CurrentIndex = i;
                            IList<IMVCViewElement> elements = ElementCreator(this, list[i], i);
                            foreach (IMVCViewElement elem in elements)
                            {
                                if (elem is IMVCViewRepeater)
                                {
                                    toRet.AddRange(((IMVCViewRepeater)elem).GetViewElements<R>());
                                }
                                else
                                {
                                    toRet.Add((R)elem);
                                }
                            }
                        }
                    }
                    
                }
            }
            return toRet;
        }

        public void RenderIncremental(System.Web.UI.HtmlTextWriter writer)
        {
            if (DataSource != null && ElementCreator != null)
            {
                IList<T> list = DataSource();

                if (list != null && list.Count > 0)
                {
                    if (list is PagedList<T>)
                    {
                        //must render only what the page brings
                        PagedList<T> pagedList = (PagedList<T>)list;
                        int start = pagedList.CurrentPage * pagedList.PageSize;
                        if (start > 0)
                        {
                            //find previous id
                            IList<IMVCViewElement> previousElements = ElementCreator(this, list[start - 1], start - 1);
                            String selector = "#" + previousElements.Last().ClientId;
                            List<IMVCViewElement> allElements = new List<IMVCViewElement>();
                            IList<MVCViewTableRow> rows = null;
                            int end = Math.Min(start + pagedList.PageSize, list.Count);
                            rows = new List<MVCViewTableRow>();
                            for (int i = start; i < end; i++)
                            {
                                CurrentBind = list[i];
                                CurrentIndex = i;
                                IList<IMVCViewElement> elements = ElementCreator(this, list[i], i);
                                foreach (IMVCViewElement element in elements)
                                {
                                    if (element is MVCViewTableRow)
                                        rows.Add((MVCViewTableRow)element);
                                    else
                                        allElements.Add(element);
                                }
                            }
                            if (rows.Count > 0)
                            {
                                this.Controller.WriteAddRowsAfter(selector, rows, writer);
                            }
                            else
                            {
                                this.Controller.WriteAddElementsAfter(selector, allElements, writer);
                            }
                            
                        }
                    }
                }
            }
        }



        public int CurrentIndex
        {
            get
            {
                return _currentIndex;
            }
            set
            {
                _currentIndex = value;
            }
        }

        public object CurrentBind
        {
            get
            {
                return _currentBind;
            }
            set
            {
                _currentBind = value;
            }
        }
    }

}
