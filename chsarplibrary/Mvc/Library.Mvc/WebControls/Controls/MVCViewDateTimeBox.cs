﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Globalization;
using Library.Mvc.Views;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewDateTimeBox : MVCViewDateBox
    {

        protected override String GetDatePattern()
        {
            String dateFormat = base.GetDatePattern();
            if (!String.IsNullOrEmpty(CultureCode))
            {
                CultureInfo culture = CultureInfo.GetCultureInfo(CultureCode);
                dateFormat += " " + culture.DateTimeFormat.ShortTimePattern;
            }
            else
            {
                dateFormat += " " + CultureInfo.InvariantCulture.DateTimeFormat.ShortTimePattern;
            }
            return dateFormat;
        }

        protected override void RenderBegin(HtmlTextWriter writer)
        {
            if (IsEnabled)
            {
                writer.Write("<div class=\"input-group date form_advance_datetime\" date-format=\"{1}\" id=\"{0}_Container\">", ClientId, GetJQueryDateFormat());
            }
            else
            {
                writer.Write("<div date-format=\"{1}\" id=\"{0}_Container\">", ClientId, GetJQueryDateFormat());
            }
        }

        private String GetJQueryDateFormat()
        {
            return JavaScriptHelper.GetDateTimeFormatToJQueryUI(GetDatePattern());
        }

        protected override void RenderEnd(HtmlTextWriter writer)
        {
            if (IsEnabled)
            {
                writer.Write("<span class=\"input-group-btn\">");
                writer.Write("<button class=\"btn default date-reset\" type=\"button\"><i class=\"fa fa-times\"></i></button>");
                writer.Write("<button class=\"btn default date-set\" type=\"button\"><i class=\"fa fa-calendar\"></i></button>");
                writer.Write("</span>");
            }
                
            writer.Write("</div>");
        }
    }
}
