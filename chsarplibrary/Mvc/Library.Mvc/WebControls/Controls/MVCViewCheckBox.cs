﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;
using System.Web.UI;
using Library.Mvc.Triggers;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewCheckBox<T> : MVCViewInputElement<T>, IMVCViewCheckBox
    {
        #region Fields

        private Func<Boolean, Boolean?, Boolean?> _isCheckedValue;
        private Boolean? _isChecked;
        private T _value;
        private MVCViewElementTriggerCollection _onCheck;
        private MVCViewElementTriggerCollection _onUncheck;
        private String _label;

        #endregion

        #region Properties

        public virtual Func<Boolean, Boolean?, Boolean?> IsCheckedValue
        {
            get
            {
                return _isCheckedValue;
            }
            set
            {
                _isCheckedValue = value;
            }
        }

        public bool IsChecked
        {
            get
            {
                if (IsCheckedValue != null)
                {
                    _isChecked = IsCheckedValue(false, false);
                }
                if (_isChecked.HasValue)
                {
                    return _isChecked.Value;
                }
                return false;
            }
            set
            {
                _isChecked = value;
            }
        }

        public T Value
        {
            get
            {
                if (Bind != null)
                    _value = Bind(false, default(T));
                return _value;
            }
            set
            {
                _value = value;
            }
        }

        public virtual Func<Boolean, String, String> LabelBind
        {
            get;
            set;
        }

        public String Label
        {
            get
            {
                if (LabelBind != null)
                    _label = Convert.ToString(LabelBind(false, String.Empty));
                return _label;
            }
            set
            {
                _label = value;
            }
        }

        public MVCViewElementTriggerCollection OnCheck
        {
            get
            {
                if (_onCheck == null)
                {
                    _onCheck = new MVCViewElementTriggerCollection();
                }

                return _onCheck;
            }
        }

        public MVCViewElementTriggerCollection OnUncheck
        {
            get
            {
                if (_onUncheck == null)
                {
                    _onUncheck = new MVCViewElementTriggerCollection();
                }

                return _onUncheck;
            }
        }

        public override string ClientId
        {
            get
            {
                return base.ClientId;
            }
            set
            {
                base.ClientId = value;
                if (String.IsNullOrEmpty(OriginalClientId))
                    OriginalClientId = value;
            }
        }

        public String OriginalClientId { get; private set; }

        public override string CssClass
        {
            get { return string.IsNullOrEmpty(base.CssClass) && this.RenderStyle != MVCViewRadioButtonRenderStyle.ButtonGroup ? "md-check" : base.CssClass; }
            set { base.CssClass = value; }
        }

        protected override string TagName
        {
            get { return "input"; }
        }

        public MVCViewRadioButtonRenderStyle RenderStyle;

        #endregion

        #region Methods

        protected override void AddFieldMapping()
        {
            if (IsEnabled && !IsReadOnly && !AvoidValueBinding)
            {
                Controller.AddFieldMapping(this.OriginalClientId, IsCheckedValue);
            }
        }

        protected override void RenderInternal(HtmlTextWriter writer)
        {
            if (this.RenderStyle == MVCViewRadioButtonRenderStyle.Regular)
            {
                writer.Write("<div class=\"md-checkbox\" id=\"{0}_Container\">", this.ClientId);

                if (IsEnabled && !IsReadOnly)
                {
                    writer.Write("<input type=\"hidden\" id=\"{0}\" name=\"{0}\" value=\"{1}\">", this.ClientId, IsChecked);
                }

                this.OriginalClientId = this.ClientId;
                this.ClientId = this.ClientId + "_Check";

                base.RenderInternal(writer);

                writer.Write("<label for=\"{0}\">", ClientId);
                writer.Write("<span>");
                writer.Write("</span>");
                writer.Write("<span class=\"check\">");
                writer.Write("</span>");
                writer.Write("<span class=\"box\">");
                writer.Write("</span>");

                if (LabelBind != null)
                {
                    writer.Write(Helper.NormalizeValue(Label, RenderMode));
                }

                writer.Write("</label>");

                this.RenderScript(writer);
                writer.Write("</div>");

                
            }
            else if (this.RenderStyle == MVCViewRadioButtonRenderStyle.ButtonGroup)
            {
                string originalClientId = this.ClientId;
                string clientId = this.ClientId + "_Check";
                string labelCss = this.IsChecked ? "btn btn-info btn-sm active" : "btn btn-info btn-sm";

                writer.Write("<div class=\"btn-group\" data-toggle=\"buttons\">");
                writer.Write("<label for=\"{0}\" class=\"{1}\">", clientId, labelCss);

                if (this.IsEnabled && !this.IsReadOnly)
                {
                    writer.Write("<input type=\"hidden\" id=\"{0}\" name=\"{0}\" value=\"{1}\"/>", originalClientId, this.IsChecked);
                }

                this.OriginalClientId = originalClientId;
                this.ClientId = clientId;

                base.RenderInternal(writer);

                if (this.LabelBind != null)
                {
                    writer.Write(Helper.NormalizeValue(this.Label, this.RenderMode));
                }

                writer.Write("</label>");
                writer.Write("</div>");
            }
        }

        private void RenderScript(HtmlTextWriter writer)
        {
            writer.Write("");
            writer.Write("<script type=\"text/javascript\">");
            writer.Write("$('#{0}').on('change', function(event)", this.ClientId);
            writer.Write("{");
            writer.Write("$(this).val(event.target.checked);");
            writer.Write("setCheckValue(event, '{0}');", this.OriginalClientId);
            writer.Write("});");

            if (this.OnChange.Count > 0)
            {
                writer.Write("$('#{0}').on('change', function(event)", this.OriginalClientId);
                writer.Write("{");
                foreach (MVCViewElementTrigger onChangeTrigg in this.OnChange)
                {
                    onChangeTrigg.Controller = this.Controller;
                    onChangeTrigg.Render(this, writer, "change");
                }
                writer.Write("});");
            }
            writer.Write("</script>");
        }

        protected override void WriteOnChangeAttribute(HtmlTextWriter writer)
        {
            //base.WriteOnChangeAttribute(writer);
        }

        protected override void WriteOnClickAttribute(HtmlTextWriter writer)
        {
            //base.WriteOnClickAttribute(writer);
        }

        public override void WriteAttributes(HtmlTextWriter writer)
        {
            base.WriteAttributes(writer);

            WriteAttribute(HtmlTextWriterAttribute.Type, "checkbox", writer);

            if (Value != null)
                WriteAttribute(HtmlTextWriterAttribute.Value, Convert.ToString(Value), writer);

            if (IsChecked)
                WriteAttribute(HtmlTextWriterAttribute.Checked, IsChecked.ToString().ToLower(), writer);
        }

        protected override void RenderMarker(HtmlTextWriter writer)
        {
            writer.Write("<font id=\"{0}_Container\" style=\"display:none\">",  ClientId);

            writer.Write("</font>", TagName);
        }

        #endregion
    }

    public interface IMVCViewCheckBox
    {
        string OriginalClientId { get; }
    }
}
