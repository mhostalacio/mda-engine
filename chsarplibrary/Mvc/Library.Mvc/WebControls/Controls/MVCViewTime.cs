﻿using Library.Mvc.WebControls.Base;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using Library.Util.Transactions;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewTime : MVCViewElement<DateTime?>
    {
        #region Fields

        private DateTime? _value;
        private string formattedValue;

        #endregion

        #region Properties

        public string Description { get; set; }
        
        protected override string TagName
        {
            get { return "time"; }
        }

        protected override bool HasChildren
        {
            get { return true; }
        }

        public DateTime? Value
        {
            get
            {
                if (Bind != null)
                {
                    _value = Bind(false, default(DateTime?));
                }
                return _value;
            }
            set
            {
                _value = value;
            }
        }

        #endregion

        #region Methods

        public override void WriteAttributes(HtmlTextWriter writer)
        {
            base.WriteAttributes(writer);

            if (this.Value.HasValue)
            {
                this.formattedValue = this.Value.Value.ToString(BusinessTransaction.CurrentContext.CurrentCulture.DateTimeFormat.ShortDatePattern);

                writer.WriteAttribute("datetime", formattedValue);
            }
        }

        protected override void WriteChildren(HtmlTextWriter writer)
        {
            base.WriteChildren(writer);

            if (!string.IsNullOrEmpty(this.Description))
            {
                writer.Write(this.Description);
            }
            else
            {
                writer.Write(this.formattedValue);
            }
        }

        #endregion
    }
}
