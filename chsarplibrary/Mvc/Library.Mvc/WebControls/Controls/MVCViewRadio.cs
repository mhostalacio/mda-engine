﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;
using System.Web.UI;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewRadio<T> : MVCViewInputElement<T>
    {
        #region Fields

        private Func<Boolean, Boolean?, Boolean?> _isCheckedValue;
        private T _value;
        private String _text;
        private String _groupName;
        private bool _selected;

        #endregion

        #region Properties

        public MVCViewRadioButtonRenderStyle RenderStyle;

        /// <summary>
        /// Option value.
        /// </summary>
        public T Value
        {
            get
            {
                if (Bind != null)
                    _value = Bind(false, default(T));
                return _value;
            }
            set
            {
                _value = value;
            }
        }


        public virtual Func<Boolean, Boolean?, Boolean?> IsCheckedValue
        {
            get
            {
                return _isCheckedValue;
            }
            set
            {
                _isCheckedValue = value;
            }
        }

        public String GroupName
        {
            get
            {
                return _groupName;
            }
            set
            {
                _groupName = value;
            }
        }

        /// <summary>
        /// Option text.
        /// </summary>
        public String Text
        {
            get
            {
                return _text;
            }
            set
            {
                _text = value;
            }
        }

        /// <summary>
        /// Indicates if the option is selected.
        /// </summary>
        public bool Selected
        {
            get
            {
                if (IsCheckedValue != null && IsCheckedValue(false, false).HasValue)
                    _selected = IsCheckedValue(false, false).Value;
                return _selected;
            }
            set
            {
                _selected = value;
            }
        }


        protected override string TagName
        {
            get
            {
                return "input";
            }
        }

        public MVCViewElementDisplay Display { get; set; }

        #endregion

        #region Methods

        public override void WriteAttributes(HtmlTextWriter writer)
        {
            base.WriteAttributes(writer);
            
            WriteAttribute(HtmlTextWriterAttribute.Type, "radio", writer);

            WriteAttribute(HtmlTextWriterAttribute.Value, Convert.ToString(Value), writer);

            if (!String.IsNullOrEmpty(HelpText))
            {
                WriteAttribute(HtmlTextWriterAttribute.Title, HelpText, writer);
            }

            if (Selected)
                WriteAttribute(HtmlTextWriterAttribute.Checked, true.ToString().ToLower(), writer);
        }

        protected override void WriteNameAttribute(HtmlTextWriter writer)
        {
            WriteAttribute(HtmlTextWriterAttribute.Name, GroupName, writer);
        }

        protected override void RenderInternal(HtmlTextWriter writer)
        {
            if (this.RenderStyle == MVCViewRadioButtonRenderStyle.Regular)
            {
                writer.Write("<div class=\"md-radio\" id=\"{0}_Container\"><label>", this.ClientId);
                if (CssClass == null)
                    CssClass = "md-radiobtn";
                base.RenderInternal(writer);

                writer.Write("<label for=\"{0}\">", this.ClientId);
				writer.Write("<span></span>");
				writer.Write("<span class=\"check\"></span>");
				writer.Write("<span class=\"box\"></span>");
                if (!String.IsNullOrEmpty(Text))
                {
                    writer.Write(Helper.NormalizeValue(Text, RenderMode));
                }
                writer.Write("</label>");
                writer.Write("</div>");
            }
            else
            {
                base.RenderInternal(writer);
            }
        }

        #endregion
    }
}
