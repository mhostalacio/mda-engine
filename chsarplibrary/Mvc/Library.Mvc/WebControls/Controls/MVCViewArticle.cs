﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewArticle<T> : MVCViewElement<T>
    {
        protected override string TagName
        {
            get
            {
                return "article";
            }
        }


        protected override bool HasChildren
        {
            get
            {
                return true;
            }
        }

        private List<IMVCViewElement> _childElements = new List<IMVCViewElement>();
        public List<IMVCViewElement> ChildElements
        {
            get
            {
                return _childElements;
            }
        }

        protected override void WriteChildren(System.Web.UI.HtmlTextWriter writer)
        {
            base.WriteChildren(writer);
            if (ChildElements != null)
            {
                foreach (IMVCViewElement element in ChildElements)
                {
                    element.Render(writer);
                }
            }
        }
    }
}
