﻿using Library.Mvc.WebControls.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewIcon : MVCViewCompositeElement<IMVCViewElement>
    {
        #region Properties

        protected override string TagName
        {
            get { return "i"; }
        }

        #endregion

        #region Static Methods

        public static MVCViewIcon Factory(string cssClass)
        {
            return new MVCViewIcon() { CssClass = cssClass };
        }

        #endregion
    }
}
