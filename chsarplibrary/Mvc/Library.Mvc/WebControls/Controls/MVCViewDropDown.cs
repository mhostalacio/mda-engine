﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;
using System.Web.UI;
using System.Collections;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewDropDown<T, R, K> : MVCViewInputElement<T>
    {
        private T _selectedValue;
        private Boolean _addDefaultOption = true;
        private String _defaultOptionText = "";

        public bool MultiSelection { get; set; }

        protected override string TagName
        {
            get
            {
                return "select";
            }
        }

        public virtual T SelectedValue
        {
            get
            {
                if (Bind != null)
                {
                    _selectedValue = Bind(false, default(T));
                }
                return _selectedValue;
            }
            set
            {
                _selectedValue = value;
            }
        }

        public virtual Func<IList<R>> DataSource { get; set; }

        public virtual Func<R, K> OptionHiddenValueBind { get; set; }

        public virtual Func<R, String> OptionDisplayValueBind { get; set; }

        protected override bool HasChildren
        {
            get
            {
                return true;
            }
        }

        public virtual Func<R, IMVCViewElement, int, MVCViewOption> ElementCreator { get; set; }

        public bool AddDefaultOption
        {
            get
            {
                return _addDefaultOption;
            }
            set
            {
                _addDefaultOption = value;
            }
        }

        public virtual String DefaultOptionText
        {
            get
            {
                if (DefaultOptionTextValue != null)
                {
                    _defaultOptionText = DefaultOptionTextValue(false, null);
                }
                else
                {
                    _defaultOptionText = this.Controller.GetTranslation("GlobalSentences", "DefaultOptionText");
                }
                return _defaultOptionText;
            }
            set
            {
                _defaultOptionText = value;
            }
        }

        public virtual Func<Boolean, String, String> DefaultOptionTextValue
        {
            get;
            set;
        }

        public override void WriteAttributes(HtmlTextWriter writer)
        {
            base.WriteAttributes(writer);

            if (MultiSelection)
                WriteAttribute(HtmlTextWriterAttribute.Multiple, true.ToString().ToLower(), writer);
        }

        protected override void RenderInternal(HtmlTextWriter writer)
        {
            //if (SelectedValue != null || (AddDefaultOption && !String.IsNullOrEmpty(DefaultOptionText)))
            //{
                CssClass += " edited";
            //}
            //writer.Write("<div class=\"input-control select\" data-role=\"input-control\" id=\"{0}_Container\">", this.ClientId);
            base.RenderInternal(writer);
            //writer.Write("</div>");

            if (IsEnabled && !IsReadOnly && !MultiSelection && !AvoidValueBinding)
                Controller.AddFieldMapping(this.ClientId, Bind);
        }

        //protected override void WriteOnClickAttribute(HtmlTextWriter writer)
        //{
        //    //do nothing -> done in option RenderInternal()
        //}

        //protected override void WriteOnChangeAttribute(HtmlTextWriter writer)
        //{
        //    //do nothing -> done in option RenderInternal()
        //}

        protected override void RenderMarker(HtmlTextWriter writer)
        {
            writer.Write("<font id=\"{0}\" style=\"display:none\">", ClientId);

            writer.Write("</font>", TagName);
        }

        protected override void WriteChildren(HtmlTextWriter writer)
        {
            base.WriteChildren(writer);

            String value2Write = "";
            IList<R> list = null;

            if (DataSource != null && ElementCreator != null)
            {
                list = DataSource();
            }
            if (AddDefaultOption && IsEnabled)
            {
                MVCViewOption defOpt = new MVCViewOption();
                defOpt.Selected = SelectedValue == null;
                defOpt.Text = DefaultOptionText;
                defOpt.Value = String.Empty;
                defOpt.Render(writer);
                value2Write = DefaultOptionText;
                if (SelectedValue != null && SelectedValue is IList)
                {
                    IList values = (IList)SelectedValue;
                    if (values.Count > 0)
                    {
                        value2Write = "";
                    }
                }
            }

            if (list != null)
            {
                int countSelectedVals = 0;
                if (list != null && list.Count > 0)
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        MVCViewOption elem = ElementCreator(list[i], Parent, i);
                        elem.ParentDefaultOptionText = this.DefaultOptionText;
                        elem.ClientId = ClientId + "_" + i;
                        elem.Controller = this.Controller;
                        
                        if (OptionHiddenValueBind != null)
                        {
                            elem.Value = Convert.ToString(OptionHiddenValueBind(list[i]));
                            //if (IsEnabled && !IsReadOnly && MultiSelection)
                            //    Controller.AddFieldMapping<T>(elem.ClientId, Bind);
                        }
                        if (OptionDisplayValueBind != null)
                        {
                            elem.Text = OptionDisplayValueBind(list[i]);
                        }
                        if (SelectedValue != null && elem.Value != null)
                        {
                            if (SelectedValue is IList)
                            {
                                IList values = (IList)SelectedValue;
                                foreach (Object val in values)
                                {
                                    if (val.ToString().Equals(Convert.ToString(elem.Value)))
                                    {
                                        elem.Selected = true;
                                        if (countSelectedVals > 0)
                                            value2Write += ", ";
                                        value2Write += Helper.NormalizeValue(elem.Text);
                                        countSelectedVals++;
                                    }
                                }
                            }
                            else if (SelectedValue.ToString().Equals(Convert.ToString(elem.Value)))
                            {
                                elem.Selected = true;
                                value2Write = Helper.NormalizeValue(elem.Text);
                            }
                        }
                        elem.Render(writer);
                        
                    }
                }
            }

        }
    }

}
