﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;
using System.Web.UI;
using Library.Mvc.Triggers;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewLookup<T,R> : MVCViewInputElement<R>
    {
        private R _selectedValue;
        private Func<Boolean, String, String> _displayTextValue;
        private MVCViewAutoComplete<T> _autoComplete = new MVCViewAutoComplete<T>();
        private MVCViewElementTriggerCollection _onSelect;
        private MVCViewElementTriggerCollection _loadDataSourceTrigger;
   

        public MVCViewAutoComplete<T> AutoComplete
        {
            get
            {
                return _autoComplete;
            }
        }

        protected override string TagName
        {
            get
            {
                return "span";
            }
        }

        public virtual R SelectedValue
        {
            get
            {
                if (HiddenField != null)
                {
                    _selectedValue = HiddenField.Bind(false, default(R));
                }
                return _selectedValue;
            }
            set
            {
                _selectedValue = value;
            }
        }

        protected override bool HasChildren
        {
            get
            {
                return false;
            }
        }

        public virtual Func<IList<T>> DataSource { get; set; }

        public Func<T, Dictionary<String, String>> DataKeyValues { get; set; }

        public virtual Func<T, IMVCViewElement, int, IList<IMVCViewElement>> ElementCreator { get; set; }

        public MVCViewHidden<R> HiddenField { get; set; }

        public MVCViewTextBox TextBox { get; set; }

        public MVCViewButton Button { get; set; }

        public override System.Func<bool, bool, bool> IsEnabledValue
        {
            get
            {
                return base.IsEnabledValue;
            }
            set
            {
                base.IsEnabledValue = value;
                TextBox.IsEnabledValue = value;
                Button.IsEnabledValue = value;
                HiddenField.IsEnabledValue = value;
            }
        }

        public override Func<bool, bool, bool> IsReadOnlyValue
        {
            get
            {
                return base.IsReadOnlyValue;
            }
            set
            {
                base.IsReadOnlyValue = value;
                TextBox.IsReadOnlyValue = value;
                Button.IsReadOnlyValue = value;
                HiddenField.IsReadOnlyValue = value;
            }
        }

        public MVCViewElementTriggerCollection LoadDataSourceTrigger
        {
            get
            {
                if (_loadDataSourceTrigger == null)
                {
                    _loadDataSourceTrigger = new MVCViewElementTriggerCollection();
                }

                return _loadDataSourceTrigger;
            }
        }

        public MVCViewElementTriggerCollection OnSelect
        {
            get
            {
                if (_onSelect == null)
                {
                    _onSelect = new MVCViewElementTriggerCollection();
                }

                return _onSelect;
            }
        }

        public void InitializeAutoComplete()
        {
            _autoComplete.ElementCreator = ElementCreator;
            _autoComplete.Id = this.Id + "Autocomplete";
            _autoComplete.ClientId = this.ClientId + "Autocomplete";
            _autoComplete.RenderInvisible = true;
            _autoComplete.DataSource = DataSource;
            _autoComplete.DataKeyValues = DataKeyValues;
        }

        protected override void RenderInternal(System.Web.UI.HtmlTextWriter writer)
        {
            writer.Write("<div id=\"{0}_Container\">", this.ClientId);

            if (IsEnabled)
            {
                RenderScript(writer);

                if (Button != null)
                {
                    Button.CssClass = "lookup-button input-group-addon";
                    Button.IconCssClass = "fa fa-search";
                    Button.Render(writer);
                }

                if (!String.IsNullOrEmpty(TextBox.Value))
                {
                    CssClass += " edited";
                }
                writer.Write("<input");
                if (OnSelect.Count > 0)
                    TextBox.OnChange.AddRange(OnSelect);
                TextBox.WriteAttributes(writer);
                WriteAttribute(HtmlTextWriterAttribute.Class, this.CssClass, writer);
                writer.Write("/>");


                HiddenField.Render(writer);



                InitializeAutoComplete();
                _autoComplete.Render(writer);

                Controller.AddFieldMapping(TextBox.ClientId, TextBox.Bind);
            }
            else
            {
                TextBox.IsEnabled = false;
                if (!String.IsNullOrEmpty(TextBox.Value))
                {
                    CssClass += " edited";
                }
                writer.Write("<input");
                TextBox.WriteAttributes(writer);
                WriteAttribute(HtmlTextWriterAttribute.Class, this.CssClass, writer);
                writer.Write("/>");
            }


            writer.Write("</div>");
        }

        private void RenderScript(HtmlTextWriter writer)
        {
            MVCViewElementCallActionTrigger callAction = null;
            if (LoadDataSourceTrigger.Count > 0)
            {
                if (LoadDataSourceTrigger.Count > 1)
                {
                    throw new NotSupportedException("Only one trigger is allowed and must be call action");
                }
                callAction = (MVCViewElementCallActionTrigger)LoadDataSourceTrigger.First();
            }
            if (callAction != null)
            {
                writer.Write("<script type=\"text/javascript\" id=\"{0}_Script\">", this.ClientId);
                writer.WriteLine("$(function() {");
                writer.WriteLine("     $('#{0}').autocomplete('{1}',", TextBox.ClientId, Microsoft.JScript.GlobalObject.escape(TriggerHelper.CreateUrl(this.Controller.ModelKey, this.Controller.AreaName, this.Controller.ControllerName, callAction.ActionName, callAction.Arguments)));
                writer.Write("{");
                writer.WriteLine("  maxItemsToShow: 10,");
                writer.WriteLine("  onItemSelect: function(data){ lookupSelectItem(data, '" + TextBox.ClientId + "', '" + HiddenField.ClientId + "');");
                foreach (MVCViewElementTrigger onClickTrigg in OnSelect)
                {
                    onClickTrigg.Controller = this.Controller;
                    onClickTrigg.Render(this, writer, "onselect");
                }
                writer.Write("}");
                writer.WriteLine("});});");
                writer.Write("</script>");
            }
        }
    }
}
