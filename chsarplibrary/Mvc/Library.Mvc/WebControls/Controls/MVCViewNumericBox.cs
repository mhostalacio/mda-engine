﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;
using System.Web.UI;
using Library.Util.Transactions;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewNumericBox<T> : MVCViewTextBoxBase<T> 
    {
        public NumericBoxMask Mask { get; set; }

        public int? NumberOfDecimalPlaces { get; set; }

        public int? MaxLentgh { get; set; }

        public String CustomMask { get; set; }

        public override void WriteAttributes(HtmlTextWriter writer)
        {
            base.WriteAttributes(writer);

            WriteAttribute(HtmlTextWriterAttribute.Type, "text", writer);

            if (MaxLength.HasValue)
            {
                WriteAttribute(HtmlTextWriterAttribute.Maxlength, MaxLength.Value.ToString(), writer);
            }

            if (Value != null)
            {
                WriteAttribute(HtmlTextWriterAttribute.Value, Convert.ToString(Value).Replace(".", BusinessTransaction.CurrentContext.CurrentCulture.NumberFormat.NumberDecimalSeparator), writer);
            }
        }

        protected override void RenderInternal(HtmlTextWriter writer)
        {
            if (Value != null && !String.IsNullOrEmpty(Convert.ToString(Value)))
            {
                CssClass += " edited";
            }
            base.RenderInternal(writer);

            writer.Write("<script type=\"text/javascript\" id=\"{0}_Script\">", this.ClientId);
            writer.Write("$(document).ready(function(){");
            if (this.Mask == NumericBoxMask.INTEGER)
            {
                writer.Write("$('#{0}').mask('0000000000');", ClientId);
            }
            else if (this.Mask == NumericBoxMask.DECIMAL)
            {
                writer.Write("$('#{0}').mask('000.000.000.000.000,00', {reverse: true});", ClientId);
            }
            else if (this.Mask == NumericBoxMask.MONEY)
            {
                writer.Write("$('#{0}').mask('#.##0,00', {reverse: true});", ClientId);
            }
            else if (this.Mask == NumericBoxMask.CUSTOM)
            {
                writer.Write("$('#{0}').mask('{1}');", ClientId, CustomMask);
            }
            writer.Write("});");
            writer.Write("</script>");
        }

        protected override void RenderMarker(HtmlTextWriter writer)
        {
            writer.Write("<font id=\"{0}\" style=\"display:none\">", ClientId);

            writer.Write("</font>");
        }

        
    }

    public enum NumericBoxMask
    {
        INTEGER,
        MONEY,
        DECIMAL,
        CUSTOM
    }
}
