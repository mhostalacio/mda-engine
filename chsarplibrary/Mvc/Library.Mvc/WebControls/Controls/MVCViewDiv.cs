﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewDiv : MVCViewCompositeElement<IMVCViewElement>
    {
        #region Properties
        
        protected override string TagName
        {
            get
            {
                return "div";
            }
        }

        #endregion

        #region Static Methods

        public static MVCViewDiv Clearfix()
        {
            return new MVCViewDiv() { CssClass = "clearfix" };
        }

        #endregion
    }
}
