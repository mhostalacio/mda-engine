﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;
using System.Web.UI;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewTextBox : MVCViewTextBoxBase<String>
    {
        public virtual TextBoxInputType InputType
        {
            get;
            set;
        }

        public String CustomMask { get; set; }


        public override void WriteAttributes(HtmlTextWriter writer)
        {
            base.WriteAttributes(writer);

            WriteAttribute(HtmlTextWriterAttribute.Type, InputType.ToString().ToLower(), writer);

            if (this.MaxLength.HasValue)
            {
                WriteAttribute(HtmlTextWriterAttribute.Maxlength, MaxLength.Value.ToString(), writer);
            }

            WriteAttribute(HtmlTextWriterAttribute.Value, Value, writer);

            WriteAllowHtmlContentAttribute(writer);
        }

        protected override void RenderInternal(HtmlTextWriter writer)
        {
            if (!String.IsNullOrEmpty(Value))
            {
                CssClass += " edited";
            }
            base.RenderInternal(writer);

            if (!String.IsNullOrEmpty(this.CustomMask))
            {
                writer.Write("<script type=\"text/javascript\" id=\"{0}_Script\">", this.ClientId);
                writer.Write("$(document).ready(function(){");
                writer.Write("$('#{0}').mask('{1}');", ClientId, CustomMask);
                writer.Write("});");
                writer.Write("</script>");
            }
            
        }

        protected override void RenderMarker(HtmlTextWriter writer)
        {
            writer.Write("<font id=\"{0}\" style=\"display:none\">", ClientId);

            writer.Write("</font>");
        }
    }

    public enum TextBoxInputType
    {
        TEXT,
        PASSWORD,
        SEARCH,
    }
}
