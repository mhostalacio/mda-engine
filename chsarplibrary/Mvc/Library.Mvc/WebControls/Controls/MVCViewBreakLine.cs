﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewBreakLine : MVCViewElement
    {
        #region Properties

        protected override string TagName
        {
            get { return "br"; }
        }

        protected override bool HasChildren
        {
            get { return false; }
        }

        protected override MVCViewElementTag TagType
        {
            get { return MVCViewElementTag.NoEndingTag; }
        }

        #endregion

        #region Static Methods

        public static MVCViewBreakLine New()
        {
            return new MVCViewBreakLine();
        }

        #endregion
    }
}
