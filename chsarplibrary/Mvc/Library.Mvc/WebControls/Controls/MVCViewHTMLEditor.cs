﻿using Library.Mvc.WebControls.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewHTMLEditor : MVCViewTextBoxBase<String>
    {
        private List<IMVCViewHTMLEditorButton> _customButtons = new List<IMVCViewHTMLEditorButton>();

        protected override string TagName
        {
            get
            {
                return InlineEditor ?  "input" : "textarea";
            }
        }

        public List<IMVCViewHTMLEditorButton> CustomButtons
        {
            get
            {
                return _customButtons;
            }
            set
            {
                _customButtons = value;
            }
        }

        public Boolean InlineEditor { get; set; }

        protected override void RenderInternal(System.Web.UI.HtmlTextWriter writer)
        {
            writer.Write("<div id=\"{0}_Container\" class=\"html-editor\">",  this.ClientId);
            base.RenderInternal(writer);
            if (InlineEditor)
            {
                writer.Write(Value);
            }
            writer.Write("</div>");
        }

        protected override void RenderMarker(HtmlTextWriter writer)
        {
            writer.Write("<font id=\"{0}_Container\" style=\"display:none\">", ClientId);

            writer.Write("</font>");
        }

        public override void WriteAttributes(HtmlTextWriter writer)
        {
            base.WriteAttributes(writer);

            if (!InlineEditor)
                WriteAttribute("htmlCtnt", "1", writer);

            if (InlineEditor)
                WriteAttribute("type", "hidden", writer);

            if (InlineEditor)
                WriteAttribute("value", Microsoft.JScript.GlobalObject.escape(Value), writer);
        }

        protected override bool HasChildren
        {
            get
            {
                return true;
            }
        }

        protected override void WriteChildren(HtmlTextWriter writer)
        {
            base.WriteChildren(writer);

            if (!InlineEditor && !String.IsNullOrEmpty(Value))
            {
                writer.Write(Helper.NormalizeValue(Value, RenderMode));
            }
        }

        protected override void WriteScripts(System.Web.UI.HtmlTextWriter writer)
        {
            writer.Write("<script type=\"text/javascript\">");
            writer.Write("  tinymce.init({");
            
            if (InlineEditor)
            {
                writer.Write("  selector: \"#{0}_Container > .editable\",", this.ClientId);
            }
            else
            {
                writer.Write("  selector: \"#{0}\",", this.ClientId);
            }
           
            
            writer.Write("  menubar: false,");
            writer.Write("  statusbar: false,");
            if (!string.IsNullOrWhiteSpace(Library.Util.Transactions.BusinessTransaction.CurrentContext.SessionInfo.LanguageCode) &&
                !Library.Util.Transactions.BusinessTransaction.CurrentContext.SessionInfo.LanguageCode.Equals("en-US", StringComparison.InvariantCultureIgnoreCase))
            {
                writer.Write("  language: '" + Library.Util.Transactions.BusinessTransaction.CurrentContext.SessionInfo.LanguageCode.Replace("-", "_") + "',");
            }
            writer.Write("  theme: \"modern\",");
            writer.Write("  height: 350,");
            writer.Write("  inline: {0},", InlineEditor ? "true": "false");
            //writer.Write("  encoding: \"xml\",");
            writer.Write("  alignH: [5,5,5,5],");
            //writer.Write("  plugins: [");
            //writer.Write("    \"advlist autolink lists link image charmap print preview anchor\",");
            //writer.Write("    \"searchreplace visualblocks code fullscreen\",");
            //writer.Write("    \"insertdatetime media table contextmenu paste\"");
            //writer.Write("  ],");
            //writer.Write("  toolbar: \"undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image");
            writer.Write("  toolbar: \"bold italic | bullist numlist outdent indent");
            if (CustomButtons.Count > 0)
            {
                writer.Write(" | ");
                foreach (IMVCViewHTMLEditorButton button in CustomButtons)
                {
                    writer.Write(button.ClientId + " ");
                }
            }
            writer.Write("\",");
            writer.WriteLine("  setup: function(editor) {");
            writer.Write("    editor.on(\"SaveContent\", function(i) {");
            writer.Write("       i.content = i.content.replace(/&#39/g, \"&apos\");");
            writer.Write("    });");
            if (CustomButtons.Count > 0)
            {
               
                foreach (IMVCViewHTMLEditorButton button in CustomButtons)
                {
                    writer.WriteLine("  editor.addButton('" + button.ClientId + "', {");
                    writer.Write("     text: '" + button.Text + "',");
                    writer.Write("     icon: false,");
                    writer.Write("     onclick: function() {");
                    writer.Write("     " + button.JQueryFunction);
                    writer.Write("     }");
                    writer.Write("  });");
                }
                
            }

            writer.Write("  }");

            writer.Write("  });");

            writer.Write("</script>");
        }
    }
}
