﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;
using System.Web.UI;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewLabel<T> : MVCViewSpan<T>
    {
        protected override string TagName
        {
            get
            {
                return "label";
            }
        }

        

        /// <summary>
        /// Element linked to this label.
        /// </summary>
        public Func<IMVCViewElement> For { get; set; }


        public override void WriteAttributes(System.Web.UI.HtmlTextWriter writer)
        {
            base.WriteAttributes(writer);

            if (For != null && For() != null)
            {
                WriteAttribute(HtmlTextWriterAttribute.For, For().ClientId, writer);
            }
        }

        

      
    }
}
