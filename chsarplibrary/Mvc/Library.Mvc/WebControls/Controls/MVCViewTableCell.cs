﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;
using System.Web.UI;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewTableCell : MVCViewCompositeElement<IMVCViewElement>
    {
        protected override string TagName
        {
            get
            {
                return this.Type == TableRowType.Header ? "th" : "td";
            }
        }

        public Func<int> Colspan { get; set; }

        public Func<int> Rowspan { get; set; }

        public TableRowType Type { get; set; }

        public override void WriteAttributes(System.Web.UI.HtmlTextWriter writer)
        {
            base.WriteAttributes(writer);

            if (Colspan != null)
            {
                WriteAttribute(HtmlTextWriterAttribute.Colspan, Colspan().ToString(), writer);
            }
            if (Rowspan != null)
            {
                WriteAttribute(HtmlTextWriterAttribute.Rowspan, Rowspan().ToString(), writer);
            }
        }
    }
}
