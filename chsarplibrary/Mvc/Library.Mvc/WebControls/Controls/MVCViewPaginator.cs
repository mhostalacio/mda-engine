﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;
using Library.Util.Collections;
using Library.Mvc.Triggers;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewPaginator<T> : MVCViewElement
    {
        private int _numberOfPagesToShow = 3;

        public int DataSourceChunkSize { get; set; }

        public int PageSize { get; set; }

        public virtual Func<PagedList<T>> DataSource { get; set; }

        protected override string TagName
        {
            get
            {
                return "table";
            }
        }

        protected override bool HasChildren
        {
            get
            {
                return true;
            }
        }

        protected override void WriteChildren(System.Web.UI.HtmlTextWriter writer)
        {
            if (DataSource != null)
            {
                writer.Write("<tr class=\"PaginatorNumberedRow\">");
                PagedList<T> source = DataSource();
                if (source != null)
                {
                    if (source.Count > PageSize)
                    {
                        int numberOfKnownPages = source.Count / PageSize;
                        numberOfKnownPages = numberOfKnownPages + (source.Count % PageSize == 0 ? 0 : 1);
                        int beginPage = 0;
                        if (source.CurrentPage > _numberOfPagesToShow)
                        {
                            writer.Write("<td class=\"PaginatorPreviousCell\">");
                            MVCViewButton but = GetPageButton(beginPage - 1);
                            but.Text = "...";
                            but.Render(writer);
                            writer.Write("</td>");
                            beginPage = source.CurrentPage - _numberOfPagesToShow;
                        }
                        int endPage = numberOfKnownPages;
                        if (numberOfKnownPages > _numberOfPagesToShow * 2)
                        {
                            endPage = _numberOfPagesToShow * 2;
                        }
                        for (int i = beginPage; i < endPage; i++)
                        {
                            if (i == source.CurrentPage)
                            {
                                writer.Write("<td><a class=\"PaginatorNumberedSelectedCell\">{0}</a></td>", i + 1);
                            }
                            else
                            {
                                writer.Write("<td class=\"PaginatorNumberedCell\">");
                                MVCViewButton but = GetPageButton(i);
                                but.Render(writer);
                                writer.Write("</td>");
                            }
                        }
                        if (source.HasMoreEntities)
                        {
                            writer.Write("<td class=\"PaginatorNextCell\">");
                            MVCViewButton but = GetPageButton(endPage);
                            but.Text = "...";
                            but.Render(writer);
                            writer.Write("</td>");
                        }
                    }
                }
                writer.Write("</tr>");
            }
        }

        protected MVCViewElementCallActionTrigger GetTrigger(int pageNumber)
        {
            MVCViewElementCallActionTrigger trigger = new MVCViewElementCallActionTrigger();
            trigger.Controller = this.Controller;
            trigger.ControllerName = this.Controller.ControllerName;
            trigger.AreaName = this.Controller.AreaName;
            trigger.ActionName = this.ClientId + "GoToPage";
            trigger.UseAjax = true;
            trigger.Arguments = new MVCViewArgumentCollection();
            MVCViewLiteralArgumentValue arg = new MVCViewLiteralArgumentValue();
            arg.Value = pageNumber.ToString();
            trigger.Arguments.Add(new MVCViewArgument() { Name = "PageNumber", Value = arg });
            return trigger;
        }

        protected MVCViewButton GetPageButton(int pageNumber)
        {
            MVCViewButton but = new MVCViewButton();
            but.Controller = this.Controller;
            but.Id = String.Format("{0}_PAGE_{1}", this.Id, pageNumber);
            but.CssClass = "btn PaginatorButton";
            but.Text = (pageNumber + 1).ToString();
            but.OnClick.Add(GetTrigger(pageNumber));
            return but;
        }

           
    }
}
