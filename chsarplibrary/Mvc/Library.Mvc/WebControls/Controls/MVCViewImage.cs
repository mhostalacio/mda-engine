﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewImage : MVCViewElement<String>
    {
        protected override string TagName
        {
            get
            {
                return "img";
            }
        }

        private String _source;
        public String Source
        {
            get
            {
                if (Bind != null)
                {
                    _source = Bind(false, null);
                }
                return _source;
            }
            set
            {
                _source = value;
            }
        }

        public Boolean ConcatAppPathToSource { get; set; }

        public override void WriteAttributes(System.Web.UI.HtmlTextWriter writer)
        {
            base.WriteAttributes(writer);

            if (Source != null)
            {
                String pathPrefix = "";
                if (ConcatAppPathToSource)
                {
                    pathPrefix = Controller.GetApplicationPath();
                }
                WriteAttribute(System.Web.UI.HtmlTextWriterAttribute.Src, pathPrefix + Source, writer);
            }
        }
    }
}
