﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;
using System.Web.UI;
using System.Web;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewTextArea : MVCViewTextBoxBase<String>
    {
        #region Fields

        private int? _rows;
        private int? _cols;

        #endregion

        #region Properties

        /// <summary>
        /// TextBox value.
        /// </summary>
        public virtual int? Rows
        {
            get
            {
                return _rows;
            }
            set
            {
                _rows = value;
            }
        }

        /// <summary>
        /// TextBox value.
        /// </summary>
        public virtual int? Cols
        {
            get
            {
                return _cols;
            }
            set
            {
                _cols = value;
            }
        }

        protected override bool HasChildren
        {
            get { return true; }
        }

        protected override string TagName
        {
            get
            {
                return "textarea";
            }
        }

        #endregion

        #region Methods

     

        public override void WriteAttributes(HtmlTextWriter writer)
        {
            base.WriteAttributes(writer);

            if (IsEnabled && Rows != null && Rows.HasValue)
            {
                WriteAttribute(HtmlTextWriterAttribute.Rows, Rows.Value.ToString(), writer);
            }

            if (IsEnabled && Cols != null && Cols.HasValue)
            {
                WriteAttribute(HtmlTextWriterAttribute.Cols, Cols.Value.ToString(), writer);
            }

            if (IsEnabled && MaxLength != null && MaxLength.HasValue)
            {
                WriteAttribute(HtmlTextWriterAttribute.Maxlength, MaxLength.Value.ToString(), writer);

                WriteAttribute("onkeyup", "if($.browser.msie && $.browser.version.substring(0,$.browser.version.indexOf('.')) < 10 && this.value.length > this.attributes.maxlength.value){this.value = this.value.substring(0,this.attributes.maxlength.value)};", writer);
            }


            WriteAllowHtmlContentAttribute(writer);
        }

        protected override void WriteChildren(HtmlTextWriter writer)
        {
            base.WriteChildren(writer);

            if (!String.IsNullOrEmpty(Value))
            {
                writer.Write(Helper.NormalizeValue(Value, RenderMode));
            }
        }

        protected override void RenderInternal(HtmlTextWriter writer)
        {
            writer.Write("<div class=\"input-control textarea\" id=\"{0}_Container\">", ClientId);
            base.RenderInternal(writer);
            writer.Write("</div>");

        }

        protected override void RenderMarker(HtmlTextWriter writer)
        {
            writer.Write("<div  id=\"{0}_Container\" style=\"display:none\">", ClientId);
            writer.Write("</div>");
        }

        #endregion
    }
}
