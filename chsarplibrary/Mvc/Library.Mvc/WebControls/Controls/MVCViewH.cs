﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;
using System.Web.UI;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewH<T> : MVCViewSpan<T>
    {
       
    }

    public class MVCViewH1<T> : MVCViewH<T>
    {
        protected override string TagName
        {
            get
            {
                return "h1";
            }
        }
    }

    public class MVCViewH2<T> : MVCViewH<T>
    {
        protected override string TagName
        {
            get
            {
                return "h2";
            }
        }
    }

    public class MVCViewH3<T> : MVCViewH<T>
    {
        protected override string TagName
        {
            get
            {
                return "h3";
            }
        }
    }

    public class MVCViewH4<T> : MVCViewH<T>
    {
        protected override string TagName
        {
            get
            {
                return "h4";
            }
        }
    }

    public class MVCViewH5<T> : MVCViewH<T>
    {
        protected override string TagName
        {
            get
            {
                return "h5";
            }
        }
    }

    public class MVCViewH6<T> : MVCViewH<T>
    {
        protected override string TagName
        {
            get
            {
                return "h6";
            }
        }
    }
}
