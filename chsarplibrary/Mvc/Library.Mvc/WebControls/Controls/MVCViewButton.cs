﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;
using System.Web.UI;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewButton : MVCViewInputElement<String>
    {
        #region Fields

        private String _text;
        private List<IMVCViewElement> _childElements = new List<IMVCViewElement>();

        #endregion

        #region Properties

        /// <summary>
        /// Button text.
        /// </summary>
        public String Text
        {
            get
            {
                if (Bind != null)
                    _text = Bind(false, null);
                return _text;
            }
            set
            {
                _text = value;
            }
        }

        protected override string TagName
        {
            get
            {
                return "button";
            }
        }

        protected override bool HasChildren
        {
            get
            {
                return true;
            }
        }

        public List<IMVCViewElement> ChildElements
        {
            get { return _childElements; }
        }

        #endregion

        #region Methods

        public override void WriteAttributes(HtmlTextWriter writer)
        {
            base.WriteAttributes(writer);

            WriteAttribute(HtmlTextWriterAttribute.Type, "button", writer);

            //if (!String.IsNullOrEmpty(Text))
            //{
            //    WriteAttribute(HtmlTextWriterAttribute.Value, Text, writer);
            //}
        }

        protected override void WriteChildren(HtmlTextWriter writer)
        {
            base.WriteChildren(writer);

            writer.Write(Helper.NormalizeValue(Text));

            if (this.ChildElements.Count > 0)
            {
                foreach (IMVCViewElement elem in this.ChildElements)
                {
                    elem.Render(writer);
                }
            }
        }

        protected override void WriteHelpTextAttribute(HtmlTextWriter writer)
        {
            if (HelpText != null)
            {
                WriteAttribute(HtmlTextWriterAttribute.Title, HelpText, writer);
            }
        }

        protected override void RenderInternal(HtmlTextWriter writer)
        {
            if (ViewMode != MVCViewElementViewMode.Print)
                base.RenderInternal(writer);
        }

        protected override void RenderMarker(HtmlTextWriter writer)
        {
            writer.Write("<font id=\"{0}\" style=\"display:none\">", ClientId);

            writer.Write("</font>");
        }

        #endregion
    }
}
