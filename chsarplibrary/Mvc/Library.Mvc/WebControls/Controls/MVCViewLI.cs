﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.WebControls.Base;

namespace Library.Mvc.WebControls.Controls
{
    public class MVCViewLI : MVCViewCompositeElement<IMVCViewElement>
    {
        protected override string TagName
        {
            get
            {
                return "li";
            }
        }
    }
}
