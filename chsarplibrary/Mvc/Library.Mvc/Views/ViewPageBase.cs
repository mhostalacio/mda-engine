﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.Models;
using Library.Mvc.WebControls.Controls;
using Library.Mvc.Controllers;

namespace Library.Mvc.Views
{
    public abstract class ViewPageBase : System.Web.Mvc.ViewPage
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitControls();
        }

        protected virtual void InitControls()
        {
        }

        public String FormPostAction()
        {
            return this.ViewContext.Controller.ControllerContext.RequestContext.HttpContext.Request.Url.LocalPath;
        }
    }

    public abstract class ViewPageBase<T> : ViewPageBase
        where T : IMVCModel
    {
        public MVCControllerBase Controller
        {
            get
            {
                return (MVCControllerBase)this.ViewContext.Controller;
            }
        }
    }
}
