﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace Library.Mvc.Views
{
    public class CustomWebFormViewEngine : WebFormViewEngine
    {
        public CustomWebFormViewEngine()
        {
            MasterLocationFormats = new[] {
                                              // Theme-specific location
                                              "~/Skins/{0}.master",
                                              // Default locations
                                              "~/Shared/{0}.master"
                                          };

            AreaViewLocationFormats = new[] { 
                                                "~/Areas/{2}/Views/{1}/{0}.aspx", 
                                                "~/Areas/{2}/Views/{1}/{0}.ascx", 
                                                "~/Areas/{2}/Views/Shared/{0}.aspx", 
                                                "~/Areas/{2}/Views/Shared/{0}.ascx", 
                                                "~/Shared/{0}.ascx", 
                                                "~/Shared/{0}.aspx", 
                                            };
            PartialViewLocationFormats = new[] { 
                                                "~/Shared/{0}.ascx", 
                                                "~/Shared/{0}.aspx", 
                                                "~/Views/{1}/{0}.ascx",
                                                "~/Areas/Shared/Views/{0}.ascx",
                                            };

        }
    }
}
