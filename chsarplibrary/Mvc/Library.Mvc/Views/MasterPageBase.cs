﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Util.Transactions;
using System.Web.UI.HtmlControls;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using Library.Mvc.Controllers;

namespace Library.Mvc.Views
{
    public class MasterPageBase : System.Web.Mvc.ViewMasterPage
    {
        private const string FormatCSS = "\n<link href=\"{0}\" rel=\"stylesheet\" type=\"text/css\" />";
        private const string FormatJS = "\n<script type=\"text/javascript\" language=\"javascript\" src=\"{0}\"></script>";

        public MVCControllerBase Controller
        {
            get
            {
                return (MVCControllerBase)this.ViewContext.Controller;
            }
        }

        public BusinessTransaction CurrentContext
        {
            get
            {
                return BusinessTransaction.CurrentContext;
            }
        }

        protected HtmlHead Head { get; set; }

        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            Head = new HtmlHead();
            AppendMetaDataCssJs(Request, Head, ViewContext);
            base.Render(writer);
            
        }

        public void AppendMetaDataCssJs(HttpRequest request, HtmlHead head, ViewContext Context)
        {
            //string metaTagAndCssKey = GetKey(CurrentContext, request.Browser);
            //StringBuilder sb = new StringBuilder();

            ////CSS
            //sb.Append(String.Format(FormatCSS, GetContentURL("\\Styles\\jquery-ui-1.8.2.custom.css")));
            //sb.Append(String.Format(FormatCSS, GetContentURL("\\Styles\\jquery.ui.dialog.css")));
            //sb.Append(String.Format(FormatCSS, GetContentURL("\\Styles\\styles.css")));

            ////JS
            //sb.Append(String.Format(FormatJS, GetContentURL("\\Scripts\\date.js")));
            //sb.Append(String.Format(FormatJS, GetContentURL("\\Scripts\\global.custom.js")));
            //sb.Append(String.Format(FormatJS, GetContentURL("\\Scripts\\jquery-2.0.3.min.js")));
            //sb.Append(String.Format(FormatJS, GetContentURL("\\Scripts\\jquery.blockUI.js")));
            //sb.Append(String.Format(FormatJS, GetContentURL("\\Scripts\\jquery.ui.widget.js")));
            //sb.Append(String.Format(FormatJS, GetContentURL("\\Scripts\\ui.base.elements.js")));
            //sb.Append(String.Format(FormatJS, GetContentURL("\\Scripts\\ui.datetimebox.js")));
            //sb.Append(String.Format(FormatJS, GetContentURL("\\Scripts\\ui.forms.actions.js")));

            //this.Controls.Add(new LiteralControl() { Text = sb.ToString() });
        }

        private String GetContentURL(String path)
        {
            return HttpContext.Current.Request.ApplicationPath + path;
        }
    }
}
