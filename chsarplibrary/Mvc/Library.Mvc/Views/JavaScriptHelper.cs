﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Util.Transactions;
using System.Globalization;
using Library.Mvc.Controllers;

namespace Library.Mvc.Views
{
    public static class JavaScriptHelper
    {
        private static string groupCodeDot = "Global_Calendar_";
        private static string[] weekDayCodes = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
        private static string[] monthCodes = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };


        public static String JavascriptButtonText(MVCControllerBase ctrl)
        {
            return "TODO";
        }

        public static String GetDateFormatToJQueryUI(String currentFormat = null)
        {
            if (currentFormat == null)
                currentFormat = BusinessTransaction.CurrentContext.CurrentCulture.DateTimeFormat.ShortDatePattern;
            switch (currentFormat)
            {
                case "M/d/yyyy":
                    return "m/d/yyyy";

                case "MM/dd/yyyy":
                    return "mm/dd/yyyy";

                case "M-d-yyyy":
                    return "m-d-yyyy";

                case "dd-MM-yyyy":
                    return "dd-mm-yyyy";

                case "dd/MM/yyyy":
                case "d/MM/yyyy":
                case "d/M/yyyy":
                    return "dd/mm/yyyy";

                case "d.M.yyyy":
                case "d.M.yyyy.":
                case "d.M.yyyy 'г.'":
                case "d. M. yyyy":
                case "dd.MM.yyyy":
                    return "dd.mm.yyyy";

                case "yyyy.MM.dd":
                case "yyyy.MM.dd.":
                    return "yyyy.mm.dd";

                case "yyyy-MM-dd":
                    return "yyyy-mm-dd";

                case "yyyy/MM/dd":
                case "yyyy/MM/d":
                case "yyyy/M/d":
                    return "yyyy/mm/dd";
            }
            throw new NotSupportedException("Date format not supported \"" + currentFormat + "\"");
        }

        public static String GetDateTimeFormatToJQueryUI(String currentFormat = null)
        {
            if (currentFormat == null)
                currentFormat = string.Format("{0} {1}", BusinessTransaction.CurrentContext.CurrentCulture.DateTimeFormat.ShortDatePattern, BusinessTransaction.CurrentContext.CurrentCulture.DateTimeFormat.ShortTimePattern);
            switch (currentFormat)
            {
                case "M/d/yyyy h:mm tt":
                    return "m/d/yyyy hh:ii";

                case "MM/dd/yyyy HH:mm":
                    return "mm/dd/yyyy hh:ii";

                case "M-d-yyyy h:mm tt":
                    return "m-d-yyyy hh:ii";

                case "dd/MM/yyyy HH:mm":
                case "dd/MM/yyyy hh:mm tt":
                case "dd/MM/yyyy h:mm tt":
                case "d/MM/yyyy h:mm tt":
                case "d/M/yyyy h:mm tt":
                case "dd/MM/yyyy H:mm tt":
                    return "dd/mm/yyyy hh:ii";

                case "dd-MM-yyyy HH:mm":
                    return "dd-mm-yyyy hh:ii";

                case "dd/MM/yyyy H:mm":
                    return "dd/mm/yyyy hh:ii";

                case "d.M.yyyy H:mm":
                case "d.M.yyyy. H:mm":
                case "d.M.yyyy 'г.' HH:mm 'ч.'":
                case "d. M. yyyy H.mm 'goź.'":
                case "d. M. yyyy H.mm 'hodź.'":
                case "dd.MM.yyyy HH:mm":
                    return "dd.mm.yyyy hh:ii";

                case "yyyy.MM.dd H:mm":
                case "yyyy.MM.dd. H:mm":
                    return "yyyy.mm.dd hh:ii";

                case "yyyy-MM-dd H:mm":
                case "yyyy-MM-dd H:mm tt":
                case "yyyy-MM-dd tt h:mm":
                case "yyyy-MM-dd HH:mm":
                    return "yyyy-mm-dd hh:ii";

                case "yyyy/MM/dd hh:mm tt":
                case "yyyy/MM/d hh:mm tt":
                case "yyyy/M/d hh:mm tt":
                    return "yyyy/mm/dd hh:ii";

            }

            throw new NotSupportedException("DateTime format not supported \"" + currentFormat + "\"");
        }

        public static String JavascriptCloseText(MVCControllerBase ctrl)
        {
            return "TODO";
        }

        public static String JavascriptPreviousText(MVCControllerBase ctrl)
        {
            return "TODO";
        }

        public static String JavascriptNextText(MVCControllerBase ctrl)
        {
            return "TODO";
        }

        public static String JavascriptTodayText(MVCControllerBase ctrl)
        {
            return "TODO";
        }

        public static string JavascriptMonthNames(MVCControllerBase ctrl)
        {
            string monthNamePrefix = "MonthName_";

            string monthNames = string.Format
            (
                "['{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}']",
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + monthNamePrefix + monthCodes[0], monthCodes[0], ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + monthNamePrefix + monthCodes[1], monthCodes[1], ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + monthNamePrefix + monthCodes[2], monthCodes[2], ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + monthNamePrefix + monthCodes[3], monthCodes[3], ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + monthNamePrefix + monthCodes[4], monthCodes[4], ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + monthNamePrefix + monthCodes[5], monthCodes[5], ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + monthNamePrefix + monthCodes[6], monthCodes[6], ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + monthNamePrefix + monthCodes[7], monthCodes[7], ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + monthNamePrefix + monthCodes[8], monthCodes[8], ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + monthNamePrefix + monthCodes[9], monthCodes[9], ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + monthNamePrefix + monthCodes[10], monthCodes[10], ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + monthNamePrefix + monthCodes[11], monthCodes[11], ctrl)
            );

            return monthNames;
        }

        public static string GetGlobalSentenceValue_JavascriptEncode(string sentenceCode, string defaultValue, MVCControllerBase ctrl)
        {
            string sentenceValue = GetGlobalSentenceValue(sentenceCode, defaultValue, ctrl);

            //string escapedValue = Microsoft.JScript.GlobalObject.escape(sentenceValue);
            string escapedValue = System.Web.HttpUtility.JavaScriptStringEncode(sentenceValue);

            return escapedValue;
        }

        public static string GetGlobalSentenceValue(string sentenceCode, string defaultValue, MVCControllerBase ctrl)
        {
            sentenceCode = ctrl.GetTranslation("GlobalSentences", sentenceCode);
            // Default response -> overriden in subclasses in order to use Translator
            return sentenceCode;
        }

        public static string JavascriptMonthNamesShort(MVCControllerBase ctrl)
        {
            string monthNameShortPrefix = "MonthNameShort_";

            string monthNamesShort = string.Format
            (
                "['{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}']",
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + monthNameShortPrefix + monthCodes[0], monthCodes[0].Substring(0, 3), ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + monthNameShortPrefix + monthCodes[1], monthCodes[1].Substring(0, 3), ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + monthNameShortPrefix + monthCodes[2], monthCodes[2].Substring(0, 3), ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + monthNameShortPrefix + monthCodes[3], monthCodes[3].Substring(0, 3), ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + monthNameShortPrefix + monthCodes[4], monthCodes[4].Substring(0, 3), ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + monthNameShortPrefix + monthCodes[5], monthCodes[5].Substring(0, 3), ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + monthNameShortPrefix + monthCodes[6], monthCodes[6].Substring(0, 3), ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + monthNameShortPrefix + monthCodes[7], monthCodes[7].Substring(0, 3), ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + monthNameShortPrefix + monthCodes[8], monthCodes[8].Substring(0, 3), ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + monthNameShortPrefix + monthCodes[9], monthCodes[9].Substring(0, 3), ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + monthNameShortPrefix + monthCodes[10], monthCodes[10].Substring(0, 3), ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + monthNameShortPrefix + monthCodes[11], monthCodes[11].Substring(0, 3), ctrl)
            );

            return monthNamesShort;
        }

        public static string JavascriptDayNames(MVCControllerBase ctrl)
        {
            string dayNamePrefix = "DayName_";

            string dayNames = string.Format("['{0}','{1}','{2}','{3}','{4}','{5}','{6}']",
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + dayNamePrefix + weekDayCodes[0], weekDayCodes[0], ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + dayNamePrefix + weekDayCodes[1], weekDayCodes[1], ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + dayNamePrefix + weekDayCodes[2], weekDayCodes[2], ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + dayNamePrefix + weekDayCodes[3], weekDayCodes[3], ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + dayNamePrefix + weekDayCodes[4], weekDayCodes[4], ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + dayNamePrefix + weekDayCodes[5], weekDayCodes[5], ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + dayNamePrefix + weekDayCodes[6], weekDayCodes[6], ctrl)
            );

            return dayNames;
        }

        public static string JavascriptDayNamesShort(MVCControllerBase ctrl)
        {
            string dayNameShortPrefix = "DayNameShort_";

            string dayNamesShort = string.Format("['{0}','{1}','{2}','{3}','{4}','{5}','{6}']",
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + dayNameShortPrefix + weekDayCodes[0], weekDayCodes[0].Substring(0, 3), ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + dayNameShortPrefix + weekDayCodes[1], weekDayCodes[1].Substring(0, 3), ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + dayNameShortPrefix + weekDayCodes[2], weekDayCodes[2].Substring(0, 3), ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + dayNameShortPrefix + weekDayCodes[3], weekDayCodes[3].Substring(0, 3), ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + dayNameShortPrefix + weekDayCodes[4], weekDayCodes[4].Substring(0, 3), ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + dayNameShortPrefix + weekDayCodes[5], weekDayCodes[5].Substring(0, 3), ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + dayNameShortPrefix + weekDayCodes[6], weekDayCodes[6].Substring(0, 3), ctrl)
            );

            return dayNamesShort;
        }

        public static string JavascriptDayNamesMin(MVCControllerBase ctrl)
        {
            string dayNameMinPrefix = "DayNameMin_";

            string dayNamesMin = string.Format("['{0}','{1}','{2}','{3}','{4}','{5}','{6}']",
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + dayNameMinPrefix + weekDayCodes[0], weekDayCodes[0].Substring(0, 2), ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + dayNameMinPrefix + weekDayCodes[1], weekDayCodes[1].Substring(0, 2), ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + dayNameMinPrefix + weekDayCodes[2], weekDayCodes[2].Substring(0, 2), ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + dayNameMinPrefix + weekDayCodes[3], weekDayCodes[3].Substring(0, 2), ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + dayNameMinPrefix + weekDayCodes[4], weekDayCodes[4].Substring(0, 2), ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + dayNameMinPrefix + weekDayCodes[5], weekDayCodes[5].Substring(0, 2), ctrl),
                GetGlobalSentenceValue_JavascriptEncode(groupCodeDot + dayNameMinPrefix + weekDayCodes[6], weekDayCodes[6].Substring(0, 2), ctrl)
            );

            return dayNamesMin;
        }

        public static string JavascriptWeekText(MVCControllerBase ctrl)
        {
            return GetGlobalSentenceValue_JavascriptEncode("Global_Calendar_Week", "Week", ctrl);
        }

        public static string JavascriptFirstDay(MVCControllerBase ctrl)
        {
            int firstDay = 1;

            return firstDay.ToString();
        }

        public static String TwoLetterISOLanguageName()
        {
            return BusinessTransaction.CurrentContext.CurrentLanguage.TwoLetterISOLanguageName;
        }

        public static String ShortDatePattern()
        {
            return BusinessTransaction.CurrentContext.CurrentCulture.DateTimeFormat.ShortDatePattern;
        }

        public static String GetDateTimePattern()
        {
            return string.Format("{0} {1}", BusinessTransaction.CurrentContext.CurrentCulture.DateTimeFormat.ShortDatePattern, BusinessTransaction.CurrentContext.CurrentCulture.DateTimeFormat.ShortTimePattern);
        }

        public static String GetCultureCode()
        {
            return BusinessTransaction.CurrentContext.CurrentCulture.TwoLetterISOLanguageName;
        }

        public static String JavascriptPleaseWaitText(MVCControllerBase ctrl)
        {
            return "TODO";
        }
    }
}
