﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.Controllers;
using Library.Util.Transactions;
using System.Web.Mvc;

namespace Library.Mvc.Views
{
    public class UserControlBase : System.Web.UI.UserControl
    {
        public MVCControllerBase Controller
        {
            get
            {
                return (MVCControllerBase)((ViewPage)this.Page).ViewContext.Controller;
            }
        }

        public BusinessTransaction CurrentContext
        {
            get
            {
                return BusinessTransaction.CurrentContext;
            }
        }
    }
}
