﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Mvc.Models
{
    /// <summary>
    /// Specifies a contract for model managers.
    /// </summary>
    public interface IModelManager
    {
        /// <summary>
        /// Adds the supplied model.
        /// </summary>
        /// <param name="modelKey">Model key.</param>
        /// <param name="model">Model to add.</param>
        void AddModel(string modelKey, IMVCModel model);

        /// <summary>
        /// Gets the model with the supplied key.
        /// </summary>
        /// <param name="modelKey">Model key.</param>
        /// <returns>Model instance with the supplied key.</returns>
        IMVCModel GetModelInstance(string modelKey);

        /// <summary>
        /// Gets the model with the supplied key.
        /// </summary>
        /// <typeparam name="ModelType">Model instance type.</typeparam>
        /// <param name="modelKey">Model key.</param>
        /// <returns>Model instance with the supplied key.</returns>
        ModelType GetModelInstance<ModelType>(string modelKey) where ModelType : IMVCModel;

        /// <summary>
        /// Removes the model with the supplied key.
        /// </summary>
        /// <param name="modelKey">Model key.</param>
        void RemoveModel(string modelKey);
    }
}
