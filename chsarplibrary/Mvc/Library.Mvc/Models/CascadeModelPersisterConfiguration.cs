﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Mvc.Models
{
    public class CascadeModelPersisterConfiguration
    {
        public virtual TimeSpan TimeToLive { get; set; }

        public virtual IModelPersister Persister { get; set; }
    }
}
