﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Concurrent;
using System.Web.Configuration;
using System.Timers;
using Library.Mvc.Controllers;
using Library.Util;
using Library.Mvc.Session;
using Library.Util.Transactions;
using Library.Util.Exceptions;

namespace Library.Mvc.Models
{
    public class ModelStorage : IModelStorage, IDisposable
    {
        #region Fields

        private Timer _timer;
        private List<CascadeModelPersisterConfiguration> _levels;
        private Dictionary<int, CascadeModelPersisterInstrumentation> _instrumentation = new Dictionary<int, CascadeModelPersisterInstrumentation>();
        private ConcurrentDictionary<String, ModelStorageSessionInfo> _sessions = new ConcurrentDictionary<String, ModelStorageSessionInfo>();
        private ConcurrentDictionary<String, ModelStorageGarbageManager> _garbageCollector = new ConcurrentDictionary<String, ModelStorageGarbageManager>();
        

        //private ILog _logger = LogManager.GetLogger("ModelStorage");

        private bool _disposed = false;

        public const string LOCAL_CACHE_KEY_NAME = "ModelQueue";

        #endregion

        public virtual int MaxRetryCount
        {
            get;
            protected set;
        }

        public IEnumerable<CascadeModelPersisterConfiguration> Levels
        {
            get { return _levels; }
        }

        public IEnumerable<CascadeModelPersisterInstrumentation> Instrumentation
        {
            get { return _instrumentation.Values; }
        }

        public ConcurrentDictionary<String, ModelStorageSessionInfo> Sessions
        {
            get { return _sessions; }
        }

        IEnumerable<ModelStorageSessionInfo> IModelStorage.Sessions
        {
            get { return _sessions.Values; }
        }

        public ModelStorage(IModelPersister persister)
            : this(new List<CascadeModelPersisterConfiguration>(new CascadeModelPersisterConfiguration[] { new CascadeModelPersisterConfiguration { Persister = persister, TimeToLive = TimeSpan.MaxValue } }))
        {
        }

        public ModelStorage(IModelPersister persister, IEnumerable<KeyValuePair<String, ModelStorageSessionInfo>> existingSessions)
            : this(persister)
        {
            foreach (KeyValuePair<String, ModelStorageSessionInfo> existingSession in existingSessions)
            {
                _sessions.TryAdd(existingSession.Key, existingSession.Value);
            }
        }

        public ModelStorage(List<CascadeModelPersisterConfiguration> levels)
            : this(levels, TimeSpan.Zero, 5)
        {

        }

        public ModelStorage(List<CascadeModelPersisterConfiguration> levels, TimeSpan interval, int maxRetryCount)
        {
            this._levels = levels;

            MaxRetryCount = maxRetryCount;

            if (interval > TimeSpan.Zero)
            {
                _timer = new Timer();
                _timer.Interval = interval.TotalMilliseconds;
                _timer.AutoReset = true;
                _timer.Elapsed += new ElapsedEventHandler(OnTimerEllapsed);
                _timer.Start();
            }
        }

        void OnTimerEllapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                _timer.Enabled = false;

                CleanUp();
            }
            catch
            {
                //if (_logger.IsFatalEnabled)
                //{
                //    _logger.Fatal(ex);
                //}
            }
            finally
            {
                _timer.Enabled = true;
            }
        }

        public void CleanUp()
        {
            // Creates another list so it does not need a lock
            List<String> sessionKeys = _sessions.Keys.ToList();

            foreach (String loginGUID in sessionKeys)
            {
                ModelStorageSessionInfo sessionInfo = null;

                if (_sessions.TryGetValue(loginGUID, out sessionInfo))
                {
                    List<String> modelKeys = sessionInfo.GetModelKeys().ToList();

                    foreach (String modelKey in modelKeys)
                    {
                        CleanUpModel(loginGUID, sessionInfo, modelKey);
                    }
                }
            }
        }

        public virtual void CleanUpModel(String loginGUID, ModelStorageSessionInfo sessionInfo, String modelKey)
        {
            CascadeModelPersisterModelInfo info = null;

            try
            {
                info = sessionInfo.GetModelInfo(modelKey);

                lock (info)
                {
                    if (info.RetryCount < MaxRetryCount)
                    {
                        TryCleanUp(loginGUID, sessionInfo, modelKey, info);
                    }
                }
            }
            catch
            {
                if (info != null)
                {
                    info.IncrementRetryCount();
                }

                //if (_logger.IsFatalEnabled)
                //{
                //    _logger.Fatal(new Exception("The model \"" + modelKey + "\" from session loginGUID \"" + loginGUID + "\" could not be demoted! See inner exception.", ex));
                //}
            }
        }

        public virtual void TryCleanUp(String loginGUID, ModelStorageSessionInfo sessionInfo, String modelKey, CascadeModelPersisterModelInfo info)
        {
            int currentLevelIdx = info.Level;

            CascadeModelPersisterConfiguration currentLevel = _levels[currentLevelIdx];

            if (ShouldDemote(info, currentLevel))
            {
                String fullKey = GetFullKey(modelKey, loginGUID);

                IMVCModel model = currentLevel.Persister.Get(fullKey);

                if (model != null)
                {
                    if (currentLevelIdx < _levels.Count - 1)
                    {
                        // If is not on the last level, demotes to the next
                        int nextLevel = currentLevelIdx + 1;

                        //if (_logger.IsDebugEnabled)
                        //{
                        //    _logger.Debug("Demoting the model \"" + fullKey + "\" from level " + currentLevel + " to " + nextLevel + "...");
                        //}

                        _levels[nextLevel].Persister.Add(fullKey, model);

                        UpdateModelInfo(info, nextLevel, DateTime.UtcNow, model.GetType());
                    }
                    else
                    {
                        //if (_logger.IsDebugEnabled)
                        //{
                        //    _logger.Debug("Removing the model \"" + fullKey + "\" from the LAST level " + currentLevel + "...");
                        //}

                        sessionInfo.RemoveModelKey(modelKey);
                    }

                    currentLevel.Persister.Remove(fullKey);
                }
                else
                {
                    throw new InvalidOperationException("The model \"" + modelKey + "\" from session loginGUID \"" + loginGUID + "\" could not be demoted because it was not found on level \"" + currentLevelIdx + "\"!");
                }
            }
        }

        public virtual bool ShouldDemote(CascadeModelPersisterModelInfo info, CascadeModelPersisterConfiguration config)
        {
            return DateTime.UtcNow.Subtract(info.LastAccessTime) > config.TimeToLive;
        }

        /// <summary>
        /// Adds the supplied model instance to the AppFabric cache.
        /// </summary>
        /// <param name="modelKey">Key that will identity the supplied model.</param>
        /// <param name="model">Model instance to add.</param>
        public void Add(String modelKey, IMVCModel model, MVCControllerBase ctrl)
        {
            CheckIfDisposed();

            if (String.IsNullOrEmpty(modelKey))
                throw new ArgumentNullException("modelKey");

            if (model == null)
                throw new ArgumentNullException("model");

            if (!model.IsNew)
                throw new ArgumentException("Cannot add a model that was already added!");

            if (ctrl == null)
                throw new ArgumentNullException("ctrl");

            String loginGUID = ctrl.Session.GetLoginGUID(ctrl.Response, true, false);

            bool added = false;
            String fullKey = null;

            try
            {
                ModelStorageSessionInfo sessionInfo = GetSessionInfo(loginGUID, false);

                if (sessionInfo != null)
                {
                    fullKey = GetFullKey(modelKey, loginGUID);

                    model.IsNew = false;

                    model.OnBeforeSerialization(true);

                    // Adds the model to the first level
                    _levels.First().Persister.Add(fullKey, model);

                    sessionInfo.AddModelKey(modelKey);

                    if (CommonSettings.CascadeModelPersisterInstrumentationEnabled)
                    {
                        IncrementModelCount(model.GetType());
                    }
                    added = true;
                }
                else
                {
                    throw new Library.Util.Exceptions.ExpiredSessionException();
                }
                
            }
            catch
            {
                // If there was an error, it should reset the Model state
                model.IsNew = true;

                throw;
            }

            if (!added)
            {
                if (fullKey == null || fullKey == "")
                    throw new ExpiredSessionException();
                // If the model could not be added, it should throw an exception
                throw CreateKeyAlreadyAddedException(fullKey);
            }
        }

        private void IncrementModelCount(Type modelType)
        {
            CascadeModelPersisterInstrumentation modelTypeInstrumentation = GetInstrumentationData(modelType);

            lock (modelTypeInstrumentation)
            {
                modelTypeInstrumentation.AddedCount++;
            }
        }

        private CascadeModelPersisterInstrumentation GetInstrumentationData(Type modelType)
        {
            CascadeModelPersisterInstrumentation modelTypeInstrumentation = null;

            int modelTypeKey = modelType.GetHashCode();

            if (!_instrumentation.TryGetValue(modelTypeKey, out modelTypeInstrumentation))
            {
                lock (_instrumentation)
                {
                    if (!_instrumentation.TryGetValue(modelTypeKey, out modelTypeInstrumentation))
                    {
                        modelTypeInstrumentation = new CascadeModelPersisterInstrumentation();
                        modelTypeInstrumentation.ModelType = modelType;

                        _instrumentation.Add(modelTypeKey, modelTypeInstrumentation);
                    }
                }
            }

            return modelTypeInstrumentation;
        }

        public ModelStorageSessionInfo GetSessionInfo(String loginGUID, Boolean throwIfNotFound)
        {
            if (_sessions.ContainsKey(loginGUID))
            {
                return _sessions[loginGUID];
            }
            else if (throwIfNotFound)
            {
                throw new ArgumentException("Could not find info from login GUID \"" + loginGUID + "\"!");
            }

            return null;
        }

        /// <summary>
        /// Gets the model with the supplied key.
        /// </summary>
        /// <param name="modelKey">Key that identifies the desired model.</param>
        /// <returns>Model with the supplied key.</returns>
        public IMVCModel Get(string modelKey, MVCControllerBase ctrl)
        {
            return Get(modelKey, ctrl, true);
        }

        /// <summary>
        /// Gets the model with the supplied key.
        /// </summary>
        /// <param name="modelKey">Key that identifies the desired model.</param>
        /// <returns>Model with the supplied key.</returns>
        public IMVCModel Get(string modelKey, MVCControllerBase ctrl, Boolean throwErrorIfNotFound)
        {
            CheckIfDisposed();

            if (String.IsNullOrEmpty(modelKey))
                throw new ArgumentNullException("modelKey");

            if (ctrl == null)
                throw new ArgumentNullException("ctrl");

            String loginGUID = ctrl.Session.GetLoginGUID(ctrl.Response, true, false);

            String fullKey = GetFullKey(modelKey, loginGUID);

            ModelStorageSessionInfo sessionInfo = GetSessionInfo(loginGUID, false);

            IMVCModel item = null;

            Dictionary<String, IMVCModel> localCache = GetLocalCache(ctrl, true);

            if (sessionInfo != null)
            {
                if (!localCache.TryGetValue(modelKey, out item))
                {
                    String[] modelKeyArgs = modelKey.Split('.');

                    if (modelKeyArgs.Length > 1)
                    {
                        // If the key contains a period (.) then it means its a child model. If it is a child model,
                        // we need to first retrieve its root, because only the root is added to the storage.
                        IMVCModel rootModel = Get(modelKeyArgs[0], ctrl, false);

                        if (rootModel != null)
                        {
                            // Now that we found the root model, we need to navigate down the model hierarchy
                            int i = 1;
                            IMVCModel currentModel = rootModel;

                            do
                            {
                                currentModel = currentModel.GetChildModel(modelKeyArgs[i], false);
                                i++;
                            }
                            while (currentModel != null && i < modelKeyArgs.Length);

                            if (currentModel != null)
                                item = currentModel;
                        }
                    }
                    else
                    {
                        // Gets the model inside the storage
                        CascadeModelPersisterModelInfo info = sessionInfo.GetModelInfo(modelKey, false);

                        if (info != null)
                        {
                            lock (info)
                            {
                                IModelPersister currentLevel = _levels[info.Level].Persister;

                                item = currentLevel.Get(fullKey);

                                if (item != null && info.Level > 0)
                                {
                                    // Promotes the model to the first level if it was found on a lower level
                                    //if (_logger.IsDebugEnabled)
                                    //{
                                    //    _logger.Debug("Promoting model \"" + fullKey + "\" from level \"" + info.Level + "\" to 0");
                                    //}

                                    _levels.First().Persister.Add(fullKey, item);

                                    UpdateModelInfo(info, 0, DateTime.UtcNow, item.GetType());

                                    currentLevel.Remove(fullKey);
                                }
                            }
                        }
                        else
                        {
                            //if (_logger.IsErrorEnabled)
                            //{
                            //    _logger.Error("Tried to GET the model \"" + modelKey + "\" from session \"" + loginGUID + "\" but its model info was not found!");
                            //}
                        }
                    }
                }

                if (item != null && !localCache.ContainsKey(modelKey))
                {
                    // Adds to local cache just to prevent more than one reference on the same thread to the same Model
                    localCache.Add(modelKey, item);
                }
                else if (item != null && localCache.ContainsKey(modelKey))
                {
                    
                }
                else if (throwErrorIfNotFound)
                {
                    throw new KeyNotFoundException(fullKey);
                }
            }

            return item;
        }

        public virtual void UpdateModelInfo(CascadeModelPersisterModelInfo info, int level, DateTime lastAccessTime, Type modelType)
        {
            if (level > _levels.Count - 1)
                throw new ArgumentException("There is only \"" + _levels.Count + "\" levels! Cannot supply a level idx of \"" + level + "\".");

            if (CommonSettings.CascadeModelPersisterInstrumentationEnabled)
            {
                UpdatePromotions(level, modelType, info);
            }

            info.LastAccessTime = lastAccessTime;
            info.Level = level;
        }

        private void UpdatePromotions(int level, Type modelType, CascadeModelPersisterModelInfo info)
        {
            CascadeModelPersisterInstrumentation modelTypeInstrumentation = GetInstrumentationData(modelType);

            if (info.Level != 0 && level == 0)
            {
                lock (modelTypeInstrumentation)
                {
                    // If the model was removed from a lower level and setted on the top level, it means it was promoted
                    modelTypeInstrumentation.PromotionsCount++;
                }
            }
        }

        /// <summary>
        /// Returns the local cache for the current request.
        /// </summary>
        /// <param name="ctrl">Current MVCControllerBase.</param>
        /// <param name="createIfNotFound">Indicates if it should create the local if it was not found.</param>
        /// <returns>Local cache.</returns>
        private static Dictionary<String, IMVCModel> GetLocalCache(MVCControllerBase ctrl, Boolean createIfNotFound)
        {
            if (ctrl == null)
                throw new ArgumentNullException("ctrl");

            if (ctrl.HttpContext == null)
                throw new NullReferenceException("The MVCControllerBase HttpContext is null!");

            if (ctrl.HttpContext.Items == null)
                throw new NullReferenceException("The MVCControllerBase HttpContext Items collection is null!");

            Dictionary<String, IMVCModel> localCache = (Dictionary<String, IMVCModel>)ctrl.HttpContext.Items[LOCAL_CACHE_KEY_NAME];

            if (createIfNotFound && localCache == null)
            {
                localCache = new Dictionary<string, IMVCModel>();
                ctrl.HttpContext.Items[LOCAL_CACHE_KEY_NAME] = localCache;
            }

            return localCache;
        }

        private void CheckIfDisposed()
        {
            if (_disposed)
                throw new InvalidOperationException("The model manager was already disposed, therefore it cannot be used.");
        }

        /// <summary>
        /// Returns the key used in the data cache.
        /// </summary>
        /// <param name="modelKey">Original key.</param>
        /// <param name="sessionId">Current session id.</param>
        /// <returns>Key used in the data cache.</returns>
        public static string GetFullKey(string modelKey, String sessionId)
        {
            return String.Format("{0}.{1}", sessionId, modelKey);
        }

        /// <summary>
        /// Creates an ArgumentException instance used when the same key is added more than once.
        /// </summary>
        /// <param name="modelKey">Key added more than once.</param>
        /// <returns>ArgumentException instance.</returns>
        private ArgumentException CreateKeyAlreadyAddedException(String fullKey)
        {
            return new ArgumentException(String.Format("The key \"{0}\" was already added!", fullKey));
        }



        /// <summary>
        /// Updates the existing model with the supplied new version.
        /// </summary>
        /// <param name="modelKey">Key that identifies the desired model.</param>
        /// <param name="model">Existing model new version.</param>
        public void Update(String modelKey, IMVCModel model, MVCControllerBase ctrl)
        {
            CheckIfDisposed();

            if (String.IsNullOrEmpty(modelKey))
                throw new ArgumentNullException("modelKey");

            if (model == null)
                throw new ArgumentNullException("model");

            if (ctrl == null)
                throw new ArgumentNullException("ctrl");

            String loginGUID = ctrl.Session.GetLoginGUID(ctrl.Response, true, false);

            ModelStorageSessionInfo sessionInfo = GetSessionInfo(loginGUID, true);

            String fullKey = GetFullKey(modelKey, loginGUID);

            model.OnBeforeSerialization(true);

            CascadeModelPersisterModelInfo info = sessionInfo.GetModelInfo(modelKey);

            lock (info)
            {
                _levels.First().Persister.Update(fullKey, model);

                UpdateModelInfo(info, 0, DateTime.UtcNow, model.GetType());

                for (int i = 1; i < _levels.Count; i++)
                {
                    _levels[i].Persister.Remove(fullKey);
                }
            }
        }

        public void Enqueue(string modelKey, IMVCModel model, MVCControllerBase ctrl)
        {
            if (String.IsNullOrEmpty(modelKey))
                throw new ArgumentNullException("modelKey");

            if (model == null)
                throw new ArgumentNullException("model");

            if (ctrl == null)
                throw new ArgumentNullException("ctrl");

            String loginGUID = ctrl.Session.GetLoginGUID(ctrl.Response, true, false);

            String fullKey = GetFullKey(modelKey, loginGUID);

            Dictionary<string, IMVCModel> localCache = GetLocalCache(ctrl, true);

            if (localCache.ContainsKey(modelKey))
                throw new ArgumentException(String.Format("The key \"{0}\" was already enqueued!", modelKey));

            localCache.Add(modelKey, model);
        }

        public void Persist(MVCControllerBase ctrl)
        {
            Dictionary<String, IMVCModel> localCache = GetLocalCache(ctrl, false);

            if (localCache != null)
            {
                foreach (KeyValuePair<String, IMVCModel> modelPair in localCache)
                {
                    IMVCModel model = modelPair.Value;

                    //if (model.ParentModel == null)
                    //{
                        if (model.IsNew)
                        {
                            Add(modelPair.Key, model, ctrl);
                        }
                        else
                            Update(modelPair.Key, model, ctrl);
                    //}
                }

                localCache.Clear();
            }
        }

        public void Remove(String modelKey, MVCControllerBase ctrl)
        {
            if (String.IsNullOrEmpty(modelKey))
                throw new ArgumentNullException("modelKey");

            if (ctrl == null)
                throw new ArgumentNullException("ctrl");

            String loginGUID = ctrl.Session.GetLoginGUID(ctrl.Response, true, false);

            if (loginGUID != null)
            {
                Remove(modelKey, loginGUID, ctrl);
            }
        }

        public void Remove(String modelKey, String loginGUID, MVCControllerBase ctrl)
        {
            if (loginGUID != null)
            {
                ModelStorageSessionInfo sessionInfo = GetSessionInfo(loginGUID, false);

                if (sessionInfo != null)
                {
                    String fullKey = GetFullKey(modelKey, loginGUID);

                    CascadeModelPersisterModelInfo info = sessionInfo.GetModelInfo(modelKey, false);

                    if (info != null)
                    {
                        lock (info)
                        {
                            _levels[info.Level].Persister.Remove(GetFullKey(modelKey, loginGUID));

                            sessionInfo.RemoveModelKey(modelKey);
                        }
                    }

                    Dictionary<String, IMVCModel> localCache = GetLocalCache(ctrl, false);

                    if (localCache != null && localCache.ContainsKey(modelKey))
                    {
                        localCache.Remove(modelKey);
                    }
                }
            }
        }

        #region IDisposable Members

        public virtual void Dispose()
        {
            _disposed = true;
        }

        #endregion

        public virtual void OnApplicationStart()
        {
            lock (_sessions)
            {
                _sessions.TryAdd(CommonSettings.PUBLIC_LOGIN_GUID, new ModelStorageSessionInfo());
            }
            lock (_garbageCollector)
            {
                _garbageCollector.TryAdd(CommonSettings.PUBLIC_LOGIN_GUID, new ModelStorageGarbageManager(CommonSettings.PUBLIC_LOGIN_GUID, null, null));
            }

        }

        public virtual void OnApplicationEnd()
        {
        }

        public virtual void OnSessionStart(String loginGUID, String sessionId)
        {
            if (loginGUID != null && _garbageCollector.ContainsKey(loginGUID))
            {
                lock (_garbageCollector)
                {
                    _garbageCollector[loginGUID].AddSession(sessionId);
                }
            }
        }

        public virtual void OnSessionEnd(String loginGUID, String sessionId)
        {
            if (loginGUID != null && _garbageCollector.ContainsKey(loginGUID))
            {
                lock (_garbageCollector)
                {
                    _garbageCollector[loginGUID].RemoveSession(sessionId);
                }
            }
        }

        public virtual void OnRequestStart()
        {
        }

        public virtual void OnRequestEnd()
        {
        }

        public virtual void OnSignOut(String loginGUID, String sessionID)
        {
            DeleteModelsByLoginGUID(loginGUID);
        }

        private void DeleteModelsByLoginGUID(String loginGUID)
        {
            ModelStorageSessionInfo sessionInfo = GetSessionInfo(loginGUID, false);

            if (sessionInfo != null)
            {
                IEnumerable<String> modelKeys = new List<String>(sessionInfo.GetModelKeys());

                if (modelKeys != null)
                {
                    // Removes all models from that session
                    foreach (String modelKey in modelKeys)
                    {
                        try
                        {
                            CascadeModelPersisterModelInfo info = sessionInfo.GetModelInfo(modelKey, false);

                            if (info != null)
                            {
                                lock (info)
                                {
                                    _levels[info.Level].Persister.Remove(GetFullKey(modelKey, loginGUID));
                                }
                            }
                        }
                        catch 
                        {
                            //if (_logger.IsFatalEnabled)
                            //{
                            //    _logger.Fatal(new Exception("The model \"" + loginGUID + "\" could not be removed on Session End! See inner exception.", ex));
                            //}
                        }
                    }
                }

                lock (_sessions)
                {
                    // Removes that session info
                    ModelStorageSessionInfo info = null;
                    _sessions.TryRemove(loginGUID, out info);
                }
                lock (_garbageCollector)
                {
                    ModelStorageGarbageManager info = null;
                    _garbageCollector.TryRemove(loginGUID, out info);
                }
            }
            else
            {
                //LogHelper.Instance.Debug("Session info not found, login GUID: " + loginGUID);
            }
        }

        public virtual void OnSignIn(String loginGUID, String sessionID, String userName, String userCode)
        {
            lock (_sessions)
            {
                if (String.IsNullOrEmpty(loginGUID))
                    throw new ArgumentException("The session loginGUID is null or empty! Cannot start a session without an loginGUID.");

                // Create new session info
                if (!_sessions.ContainsKey(loginGUID))
                {
                    _sessions.TryAdd(loginGUID, new ModelStorageSessionInfo());
                    lock (_garbageCollector)
                    {
                        _garbageCollector.TryAdd(loginGUID, new ModelStorageGarbageManager(loginGUID, userName, userCode));
                    }
                }
            }
            lock (_garbageCollector)
            {
                _garbageCollector[loginGUID].AddSession(sessionID);
            }
        }

        /// <summary>
        /// Method called to remove garbage from storage
        /// </summary>
        public void CleanUpGarbageCollector()
        {
            //avoid concurrence
            List<String> loginGUIDs = new List<string>();
            AuthenticationSection section = (AuthenticationSection)WebConfigurationManager.GetSection("system.web/authentication");
            TimeSpan expirationTimeout = section.Forms.Timeout;

            lock (_garbageCollector)
            {
                loginGUIDs.AddRange(_garbageCollector.Keys);
            }
            foreach (String loginGUID in loginGUIDs)
            {
                ModelStorageGarbageManager mngr = null;
                lock (_garbageCollector)
                {
                    if (_garbageCollector.ContainsKey(loginGUID))
                    {
                        mngr = _garbageCollector[loginGUID];
                    }
                }

                //if it does not have active sessions -> verify if can be removed
                if (mngr != null && !mngr.HasActiveSessions())
                {
                    //verify if has exceeded timeout
                    if (DateTime.UtcNow.Subtract(mngr.LastUpdate).CompareTo(expirationTimeout) > 0)
                    {
                        lock (_garbageCollector)
                        {
                            if (_garbageCollector.ContainsKey(loginGUID))
                            {
                                _garbageCollector.TryRemove(loginGUID, out mngr);
                            }
                        }

                        //if it was actually removed here not by a user signout, try to delete models
                        if (mngr != null)
                        {
                            DeleteModelsByLoginGUID(loginGUID);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Method called to get detail from garbage collector
        /// </summary>
        public ConcurrentDictionary<String, ModelStorageGarbageManager> GetGarbageCollectorDetail()
        {
            return _garbageCollector;
        }
    }
}
