﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Mvc.Models
{
    [Serializable]
    public class CascadeModelPersisterModelInfo
    {
        public DateTime LastAccessTime { get; set; }

        public virtual String Identifier { get; set; }

        public virtual int Level { get; set; }

        public virtual int RetryCount { get; set; }

        public virtual void IncrementRetryCount()
        {
            RetryCount++;
        }
    }
}
