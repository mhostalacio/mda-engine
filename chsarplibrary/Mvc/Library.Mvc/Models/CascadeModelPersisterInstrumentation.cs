﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Mvc.Models
{
    public class CascadeModelPersisterInstrumentation
    {
        /// <summary>
        /// Model type that this information applies to.
        /// </summary>
        public Type ModelType { get; set; }

        /// <summary>
        /// The amount of times instances of the ModelType were promoted from a lower level to the top level.
        /// </summary>
        public long PromotionsCount { get; set; }

        /// <summary>
        /// The number of instances from this Model type.
        /// </summary>
        public long AddedCount { get; set; }
    }
}
