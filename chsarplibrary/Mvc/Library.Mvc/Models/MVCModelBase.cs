﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Globalization;
using Library.Util.Transactions;
using Library.Mvc.Controllers;

namespace Library.Mvc.Models
{
    [Serializable]
    public abstract class MVCModelBase : IMVCModel, IDisposable
    {
        private String _viewElementsPrefix = "";
        private Dictionary<String, IMVCModel> _childModels = null;
        private Dictionary<String, Object> _fieldMappings = new Dictionary<string, Object>();
        private Dictionary<String, Func<Boolean>> _requiredFields = new Dictionary<string, Func<Boolean>>();
        private bool _isNew = true;
        private Boolean _enabled = true;

        public MVCModelBase()
        {
            EditingLanguage = BusinessTransaction.CurrentContext.CurrentLanguage;
        }

        private CultureInfo _editingLanguage = BusinessTransaction.CurrentContext.CurrentLanguage;

        public CultureInfo EditingLanguage
        {
            get
            {
                return _editingLanguage;
            }
            set
            {
                _editingLanguage = value;
            }
        }

        public String ViewElementsPrefix 
        {
            get
            {
                return _viewElementsPrefix;
            }
            set
            {
                _viewElementsPrefix = value;
            }
        }

        public Boolean Enabled
        {
            get
            {
                return _enabled;
            }
            set
            {
                _enabled = value;
            }
        }

        public void Dispose()
        {
            
        }

        public bool IsNew
        {
            get { return _isNew; }
            set { _isNew = value; }
        }

        public bool IsValid
        {
            get;
            set;
        }

        public void OnBeforeSerialization(bool setXmlIgnoreFieldsAsNull = false)
        {
            if (setXmlIgnoreFieldsAsNull)
            {
                //Profile it and see if it is taking a lot of time, if yes, use cache
                var properties = from property in GetType().GetProperties() orderby property.Name select property;
                foreach (var property in properties)
                {
                    var xmlIgnoreOrNonSerializedDefined = Attribute.IsDefined(property, typeof(XmlIgnoreAttribute));
                    if (xmlIgnoreOrNonSerializedDefined)
                    {
                        property.SetValue(this, null, null);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the child model with the supplied key.
        /// </summary>
        /// <param name="key">Child model key.</param>
        /// <param name="throwError">Indicates if it should throw an error if the model is not found.</param>
        /// <returns>Child model.</returns>
        public IMVCModel GetChildModel(String key, bool throwError)
        {
            if (String.IsNullOrEmpty(key))
                throw new ArgumentException("key");

            IMVCModel childModel = null;

            if (_childModels == null || !_childModels.TryGetValue(key, out childModel) && throwError)
                throw new ArgumentException(String.Format("The child model \"{0}\" was not found!", key));

            return childModel;
        }

        public IMVCModel ParentModel
        {
            get;
            set;
        }

        public void AddFieldMapping(String key, Object value)
        {
            if (!_fieldMappings.ContainsKey(key))
            {
                _fieldMappings.Add(key, value);
            }
            else
            {
                _fieldMappings[key] = value;
            }
        }

        public Object GetFieldMapping(String key)
        {
            if (_fieldMappings.ContainsKey(key))
            {
                return _fieldMappings[key];
            }
            return null;
        }

        public string ModelKey
        {
            get;
            set;
        }

        public Dictionary<string, object> GetFieldMappings()
        {
            return _fieldMappings;
        }

        

        public void AddRequiredField(String key, Func<Boolean> value)
        {
            if (!_requiredFields.ContainsKey(key))
            {
                _requiredFields.Add(key, value);
            }
            else
            {
                _requiredFields[key] = value;
            }
        }

        public bool IsRequiredfield(String key)
        {
            bool isRequired = false;

            if (_requiredFields.ContainsKey(key))
            {
                isRequired = _requiredFields[key].Invoke();
            }

            return isRequired;

        }
    }

}
