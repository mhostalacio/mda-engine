﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Mvc.Controllers;
using System.Collections.Concurrent;

namespace Library.Mvc.Models
{
    /// <summary>
    /// Defines a contract for a model storage.
    /// </summary>
    public interface IModelStorage
    {
        IEnumerable<CascadeModelPersisterConfiguration> Levels { get; }

        IEnumerable<ModelStorageSessionInfo> Sessions { get; }

        IEnumerable<CascadeModelPersisterInstrumentation> Instrumentation { get; }

        /// <summary>
        /// Method called when the application start.
        /// </summary>
        void OnApplicationStart();

        /// <summary>
        /// Method called when the application ends.
        /// </summary>
        void OnApplicationEnd();

        /// <summary>
        /// Method called when a session starts.
        /// </summary>
        void OnSessionStart(String loginGUID, String sessionId);

        /// <summary>
        /// Method called when a session end.
        /// </summary>
        void OnSessionEnd(String loginGUID, String sessionId);

        /// <summary>
        /// Method called when a request starts.
        /// </summary>
        void OnRequestStart();

        /// <summary>
        /// Method called when a request ends.
        /// </summary>
        void OnRequestEnd();

        /// <summary>
        /// Adds the supplied model to this storage.
        /// </summary>
        /// <param name="modelKey">Key that will identity the supplied model.</param>
        /// <param name="model">Model instance to add.</param>
        /// <param name="ctrl">Current controller.</param>
        void Add(String modelKey, IMVCModel model, MVCControllerBase ctrlCtx);

        /// <summary>
        /// Enqueues the supplied model instance.
        /// </summary>
        /// <param name="modelKey">Key that will identity the supplied model.</param>
        /// <param name="model">Model instance to enqueue.</param>
        /// <param name="ctrl">Current controller.</param>
        void Enqueue(String modelKey, IMVCModel model, MVCControllerBase ctrlCtx);

        /// <summary>
        /// Returns the model with the supplied key.
        /// </summary>
        /// <param name="modelKey">Key that identifies the desired model.</param>
        /// <param name="ctrl">Current controller.</param>
        /// <returns>Model with the supplied key.</returns>
        IMVCModel Get(String modelKey, MVCControllerBase ctrlCtx);

        IMVCModel Get(string modelKey, MVCControllerBase ctrl, Boolean throwErrorIfNotFound);

        /// <summary>
        /// Persists all models on the queue.
        /// </summary>
        /// <param name="ctrl">Current controller.</param>
        void Persist(MVCControllerBase ctrl);

        /// <summary>
        /// Removes the model with the supplied key.
        /// </summary>
        /// <param name="modelKey">Model key.</param>
        /// <param name="ctrl">Current controller.</param>
        void Remove(String modelKey, MVCControllerBase ctrl);

        /// <summary>
        /// Removes the model with the supplied key.
        /// </summary>
        /// <param name="modelKey">Model key.</param>
        /// /// <param name="modelKey">Login GUID.</param>
        /// <param name="ctrl">Current controller.</param>
        void Remove(String modelKey, String loginGUID, MVCControllerBase ctrl);

        /// <summary>
        /// Updates the existing model with the supplied new version.
        /// </summary>
        /// <param name="modelKey">Key that identifies the desired model.</param>
        /// <param name="model">Existing model new version.</param>
        /// <param name="ctrl">Current controller.</param>
        void Update(String modelKey, IMVCModel model, MVCControllerBase ctrlCtx);

        /// <summary>
        /// Method called when a login is done.
        /// </summary>
        void OnSignIn(String loginGUID, String sessionId, String userName, String userCode);

        /// <summary>
        /// Method called when a logout is done.
        /// </summary>
        void OnSignOut(String loginGUID, String sessionId);

        /// <summary>
        /// Method called to remove garbage from storage
        /// </summary>
        void CleanUpGarbageCollector();

        /// <summary>
        /// Method called to get detail from garbage collector
        /// </summary>
        ConcurrentDictionary<String, ModelStorageGarbageManager> GetGarbageCollectorDetail();

        /// <summary>
        /// Method to return session info
        /// </summary>
        ModelStorageSessionInfo GetSessionInfo(String loginGUID, Boolean throwIfNotFound);
    }
}
