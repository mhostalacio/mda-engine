﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Mvc.Models
{
    public interface IModelPersister : IDisposable
    {
        void Initialize();

        void Add(String key, IMVCModel model);

        IMVCModel Get(String key);

        void Update(String key, IMVCModel model);

        void Remove(String key);
    }
}
