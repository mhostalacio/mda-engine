﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Library.Mvc.Models
{
    public interface IMVCModel
    {
        String ViewElementsPrefix { get; set; }

        Boolean IsNew { get; set; }

        void OnBeforeSerialization(bool setXmlIgnoreFieldsAsNull = false);

        IMVCModel GetChildModel(String key, bool throwError);

        IMVCModel ParentModel { get; set; }

        String ModelKey { get; set; }

        void AddFieldMapping(String key, Object value);

        Object GetFieldMapping(String key);

        Dictionary<String, Object> GetFieldMappings();

        CultureInfo EditingLanguage { get; set; }

        Boolean IsValid { get; set; }
    }
}
