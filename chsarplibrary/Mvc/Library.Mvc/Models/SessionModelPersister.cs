﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Mvc.Models
{
    /// <summary>
    /// A model storage that uses the user session for model persistency.
    /// </summary>
    public class SessionModelPersister : IModelPersister
    {
        private Dictionary<String, Dictionary<String, IMVCModel>> _models = new Dictionary<string, Dictionary<string, IMVCModel>>();

        public void Add(string fullKey, IMVCModel model)
        {
            String[] key = fullKey.Split('.');

            Dictionary<String, IMVCModel> models = GetSessionDictionary(key[0]);

            models.Add(key[1], model);
        }

        public IMVCModel Get(string fullKey)
        {
            String[] key = fullKey.Split('.');

            Dictionary<String, IMVCModel> models = GetSessionDictionary(key[0]);

            IMVCModel model = null;

            models.TryGetValue(key[1], out model);

            return model;
        }

        public void Update(string fullKey, IMVCModel model)
        {
            String[] key = fullKey.Split('.');

            Dictionary<String, IMVCModel> models = GetSessionDictionary(key[0]);

            models[key[1]] = model;
        }

        public void Remove(string fullKey)
        {
            String[] key = fullKey.Split('.');

            Dictionary<String, IMVCModel> models = GetSessionDictionary(key[0]);

            models.Remove(key[1]);
        }

        public void Initialize()
        {
        }

        public void Dispose()
        {
        }

        private Dictionary<String, IMVCModel> GetSessionDictionary(String sessionId)
        {
            Dictionary<String, IMVCModel> models = null;

            if (!_models.TryGetValue(sessionId, out models))
            {
                lock (_models)
                {
                    if (!_models.TryGetValue(sessionId, out models))
                    {
                        models = new Dictionary<string, IMVCModel>();
                        _models.Add(sessionId, models);
                    }
                }
            }

            return models;
        }
    }
}
