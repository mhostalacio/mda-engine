﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Concurrent;

namespace Library.Mvc.Models
{
    public class ModelStorageSessionInfo
    {
        private ConcurrentDictionary<string, CascadeModelPersisterModelInfo> _modelKeys = new ConcurrentDictionary<string, CascadeModelPersisterModelInfo>();

        public virtual void AddModelKey(String modelKey)
        {
            if (_modelKeys.ContainsKey(modelKey))
                throw new ArgumentException("ModelKey \"" + modelKey + "\" was already added!");

            _modelKeys.TryAdd(modelKey, new CascadeModelPersisterModelInfo { Level = 0, LastAccessTime = DateTime.UtcNow, Identifier = modelKey });
        }

        public virtual CascadeModelPersisterModelInfo GetModelInfo(String key)
        {
            return GetModelInfo(key, true);
        }

        public CascadeModelPersisterModelInfo GetModelInfo(String key, bool throwError)
        {
            CascadeModelPersisterModelInfo info = null;

            if (!_modelKeys.TryGetValue(key, out info) && throwError)
                throw new ArgumentException("The model info with the key \"" + key + "\" was not found!");

            return info;
        }

        public virtual void RemoveModelKey(String modelKey)
        {
            CascadeModelPersisterModelInfo val = null;
            _modelKeys.TryRemove(modelKey, out val);
        }

        public virtual IEnumerable<String> GetModelKeys()
        {
            return _modelKeys.Keys;
        }

        public virtual ConcurrentDictionary<string, CascadeModelPersisterModelInfo> GetModelInfos()
        {
            return _modelKeys;
        }
    }
}
