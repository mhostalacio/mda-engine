﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Mvc.Models
{
    [Serializable]
    public class ModelStorageGarbageManager
    {
        private DateTime _createDate = DateTime.UtcNow;
        private DateTime _lastUpdate = DateTime.UtcNow;
        [NonSerialized]
        private List<String> _sessionIDs = new List<string>();
        private String _loginGUID;
        private String _userName;
        private String _userCode;
        private int _numberOfModels;

        private ModelStorageGarbageManager()
        {
        }

        public ModelStorageGarbageManager(string loginGUID, string userName, string userCode)
        {
            _loginGUID = loginGUID;
            _userName = userName;
            _userCode = userCode;
        }

        public int NumberOfModels
        {
            get
            {
                return _numberOfModels;
            }
            set
            {
                _numberOfModels = value;
            }
        }

        public String LoginGUID
        {
            get
            {
                return _loginGUID;
            }
            set
            {
                _loginGUID = value;
            }
        }

        public String UserName
        {
            get
            {
                return _userName;
            }
            set
            {
                _userName = value;
            }
        }

        public String UserCode
        {
            get
            {
                return _userCode;
            }
            set
            {
                _userCode = value;
            }
        }

        public DateTime CreateDate
        {
            get
            {
                return _createDate;
            }
            set
            {
                _createDate = value;
            }
        }

        public DateTime LastUpdate
        {
            get
            {
                return _lastUpdate;
            }
            set
            {
                _lastUpdate = value;
            }
        }

        public List<String> SessionIDs
        {
            get
            {
                return _sessionIDs;
            }
        }

        public int NumberOfSessions
        {
            get
            {
                return _sessionIDs.Count;
            }
        }

        public void AddSession(string sessionID)
        {
            if (!_sessionIDs.Contains(sessionID))
            {
                lock (_sessionIDs)
                {
                    _sessionIDs.Add(sessionID);
                }
            }
            _lastUpdate = DateTime.UtcNow;
        }

        public void RemoveSession(string sessionID)
        {
            if (_sessionIDs.Contains(sessionID))
            {
                lock (_sessionIDs)
                {
                    _sessionIDs.Remove(sessionID);
                }
            }
            _lastUpdate = DateTime.UtcNow;
        }

        public Boolean HasActiveSessions()
        {
            Boolean ret = false;
            lock (_sessionIDs)
            {
                ret = _sessionIDs.FirstOrDefault() != null;
            }
            return ret;
        }
    }
}
