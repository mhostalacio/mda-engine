﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Util.Text;

namespace Library.Mvc.Controllers
{
    public class DisplayMessage : IMessage
    {
        private String _text;
        private MessageTypeEnum _messageType;

        public DisplayMessage(String text, MessageTypeEnum type)
        {
            _text = text;
            _messageType = type;
        }

        public string Text
        {
            get { return _text; }
        }

        public MessageTypeEnum MessageType
        {
            get { return _messageType; }
        }

        public String AssociatedFieldClientID
        {
            get;
            set;
        }
    }
}
