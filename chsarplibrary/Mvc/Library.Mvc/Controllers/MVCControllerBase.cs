﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Library.Mvc.Models;
using Library.Util.AOP;
using Library.Util;
using System.Web.UI;
using System.Web;
using System.Linq.Expressions;
using Library.Util.Transactions;
using Library.Mvc.WebControls.Controls;
using Library.Mvc.Triggers;
using Library.Util.Localization;
using System.Globalization;
using Library.Util.Text;
using System.Reflection.Emit;
using System.Reflection;
using Library.Util.FileStorage;
using System.Configuration;
using System.IO;
using System.Drawing;
using System.Security.Cryptography;
using Library.Mvc.WebControls.Base;
using System.Collections;
using Library.Util.Collections;
using Library.Mvc.Scripting.Base;
using Library.Util.Exceptions;

namespace Library.Mvc.Controllers
{
    public abstract class MVCControllerBase : Controller
    {
        private Boolean _keepMessages = false;

        public MVCControllerBase()
        {
            _modelsStorage = ServiceLocator.GetService<IModelStorage>();
        }

        public virtual String UrlPrefix
        {
            get
            {
                return Request.Url.Scheme + "://" + Request.Url.Host + Request.ApplicationPath;
            }

        }

        public abstract String AreaName { get; }

        public abstract String ControllerName { get; }

        public abstract CultureInfo EditingLanguage { get; }

        protected Boolean AvoidRenderMessagePanel { get; set; }

        protected virtual List<IMVCViewElement> PrintElements { get; set; }

        private string StorageRoot
        {
            get { return ConfigurationManager.AppSettings["FilePath"]; }
        }

        private Dictionary<String, String> _timezones;

        public Dictionary<String, String> TimeZones
        {
            get
            {
                if (_timezones == null)
                {
                    _timezones = new Dictionary<string, string>();
                    foreach (TimeZoneInfo z in TimeZoneInfo.GetSystemTimeZones())
                        _timezones.Add(z.Id, z.DisplayName);
                }
                return _timezones;
            }
        }

        protected virtual Boolean HasAccess()
        {
            return true;
        }

        protected virtual Boolean HasAccessToAction(String actionId)
        {
            return true;
        }

        protected virtual Boolean HasAccessToElement(String elementId)
        {
            return true;
        }

        public virtual Boolean HasAccessToElement(MVCViewElement element)
        {
            return true;
        }

        public virtual String GetPageTitle()
        {
            return "";
        }

        protected R GetServiceInterface<R>()
           where R : Library.Services.SOA.IServiceInterface<R>
        {
            R service = ServiceLocator.GetService<R>();
            if (service == null)
            {
                throw new Exception("Could not find the implementation for the interface named " + typeof(R).FullName);
            }
            return service.GetInstance();
        }

        #region View Elements

        private JSScriptElement scriptElement;
        protected MVCViewValidationSummary _messagesPanel;
        protected MVCViewElement _loginButton;
        private static string MESSAGES_PANEL_VIEW_DATA_KEY = "messagesPanel";

        public virtual MVCViewDropDown<String, CultureInfo, String> LangDrop
        {
            get
            {
                MVCViewDropDown<String, CultureInfo, String> langDrop = new MVCViewDropDown<String, CultureInfo, String>();
                langDrop.Controller = this;
                langDrop.Id = "LanguageSwitch";
                langDrop.CssClass = "LanguageSwitch";
                langDrop.HelpTextValue = delegate(Boolean setValue, String newValue) { return GetTranslation("GlobalSentences", "ChangeLanguage"); };
                langDrop.ClientId = langDrop.Id;
                langDrop.DataSource = delegate() { return LanguageSettings.ActiveLanguageList; };
                langDrop.ElementCreator = delegate(CultureInfo item, IMVCViewElement parentElement, int index) { return LanguageCreateElement(item, parentElement, index); };
                langDrop.OptionHiddenValueBind = delegate(CultureInfo lang) { return lang.IetfLanguageTag; };
                langDrop.OptionDisplayValueBind = delegate(CultureInfo lang) { return Convert.ToString(lang.DisplayName); };
                langDrop.OnChange.Add(
                    new MVCViewElementPostActionTrigger()
                    {
                        AreaName = this.AreaName,
                        ControllerName = this.ControllerName,
                        UseAjax = true,
                        ActionName = "ChangeCurrentLanguage",
                        BlockForm = true,
                        Arguments = new MVCViewArgumentCollection(new[]
                            {
                                new MVCViewArgument { Name = "Value", Value = new MVCViewFormElementArgumentValue{ Element = () => langDrop , AddEncodeUri = true } },
                            }),
                    });
                ViewData["LanguageSwitch"] = langDrop;

                return langDrop;
            }
        }

        protected virtual MVCViewOption LanguageCreateElement(CultureInfo memberCountry, IMVCViewElement parent, int memberCountryIndex)
        {
            MVCViewOption option = new MVCViewOption();
            return option;
        }

        public virtual void InitializeElements()
        {

        }

        /// <summary>
        /// Initializes the messages panel
        /// </summary>
        protected void InitializeMessagesPanel()
        {
            if (BusinessTransaction.CurrentContext == null)
                BusinessTransaction.SetCurrentContext((BusinessTransaction)Session[BusinessTransaction.USER_CONTEXT_SESSION_KEY]);

            if (_messagesPanel == null)
            {
                _messagesPanel = new MVCViewValidationSummary()
                {
                    Id = "MessagesPanel",
                    ClientId = "MessagesPanel",
                    Controller = this,
                    Messages = BusinessTransaction.CurrentContext.Messages,
                };

                ViewData[MESSAGES_PANEL_VIEW_DATA_KEY] = _messagesPanel;
            }
        }

        public void AddScriptBlock(JSScriptBlock block)
        {
            if (this.scriptElement == null)
            {
                this.scriptElement = new JSScriptElement();
            }

            this.scriptElement.Add(block);
        }

        public MVCViewValidationSummary MessagesPanel
        {
            get
            {
                return _messagesPanel;
            }
        }

        public virtual MVCViewElement LoginButton
        {
            get
            {
                if (_loginButton == null)
                {
                    MVCViewButton loginButton = new MVCViewButton();
                    _loginButton = loginButton;
                    loginButton.Controller = this;
                    loginButton.Id = "LoginButton";
                    loginButton.CssClass = "LoginButton";
                    loginButton.HelpTextValue = delegate(Boolean setValue, String newValue) { return GetTranslation("GlobalSentences", "Login"); };
                    loginButton.ClientId = _loginButton.Id;
                    loginButton.Bind = delegate(Boolean setValue, String newValue) { return GetTranslation("GlobalSentences", "Login"); };
                    loginButton.OnClick.Add(
                        new MVCViewElementOpenPopupViewTrigger()
                        {
                            ActionName = "Index",
                            BlockForm = false,
                            PopupWindowName = "Login",
                            AreaName = "Private",
                            ControllerName = "Login",
                            PopupWidth = 320,
                            PopupHeight = 380,
                            Arguments = new MVCViewArgumentCollection(new[]
                            {
                                new MVCViewArgument { Name = "ParentModelKey", Value = new MVCViewModelArgumentValue<String> { Value = () => ModelKey , AddEncodeUri = true } },
                            }),
                        });
                    ViewData["loginButton"] = _loginButton;
                }
                return _loginButton;
            }
        }

        public void RenderJSScriptElement(HtmlHelper helper)
        {
            if (this.scriptElement != null)
            {
                this.scriptElement.Render(helper);
            }
        }

        #endregion

        #region Overrides

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);

            InitializeModelKey();

            InitializeMessagesPanel();
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            AddErrorMessageToContext(filterContext.Exception.Message + filterContext.Exception.StackTrace);

            if (filterContext.Result is EmptyResult) // Redirect not requested
            {
                if (Request.IsAjaxRequest())
                {
                    filterContext.ExceptionHandled = true;
                    filterContext.Result = new EmptyResult();
                    //filterContext.RequestContext.HttpContext.Response.End();
                    filterContext.Exception = null;
                    RenderAndClearMessages();
                }
                else
                {
                    RenderAndClearMessages();
                    RedirectToErrorPage(filterContext.RequestContext, filterContext.Exception);
                    filterContext.RequestContext.HttpContext.Response.End();
                }
            }
            base.OnException(filterContext);
        }

        protected override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
            if (!HasAccess())
            {
                filterContext.Result = RedirectToAccessDenied(filterContext.RequestContext);
            }
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);

            if (Request.IsAjaxRequest())
            {
                // if response is JSON format, omit additional elements
                if (!(filterContext.Result is JsonResult))
                {
                    if (filterContext.Exception == null) // otherwise will be written OnException
                        RenderAndClearMessages();

                    if (this.scriptElement != null)
                        this.scriptElement.Render(this.CreateHtmlTextWriter());
                }
            }
        }

        #endregion

        #region Model

        private String _modelKey;
        private String _parentModelKey;
        private IModelStorage _modelsStorage;

        public String ModelKey
        {
            get
            {
                return _modelKey;
            }
            protected set
            {
                _modelKey = value;
            }
        }

        public virtual String ParentModelKey
        {
            get
            {
                return _parentModelKey;
            }
            protected set
            {
                _parentModelKey = value;
            }
        }

        protected IModelStorage ModelsStorage
        {
            get
            {
                return _modelsStorage;
            }
        }

        public virtual bool OpenedAsPopup 
        { 
            get
            {
                return Request["asPopupView"] != null ? Boolean.Parse(Request["asPopupView"]) : false;
            }
        }
        
        protected bool OpenAsPopup;

        public abstract void AddFieldMapping(String key, Object value);

        /// <summary>
        /// Gets the Model with the supplied model key.
        /// </summary>
        /// <typeparam name="ModelType">Model type.</typeparam>
        /// <param name="modelKey">Model key.</param>
        /// <returns>Model instance with the supplied key.</returns>
        public ModelType GetModelInstance<ModelType>(string modelKey) where ModelType : IMVCModel
        {
            if (String.IsNullOrEmpty(modelKey))
                throw new ArgumentException("modelKey");

            ModelType model = (ModelType)_modelsStorage.Get(modelKey, this, true);

            return model;
        }

        /// <summary>
        /// Initializes the ModelKey field.
        /// </summary>
        public void InitializeModelKey()
        {
            if (!String.IsNullOrEmpty(Request.QueryString[CommonSettings.MODEL_KEY_HIDDEN_FIELD_NAME]))
            {
                _modelKey = Request.QueryString[CommonSettings.MODEL_KEY_HIDDEN_FIELD_NAME];
            }
        }


        protected virtual void SetModelDefaultValues()
        {
        }



        #endregion

        #region Messages

        protected void AddSuccessMessageToContext(String text, String associatedFieldClientID = null)
        {
            DisplayMessage msg = new DisplayMessage(text, Library.Util.Text.MessageTypeEnum.SUCCESS);
            msg.AssociatedFieldClientID = associatedFieldClientID;
            BusinessTransaction.CurrentContext.Messages.Add(msg);
        }

        protected void AddErrorMessageToContext(String text, String associatedFieldClientID = null)
        {
            DisplayMessage msg = new DisplayMessage(text, Library.Util.Text.MessageTypeEnum.ERROR);
            msg.AssociatedFieldClientID = associatedFieldClientID;
            BusinessTransaction.CurrentContext.Messages.Add(msg);
        }

        protected void AddWarningMessageToContext(String text, String associatedFieldClientID = null)
        {
            DisplayMessage msg = new DisplayMessage(text, Library.Util.Text.MessageTypeEnum.WARNING);
            msg.AssociatedFieldClientID = associatedFieldClientID;
            BusinessTransaction.CurrentContext.Messages.Add(msg);
        }

        protected void AddInfoMessageToContext(String text, String associatedFieldClientID = null)
        {
            DisplayMessage msg = new DisplayMessage(text, Library.Util.Text.MessageTypeEnum.INFO);
            msg.AssociatedFieldClientID = associatedFieldClientID;
            BusinessTransaction.CurrentContext.Messages.Add(msg);
        }

        private void RenderAndClearMessages()
        {
            if (!AvoidRenderMessagePanel)
            {
                InitializeMessagesPanel();
                using (HtmlTextWriter writer = CreateHtmlTextWriter())
                {
                    _messagesPanel.Render(writer);
                }
                if (!_keepMessages)
                    BusinessTransaction.CurrentContext.ClearMessages();
            }
        }

        #endregion

        #region Aux Methods

        public HtmlTextWriter CreateHtmlTextWriter()
        {
            return new HtmlTextWriter(ControllerContext.HttpContext.Response.Output);
        }

        public String GetApplicationPath()
        {
            if (String.IsNullOrEmpty(this.Request.ApplicationPath))
            {
                return this.Request.Url.Scheme + "://" + this.Request.Url.Host.Replace("/", "");
            }
            else if (this.Request.ApplicationPath != null && this.Request.ApplicationPath != "/")
            {
                return this.Request.Url.Scheme + "://" + this.Request.Url.Host + this.Request.ApplicationPath;
            }
            else
            {
                return this.Request.Url.Scheme + "://" + this.Request.Url.Host + this.Request.ApplicationPath.Replace("/", "");
            }

        }

        protected virtual void RedirectToErrorPage(System.Web.Routing.RequestContext requestContext, Exception e)
        {

        }

        protected virtual RedirectResult RedirectToAccessDenied(System.Web.Routing.RequestContext requestContext)
        {
            return null;
        }

        /// <summary>
        /// Redirects to the supplied url.
        /// </summary>
        /// <param name="writer">HtmlTextWriter to write to.</param>
        /// <param name="areaName">Area to redirect to.</param>
        /// <param name="controllerName">Controller to redirect to.</param>
        /// <param name="actionName">Action to redirect to.</param>
        /// <param name="args">Arguments to redirect.</param>
        protected void WriteRedirectToAction(HtmlTextWriter writer, string areaName, string controllerName, string actionName, Dictionary<String, Object> args, bool clientSide = false)
        {
            //_redirect = true;
            _keepMessages = true;
            writer.Write("<RedirectToAction value=\"");
            TriggerHelper.WriteUrl(writer, null, areaName, controllerName, actionName, args, clientSide);
            writer.Write("\"></RedirectToAction>");
        }

        /// <summary>
        /// Redirects to the supplied url.
        /// </summary>
        /// <param name="writer">HtmlTextWriter to write to.</param>
        /// <param name="url">URL to redirect to.</param>
        /// <param name="args">Arguments to redirect.</param>
        protected void WriteRedirectToAction(HtmlTextWriter writer, string url)
        {
            //_redirect = true;
            _keepMessages = true;
            writer.Write("<RedirectToAction value=\"");
            writer.Write(url);
            writer.Write("\"></RedirectToAction>");
        }

        public virtual String GetTranslation(String keyType, String key)
        {
            String val = ServiceLocator.Instance.GetService<ITranslationProvider>().GetTranslation(Library.Util.Transactions.BusinessTransaction.CurrentContext.CurrentLanguage, keyType, key);
            if (val == null)
            {
                return keyType + ":" + key;
            }
            return val;
        }

        public virtual String GetTranslation(String keyType, String key, String defaultVaue)
        {
            String val = GetTranslation(keyType, key);
            return String.IsNullOrEmpty(val) ? defaultVaue : val;
        }

        public void WriteCell(HtmlTextWriter writer, MVCViewTableCell cell)
        {
            if (cell == null)
                throw new ArgumentNullException("cell");

            MVCViewTableRow row = new MVCViewTableRow();
            row.Cells.Add(cell);

            WriteRow(writer, row);
        }

        public void WriteRow(HtmlTextWriter writer, MVCViewTableRow row)
        {
            if (row == null)
                throw new ArgumentNullException("row");

            writer.Write("<table>");
            row.Render(writer);
            writer.Write("</table>");
        }

        public void WriteAddRowsAfter(string selector, IEnumerable<MVCViewTableRow> rows, HtmlTextWriter writer)
        {
            if (String.IsNullOrEmpty(selector))
                throw new ArgumentNullException("parentElemId");

            if (rows == null)
                throw new ArgumentNullException("elements");

            writer.Write(String.Format("<table addrowsafter=\"{0}\">", selector));
            foreach (MVCViewTableRow elem in rows)
            {
                elem.Render(writer);
            }
            writer.Write("</table>");
        }

        public void WriteAddElementsAfter(string selector, List<IMVCViewElement> elements, HtmlTextWriter writer)
        {
            if (String.IsNullOrEmpty(selector))
                throw new ArgumentNullException("parentElemId");

            if (elements == null)
                throw new ArgumentNullException("elements");

            writer.Write(String.Format("<div addelementsafter=\"{0}\">", selector));

            foreach (MVCViewElement elem in elements)
            {
                elem.Render(writer);
            }

            writer.Write("</div>");
        }

        public void WriteAddElementsBefore(string selector, List<IMVCViewElement> elements, HtmlTextWriter writer)
        {
            if (String.IsNullOrEmpty(selector))
                throw new ArgumentNullException("parentElemId");

            if (elements == null)
                throw new ArgumentNullException("elements");

            writer.Write(String.Format("<div addelementsbefore=\"{0}\">", selector));

            foreach (MVCViewElement elem in elements)
            {
                elem.Render(writer);
            }

            writer.Write("</div>");
        }

        public void WriteRemoveElement(HtmlTextWriter writer, string elemId)
        {
            writer.Write("<noscript removeElement=\"{0}\"/>", elemId);
        }

        public virtual void ClosePopupAndCallOpenerAction(string areaName, string controllerName, string actionName, Dictionary<String, Object> args = null, String modelKey = null)
        {
            using (HtmlTextWriter writer = CreateHtmlTextWriter())
            {
                writer.Write("<ClosePopup value=\"");
                TriggerHelper.WriteUrl(writer, ParentModelKey, areaName, controllerName, actionName, args, false);
                writer.Write("\"></ClosePopup>");
            }
        }

        public virtual void ClosePopupAndCallOpenerUrl(string url, Dictionary<String, Object> args = null, String modelKey = null)
        {
            using (HtmlTextWriter writer = CreateHtmlTextWriter())
            {
                writer.Write("<ClosePopup value=\"");
                TriggerHelper.WriteUrl(writer, ParentModelKey, url, args, false);
                writer.Write("\"></ClosePopup>");
            }
        }

        public virtual void CallOpenerAction(string areaName, string controllerName, string actionName, Dictionary<String, Object> args = null, String modelKey = null)
        {
            using (HtmlTextWriter writer = CreateHtmlTextWriter())
            {
                writer.Write("<CallOpenerAction value=\"");
                TriggerHelper.WriteUrl(writer, ParentModelKey, areaName, controllerName, actionName, args, false);
                writer.Write("\"></CallOpenerAction>");
            }
        }

        public virtual void ClosePopupAndRedirectOpener(string areaName, string controllerName, string actionName, Dictionary<String, Object> args = null, String modelKey = null)
        {
            using (HtmlTextWriter writer = CreateHtmlTextWriter())
            {
                writer.Write("<ClosePopup value=\"");
                TriggerHelper.WriteUrl(writer, null, areaName, controllerName, actionName, args, false);
                writer.Write("\"></ClosePopup>");
            }
        }

        public virtual void ReloadPage()
        {
            using (HtmlTextWriter writer = CreateHtmlTextWriter())
            {
                writer.Write("<script language=\"javascript\">location.reload();</script>");
            }
        }

        public virtual void ClosePopup()
        {
            using (HtmlTextWriter writer = CreateHtmlTextWriter())
            {
                writer.Write("<ClosePopup value=\"\"></ClosePopup>");
            }
        }

        public void WriteOpenToolTip(HtmlTextWriter writer, List<IMVCViewElement> content, String openerId, ToolTipHorizontalPosition posX = ToolTipHorizontalPosition.Right, ToolTipVerticalPosition posY = ToolTipVerticalPosition.Bottom, int ypad = 20)
        {
            writer.Write("<script language=\"javascript\">openToolTip('{0}', '{1}', '{2}', {3},'", openerId, posX, posY, ypad);

            StringWriter stringWriter = new StringWriter();

            using (HtmlTextWriter writer2 = new HtmlTextWriter(stringWriter))
            {
                foreach (IMVCViewElement elem in content)
                {
                    elem.Render(writer2);
                }
            }

            writer.Write(HttpUtility.JavaScriptStringEncode(stringWriter.ToString(), false));

            writer.Write("')</script>");
        }

        public virtual void CloseToolTips(HtmlTextWriter writer)
        {
            writer.Write("<script language=\"javascript\">closeAllToolTips()</script>");
        }

        public String GenerateContentUrl(String absolutePath)
        {
            return Request.Url.Scheme + "://" + Request.Url.Host + Request.ApplicationPath + absolutePath;
        }

        public void SetHtmlEditorContent(String html)
        {
            using (HtmlTextWriter writer = CreateHtmlTextWriter())
            {
                writer.Write("<SetHtmlEditorContent value=\"{0}\"></SetHtmlEditorContent>", HttpUtility.HtmlEncode(html));
            }
        }

        #endregion

        #region Actions

        public virtual ActionResult ChangeEditingLanguage(String mkey, FormCollection frmData, String value)
        {

            return new EmptyResult();
        }

        public virtual ActionResult ChangeCurrentLanguage(String mkey, FormCollection frmData, String value)
        {
            return new EmptyResult();
        }

        public virtual ActionResult ChangeCurrentTimeZone(String mkey, FormCollection frmData, String value)
        {
            return new EmptyResult();
        }

        public virtual ActionResult UpdateSettingsAccordingToBrowser(string lang, int timezoneoffset)
        {
            BusinessTransaction.CurrentContext.SessionInfo.LanguageCode = lang;
            BusinessTransaction.CurrentContext.SessionInfo.CultureCode = lang;
            return new EmptyResult();
        }

        #endregion

        #region Upload / Download Handling

        //encodes files to base64
        private string EncodeFile(string fileName)
        {
            return Convert.ToBase64String(System.IO.File.ReadAllBytes(fileName));
        }

        //Credit to i-e-b and his ASP.Net uploader for the bulk of the upload helper methods - https://github.com/i-e-b/jQueryFileUpload.Net
        //private void UploadPartialFile(string fileName, HttpRequestBase request, List<ViewDataUploadFilesResult> statuses, int width, int height, ref String fileFinalName)
        //{
        //    if (request.Files.Count != 1) throw new HttpRequestValidationException("Attempt to upload chunked file containing more than one fragment per request");
        //    var file = request.Files[0];
        //    fileFinalName = null;
        //    Stream inpuStream = file.InputStream;
        //    var fullPath = UploadAndResizeImage(width, height, ref fileFinalName, inpuStream);

        //    //var inputStream = img.;

        //    //using (var fs = new FileStream(fullPath, FileMode.Append, FileAccess.Write))
        //    //{
        //    //    var buffer = new byte[1024];

        //    //    var l = inputStream.Read(buffer, 0, 1024);
        //    //    while (l > 0)
        //    //    {
        //    //        fs.Write(buffer, 0, l);
        //    //        l = inputStream.Read(buffer, 0, 1024);
        //    //    }
        //    //    fs.Flush();
        //    //    fs.Close();
        //    //}
        //    statuses.Add(new ViewDataUploadFilesResult()
        //    {
        //        name = fileName,
        //        size = file.ContentLength,
        //        type = file.ContentType,
        //        url = "/Home/Download/" + fileName,
        //        delete_url = "/Home/Delete/" + fileName,
        //        thumbnail_url = @"data:image/png;base64," + EncodeFile(fullPath),
        //        delete_type = "GET",
        //        status = "success",
        //    });
        //}

        //Credit to i-e-b and his ASP.Net uploader for the bulk of the upload helper methods - https://github.com/i-e-b/jQueryFileUpload.Net
        //private void UploadWholeFile(HttpRequestBase request, List<ViewDataUploadFilesResult> statuses, int width, int height, ref String fileFinalName)
        //{

        //    for (int i = 0; i < request.Files.Count; i++)
        //    {
        //        HttpPostedFileBase file = request.Files[i];
        //        fileFinalName = null;
        //        Stream inpuStream = file.InputStream;

        //        var fullPath = UploadAndResizeImage(width, height, ref fileFinalName, inpuStream);

        //        statuses.Add(new ViewDataUploadFilesResult()
        //        {
        //            name = file.FileName,
        //            size = file.ContentLength,
        //            type = file.ContentType,
        //            url = "/Home/Download/" + file.FileName,
        //            delete_url = "/Home/Delete/" + file.FileName,
        //            thumbnail_url = @"data:image/png;base64," + EncodeFile(fullPath),
        //            delete_type = "GET",
        //            status = "success",
        //        });
        //    }
        //}

        private static Int64 NumberResult = 1;

        public static Int64 GenerateRandomNumber()
        {
            var ramdom = new Random(DateTime.UtcNow.Millisecond);
            NumberResult++;
            var builder = new StringBuilder();
            Int64 result;

            for (int i = 0; i < 9; i++)
                builder.Append(ramdom.Next(0, 9));

            builder.Append(NumberResult.ToString());
            result = Int64.Parse(builder.ToString());
            return result;
        }

        //protected string UploadAndResizeImage(int width, int height, ref String fileFinalName, Stream inpuStream)
        //{
        //    Image img = Image.FromStream(inpuStream, true, true);
        //    img = ImageHelper.ResizeAndCropImage(img, width, height, true);
        //    byte[] hash = SHA512.Create().ComputeHash(inpuStream);
        //    if (fileFinalName == null)
        //        fileFinalName = GenerateRandomNumber().ToString() + "_" + width + "_" + height + ".png";
        //    //fileFinalName = BitConverter.ToString(hash).Replace("-", String.Empty) + "_" + width + "_" + height + ".png";
        //    var fullPath = Path.Combine(Server.MapPath(StorageRoot), Path.GetFileName(fileFinalName));
        //    img.Save(fullPath);
        //    return fullPath;
        //}

        //[HttpGet]
        //public void Delete(string filename)
        //{
        //    var filePath = Path.Combine(StorageRoot, filename);

        //    if (System.IO.File.Exists(filePath))
        //    {
        //        System.IO.File.Delete(filePath);
        //    }
        //}

        //[HttpGet]
        //public void Download(string filename, HttpContextBase context)
        //{
        //    var filePath = Path.Combine(StorageRoot, filename);

        //    if (System.IO.File.Exists(filePath))
        //    {
        //        context.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
        //        context.Response.ContentType = "application/octet-stream";
        //        context.Response.ClearContent();
        //        context.Response.WriteFile(filePath);
        //    }
        //    else
        //        context.Response.StatusCode = 404;
        //}

        //[HttpPost]
        //public ActionResult UploadImages(int width, int height, ref String fileName)
        //{
        //    var r = new List<ViewDataUploadFilesResult>();

        //    foreach (string file in Request.Files)
        //    {
        //        var statuses = new List<ViewDataUploadFilesResult>();
        //        var headers = Request.Headers;

        //        if (string.IsNullOrEmpty(headers["X-File-Name"]))
        //        {
        //            UploadWholeFile(Request, statuses, width, height, ref fileName);
        //        }
        //        else
        //        {
        //            UploadPartialFile(headers["X-File-Name"], Request, statuses, width, height, ref fileName);
        //        }

        //        JsonResult result = Json(statuses);
        //        result.ContentType = "text/plain";

        //        return result;
        //    }

        //    return Json(r);
        //}

        #endregion

    }

    public abstract class MVCControllerBase<T> : MVCControllerBase, IModelManager
        where T : IMVCModel
    {
        private Boolean _useModelStorage = true;

        public virtual Boolean UseModelStorage
        {
            get
            {
                return _useModelStorage;
            }
        }

        public virtual T Model
        {
            get;
            protected set;
        }

        public override string ParentModelKey
        {
            get
            {
                if (Model != null && Model.ParentModel != null)
                {
                    return Model.ParentModel.ModelKey;
                }
                return base.ParentModelKey;
            }
            protected set
            {
                base.ParentModelKey = value;
            }
        }

        public abstract String ViewName { get; }

        public override string AreaName
        {
            get { throw new NotImplementedException(); }
        }

        public override string ControllerName
        {
            get { throw new NotImplementedException(); }
        }

        public override CultureInfo EditingLanguage
        {
            get { return Model.EditingLanguage; }
        }

        public override void AddFieldMapping(String key, Object value)
        {
            if (key != null)
                Model.AddFieldMapping(key, value);
        }

        public IMVCModel GetModelInstance(string modelKey)
        {
            return GetModelInstance<IMVCModel>(modelKey);
        }

        protected virtual void CreateModel()
        {
            throw new NotImplementedException();
        }

        public void CreateModelKey()
        {
            ModelKey = Guid.NewGuid().ToString().Replace('-', '_');
            Model.ModelKey = ModelKey;
        }

        protected override IAsyncResult BeginExecute(System.Web.Routing.RequestContext requestContext, AsyncCallback callback, object state)
        {
            return base.BeginExecute(requestContext, callback, state);
        }

        protected override void EndExecute(IAsyncResult asyncResult)
        {
            base.EndExecute(asyncResult);

            if (UseModelStorage)
            {
                try
                {
                    ModelsStorage.Persist(this);
                }
                catch (ExpiredSessionException ex)
                {
                    BusinessTransaction.ClearContext(Session);
                   
                }
            }

        }

        protected void InitModel()
        {
            InitializeModel();
            InitModelInternal();
        }

        protected virtual void InitModelInternal()
        {

        }

        protected void InitializeModel()
        {
            if (Model == null)
            {
                CreateModel();
                CreateModelKey();
                SetModelDefaultValues();
                AddModel(ModelKey, Model);
                Model.EditingLanguage = BusinessTransaction.CurrentContext.CurrentLanguage;
            }

            if (!String.IsNullOrEmpty(ParentModelKey) && this.UseModelStorage)
            {
                Model.ParentModel = GetModelInstance(ParentModelKey);
            }

        }

        protected void RecoverModel()
        {
            if (UseModelStorage && ModelKey == null)
                throw new Exception("Cannot recover a model withou a key");
            if (UseModelStorage)
            {
                Model = GetModelInstance<T>(ModelKey);
            }
            else if (Model == null)
            {
                ModelKey = null;
                InitModel();
                //throw new Exception("Cannot recover a model with key " + ModelKey);
            }
            //if (Model != null && HttpContext.Request.HttpMethod == "POST")
            //    UpdateModelBinds();
        }

        /// <summary>
        /// Adds a new model to the Model manager.
        /// </summary>
        /// <param name="modelKey">Model key.</param>
        /// <param name="model">Model instance to add.</param>
        public void AddModel(string modelKey, IMVCModel model)
        {
            if (String.IsNullOrEmpty(modelKey))
                throw new ArgumentException("modelKey");

            if (!model.IsNew)
                throw new ArgumentException("model must be new!");

            ModelsStorage.Enqueue(modelKey, model, this);
        }

        /// <summary>
        /// Removes the model with the supplied key.
        /// </summary>
        /// <param name="modelKey">Model key.</param>
        public void RemoveModel(String modelKey)
        {
            if (String.IsNullOrEmpty(modelKey))
                throw new ArgumentException("modelKey");

            ModelsStorage.Remove(modelKey, this);
        }

        /// <summary>
        /// Updates the value nodes with the supplied Form data.
        /// </summary>
        /// <param name="frmData">Form data.</param>
        public virtual void UpdateModelFromPostedForm(FormCollection frmData)
        {
            if (frmData == null)
                throw new ArgumentNullException("frmData");

            if (Model == null)
                throw new NullReferenceException("The controller Model is null!");

            String[] frmKeys = frmData.AllKeys;
            List<IList> treatedLists = new List<IList>();

            foreach (String frmKey in frmKeys)
            {
                if (frmKey == null)
                    continue;
                Object val = Model.GetFieldMapping(frmKey);
                if (val is Func<Boolean, String, String>)
                {
                    Func<Boolean, String, String> func = (Func<Boolean, String, String>)val;
                    func(true, frmData[frmKey]);
                }
                else if (val is Func<Boolean, Int64?, Int64?>)
                {
                    String rawValue = frmData[frmKey];
                    Int64 value = 0;
                    if (Int64.TryParse(rawValue, NumberStyles.AllowDecimalPoint, BusinessTransaction.CurrentContext.CurrentCulture.NumberFormat, out value))
                    {
                        Func<Boolean, Int64?, Int64?> func = (Func<Boolean, Int64?, Int64?>)val;
                        func(true, value);
                    }
                    else if (String.IsNullOrEmpty(rawValue))
                    {
                        Func<Boolean, Int64?, Int64?> func = (Func<Boolean, Int64?, Int64?>)val;
                        func(true, null);
                    }
                }
                else if (val is Func<Boolean, Int64, Int64>)
                {
                    String rawValue = frmData[frmKey];
                    Int64 value = 0;
                    if (Int64.TryParse(rawValue, NumberStyles.AllowDecimalPoint, BusinessTransaction.CurrentContext.CurrentCulture.NumberFormat, out value))
                    {
                        Func<Boolean, Int64, Int64> func = (Func<Boolean, Int64, Int64>)val;
                        func(true, value);
                    }
                }
                else if (val is Func<Boolean, Int32?, Int32?>)
                {
                    String rawValue = frmData[frmKey];
                    Int32 value = 0;
                    if (Int32.TryParse(rawValue, NumberStyles.AllowDecimalPoint, BusinessTransaction.CurrentContext.CurrentCulture.NumberFormat, out value))
                    {
                        Func<Boolean, Int32?, Int32?> func = (Func<Boolean, Int32?, Int32?>)val;
                        func(true, value);
                    }
                    else if (String.IsNullOrEmpty(rawValue))
                    {
                        Func<Boolean, Int32?, Int32?> func = (Func<Boolean, Int32?, Int32?>)val;
                        func(true, null);
                    }
                }
                else if (val is Func<Boolean, Int32, Int32>)
                {
                    String rawValue = frmData[frmKey];
                    Int32 value = 0;
                    if (Int32.TryParse(rawValue, NumberStyles.AllowDecimalPoint, BusinessTransaction.CurrentContext.CurrentCulture.NumberFormat, out value))
                    {
                        Func<Boolean, Int32, Int32> func = (Func<Boolean, Int32, Int32>)val;
                        func(true, value);
                    }
                }
                else if (val is Func<Boolean, Int16?, Int16?>)
                {
                    String rawValue = frmData[frmKey];
                    Int16 value = 0;
                    if (Int16.TryParse(rawValue, NumberStyles.AllowDecimalPoint, BusinessTransaction.CurrentContext.CurrentCulture.NumberFormat, out value))
                    {
                        Func<Boolean, Int16?, Int16?> func = (Func<Boolean, Int16?, Int16?>)val;
                        func(true, value);
                    }
                    else if (String.IsNullOrEmpty(rawValue))
                    {
                        Func<Boolean, Int16?, Int16?> func = (Func<Boolean, Int16?, Int16?>)val;
                        func(true, null);
                    }
                }
                else if (val is Func<Boolean, Int16, Int16>)
                {
                    String rawValue = frmData[frmKey];
                    Int16 value = 0;
                    if (Int16.TryParse(rawValue, NumberStyles.AllowDecimalPoint, BusinessTransaction.CurrentContext.CurrentCulture.NumberFormat, out value))
                    {
                        Func<Boolean, Int16, Int16> func = (Func<Boolean, Int16, Int16>)val;
                        func(true, value);
                    }
                }
                else if (val is Func<Boolean, Byte?, Byte?>)
                {
                    String rawValue = frmData[frmKey];
                    Byte value = 0;
                    if (Byte.TryParse(rawValue, NumberStyles.AllowDecimalPoint, BusinessTransaction.CurrentContext.CurrentCulture.NumberFormat, out value))
                    {
                        Func<Boolean, Byte?, Byte?> func = (Func<Boolean, Byte?, Byte?>)val;
                        func(true, value);
                    }
                    else if (String.IsNullOrEmpty(rawValue))
                    {
                        Func<Boolean, Byte?, Byte?> func = (Func<Boolean, Byte?, Byte?>)val;
                        func(true, null);
                    }
                }
                else if (val is Func<Boolean, Byte, Byte>)
                {
                    String rawValue = frmData[frmKey];
                    Byte value = 0;
                    if (Byte.TryParse(rawValue, NumberStyles.AllowDecimalPoint, BusinessTransaction.CurrentContext.CurrentCulture.NumberFormat, out value))
                    {
                        Func<Boolean, Byte, Byte> func = (Func<Boolean, Byte, Byte>)val;
                        func(true, value);
                    }
                }
                else if (val is Func<Boolean, Boolean?, Boolean?>)
                {
                    String rawValue = frmData[frmKey];
                    if (rawValue != null && rawValue.Equals("on", StringComparison.InvariantCultureIgnoreCase))
                    {
                        rawValue = "true";
                    }
                    Boolean value = false;
                    if (Boolean.TryParse(rawValue.ToLower(), out value))
                    {
                        Func<Boolean, Boolean?, Boolean?> func = (Func<Boolean, Boolean?, Boolean?>)val;
                        func(true, value);
                    }
                    else if (String.IsNullOrEmpty(rawValue.ToLower()))
                    {
                        Func<Boolean, Boolean?, Boolean?> func = (Func<Boolean, Boolean?, Boolean?>)val;
                        func(true, null);
                    }
                }
                else if (val is Func<Boolean, Boolean, Boolean>)
                {
                    String rawValue = frmData[frmKey];
                    if (rawValue != null && rawValue.Equals("on", StringComparison.InvariantCultureIgnoreCase))
                    {
                        rawValue = "true";
                    }
                    Boolean value = false;
                    if (Boolean.TryParse(rawValue.ToLower(), out value))
                    {
                        Func<Boolean, Boolean, Boolean> func = (Func<Boolean, Boolean, Boolean>)val;
                        func(true, value);
                    }
                }
                else if (val is Func<Boolean, Decimal?, Decimal?>)
                {
                    String rawValue = frmData[frmKey];
                    Decimal value = 0;
                    if (Decimal.TryParse(rawValue, NumberStyles.AllowDecimalPoint, BusinessTransaction.CurrentContext.CurrentCulture.NumberFormat, out value))
                    {
                        Func<Boolean, Decimal?, Decimal?> func = (Func<Boolean, Decimal?, Decimal?>)val;
                        func(true, value);
                    }
                    else if (String.IsNullOrEmpty(rawValue))
                    {
                        Func<Boolean, Decimal?, Decimal?> func = (Func<Boolean, Decimal?, Decimal?>)val;
                        func(true, null);
                    }
                }
                else if (val is Func<Boolean, Decimal, Decimal>)
                {
                    String rawValue = frmData[frmKey];
                    Decimal value = 0;
                    if (Decimal.TryParse(rawValue, NumberStyles.AllowDecimalPoint, BusinessTransaction.CurrentContext.CurrentCulture.NumberFormat, out value))
                    {
                        Func<Boolean, Decimal, Decimal> func = (Func<Boolean, Decimal, Decimal>)val;
                        func(true, value);
                    }
                }
                else if (val is Func<Boolean, DateTime, DateTime>)
                {
                    String rawValue = frmData[frmKey];
                    DateTime value = DateTime.MinValue;
                    if (DateTime.TryParse(rawValue, BusinessTransaction.CurrentContext.CurrentCulture, DateTimeStyles.AssumeLocal, out value))
                    {
                        Func<Boolean, DateTime, DateTime> func = (Func<Boolean, DateTime, DateTime>)val;
                        func(true, value);
                    }
                }
                else if (val is Func<Boolean, DateTime?, DateTime?>)
                {
                    String rawValue = frmData[frmKey];
                    DateTime value = DateTime.MinValue;
                    if (DateTime.TryParse(rawValue, BusinessTransaction.CurrentContext.CurrentCulture, DateTimeStyles.AssumeLocal, out value))
                    {
                        Func<Boolean, DateTime?, DateTime?> func = (Func<Boolean, DateTime?, DateTime?>)val;
                        func(true, value);
                    }
                }
                else if (val != null)
                {
                    Type d = val.GetType();

                    if (d.BaseType != typeof(MulticastDelegate))
                        throw new Exception("Not a delegate");

                    MethodInfo invoke = d.GetMethod("Invoke");
                    if (invoke == null)
                        throw new Exception("Not a delegate");

                    Type R = GetDelegateReturnType(val.GetType());
                    Object value = null;
                    if (!String.IsNullOrEmpty(frmData[frmKey]))
                    {
                        if (R.IsEnum)
                        {
                            value = Enum.Parse(R, frmData[frmKey]);
                        }
                        else if (Nullable.GetUnderlyingType(R).IsEnum)
                        {
                            value = Enum.Parse(Nullable.GetUnderlyingType(R), frmData[frmKey]);
                        }
                        else
                        {
                            value = Convert.ChangeType(frmData[frmKey], R);
                        }
                        invoke.Invoke(val, new object[] { true, value });
                    }
                    else
                    {
                        invoke.Invoke(val, new object[] { true, null });
                    }
                }
            }
        }

        private Type GetDelegateReturnType(Type d)
        {
            if (d.BaseType != typeof(MulticastDelegate))
                throw new Exception("Not a delegate");

            MethodInfo invoke = d.GetMethod("Invoke");
            if (invoke == null)
                throw new Exception("Not a delegate");

            return invoke.ReturnType;
        }

        private Type[] GetDelegateParameterTypes(Type d)
        {
            if (d.BaseType != typeof(MulticastDelegate))
                throw new Exception("Not a delegate");

            MethodInfo invoke = d.GetMethod("Invoke");
            if (invoke == null)
                throw new Exception("Not a delegate");

            ParameterInfo[] parameters = invoke.GetParameters();
            Type[] typeParameters = new Type[parameters.Length];
            for (int i = 0; i < parameters.Length; i++)
            {
                typeParameters[i] = parameters[i].ParameterType;
            }

            return typeParameters;
        }

        public override ActionResult ChangeEditingLanguage(String mkey, FormCollection frmData, String value)
        {
            base.ChangeEditingLanguage(mkey, frmData, value);
            ActionResult result = new EmptyResult();
            ModelKey = mkey;
            RecoverModel();
            Model.EditingLanguage = CultureInfo.GetCultureInfo(value);
            UpdateModelFromPostedForm(frmData);
            if (!Request.IsAjaxRequest())
            {
                result = View(ViewName, Model);
            }
            return result;
        }

        public override ActionResult ChangeCurrentLanguage(String mkey, FormCollection frmData, String value)
        {
            base.ChangeCurrentLanguage(mkey, frmData, value);
            BusinessTransaction.CurrentContext.SessionInfo.LanguageCode = value;
            return ChangeEditingLanguage(mkey, frmData, value);
        }

        public override ActionResult ChangeCurrentTimeZone(String mkey, FormCollection frmData, String value)
        {
            base.ChangeCurrentTimeZone(mkey, frmData, value);
            BusinessTransaction.CurrentContext.SessionInfo.TimeZoneCode = value;
            ActionResult result = new EmptyResult();
            ModelKey = mkey;
            RecoverModel();
            UpdateModelFromPostedForm(frmData);
            if (!Request.IsAjaxRequest())
            {
                result = View(ViewName, Model);
            }
            return result;
        }

        protected virtual void ValidateModel(string groupName)
        {
            Model.IsValid = true;
        }
    }
}
