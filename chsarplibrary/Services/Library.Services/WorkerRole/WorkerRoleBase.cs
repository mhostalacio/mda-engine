﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Library.Entities.Collections;
using Library.Util;
using Library.Util.AOP;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using Library.Util.Transactions;
using Library.Util.Logging;

namespace Library.Services.WorkerRole
{
    public abstract class WorkerRoleBase : RoleEntryPoint
    {
        private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private readonly ManualResetEvent runCompleteEvent = new ManualResetEvent(false);

        protected virtual String ServiceName
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        protected virtual int ServiceDelay
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override void Run()
        {
            Trace.TraceInformation(ServiceName + " is running");

            try
            {
                
                this.RunAsync(this.cancellationTokenSource.Token).Wait();
            }
            finally
            {
                if (CommonSettings.PersistLogs)
                {
                    ServiceLocator.GetService<ILogPersister>().SaveAndClearLogs();
                }
                this.runCompleteEvent.Set();
            }
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 12;

            // For information on handling configuration changes
            // see the MSDN topic at http://go.microsoft.com/fwlink/?LinkId=166357.

            bool result = base.OnStart();

            Trace.TraceInformation(ServiceName + " has been started");

            return result;
        }

        public override void OnStop()
        {
            Trace.TraceInformation(ServiceName + " is stopping");

            if (CommonSettings.PersistLogs)
            {
                ServiceLocator.GetService<ILogPersister>().SaveAndClearLogs();
            }

            this.cancellationTokenSource.Cancel();
            this.runCompleteEvent.WaitOne();

            base.OnStop();

            Trace.TraceInformation(ServiceName + " has stopped");
        }


        private async Task RunAsync(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                Trace.TraceInformation(ServiceName + " is working");

                using (BusinessTransaction tran = new BusinessTransaction(new SessionInfo() { UserCode = "SYSTEM", CompanyCode = "SYSTEM", SessionId = Guid.NewGuid().ToString() }, DateTime.UtcNow))
                {
                    RunAsyncInternal();
                }

                await Task.Delay(ServiceDelay);
            }
        }

        protected virtual void RunAsyncInternal()
        {
            
        }

        protected static R GetServiceInterface<R>()
           where R : Library.Services.SOA.IServiceInterface<R>
        {
            R service = ServiceLocator.GetService<R>();
            if (service == null)
            {
                throw new Exception("Could not find the implementation for the interface named " + typeof(R).FullName);
            }
            return service.GetInstance();
        }
    }
}

