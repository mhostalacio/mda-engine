﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Library.Services.SOA
{
    public class RestClient
    {

        public string EndPoint { get; set; }
        public RestClientHttpVerb Method { get; set; }
        public string ContentType { get; set; }
        public byte[] PostData { get; set; }
        Dictionary<String, String> Headers = new Dictionary<string, string>();


        public RestClient(string endpoint)
        {
            EndPoint = endpoint;
            Method = RestClientHttpVerb.GET;
            ContentType = "application/xml; charset=utf-8";

        }

        public RestClient(string endpoint, RestClientHttpVerb method)
        {
            EndPoint = endpoint;
            Method = method;
        }

        public RestClient(string endpoint, RestClientHttpVerb method, byte[] postData)
        {
            EndPoint = endpoint;
            Method = method;
            PostData = postData;
        }

        public T MakeJsonRequest<T>(HttpStatusCode targetStatus)
        {
            ContentType = "application/json";
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(MakeRequest(targetStatus));
        }

        public T MakeXmlRequest<T>(HttpStatusCode targetStatus)
        {
            ContentType = "xml";
            return XmlDeserializeFromString<T>(MakeRequest(targetStatus)); 
        }

        public String MakeRequest(HttpStatusCode targetStatus)
        {
            var request = (HttpWebRequest)WebRequest.Create(EndPoint);

            if (Headers != null)
            {
                foreach(String key in Headers.Keys)
                {
                    request.Headers.Add(key, Headers[key]);
                }
            }

            request.Method = Method.ToString();
            request.ContentLength = 0;
            request.ContentType = ContentType;

            if (ConfigurationManager.AppSettings["proxyaddress"] != null && ConfigurationManager.AppSettings["proxyport"] != null)
            {
                request.Proxy = new WebProxy(ConfigurationManager.AppSettings["proxyaddress"], int.Parse(ConfigurationManager.AppSettings["proxyport"]));
                if (ConfigurationManager.AppSettings["proxyuser"] != null && ConfigurationManager.AppSettings["proxypass"] != null)
                {
                    request.Proxy.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["proxyuser"], ConfigurationManager.AppSettings["proxypass"]);
                }
            }

            if (PostData != null)
            {
                request.ContentLength = PostData.Length;

                using (var writeStream = request.GetRequestStream())
                {
                    writeStream.Write(PostData, 0, PostData.Length);
                }
            }

            using (var response = (HttpWebResponse)request.GetResponse())
            {
                var responseValue = string.Empty;

                if (response.StatusCode != targetStatus)
                {
                    var message = String.Format("Request failed. Received {0}", response.StatusCode);
                    throw new ApplicationException(message);
                }

                // grab the response
                using (var responseStream = response.GetResponseStream())
                {
                    if (responseStream != null)
                    {
                        using (var reader = new StreamReader(responseStream))
                        {
                            responseValue = reader.ReadToEnd();
                        }
                    }
                }

                return responseValue;
            }
        }

        private static T JsonDeserializeFromString<T>(string objectData)
        {
            return JsonDeserializeFromString<T>(objectData);
        }

        private static T XmlDeserializeFromString<T>(string objectData)
        {
            return (T)XmlDeserializeFromString(objectData, typeof(T));
        }

        public static object XmlDeserializeFromString(string objectData, Type type)
        {
            var serializer = new XmlSerializer(type);
            object result;

            using (TextReader reader = new StringReader(objectData))
            {
                result = serializer.Deserialize(reader);
            }

            return result;
        }

    }

    public enum RestClientHttpVerb
    {
        GET,
        POST,
        PUT,
        DELETE
    }
}
