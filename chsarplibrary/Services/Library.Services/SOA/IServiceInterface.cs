﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Services.SOA
{
    public interface IServiceInterface<T>
    {
        T GetInstance();
    }
}
