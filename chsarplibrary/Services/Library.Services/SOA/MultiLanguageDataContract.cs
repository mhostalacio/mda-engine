﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Library.Util.Collections;

namespace Library.Services.SOA
{
    [Serializable]
    [CollectionDataContract]
    public class MultiLanguageDataContract<T> : CustomList<MultiLanguageDataContractValue<T>>
    {
    }
}
