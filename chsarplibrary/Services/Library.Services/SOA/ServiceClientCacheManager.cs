﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Util.Cache;

namespace Library.Services.SOA
{
    public class ServiceClientCacheManager<T> : CacheBase<String, List<ServiceClientBase<T>>> where T : class, IService
    {

        //TODO: improve this cache to handle a pool of valid clients

        private static Dictionary<String, Type> _typesList = new Dictionary<string, Type>();

        #region Singleton

        private static ServiceClientCacheManager<T> _instance;
        private static Object lockObject = new Object();

        public static ServiceClientCacheManager<T> Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        if (_instance == null)
                            _instance = new ServiceClientCacheManager<T>();
                    }
                }
                return _instance;
            }
        }

        private ServiceClientCacheManager()
        {
        }

        #endregion



        public R Get<R>() where R : ServiceClientBase<T>
        {
            if (!_typesList.ContainsKey(typeof(R).FullName))
            {
                lock (lockObject)
                {
                    if (!_typesList.ContainsKey(typeof(R).FullName))
                        _typesList.Add(typeof(R).FullName, typeof(R));
                }
            }
            return (R)Get(typeof(R).FullName).First();
        }

        protected override List<ServiceClientBase<T>> AddInternal(string key)
        {
            Type type = _typesList[key];
            ServiceClientBase<T> client = (ServiceClientBase<T>)Activator.CreateInstance(type);
            return new List<ServiceClientBase<T>>() { client }; 
        }
    }
}
