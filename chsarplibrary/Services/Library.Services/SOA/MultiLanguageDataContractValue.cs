﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Library.Services.SOA
{
    [Serializable]
    [DataContractAttribute]
    public class MultiLanguageDataContractValue<T>
    {
        [DataMember]
        public string Language { get; set; }

        [DataMember]
        public T Value { get; set; }
    }
}
