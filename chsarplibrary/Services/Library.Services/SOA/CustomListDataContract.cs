﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Library.Util.Collections;

namespace Library.Services.SOA
{
    [Serializable]
    [DataContract]
    public class CustomListDataContract<T>
    {
        [DataMember]
        public Boolean HasMoreEntities { get; set; }

        [DataMember]
        public CustomList<T> Items { get; set; }

        public CustomListDataContract()
        {
            Items = new CustomList<T>();
        }

        public CustomListDataContract(CustomList<T> items)
        {
            Items = items;
            HasMoreEntities = items != null ? items.HasMoreEntities : false;
        }

        public int Count
        {
            get
            {
                return Items != null ? Items.Count : 0;
            }
        }

        public void AddRange(IEnumerable<T> list)
        {
            Items.AddRange(list);
        }

        public void Add(T item)
        {
            Items.Add(item);
        }

        public T First()
        {
            return Items.First();
        }

        public T FirstOrDefault()
        {
            return Items.FirstOrDefault();
        }

        public bool Remove(T entity)
        {
            return Items.Remove(entity);
        }

    }
}
