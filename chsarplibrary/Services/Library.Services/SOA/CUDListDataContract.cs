﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Library.Services.SOA
{
    [Serializable]
    [DataContract]
    public class CUDListDataContract<T>
    {
        [DataMember]
        public CustomListDataContract<T> NewEntities { get; set; }

        [DataMember]
        public CustomListDataContract<T> ExistingEntities { get; set; }

        [DataMember]
        public CustomListDataContract<Int64> DeletedEntities { get; set; }
    }
}
