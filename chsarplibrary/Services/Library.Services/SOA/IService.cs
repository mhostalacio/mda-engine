﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace Library.Services.SOA
{
    /// <summary>
    /// Defines a common interface for Services.
    /// </summary>
    [ServiceContract]
    public interface IService
    {
        /// <summary>
        /// Just a simple method with no arguments and no return value used to check if the service is alive.
        /// </summary>
        [OperationContract]
        void Ping(PingRequestMessage request);

    }
}
