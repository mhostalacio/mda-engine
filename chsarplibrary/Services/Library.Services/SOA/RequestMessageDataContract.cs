﻿using System;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.ServiceModel;
using Library.Util.Transactions;

namespace Library.Services.SOA
{
    [MessageContract]
    public abstract class RequestMessageDataContract
    {
        protected RequestMessageDataContract()
        {
        }

        public RequestMessageDataContract(SessionInfo reqInfo, DateTime currentDateTime)
        {
            Info = reqInfo;
            InvokeTime = currentDateTime;
        }

        [MessageBodyMember]
        public SessionInfo Info { get; private set; }

        [MessageBodyMember]
        public DateTime InvokeTime { get; private set; }
    }
}
