﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace Library.Services.SOA
{
    [MessageContract]
    public class PingRequestMessage
    {
        /// <summary>
        /// The amount of time to sleep at the Back-end.
        /// </summary>
        [MessageBodyMember]
        public int? BESleep { get; set; }

        /// <summary>
        /// The amout of time to sleep at the DataBase.
        /// </summary>
        [MessageBodyMember]
        public int? DBSleep { get; set; }

        /// <summary>
        /// Indicates if it should go to the DataBase.
        /// </summary>
        [MessageBodyMember]
        public bool? GoToDB { get; set; }
    }
}
