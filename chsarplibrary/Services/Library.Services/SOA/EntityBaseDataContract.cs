﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Library.Entities.Base;
using System.Globalization;
using Library.Util.Collections;

namespace Library.Services.SOA
{
    [Serializable]
    [DataContractAttribute(Name = "Entity", Namespace = "http://www.indralab.com/library/services/soa")]
    public class EntityBaseDataContract
    {
        #region Fields

        private int _versionNumber;
        private Guid _internalUID;
        private Boolean _ignoreCaches;
        private String _defaultLanguage;
        private Int64 _id;

        #endregion

        #region Properties

        /// <summary>
        /// Id
        /// </summary>
        [DataMemberAttribute(IsRequired = true)]
        public virtual Int64 Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        /// <summary>
        /// Version Number
        /// </summary>
        [DataMemberAttribute(IsRequired = true)]
        public int VersionNumber
        {
            get { return _versionNumber; }
            set { _versionNumber = value; }
        }

        /// <summary>
        /// Internal UID
        /// </summary>
        [DataMemberAttribute(IsRequired = false)]
        public Guid InternalUID
        {
            get { return _internalUID; }
            set { _internalUID = value; }
        }

        /// <summary>
        /// Default Language
        /// </summary>
        [DataMemberAttribute(IsRequired = false)]
        public String DefaultLanguage
        {
            get { return _defaultLanguage; }
            set { _defaultLanguage = value; }
        }

        /// <summary>
        /// Ignore Caches
        /// </summary>
        [DataMemberAttribute(IsRequired = false)]
        public Boolean IgnoreCaches
        {
            get { return _ignoreCaches; }
            set { _ignoreCaches = value; }
        }

        #endregion

        #region Constructor

        public EntityBaseDataContract()
        {
            _internalUID = Guid.NewGuid();
        }

        #endregion

        #region Methods

        public static MultiLanguageDataContract<string> ConvertMultiLanguageToDataContract(MultiLanguageAttribute<string> multiLanguageAttribute)
        {
            MultiLanguageDataContract<string> toRet = null;

            if (multiLanguageAttribute != null && multiLanguageAttribute.Values != null)
            {
                toRet = new MultiLanguageDataContract<string>();
                
                foreach (KeyValuePair<CultureInfo, String> pair in multiLanguageAttribute.Values)
                {
                    toRet.Add(new MultiLanguageDataContractValue<string>()
                    {
                        Language = pair.Key.IetfLanguageTag,
                        Value = pair.Value,
                    });
                }
            }

            return toRet;

        }

        #endregion

        public static MultiLanguageAttribute<string> ConvertMultiLanguageToDataContract(MultiLanguageDataContract<string> multiLanguageDataContract)
        {
            MultiLanguageAttribute<string> toRet = null;

            if (multiLanguageDataContract != null)
            {
                toRet = new MultiLanguageAttribute<string>();
                toRet.Values = new Dictionary<CultureInfo,string>();

                foreach (MultiLanguageDataContractValue<String> pair in multiLanguageDataContract)
                {
                    toRet.Values.Add(CultureInfo.GetCultureInfo(pair.Language), pair.Value);
                }
            }

            return toRet;
        }

        public static void AddToAggregationList(AggregationList list2Add, CUDListDataContract<Int64> list2ExtractIds)
        {
            if (list2ExtractIds != null)
            {
                if (list2ExtractIds.NewEntities != null && list2ExtractIds.NewEntities.Items != null && list2ExtractIds.NewEntities.Items.Count > 0)
                {
                    list2Add.AddRange(list2ExtractIds.NewEntities.Items);
                }
                if (list2ExtractIds.ExistingEntities != null && list2ExtractIds.ExistingEntities.Items != null && list2ExtractIds.ExistingEntities.Items.Count > 0)
                {
                    list2Add.AddRange(list2ExtractIds.ExistingEntities.Items);
                }
            }
        }

        public static void AddToCUDList(CUDListDataContract<Int64> list2Add, AggregationList list2ExtractIds) 
        {
            if (list2ExtractIds != null && list2ExtractIds.Count > 0)
            {
                if (list2Add.ExistingEntities == null)
                {
                    list2Add.ExistingEntities = new CustomListDataContract<Int64>();
                }
                if (list2Add.ExistingEntities.Items == null)
                {
                    list2Add.ExistingEntities.Items = new CustomList<Int64>();
                }

                list2Add.ExistingEntities.Items.AddRange(list2ExtractIds);
            }
        }
    }
}
