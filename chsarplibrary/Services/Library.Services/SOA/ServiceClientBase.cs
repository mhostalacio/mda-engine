﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Channels;
using System.ServiceModel;

namespace Library.Services.SOA
{
    public class ServiceClientBase<ServiceT> : ClientBase<ServiceT>, IDisposable where ServiceT : class, IService
    {
        //public ServiceClientBase()
        //{
        //}

        //public ServiceClientBase(String endpointName)
        //{
            
        //}

        //public ServiceClientBase(EndpointAddress address)
        //{
        //    if (address == null)
        //        throw new ArgumentNullException("address");
        //}

        //public ServiceClientBase(EndpointAddress address, Binding binding)
        //{
        //    if (address == null)
        //        throw new ArgumentNullException("address");

        //    if (binding == null)
        //        throw new ArgumentNullException("binding");

        //}

        //public ServiceClientBase(String endpointName, EndpointAddress address)
        //{
        //    if (endpointName == null)
        //        throw new ArgumentNullException("endpointName");

        //    if (address == null)
        //        throw new ArgumentNullException("address");

        //}

        #region IDisposable Members

        public void Dispose()
        {
            //ServiceChannelFactory.Instance.ReleaseChannel(_channel);
        }

        #endregion

        #region IService Members

        /// <summary>
        /// Executes a simple method on the supplied service looking for service activation errors.
        /// </summary>
        public void Ping()
        {
            Ping(new PingRequestMessage());
        }

        /// <summary>
        /// Executes a simple method on the supplied service looking for service activation errors.
        /// </summary>
        /// <param name="request">Request options.</param>
        public void Ping(PingRequestMessage request)
        {
            //if (Channel.State != CommunicationState.Opened)
            //    Channel.Open();

            //try
            //{
            //    Channel.Service.Ping(request);
            //}
            //catch
            //{
            //    Channel.Abort();

            //    throw;
            //}
        }



        #endregion
    }
}
