﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;
using Library.Util.Collections;

namespace Library.Entities.Persistence
{
    public class RepositoryParameter : DbParameter
    {
        #region Fields

        private DbType _dbType;


        #endregion

        #region Properties

        public override System.Data.DbType DbType
        {
            get { return _dbType; }
            set { _dbType = value; }
        }

        public override System.Data.ParameterDirection Direction { get; set; }

        public override bool IsNullable { get; set; }

        public override string ParameterName { get; set; }

        public override int Size { get; set; }

        public override string SourceColumn { get; set; }

        public override bool SourceColumnNullMapping { get; set; }

        public override System.Data.DataRowVersion SourceVersion { get; set; }

        public override object Value { get; set; }

        #endregion

        #region Methods

        public override void ResetDbType()
        {
            _dbType = DbType.Object;
        }

        public override string ToString()
        {
            return String.Format("{0}:{1}", ParameterName, Value);
        }

        #endregion
    }

    public class RepositoryParameterCollection : CustomList<RepositoryParameter>
    {
        private RepositoryParameter _idParameter;

        public RepositoryParameter IdParameter
        {
            get { return _idParameter; }
        }

        public RepositoryParameter AddInParameter(string name, DbType type, object value)
        {
            return Add(name, type, value, ParameterDirection.Input);
        }

        public RepositoryParameter AddOutParameter(string name, DbType type, object value)
        {
            return Add(name, type, value, ParameterDirection.Output);
        }

        public RepositoryParameter Add(string name, DbType type, object value, ParameterDirection direction)
        {
            if (String.IsNullOrEmpty(name))
                throw new ArgumentNullException("name");

            RepositoryParameter prm = new RepositoryParameter();
            prm.ParameterName = name;
            prm.DbType = type;
            prm.Value = value;
            prm.Size = -1;
            prm.SourceVersion = DataRowVersion.Default;
            prm.Direction = direction;

            Add(prm);

            return prm;
        }

        protected override void OnAddedItem(RepositoryParameter item, int idx)
        {
            base.OnAddedItem(item, idx);

            if (item.ParameterName.Equals("Id", StringComparison.CurrentCultureIgnoreCase))
                _idParameter = item;
        }

        protected override void OnRemovedItem(RepositoryParameter item, int idx)
        {
            base.OnRemovedItem(item, idx);

            if (item.ParameterName.Equals("Id", StringComparison.CurrentCultureIgnoreCase))
                _idParameter = null;
        }

        public int GetParameterCollectionHashCode(String procName)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(procName);
            if (this.Count > 0)
            {
                foreach (RepositoryParameter param in this)
                {
                    if (param.DbType == DbType.Object)
                        return 0;

                    sb.Append(param.ParameterName);

                    if (param.Value != null)
                        sb.Append(param.Value.ToString());
                    else
                        sb.Append("NULL");
                }
            }
            return sb.ToString().GetHashCode();
        }
    }
}
