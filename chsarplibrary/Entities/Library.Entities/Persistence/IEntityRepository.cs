﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Entities.Base;

namespace Library.Entities.Persistence
{
    public interface IEntityRepository
    {

        object ExecuteScalar(RepositoryParameterCollection parameters, string procName);

        /// <summary>
        /// Raises CRUDOperation event.
        /// </summary>
        /// <param name="entity">Event arguments.</param>
        void OnCRUDOperation(RepositoryEventArgs args);
    }
    public interface IEntityRepository<T> : IEntityRepository
    {
        T GetById(long id);

        T GetById(long id, bool ignoreCaches);
    }
}
