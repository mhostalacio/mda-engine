﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Entities.Base;
using System.Collections;

namespace Library.Entities.Persistence
{
    public class RepositoryContext : IDisposable
    {
        [ThreadStatic]
        private static RepositoryContext _current = null;

        private Dictionary<Type, Dictionary<long, BaseEntity>> _list = null;
        private Dictionary<int, IList> _queryResultslist = null;
        private Dictionary<int, BaseEntity> _querySingleResultslist = null;

        public static RepositoryContext Current
        {
            get { return _current; }
        }

        public RepositoryContext()
        {
            _list = new Dictionary<Type, Dictionary<long, BaseEntity>>();
            _queryResultslist = new Dictionary<int, IList>();
            _querySingleResultslist = new Dictionary<int, BaseEntity>();
            _current = this;
        }

        #region Entities

        public IEnumerable<T> GetEntities<T>() where T : BaseEntity
        {
            return GetEntities<T>(typeof(T));
        }

        public IEnumerable<T> GetEntities<T>(Type type) where T : BaseEntity
        {
            IEnumerable<T> ents = null;

            if (_list.ContainsKey(type))
                ents = _list[type].Values.Cast<T>();

            return ents;
        }

        public T GetEntity<T>(Type type, long id) where T : BaseEntity
        {
            T entity = null;

            if (ContainsEntity(type, id))
            {
                entity = (T)_list[type][id];
            }

            return entity;
        }

        public bool ContainsEntity(Type type, BaseEntity entity)
        {
            return ContainsEntity(type, entity.Id);
        }

        public bool ContainsEntity<T>(long id) where T : BaseEntity
        {
            return ContainsEntity(typeof(T), id);
        }

        public bool ContainsEntity(Type type, long id)
        {
            return (_list.ContainsKey(type) && _list[type].ContainsKey(id));
        }

        public void RemoveEntity(Type type, BaseEntity entity)
        {
            RemoveEntity(type, entity.Id);
        }

        public void RemoveEntity(Type type, long id)
        {

            if (ContainsEntity(type, id))
            {
                _list[type].Remove(id);
            }
        }

        public void AddEntity(Type type, BaseEntity entity)
        {
            if (type == null)
                throw new ArgumentNullException("type");

            if (entity == null)
                throw new ArgumentNullException("entity");

            if (!_list.ContainsKey(type))
            {
                // Creates type list
                Dictionary<long, BaseEntity> listToAddTo = new Dictionary<long, BaseEntity>();
                listToAddTo.Add(entity.Id, entity);
                _list.Add(type, listToAddTo);
            }
            else
            {
                Dictionary<long, BaseEntity> listToAddTo = _list[type];

                // Checks if its already in list
                if (listToAddTo.ContainsKey(entity.Id))
                    throw new ArgumentException(string.Format("Entity from type \"{0}\" with Id \"{1}\" was already in context!", type.FullName, entity.Id));

                // Adds to list
                listToAddTo.Add(entity.Id, entity);
            }
        }

        #endregion

        public void AddQueryResult(int UID, IList result)
        {
            if (UID == 0)
                throw new ArgumentNullException("UID");

            if (!_queryResultslist.ContainsKey(UID))
            {
                _queryResultslist.Add(UID, result);
            }
        }

        public IList GetQueryResult(int UID)
        {
            if (_queryResultslist.ContainsKey(UID))
            {
                return _queryResultslist[UID];
            }
            return null;
        }

        public bool ContainsQueryResult(int UID)
        {
            return _queryResultslist.ContainsKey(UID);
        }

        public void AddQuerySingleResult(int UID, BaseEntity result)
        {
            if (UID == 0)
                throw new ArgumentNullException("UID");

            if (!_querySingleResultslist.ContainsKey(UID))
            {
                _querySingleResultslist.Add(UID, result);
            }
        }

        public BaseEntity GetQuerySingleResult(int UID)
        {
            if (_querySingleResultslist.ContainsKey(UID))
            {
                return _querySingleResultslist[UID];
            }
            return null;
        }

        public bool ContainsQuerySingleResult(int UID)
        {
            return _querySingleResultslist.ContainsKey(UID);
        }

        #region Queries


        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            _current = null;
        }

        #endregion
    }
}
