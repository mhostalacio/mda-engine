﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Entities.Persistence
{
    public interface IRepository<T>
    {
        T GetInstance();
    }
}
