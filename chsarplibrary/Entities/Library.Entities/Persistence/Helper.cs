﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Library.Entities.Base;

namespace Library.Entities.Persistence
{
    public static class Helper
    {
        #region Methods for getting DataReader value

        public static byte[] GetBytes(IDataReader dr, String fieldName)
        {
            long size = dr.GetBytes(dr.GetOrdinal(fieldName), 0, null, 0, 0);
            byte[] binarydata = new byte[size];

            int bufferSize = Int32.MaxValue;
            long bytesRead = 0;
            int curPos = 0;

            while (bytesRead < size)
            {
                bytesRead += GetBytes(dr, fieldName, curPos, binarydata, curPos, bufferSize);
                curPos += bufferSize;
            }
            return binarydata;
        }

        private static long GetBytes(IDataReader dr, String fieldName, long fieldOffset, byte[] buffer, int bufferoffset, int length)
        {
            return dr.GetBytes(dr.GetOrdinal(fieldName), fieldOffset, buffer, bufferoffset, length);
        }

        public static Byte GetByte(IDataReader dr, String fieldName)
        {
            return dr.GetByte(dr.GetOrdinal(fieldName));
        }

        public static Boolean GetBoolean(IDataReader dr, String fieldName)
        {
            return dr.GetBoolean(dr.GetOrdinal(fieldName));
        }

        public static Decimal GetDecimal(IDataReader dr, String fieldName)
        {
            return dr.GetDecimal(dr.GetOrdinal(fieldName));
        }

        public static Double GetDouble(IDataReader dr, String fieldName)
        {
            return dr.GetDouble(dr.GetOrdinal(fieldName));
        }

        public static float GetFloat(IDataReader dr, String fieldName)
        {
            return dr.GetFloat(dr.GetOrdinal(fieldName));
        }

        public static Guid GetGuid(IDataReader dr, String fieldName)
        {
            return dr.GetGuid(dr.GetOrdinal(fieldName));
        }

        public static String GetString(IDataReader dr, String fieldName)
        {
            return dr.GetString(dr.GetOrdinal(fieldName));
        }

        public static Int16 GetInt16(IDataReader dr, String fieldName)
        {
            return dr.GetInt16(dr.GetOrdinal(fieldName));
        }

        public static Int32 GetInt32(IDataReader dr, String fieldName)
        {
            return dr.GetInt32(dr.GetOrdinal(fieldName));
        }

        public static Int64 GetInt64(IDataReader dr, String fieldName)
        {
            return dr.GetInt64(dr.GetOrdinal(fieldName));
        }

        public static DateTime GetDateTime(IDataReader dr, String fieldName)
        {
            return DateTime.SpecifyKind(dr.GetDateTime(dr.GetOrdinal(fieldName)), DateTimeKind.Utc);
        }

        public static MultiLanguageAttribute<String> GetMultiLanguageAttribute<T1>(IDataReader dr, string fieldName)
        {
            MultiLanguageAttribute<String> multi = new MultiLanguageAttribute<string>();
            multi.Value = dr.GetString(dr.GetOrdinal(fieldName));
            return multi;
        }

        #endregion

        
    }
}
