﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Entities.Base;
using Library.Util.Collections;
using Library.Util.Text;
using System.Data.Common;
using Library.Util.Transactions;
using System.Transactions;
using Library.Util;
using System.Diagnostics;
using System.Data.SqlClient;
using Library.Entities.Exceptions;
using Library.Entities.Collections;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using System.Data;

namespace Library.Entities.Persistence
{
    public abstract class EntityRepository<T> : Repository<T>, IEntityRepository<T>, ICacheItemRefreshAction
        where T : BaseEntity, new()
    {
        #region Fields

        public static int EntityRepositoryCacheCount
        {
            get
            {
                return _cacheManager.Count;
            }
        }

        private static readonly string TYPE_FULL_NAME = typeof(T).FullName;
        private static readonly ICacheManager _cacheManager = (_useCache) ? CacheFactory.GetCacheManager() : null;

        private const string INSERT_WITHOUT_RESULT_SET_EXCEPTION = "The entity's of type \"{0}\" Insert Procedure didn´t return any ResultSet.";
        private const string UPDATE_WITHOUT_RESULT_SET_EXCEPTION = "The entity's of type \"{0}\" Update Procedure didn´t return any ResultSet.";

        private HashSet<Int64> _ids = new HashSet<Int64>();

        //[ThreadStatic]
        //private static bool _useFastInsert = true;

        //[ThreadStatic]
        //private static bool _useFastUpdate = true;

        #endregion Fields

        #region Constructors

        private EntityRepository()
        {
        }

        public EntityRepository(String databaseName)
            : base(databaseName)
        {
        }

        #endregion Constructors

        #region Events

        /// <summary>
        /// This event is triggered when an entity is inserted, updated or deleted.
        /// </summary>
        public event RepositoryEventHandler<T> CRUDOperation;

        /// <summary>
        /// This event is triggered when an entity is inserted.
        /// </summary>
        public event RepositoryEventHandler<T> EntityInserted;

        /// <summary>
        /// This event is triggered when an entity is updated.
        /// </summary>
        public event RepositoryEventHandler<T> EntityUpdated;

        /// <summary>
        /// This event is triggered when an entity is deleted.
        /// </summary>
        public event RepositoryEventHandler<T> EntityDeleted;

        #endregion Events

        #region Properties

        /// <summary>
        /// Count of entities on cache.
        /// </summary>
        public Int64 CachedEntitiesCount
        {
            get { return _ids.Count; }
        }

        /// <summary>
        /// Define a procedure de Delete.
        /// </summary>
        protected virtual string DeleteProc
        {
            get { return null; }
        }


        /// <summary>
        /// Define a procedure de Insert.
        /// </summary>
        protected virtual string InsertProc
        {
            get { return null; }
        }

        /// <summary>
        /// Define a procedure de Update.
        /// </summary>
        protected virtual string UpdateProc
        {
            get { return null; }
        }

        ///// <summary>
        ///// Indicates if it should use the fast insert procedure or not.
        ///// </summary>
        ///// <remarks>This is a thread variable and the default value is true.</remarks>
        //protected bool UseFastInsert
        //{
        //    get { return _useFastInsert; }
        //}

        ///// <summary>
        ///// Indicates if it should use the fast update procedure or not.
        ///// </summary>
        ///// <remarks>This is a thread variable and the default value is true.</remarks>
        //protected bool UseFastUpdate
        //{
        //    get { return true; }
        //}

        /// <summary>
        /// Identifica se a procedure de Delete está definida.
        /// </summary>
        private bool IsDeleteProcDefined
        {
            get { return !String.IsNullOrEmpty(DeleteProc); }
        }


        /// <summary>
        /// Identifica se a procedure de Insert está definida.
        /// </summary>
        private bool IsInsertProcDefined
        {
            get { return !String.IsNullOrEmpty(InsertProc); }
        }

        /// <summary>
        /// Identifica se a procedure de Update está definida.
        /// </summary>
        private bool IsUpdateProcDefined
        {
            get { return !String.IsNullOrEmpty(UpdateProc); }
        }

        /// <summary>
        /// Indicates if its the repository for a log entity.
        /// </summary>
        public virtual bool IsLog
        {
            get { return false; }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Insere os parâmetros de Delete.
        /// </summary>
        /// <param name="command">DbCommand utilizado na execução.</param>
        /// <param name="entity">Entidade a ser excluída.</param>
        protected virtual void AddDeleteParameters(DbCommand command, T entity)
        {
        }

        /// <summary>
        /// Insere os parâmetros de Select.
        /// </summary>
        /// <param name="command">DbCommand utilizado na execução.</param>
        /// <param name="id">Id da entidade.</param>
        protected virtual void AddGetObjectByIdParameters(DbCommand command, string id)
        {
        }

        /// <summary>
        /// Insere os parâmetros de Select.
        /// </summary>
        /// <param name="command">DbCommand utilizado na execução.</param>
        /// <param name="id">Id da entidade.</param>
        protected virtual void AddGetObjectByIdParameters(DbCommand command, string id, string idParent)
        {
        }

        /// <summary>
        /// Insere os parâmetros de Insert.
        /// </summary>
        /// <param name="command">DbCommand utilizado na execução.</param>
        /// <param name="entity">Entidade a ser inserida.</param>
        protected abstract void AddInsertParameters(DbCommand command, T entity);

        protected override void AddToCache(T entityToAdd)
        {
            if (entityToAdd == null)
                throw new ArgumentNullException("entity");

            if (!HasEntityInCache(entityToAdd.Id))
            {
                string cacheKey = GetCacheKey(entityToAdd.Id);
                _cacheManager.Add(cacheKey, entityToAdd, CacheItemPriority.NotRemovable, null, null);

                // In case the cache manager was flushed or some items expired and the EntityRepository
                // did not update its ids list, it only adds if it does not exist
                if (!_ids.Contains(entityToAdd.Id))
                    _ids.Add(entityToAdd.Id);
            }
            else
                throw new ArgumentException(string.Format("Entity \"{0}\" from type \"{1}\" is already on cache!", entityToAdd,
                    typeof(T).FullName), "entity");
        }

        /// <summary>
        /// Insere os parâmetros de Update.
        /// </summary>
        /// <param name="command">DbCommand utilizado na execução.</param>
        /// <param name="entity">Entidade a ser atualizada.</param>
        protected virtual void AddUpdateParameters(DbCommand command, T entity)
        {
        }

        public void ClearCache()
        {
            IList<Int64> idsToRemove = _ids.ToList();

            foreach (Int64 id in idsToRemove)
            {
                _cacheManager.Remove(GetCacheKey(id));
            }

            _ids.Clear();
        }

        /// <summary>
        /// Exclui uma entidade.
        /// </summary>
        /// <param name="entity">Entidade a ser excluída.</param>
        public virtual void Delete(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            if (!IsDeleteProcDefined)
                throw new NotSupportedException();

            ValidateBusinessTransactionContext();

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, CommonSettings.DefaultTransactionOptions))
            {
                DeleteChildren(entity);

                Stopwatch watch = null;

                if (!Debugger.IsAttached)
                {
                    watch = new Stopwatch();
                    watch.Start();
                }

                using (DbCommand command = Database.GetStoredProcCommand(DeleteProc))
                {
                    ++CommonSettings.TotalDatabaseHit;

                    AddDeleteParameters(command, entity);

                    try
                    {
                        int rowCount = Database.ExecuteNonQuery(command);

                        if (rowCount < 1)
                        {
                            throw new Exception(String.Format("Entidade do tipo {0} com id {1} já não existe!", typeof(T).Name, entity.Id));
                        }
                    }
                    catch (SqlException ex)
                    {
                        throw new DeleteWithFKReferencesException(ex, entity, getObjectNamesWhichHaveReferences2me());
                    }
                }

                if (!Debugger.IsAttached)
                {
                    watch.Stop();
                }

                OnAfterDelete(entity);

                scope.Complete();
            }

            OnEntityDeleted(entity);

            RepositoryContext ctx = RepositoryContext.Current;

            if (ctx != null && ctx.ContainsEntity(_itemType, entity.Id))
                ctx.RemoveEntity(_itemType, entity.Id);

        }


        /// <summary>
        /// Exclui os "filhos" da entidade.
        /// </summary>
        /// <param name="entity">Entidade a ser excluída.</param>
        protected virtual void DeleteChildren(T entity)
        {
        }

        /// Exclui os "filhos" da entidade.
        /// </summary>
        /// <param name="entity">Entidade a ser excluída.</param>
        protected virtual void OnAfterDelete(T entity)
        {
        }

        /// <summary>
        /// Return an entity using a database procedure with parameters
        /// </summary>
        /// <param name="procName">Procedure Name</param>
        /// <param name="parameters">Procedure's parameters - can be null</param>
        /// <param name="ignoreCaches">Indicates if it should go to persistence even if the Entity its in the Repository Context.</param>
        /// <param name="getAuditFields">Indicates if it should get the audit fields.</param>
        /// <returns>Entity found in persistence or a null reference.</returns>
        public virtual T ExecuteUpdateCustom(RepositoryParameterCollection parameters, string procName, bool ignoreCaches, bool getAuditFields)
        {
            ValidateBusinessTransactionContext();

            T entity = default(T);

            RepositoryParameter idPrm = parameters != null ? parameters.IdParameter : null;

            if (!ignoreCaches && idPrm != null && idPrm.Value is Int64)
            {
                long id = (Int64)idPrm.Value;

                RepositoryContext ctx = RepositoryContext.Current;

                if (UseCache() && HasEntityInCache(id))
                {
                    entity = GetCachedEntity(id);
                }
                else if (ctx != null && ctx.ContainsEntity(_itemType, id))
                {
                    entity = ctx.GetEntity<T>(_itemType, id);
                }
            }

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, CommonSettings.DefaultTransactionOptions))
            {
                Stopwatch watch = null;

                if (!Debugger.IsAttached)
                {
                    watch = new Stopwatch();
                    watch.Start();
                }

                int columns = 0;

                using (DbCommand command = Database.GetStoredProcCommand(procName))
                {
                    ++CommonSettings.TotalDatabaseHit;

                    if (parameters != null)
                        AddRepositoryParameters(Database, parameters, command);

                    using (IDataReader dr = Database.ExecuteReader(command))
                    {
                        try
                        {
                            if (dr.Read())
                            {
                                columns = dr.FieldCount;

                                OnAfterExecuteUpdateCustom(procName, ref entity, dr, ignoreCaches, getAuditFields);
                            }
                        }
                        catch (Exception ex)
                        {
                            if (ex.Message != null && ex.Message.IndexOf("***ERROR234") > -1)
                            {
                                throw new ConcurrentUpdateException(entity, GetById(entity.Id), GetById(entity.Id, true), ex);
                            }
                            else
                            {
                                throw;
                            }
                        }
                    }
                }

                if (!Debugger.IsAttached)
                {
                    watch.Stop();
                }

                scope.Complete();
            }

            return entity;
        }

        protected virtual void OnAfterExecuteUpdateCustom(string procName, ref T entity, IDataReader dr, Boolean ignoreCaches, Boolean getAuditFields)
        {
        }

        protected void FillMetadataToInsert(DbCommand command, T entity)
        {
            BusinessTransaction ctx = BusinessTransaction.CurrentContext;
            Database.AddInParameter(command, "CreateAuthor", DbType.String, ctx.SessionInfo.UserCode);
            Database.AddInParameter(command, "CreateDate", DbType.DateTime2, ctx.ClientApplicationDateTime.ToUniversalTime());
        }

        protected void FillMetadataToUpdate(DbCommand command, T entity)
        {
            BusinessTransaction ctx = BusinessTransaction.CurrentContext;
            Database.AddInParameter(command, "VersionAuthor", DbType.String, ctx.SessionInfo.UserCode);
            Database.AddInParameter(command, "VersionDate", DbType.DateTime2, ctx.ClientApplicationDateTime.ToUniversalTime());
            Database.AddInParameter(command, "VersionNumber", DbType.Int64, entity.VersionNumber);
        }

        private string GetCacheKey(long id)
        {
            return string.Format("{0}:{1}", TYPE_FULL_NAME, id);
        }

        public override IList<T> GetCachedEntities()
        {
            IList<T> entitiesOnCache = null;

            lock (CACHE_LOCK_OBJECT)
            {
                if (_ids.Count > 0)
                {
                    entitiesOnCache = new List<T>();

                    IList<Int64> idsToRemove = new List<Int64>();

                    foreach (Int64 id in _ids)
                    {
                        if (HasEntityInCache(id))
                            entitiesOnCache.Add(GetCachedEntity(id));
                        else
                            idsToRemove.Add(id);
                    }

                    // Clears ids of items that are no longer in Cache
                    foreach (Int64 idToRemove in idsToRemove)
                    {
                        _ids.Remove(idToRemove);
                    }
                }
            }

            return entitiesOnCache;
        }

        public override T GetCachedEntity(long? id)
        {
            T entity = default(T);

            if (id.HasValue && HasEntityInCache(id.Value))
                entity = (T)_cacheManager.GetData(GetCacheKey(id.Value));

            return entity;
        }

        protected virtual List<T> GetChildList(BaseEntity Parent, RepositoryParameterCollection parameters, string procName)
        {
            return GetChildList(Parent, parameters, procName, false);
        }

        protected virtual List<T> GetChildList(BaseEntity Parent, RepositoryParameterCollection parameters, string procName, bool getAuditFields)
        {
            List<T> list = default(List<T>);

            using (DbCommand command = Database.GetStoredProcCommand(procName))
            {
                ++CommonSettings.TotalDatabaseHit;

                AddRepositoryParameters(Database, parameters, command);

                using (IDataReader dr = Database.ExecuteReader(command))
                {
                    list = new List<T>();

                    if (dr.Read())
                    {
                        do
                        {
                            T newObj = new T();

                            newObj.Fill(dr);

                            list.Add(newObj);
                        }
                        while (dr.Read());
                    }
                }
            }

            return list;
        }


        protected override bool HasEntityInCache(long id)
        {
            return _ids.Contains(id) && _cacheManager.GetData(GetCacheKey(id)) != null;
        }



        void IEntityRepository.OnCRUDOperation(RepositoryEventArgs args)
        {
            if (args == null)
                throw new ArgumentNullException("args");

            RepositoryEventArgs<T> typedArgs = null;

            if (!(args is RepositoryEventArgs<T>))
                typedArgs = new RepositoryEventArgs<T>((T)args.Entity, args.CRUDOperation);
            else
                typedArgs = (RepositoryEventArgs<T>)args;

            OnCRUDOperation(typedArgs);
        }

        /// <summary>
        /// This method its called only when inserting a new entity so it can update its children
        /// reference to it.
        /// </summary>
        public virtual void UpdateChildrenReference(T entity)
        {
        }

        /// <summary>
        /// Insere uma entidade.
        /// </summary>
        /// <param name="entity">Entidade a ser inserida.</param>
        public virtual void Insert(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            if (!IsInsertProcDefined)
                throw new NotSupportedException();

            ValidateBusinessTransactionContext();

            // Inicializa TransactionScope
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, CommonSettings.DefaultTransactionOptions))
            {
                OnBeforeInsert(entity);

                Stopwatch watch = null;

                if (!Debugger.IsAttached)
                {
                    watch = new Stopwatch();
                    watch.Start();
                }

                using (DbCommand command = Database.GetStoredProcCommand(InsertProc))
                {
                    ++CommonSettings.TotalDatabaseHit;

                    // Adiciona parâmetros
                    AddInsertParameters(command, entity);

                    // Adiciona parâmetros do Metadata
                    FillMetadataToInsert(command, entity);

                    entity.Id = (Int64)Database.ExecuteScalar(command);

                    entity.VersionNumber = 1;
                    entity.IsDirty = false;
                    //entity.WasNew = true;
                }

                if (!Debugger.IsAttached)
                {
                    watch.Stop();
                }

                UpdateChildrenReference(entity);

                // Insert children entities
                SaveChildren(entity);

                // Actualiza os Resources da entidade
                RefreshResources(entity);

                // Finaliza TransactionScope
                scope.Complete();
            }

            OnEntityInserted(entity);

            RepositoryContext ctx = RepositoryContext.Current;

            if (ctx != null)
                ctx.AddEntity(_itemType, entity);

        }


        protected virtual string GetClassName()
        {
            return null;
        }

        public virtual void Initialize()
        {

        }

        /// <summary>
        /// Actualiza os resources dos campos multi-língua da entidade.
        /// </summary>
        /// <param name="entity">Entidade inserida.</param>
        protected virtual void RefreshResources(T entity)
        {
        }

        protected virtual void OnBeforeInsert(T entity)
        {

        }

        protected virtual void OnBeforeUpdate(T entity)
        {

        }

        /// <summary>
        /// Raises EntityInserted event.
        /// </summary>
        /// <param name="entity">Entity that was inserted.</param>
        protected virtual void OnEntityInserted(T entity)
        {
            RepositoryEventArgs<T> args = new RepositoryEventArgs<T>(entity, RepositoryCRUDOperation.Create);

            if (EntityInserted != null)
            {
                EntityInserted(args);
            }

            OnCRUDOperation(args);
        }

        /// <summary>
        /// Raises EntityUpdated event.
        /// </summary>
        /// <param name="entity">Entity that was updated.</param>
        protected virtual void OnEntityUpdated(T entity)
        {
            RepositoryEventArgs<T> args = new RepositoryEventArgs<T>(entity, RepositoryCRUDOperation.Update);

            if (EntityUpdated != null)
            {
                EntityUpdated(args);
            }

            OnCRUDOperation(args);
        }

        /// <summary>
        /// Raises CRUDOperation event.
        /// </summary>
        /// <param name="entity">Event arguments.</param>
        public virtual void OnCRUDOperation(RepositoryEventArgs<T> args)
        {
            if (args == null)
                throw new ArgumentNullException("args");

            if (UseCache())
            {
                lock (CACHE_LOCK_OBJECT)
                {
                    switch (args.CRUDOperation)
                    {
                        case RepositoryCRUDOperation.Create:
                            AddToCache((T)args.Entity);
                            break;

                        case RepositoryCRUDOperation.Update:
                            if (HasEntityInCache(args.Entity.Id) && !ReferenceEquals(args.Entity, GetCachedEntity(args.Entity.Id)))
                            {
                                RemoveFromCache(args.Entity.Id);
                                AddToCache((T)args.Entity);
                            }
                            break;

                        case RepositoryCRUDOperation.Delete:
                            RemoveFromCache(args.Entity.Id);
                            break;
                    }
                }
            }

            // Triggers global event
            OnRepositoryEvent(args);

            if (CRUDOperation != null)
            {
                //Invokes the delegates.
                CRUDOperation(args);
            }
        }

        /// <summary>
        /// Raises EntityDeleted event.
        /// </summary>
        /// <param name="entity">Entity that was deleted.</param>
        protected virtual void OnEntityDeleted(T entity)
        {
            RepositoryEventArgs<T> args = new RepositoryEventArgs<T>(entity, RepositoryCRUDOperation.Delete);

            if (EntityDeleted != null)
            {
                EntityDeleted(args);
            }

            OnCRUDOperation(args);
        }

        public void RemoveFromCache(T entity)
        {
            RemoveFromCache(entity.Id);
        }

        protected override void RemoveFromCache(long entityId)
        {
            if (HasEntityInCache(entityId))
            {
                lock (CACHE_LOCK_OBJECT)
                {
                    if (HasEntityInCache(entityId))
                    {
                        string cacheKey = GetCacheKey(entityId);

                        _cacheManager.Remove(cacheKey);
                        _ids.Remove(entityId);
                    }
                }
            }
        }

        /// <summary>
        /// Guarda a entidade.
        /// </summary>
        /// <param name="trn">Transaction context.</param>
        /// <param name="entityToSave">Entidade a ser guardada.</param>
        public virtual void Save(T entityToSave)
        {
            Save(entityToSave, false);
        }



        public override void Save(IEnumerable<BaseEntity> ents)
        {
            Save(new EntityList<T>(ents.Cast<T>()));
        }


        /// <summary>
        /// Saves a list of entities.
        /// </summary>
        /// <param name="entities">Entities to be saved.</param>
        public virtual void Save(EntityList<T> entities)
        {
            if (entities == null)
                throw new ArgumentNullException("entities");

            if (!BusinessTransaction.HasCurrentContext)
                throw new InvalidOperationException("There is no BusinessTransactionContext set!");

            BusinessTransaction ctx = BusinessTransaction.CurrentContext;

            if (entities.Count != 0 || entities.RemovedIds.Count != 0)
            {
                string procedureName = GetCUDProcedureName();

                InsertTable<T> entitiesToInsert = CreateInsertTable();
                UpdateTable<T> entitiesToUpdate = CreateUpdateTable();
                DeleteTable entitiesToDelete = null;


                Int16 counter = 0;
                foreach (T ent in entities)
                {
                    if (ent.IsNew)
                    {
                        OnBeforeInsert(ent);
                        ent.PersistOrder = counter++;
                        entitiesToInsert.AddAndEnsure(ent);

                    }
                    else if (ent.IsDirty)
                    {
                        OnBeforeUpdate(ent);

                        entitiesToUpdate.AddAndEnsure(ent);

                    }
                }

                if (entitiesToInsert.Count == 0)
                    entitiesToInsert = null;

                if (entitiesToUpdate.Count == 0)
                    entitiesToUpdate = null;
                else if (entitiesToUpdate.FirstOrDefault(o => o.Id < 0) != null)
                {
                    throw new Exception("Trying to update a register with negative id!");
                }

                if (entities.RemovedIds != null && entities.RemovedIds.Count > 0)
                {
                    entitiesToDelete = new DeleteTable(entities.RemovedIds);

                    foreach (long idToRemove in entities.RemovedIds)
                    {
                        //TODO: Change EntityList to store the deleted entities VersionNumber.
                    }
                }

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, CommonSettings.DefaultTransactionOptions))
                {
                    // Only call persistence if it has something to do
                    if (entitiesToInsert != null || entitiesToUpdate != null || entitiesToDelete != null)
                    {
                        T entityToDelete = null;

                        if (entitiesToDelete != null && entitiesToDelete.Count > 0)
                        {
                            foreach (long id in entitiesToDelete)
                            {
                                entityToDelete = GetById(id);

                                DeleteChildren(entityToDelete);

                                OnAfterDelete(entityToDelete);
                            }
                        }

                        Stopwatch watch = null;

                        if (!Debugger.IsAttached)
                        {
                            watch = new Stopwatch();

                            watch.Start();
                        }


                        using (DbCommand command = Database.GetStoredProcCommand(procedureName))
                        {
                            ++CommonSettings.TotalDatabaseHit;

                            SqlCommand sqlCommand = (SqlCommand)command;

                            T ent = new T();

                            var insertParam = sqlCommand.Parameters.AddWithValue("@ValuesC", entitiesToInsert);
                            var updateParam = sqlCommand.Parameters.AddWithValue("@ValuesU", entitiesToUpdate);
                            var deleteParam = sqlCommand.Parameters.AddWithValue("@ValuesD", entitiesToDelete);


                            sqlCommand.Parameters.AddWithValue("@CreateAuthor", ctx.SessionInfo.UserCode);
                            sqlCommand.Parameters.AddWithValue("@VersionAuthor", ctx.SessionInfo.UserCode);
                            sqlCommand.Parameters.Add(new SqlParameter() { ParameterName = "@CreateDate", Value = DateTime.UtcNow, SqlDbType = SqlDbType.DateTime2 });
                            sqlCommand.Parameters.Add(new SqlParameter() { ParameterName = "@VersionDate", Value = DateTime.UtcNow, SqlDbType = SqlDbType.DateTime2 });

                            insertParam.SqlDbType = SqlDbType.Structured;
                            updateParam.SqlDbType = SqlDbType.Structured;
                            deleteParam.SqlDbType = SqlDbType.Structured;

                            try
                            {
                                using (IDataReader dr = Database.ExecuteReader(command))
                                {
                                    if (entitiesToInsert != null)
                                    {
                                        // Creates insert log
                                        int insertCount = entitiesToInsert.Count;
                                        for (int i = 0; i < insertCount; i++)
                                        {
                                            T item = entitiesToInsert[i];
                                            // "Invalid attempt to read when no data is present." Error
                                            if (dr.Read())
                                            {
                                                item.Id = dr.GetInt64(0);
                                                item.VersionNumber = 1;

                                                item.WasNew = true;
                                                item.IsDirty = false;
                                            }
                                        }
                                    }

                                    if (entitiesToUpdate != null)
                                    {
                                        dr.NextResult();
                                        foreach (T item in entitiesToUpdate)
                                        {
                                            dr.Read();

                                            if (item.Id != dr.GetInt64(0))
                                                throw new NotSupportedException("It should be the same Id");

                                            item.VersionNumber = dr.GetInt64(1);
                                            item.IsDirty = false;
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                if (ex.Message != null && ex.Message.IndexOf("***ERROR234") > -1)
                                {
                                    throw new ConcurrentUpdateException(null, null, null, ex);
                                }
                                else
                                {
                                    throw;
                                }
                            }
                        }

                        if (!Debugger.IsAttached)
                        {
                            watch.Stop();
                        }
                    }

                    foreach (T entity in entities)
                    {
                        SaveChildren(entity);
                    }


                    RepositoryContext repositoryCtx = RepositoryContext.Current;

                    if (entitiesToInsert != null && entitiesToInsert.Count > 0)
                    {
                        if (entitiesToInsert[0].HasMultiLanguageAttributes)
                        {
                            List<MultiLanguageEntry> entries = new List<MultiLanguageEntry>();
                            foreach (T entity in entitiesToInsert)
                            {
                                entries.AddRange(entity.GetMultiLanguageEntries());
                            }

                            EntityResourcesBulkCUD(entitiesToInsert[0].EntityName + "_Resources", entries);
                        }

                        if (repositoryCtx != null)
                        {
                            foreach (T entity in entitiesToInsert)
                            {
                                if (!repositoryCtx.ContainsEntity<T>(entity.Id))
                                    repositoryCtx.AddEntity(_itemType, entity);
                            }
                        }
                    }

                    if (entitiesToUpdate != null && entitiesToUpdate.Count > 0)
                    {
                        if (entitiesToUpdate[0].HasMultiLanguageAttributes)
                        {
                            List<MultiLanguageEntry> entries = new List<MultiLanguageEntry>();
                            foreach (T entity in entitiesToUpdate)
                            {
                                entries.AddRange(entity.GetMultiLanguageEntries());
                            }

                            EntityResourcesBulkCUD(entitiesToUpdate[0].EntityName + "_Resources", entries);
                        }

                        if (repositoryCtx != null)
                        {
                            foreach (T entity in entitiesToUpdate)
                            {
                                if (!repositoryCtx.ContainsEntity<T>(entity.Id))
                                    repositoryCtx.AddEntity(_itemType, entity);
                            }
                        }
                    }


                    scope.Complete();
                }
            }

        }


        /// <summary>
        /// Saves a list of entities.
        /// </summary>
        /// <param name="entities">Entities to be saved.</param>
        /// <param name="isReferenceData">Are entities ReferenceData.</param>
        public virtual void BulkSave(EntityList<T> entities, bool isReferenceData)
        {
            if (entities == null)
                throw new ArgumentNullException("entities");

            if (!BusinessTransaction.HasCurrentContext)
                throw new InvalidOperationException("There is no BusinessTransactionContext set!");

            BusinessTransaction ctx = BusinessTransaction.CurrentContext;

            if (entities.Count != 0 || entities.RemovedIds.Count != 0)
            {
                string procedureName = GetCUDProcedureName();

                InsertTable<T> entitiesToInsert = CreateInsertTable();
                UpdateTable<T> entitiesToUpdate = CreateUpdateTable();
                DeleteTable entitiesToDelete = null;

                Int16 counter = 0;
                foreach (T ent in entities)
                {
                    if (ent.IsNew)
                    {
                        OnBeforeInsert(ent);
                        ent.PersistOrder = counter++;
                        entitiesToInsert.AddAndEnsure(ent);
                    }
                    else if (ent.IsDirty)
                    {
                        OnBeforeUpdate(ent);

                        entitiesToUpdate.AddAndEnsure(ent);
                    }
                }

                if (entitiesToInsert.Count == 0)
                    entitiesToInsert = null;

                if (entitiesToUpdate.Count == 0)
                    entitiesToUpdate = null;
                else if (entitiesToUpdate.FirstOrDefault(o => o.Id < 0) != null)
                {
                    throw new Exception("Trying to update a register with negative id!");
                }

                if (entities.RemovedIds != null && entities.RemovedIds.Count > 0)
                {
                    entitiesToDelete = new DeleteTable(entities.RemovedIds);

                    foreach (long idToRemove in entities.RemovedIds)
                    {
                        //TODO: Change EntityList to store the deleted entities VersionNumber.
                    }
                }

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, CommonSettings.DefaultTransactionOptions))
                {
                    // Only call persistence if it has something to do
                    if (entitiesToInsert != null || entitiesToUpdate != null || entitiesToDelete != null)
                    {
                        T entityToDelete = null;

                        if (entitiesToDelete != null && entitiesToDelete.Count > 0)
                        {
                            foreach (long id in entitiesToDelete)
                            {
                                entityToDelete = GetById(id);

                                DeleteChildren(entityToDelete);

                                OnAfterDelete(entityToDelete);
                            }
                        }

                        Stopwatch watch = null;

                        if (!Debugger.IsAttached)
                        {
                            watch = new Stopwatch();

                            watch.Start();
                        }

                        using (DbCommand command = Database.GetStoredProcCommand(procedureName))
                        {
                            ++CommonSettings.TotalDatabaseHit;

                            SqlCommand sqlCommand = (SqlCommand)command;

                            T ent = new T();

                            var insertParam = sqlCommand.Parameters.AddWithValue("@ValuesC", entitiesToInsert);
                            var updateParam = sqlCommand.Parameters.AddWithValue("@ValuesU", entitiesToUpdate);
                            var deleteParam = sqlCommand.Parameters.AddWithValue("@ValuesD", entitiesToDelete);

                            sqlCommand.Parameters.AddWithValue("@CreateAuthor", ctx.SessionInfo.UserCode);
                            sqlCommand.Parameters.Add(new SqlParameter() { ParameterName = "@CreateDate", Value = ctx.ClientApplicationDateTime, SqlDbType = SqlDbType.DateTime2 });

                            insertParam.SqlDbType = SqlDbType.Structured;
                            updateParam.SqlDbType = SqlDbType.Structured;
                            deleteParam.SqlDbType = SqlDbType.Structured;

                            using (IDataReader dr = Database.ExecuteReader(command))
                            {
                                if (entitiesToInsert != null)
                                {
                                    // Creates insert log
                                    int insertCount = entitiesToInsert.Count;

                                    for (int i = 0; i < insertCount; i++)
                                    {
                                        T item = entitiesToInsert[i];
                                        dr.Read();

                                        if (!isReferenceData)
                                        {
                                            item.Id = dr.GetInt64(0);
                                            item.VersionNumber = 1;
                                        }

                                        item.WasNew = true;
                                        item.IsDirty = false;
                                    }
                                }

                                if (entitiesToUpdate != null)
                                {
                                    dr.NextResult();

                                    foreach (T item in entitiesToUpdate)
                                    {
                                        dr.Read();

                                        if (item.Id != dr.GetInt64(0))
                                            throw new NotSupportedException("It should be the same Id");

                                        item.VersionNumber = dr.GetInt64(1);
                                        item.IsDirty = false;
                                    }
                                }
                            }
                        }

                        if (!Debugger.IsAttached)
                        {
                            watch.Stop();
                        }

                    }



                    SaveChildren(entities);


                    RepositoryContext repositoryCtx = RepositoryContext.Current;

                    if (entitiesToInsert != null && entitiesToInsert.Count > 0)
                    {
                        if (entitiesToInsert[0].HasMultiLanguageAttributes)
                        {
                            List<MultiLanguageEntry> entries = new List<MultiLanguageEntry>();
                            foreach (T entity in entitiesToInsert)
                            {
                                entries.AddRange(entity.GetMultiLanguageEntries());
                            }

                            EntityResourcesBulkCUD(entitiesToInsert[0].EntityName + "_Resources", entries);
                        }

                        if (repositoryCtx != null)
                        {
                            foreach (T entity in entitiesToInsert)
                            {
                                if (!repositoryCtx.ContainsEntity<T>(entity.Id))
                                    repositoryCtx.AddEntity(_itemType, entity);
                            }
                        }
                    }

                    if (entitiesToUpdate != null && entitiesToUpdate.Count > 0)
                    {
                        if (entitiesToUpdate[0].HasMultiLanguageAttributes)
                        {
                            List<MultiLanguageEntry> entries = new List<MultiLanguageEntry>();
                            foreach (T entity in entitiesToUpdate)
                            {
                                entries.AddRange(entity.GetMultiLanguageEntries());
                            }

                            EntityResourcesBulkCUD(entitiesToUpdate[0].EntityName + "_Resources", entries);
                        }

                        if (repositoryCtx != null)
                        {
                            foreach (T entity in entitiesToUpdate)
                            {
                                if (!repositoryCtx.ContainsEntity<T>(entity.Id))
                                    repositoryCtx.AddEntity(_itemType, entity);
                            }
                        }
                    }


                    scope.Complete();
                }
            }

        }


        protected virtual void SaveChildren(EntityList<T> entities)
        {
            throw new NotImplementedException();
        }

        protected virtual UpdateTable<T> CreateUpdateTable()
        {
            throw new NotImplementedException();
        }

        protected virtual InsertTable<T> CreateInsertTable()
        {
            throw new NotImplementedException();
        }

        protected virtual List<MultiLanguageEntry> CreateMultiLanguageInsertUpdateTable()
        {
            return new MultiLanguageEntryInsertUpdateTable();
        }

        /// <summary>
        /// Saves a list of aggregated entities.
        /// </summary>
        /// <param name="entities">Entities to be saved.</param>
        /// <param name="bt">Business transaction.</param>
        public void SaveAggregationList(long sourceId, AggregationList targetIds, string procedureName)
        {
            if (targetIds == null)
                throw new ArgumentNullException("targetIds");

            ValidateBusinessTransactionContext();

            if (targetIds.Count != 0 ||
                (targetIds.RemovedIds != null && targetIds.RemovedIds.Count != 0) ||
                (targetIds.AddedIds != null && targetIds.AddedIds.Count != 0))
            {
                AggregationsListTable entitiesToInsert = (targetIds.AddedIds != null && targetIds.AddedIds.Count != 0) ? new AggregationsListTable(targetIds.AddedIds) : null;
                AggregationsListTable entitiesToDelete = (targetIds.RemovedIds != null && targetIds.RemovedIds.Count != 0) ? new AggregationsListTable(targetIds.RemovedIds) : null;

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, CommonSettings.DefaultTransactionOptions))
                {
                    using (DbCommand command = Database.GetStoredProcCommand(procedureName))
                    {
                        ++CommonSettings.TotalDatabaseHit;

                        SqlCommand sqlCommand = (SqlCommand)command;

                        SqlParameter sourceIdParam = sqlCommand.Parameters.AddWithValue("@SourceId", sourceId);
                        SqlParameter insertParam = sqlCommand.Parameters.AddWithValue("@ValuesC", entitiesToInsert);
                        SqlParameter deleteParam = sqlCommand.Parameters.AddWithValue("@ValuesD", entitiesToDelete);

                        sourceIdParam.SqlDbType = SqlDbType.BigInt;
                        insertParam.SqlDbType = SqlDbType.Structured;
                        deleteParam.SqlDbType = SqlDbType.Structured;

                        using (IDataReader dr = Database.ExecuteReader(command))
                        {
                            targetIds.Clear();

                            while (dr.Read())
                            {
                                targetIds.Add(dr.GetInt64(0));
                            }

                            targetIds.MarkAsSaved();
                        }
                    }

                    scope.Complete();
                }
            }


        }

        /// <summary>
        /// Saves a list of aggregated entities.
        /// </summary>
        /// <param name="entities">Entities to be saved.</param>
        /// <param name="bt">Business transaction.</param>
        public void DeleteAggregationList(long sourceId, IList<long> targetIds, string procedureName)
        {
            ValidateBusinessTransactionContext();

            if (targetIds != null && targetIds.Count > 0)
            {
                AggregationsListTable entitiesToDelete = new AggregationsListTable(targetIds);// (targetIds.RemovedIds != null && targetIds.RemovedIds.Count != 0) ? new AggregationsListTable(targetIds.RemovedIds) : null;

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, CommonSettings.DefaultTransactionOptions))
                {
                    using (DbCommand command = Database.GetStoredProcCommand(procedureName))
                    {
                        ++CommonSettings.TotalDatabaseHit;

                        SqlCommand sqlCommand = (SqlCommand)command;

                        SqlParameter sourceIdParam = sqlCommand.Parameters.AddWithValue("@SourceId", sourceId);
                        SqlParameter insertParam = sqlCommand.Parameters.AddWithValue("@ValuesC", null);
                        SqlParameter deleteParam = sqlCommand.Parameters.AddWithValue("@ValuesD", entitiesToDelete);

                        sourceIdParam.SqlDbType = SqlDbType.BigInt;
                        insertParam.SqlDbType = SqlDbType.Structured;
                        deleteParam.SqlDbType = SqlDbType.Structured;

                        using (IDataReader dr = Database.ExecuteReader(command))
                        {

                        }
                    }

                    scope.Complete();
                }
            }
        }

        protected virtual string GetCUDProcedureName()
        {
            return null;
        }


        /// <summary>
        /// Guarda a entidade.
        /// </summary>
        /// <param name="trn">Transaction context.</param>
        /// <param name="entityToSave">Entidade a ser guardada.</param>
        /// <param name="forceUpSert">Indica se caso seja disparada uma Exception de PrimaryKeyConstraint
        /// seja chamado o método de Update da entidade.</param>
        public virtual void Save(T entityToSave, bool forceUpSert)
        {
            if (entityToSave == null)
                throw new ArgumentNullException("entityToSave");

            ValidateBusinessTransactionContext();

            try
            {
                entityToSave.IsAccessingPersistence = true;

                if (forceUpSert)
                {

                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, CommonSettings.DefaultTransactionOptions))
                    {
                        if (entityToSave.IsNew)
                        {
                            Insert(entityToSave);
                        }
                        else
                        {
                            Update(entityToSave);
                        }

                        scope.Complete();
                    }

                    //if (insertHasError)
                    //{
                    //    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, CommonSettings.DefaultTransactionOptions))
                    //    {
                    //        Update(entityToSave);
                    //        scope.Complete();
                    //    }
                    //}
                }
                else
                {
                    //RSR: Commented the transaction scope because the Insert and the Update will create one
                    //using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, BaseEntity.DefaultTransactionOptions))
                    //{
                    if (entityToSave.IsNew)
                    {
                        Insert(entityToSave);

                    }
                    else
                    {
                        Update(entityToSave);
                    }
                    //    scope.Complete();
                    //}
                }

                if (entityToSave.HasMultiLanguageAttributes)
                {
                    EntityResourcesBulkCUD(entityToSave.EntityName + "_Resources", entityToSave.GetMultiLanguageEntries());
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                entityToSave.IsAccessingPersistence = false;
            }
        }


        private void EntityResourcesBulkCUD(string tableName, List<MultiLanguageEntry> entries)
        {
            if (string.IsNullOrWhiteSpace(tableName))
                throw new ArgumentNullException("tableName");

            if (entries == null)
                throw new ArgumentNullException("entries");

            if (!BusinessTransaction.HasCurrentContext)
                throw new InvalidOperationException("There is no BusinessTransactionContext set!");

            BusinessTransaction ctx = BusinessTransaction.CurrentContext;

            if (entries.Count != 0)
            {
                string procedureName = tableName + "BulkCUD";

                MultiLanguageEntryInsertUpdateTable entitiesToInsert = new MultiLanguageEntryInsertUpdateTable();
                DeleteTable entitiesToDelete = null;

                int counter = 0;
                foreach (MultiLanguageEntry ent in entries)
                {
                    ent.PersistOrder = counter++;
                    entitiesToInsert.Add(ent);
                }

                if (entitiesToInsert.Count == 0)
                    entitiesToInsert = null;

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, CommonSettings.DefaultTransactionOptions))
                {
                    // Only call persistence if it has something to do
                    if (entitiesToInsert != null || entitiesToDelete != null)
                    {
                        Stopwatch watch = null;

                        if (!Debugger.IsAttached)
                        {
                            watch = new Stopwatch();

                            watch.Start();
                        }

                        using (DbCommand command = Database.GetStoredProcCommand(procedureName))
                        {
                            ++CommonSettings.TotalDatabaseHit;

                            SqlCommand sqlCommand = (SqlCommand)command;

                            T ent = new T();

                            var insertParam = sqlCommand.Parameters.AddWithValue("@ValuesC", entitiesToInsert);
                            var deleteParam = sqlCommand.Parameters.AddWithValue("@ValuesD", entitiesToDelete);

                            insertParam.SqlDbType = SqlDbType.Structured;
                            deleteParam.SqlDbType = SqlDbType.Structured;

                            using (IDataReader dr = Database.ExecuteReader(command))
                            {
                                if (entitiesToInsert != null)
                                {
                                    // Creates insert log
                                    int insertCount = entitiesToInsert.Count;

                                    // Adds insert log to result set list
                                    for (int i = 0; i < insertCount; i++)
                                    {
                                        MultiLanguageEntry item = entitiesToInsert[i];
                                        dr.Read();
                                    }
                                }
                            }
                        }

                        if (!Debugger.IsAttached)
                        {
                            watch.Stop();
                        }
                    }

                    scope.Complete();
                }
            }
        }

        /// <summary>
        /// Atualiza uma entidade.
        /// </summary>
        /// <param name="entity">Entidade a ser atualizada.</param>
        public virtual void Update(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            if (!IsUpdateProcDefined)
                throw new NotSupportedException();

            ValidateBusinessTransactionContext();



            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, CommonSettings.DefaultTransactionOptions))
            {
                OnBeforeUpdate(entity);

                //Only saves when entity has changes
                if (entity.IsDirty)
                {
                    Stopwatch watch = null;

                    if (!Debugger.IsAttached)
                    {
                        watch = new Stopwatch();
                        watch.Start();
                    }

                    using (DbCommand command = Database.GetStoredProcCommand(UpdateProc))
                    {
                        ++CommonSettings.TotalDatabaseHit;

                        AddUpdateParameters(command, entity);

                        FillMetadataToUpdate(command, entity);

                        try
                        {
                            entity.VersionNumber = (Int64)Database.ExecuteScalar(command);
                            entity.IsDirty = false;
                        }
                        catch (Exception ex)
                        {
                            if (ex.Message != null && ex.Message.IndexOf("***ERROR234") > -1)
                            {
                                throw new ConcurrentUpdateException(entity, GetById(entity.Id), GetById(entity.Id, true), ex);
                            }
                            else
                            {
                                throw;
                            }
                        }
                    }

                    if (!Debugger.IsAttached)
                    {
                        watch.Stop();
                    }

                }

                if (!entity.AvoidUpdateChildren)
                    SaveChildren(entity);

                RefreshResources(entity);

                scope.Complete();
            }

            OnEntityUpdated(entity);

            RepositoryContext ctx = RepositoryContext.Current;

            if (ctx != null && !ctx.ContainsEntity<T>(entity.Id))
                ctx.AddEntity(_itemType, entity);


        }


        /// <summary>
        /// Insere / Atualiza os "filhos" da entidade.
        /// </summary>
        /// <param name="entity">Entidade atualizada.</param>
        protected virtual void SaveChildren(T entity)
        {
        }

        /// <summary>
        /// Updates the reference of the entity supplied in the arguments.
        /// </summary>
        /// <typeparam name="TCache">Entity in cache type.</typeparam>
        /// <param name="entityMethod">Method that will return the entity that may hold the reference.</param>
        /// <param name="cachedEntityMethod">Method that will return the reference.</param>
        /// <param name="args">Cached entity arguments.</param>
        protected void UpdateCachedReferences<TCache>(RepositoryEventArgs<TCache> args,
            Func<RepositoryEventArgs<TCache>, T> entityMethod, Func<T, TCache> cachedEntityMethod,
            Action<T, TCache> updateReferenceMethod)
            where TCache : BaseEntity
        {
            if (UseCache())
            {
                lock (CACHE_LOCK_OBJECT)
                {
                    T entity = entityMethod.Invoke(args);

                    if (entity != null)
                    {
                        TCache cachedEntity = cachedEntityMethod.Invoke(entity);

                        switch (args.CRUDOperation)
                        {
                            case RepositoryCRUDOperation.Create:
                            case RepositoryCRUDOperation.Update:

                                // Only switch versions if it does not have the updated version
                                if (!ReferenceEquals(cachedEntity, args.TypedEntity))
                                {
                                    updateReferenceMethod.Invoke(entity, args.TypedEntity);
                                }

                                break;

                            case RepositoryCRUDOperation.Delete:

                                // Removes cached Service from list
                                updateReferenceMethod.Invoke(entity, null);
                                break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Updates the reference in a list of the entity supplied in the arguments.
        /// </summary>
        /// <typeparam name="TCache">Entity in cache type.</typeparam>
        /// <param name="entityMethod">Method that will return the entity that may hold the reference.</param>
        /// <param name="cachedListMethod">Method that will return the list of the entity that may hold the reference.</param>
        /// <param name="args">Cached entity arguments.</param>
        protected void UpdateCachedReferences<TCache>(RepositoryEventArgs<TCache> args, Func<RepositoryEventArgs<TCache>, T> entityMethod, Func<T, IList<TCache>> cachedListMethod)
            where TCache : BaseEntity
        {

        }

        protected override bool UseCache()
        {
            return base.UseCache() && UseEntityCache();
        }

        /// <summary>
        /// Indicates if it should use cache for the entity.
        /// </summary>
        protected virtual bool UseEntityCache()
        {
            return false;
        }

        public virtual List<String> getObjectNamesWhichHaveReferences2me()
        {
            List<String> toRet = new List<String>();
            return toRet;
        }



        #endregion Methods

        #region IEntityRepository Members

        object IEntityRepository.ExecuteScalar(RepositoryParameterCollection parameters, string procName)
        {
            return ExecuteScalar(parameters, procName);
        }

        public T GetById(long id)
        {
            return GetById(id, false);
        }

        public virtual T GetById(long id, bool ignoreCaches)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region ICacheItemRefreshAction Members

        public void Refresh(string removedKey, object expiredValue, Microsoft.Practices.EnterpriseLibrary.Caching.CacheItemRemovedReason removalReason)
        {
            _ids.Remove(((BaseEntity)expiredValue).Id);
        }

        #endregion


    }
}
