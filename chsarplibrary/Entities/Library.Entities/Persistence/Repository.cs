﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Entities.Base;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Library.Util;
using Library.Util.Transactions;
using Library.Util.Collections;
using System.Diagnostics;
using System.Threading;
using Library.Entities.Collections;
using System.Configuration;
using Library.Util.Text;
using Microsoft.SqlServer.Server;

namespace Library.Entities.Persistence
{
    public abstract class Repository
    {
        public virtual bool ShouldCreateStoredProcedureLogs
        {
            get { return true; }
        }

        /// <summary>
        /// This event is triggered when any Repository triggers the CRUDOperation event.
        /// </summary>
        public static event RepositoryEventHandler RepositoryEvent;

        public static void OnRepositoryEvent(RepositoryEventArgs args)
        {
            if (RepositoryEvent != null)
            {
                RepositoryEvent(args);
            }
        }

        public static void ExecuteReader(string databaseName, string procedureName, Action<IDataReader> processDr)
        {
            ExecuteReader(databaseName, procedureName, null, processDr);
        }

        public static void ExecuteReader(string databaseName, string procedureName, RepositoryParameterCollection prms, Action<IDataReader> processDr)
        {
            ExecuteReader(databaseName, procedureName, prms, null, processDr);
        }

        protected void ValidateBusinessTransactionContext()
        {
            if (!BusinessTransaction.HasCurrentContext)
                throw new InvalidOperationException("There is no BusinessTransaction set.");
        }

        public static void ExecuteReader(string databaseName, string procedureName, RepositoryParameterCollection prms, Action<DbCommand> processCmd, Action<IDataReader> processDr)
        {
            //ValidateBusinessTransactionContext();

            Database db = DatabaseFactory.CreateDatabase(databaseName);

            using (DbCommand cmd = db.GetStoredProcCommand(procedureName))
            {
                ++CommonSettings.TotalDatabaseHit;

                if (prms != null && prms.Count > 0)
                {
                    AddRepositoryParameters(db, prms, cmd);
                }

                if (processCmd != null)
                    processCmd(cmd);

                using (DbConnection conn = db.CreateConnection())
                {
                    cmd.Connection = conn;

                    conn.Open();

                    using (IDataReader dr = cmd.ExecuteReader())
                    {
                        processDr(dr);
                    }
                }
            }
        }

        public static void ExecuteQueryReader(string databaseName, string query, RepositoryParameterCollection prms, Action<IDataReader> processDr)
        {
            Database db = DatabaseFactory.CreateDatabase(databaseName);

            using (DbCommand cmd = db.GetSqlStringCommand(query))
            {
                if (prms != null && prms.Count > 0)
                {
                    AddRepositoryParameters(db, prms, cmd);
                }

                using (DbConnection conn = db.CreateConnection())
                {
                    cmd.Connection = conn;

                    conn.Open();

                    using (IDataReader dr = cmd.ExecuteReader())
                    {
                        processDr(dr);
                    }
                }
            }
        }

        protected static void AddRepositoryParameters(Database db, RepositoryParameterCollection parameters, DbCommand command)
        {
            if (parameters != null && parameters.Count > 0)
            {
                foreach (RepositoryParameter prm in parameters)
                {
                    if (prm.DbType == DbType.Object)
                    {
                        SqlParameter sqlParam = new SqlParameter();
                        sqlParam.ParameterName = prm.ParameterName;
                        sqlParam.SqlDbType = SqlDbType.Structured;
                        sqlParam.Value = prm.Value;
                        sqlParam.SourceVersion = DataRowVersion.Default;
                        sqlParam.Direction = prm.Direction;

                        command.Parameters.Add(sqlParam);
                    }
                    else
                    {
                        SqlParameter sqlParam = new SqlParameter();
                        sqlParam.ParameterName = prm.ParameterName;
                        sqlParam.DbType = prm.DbType;
                        sqlParam.Value = prm.Value;
                        sqlParam.SourceVersion = prm.SourceVersion;
                        sqlParam.SourceColumn = prm.SourceColumn;
                        sqlParam.Direction = prm.Direction;
                        sqlParam.Size = prm.Size;

                        command.Parameters.Add(sqlParam);

                        //db.AddParameter(command, prm.ParameterName, prm.DbType, prm.Direction, prm.SourceColumn,
                        //                  prm.SourceVersion, prm.Value);
                    }
                }
            }
        }

        public virtual void Save(IEnumerable<BaseEntity> ents)
        {
            throw new NotImplementedException();
        }

        protected object GetDbValue(object value)
        {
            return GetDbValue(value, false);
        }

        protected object GetDbValue(object value, bool addJokerCharToTextFields)
        {
            object dbValue = value;

            if (value != null)
            {
                if (value is IMultiLanguageAttribute)
                {
                    if (((IMultiLanguageAttribute)value).CurrentValue != null)
                    {
                        dbValue = ((IMultiLanguageAttribute)value).CurrentValue;
                    }
                    else
                    {
                        dbValue = DBNull.Value;
                    }

                }

                else if (value is DateTime)
                {
                    DateTime dateTimeVal = (DateTime)dbValue;
                    dateTimeVal = dateTimeVal.ToUniversalTime();
                }

                else if (value is AggregationList)
                {
                    AggregationList agg = (AggregationList)value;
                    if (agg.Count == 0)
                        dbValue = null;
                    else
                        dbValue = value;
                }

                else if (value is IEnumerable<SqlDataRecord>)
                {
                    IEnumerable<SqlDataRecord> agg = (IEnumerable<SqlDataRecord>)value;
                    if (agg.Count() == 0)
                        dbValue = null;
                    else
                        dbValue = value;
                }

                if (value is String && addJokerCharToTextFields)
                    dbValue = String.Format("%{0}%", value);
            }
            else
            {
                dbValue = DBNull.Value;
            }

            return dbValue;
        }
    }

    public abstract class Repository<T> : Repository
        where T : BaseEntity, new()
    {
        #region Fields

        protected Type _itemType;

        protected readonly object CACHE_LOCK_OBJECT = new object();

        protected static readonly bool _useCache = ConfigurationManager.AppSettings[USE_CACHE_KEY] != null && Convert.ToBoolean(ConfigurationManager.AppSettings[USE_CACHE_KEY]);

        private const string USE_CACHE_KEY = "UseRepositoryCache";

        private const string PING_STORED_PROC_NAME = "[dbo].[Ping]";

        private Database databaseObj;

        #endregion Fields

        #region Constructors

        public Repository(String databaseName)
            : this()
        {
            databaseObj = DatabaseFactory.CreateDatabase(databaseName);
            this._itemType = typeof(T);
        }

        public Repository()
        {

        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Database to where commands are executed
        /// </summary>
        public Database Database
        {
            get
            {
                return databaseObj;
            }
            set
            {
                databaseObj = value;
            }
        }

        #endregion Properties

        #region Methods

        public virtual void Ping(int sleep)
        {
            RepositoryParameterCollection prms = new RepositoryParameterCollection();

            prms.AddInParameter("Sleep", DbType.String, new TimeSpan(0, 0, 0, 0, sleep).ToString());

            ExecuteNonQuery(prms, PING_STORED_PROC_NAME);
        }

        public void RefreshDatabase()
        {
            databaseObj = null;
        }

        /// <summary>
        /// Adds the new entity to cache.
        /// </summary>
        /// <param name="entityToAdd">Entity to add.</param>
        /// <returns>True or False</returns>
        protected virtual void AddToCache(T entityToAdd)
        {
        }

        /// <summary>
        /// Checks if the IDataReader row its from an already cached entity.
        /// </summary>
        /// <param name="dr">IDataReader to check.</param>
        /// <returns>Entity in cache or a null reference.</returns>
        protected T CheckDataReaderRowForCachedEntity(IDataReader dr, bool ignoreCaches)
        {
            T entity = null;

            // Looks for the record Id to look up on the Cache
            if (columnExists(dr, "Id"))
            {
                entity = CheckDataReaderRowForCachedEntity(dr, dr.GetInt64(dr.GetOrdinal("Id")), ignoreCaches);
            }

            return entity;
        }

        /// <summary>
        /// Checks if the IDataReader row its from an already cached entity.
        /// </summary>
        /// <param name="dr">IDataReader to check.</param>
        /// <param name="recordId">Row record id.</param>
        /// <returns>Entity in cache or a null reference.</returns>
        private T CheckDataReaderRowForCachedEntity(IDataReader dr, long recordId, bool ignoreCaches)
        {
            T entity = null;

            if (!ignoreCaches)
            {
                RepositoryContext ctx = RepositoryContext.Current;

                if (UseCache() && HasEntityInCache(recordId))
                    entity = GetCachedEntity(recordId);
                else if (ctx != null && ctx.ContainsEntity(_itemType, recordId))
                    entity = ctx.GetEntity<T>(_itemType, recordId);
            }

            return entity;
        }

        protected virtual int ExecuteNonQuery(string procName)
        {
            return ExecuteNonQuery(null, procName);
        }

        public virtual int ExecuteNonQuery(RepositoryParameterCollection parameters, string procName)
        {
            int returnCount = 0;

            Stopwatch watch = new Stopwatch();

            watch.Start();

            using (DbCommand command = Database.GetStoredProcCommand(procName))
            {
                ++CommonSettings.TotalDatabaseHit;

                AddRepositoryParameters(Database, parameters, command);

                returnCount = Database.ExecuteNonQuery(command);
            }

            watch.Stop();

            return returnCount;
        }

        protected virtual IDataReader ExecuteReader(string procName)
        {
            return ExecuteReader(null, procName);
        }

        protected virtual IDataReader ExecuteReader(RepositoryParameterCollection parameters,
            string procName)
        {
            using (DbCommand command = Database.GetStoredProcCommand(procName))
            {
                ++CommonSettings.TotalDatabaseHit;

                AddRepositoryParameters(Database, parameters, command);

                return Database.ExecuteReader(command);
            }
        }

        protected virtual ReturnValueType ExecuteScalar<ReturnValueType>(string procName)
        {
            return ExecuteScalar<ReturnValueType>(null, procName);
        }

        protected virtual ReturnValueType ExecuteScalar<ReturnValueType>(RepositoryParameterCollection parameters, string procName)
        {
            ReturnValueType returnValue = default(ReturnValueType);

            //int returnedCount = 0;

            Stopwatch watch = new Stopwatch();

            watch.Start();

            using (DbCommand command = Database.GetStoredProcCommand(procName))
            {
                ++CommonSettings.TotalDatabaseHit;

                AddRepositoryParameters(Database, parameters, command);

                try
                {
                    object returnResult = Database.ExecuteScalar(command);

                    if (!Convert.IsDBNull(returnResult))
                    {
                        returnValue = (ReturnValueType)returnResult;
                        //returnedCount = 1;
                    }
                }
                catch (Exception e)
                {
                    throw CreateDetailException(e, parameters, procName, command);
                }
            }

            watch.Stop();

            return returnValue;
        }

        protected virtual ReturnValueType ExecuteScalarQuery<ReturnValueType>(RepositoryParameterCollection parameters, string query)
        {
            ReturnValueType returnValue = default(ReturnValueType);

            //int returnedCount = 0;

            Stopwatch watch = new Stopwatch();

            watch.Start();

            using (DbCommand command = Database.GetSqlStringCommand(query))
            {
                ++CommonSettings.TotalDatabaseHit;

                AddRepositoryParameters(Database, parameters, command);

                try
                {
                    object returnResult = Database.ExecuteScalar(command);

                    if (!Convert.IsDBNull(returnResult))
                    {
                        returnValue = (ReturnValueType)returnResult;
                        //returnedCount = 1;
                    }
                }
                catch (Exception e)
                {
                    throw CreateDetailException(e, parameters, query, command);
                }
            }

            watch.Stop();

            return returnValue;
        }

        protected virtual object ExecuteScalar(RepositoryParameterCollection parameters, string procName)
        {
            return ExecuteScalar<object>(parameters, procName);
        }


        public virtual T GetCachedEntity(long? entityId)
        {
            return null;
        }

        public virtual IList<T> GetCachedEntities()
        {
            return null;
        }

        /// <summary>
        /// Return an entity using a database procedure
        /// </summary>
        /// <param name="procName"></param>
        /// <returns>the entity</returns>
        public virtual T GetCustom(string procName)
        {
            return GetCustom(procName, false);
        }

        /// <summary>
        /// Return an entity using a database procedure
        /// </summary>
        /// <param name="procName">Procedure Name</param>
        /// <param name="ignoreCaches">Indicates if it should go to persistence even if the Entity its in the Repository Context.</param>
        /// <returns>Entity found in persistence or a null reference.</returns>
        public virtual T GetCustom(string procName, bool ignoreCaches)
        {
            return GetCustom(null, procName, ignoreCaches);
        }

        /// <summary>
        /// Return an entity using a database procedure with parameters
        /// </summary>
        /// <param name="parameters">Procedure's parameters - can be null</param>
        /// <param name="procName">Procedure Name</param>
        /// <returns>Entity found in persistence or a null reference.</returns>
        public virtual T GetCustom(RepositoryParameterCollection parameters, string procName)
        {
            return GetCustom(parameters, procName, false);
        }

        /// <summary>
        /// Return an entity using a database procedure with parameters
        /// </summary>
        /// <param name="parameters">Procedure's parameters - can be null</param>
        /// <param name="procName">Procedure Name</param>
        /// <param name="ignoreCaches">Indicates if it should go to persistence even if the Entity its in the Repository Context.</param>
        /// <returns>Entity found in persistence or a null reference.</returns>
        public virtual T GetCustom(RepositoryParameterCollection parameters, string procName, bool ignoreCaches)
        {
            return GetCustom(parameters, procName, ignoreCaches, false);
        }

        /// <summary>
        /// Return an entity using a database procedure with parameters
        /// </summary>
        /// <param name="procName">Procedure Name</param>
        /// <param name="ignoreCaches">Indicates if it should go to persistence even if the Entity its in the Repository Context.</param>
        /// <param name="getAuditFields">Indicates if it should get the audit fields.</param>
        /// <returns>Entity found in persistence or a null reference.</returns>
        public virtual T GetCustom(string procName, bool ignoreCaches, bool getAuditFields)
        {
            return GetCustom(null, procName, ignoreCaches, getAuditFields);
        }

        /// <summary>
        /// Return an entity using a database procedure with parameters
        /// </summary>
        /// <param name="procName">Procedure Name</param>
        /// <param name="parameters">Procedure's parameters - can be null</param>
        /// <param name="ignoreCaches">Indicates if it should go to persistence even if the Entity its in the Repository Context.</param>
        /// <param name="getAuditFields">Indicates if it should get the audit fields.</param>
        /// <returns>Entity found in persistence or a null reference.</returns>
        public virtual T GetCustom(RepositoryParameterCollection parameters, string procName, bool ignoreCaches, bool getAuditFields)
        {


            T entity = default(T);

            int queryUID = parameters != null ? parameters.GetParameterCollectionHashCode(procName) : 0;

            if (!ignoreCaches && RepositoryContext.Current != null && queryUID != 0)
            {
                if (RepositoryContext.Current.ContainsQueryResult(queryUID))
                {
                    return (T)RepositoryContext.Current.GetQuerySingleResult(queryUID);
                }
            }

            RepositoryParameter idPrm = parameters != null ? parameters.IdParameter : null;

            if (!ignoreCaches && idPrm != null && idPrm.Value is Int64)
            {
                long id = (Int64)idPrm.Value;

                RepositoryContext ctx = RepositoryContext.Current;

                if (UseCache() && HasEntityInCache(id))
                {
                    entity = GetCachedEntity(id);
                }
                else if (ctx != null && ctx.ContainsEntity(_itemType, id))
                {
                    entity = ctx.GetEntity<T>(_itemType, id);
                }
            }

            // If not found in cache
            if (entity == null)
            {
                int columns = 0;

                Stopwatch watch = new Stopwatch();

                watch.Start();

                using (DbCommand command = Database.GetStoredProcCommand(procName))
                {
                    ++CommonSettings.TotalDatabaseHit;

                    AddRepositoryParameters(Database, parameters, command);
                    try
                    {
                        using (IDataReader dr = Database.ExecuteReader(command))
                        {
                            if (dr.Read())
                            {
                                columns = dr.FieldCount;

                                entity = CheckDataReaderRowForCachedEntity(dr, ignoreCaches);

                                if (entity == null)
                                    entity = ProcessDataReaderRow(dr, ignoreCaches, getAuditFields);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        throw CreateDetailException(e, parameters, procName, command);
                    }
                }

                if (queryUID != 0 && RepositoryContext.Current != null)
                {
                    RepositoryContext.Current.AddQuerySingleResult(queryUID, entity);
                }

                watch.Stop();

            }

            return entity;
        }

        /// <summary>
        /// Return an entity using a database procedure with parameters
        /// </summary>
        /// <param name="parameters">Procedure's parameters - can be null</param>
        /// <param name="procName">Procedure Name</param>
        /// <param name="ignoreRepositoryCtx">Indicates if it should go to persistence even if the Entity its in the Repository Context.</param>
        /// <returns>Entity found in persistence or a null reference.</returns>
        protected virtual ResultSetType GetCustom<ResultSetType>(string procName, bool ignoreCaches) where ResultSetType : IFillable, new()
        {
            return GetCustom<ResultSetType>(null, procName, ignoreCaches);
        }

        /// <summary>
        /// Return an entity using a database procedure with parameters
        /// </summary>
        /// <param name="parameters">Procedure's parameters - can be null</param>
        /// <param name="procName">Procedure Name</param>
        /// <param name="ignoreRepositoryCtx">Indicates if it should go to persistence even if the Entity its in the Repository Context.</param>
        /// <returns>Entity found in persistence or a null reference.</returns>
        protected virtual ResultSetType GetCustom<ResultSetType>(RepositoryParameterCollection parameters, string procName, bool ignoreCaches) where ResultSetType : IFillable, new()
        {

            if (String.IsNullOrEmpty(procName))
                throw new ArgumentNullException("procName");

            ResultSetType result = default(ResultSetType);

            int columnReturned = 0;

            Stopwatch watch = new Stopwatch();

            watch.Start();

            using (DbCommand command = Database.GetStoredProcCommand(procName))
            {
                ++CommonSettings.TotalDatabaseHit;

                AddRepositoryParameters(Database, parameters, command);

                using (IDataReader dr = Database.ExecuteReader(command))
                {
                    if (dr.Read())
                    {
                        columnReturned = dr.FieldCount;

                        result = new ResultSetType();

                        result.Fill(dr);
                    }
                }
            }

            watch.Stop();


            return result;
        }

        /// <summary>
        /// Executes the supplied procedure and returns the list of entities of its result set.
        /// </summary>
        /// <param name="procName">Procedure to execute.</param>
        /// <returns>List of entities.</returns>
        public virtual EntityList<T> GetCustomList(string procName)
        {
            return GetCustomList(null, procName, false);
        }

        /// <summary>
        /// Executes the supplied procedure and returns the list of entities of its result set.
        /// </summary>
        /// <param name="procName">Procedure to execute.</param>
        /// <param name="ignoreCaches">Indicates if it should ignore the Repository Context and the Entity cache and go to persistence.</param>
        /// <returns>List of entities.</returns>
        public virtual EntityList<T> GetCustomList(string procName, bool ignoreCaches)
        {
            return GetCustomList(null, procName, ignoreCaches);
        }

        /// <summary>
        /// Executes the supplied procedure and returns the list of entities of its result set.
        /// </summary>
        /// <param name="parameters">List of parameters to supply to the procedure.</param>
        /// <param name="procName">Procedure to execute.</param>
        /// <returns>List of entities.</returns>
        public virtual EntityList<T> GetCustomList(RepositoryParameterCollection parameters, string procName)
        {
            return GetCustomList(parameters, procName, false);
        }

        /// <summary>
        /// Executes the supplied procedure and returns the list of entities of its result set.
        /// </summary>
        /// <param name="parameters">List of parameters to supply to the procedure.</param>
        /// <param name="procName">Procedure to execute.</param>
        /// <param name="ignoreCaches">Indicates if it should ignore the Repository Context and go to persistence.</param>
        /// <returns>List of entities.</returns>
        public EntityList<T> GetCustomList(RepositoryParameterCollection parameters, string procName, bool ignoreCaches)
        {
            return GetCustomList(parameters, procName, false, false);
        }

        /// <summary>
        /// Executes the supplied procedure and returns the list of entities of its result set.
        /// </summary>
        /// <param name="procName">Procedure to execute.</param>
        /// <param name="ignoreCaches">Indicates if it should ignore the Repository Context and go to persistence.</param>
        /// <param name="getAuditFields">Indicates if it should get the audit fields.</param>
        /// <returns>List of entities.</returns>
        public EntityList<T> GetCustomList(string procName, bool ignoreCaches, bool getAuditFields)
        {
            return GetCustomList(null, procName, ignoreCaches, getAuditFields);
        }

        /// <summary>
        /// Executes the supplied procedure and returns the list of entities of its result set.
        /// </summary>
        /// <param name="parameters">List of parameters to supply to the procedure.</param>
        /// <param name="procName">Procedure to execute.</param>
        /// <param name="ignoreCaches">Indicates if it should ignore the Repository Context and go to persistence.</param>
        /// <param name="getAuditFields">Indicates if it should get the audit fields.</param>
        /// <returns>List of entities.</returns>
        public EntityList<T> GetCustomList(RepositoryParameterCollection parameters, string procName, bool ignoreCaches, bool getAuditFields)
        {


            EntityList<T> list = default(EntityList<T>);
            int queryUID = parameters != null ? parameters.GetParameterCollectionHashCode(procName) : 0;

            if (!ignoreCaches && RepositoryContext.Current != null && queryUID != 0)
            {
                if (RepositoryContext.Current.ContainsQueryResult(queryUID))
                {
                    return (EntityList<T>)RepositoryContext.Current.GetQueryResult(queryUID);
                }
            }

            // If it was not found goes to persistence
            if (list == null)
            {
                int requiredTotalRows = int.MaxValue;
                long startIdx = 1;
                long endIdx = long.MaxValue;

                // TODO: Change to a parameter
                Boolean hasPagination = parameters != null && parameters.Exists(p => p.ParameterName == "EndIndex") && parameters.Exists(p => p.ParameterName == "StartIndex");

                if (hasPagination)
                {
                    //Increment endIdx in order to calculate 'has more entities'
                    bool startParsed = long.TryParse(parameters.Find(p => p.ParameterName == "StartIndex").Value.ToString(), out startIdx);
                    bool endParsed = long.TryParse(parameters.Find(p => p.ParameterName == "EndIndex").Value.ToString(), out endIdx);
                    if (startParsed && endParsed && endIdx < int.MaxValue)
                    {
                        requiredTotalRows = (int)(endIdx - startIdx + 1);
                        RepositoryParameter param = parameters.Find(p => p.ParameterName == "EndIndex");
                        endIdx += 1;
                        param.Value = GetDbValue(endIdx);
                    }

                    //Throw exception when StartIdx = 0
                    if (startIdx == 0)
                        throw new Exception(String.Format("StartIndex = 0 in {0}. StartIndex has to be 1.", procName));

                    if (endIdx < startIdx)
                        throw new Exception(String.Format("EndIndex ({1}) cannot be lower than StartIndex ({2}): {0}.", procName, endIdx, startIdx));
                }

                int columnsReturned = 0;

                Stopwatch watch = new Stopwatch();

                watch.Start();

                using (DbCommand command = Database.GetStoredProcCommand(procName))
                {
                    ++CommonSettings.TotalDatabaseHit;

                    AddRepositoryParameters(Database, parameters, command);

                    try
                    {
                        using (IDataReader dr = Database.ExecuteReader(command))
                        {
                            list = new EntityList<T>(new List<T>(), 0, int.MaxValue);

                            if (dr.Read())
                            {
                                columnsReturned = dr.FieldCount;

                                do
                                {
                                    T newObj = null;

                                    //if (!(newObj is BaseReferenceDataEntity))
                                    long recordId = dr.GetInt64(dr.GetOrdinal("Id"));

                                    // Checks for entity on cache
                                    newObj = CheckDataReaderRowForCachedEntity(dr, recordId, ignoreCaches);

                                    // If it was not found in cache
                                    if (newObj == null)
                                    {
                                        newObj = ProcessDataReaderRow(dr, ignoreCaches, getAuditFields);
                                    }

                                    // Adds entity to the list
                                    list.Add(newObj);

                                } while (dr.Read());
                            }

                            if (queryUID != 0 && RepositoryContext.Current != null)
                            {
                                RepositoryContext.Current.AddQueryResult(queryUID, list);
                            }

                            if (hasPagination && list.Count > requiredTotalRows)
                            {
                                list.HasMoreEntities = true;
                                list.RemoveAt(list.Count - 1);
                                list.StartIndex = (int)startIdx;
                                list.EndIndex = (int)(endIdx - 1);
                            }
                            else
                            {
                                list.HasMoreEntities = false;
                            }

                            list.TotalCount = list.Count;
                        }
                    }
                    catch (Exception e)
                    {
                        throw CreateDetailException(e, parameters, procName, command);
                    }
                }

            }

            return list;
        }


        private Exception CreateDetailException(Exception e, RepositoryParameterCollection parameters, string procName, DbCommand command)
        {
            throw new Exception(CreateLogInfo(parameters, procName, command), e);
        }

        private string CreateLogInfo(RepositoryParameterCollection parameters, string procName, DbCommand command)
        {
            var result = new StringBuilder();
            try
            {
                result.Append("===============================================" + Environment.NewLine);
                result.Append("Error in database:" + Environment.NewLine);
                result.Append("Stored Procedure Name: " + procName + Environment.NewLine);
                result.Append("Command Name: " + command.CommandText + Environment.NewLine);

                if (command.Parameters != null)
                {
                    result.Append("# Command Parameters: " + Environment.NewLine);
                    foreach (DbParameter prm in command.Parameters)
                    {
                        try
                        {
                            result.Append(string.Format("Parameter Name:{0} DbType:{1}  Parameter Value:{2} {3} ", prm.ParameterName, prm.DbType, prm.Value != null ? prm.Value : "NULL VALUE", Environment.NewLine));
                        }
                        catch (Exception e)
                        {
                            result.Append(e.Message + e.StackTrace + Environment.NewLine);
                        }
                    }
                }
                else
                    result.Append("# Command Parameters: NOT FOUND!" + Environment.NewLine);

                if (parameters != null)
                {
                    foreach (DbParameter prm in parameters)
                    {
                        try
                        {
                            result.Append(string.Format("Parameter Name:{0} DbType:{1}  Parameter Value:{2} {3} ", prm.ParameterName, prm.DbType, prm.Value != null ? prm.Value : "NULL VALUE", Environment.NewLine));
                        }
                        catch (Exception e)
                        {
                            result.Append(e.Message + e.StackTrace + Environment.NewLine);
                        }
                    }
                }
                result.Append("===============================================" + Environment.NewLine);
            }
            catch (Exception ex)
            {
                result.Append(ex.Message + ex.StackTrace);
            }
            return result.ToString();
        }

        /// <summary>
        /// Executes the supplied procedure and returns the list of entities of its result set.
        /// </summary>
        /// <param name="procName">Procedure to execute.</param>
        /// <param name="ignoreCaches">Indicates if it should ignore the Repository Context and go to persistence.</param>
        /// <returns>List of entities.</returns>
        public EntityList<ResultSetType> GetCustomList<ResultSetType>(string procName, bool ignoreCaches) where ResultSetType : IFillable, IBaseEntity, new()
        {
            return GetCustomList<ResultSetType>(null, procName, ignoreCaches);
        }

        /// <summary>
        /// Executes the supplied procedure and returns the list of entities of its result set.
        /// </summary>
        /// <param name="parameters">List of parameters to supply to the procedure.</param>
        /// <param name="procName">Procedure to execute.</param>
        /// <param name="ignoreCaches">Indicates if it should ignore the Repository Context and go to persistence.</param>
        /// <param name="getAuditFields">Indicates if it should get the audit fields or not.</param>
        /// <returns>List of entities.</returns>
        public EntityList<ResultSetType> GetCustomList<ResultSetType>(RepositoryParameterCollection parameters, string procName, bool ignoreCaches) where ResultSetType : IFillable, IBaseEntity, new()
        {

            EntityList<ResultSetType> list = new EntityList<ResultSetType>(new List<ResultSetType>(), 0, int.MaxValue);

            int queryUID = parameters != null ? parameters.GetParameterCollectionHashCode(procName) : 0;

            if (!ignoreCaches && RepositoryContext.Current != null && queryUID != 0)
            {
                if (RepositoryContext.Current.ContainsQueryResult(queryUID))
                {
                    return (EntityList<ResultSetType>)RepositoryContext.Current.GetQueryResult(queryUID);
                }
            }

            int requiredTotalRows = int.MaxValue;
            if (parameters != null && parameters.Find(p => p.ParameterName == "EndIndex") != null && parameters.Find(p => p.ParameterName == "StartIndex") != null)
            {
                //Increment endIdx in order to calculate 'has more entities'
                int paramStart = 1;
                bool startParsed = int.TryParse(parameters.Find(p => p.ParameterName == "StartIndex").Value.ToString(), out paramStart);
                int paramEnd = int.MaxValue;
                bool endParsed = int.TryParse(parameters.Find(p => p.ParameterName == "EndIndex").Value.ToString(), out paramEnd);
                if (startParsed && endParsed && paramEnd < int.MaxValue)
                {
                    requiredTotalRows = paramEnd - paramStart;
                    RepositoryParameter param = parameters.Find(p => p.ParameterName == "EndIndex");
                    paramEnd += 1;
                    param.Value = GetDbValue(paramEnd);
                }

                list.StartIndex = paramStart;
                list.EndIndex = paramEnd - 1;

                //Throw exception when StartIdx = 0
                if (paramStart == 0)
                    throw new Exception(String.Format("Start Index = 0 in {0}. Start Index has to be 1.", procName));
            }

            int columns = 0;

            Stopwatch watch = new Stopwatch();

            watch.Start();

            using (DbCommand command = Database.GetStoredProcCommand(procName))
            {
                ++CommonSettings.TotalDatabaseHit;

                AddRepositoryParameters(Database, parameters, command);

                using (IDataReader dr = Database.ExecuteReader(command))
                {
                   

                    if (dr.Read())
                    {
                        columns = dr.FieldCount;

                        do
                        {
                            ResultSetType item = new ResultSetType();

                            item.Fill(dr);

                            list.Add(item);
                        }
                        while (dr.Read());
                    }
                }
            }

            if (queryUID != 0 && RepositoryContext.Current != null)
            {
                RepositoryContext.Current.AddQueryResult(queryUID, list);
            }

            watch.Stop();

            if (list.Count > requiredTotalRows)
            {
                list.HasMoreEntities = true;
                list.RemoveAt(list.Count - 1);
            }
            else
            {
                list.HasMoreEntities = false;
            }

            if (!list.HasMoreEntities)
                list.TotalCount = list.Count;

            return list;
        }

     

        public static bool IsNullableEnum(Type t)
        {
            Type u = Nullable.GetUnderlyingType(t);
            return (u != null) && u.IsEnum;
        }

        



        protected virtual bool HasEntityInCache(long id)
        {
            return false;
        }

        /// <summary>
        /// Process a IDataReader row and returns a filled entity.
        /// </summary>
        /// <param name="dr">Opened IDataReader.</param>
        /// <returns>Filled entity.</returns>
        protected T ProcessDataReaderRow(IDataReader dr, bool ignoreCaches, bool getAuditFields)
        {
            T entity = new T();

            entity.Fill(dr);

            if (UseCache() && !ignoreCaches)
            {
                bool addedToCache = false;

                if (!HasEntityInCache(entity.Id))
                {
                    lock (CACHE_LOCK_OBJECT)
                    {
                        if (!HasEntityInCache(entity.Id))
                        {
                            AddToCache(entity);
                            addedToCache = true;
                        }
                    }
                }

                // If not added in cache it means it was added by another thread
                if (!addedToCache)
                    entity = GetCachedEntity(entity.Id);
            }

            RepositoryContext ctx = RepositoryContext.Current;

            if (ctx != null && !ctx.ContainsEntity(_itemType, entity.Id))
                ctx.AddEntity(_itemType, entity);

            return entity;
        }

        /// <summary>
        /// Removes an entity (if found) from cache.
        /// </summary>
        /// <param name="entityId">Entity Id.</param>
        protected virtual void RemoveFromCache(long entityId)
        {
        }

        /// <summary>
        /// Indicates if it should use cache.
        /// </summary>
        /// <returns>True or False.</returns>
        /// <remarks>This method will only return true if there is an AppSetting key UseRepositoryCache with its value set to True and
        /// if the UseEntityCache also returns true.</remarks>
        protected virtual bool UseCache()
        {
            return _useCache;
        }

        private bool columnExists(IDataReader reader, string columnName)
        {
            reader.GetSchemaTable().DefaultView.RowFilter = "ColumnName= '" + columnName + "'";

            return (reader.GetSchemaTable().DefaultView.Count > 0);
        }

        #endregion Methods
    }

    public delegate void RepositoryEventHandler<T>(RepositoryEventArgs<T> args) where T : BaseEntity;

    public delegate void RepositoryEventHandler(RepositoryEventArgs args);

    /// <summary>
    /// Arguments for Repository events.
    /// </summary>
    public class RepositoryEventArgs
    {
        /// <summary>
        /// Initializes a new instance of the RepositoryEventArgs class.
        /// </summary>
        /// <param name="entity">Entity.</param>
        /// <param name="crudOperation">CRUD operation that triggered the event.</param>
        public RepositoryEventArgs(BaseEntity entity, RepositoryCRUDOperation crudOperation)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            Entity = entity;
            CRUDOperation = crudOperation;
        }

        /// <summary>
        /// CRUD operation that triggered the event.
        /// </summary>
        public RepositoryCRUDOperation CRUDOperation { get; private set; }

        /// <summary>
        /// Entity.
        /// </summary>
        public BaseEntity Entity { get; private set; }
    }

    /// <summary>
    /// Typed version for the RepositoryEventArgs class.
    /// </summary>
    /// <typeparam name="T">Entity's type.</typeparam>
    public class RepositoryEventArgs<T> : RepositoryEventArgs where T : BaseEntity
    {
        /// <summary>
        /// Typed version of the Entity property.
        /// </summary>
        public T TypedEntity { get; private set; }

        /// <summary>
        /// Initializes a new instance of the RepositoryEventArgs class.
        /// </summary>
        /// <param name="entity">Entity.</param>
        /// <param name="crudOperation">CRUD operation that triggered the event.</param>
        public RepositoryEventArgs(T entity, RepositoryCRUDOperation crudOperation) :
            base(entity, crudOperation)
        {
            TypedEntity = (T)Entity;
        }
    }

    public enum RepositoryCRUDOperation
    {
        Create,

        Retrieve,

        Update,

        Delete
    }
}
