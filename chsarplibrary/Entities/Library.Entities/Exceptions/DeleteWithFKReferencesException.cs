﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Library.Entities.Base;

namespace Library.Entities.Exceptions
{
    public class DeleteWithFKReferencesException : Exception
    {
        private List<String> _possibleReferences;
        private SqlException _ex;
        private string _message = "";
        public DeleteWithFKReferencesException(SqlException ex, BaseEntity entity, List<String> possibleReferences)
            : base(null, ex)
        {
            _ex = ex;
            _possibleReferences = possibleReferences;

            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("It is not possible to delete the entity with id \"{0}\" of type \"{1}\". Possibly it is being referenced by other entities", entity.Id, entity.GetType().FullName));

            bool first = true;

            foreach (String s in possibleReferences)
            {
                if (!first)
                {
                    sb.AppendLine("<li> ; <b>" + s + "</b></li>");
                }
                else
                {
                    sb.AppendLine(_message = _message + "<li><b>" + s + "</b></li>");
                    first = false;
                }
            }

            _message = sb.ToString();
        }

        public SqlException SqlException
        {
            get
            {
                return _ex;
            }
        }

        public List<String> PossibleReferences
        {
            get
            {
                return _possibleReferences;
            }
        }


        public override string Message
        {
            get
            {
                return _message;
            }
        }
    }
}
