﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Entities.Base;

namespace Library.Entities.Exceptions
{
    public class ConcurrentUpdateException : Exception
    {
        private string _message = "";
        private string _stackTrace = "";
        public const string ERROR_CODE = "234";

        private static String version_author_format = "|VersionAuthor:";
        private static String table_name_format = "|Table:";
        private static String register_id_format = "ERROR234:RegisterId:";
        private static String last_update_date_format = "|DateTime:";
        private static String last_update_transaction_format = "|LatestVersionTransactionId:";
        private static String current_transaction_format = "|CurrentVersionTransactionId:";
        private static String separator = "||";
        private static String table_name_cud_message = "***ERROR234 Não foi feito update a todos os registos do tipo ";

        public ConcurrentUpdateException(BaseEntity entityToMergeFrom, BaseEntity entityToMergeTo, BaseEntity entityToMergeToIgnoringCache, Exception ex)
        {
            _message = ex.Message;
            _stackTrace = ex.StackTrace;
            EntityToMergeFrom = entityToMergeFrom;
            EntityToMergeTo = entityToMergeTo;
            EntityToMergeToIgnoringCache = entityToMergeToIgnoringCache;
        }

        public override string Message
        {
            get
            {
                return _message;
            }
        }

        public override string StackTrace
        {
            get
            {
                return _stackTrace;
            }
        }

        

        public BaseEntity EntityToMergeFrom { get; set; }

        public BaseEntity EntityToMergeTo { get; set; }

        public BaseEntity EntityToMergeToIgnoringCache { get; set; }


        private static string GetString(String errorMessage, String formatStr)
        {
            int beginIdx = errorMessage.IndexOf(formatStr) + formatStr.Length;
            int endIdx = errorMessage.IndexOf(separator, beginIdx);

            if (endIdx > beginIdx)
            {
                return errorMessage.Substring(beginIdx, endIdx - beginIdx);
            }
            else
            {
                return String.Empty;
            }
        }

        public static String GetLastUpdateUserCode(String errorMessage)
        {
            return GetString(errorMessage, version_author_format);
        }

        public static String GetTableName(String errorMessage)
        {
            String tableName = GetString(errorMessage, table_name_format);

            // CUD
            if (String.IsNullOrEmpty(tableName))
            {
                int startIndex = errorMessage.IndexOf(table_name_cud_message) + table_name_cud_message.Length;
                int endIndex = errorMessage.IndexOf("\r\n", startIndex);

                tableName = errorMessage.Substring(startIndex, endIndex - startIndex);
            }

            return tableName;
        }

        public static String GetRegisterId(String errorMessage)
        {
            return GetString(errorMessage, register_id_format);
        }

        public static String GetLastUpdateDate(String errorMessage)
        {
            return GetString(errorMessage, last_update_date_format);
        }

        public static String GetLastUpdateTransactionId(String errorMessage)
        {
            return GetString(errorMessage, last_update_transaction_format);
        }

        public static String GetCurrentUpdateTransactionId(String errorMessage)
        {
            return GetString(errorMessage, current_transaction_format);
        }
    }
}
