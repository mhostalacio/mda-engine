﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Util.Collections;
using Library.Entities.Base;
using Microsoft.SqlServer.Server;
using System.Data;
using System.Collections;

namespace Library.Entities.Collections
{
    [Serializable]
    public class EntityList<T> : PagedList<T>, IEnumerable<SqlDataRecord> where T : IBaseEntity
    {
        private List<Int64> _removedIds = new List<Int64>();

        #region Constructors

        public EntityList()
            : base()
        {
        }

        public EntityList(int count)
            : base(count)
        {
        }

        public EntityList(IEnumerable<T> items)
            : base(items)
        {
        }

        public EntityList(IQueryable<T> source, int currentPage, int pageSize) :
            base(source, currentPage, pageSize)
        {
        }

        public EntityList(List<T> source, int currentPage, int pageSize) :
            base(source, currentPage, pageSize)
        {
        }

        

        #endregion

        protected override void OnAddedItem(T item, int idx)
        {
            base.OnAddedItem(item, idx);

            // Removes the once removed item
            if (_removedIds.Count > 0)
                _removedIds.Remove(item.Id);
        }

        protected override void OnRemovedItem(T item, int idx)
        {
            base.OnRemovedItem(item, idx);

            if (!item.IsNew)
            {
                _removedIds.Add(item.Id);
            }
        }

        /// <summary>
        /// List of removed ids. (It can be null)
        /// </summary>
        public List<Int64> RemovedIds
        {
            get { return _removedIds; }
        }

        IEnumerator<SqlDataRecord> IEnumerable<SqlDataRecord>.GetEnumerator()
        {
            SqlDataRecord sdr = this.Count > 0  ?  this[0].CreateSqlDataRecord() : new SqlDataRecord();

            foreach (T item in this)
            {
                item.SetSqlDataRecord(sdr);

                yield return sdr;
            }
        }

        public T FirstOrDefault()
        {
            if (this.Count > 0)
                return this[0];
            else
                return default(T);
        }

    }
}
