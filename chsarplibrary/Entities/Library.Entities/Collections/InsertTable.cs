﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Entities.Base;

namespace Library.Entities.Collections
{
    public class InsertTable<T> : List<T>
        where T : BaseEntity
    {
        public InsertTable()
        {
        }

        public InsertTable(IEnumerable<T> list) :
            base(list)
        {
        }

        //this method was created to ensure that multilanguage attributes are loaded before calling another sql command
        public virtual void AddAndEnsure(T item)
        {
            this.Add(item);
        }
    }
}
