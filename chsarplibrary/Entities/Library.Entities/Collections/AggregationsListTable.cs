﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Server;
using System.Data;

namespace Library.Entities.Collections
{
    public class AggregationsListTable : List<long>, IEnumerable<SqlDataRecord>
    {
        public AggregationsListTable()
        {

        }

        public AggregationsListTable(IEnumerable<long> list) :
            base(list)
        {
        }

        IEnumerator<SqlDataRecord> IEnumerable<SqlDataRecord>.GetEnumerator()
        {
            var sdr = new SqlDataRecord(
                new SqlMetaData("AggregatedEntityId", SqlDbType.BigInt));


            foreach (long item in this)
            {
                // AggregatedEntityId
                sdr.SetInt64(0, item);

                yield return sdr;
            }
        }
    }
}
