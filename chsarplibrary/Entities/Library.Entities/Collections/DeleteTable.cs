﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Server;
using System.Data;

namespace Library.Entities.Collections
{
    public class DeleteTable : List<long>, IEnumerable<SqlDataRecord>
    {
        public DeleteTable(IEnumerable<long> items)
        {
            AddRange(items);
        }

        IEnumerator<SqlDataRecord> IEnumerable<SqlDataRecord>.GetEnumerator()
        {
            var sdr = new SqlDataRecord(
            new SqlMetaData("Id", SqlDbType.BigInt));

            foreach (long item in this)
            {
                sdr.SetInt64(0, item);

                yield return sdr;
            }
        }
    }
}
