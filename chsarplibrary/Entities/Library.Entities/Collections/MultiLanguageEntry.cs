﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Server;
using System.Data;

namespace Library.Entities.Collections
{
    public class MultiLanguageEntry
    {
        public int PersistOrder { get; set; }

        public long InstanceId { get; set; }

        public string ColumnName { get; set; }

        public string ColumnLanguage { get; set; }

        public string ColumnMainLanguage { get; set; }

        public string ColumnValue { get; set; }
    }

    public class MultiLanguageEntryInsertUpdateTable : List<MultiLanguageEntry>, IEnumerable<SqlDataRecord>
    {
        public MultiLanguageEntryInsertUpdateTable() :
            base()
        {
        }

        public MultiLanguageEntryInsertUpdateTable(IEnumerable<MultiLanguageEntry> list) :
            base(list)
        {
        }

        IEnumerator<SqlDataRecord> IEnumerable<SqlDataRecord>.GetEnumerator()
        {
            var sdr = new SqlDataRecord(
                new SqlMetaData("ColumnName", SqlDbType.VarChar, 50),
                new SqlMetaData("Lang", SqlDbType.VarChar, 5),
                new SqlMetaData("MainLang", SqlDbType.VarChar, 2),
                new SqlMetaData("ObjectId", SqlDbType.BigInt),
                new SqlMetaData("Value", SqlDbType.NVarChar, SqlMetaData.Max));

            foreach (MultiLanguageEntry item in this)
            {
                // ColumnName
                sdr.SetString(0, item.ColumnName);

                // Lang
                sdr.SetString(1, item.ColumnLanguage);

                // MainLang
                sdr.SetString(2, item.ColumnMainLanguage);

                // ObjectId
                sdr.SetInt64(3, item.InstanceId);

                // Value
                sdr.SetString(4, item.ColumnValue);

                yield return sdr;
            }
        }
    }
}
