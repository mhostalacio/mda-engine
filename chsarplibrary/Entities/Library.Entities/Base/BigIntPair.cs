﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Server;
using System.Data;

namespace Library.Entities.Base
{
    public class BigIntPair : BaseEntity
    {
        public Int64 Key { get; set; }

        public Decimal? Value { get; set;  }

        public override string ClassName
        {
            get { return "BigIntPair"; }
        }

        public override SqlDataRecord CreateSqlDataRecord()
        {
            SqlDataRecord sdr = new SqlDataRecord(
                new SqlMetaData("Key", SqlDbType.BigInt),
                new SqlMetaData("Value", SqlDbType.Decimal));

            return sdr;
        }

        public override void SetSqlDataRecord(Microsoft.SqlServer.Server.SqlDataRecord sdr)
        {
            sdr.SetInt64(0, this.Key);
            if (this.Value.HasValue)
                sdr.SetDecimal(1, this.Value.Value);
            else
                sdr.SetDBNull(1);
        }
    }
}
