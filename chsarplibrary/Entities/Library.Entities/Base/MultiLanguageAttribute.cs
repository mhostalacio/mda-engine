﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using Library.Util.Text;
using Library.Util;
using Library.Util.Attributes;
using Library.Util.Transactions;

namespace Library.Entities.Base
{
    public class MultiLanguageAttribute<T> : PersistableAttribute, IMultiLanguageAttribute
    {
        private Dictionary<CultureInfo, T> _values;
        public virtual CultureInfo EditingLanguage { get; private set; }

        public MultiLanguageAttribute()
        {
            
        }

        public MultiLanguageAttribute(T value)
            : this()
        {
            Value = value;
        }

        public virtual void SetEditingLanguage(CultureInfo cult)
        {
            EditingLanguage = cult;
        }

        /// <summary>
        /// Gets or sets the value in the current culture.
        /// </summary>
        public T Value
        {
            get
            {
                if (EditingLanguage != null)
                    return GetValueFromCulture(EditingLanguage);
                return GetValueFromCulture(BusinessTransaction.CurrentContext.CurrentLanguage);
            }
            set
            {
                if (EditingLanguage != null)
                    SetValueFromCulture(EditingLanguage, value);
                SetValueFromCulture(BusinessTransaction.CurrentContext.CurrentLanguage, value);
            }
        }


        /// <summary>
        /// Map between CultureInfo X Value.
        /// </summary>
        public Dictionary<CultureInfo, T> Values
        {
            get
            {
                return _values;
            }
            set
            {
                _values = value;
            }
        }

        /// <summary>
        /// Returns the value from the supplied culture. If no such value exists, the following rules are applied:
        /// 
        ///     1) Return the fallback culture value recursively
        ///     2) Return the default culture value
        ///     3) Return first element
        ///     4) Return an empty string.
        /// 
        /// </summary>
        /// <param name="cultureInfo">Culture to retrieve its value.</param>
        /// <returns>Value for the supplied culture.</returns>
        public T GetValueFromCulture(CultureInfo cultureInfo)
        {
            T value = default(T);

            InitializeValueMap();

            if (cultureInfo == null && BusinessTransaction.CurrentContext.CurrentLanguage != null)
                cultureInfo = BusinessTransaction.CurrentContext.CurrentLanguage;

            if (cultureInfo == null && CommonSettings.DefaultLanguageCode != null)
                cultureInfo = CultureInfo.GetCultureInfo(CommonSettings.DefaultLanguageCode);


            // Check if the provided culture exists
            if (_values.ContainsKey(cultureInfo))
            {
                value = _values[cultureInfo];
            }
            else
            {
                // Check fallback languages
                String masterLanguageCode = cultureInfo.TwoLetterISOLanguageName;
                KeyValuePair<CultureInfo, T> possibleEntry = _values.FirstOrDefault(o => o.Key.TwoLetterISOLanguageName == masterLanguageCode);
                if (possibleEntry.Key != null)
                {
                    value = possibleEntry.Value;
                }
                else
                {
                    //fallback language is english
                    possibleEntry = _values.FirstOrDefault(o => o.Key.TwoLetterISOLanguageName == "en");
                    if (possibleEntry.Key != null)
                    {
                        value = possibleEntry.Value;
                    }
                    else
                    {
                        possibleEntry = _values.FirstOrDefault();
                        value = possibleEntry.Value;
                    }
                }
            }

            return value;
        }

        public T GetValueFromCulture(string cultureInfo)
        {
            CultureInfo cultureInfoObject;

            if (cultureInfo != null)
                cultureInfoObject = new CultureInfo(cultureInfo);
            else
                cultureInfoObject = null;

            return GetValueFromCulture(cultureInfoObject);
        }

        private void InitializeValueMap()
        {
            if (_values == null)
            {
                lock (this)
                {
                    if (_values == null)
                        _values = new Dictionary<CultureInfo, T>();
                }
            }
        }

        /// <summary>
        /// Sets a value from the supplied culture.
        /// </summary>
        /// <param name="cultureInfo">Culture to set its value.</param>
        /// <param name="value">Value to set.</param>
        public void SetValueFromCulture(CultureInfo cultureInfo, T value)
        {
            InitializeValueMap();

            _values[cultureInfo] = value;

            if (!_isDirty)
            {
                OnBecameDirty();
                _isDirty = true;
            }
        }

        /// <summary> 
        /// Converts the typed value to a MultiLanguageAttribute instance.
        /// </summary>
        /// <param name="value">Typed value.</param>
        /// <returns>MultiLanguageAttribute instance.</returns>
        public static implicit operator MultiLanguageAttribute<T>(T value)
        {
            MultiLanguageAttribute<T> locValue = new MultiLanguageAttribute<T>();

            locValue.Value = value;

            return locValue;
        }

        /// <summary>
        /// Converts the MultiLanguageAttribute instance to its typed value.
        /// </summary>
        /// <param name="inst">MultiLanguageAttribute instance.</param>
        /// <returns>Typed value.</returns>
        public static implicit operator T(MultiLanguageAttribute<T> inst)
        {
            T retValue = default(T);

            if (inst != null)
                retValue = inst.Value;

            return retValue;
        }

        /// <summary>
        /// Informs if the values repository has been loaded.
        /// </summary>
        public bool HasValues
        {
            get { return _values != null && _values.Count > 0; }
        }

        #region IMultiLanguageAttribute Members

        object IMultiLanguageAttribute.CurrentValue
        {
            get { return Value; }
        }

        #endregion

        
    }
}
