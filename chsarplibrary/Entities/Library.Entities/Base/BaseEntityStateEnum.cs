﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Entities.Base
{
    public enum BaseEntityStateEnum
    {
        /// <summary>
        /// The entity is new and the system is initializing properties to default values
        /// </summary>
        Initializing = 0,
        /// <summary>
        /// The entity was being loaded from the repository
        /// </summary>
        Loading = 1,
        /// <summary>
        /// The entity is ready
        /// </summary>
        Ready = 2,
        /// <summary>
        /// The entity was being persisted to repository
        /// </summary>
        Persisting = 3,
        /// <summary>
        /// The entity is being serialized
        /// </summary>
        Deserializing = 4
    }
}
