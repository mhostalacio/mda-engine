﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Entities.Base;
using Microsoft.SqlServer.Server;

namespace Library.Entities.Base
{
    public interface IBaseEntity
    {
        Int64 Id { get; set; }

        bool IsNew { get; }

        BaseEntityStateEnum RuntimeState { get; }

        void MarkNew();

        String ClassName { get; }

        SqlDataRecord CreateSqlDataRecord();

        void SetSqlDataRecord(SqlDataRecord sdr);
    }
}
