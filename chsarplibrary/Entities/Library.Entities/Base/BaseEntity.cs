﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Collections;
using Library.Entities.Persistence;
using System.Data;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Library.Entities.Collections;
using System.Configuration;
using Library.Util.Transactions;
using Microsoft.SqlServer.Server;
using Library.Util.AOP;

namespace Library.Entities.Base
{
    /// <summary>
    /// Base class for all application classes
    /// </summary>
    [Serializable]
    public abstract class BaseEntity : IBaseEntity, IFillable
    {
        public BaseEntity()
        {
            Id = GenerateRandomId();
            CreateAuthor = BusinessTransaction.CurrentContext.SessionInfo.UserCode;
            VersionAuthor = BusinessTransaction.CurrentContext.SessionInfo.UserCode;
            CreateDate = DateTime.UtcNow;
            VersionDate = DateTime.UtcNow;
        }

        #region Fields

        private Guid _internalUID;
        [NonSerialized]
        private Dictionary<string, object> _persistenceValues = null;
        private Int64 _id;
        private Int64 _versionNumber;
        private static Int64 NumberResult = 1;
        private bool _isDirty;
        

        #endregion

        #region Events

        /// <summary>
        /// This event its triggered when this entity becomes dirty.
        /// </summary>
        public event EventHandler BecameDirty;

        #endregion

        #region Properties

        public virtual bool AvoidUpdateChildren
        {
            get;
            set;
        }

        public abstract String ClassName { get; }

        [XmlIgnore]
        public Dictionary<string, object>.KeyCollection DirtyProperties
        {
            get
            {
                return (_persistenceValues != null && _persistenceValues.Count > 0) ? _persistenceValues.Keys : null;
            }
        }

        public virtual String EntityName
        {
            get { throw new NotImplementedException(); }
        }

        public virtual bool HasMultiLanguageAttributes { get { return false; } }

        public virtual Int64 Id
        {
            get 
            { 
                return _id; 
            }
            set
            {
                _id = value;
            }
        }

        public bool IsDirty
        {
            get
            {
                bool isDirty = false;

                if (_persistenceValues != null)
                {
                    isDirty = (_persistenceValues != null && _persistenceValues.Count > 0);
                }

                return isDirty;
            }
            set
            {
                _isDirty = value;

                if (value)
                    OnBecameDirty();
                else
                    ClearPersistenceValuesList();
            }
        }

        public virtual bool IsNew
        {
            get { return VersionNumber <= 0; }
        }

        public virtual Boolean InEdition { get; set; }

        /// <summary>
        /// This bag holds the original persistence values so the IsDirty property can
        /// be evaluated.
        /// </summary>
        /// <remarks>This bag is only initialized if there is a change, and contains only the properties that have changed.</remarks>
        [XmlIgnore]
        protected Dictionary<string, object> PersistenceValues
        {
            get
            {
                return _persistenceValues;
            }
        }

        public Int16 PersistOrder { get; set; }

        public BaseEntityStateEnum RuntimeState { get; private set; }

        /// <summary>
        /// Indicates if the entity was just persisted.
        /// </summary>
        public bool WasNew { get; set; }

        public Guid InternalUID
        {
            get { return _internalUID; }
            set { _internalUID = value; }
        }

        ///<summary>
        ///VersionNumber
        ///</summary>
        public virtual Int64 VersionNumber
        {
            get
            {
                return _versionNumber;
            }
            set
            {
                _versionNumber = value;
            }
        }

        ///<summary>
        ///CreateAuthor
        ///</summary>
        public virtual String CreateAuthor
        {
            get;
            set;
        }

        ///<summary>
        ///VersionAuthor
        ///</summary>
        public virtual String VersionAuthor
        {
            get;
            set;
        }

        ///<summary>
        ///CreateDate
        ///</summary>
        public virtual DateTime CreateDate
        {
            get;
            set;
        }

        ///<summary>
        ///VersionDate
        ///</summary>
        public virtual DateTime VersionDate
        {
            get;
            set;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds a value to the persistence bag of values.
        /// </summary>
        /// <param name="name">Name of the property.</param>
        /// <param name="value">Value to add.</param>
        /// <remarks>Only adds the value if its was not already added (because it needs to maintain the original list).</remarks>
        protected void AddPersistenceValue(string name, object value)
        {
            if (_persistenceValues == null)
            {
                _persistenceValues = new Dictionary<string, object>();
            }

            if (!_persistenceValues.ContainsKey(name))
            {
                _persistenceValues.Add(name, value);

                OnBecameDirty();
            }
            else
            {
                // Compares both values and if its equals to the one stored, clears its value (So, when comparing
                // to check if the entity its really dirty, it should only see if there are any keys on the _persistenceValues dictionary
                object persistenceValue = _persistenceValues[name];

                if (CompareProperty(persistenceValue, value))
                    _persistenceValues.Remove(name);
                else
                    OnBecameDirty();
            }
        }

        /// <summary>
        /// Clears the persistence values list.
        /// </summary>
        protected void ClearPersistenceValuesList()
        {
            if (_persistenceValues != null)
                _persistenceValues = null;
        }

        

        protected virtual bool CompareProperty(object v1, object v2)
        {
            if (ReferenceEquals(v1, v2))
                return true;

            if (v1 == null && v2 == null)
                return true;

            if (v1 != null && v2 != null)
            {
                if (v1 is Array && v2 is Array)
                {
                    Array a1 = (Array)v1;
                    Array a2 = (Array)v2;

                    if (a1.Length != a2.Length)
                        return false;

                    for (int i = 0; i < a1.Length; i++)
                    {
                        if (!a1.GetValue(i).Equals(a2.GetValue(i)))
                            return false;
                    }

                    return true;
                }
                else if (v1 is IList && v2 is IList)
                {

                    IList l1 = (IList)v1;
                    IList l2 = (IList)v2;

                    if (l1.Count != l2.Count)
                        return false;

                    for (int i = 0; i < l1.Count; i++)
                    {
                        if (!l1[i].Equals(l2[i]))
                            return false;
                    }

                    return true;
                }
                else
                    return v1.Equals(v2);
            }
            else
                return false;
        }

        //public virtual void Delete()
        //{
        //}

        /// <summary>
        /// Fills this instance with the data from the supplied IDataReader instance.
        /// </summary>
        /// <param name="dr">IDataReader instance.</param>
        public void Fill(IDataReader dr)
        {
            if (dr == null)
                throw new ArgumentNullException("dr");

            MarkOld();

            FillInternal(dr);
        }

        protected virtual void FillInternal(IDataReader dr)
        {

        }

        public static Int64 GenerateRandomId()
        {
            var ramdom = new Random(DateTime.UtcNow.Millisecond);
            NumberResult++;
            var builder = new StringBuilder();
            Int64 result;

            for (int i = 0; i < 9; i++)
                builder.Append(ramdom.Next(0, 9));

            builder.Append(NumberResult.ToString());
            result = Int64.Parse("-" + builder);
            return result;
        }

        public virtual List<MultiLanguageEntry> GetMultiLanguageEntries()
        {
            return null;
        }

        /// <summary>
        /// This field is used to indicate that the instance is being sent or retrieved to the persistence layer.
        /// </summary>
        public bool IsAccessingPersistence { get; set; }

        public virtual void MarkNew()
        {
            VersionNumber = 0;
            Id = BaseEntity.GenerateRandomId();
        }

        public virtual void MarkOld()
        {
            VersionNumber = 1;
        }

        /// <summary>
        /// Triggers the BecameDirty event.
        /// </summary>
        protected virtual void OnBecameDirty()
        {
            if (BecameDirty != null)
                BecameDirty(this, new EventArgs());
        }

        protected virtual void OnAfterPersist()
        {

        }

        //public abstract void Persist();

        public virtual void PrepareDataRecord(Microsoft.SqlServer.Server.SqlDataRecord sdr)
        {
            
        }

        public virtual SqlDataRecord CreateSqlDataRecord()
        {
            return null;
        }

        public virtual void SetSqlDataRecord(SqlDataRecord sdr)
        {
        }

        public static T GetRepository<T>()
            where T : IRepository<T>
        {
            T rep = ServiceLocator.GetService<T>();
            if (rep == null)
            {
                throw new Exception("Could not find the implementation for the interface named " + typeof(T).FullName);
            }
            return rep.GetInstance();
        }

        #endregion
    }
}
