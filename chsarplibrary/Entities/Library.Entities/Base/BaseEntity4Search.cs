﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Collections;
using Library.Util.Transactions;
using System.Data;

namespace Library.Entities.Base
{
    [Serializable]
    public abstract class BaseEntity4Search : IBaseEntity, IFillable
    {
        public enum OrderByDirectionEnum
        {
            ASC,
            DESC
        }

        #region Fields

        private bool _addJokerCharToTextFields = false;
        //private BaseEntity4SearchInfo info;
        private string orderBy;
        private OrderByDirectionEnum orderByDirection;
        private int pageNumber = 0;
        private int pageSize = int.MaxValue;
        private int numberOfPages = 1;
        private bool paginated = true;
        private long? _startIdx = null;
        private long? _endIdx = null;
        private Boolean _ignoreCaches = false;

        #endregion

        #region Properties

        public Boolean IgnoreCaches
        {
            get { return _ignoreCaches; }
            set { _ignoreCaches = value; }
        }

        /// <summary>
        /// Specifies if text fields should have a prefix and sufix '%'.
        /// </summary>
        public bool AddJokerCharToTextFields
        {
            get { return _addJokerCharToTextFields; }
            set { _addJokerCharToTextFields = value; }
        }

        public abstract String ClassName { get; }

        public virtual String EntityName
        {
            get { throw new NotImplementedException(); }
        }

        public long? EndIdx
        {
            get
            {
                long ret = 0;

                if (_endIdx == null)
                {
                    ret = (StartIdx.Value + PageSize * NumberOfPages);
                    if (ret < 0)
                        ret = long.MaxValue;
                }
                else
                    ret = _endIdx.Value;

                return ret;
            }
            set
            { _endIdx = value; }
        }

        public string OrderBy
        {
            get { return orderBy; }
            set { orderBy = value; }
        }

        public OrderByDirectionEnum OrderByDirection
        {
            get { return orderByDirection; }
            set { orderByDirection = value; }
        }

        public int PageNumber
        {
            get { return pageNumber; }
            set { pageNumber = value; }
        }

        public int PageSize
        {
            get { return pageSize; }
            set { pageSize = value; }
        }

        public int NumberOfPages
        {
            get { return numberOfPages; }
            set { numberOfPages = value; }
        }

        public bool Paginated
        {
            get { return paginated; }
            set { paginated = value; }
        }


        public long? StartIdx
        {
            get { return (_startIdx == null) ? 1 : _startIdx.Value; }
            set { _startIdx = value; }
        }

        public int PreviousBlockPageNumber
        {
            get { return (PageNumber < NumberOfPages) ? 1 : NextBlockPageNumber - NumberOfPages; }
        }

        public int NextBlockPageNumber
        {
            get { return (PageNumber < NumberOfPages) ? NumberOfPages + 1 : PageNumber + (NumberOfPages - (PageNumber % NumberOfPages)) + 1; }
        }

        public int RecordsPerBlock
        {
            get { return PageSize * NumberOfPages; }
        }

        public int BlockNumber
        {
            get { return (PageNumber <= NumberOfPages) ? 1 : ((PageNumber % NumberOfPages) == 0) ? PageNumber / NumberOfPages : PageNumber / NumberOfPages + 1; }
        }

        public String SearchCulture
        {
            get
            {
                return new CultureInfo(BusinessTransaction.CurrentContext.SessionInfo.CultureCode).ToString().Split("-".ToCharArray())[0] + "%";
            }
        }


        #endregion

        #region Methods

        protected virtual bool CompareProperty(object v1, object v2)
        {
            if (ReferenceEquals(v1, v2))
                return true;

            if (v1 == null && v2 == null)
                return true;

            if (v1 != null && v2 != null)
            {
                if (v1 is Array && v2 is Array)
                {
                    Array a1 = (Array)v1;
                    Array a2 = (Array)v2;

                    if (a1.Length != a2.Length)
                        return false;

                    for (int i = 0; i < a1.Length; i++)
                    {
                        if (!a1.GetValue(i).Equals(a2.GetValue(i)))
                            return false;
                    }

                    return true;
                }
                else if (v1 is IList && v2 is IList)
                {

                    IList l1 = (IList)v1;
                    IList l2 = (IList)v2;

                    if (l1.Count != l2.Count)
                        return false;

                    for (int i = 0; i < l1.Count; i++)
                    {
                        if (!l1[i].Equals(l2[i]))
                            return false;
                    }

                    return true;
                }
                else
                    return v1.Equals(v2);
            }
            else
                return false;
        }

        protected void AddPersistenceValue(string name, object value)
        {
        }

        public virtual IEnumerable SearchByExample(IEnumerable entities)
        {
            return entities;
        }

        public static string GetStringParameterFromList(List<long> list)
        {
            if (list != null && list.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                foreach (long id in list)
                {
                    sb.Append(id.ToString()).Append(";");
                }
                return sb.ToString().Substring(0, sb.ToString().Length - 1);
            }
            else
            {
                return null;
            }
        }


        public static string GetStringParameterFromList(List<String> list)
        {
            if (list != null && list.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                foreach (string str in list)
                {
                    sb.Append(str).Append(";");
                }
                return sb.ToString().Substring(0, sb.ToString().Length - 1);
            }
            else
            {
                return null;
            }
        }

        #endregion

        public virtual long Id
        {
            get;
            set;
        }

        public Guid InternalUID { get; set; }

        public bool IsNew
        {
            get { return true; }
        }

        public BaseEntityStateEnum RuntimeState
        {
            get { throw new NotImplementedException(); }
        }

        public void Persist()
        {
            throw new NotImplementedException();
        }

        public void MarkNew()
        {
            throw new NotImplementedException();
        }

        public Microsoft.SqlServer.Server.SqlDataRecord CreateSqlDataRecord()
        {
            throw new NotImplementedException();
        }

        public void SetSqlDataRecord(Microsoft.SqlServer.Server.SqlDataRecord sdr)
        {
            throw new NotImplementedException();
        }

        public void Fill(IDataReader dr)
        {
            FillInternal(dr);
        }

        protected virtual void FillInternal(IDataReader dr)
        {
           
        }
    }
}
