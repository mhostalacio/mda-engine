﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Library.Entities.Base
{
    [Serializable]
    public abstract class MultiLanguageBaseEntity : BaseEntity
    {
        public virtual String DefaultLanguage { get; set; }

        public virtual void SetEditingLanguage(CultureInfo cult)
        {
            
        }

        public override string ClassName
        {
            get { throw new NotImplementedException(); }
        }

    }
}
