﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Library.Entities.Base
{
    /// <summary>
    /// Defines a contract for an object that can be filled.
    /// </summary>
    public interface IFillable
    {
        /// <summary>
        /// Fills the object with a DataReader.
        /// </summary>
        /// <param name="dr">DataReader to fill.</param>
        void Fill(IDataReader dr);
    }
}
