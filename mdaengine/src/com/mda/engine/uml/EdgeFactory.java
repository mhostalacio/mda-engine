package com.mda.engine.uml;

import att.grappa.Edge;
import att.grappa.Node;
import att.grappa.Subgraph;


public class EdgeFactory {

	public static Edge buildUMLAssociationEdge(Subgraph subGraph, Node tailNode, Node headNode, String headMultiplicity, String edgeLabel, GraphBuilderOptions options) {
		
		Edge umlAssoc = new Edge(subGraph, tailNode, headNode);
		
		// [Deprecated]Set tail multiplicity
		// umlAssoc.setAttribute("taillabel", tailMultiplicity);
		
		// Set head multiplicity, if defined
		if (headMultiplicity != null)
			umlAssoc.setAttribute("headlabel", headMultiplicity);
		
		// Set edge label
		if (options.getShowRelationshipLabels())
			umlAssoc.setAttribute("label", edgeLabel);
		
		// Set head arrow head
		umlAssoc.setAttribute("arrowhead", "normal");
		
		// Disable constraint (allows both nodes to be on the same rank)
		if (options.getForceSameRankOnAssociations())
			umlAssoc.setAttribute("constraint", "false");
		
		return umlAssoc;
	}
	
	public static Edge buildUMLAssociationEdge(Subgraph subGraph, Node tailNode, String headNodeName, String tailMultiplicity, String edgeLabel, GraphBuilderOptions options) {
		Node headNode = new Node(subGraph, headNodeName);
		return buildUMLAssociationEdge(subGraph, tailNode, headNode, tailMultiplicity, edgeLabel, options);
	}
	
	public static Edge buildUMLGeneralizationEdge(Subgraph subGraph, Node baseNode, Node derivedNode) {
		
		Edge umlGeneral = new Edge(subGraph, baseNode, derivedNode);
		
		// Set tail arrow head
		umlGeneral.setAttribute("arrowtail", "empty");
				
		return umlGeneral;
	}
	
	public static Edge buildUMLGeneralizationEdge(Subgraph subGraph, String baseNodeName, Node derivedNode) {
		Node baseNode = new Node(subGraph, baseNodeName);
		return buildUMLGeneralizationEdge(subGraph, baseNode, derivedNode);		
	}
	
	
	public static Edge buildUMLAggregationEdge(Subgraph subGraph, Node sourceNode, Node dependentNode, String sourceMultiplicity, String sourceLabel, String dependentMultiplicity, String dependentLabel, GraphBuilderOptions options) {
		
		Edge umlAggreg = new Edge(subGraph, sourceNode, dependentNode);
		
		// Set source arrow head
		umlAggreg.setAttribute("arrowtail", "odiamond");
		
		// Set source label (multiplicity + label), if defined
		String tailLabel = null;
		if (sourceMultiplicity != null)
			tailLabel = sourceMultiplicity;
		if (sourceLabel != null && options.getShowRelationshipLabels())
			tailLabel += " (" + sourceLabel + ")";		
		if (tailLabel != null)	
			umlAggreg.setAttribute("taillabel", tailLabel);
		
		// Set dependent label (multiplicity + label), if defined
		String headLabel = null;
		if (dependentMultiplicity != null)
			headLabel = dependentMultiplicity;
		if (dependentLabel != null && options.getShowRelationshipLabels())
			umlAggreg.setAttribute("label", dependentLabel);
			//headLabel += " (" + dependentLabel +")";		
		if (headLabel != null)
			umlAggreg.setAttribute("headlabel", headLabel);
				
		return umlAggreg;
	}
	
	public static Edge buildUMLAggregationEdge(Subgraph subGraph, Node sourceNode, String dependentNodeName, String sourceMultiplicity, String dependentMultiplicity) {
		Node dependentNode = new Node(subGraph, dependentNodeName);
		return buildUMLAggregationEdge(subGraph, sourceNode, dependentNode, sourceMultiplicity, null, dependentMultiplicity, null, null);
	}
	
	public static Edge buildUMLCompositionEdge(Subgraph subGraph, Node sourceNode, Node dependentNode, String sourceMultiplicity, String sourceLabel, String dependentMultiplicity, String dependentLabel, GraphBuilderOptions options) {
		
		Edge umlCompos = buildUMLAggregationEdge(subGraph, sourceNode, dependentNode, sourceMultiplicity, sourceLabel, dependentMultiplicity, dependentLabel, options);
		
		// Set source arrow head
		umlCompos.setAttribute("arrowtail", "diamond");
		
		return umlCompos;
	}
	
	public static Edge buildUMLCompositionEdge(Subgraph subGraph, Node sourceNode, String dependentNodeName, String sourceMultiplicity, String dependentMultiplicity) {
		Node dependentNode = new Node(subGraph, dependentNodeName);
		return buildUMLCompositionEdge(subGraph, sourceNode, dependentNode, sourceMultiplicity, null, dependentMultiplicity, null, null);
	}
	
	public static Edge buildUMLImplementationEdge(Subgraph subGraph, Node interfaceNode, Node implementationNode) {
		
		Edge umlImpl = new Edge(subGraph, interfaceNode, implementationNode);
		
		// Set edge style
		umlImpl.setAttribute("style", "dashed");		
		
		// Set tail arrow head
		umlImpl.setAttribute("arrowtail", "empty");
					
		return umlImpl;
	}
	
	public static Edge buildUMLDependencyEdge(Subgraph subGraph, Node sourceNode, Node targetNode) {
		
		Edge umlDepend = new Edge(subGraph, targetNode, sourceNode);
		
		// Set edge style
		umlDepend.setAttribute("style", "dashed");		
		
		// Set tail arrow head
		umlDepend.setAttribute("arrowtail", "normal");
		
		// Set edge label
		umlDepend.setAttribute("label", "\\<\\<depends\\>\\>");
					
		return umlDepend;
	}
}

