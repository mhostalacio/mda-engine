package com.mda.engine.uml;

import java.util.List;
import org.apache.commons.lang.StringUtils;
import att.grappa.Node;
import att.grappa.Subgraph;


public class NodeFactory {
	
	private static final String NEWLINE = "\\n";
	private static final String NEWLINE_LEFTALIGN = "\\l";	
	private static final String OPEN_STEREOTYPE   = "\\<\\<";
	private static final String CLOSE_STEREOTYPE  = "\\>\\>";
	
	private static final String NEWLINE_HTML = "<BR/>";
	private static final String NEWLINE_LEFTALIGN_HTML = "<BR ALIGN='LEFT'/>";
	private static final String OPEN_STEREOTYPE_HTML   = "&lt;&lt;";
	private static final String CLOSE_STEREOTYPE_HTML  = "&gt;&gt;";
	
	public static Node buildUMLClassNode(Subgraph subGraph, String name) {
		return new Node(subGraph, name);
	}
	
	public static Node buildUMLClassNode(Subgraph subGraph, String nodeName, String className) {
		Node umlClass = new Node(subGraph, nodeName);
		umlClass.setAttribute("label", className);
		return umlClass;
	}
	
	public static Node buildUMLClassNode(Subgraph subGraph, String nodeName, String className, String umlStereotype) {
		Node umlClass = buildUMLClassNode(subGraph, nodeName, className);
		umlClass.setAttribute("label", OPEN_STEREOTYPE + umlStereotype + CLOSE_STEREOTYPE + NEWLINE + className);
		return umlClass;
	}
	
	public static Node buildUMLClassNode(Subgraph subGraph, String name, String className, List<String> classFields, List<String> classMethods, GraphBuilderOptions options) {				
		Node umlClass = buildUMLClassNode(subGraph, name);		
		return updateUMLClassNode(umlClass, className, classFields, classMethods, options);
	}

	/**
	 * @deprecated Use ClassFieldInfo instead for field labels
	 */
	private static Node updateUMLClassNode(Node umlClass, String className, List<String> classFields, List<String> classMethods, GraphBuilderOptions options) {
		StringBuffer nodeLabel = new StringBuffer();			
		
		// [DEPRECATED]
		// Display the class name and UML stereotype, if defined
		// if (umlStereotype != null)
		//	nodeLabel.append("{" + OPEN_STEREOTYPE + umlStereotype + CLOSE_STEREOTYPE + NEWLINE + className + "|");
		// else
		nodeLabel.append("{" + className + "|");
		
		// Display the class fields
		for (String classField : classFields) {
			nodeLabel.append(classField + NEWLINE_LEFTALIGN);
		}
		nodeLabel.append("|");
		
		// Display the class methods
		for (String classMethod : classMethods) {
			nodeLabel.append(classMethod + NEWLINE_LEFTALIGN);
		}
		nodeLabel.append("}");
	
		// Set the Node label
		umlClass.setAttribute("label", nodeLabel.toString());
		
		// [DEPRECATED] Set an italic font if the class is abstract 
		// if (isAbstract)
		//	umlClass.setAttribute("fontname ", options.getClassNodeFont() + " Italic");
		
		return umlClass;
	}
	
	public static Node updateUMLClassNode(Node umlClass, boolean isAbstract, String umlStereotype, String className, List<ClassFieldInfo> classFields, List<String> classMethods, GraphBuilderOptions options) {

		// According to the graph building options, the node labels can be drawn using HTML or escString format
		String _newLine_, _newLineLeftAlign_, _openStereotype_, _closeStereotype_;
		if (options.getUseHtmlLabelsForClassNodes()) {
			_newLine_ = NEWLINE_HTML;
			_newLineLeftAlign_ = NEWLINE_LEFTALIGN_HTML;
			_openStereotype_  = OPEN_STEREOTYPE_HTML;
			_closeStereotype_ = CLOSE_STEREOTYPE_HTML;
		}
		else {
			_newLine_ = NEWLINE;
			_newLineLeftAlign_ = NEWLINE_LEFTALIGN;
			_openStereotype_  = OPEN_STEREOTYPE;
			_closeStereotype_ = CLOSE_STEREOTYPE;

		}
		
		// Buffer for building the node label incrementally
		StringBuffer nodeLabel = new StringBuffer();			
		
		// Display the class name and UML stereotype, if defined
		if (umlStereotype != null)
			nodeLabel.append("{" + _openStereotype_ + umlStereotype + _closeStereotype_ + _newLine_ + className + "|");
		else
			nodeLabel.append("{" + className + "|");
		
		// Display the class fields
		for (ClassFieldInfo classField : classFields) {													
			nodeLabel.append(classField.produceLabel(options) + _newLineLeftAlign_);
		}
		nodeLabel.append("|");
		
		// Display the class methods
		for (String classMethod : classMethods) {
			nodeLabel.append(classMethod + _newLineLeftAlign_);
		}
		nodeLabel.append("}");
	
		// Set the Node label
		if (options.getUseHtmlLabelsForClassNodes()) {
			nodeLabel.insert(0, '<');
			nodeLabel.append('>');
		}
		umlClass.setAttribute("label", nodeLabel.toString());
		
		// Set an italic font if the class is abstract 
		if (isAbstract)
			umlClass.setAttribute("fontname ", options.getClassNodeFont() + " Italic");
		
		return umlClass;
	}
	
	/*
	 * [DEPRECATED]
	 * *
	private static List<String> replaceSpecialChars(List<String> classFields, GraphBuilderOptions options) {
		
		if (classFields == null)
			return null;
		
		// Replace reserved characters according to the label format used
		List<String> formattedFields = new ArrayList<String>();
		if (options.getUseHtmlLabelsForClassNodes() && !options.getDrawAssociationsAsEdges()) {
			for(String classField : classFields)
				formattedFields.add(classField.replaceAll("<", "&lt;").replaceAll(">", "&gt;"));
		}
		else {
			for(String classField : classFields)
				formattedFields.add(classField.replaceAll("<", "\\<").replaceAll(">", "\\>"));
		}
		return formattedFields;
	}
	*
	**/
	
	public static Node buildUMLPackageNode(Subgraph subGraph, String name) {
		return new Node(subGraph, name);
	}
	
	public static class ClassFieldInfo 
	{
		private String name = null;
		private String type = null;
		private boolean isPublic = true;
		private boolean isAssociation = false;
		private boolean isList = false;
		
		public ClassFieldInfo(String name, String type, boolean isAssociation, boolean isList) {
			this.name = name;
			this.type = type;
			this.isAssociation = isAssociation;
			this.isList = isList;
		}
		
		public String produceLabel(GraphBuilderOptions options) {
			
			// Determine the field visibility symbol
			String fieldLabel = StringUtils.EMPTY;
			if (options.getShowAttributeVisibility()) {
				if (isPublic)
					fieldLabel += "+ ";
				else
					fieldLabel += "- ";
			}
			
			// Add the field name and separator
			fieldLabel += name + " : ";
			
			// If the field represents a list, add the appropriate keyword (according to the label format)
			if (isList) { 
				if (options.getUseHtmlLabelsForClassNodes())
					fieldLabel += "List&lt;" + type + "&gt;";
				else
					fieldLabel += "List\\<" + type + "\\>";
			}				
			else
				fieldLabel += type;

			// Highlight the label if it represents an association
			if (isAssociation && options.getUseHtmlLabelsForClassNodes()) {
				fieldLabel = "<FONT FACE='" + options.getClassNodeFont() +  " Bold'>" + fieldLabel + "</FONT>";
			}
			
			return fieldLabel;
		}
	}
}

