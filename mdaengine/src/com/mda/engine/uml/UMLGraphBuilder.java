package com.mda.engine.uml;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.jvnet.jaxb2_commons.lang.StringUtils;

import att.grappa.Graph;
import att.grappa.Node;
import att.grappa.Subgraph;

import com.mda.engine.core.MdaLogger;
import com.mda.engine.models.businessObjectModel.*;
import com.mda.engine.models.configurationManagementModel.Application;
import com.mda.engine.uml.NodeFactory.ClassFieldInfo;
import com.mda.engine.utils.QueryHelper;



public class UMLGraphBuilder extends MdaLogger {

	private boolean isLoaded = false;
	private Graph umlClassGraph = null;
	private GraphBuilderOptions options = null;
	private ArrayList<Application> applications = null;

	public UMLGraphBuilder() {
		this.options = new GraphBuilderOptions();
	}

	public UMLGraphBuilder(GraphBuilderOptions options) {
		this.options = options;
	}

	public void setApplication(ArrayList<Application> applications) {
		this.applications = applications;
	}


	public void loadUMLClassGraph(Application app, String graphTitle) {
	
		// Begin the loading process for the package list
		this.isLoaded = false;
		this.umlClassGraph = GraphFactory.buildUMLClassGraph("uml_class_diagram", "Class Diagram for <" + graphTitle + ">", this.options);

		// Repository for package subgraphs (including the root Graph) created during the process
		Hashtable<String, Subgraph> pkgSubgraphsRep = new Hashtable<String, Subgraph>();

		// Repository for node name counters (tracks how many times a class name repeats)
		Hashtable<String, Integer> nodeNameCounters = new Hashtable<String, Integer>();
				
		
		for(ObjectType bom : app.getProduct().getApplicationBOMs(app))
		{
			if (bom.isPersistable() && !bom.getIsAutoCreated())
			{
				// Read the class description (Name, Fields and Methods)
				List<ClassFieldInfo> classFields = new ArrayList<ClassFieldInfo>();
				for (BaseAttributeType attribute : bom.getAttributes().getAttributeList()) {
					if (attribute.isPersistable()) {
						// Check if the attribute describes a class field
						if (isFieldAttribute(attribute) && !attribute.isAuditAttribute())
							classFields.add(new NodeFactory.ClassFieldInfo(attribute.getName(), mapAttributeType(bom, attribute), false, false));
	
						// Check if Associations are to be drawn as fields
						if (isAssociationRelation(attribute) && !options.getDrawAssociationsAsEdges()) {
							AssociationAttributeType association = (AssociationAttributeType) attribute;
	
							// Compute the association field type
							boolean isList = association.getMultiplicity().contains("*");
							String fieldType = association.getTargetObjectName();
							if (fieldType.contains("."))
								fieldType = fieldType.substring(fieldType.lastIndexOf('.') + 1);
	
							classFields.add(new ClassFieldInfo(attribute.getName(), fieldType, true, isList));
						}
					}
				}
				
				// 1) Create a new Class node or update the node's content for the current Type
				Node umlClassNode = createOrRetrieveClassNode(app.getUniqueName(), bom.getName(), pkgSubgraphsRep, nodeNameCounters);
				NodeFactory.updateUMLClassNode(umlClassNode, false, null, bom.getName(), classFields, new ArrayList<String>(), options);

				for (BaseAttributeType attribute : bom.getAttributes().getAttributeList()) {

					if (attribute.isPersistable()) {

						// 2.2) Association relation
						if (isAssociationRelation(attribute) && options.getDrawAssociationsAsEdges()) {
							AssociationAttributeType association = (AssociationAttributeType) attribute;
							Node targetClass = createOrRetrieveClassNode(app.getUniqueName(), association.getTargetObjectName(), pkgSubgraphsRep,nodeNameCounters);
							EdgeFactory.buildUMLAssociationEdge(umlClassGraph, umlClassNode, targetClass, association.getMultiplicity(),association.getName(), options);
						}

						// 2.3) Aggregation relation
						else if (attribute instanceof AggregationAttributeType) {

							AggregationAttributeType aggregation = (AggregationAttributeType) attribute;

							if (aggregation.getTargetObject().getApplication() == bom.getApplication()) {

								String sourceMultiplicity = null, sourceLabel = null;
								if (aggregation.getTargetAttribute() != null) {
									sourceMultiplicity = "0..1";
									sourceLabel = aggregation.getTargetAttribute().getName();
								}

								Node dependentClass = createOrRetrieveClassNode(app.getUniqueName(), aggregation.getTargetObjectName(), pkgSubgraphsRep,nodeNameCounters);
								EdgeFactory.buildUMLAggregationEdge(umlClassGraph, umlClassNode, dependentClass, sourceMultiplicity, sourceLabel,aggregation.getMultiplicity(), aggregation.getName(), options);
								
							}
						}

						// 2.4) Composition relation
						else if (attribute instanceof CompositionAttributeType) {
							
							CompositionAttributeType composition = (CompositionAttributeType) attribute;

							if (composition.getTargetObject().getApplication() == bom.getApplication()) {
								
								String sourceMultiplicity = null, sourceLabel = null;
								if (composition.getTargetAttribute() != null) {
									sourceMultiplicity = "0..1";
									sourceLabel = composition.getTargetAttribute().getName();
								}

								Node dependentClass = createOrRetrieveClassNode(app.getUniqueName(), composition.getTargetObjectName(), pkgSubgraphsRep,nodeNameCounters);
								EdgeFactory.buildUMLCompositionEdge(umlClassGraph, umlClassNode, dependentClass, sourceMultiplicity, sourceLabel,composition.getMultiplicity(), composition.getName(), options);
							}
						}
					}
				}
			}
		}

		

		// Signal that the loading process for the package list is over
		this.isLoaded = true;
	}

	// Check if a given class node exists. If not, create it.
	// If the class belongs to a different package, it must be created in avsubgraph.
	private Node createOrRetrieveClassNode(String eboPackageName, String classQualifiedName, Hashtable<String, Subgraph> pkgSubgraphsRep,
			Hashtable<String, Integer> nodeNameCounters) {

		// Obtain the class name and class package from the qualified name
		String classPackageName, className;

		int separatorPos = classQualifiedName.lastIndexOf('.');
		if (separatorPos < 0) {
			classPackageName = eboPackageName;
			className = classQualifiedName;
		} else {
			classPackageName = classQualifiedName.substring(0, separatorPos);
			className = classQualifiedName.substring(separatorPos + 1);
		}

		// Obtain the Subgraph to create/retrieve the class node, or create a new one
		Subgraph umlPackageSubgraph;

		if (!pkgSubgraphsRep.containsKey(classPackageName)) {
			umlPackageSubgraph = GraphFactory.buildUMLPackageSubgraph(this.umlClassGraph, "uml_pkg_diagram_" + pkgSubgraphsRep.size(), "\\<" + classPackageName
					+ "\\>", options);
			pkgSubgraphsRep.put(classPackageName, umlPackageSubgraph);
		} else
			umlPackageSubgraph = pkgSubgraphsRep.get(classPackageName);

		// Check if the class node already exists and return it
		// NOTE: Class node name is given by [className_counter], in order to alwaysc same name classes on the root graph
		Node umlClassNode = null;
		if (nodeNameCounters.containsKey(className)) {
			for (int i = 1; i <= nodeNameCounters.get(className) && umlClassNode == null; i++) {
				umlClassNode = umlPackageSubgraph.findNodeByName(className + "_" + i);
			}
		} else
			nodeNameCounters.put(className, 0);

		if (umlClassNode == null) {

			// Update the appropriate node name counter
			nodeNameCounters.put(className, nodeNameCounters.get(className) + 1);
			String umlClassNodeName = className + "_" + nodeNameCounters.get(className);

			// Create new class node
			umlClassNode = NodeFactory.buildUMLClassNode(umlPackageSubgraph, umlClassNodeName, className);
		}

		return umlClassNode;
	}

		
	private boolean isFieldAttribute(BaseAttributeType attribute) {
		return !(attribute instanceof AssociationAttributeType || attribute instanceof AggregationAttributeType || attribute instanceof CompositionAttributeType);
	}

	private boolean isAssociationRelation(BaseAttributeType attribute) {
		return (attribute.getClass() == AssociationAttributeType.class);
	}

	private String mapAttributeType(ObjectType bom, BaseAttributeType attribute) {
		if (attribute instanceof EnumAttributeType)
			return "INTEGER (" + attribute.getClassName() + "Enum)";
		else if (bom.getIdentity() == attribute)
			return "BIGINT (Primary Key)";
		else if (attribute.getMatchingTableColumn() != null)
		{
			String name = QueryHelper.getSQLType(attribute.getMatchingTableColumn());
			if (!StringUtils.isEmpty(attribute.getAttributeForTargetIdName()))
			{
				name += " (Foreign Key)";
			}
			return  name;
		}
		else
			return attribute.getClassName();
			
	}
	
	public void printGraph() {
		if (this.isLoaded)
			this.umlClassGraph.printGraph(System.out);
		else
			log("ERROR: There is no loaded UML graph.");
	}

	// Export the DOT language instructions to a .dot file
	public void exportGraph(String filepath) throws FileNotFoundException {
		if (this.isLoaded) {
			FileOutputStream fileStream = new FileOutputStream(filepath);
			this.umlClassGraph.printGraph(fileStream);
		} else
			log("ERROR: There is no loaded UML graph.");
	}

}
