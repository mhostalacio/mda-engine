package com.mda.engine.uml;

import att.grappa.Graph;
import att.grappa.Subgraph;

public class GraphFactory {

	public static Graph buildUMLClassGraph(String name, String title, GraphBuilderOptions options) {		
		Graph umlGraph = new Graph(name);
		
		// Set common properties for all UML class graphs
		setCommonAttributes(umlGraph, title, options);
		
		return umlGraph;
	}
	
	public static Subgraph buildUMLPackageSubgraph(Subgraph subGraph, String name, String title, GraphBuilderOptions options) {
		Subgraph umlSubgraph = new Subgraph(subGraph, "cluster_" + name);
		
		// Set common properties for all UML package subgraphs
		setCommonAttributes(umlSubgraph, title, options);
		umlSubgraph.setAttribute("labelloc", "b");
		umlSubgraph.setAttribute("fillcolor", options.getPackageClusterFillColor());
		umlSubgraph.setAttribute("style", "filled");
		umlSubgraph.setAttribute("penwidth", "0.5");
		
		return umlSubgraph;
	}
	
	public static Graph buildUMLPackageGraph(String name, String title, GraphBuilderOptions options) {		
		Graph umlGraph = new Graph(name);
		
		// Set common properties for all UML package graphs
		setCommonAttributes(umlGraph, title, options);
		
		// Override specific properties
		umlGraph.setNodeAttribute("shape", "tab");
		
		return umlGraph;
	}
	
	private static void setCommonAttributes(Subgraph subgraph, String label, GraphBuilderOptions options) {
		
		// Set common properties for the entire subgraph
		subgraph.setAttribute("fontname ", "Courier New");
		subgraph.setAttribute("fontsize", 8);		
		subgraph.setAttribute("labelloc", "t");
		subgraph.setAttribute("label", label);
		if (options.getDrawConcentratedEdges())
			subgraph.setAttribute("concentrate", "true");
		
		// Set common properties for all contained nodes
		subgraph.setNodeAttribute("fontname ", options.getClassNodeFont());
		subgraph.setNodeAttribute("fontsize", options.getClassNodeFontSize());
		subgraph.setNodeAttribute("shape", "record");
		subgraph.setNodeAttribute("style", "filled");
		subgraph.setNodeAttribute("fillcolor", options.getClassNodeFillColor());		
		
		// Set common properties for all contained edges
		subgraph.setEdgeAttribute("fontname ", "Courier New");
		subgraph.setEdgeAttribute("fontsize", 18);
		subgraph.setEdgeAttribute("dir", "none");
		subgraph.setEdgeAttribute("arrowsize", "1.5");
	}
}

