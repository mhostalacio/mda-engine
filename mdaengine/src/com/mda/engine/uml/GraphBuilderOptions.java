package com.mda.engine.uml;

import java.util.Properties;


public class GraphBuilderOptions {

	// Default values for each available option
	private boolean drawConcentratedEdges = false;
	private boolean drawAssociationsAsEdges = false;
	private boolean forceSameRankOnAssociations = false;
	private boolean drawReferencedPackagesAsSubgraphs = true;
	private boolean useHtmlLabelsForClassNodes = false;
	private boolean showAttributeVisibility = true;
	private boolean showRelationshipLabels = true;
	
	private String classNodeFillColor = "lemonchiffon";
	private String classNodeFont = "Courier New";
	private int classNodeFontSize = 18;
	private String packageClusterFillColor = "white";
		
	/**
	 * @return the drawConcentratedEdges
	 */
	public boolean getDrawConcentratedEdges() {
		return drawConcentratedEdges;
	}
	/**
	 * @param drawConcentratedEdges the drawConcentratedEdges to set
	 */
	public void setDrawConcentratedEdges(boolean drawConcentratedEdges) {
		this.drawConcentratedEdges = drawConcentratedEdges;
	}	
	/**
	 * @return the drawAssociationsAsEdges
	 */
	public boolean getDrawAssociationsAsEdges() {
		return drawAssociationsAsEdges;
	}
	/**
	 * @param drawAssociationsAsEdges the drawAssociationsAsEdges to set
	 */
	public void setDrawAssociationsAsEdges(boolean drawAssociationsAsEdges) {
		this.drawAssociationsAsEdges = drawAssociationsAsEdges;
	}
	/**
	 * @return the forceSameRankOnAssociations
	 */
	public boolean getForceSameRankOnAssociations() {
		return forceSameRankOnAssociations;
	}
	/**
	 * @param forceSameRankOnAssociations the forceSameRankOnAssociations to set
	 */
	public void setForceSameRankOnAssociations(boolean forceSameRankOnAssociations) {
		this.forceSameRankOnAssociations = forceSameRankOnAssociations;
	}
	/**
	 * @return the drawReferencedPackagesAsSubgraphs
	 */
	public boolean getDrawReferencedPackagesAsSubgraphs() {
		return drawReferencedPackagesAsSubgraphs;
	}
	/**
	 * @param drawReferencedPackagesAsSubgraphs the drawReferencedPackagesAsSubgraphs to set
	 */
	public void setDrawReferencedPackagesAsSubgraphs(
			boolean drawReferencedPackagesAsSubgraphs) {
		this.drawReferencedPackagesAsSubgraphs = drawReferencedPackagesAsSubgraphs;
	}
	/**
	 * @return the useHtmlLabelsForClassNodes
	 */
	public boolean getUseHtmlLabelsForClassNodes() {
		return useHtmlLabelsForClassNodes;
	}
	/**
	 * @param useHtmlLabelsForClassNodes the useHtmlLabelsForClassNodes to set
	 */
	public void setUseHtmlLabelsForClassNodes(boolean useHtmlLabelsForClassNodes) {
		this.useHtmlLabelsForClassNodes = useHtmlLabelsForClassNodes;
	}
	/**
	 * @return the showAttributeVisibility
	 */
	public boolean getShowAttributeVisibility() {
		return showAttributeVisibility;
	}
	/**
	 * @param showAttributeVisibility the showAttributeVisibility to set
	 */
	public void setShowAttributeVisibility(boolean showAttributeVisibility) {
		this.showAttributeVisibility = showAttributeVisibility;
	}
	/**
	 * @return the showRelationshipLabels
	 */
	public boolean getShowRelationshipLabels() {
		return showRelationshipLabels;
	}
	/**
	 * @param showRelationshipLabels the showRelationshipLabels to set
	 */
	public void setShowRelationshipLabels(boolean showRelationshipLabels) {
		this.showRelationshipLabels = showRelationshipLabels;
	}
	
	/**
	 * @return the classNodeFillColor
	 */
	public String getClassNodeFillColor() {
		return classNodeFillColor;
	}
	/**
	 * @param classNodeFillColor the classNodeFillColor to set
	 */
	public void setClassNodeFillColor(String classNodeFillColor) {
		this.classNodeFillColor = classNodeFillColor;
	}
	/**
	 * @return the classNodeFont
	 */
	public String getClassNodeFont() {
		return classNodeFont;
	}
	/**
	 * @param classNodeFont the classNodeFont to set
	 */
	public void setClassNodeFont(String classNodeFont) {
		this.classNodeFont = classNodeFont;
	}
	/**
	 * @return the classNodeFontSize
	 */
	public int getClassNodeFontSize() {
		return classNodeFontSize;
	}
	/**
	 * @param classNodeFontSize the classNodeFontSize to set
	 */
	public void setClassNodeFontSize(int classNodeFontSize) {
		this.classNodeFontSize = classNodeFontSize;
	}
	/**
	 * @return the packageClusterFillColor
	 */
	public String getPackageClusterFillColor() {
		return packageClusterFillColor;
	}
	/**
	 * @param packageClusterFillColor the packageClusterFillColor to set
	 */
	public void setPackageClusterFillColor(String packageClusterFillColor) {
		this.packageClusterFillColor = packageClusterFillColor;
	}
	
	// Factory method for initializing a new instance from a Properties object 
	public static GraphBuilderOptions loadProperties(Properties builderProps) {
		GraphBuilderOptions options = new GraphBuilderOptions();
		
		// Map each graph builder option to a given key/value pair
		if (builderProps.containsKey("DrawConcentratedEdges"))
			options.setDrawConcentratedEdges(Boolean.parseBoolean(builderProps.getProperty("DrawConcentratedEdges")));
		
		if (builderProps.containsKey("DrawAssociationsAsEdges"))
			options.setDrawAssociationsAsEdges(Boolean.parseBoolean(builderProps.getProperty("DrawAssociationsAsEdges")));
		
		if (builderProps.containsKey("ForceSameRankOnAssociations"))
			options.setForceSameRankOnAssociations(Boolean.parseBoolean(builderProps.getProperty("ForceSameRankOnAssociations")));
		
		if (builderProps.containsKey("DrawReferencedPackagesAsSubgraphs"))
			options.setDrawReferencedPackagesAsSubgraphs(Boolean.parseBoolean(builderProps.getProperty("DrawReferencedPackagesAsSubgraphs")));

		if (builderProps.containsKey("UseHtmlLabelsForClassNodes"))
			options.setUseHtmlLabelsForClassNodes(Boolean.parseBoolean(builderProps.getProperty("UseHtmlLabelsForClassNodes")));		
		
		if (builderProps.containsKey("ShowAttributeVisibility"))
			options.setShowAttributeVisibility(Boolean.parseBoolean(builderProps.getProperty("ShowAttributeVisibility")));
		
		if (builderProps.containsKey("ClassNodeFillColor"))
			options.setClassNodeFillColor(builderProps.getProperty("ClassNodeFillColor"));
		
		if (builderProps.containsKey("ClassNodeFont"))
			options.setClassNodeFont(builderProps.getProperty("ClassNodeFont"));
		
		if (builderProps.containsKey("ClassNodeFontSize"))
			options.setClassNodeFontSize(Integer.parseInt(builderProps.getProperty("ClassNodeFontSize")));
		
		if (builderProps.containsKey("PackageClusterFillColor"))
			options.setPackageClusterFillColor(builderProps.getProperty("PackageClusterFillColor"));
		
		return options; 
	}
}

