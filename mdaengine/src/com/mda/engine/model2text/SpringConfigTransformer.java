package com.mda.engine.model2text;

import com.mda.engine.core.ApplicationScope;
import com.mda.engine.core.Product;
import com.mda.engine.models.businessObjectModel.ObjectType;
import com.mda.engine.models.configurationManagementModel.Application;
import com.mda.engine.utils.Stringcode;

public class SpringConfigTransformer extends ModelToTextTransformerBase {
	
	private ApplicationScope applicationScope;
	private Product product;
	
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Override
	public String generateCodeInternal() {
		Stringcode c = new Stringcode();
		
		//Backend
		if (applicationScope  == ApplicationScope.ServicesLayer)
		{
			for(Application app : product.getApplications())
			{
				if (app.getEntitiesProjectNamespace() != null && app.getRepositoryProjectNamespace() != null)
				{
					for(ObjectType bom : product.getApplicationBOMs(app))
					{
						if (bom.isPersistable() && !bom.getIsAutoCreated())
						{
							c.line("<object name=\"{0}.{1}.I{2}Repository\" type=\"{3}.{1}.{2}Repository, {4}\" singleton=\"true\" xmlns=\"http://www.springframework.net\"></object>", app.getEntitiesProjectNamespace(), bom.getPackageName(), bom.getName(), app.getRepositoryProjectNamespace(), app.getRepositoryProject().getAssemblyName());
							c.line("<object name=\"{0}.{1}.I{2}Service\" type=\"{3}.{1}.{2}Service, {4}\" singleton=\"true\" xmlns=\"http://www.springframework.net\"></object>", app.getServiceInterfaceProjectNamespace(), bom.getPackageName(), bom.getName(), app.getServiceImplementationProjectNamespace(), app.getServiceImplementationProject().getAssemblyName());
						}
					}
				}
			}
		}
		
		//Frontend
		if (applicationScope  == ApplicationScope.MVCLayer)
		{
			for(Application app : product.getApplications())
			{
				if (app.getServiceInterfaceProjectNamespace() != null && app.getWcfImplementationProjectNamespace() != null)
				{
					for(ObjectType bom : product.getApplicationBOMs(app))
					{
						if (bom.isPersistable() && !bom.getIsAutoCreated())
							c.line("<object name=\"{0}.{1}.I{2}Service\" type=\"{3}.{1}.{2}WCFService, {4}\" singleton=\"true\" xmlns=\"http://www.springframework.net\"></object>", app.getServiceInterfaceProjectNamespace(), bom.getPackageName(), bom.getName(), app.getWcfImplementationProjectNamespace(), app.getWCFImplementationProject().getAssemblyName());
					}
				}
				else if (app.getServiceInterfaceProjectNamespace() != null && app.getServiceImplementationProjectNamespace() != null )
				{
					for(ObjectType bom : product.getApplicationBOMs(app))
					{
						if (bom.isPersistable() && !bom.getIsAutoCreated())
							c.line("<object name=\"{0}.{1}.I{2}Service\" type=\"{3}.{1}.{2}Service, {4}\" singleton=\"true\" xmlns=\"http://www.springframework.net\"></object>", app.getServiceInterfaceProjectNamespace(), bom.getPackageName(), bom.getName(), app.getServiceImplementationProjectNamespace(), app.getServiceImplementationProject().getAssemblyName());
					}
				}
				if (app.getEntitiesProjectNamespace() != null && app.getRepositoryProjectNamespace() != null)
				{
					for(ObjectType bom : product.getApplicationBOMs(app))
					{
						if (bom.isPersistable() && !bom.getIsAutoCreated())
						{
							c.line("<object name=\"{0}.{1}.I{2}Repository\" type=\"{3}.{1}.{2}Repository, {4}\" singleton=\"true\" xmlns=\"http://www.springframework.net\"></object>", app.getEntitiesProjectNamespace(), bom.getPackageName(), bom.getName(), app.getRepositoryProjectNamespace(), app.getRepositoryProject().getAssemblyName());
						}
					}
				}
			}
		}
		
		return c.toString();
	}

	public void setApplicationScope(ApplicationScope applicationScope) {
		this.applicationScope = applicationScope;
	}

	public ApplicationScope getApplicationScope() {
		return applicationScope;
	}

	
}
