package com.mda.engine.model2text;

import com.mda.engine.models.businessObjectModel.ObjectType;
import com.mda.engine.models.businessTransactionModel.BusinessTransaction;
import com.mda.engine.models.common.ModelAttribute;
import com.mda.engine.models.common.ModelAttributeObject;
import com.mda.engine.models.queryModel.Query;
import com.mda.engine.utils.StringUtils;
import com.mda.engine.utils.Stringcode;

public class WCFImplementationTransformer extends ModelToTextTransformerBase {
	private ObjectType bom;
	private String namespace;

	public void setBom(ObjectType bom) {
		this.bom = bom;
	}

	public ObjectType getBom() {
		return bom;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String getNamespace() {
		return namespace;
	}
	
	private String getNamespacePrefix()
	{
		return StringUtils.replaceLast(namespace, bom.getPackageName(), "");
	}
	
	@Override
	public String generateCodeInternal() {
		Stringcode c  = new Stringcode();
		String name = com.mda.engine.utils.StringUtils.getPascalCase(bom.getName());
		
		writeUsingNamespaces(c, getNamespacePrefix(), bom);
		
		c.line();
		c.line("//------------------------------------------------------------------------------");
		c.line("// <auto-generated>");
		c.line("//		     This code was generated by mdaengine Tool.");
		c.line("//		     Transformer class: {0} ", this.getClass().getName());
		c.line("//		     Changes to this file may cause incorrect behavior and will be lost if the code be generated again.");
		c.line("// </auto-generated>");
		c.line("//------------------------------------------------------------------------------");
		c.line("namespace {0}", namespace);
		c.line("{");
		c.tokenAdd2BeginOfLineIndent();
		c.line();
		c.line("[Serializable]");
		c.line("public partial class {0}WCFServiceGen : Library.Services.SOA.EntityService<I{0}Service>, I{0}Service", name, bom.getClassName());
		c.line("{");
		c.tokenAdd2BeginOfLineIndent();
		c.line("private static {0}WCFService _instance;", name);
		c.line();
		c.line("public static {0}WCFService Instance", name);
		c.line("{");
		c.line("    get");
		c.line("    {");
		c.line("        if (_instance == null)");
		c.line("        {");
		c.line("            lock (typeof({0}WCFService))", name);
		c.line("            {");
		c.line("                if (_instance == null)");
		c.line("                {");
		c.line("                    _instance = new {0}WCFService();", name);
		c.line("                    _instance.Initialize();");
		c.line("                }");
		c.line("            }");
		c.line("        }");
		c.line("        return _instance;");
		c.line("    }");
//		c.line("    set");
//		c.line("    {");
//		c.line("        _instance = value;");
//		c.line("    }");
		c.line("}");
		c.line();
		c.line("public override I{0}Service GetInstance()", name);
		c.line("{");
		c.line("	return Instance;");
		c.line("}");
		c.line();
		c.line("I{0}Repository Library.Entities.Persistence.IRepository<I{0}Repository>.GetInstance()", name);
		c.line("{");
		c.line("	throw new NotSupportedException();");
		c.line("}");
		c.line();
		c.line("public {0} GetById(long id, bool ignoreCaches = false)", name);
		c.line("{");
		c.tokenAdd2BeginOfLineIndent();
		c.line("{0}GetByIdRequestMessage request = new {0}GetByIdRequestMessage(BusinessTransaction.CurrentContext.SessionInfo, DateTime.UtcNow);", name);
		c.line("request.Arguments = new {0}GetByIdQueryArgumentDataContract();", name);
		c.line("request.Arguments.Id = id;");
		c.line("request.Arguments.IgnoreCaches = ignoreCaches;");
		c.line("{0}ServiceClient client = ServiceClientCacheManager<I{0}Service>.Instance.Get<{0}ServiceClient>();", bom.getPackageName());
		c.line("{0}GetByIdResponseMessage resp = client.{0}GetById(request);", name);
		c.line("return {0}Converter.ConvertToEntity(resp.Result);", name);
		c.tokenRemove2BeginOfLine();
		c.line("}");
		c.line();
		if (bom.hasMultiLanguageAttribute())
		{
			c.line("public Dictionary<CultureInfo, string> LoadResources(long entityId, string fieldName)");
			c.line("{");
			c.line("	return null;");
			c.line("}");
			c.line();
		}
		
		writeMethods(c);
		c.tokenRemove2BeginOfLine();
		c.line("}");
		c.line();
		c.line("[Serializable]");
		c.line("public partial class {0}WCFService : {0}WCFServiceGen", name);
		c.line("{");
		c.line("}");
		c.line();
		c.tokenRemove2BeginOfLine();
		c.line("}");
		return c.toString();
	}
	
	private void writeMethods(Stringcode c) {

		if (bom.getAssociatedTransactions() != null && bom.getAssociatedTransactions().size() > 0)
		{
			for (BusinessTransaction btm : bom.getAssociatedTransactions())
			{
				if (btm.getOriginatorQuery() == null)
				{
					c.line("public virtual {0}BusinessTransactionOutput {0}({0}BusinessTransactionInput args)", btm.getName());
					c.line("{");
					c.tokenAdd2BeginOfLineIndent();
					c.line("{0}RequestMessage request = new {0}RequestMessage(BusinessTransaction.CurrentContext.SessionInfo, DateTime.UtcNow);", btm.getName());
					
					if (btm.getInputs() != null && btm.getInputs().getAttributeList() != null)
					{
						for(ModelAttribute att : btm.getInputs().getAttributeList())
						{
							if (att instanceof ModelAttributeObject)
								c.line("request.{0} = {1}Converter.ConvertToDataContract(args.{0});", att.getName(), ((ModelAttributeObject)att).getBom().getName());
							else
								c.line("request.{0} = request.{0};", att.getName(), att.getClassName());
						}
					}
					c.line("{0}ServiceClient client = ServiceClientCacheManager<I{0}Service>.Instance.Get<{0}ServiceClient>();", bom.getPackageName());
					c.line("{0}ResponseMessage resp = client.{0}(request);", btm.getName());
					c.line("{0}BusinessTransactionOutput output = new {0}BusinessTransactionOutput();", btm.getName());
					
					if (btm.getOutputs() != null && btm.getOutputs().getAttributeList() != null)
					{
						for(ModelAttribute att : btm.getOutputs().getAttributeList())
						{
							if (att instanceof ModelAttributeObject)
								c.line("output.{0} = {1}Converter.ConvertToEntity(resp.{0});", att.getName(), ((ModelAttributeObject)att).getBom().getName());
							else
								c.line("output.{0} = resp.{0};", att.getName(), att.getClassName());
						}
					}

					c.line("return output;");
					c.tokenRemove2BeginOfLine();
					c.line("}");
					
					c.line("");
				}
			}
		}
		if (bom.getAssociatedQueries() != null && bom.getAssociatedQueries().size() > 0)
		{
			for (Query query : bom.getAssociatedQueries())
			{
				String argName = query.getArgumentsClassName();
				String resName = query.getResultSetClassName();
				String methodName = query.getQueryMethodName();
				String bomName = com.mda.engine.utils.StringUtils.getPascalCase(bom.getName());
				
				c.line("public virtual {0} {1}({2} args)", resName, methodName, argName);
				c.line("{");
				c.tokenAdd2BeginOfLineIndent();
				c.line("{0}{1}RequestMessage request = new {0}{1}RequestMessage(BusinessTransaction.CurrentContext.SessionInfo, DateTime.UtcNow);", bomName, methodName);
				c.line("request.Arguments = {0}Converter.ConvertToDataContract(args);", argName);
				c.line("request.Arguments.IgnoreCaches = args.IgnoreCaches;");
				c.line("{0}ServiceClient client = ServiceClientCacheManager<I{0}Service>.Instance.Get<{0}ServiceClient>();", bom.getPackageName());
				c.line("{0}{1}ResponseMessage resp = client.{0}{1}(request);", bomName, methodName);
				c.line("return {0}Converter.ConvertToEntity(resp.Result);", bomName, methodName);
				c.tokenRemove2BeginOfLine();
				c.line("}");
				c.line("");
			}
		}
	}

	public void writeUsingNamespaces(Stringcode c, String namespacePrefix, ObjectType bom){
		c.line("using System;");
		c.line("using System.Collections.Generic;");
		c.line("using System.Linq;");
		c.line("using System.Text;");
		c.line("using System.Collections.ObjectModel;");
		c.line("using System.Globalization;");
		c.line("using Library.Util.Collections;");
		c.line("using Library.Util;");
		c.line("using Library.Entities.Collections;");
		c.line("using Library.Services.SOA;");
		c.line("using Library.Util.Transactions;");
		
		
		String entitiesNamespacePrefix = getBom().getApplication().getEntitiesProjectNamespace();
		String entitiesConverterNamespacePrefix = getBom().getApplication().getEntitiesConverterProjectNamespace();
		String transactionsNamespacePrefix = getBom().getApplication().getTransactionsProjectNamespace();
		String serviceInterfNamespacePrefix = getBom().getApplication().getServiceInterfaceProjectNamespace();
		String serviceHostNamespacePrefix = getBom().getApplication().getServiceHostProjectNamespace();
		String contractsNamespacePrefix = getBom().getApplication().getServiceContractsProjectNamespace();
		
		
		if (bom.getApplication() != null && bom.getApplication().getProduct() != null && bom.getApplication().getProduct().getApplicationPackages(bom.getApplication()) != null)
		{
			for (String pack : bom.getApplication().getProduct().getApplicationPackages(bom.getApplication()))
			{
				c.line("using {0}{1};", namespacePrefix, pack);
				c.line("using {0}.{1};", entitiesNamespacePrefix, pack);
				c.line("using {0}.{1};", serviceInterfNamespacePrefix, pack);
				c.line("using {0}.{1};", transactionsNamespacePrefix, pack);
				c.line("using {0}.{1};", contractsNamespacePrefix, pack);
				c.line("using {0}.{1};", entitiesConverterNamespacePrefix, pack);
				c.line("using {0}.{1};", serviceHostNamespacePrefix, pack);
			}
		}
		c.line();
	}
}
