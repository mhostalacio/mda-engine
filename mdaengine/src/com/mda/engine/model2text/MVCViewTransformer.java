package com.mda.engine.model2text;

import com.mda.engine.models.mvc.MVCInstance;
import com.mda.engine.models.mvc.MVCViewElement;
import com.mda.engine.utils.StringUtils;
import com.mda.engine.utils.Stringcode;

public class MVCViewTransformer extends ModelToTextTransformerBase{
	private MVCInstance mvc;
	private String namespace;

	public void setMVC(MVCInstance mvc) {
		this.mvc = mvc;
	}

	public MVCInstance getVC() {
		return mvc;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String getNamespace() {
		return namespace;
	}
	
	private String getNamespacePrefix()
	{
		return StringUtils.replaceLast(namespace, mvc.getPackageName(), "");
	}
	
	@Override
	public String generateCodeInternal() throws Exception {
		Stringcode c  = new Stringcode();
		writeUsingNamespaces(c, getNamespacePrefix(), mvc);
		
		c.line();
		c.line("//------------------------------------------------------------------------------");
		c.line("// <auto-generated>");
		c.line("//		     This code was generated by mdaengine Tool.");
		c.line("//		     Transformer class: {0} ", this.getClass().getName());
		c.line("//		     Changes to this file may cause incorrect behavior and will be lost if the code be generated again.");
		c.line("// </auto-generated>");
		c.line("//------------------------------------------------------------------------------");
		c.line("namespace {0}", namespace);
		c.line("{");
		c.tokenAdd2BeginOfLineIndent();
		c.line();
		c.line("public abstract class {0}ViewGen : ViewPageBase<{0}Model>", com.mda.engine.utils.StringUtils.getPascalCase(mvc.getName()));
		c.line("{");
		c.tokenAdd2BeginOfLineIndent();
		c.line();
		c.line("#region Fields");
		c.line();
		writeFields(c);
		c.line();
		c.line("#endregion");
		c.line();
		c.line("#region Methods");
		c.line();
		writeMethods(c);
		c.line();
		c.line("#endregion");
		c.line();
		c.tokenRemove2BeginOfLine();
		c.line("}");
		c.line();
		c.line("public partial class {0}View : {0}ViewGen", com.mda.engine.utils.StringUtils.getPascalCase(mvc.getName()));
		c.line("{");
		c.line("}");
		c.line();
		
		c.tokenRemove2BeginOfLine();
		c.line("}");
		return c.toString();
	}
	
	private void writeMethods(Stringcode c) throws Exception {

		c.line("protected override void InitControls()");
        c.line("{");
		c.tokenAdd2BeginOfLineIndent();
		if (mvc.getView() != null && mvc.getView().getElements() != null && mvc.getView().getElements().getElementList() != null)
		{
			for (MVCViewElement elem : mvc.getView().getElements().getElementList())
			{
				c.line(" _{1} = ({0})ViewData[\"{1}\"];",elem.getClassName(), com.mda.engine.utils.StringUtils.getCamelCase(elem.getId()));
			}
		}
		c.tokenRemove2BeginOfLine();
		c.line("}");
	}

	private void writeFields(Stringcode c) throws Exception {
		if (mvc.getView() != null && mvc.getView().getElements() != null && mvc.getView().getElements().getElementList() != null)
		{
			for (MVCViewElement elem : mvc.getView().getElements().getElementList())
			{
	        	c.line("protected {0} _{1} = null;",elem.getClassName(), com.mda.engine.utils.StringUtils.getCamelCase(elem.getId()));
			}
		}
	}

	public static void writeUsingNamespaces(Stringcode c, String namespacePrefix, MVCInstance mvc){
		c.line("using System;");
		c.line("using System.Collections.Generic;");
		c.line("using System.Linq;");
		c.line("using System.Web;");
		c.line("using System.Web.Mvc;");
		c.line("using Library.Mvc.Views;");
		c.line("using Library.Util.Collections;");
		c.line("using Library.Util;");
		c.line("using Library.Mvc.WebControls.Controls;");
		c.line("using Library.Mvc.WebControls.Widgets;");
		c.line("using Library.Mvc.WebControls.Base;");
		c.line("using {0};", mvc.getModelNamespace());
		c.line("using Library.Entities.Collections;");

		if (mvc.getApplication() != null && mvc.getApplication().getProduct() != null && mvc.getApplication().getProduct().getApplicationPackages(mvc.getApplication()) != null)
		{
			for (String pack : mvc.getApplication().getProduct().getApplicationPackages(mvc.getApplication()))
			{
				c.line("using {0}.{1};", mvc.getApplication().getEntitiesProjectNamespace(), pack);
			}
		}
		c.line();
	}
}
