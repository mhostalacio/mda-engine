package com.mda.engine.model2text;

import com.mda.engine.core.ModelNodeBase;
import com.mda.engine.models.dataModel.DataBaseItem;
import com.mda.engine.models.expressionsModel.Expression;
import com.mda.engine.models.queryModel.BaseJoinClause;
import com.mda.engine.models.queryModel.InnerJoinClause;
import com.mda.engine.models.queryModel.LeftJoinClause;
import com.mda.engine.models.queryModel.Query;
import com.mda.engine.models.queryModel.QueryDefinitionCollection;
import com.mda.engine.models.queryModel.QueryDefinitionCollectionList;
import com.mda.engine.models.queryModel.QueryDefinitionSelect;
import com.mda.engine.models.queryModel.RightJoinClause;
import com.mda.engine.models.queryModel.SelectAttributesBase;
import com.mda.engine.models.queryModel.SelectColumnOrderByReference;
import com.mda.engine.models.queryModel.SelectColumnReference;
import com.mda.engine.utils.QueryHelper;
import com.mda.engine.utils.Stringcode;

public class QueryTransformer extends ModelToTextTransformerBase{
	
	private Query query;
	
	public void setQuery(Query query) {
		this.query = query;
	}

	public Query getQuery() {
		return query;
	}
	
	@Override
	public String generateCodeInternal() {
		Stringcode c  = new Stringcode();
		
		c.line();
		c.line("-- ************** IMPORTANT NOTICE *****************");
		c.line("-- This script was generated by mdaengine Tool");
		c.line("-- Runtime Version: 1.0.0.0");
		c.line("-- *************************************************");
		c.line("-- =================================================");
		c.line("-- Query: '{0}'", query.getExtendedName());
		c.line("-- =================================================");
		c.line("IF EXISTS (SELECT object_id FROM sys.objects WHERE object_id = OBJECT_ID(N'{0}') AND type in (N'P', N'PC'))", query.getExtendedName());
		c.line("BEGIN");
		c.line("    DROP PROCEDURE [dbo].[{0}] ", query.getExtendedName());
		c.line("END");
		c.line("GO");
		c.line("CREATE PROCEDURE [dbo].[{0}]", query.getExtendedName());
		
		if (query.getParameters().getDefinition() != null && query.getParameters().getDefinition().getColumnList()!= null && query.getParameters().getDefinition().getColumnList().size() > 0)
		{
			c.line("(");
			for (DataBaseItem prm : query.getParameters().getDefinition().getColumnList()) {
				c.line("	@{0} {1}{2},", prm.getName(), QueryHelper.getSQLType(prm),(prm.isNullable()) ? " = NULL" : "");
			}
			
			if(query.isUsePagination()){
				c.line("	@StartIndex BIGINT,");
				c.line("	@EndIndex BIGINT,");
			}
			
			c.removeLastChar();
			c.line(")");
			
		} else if(query.isUsePagination()){
			c.line("(");
			c.line("	@StartIndex BIGINT,");
			c.line("	@EndIndex BIGINT,");
			c.removeLastChar();
			c.line(")");			
		}
		
		c.line("AS");
		c.line();
		c.line("BEGIN");
		c.tokenAdd2BeginOfLineIndent();
		c.line("SET NOCOUNT ON;");
		
		writeQueryDefinitionCollection(c, query.getDefinition());
		
		c.line("SET NOCOUNT OFF;");
		c.tokenRemove2BeginOfLine();
		c.line("END");
		c.line("GO");
		return c.toString();
	}

	private void writeQueryDefinitionCollection(Stringcode c, QueryDefinitionCollection collection) {
		if (query.getDefinition() != null) {
						
			c.tokenAdd2BeginOfLineIndent();
			
			if (collection.getSelect() != null){
				
				writeSelectInternal(c, collection.getSelect());
			}
			else if (collection.getUnion() != null && collection.getUnion().getSelectOrUnion() != null)
			{
				writeUnionRecursive(c, collection.getUnion());
			}
			
			c.tokenRemove2BeginOfLine();
		}
	}

	private void writeUnionRecursive(Stringcode c, QueryDefinitionCollectionList list) {
		int count = 0;
		for(ModelNodeBase item : list.getSelectOrUnion())
		{
			if (item instanceof QueryDefinitionSelect)
			{
				if (count > 0)
					c.line("UNION");
				writeSelectInternal(c, (QueryDefinitionSelect)item);
				
			}
			else if (item instanceof QueryDefinitionCollectionList)
			{
				writeUnionRecursive(c, (QueryDefinitionCollectionList)item);
			}
			count++;
		}
		
	}

	private void writeSelectInternal(Stringcode c, QueryDefinitionSelect select) {
		if(!query.isUsePagination()){
			this.writeSelectClause(c, select);
			this.writeFromClause(c, select);					
			this.writeWhereClause(c, select);
			this.writeGroupByClause(c, select);
			this.writeOrderByClause(c, select);
		}else {
			this.writePaginatedQuery(c, select);
		}
	}

	private void writeSelectClause(Stringcode c, QueryDefinitionSelect select) {
		//SELECT CLAUSE
		c.line("SELECT ");
		if (select.isDistinct()) {
			c.line("DISTINCT ");
		}
		
		
		if (select.getTop() != null){
			c.line("TOP {0} ", select.getTop());
		}
		
		if (select.getColumns() != null && select.getColumns().getColumns() != null) {					
			
			for (SelectAttributesBase colSelection : select.getColumns().getColumns()) {
				colSelection.writeSelection(c);
			}
			
			c.removeLastChar();
		}
		else {
			c.line(" * ");
		}
	}

	private void writeGroupByClause(Stringcode c, QueryDefinitionSelect select) {
		//GROUP BY CLAUSE
		if (select.getGroupBy() != null && select.getGroupBy().getColumns() != null && select.getGroupBy().getColumns().size() > 0)
		{
			c.line("GROUP BY ");
			for (SelectColumnReference col : select.getGroupBy().getColumns()) {
				c.line("{0}{1}{3}{2},", (col.getTableAlias() != null ? (col.getTableAlias() + ".[") : ""), col.getColumnName(), 
						col.getResultAlias() != null ? (" AS " + col.getResultAlias()) : "",
						(col.getTableAlias() != null ? ("]") : ""));
			}
			c.removeLastChar();
		}
	}
	
	private void writeOrderByClause(Stringcode c, QueryDefinitionSelect select) {
		//ORDER BY CLAUSE
		if (select.getOrderBy() != null && select.getOrderBy().getColumns() != null && select.getOrderBy().getColumns().size() > 0)
		{
			c.line("ORDER BY ");
			for (SelectColumnOrderByReference col : select.getOrderBy().getColumns()) {
				c.line("{0}{1}{3}{2} {4},", (col.getTableAlias() != null ? (col.getTableAlias() + ".[") : ""), col.getColumnName(), 
						col.getResultAlias() != null ? (" AS " + col.getResultAlias()) : "",
						(col.getTableAlias() != null ? ("]") : ""), col.getDirection());
			}
			c.removeLastChar();
		}
	}

	private void writeWhereClause(Stringcode c, QueryDefinitionSelect select) {
		//WHERE CLAUSE
		if (select.getWhere() != null && select.getWhere().getExpressions() != null)
		{
			if (select.getWhere().getExpressions().size() > 0)
			{
				c.line("WHERE");
				c.line();
				for (Expression exp : select.getWhere().getExpressions()) {
					exp.writeSQLExpression(c, this.query.getOwner().getApplication().getProduct());
				}
			}
		}
	}
	
	private void writePaginatedQuery(Stringcode c, QueryDefinitionSelect select){
		
		c.line("SELECT * ");
		c.line("FROM");
		c.line("(");
		c.tokenAdd2BeginOfLineIndent();
		
		this.writePaginatedSelectClause(c, select);
		this.writeFromClause(c, select);
		this.writeWhereClause(c, select);
		this.writeGroupByClause(c, select);
		c.tokenRemove2BeginOfLine();
		c.line(") as PageRecords");
		c.line("WHERE PageRecords.RowNum >= @StartIndex AND PageRecords.RowNum < @EndIndex");		
	}
	
	/**
	 * 
	 * @param c
	 */
	private void writePaginatedSelectClause(Stringcode c, QueryDefinitionSelect select){

		c.line("SELECT ");
		
		if (select.isDistinct()) {
			c.line("DISTINCT ");
		}
		
		c.line("ROW_NUMBER() OVER");
		c.line("(");
		c.tokenAdd2BeginOfLineIndent();
		
		if (select.getOrderBy() != null && select.getOrderBy().getColumns() != null && select.getOrderBy().getColumns().size() > 0){
			c.line("ORDER BY ");
			for (SelectColumnOrderByReference col : query.getDefinition().getSelect().getOrderBy().getColumns()) {
				c.line("{0}{1}{3}{2} {4},", (col.getTableAlias() != null ? (col.getTableAlias() + ".[") : ""), col.getColumnName(), 
						col.getResultAlias() != null ? (" AS " + col.getResultAlias()) : "",
						(col.getTableAlias() != null ? ("]") : ""), col.getDirection());
			}
			c.removeLastChar();
		}
		
		c.tokenRemove2BeginOfLine();
		c.line(") AS RowNum,");
		
		if (query.getDefinition().getSelect().getColumns() != null && query.getDefinition().getSelect().getColumns().getColumns() != null) {					
			
			for (SelectAttributesBase colSelection : query.getDefinition().getSelect().getColumns().getColumns()) {
				colSelection.writeSelection(c);
			}
			
			c.removeLastChar();
		}
		else {
			c.line(" * ");
		}
	}
	
	/**
	 * 
	 * @param c
	 */
	private void writeFromClause(Stringcode c, QueryDefinitionSelect select) {
		//FROM CLAUSE
		if (select.getFrom() != null)
		{
			c.line("FROM");
			if (select.getFrom().getTable() != null)
			{
				c.line("[{0}] {1}", select.getFrom().getTable().getName(), select.getFrom().getAlias());
			}
			else if (select.getFrom().getQuery() != null)
			{
				c.line("(");
				writeQueryDefinitionCollection(c, select.getFrom().getQuery());
				c.line(")");
				if (select.getFrom().getQuery().getAlias() != null)
				{
					c.line("AS {0}", select.getFrom().getQuery().getAlias());
				}
			}
			if (query.isUseNolockHint() && select.getFrom().getQuery() == null)
			{
				c.line("WITH(NOLOCK)");
			}
			//write joins
			if (select.getFrom().getClauseList() != null)
			{
				for (BaseJoinClause clause : select.getFrom().getClauseList()) {
					
					if (clause instanceof InnerJoinClause)
					{
						if (clause.getWith().getTable() != null)
						{
							c.line("INNER JOIN [dbo].[{0}] AS {1} ON ", clause.getWith().getTable().getName(), clause.getWith().getAlias());
						}
					}
					else if (clause instanceof LeftJoinClause)
					{
						if (clause.getWith().getTable() != null)
						{
							c.line("LEFT JOIN [dbo].[{0}] AS {1} ON ", clause.getWith().getTable().getName(), clause.getWith().getAlias());
						}
					}
					else if (clause instanceof RightJoinClause)
					{
						if (clause.getWith().getTable() != null)
						{
							c.line("RIGHT JOIN [dbo].[{0}] AS {1} ON ", clause.getWith().getTable().getName(), clause.getWith().getAlias());
						}
					}
					
					if (clause.getWith().getOn().getExpressions() != null)
					{
						for (Expression exp : clause.getWith().getOn().getExpressions()) {
							exp.writeSQLExpression(c, this.query.getOwner().getApplication().getProduct());
						}
					}
				}
			}
		}
	}
}
