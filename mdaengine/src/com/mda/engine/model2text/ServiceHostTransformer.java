package com.mda.engine.model2text;

import com.mda.engine.core.Product;
import com.mda.engine.models.businessObjectModel.ObjectType;
import com.mda.engine.models.businessTransactionModel.BusinessTransaction;
import com.mda.engine.models.configurationManagementModel.Application;
import com.mda.engine.models.queryModel.Query;
import com.mda.engine.utils.StringUtils;
import com.mda.engine.utils.Stringcode;

public class ServiceHostTransformer extends ModelToTextTransformerBase {

	private String namespace;
	private String packageName;
	private Application application;
	private Product product;
	
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
	public String getNamespace() {
		return namespace;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setApplication(Application application) {
		this.application = application;
	}
	public Application getApplication() {
		return application;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public Product getProduct() {
		return product;
	}
	
	@Override
	public String generateCodeInternal() {
		Stringcode c  = new Stringcode();

		c.line();
		c.line("<!--------------------------------------------------------------------------------");
		c.line("// <auto-generated>");
		c.line("//		     This code was generated by mdaengine Tool.");
		c.line("//		     Transformer class: {0} ", this.getClass().getName());
		c.line("//		     Changes to this file may cause incorrect behavior and will be lost if the code be generated again.");
		c.line("// </auto-generated>");
		c.line("//------------------------------------------------------------------------------>");
		c.line("<%@ ServiceHost Language=\"C#\" Debug=\"false\" Service=\"{0}.{1}.{1}ServiceImpl\"  %>", application.getServiceHostProject().getNamespace(), packageName);
		
		
		return c.toString();
	}
	

}