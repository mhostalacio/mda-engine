package com.mda.engine.model2text;

import com.mda.engine.models.configurationManagementModel.Application;
import com.mda.engine.models.dataModel.ColumnReference;
import com.mda.engine.models.dataModel.DataTable;
import com.mda.engine.models.dataModel.ForeignKey;
import com.mda.engine.models.dataModel.TableColumn;
import com.mda.engine.utils.QueryHelper;
import com.mda.engine.utils.Stringcode;

public class DataModuleTransformer  extends ModelToTextTransformerBase{

	private Application application;
	
	@Override
	public String generateCodeInternal() {
		Stringcode c  = new Stringcode();
		c.line("<datamodules>");
		c.tokenAdd2BeginOfLineIndent();
		for(DataTable tb : application.getProduct().getApplicationTables(application))
		{
			c.line("<datamodule>");
			c.tokenAdd2BeginOfLineIndent();
			
			//generate table
			c.line("<table name=\"{0}\">", tb.getName());
			c.tokenAdd2BeginOfLineIndent();
			c.line("<columns>");
			c.tokenAdd2BeginOfLineIndent();
			for(TableColumn col : tb.getColumns().getColumnList())
			{
				String sqlType = QueryHelper.getSQLType(col);
				String isNull = col.isNullable() ? "true" : "false";
				String isPrimaryKey = col.getName().equals("Id") ? "true" : "false";
				c.line("<column name=\"{0}\" datatype=\"{1}\" nullable=\"{2}\" isprimarykey=\"{3}\"/>", col.getName(), sqlType, isNull, isPrimaryKey);
			}
			c.tokenRemove2BeginOfLine();
			c.line("</columns>");
			c.line("<constraints>");
			c.tokenAdd2BeginOfLineIndent();
			if (tb.getPrimaryKey() != null)
			{
				c.line("<constraint name=\"PK_{0}\" type=\"PRIMARY KEY CLUSTERED\" allowPageLocks=\"{1}\" allowRowLocks=\"{2}\">", tb.getName(), tb.getPrimaryKey().isAllowPageLocks(), tb.getPrimaryKey().isAllowRowLocks());
				c.tokenAdd2BeginOfLineIndent();
				c.line("<columns>");
				c.tokenAdd2BeginOfLineIndent();
				for(ColumnReference columnName : tb.getPrimaryKey().getColumns())
				{
					c.line("<column name=\"{0}\" direction=\"{1}\"/>",  columnName.getName() ,columnName.getDirection().toString());
				}
				c.tokenRemove2BeginOfLine();
				c.line("</columns>");
				c.tokenRemove2BeginOfLine();
				c.line("</constraint>");
			}
			if (tb.getForeignKeys() != null && tb.getForeignKeys().getForeignKeyList() != null && tb.getForeignKeys().getForeignKeyList().size() > 0)
			{
				for(ForeignKey fk : tb.getForeignKeys().getForeignKeyList())
				{
					c.line("<constraint name=\"{0}\" type=\"FOREIGN KEY\" >", fk.getName());
					c.tokenAdd2BeginOfLineIndent();
					c.line("<origin tablename=\"{0}\">", fk.getOrigin().getTableName());
					c.tokenAdd2BeginOfLineIndent();
					for(TableColumn columnName : fk.getOrigin().getColumns().getColumnList())
					{
						c.line("<column name=\"{0}\"/>",  columnName.getName());
					}
					c.tokenRemove2BeginOfLine();
					c.line("</origin>");
					c.line("<target tablename=\"{0}\">", fk.getTarget().getTableName());
					c.tokenAdd2BeginOfLineIndent();
					for(TableColumn columnName : fk.getTarget().getColumns().getColumnList())
					{
						c.line("<column name=\"{0}\"/>",  columnName.getName());
					}
					c.tokenRemove2BeginOfLine();
					c.line("</target>");
					c.tokenRemove2BeginOfLine();
					c.line("</constraint>");
				}
				
			}
			c.tokenRemove2BeginOfLine();
			c.line("</constraints>");
			c.tokenRemove2BeginOfLine();
			c.line("</table>");
			
			//generate table types
			c.line("<tabletype name=\"{0}_InsertTable\">", tb.getName());
			c.tokenAdd2BeginOfLineIndent();
			c.line("<columns>");
			c.tokenAdd2BeginOfLineIndent();
			for(TableColumn col : tb.getColumns().getColumnList())
			{
				if (QueryHelper.isParameter4InsertTableType(col))
				{
					String sqlType = QueryHelper.getSQLType(col);
					String isNull = col.isNullable() ? "true" : "false";
					String isPrimaryKey = col.getName().equals("Id") ? "true" : "false";
					c.line("<column name=\"{0}\" datatype=\"{1}\" nullable=\"{2}\" isprimarykey=\"{3}\"/>", col.getName(), sqlType, isNull, isPrimaryKey);
				}
			}
			c.tokenRemove2BeginOfLine();
			c.line("</columns>");
			c.tokenRemove2BeginOfLine();
			c.line("</tabletype>");
			
			c.line("<tabletype name=\"{0}_UpdateTable\">", tb.getName());
			c.tokenAdd2BeginOfLineIndent();
			c.line("<columns>");
			c.tokenAdd2BeginOfLineIndent();
			for(TableColumn col : tb.getColumns().getColumnList())
			{
				if (QueryHelper.isParameter4UpdateTableType(col))
				{
					String sqlType = QueryHelper.getSQLType(col);
					String isNull = col.isNullable() ? "true" : "false";
					String isPrimaryKey = col.getName().equals("Id") ? "true" : "false";
					c.line("<column name=\"{0}\" datatype=\"{1}\" nullable=\"{2}\" isprimarykey=\"{3}\"/>", col.getName(), sqlType, isNull, isPrimaryKey);
				}
			}
			c.line("<column name=\"RowIdx\" datatype=\"INT\" nullable=\"true\" isprimarykey=\"false\"/>");
			c.tokenRemove2BeginOfLine();
			c.line("</columns>");
			c.tokenRemove2BeginOfLine();
			c.line("</tabletype>");
			
			c.line("<tabletype name=\"{0}_DeleteTable\">", tb.getName());
			c.tokenAdd2BeginOfLineIndent();
			c.line("<columns>");
			c.tokenAdd2BeginOfLineIndent();
			if (tb.getPrimaryKey() != null)
			{
				for (ColumnReference column : tb.getPrimaryKey().getColumns()) {
					TableColumn col = tb.getColumnByName(column.getName());
					String sqlType = QueryHelper.getSQLType(col);
					String isNull = col.isNullable() ? "true" : "false";
					String isPrimaryKey = col.getName().equals("Id") ? "true" : "false";
					c.line("<column name=\"{0}\" datatype=\"{1}\" nullable=\"{2}\" isprimarykey=\"{3}\"/>", col.getName(), sqlType, isNull, isPrimaryKey);
				}
			}
			else
			{
				for (TableColumn col : tb.getColumns().getOrderedColumnList()) {
					if (!col.getAuditColumn() || col.getName().equals(QueryHelper.AUDIT_FIELD_DEFAULT_LANGUAGE))
					{
						String sqlType = QueryHelper.getSQLType(col);
						String isNull = col.isNullable() ? "true" : "false";
						String isPrimaryKey = col.getName().equals("Id") ? "true" : "false";
						c.line("<column name=\"{0}\" datatype=\"{1}\" nullable=\"{2}\" isprimarykey=\"{3}\"/>", col.getName(), sqlType, isNull, isPrimaryKey);
					}
				}
			}
			c.tokenRemove2BeginOfLine();
			c.line("</columns>");
			c.tokenRemove2BeginOfLine();
			c.line("</tabletype>");
			
			c.tokenRemove2BeginOfLine();
			c.line("</datamodule>");
		}
		c.tokenRemove2BeginOfLine();
		c.line("</datamodules>");
		return c.toString();
	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}
}
