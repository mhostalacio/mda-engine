package com.mda.engine.model2text;

import com.mda.engine.models.configurationManagementModel.ClassLibraryProject;
import com.mda.engine.models.configurationManagementModel.ConsoleApplicationProject;
import com.mda.engine.models.configurationManagementModel.Project;
import com.mda.engine.models.configurationManagementModel.Reference;
import com.mda.engine.utils.Stringcode;

public class ProjectFileTransformer extends ModelToTextTransformerBase{
	
	private Project project;
	
	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	@Override
	public String generateCodeInternal() {

		Stringcode c = new Stringcode();
		c.setIndentValue("  ");

		c.line("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
		c.line("<Project ToolsVersion=\"12.0\" DefaultTargets=\"Build\" xmlns=\"http://schemas.microsoft.com/developer/msbuild/2003\">");
		c.tokenAdd2BeginOfLineIndent();

//		if (project instanceof WebServiceHostProject)
//			c.line("<UsingTask AssemblyFile=\"{0}\" TaskName=\"Vortal.Practices.MSBuild.CreateSymbolicLinkTask\"/>",
					//IOHelper.combine(product.getGenConfig().getToolsFolder(), "MSBuild", "Vortal.Practices.MSBuild.dll"));

		c.line("<PropertyGroup>");
		c.tokenAdd2BeginOfLineIndent();
		c.line("<ProjectFullName>{0}</ProjectFullName>", project.getFullName());
		c.line("<Configuration Condition=\"'$(Configuration)' == '' \">Debug</Configuration>");
		c.line("<Platform Condition=\"'$(Platform)' == '' \">AnyCPU</Platform>");
		c.line("<ProductVersion>8.0.30703</ProductVersion>");
		c.line("<SchemaVersion>2.0</SchemaVersion>");
		c.line("<ProjectGuid>{0}</ProjectGuid>", String.format("{%s}", project.getGuid()));

		String outputType = null;

		if (project instanceof ClassLibraryProject)
		{
			outputType = "Library";
		}
		else if (project instanceof ConsoleApplicationProject)
		{
			outputType = "Exe";
		}

		c.line("<OutputType>{0}</OutputType>", outputType);
		c.line("<AppDesignerFolder>Properties</AppDesignerFolder>");
		c.line("<RootNamespace>{0}</RootNamespace>", project.getNamespace());
		c.line("<AssemblyName>{0}</AssemblyName>", project.getAssemblyName());
		c.line("<TargetFrameworkVersion>v4.5.1</TargetFrameworkVersion>");
		c.line("<FileAlignment>512</FileAlignment>");

//		if (project.getAdditionalProjectTypeGUIDS() != null) {
//
//			c.line("<ProjectTypeGuids>");
//
//			int guidsCount = project.getAdditionalProjectTypeGUIDS().size();
//
//			for (int i = 0; i < guidsCount; i++) {
//
//				String guid = project.getAdditionalProjectTypeGUIDS().get(i);
//
//				c.write("{{0}}", guid);
//
//				if (i < guidsCount - 1)
//					c.write(";");
//			}
//
//			c.write("</ProjectTypeGuids>");
//		}
//
//		if (project.isAllowUnsafeCode()) {
//
//			c.line("<AllowUnsafeBlocks>true</AllowUnsafeBlocks>");
//		}

		c.tokenRemove2BeginOfLine();
		c.line("</PropertyGroup>");
		c.line("<PropertyGroup Condition=\" '$(Configuration)|$(Platform)' == 'Debug|AnyCPU' \">");
		c.tokenAdd2BeginOfLineIndent();
		c.line("<DebugSymbols>true</DebugSymbols>");
		c.line("<DebugType>full</DebugType>");
		c.line("<Optimize>false</Optimize>");
		c.line("<OutputPath>bin\\</OutputPath>");
		c.line("<DefineConstants>DEBUG;TRACE</DefineConstants>");
		c.line("<ErrorReport>prompt</ErrorReport>");
		c.line("<WarningLevel>4</WarningLevel>");
		c.line("<UseVSHostingProcess>true</UseVSHostingProcess>");
		c.tokenRemove2BeginOfLine();
		c.line("</PropertyGroup>");
		c.line("<PropertyGroup Condition=\" '$(Configuration)|$(Platform)' == 'Release|AnyCPU' \">");
		c.tokenAdd2BeginOfLineIndent();
		c.line("<DebugType>pdbonly</DebugType>");
		c.line("<Optimize>true</Optimize>");
		c.line("<OutputPath>bin\\</OutputPath>");
		c.line("<DefineConstants>TRACE</DefineConstants>");
		c.line("<ErrorReport>prompt</ErrorReport>");
		c.line("<WarningLevel>4</WarningLevel>");
		c.tokenRemove2BeginOfLine();
		c.line("</PropertyGroup>");

		if (project.getDependencies() != null) {

			c.line("<ItemGroup>");
			c.tokenAdd2BeginOfLineIndent();

			for (Reference reference : project.getDependencies().getReference()) {

				//reference.addCSharpProjectInformation(c, project);
			}

			c.tokenRemove2BeginOfLine();
			c.line("</ItemGroup>");
		}

//		if (project.getFiles() != null) {
//
//			c.line("<ItemGroup>");
//			c.tokenAdd2BeginOfLineIndent();
//
//			for (CSharpProjectFileInclude fileRef : project.getFiles().getFile()) {
//
//				String tagName = null;
//
//				switch (fileRef.getType()) {
//
//				case CONTENT:
//					tagName = "Content";
//					break;
//
//				case COMPILE:
//					tagName = "Compile";
//					break;
//
//				case NONE:
//					tagName = "None";
//					break;
//
//				case EMBEDDED_RESOURCE:
//					tagName = "EmbeddedResource";
//					break;
//
//				case WCF_METADATA:
//					tagName = "WCFMetadata";
//					break;
//
//				case WCF_METADATA_STORAGE:
//					tagName = "WCFMetadataStorage";
//					break;
//				}
//
//				c.line("<{0} Include=\"{1}\" Label=\"{2}\">", tagName, fileRef.getPath(), fileRef.getSource());
//
//				switch (fileRef.getCopyToOutput()) {
//				case COPY_ALWAYS:
//					c.line("  <CopyToOutputDirectory>Always</CopyToOutputDirectory>");
//					break;
//
//				case COPY_IF_NEWER:
//					c.line("  <CopyToOutputDirectory>PreserveNewest</CopyToOutputDirectory>");
//					break;
//				}
//
//				if (!StringUtils.isEmpty(fileRef.getSubType())) {
//
//					c.line("  <SubType>{0}</SubType>", fileRef.getSubType());
//				}
//
//				c.line("</{0}>", tagName);
//			}
//
//			c.tokenRemove2BeginOfLine();
//			c.line("</ItemGroup>");
//		}

		c.line("<Import Project=\"$(MSBuildToolsPath)\\Microsoft.CSharp.targets\" />");

		//project.addMSBuildDeclarations(c);

		

//		if (project.isHostProject()) {
//
//			c.line("<Target Name=\"AfterBuild\">");
//
//			c.tokenAdd2BeginOfLineIndent();
//
//			// TODO Remove and just use the EnvRideFiles collection
//			if (project.shouldProcessEnvRide()) {
//
//				addEnvRideCall(c);
//			}			
//			
//			HashSet<String> processedDlls = new HashSet<String>();
//
//			addCreateSymbolicLinksDeclarations(c, project, project, processedDlls, BuildLanguage.MSBUILD);
//
//			c.tokenRemove2BeginOfLine();
//
//			c.line("</Target>");
//			
//		} else {
//			
//			// TODO Remove and just use the EnvRideFiles collection
//			if (project.shouldProcessEnvRide()) {
//
//				c.line("<Target Name=\"AfterBuild\">");
//				c.tokenAdd2BeginOfLineIndent();
//				addEnvRideCall(c);
//				c.tokenRemove2BeginOfLine();
//				c.line("</Target>");
//			}			
//		}

		c.tokenRemove2BeginOfLine();
		c.line("</Project>");

		return c.toString();
	}
}
