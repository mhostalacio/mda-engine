package com.mda.engine.model2text;

import java.io.File;
import java.util.List;

import com.mda.engine.core.ICodeGenerator;
import com.mda.engine.utils.IOHelper;

public abstract class ModelToTextTransformerBase implements ICodeGenerator {
	
	private List<String> errors;
	/* (non-Javadoc)
	 * @see com.mda.engine.core.ICodeGenerator#getErrors()
	 */
	public List<String> getErrors() {
		return errors;
	}
	
	/* (non-Javadoc)
	 * @see com.mda.engine.core.ICodeGenerator#hasErrors()
	 */
	public boolean hasErrors() {
		return errors != null && errors.size() >0;
	}
	
	@Override
	public void generateCode(File fileToWriteTo) throws Exception {
		String genCode = generateCodeInternal();

		IOHelper.writeToFile(genCode, fileToWriteTo);

	}

	public String generateCodeInternal() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
}
