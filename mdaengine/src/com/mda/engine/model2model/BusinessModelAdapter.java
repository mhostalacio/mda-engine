package com.mda.engine.model2model;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import com.mda.engine.core.MdaLogger;
import com.mda.engine.core.Product;
import com.mda.engine.models.businessObjectModel.AggregationAttributeType;
import com.mda.engine.models.businessObjectModel.AttributeCollectionType;
import com.mda.engine.models.businessObjectModel.AttributeNumberDataType;
import com.mda.engine.models.businessObjectModel.BaseAttributeType;
import com.mda.engine.models.businessObjectModel.BaseAttributeWithTargetType;
import com.mda.engine.models.businessObjectModel.CompositionAttributeType;
import com.mda.engine.models.businessObjectModel.DatetimeAttributeType;
import com.mda.engine.models.businessObjectModel.GenerateQueryType;
import com.mda.engine.models.businessObjectModel.HeaderType;
import com.mda.engine.models.businessObjectModel.IndentityAttributeType;
import com.mda.engine.models.businessObjectModel.Index;
import com.mda.engine.models.businessObjectModel.IndexCollection;
import com.mda.engine.models.businessObjectModel.InterfaceType;
import com.mda.engine.models.businessObjectModel.NumberAttributeType;
import com.mda.engine.models.businessObjectModel.ObjectType;
import com.mda.engine.models.businessObjectModel.TextAttributeType;
import com.mda.engine.models.common.BOMReferenceType;
import com.mda.engine.models.configurationManagementModel.Application;
import com.mda.engine.models.expressionsModel.ExpressionChoice;
import com.mda.engine.models.expressionsModel.QueryParameterExpression;
import com.mda.engine.models.expressionsModel.TableColumnExpression;
import com.mda.engine.models.queryModel.FromClauseChoice;
import com.mda.engine.models.queryModel.InnerJoinClause;
import com.mda.engine.models.queryModel.OnClause;
import com.mda.engine.models.queryModel.ParameterCollection;
import com.mda.engine.models.queryModel.Query;
import com.mda.engine.models.queryModel.QueryDefinitionCollection;
import com.mda.engine.models.queryModel.QueryDefinitionSelect;
import com.mda.engine.models.queryModel.QueryParameters;
import com.mda.engine.models.queryModel.QueryResultSet;
import com.mda.engine.models.queryModel.WhereClause;
import com.mda.engine.models.queryModel.WithClause;
import com.mda.engine.utils.QueryHelper;

public class BusinessModelAdapter extends MdaLogger {

	private Product product;
	
	public BusinessModelAdapter(Product product)
	{
		this.product = product;
	}
	
	public void adaptBusinessModel()
	{	
		for(Application app : product.getApplications())
		{
			//add needed BOM's to query types
			addDefaultBOMs2FirstApplication(app);
			
			for(InterfaceType interf : product.getApplicationInterfaces(app))
			{
				adaptInterface(app, interf);
			}
			
			//list will change in between
			ArrayList<ObjectType> bomList = new ArrayList<ObjectType>();
			bomList.addAll(product.getApplicationBOMs(app));

			//First iteration - add identity and default queries
			for(ObjectType bom : bomList)
			{
				if (bom.getIdentity() == null)
				{
					IndentityAttributeType id = new IndentityAttributeType();
					id.setName("Id");
					id.setIsNullable(false);
					id.setDescription("Id");
					bom.getAttributes().getAttributeList().add(id);
					bom.setIdentity(id);
					
					if (bom.getPrimaryKey() != null)
					{
						id.setMode("Manual");
					}
					else
					{
						id.setMode("Automatic");
					}
				}
				if (bom.isPersistable() && bom.getQueries() != null)
				{
					if (bom.getQueries().getGetById() != null)
					{
						Query getById = QueryHelper.createGetByIdQuery(app, bom, bom.getQueries().getGetById());
						product.addQuery(app, getById);
					}
					else
					{
						//add by default
						GenerateQueryType defaultConfig = new GenerateQueryType();
						defaultConfig.setUseNoLockHint(true);
						defaultConfig.setGenerateService(false);
						Query getById = QueryHelper.createGetByIdQuery(app, bom, defaultConfig);
						product.addQuery(app, getById);
					}
					if (bom.getQueries().getGetAll() != null)
					{
						Query getAll = QueryHelper.createGetAllQuery(app, bom, bom.getQueries().getGetAll());
						product.addQuery(app, getAll);
					}
				}
			}
			//second iteration
			for(ObjectType bom : bomList)
			{
				List<BaseAttributeType> attributeList = new ArrayList<BaseAttributeType>();
				attributeList.addAll(bom.getAttributes().getAttributeList());
				for(BaseAttributeType att : attributeList)
				{
					//set target object when attribute is an association or composition
					if (att instanceof BaseAttributeWithTargetType)
					{
						BaseAttributeWithTargetType attWithTarget = (BaseAttributeWithTargetType)att;
						String targeObjName = attWithTarget.getTargetObjectName();
						ObjectType targetObj = product.getBOMByApplicationAndName(app, targeObjName);
						attWithTarget.setTargetObject(targetObj);
						
						//fix multiplicity when needed
						if (att instanceof CompositionAttributeType) {

							CompositionAttributeType comp = (CompositionAttributeType) att;

							if (targetObj.getCompositionToMeCounter() > 1) {

								if (comp.getMultiplicity().equals("1")) {

									comp.setMultiplicity("0..1");
								} else if (comp.getMultiplicity().equals("1..*")) {

									comp.setMultiplicity("0..*");
								}
							}
						}
						
						//Add Id attribute
						if (attWithTarget.getMultiplicityNumber() <= 1 && attWithTarget.isPersistable())
						{
							NumberAttributeType attId = new NumberAttributeType();
							attId.setName(attWithTarget.getName() + attWithTarget.getIdSuffix());
							attId.setDataType(AttributeNumberDataType.LONG);
							attId.setIsNullable(true);
							if (attWithTarget instanceof CompositionAttributeType)
							{
								attId.setPersistable(false);
							}
							attId.setAttributeForTargetIdName("Id");
							attId.setAttributeForTargetOriginalName(attWithTarget.getName());
							attId.setTargetIdObject(targetObj);
							if (bom.getAttributeByName(attId.getName()) == null)
								bom.getAttributes().getAttributeList().add(attId);
						}
						
						//adapt composition
						if (att instanceof CompositionAttributeType) {

							adaptComposition(app, bom, att, targetObj);

						} 
						//adapt aggregation
						else if (att instanceof AggregationAttributeType) {

							adaptAggregation(app, bom, att, targetObj);
						}
					}
				}
				
				//add audit fields
				addAuditFields(bom);
			}
			
			
		}
	}

	private void addDefaultBOMs2FirstApplication(Application app) {
		ObjectType bigIntPair = new ObjectType();
		bigIntPair.setPersistable(false);
		bigIntPair.setName(Product.BIG_INT_PAIR_BOM_NAME);
		bigIntPair.setIsAutoCreated(true);
		bigIntPair.setAttributes(new AttributeCollectionType());
		

		NumberAttributeType att1 = new NumberAttributeType();
		att1.setName("Key");
		att1.setDataType(AttributeNumberDataType.LONG);
		att1.setIsNullable(false);
		bigIntPair.getAttributes().getAttributeList().add(att1);
		
		NumberAttributeType att2 = new NumberAttributeType();
		att2.setName("Value");
		att2.setDataType(AttributeNumberDataType.LONG);
		att2.setIsNullable(true);
		bigIntPair.getAttributes().getAttributeList().add(att2);
		
		
		product.addObject(app, bigIntPair);
	}

	private void addAuditFields(ObjectType bom) {
		

		NumberAttributeType attNum = new NumberAttributeType();
		attNum.setIsNullable(false);
		attNum.setName(QueryHelper.AUDIT_FIELD_NAME_PERSIST_ORDER);
		attNum.setDataType(AttributeNumberDataType.SHORT);
		attNum.setAuditAttribute(true);
		bom.getAttributes().getAttributeList().add(attNum);
		
		attNum = new NumberAttributeType();
		attNum.setIsNullable(false);
		attNum.setName(QueryHelper.AUDIT_FIELD_NAME_VERSION_NUMBER);
		attNum.setDataType(AttributeNumberDataType.LONG);
		attNum.setAuditAttribute(true);
		bom.getAttributes().getAttributeList().add(attNum);
		
		TextAttributeType attAuthor = new TextAttributeType();
		attAuthor.setIsNullable(false);
		attAuthor.setName(QueryHelper.AUDIT_FIELD_NAME_CREATE_AUTHOR);
		attAuthor.setMaxLength(BigInteger.valueOf(50));
		attAuthor.setAuditAttribute(true);
		bom.getAttributes().getAttributeList().add(attAuthor);
		
		attAuthor = new TextAttributeType();
		attAuthor.setIsNullable(false);
		attAuthor.setName(QueryHelper.AUDIT_FIELD_NAME_VERSION_AUTHOR);
		attAuthor.setMaxLength(BigInteger.valueOf(50));
		attAuthor.setAuditAttribute(true);
		bom.getAttributes().getAttributeList().add(attAuthor);
		
		DatetimeAttributeType attDate = new DatetimeAttributeType();
		attDate.setIsNullable(false);
		attDate.setName(QueryHelper.AUDIT_FIELD_NAME_CREATE_DATE);
		attDate.setAuditAttribute(true);
		bom.getAttributes().getAttributeList().add(attDate);
		
		attDate = new DatetimeAttributeType();
		attDate.setIsNullable(false);
		attDate.setName(QueryHelper.AUDIT_FIELD_NAME_VERSION_DATE);
		attDate.setAuditAttribute(true);
		bom.getAttributes().getAttributeList().add(attDate);
		
		if (bom.hasMultiLanguageAttribute())
		{
			TextAttributeType attLang = new TextAttributeType();
			attLang.setIsNullable(false);
			attLang.setName(QueryHelper.AUDIT_FIELD_DEFAULT_LANGUAGE);
			attLang.setMaxLength(BigInteger.valueOf(5));
			attLang.setAuditAttribute(true);
			bom.getAttributes().getAttributeList().add(attLang);
		}
	}

	private void adaptAggregation(Application app,ObjectType bom, BaseAttributeType att, ObjectType targetObj) {

		AggregationAttributeType agg = (AggregationAttributeType) att;
		if (bom == targetObj) {

			agg.convertToNullable();
		}
		
		if (agg.getMultiplicityNumber() <= 1 && agg.isPersistable()) {

			agg.setPopulatedByQuery(product.getQueryById(app, targetObj));

			Query aggQuery = new Query();
			aggQuery.setOwner(bom);
			bom.associateQuery(aggQuery);
			aggQuery.setOwnedBy(new BOMReferenceType());
			aggQuery.getOwnedBy().setName(bom.getName());
			aggQuery.setName(String.format("GetBy%s", agg.getName()));
			product.addQuery(app,aggQuery);
			aggQuery.setResultSet(QueryResultSet.LIST);
			aggQuery.setDescription(String.format("Returns the list of %s aggregated by the supplied %s instance Id", bom.getName(), targetObj.getName()));
			
			aggQuery.setResultSet(QueryResultSet.SINGLE);
			aggQuery.setParameters(new QueryParameters());
			aggQuery.getParameters().setDefinition(new ParameterCollection());
			aggQuery.getParameters().getDefinition().getColumnList().add(QueryHelper.createLongParameter(String.format("%sId", agg.getName()), false));
			aggQuery.setDefinition(new QueryDefinitionCollection());
			QueryDefinitionSelect select = new QueryDefinitionSelect();
			aggQuery.getDefinition().setSelect(select);
			select.setFrom(new FromClauseChoice());
			select.getFrom().setAlias("Ent");
			select.getFrom().setTable(new BOMReferenceType());
			select.getFrom().getTable().setName(bom.getName());
			WhereClause where = new WhereClause();
			aggQuery.getDefinition().getSelect().setWhere(where);
			com.mda.engine.models.expressionsModel.EqualsExpression equals = new com.mda.engine.models.expressionsModel.EqualsExpression();
			where.getExpressions().add(equals);
			TableColumnExpression colExp = new TableColumnExpression();
			colExp.setTableAlias("Ent");
			colExp.setColumnName(agg.getName() + "Id");
			equals.setFirstArg(new ExpressionChoice());
			equals.getFirstArg().setExpression(colExp);
			QueryParameterExpression paramExp = new QueryParameterExpression();
			paramExp.setParameterName(String.format("%sId", agg.getName()));
			equals.setSecondArg(new ExpressionChoice());
			equals.getSecondArg().setExpression(paramExp);
			

		} 
		else if (!agg.isAutoCreated()) {

			ObjectType aggObj = new ObjectType();
			aggObj.setIsAutoCreated(true);
			agg.setAggregationObject(aggObj);
			aggObj.setName(String.format("%s%s", bom.getName(), agg.getName()));
			aggObj.setPackageName(bom.getPackageName());
			product.addObject(app, aggObj);
			aggObj.setHeader(new HeaderType());
			aggObj.getHeader().setLabel(aggObj.getName());
			aggObj.setAttributes(new AttributeCollectionType());
			
			IndentityAttributeType id = new IndentityAttributeType();
			id.setName("Id");
			id.setIsNullable(false);
			id.setDescription("Id");
			aggObj.getAttributes().getAttributeList().add(id);
			aggObj.setIdentity(id);
			id.setMode("Automatic");
			
			String idAattName;
			String idBattName;
			if (bom == targetObj) {
				idAattName = String.format("%sIdA", bom.getName());
				idBattName = String.format("%sIdB", targetObj.getName());
			} else {
				idAattName = String.format("%sId", bom.getName());
				idBattName = String.format("%sId", targetObj.getName());
			}
			NumberAttributeType id1 = new NumberAttributeType();
			id1.setDataType(AttributeNumberDataType.LONG);
			id1.setName(idAattName);
			id1.setDescription(String.format("Id of the %s instance in the %s aggregation.", bom.getName(), agg.getName()));
			id1.setIsNullable(false);
			NumberAttributeType id2 = new NumberAttributeType();
			id2.setDataType(AttributeNumberDataType.LONG);
			id2.setName(idBattName);
			id2.setDescription(String.format("Id of the %s instance in the %s aggregation.", bom.getName(), agg.getName()));
			id2.setIsNullable(false);
			aggObj.getAttributes().getAttributeList().add(id1);
			aggObj.getAttributes().getAttributeList().add(id2);
			aggObj.setPackageName(bom.getPackageName());
			String queryName = String.format("By%s%s", bom.getName(), agg.getName());
			Query aggQuery = createMultipleAggregationQuery(app, bom, targetObj, aggObj.getName(), agg, aggObj, idAattName, idBattName,queryName);
			agg.setPopulatedByQuery(aggQuery);
			//agg.getPopulatedByQuery().setAddPagingLogic(agg.isPaged());
		}
	}

	private Query createMultipleAggregationQuery(Application app, ObjectType bom, ObjectType targetObj, String aggAttributeName, AggregationAttributeType agg, ObjectType aggObj, String idAattName, String idBattName, String queryName) {
		Query aggQuery = new Query();
		aggQuery.setOwner(targetObj);
		targetObj.associateQuery(aggQuery);
		aggQuery.setOwnedBy(new BOMReferenceType());
		aggQuery.getOwnedBy().setName(targetObj.getName());
		product.addQuery(app, aggQuery);
		aggQuery.setName(queryName);
		aggQuery.setResultSet(QueryResultSet.LIST);
		aggQuery.setUseNolockHint(true);
		aggQuery.setDescription(String.format("List all instances that are referenced by the %s %s aggregation.", bom.getName(), aggAttributeName));
		aggQuery.setParameters(new QueryParameters());
		aggQuery.getParameters().setDefinition(new ParameterCollection());
		aggQuery.getParameters().getDefinition().getColumnList().add(QueryHelper.createLongParameter(String.format("%sId", bom.getName()), false));
		aggQuery.setDefinition(new QueryDefinitionCollection());
		QueryDefinitionSelect select = new QueryDefinitionSelect();
		aggQuery.getDefinition().setSelect(select);
		select.setFrom(new FromClauseChoice());
		select.getFrom().setAlias("Ent");
		select.getFrom().setTable(new BOMReferenceType());
		select.getFrom().getTable().setName(targetObj.getName());
		
		InnerJoinClause join = new InnerJoinClause();
		select.getFrom().getClauseList().add(join);
		join.setWith(new WithClause());
		join.getWith().setTable(new BOMReferenceType());
		join.getWith().getTable().setName(aggAttributeName);
		join.getWith().setOn(new OnClause());
		join.getWith().setAlias("Agg");
		com.mda.engine.models.expressionsModel.EqualsExpression equals = new com.mda.engine.models.expressionsModel.EqualsExpression();
		join.getWith().getOn().getExpressions().add(equals);
		
		TableColumnExpression colExp = new TableColumnExpression();
		colExp.setTableAlias("Agg");
		colExp.setColumnName(idBattName);
		equals.setFirstArg(new ExpressionChoice());
		equals.getFirstArg().setExpression(colExp);
		colExp = new TableColumnExpression();
		colExp.setTableAlias("Ent");
		colExp.setColumnName("Id");
		equals.setSecondArg(new ExpressionChoice());
		equals.getSecondArg().setExpression(colExp);
		
		WhereClause where = new WhereClause();
		aggQuery.getDefinition().getSelect().setWhere(where);
		equals = new com.mda.engine.models.expressionsModel.EqualsExpression();
		where.getExpressions().add(equals);
		colExp = new TableColumnExpression();
		colExp.setTableAlias("Agg");
		colExp.setColumnName(idAattName);
		equals.setFirstArg(new ExpressionChoice());
		equals.getFirstArg().setExpression(colExp);
		QueryParameterExpression paramExp = new QueryParameterExpression();
		paramExp.setParameterName(String.format("%sId", bom.getName()));
		equals.setSecondArg(new ExpressionChoice());
		equals.getSecondArg().setExpression(paramExp);
		return aggQuery;
	}
	
	private void adaptComposition(Application app, ObjectType bom, BaseAttributeType att, ObjectType targetObj) {
		
		CompositionAttributeType comp = (CompositionAttributeType) att;
		
		if (bom.isPersistable() && comp.isPersistable()) {

			String childtoParentAggName = String.format("%s%s", bom.getName(), comp.getName());

			//add auto created association
			AggregationAttributeType childToParentAgg = new AggregationAttributeType();
			childToParentAgg.setName(childtoParentAggName);
			childToParentAgg.setMultiplicity(comp.getMultiplicityNumber() < 1 ? "0..1" : "1");
			childToParentAgg.setAvoidGenerateAggIndex(comp.isAvoidGenerateAggIndex());
			childToParentAgg.setTargetObjectName(bom.getName());
			childToParentAgg.setTargetObject(bom);
			childToParentAgg.setAutoCreated(true);
			childToParentAgg.setPopulatedByQuery(product.getQueryById(app, bom));
			comp.setTargetObjectParentAggregation(childToParentAgg);
			targetObj.getAttributes().getAttributeList().add(childToParentAgg);
			
			//Add Id attribute
			NumberAttributeType attId = new NumberAttributeType();
			attId.setName(childtoParentAggName + "Id");
			attId.setDataType(AttributeNumberDataType.LONG);
			attId.setIsNullable(true);
			attId.setAttributeForTargetIdName("Id");
			attId.setAttributeForTargetOriginalName(childtoParentAggName);
			attId.setTargetIdObject(bom);
			targetObj.getAttributes().getAttributeList().add(attId);
			
			if (targetObj.isPersistable() && !(targetObj instanceof InterfaceType))
			{
				//add association query - to list all children by its father
				Query query = new Query();
				query.setOwner(targetObj);
				targetObj.associateQuery(query);
				query.setOwnedBy(new BOMReferenceType());
				query.getOwnedBy().setName(targetObj.getName());
				product.addQuery(app, query);
				query.setName(String.format("By%s%s", bom.getName(), att.getName()));
				query.setUseNolockHint(true);

				if (comp.getMultiplicityNumber() <= 1) {

					query.setResultSet(QueryResultSet.SINGLE);
					query.setDescription(String.format("Retrieves the child instance from the supplied %s in the %s composition.", bom.getName(),att.getName()));

				} else {

					query.setResultSet(QueryResultSet.LIST);
					query.setDescription(String.format("Retrieves the children instances from the supplied %s in the %s composition.", bom.getName(),att.getName()));
				}

				//query.setAddPagingLogic(comp.isPaged());
				query.setParameters(new QueryParameters());
				query.getParameters().setDefinition(new ParameterCollection());
				query.getParameters().getDefinition().getColumnList().add(QueryHelper.createLongParameter(String.format("%sId", bom.getName()), false));
				query.setDefinition(new QueryDefinitionCollection());
				QueryDefinitionSelect select = new QueryDefinitionSelect();
				query.getDefinition().setSelect(select);
				select.setFrom(new FromClauseChoice());
				select.getFrom().setAlias("Ent");
				select.getFrom().setTable(new BOMReferenceType());
				select.getFrom().getTable().setName(targetObj.getName());
				WhereClause where = new WhereClause();
				query.getDefinition().getSelect().setWhere(where);
				com.mda.engine.models.expressionsModel.EqualsExpression equals = new com.mda.engine.models.expressionsModel.EqualsExpression();
				where.getExpressions().add(equals);
				TableColumnExpression colExp = new TableColumnExpression();
				colExp.setTableAlias("Ent");
				colExp.setColumnName(childToParentAgg.getName() + "Id");
				equals.setFirstArg(new ExpressionChoice());
				equals.getFirstArg().setExpression(colExp);
				QueryParameterExpression paramExp = new QueryParameterExpression();
				paramExp.setParameterName(String.format("%sId", bom.getName()));
				equals.setSecondArg(new ExpressionChoice());
				equals.getSecondArg().setExpression(paramExp);
				
				comp.setPopulatedByQuery(query);
			}
		}
	}
	
	private void adaptInterface(Application app, InterfaceType interf)
	{
		for(BOMReferenceType bomRef : interf.getImplementedBy().getObject()) {
			
			ObjectType target = product.getBOMByApplicationAndName(app, bomRef.getName());
			target.addImplementedInterface(interf.getName());
			//add attributes
			for(BaseAttributeType att : interf.getAttributes().getAttributeList()) 
			{												
				if (target.getAttributeByName(att.getName()) == null)
				{							
					BaseAttributeType clonedAtt = (BaseAttributeType)att.clone();
					target.getAttributes().getAttributeList().add(clonedAtt);
				}
			}
			
			//add indexes
		    if(interf.getIndexes() != null && interf.getIndexes().getIndex() != null && !interf.getIndexes().getIndex().isEmpty())
		    {
		    	if(target.getIndexes() == null || target.getIndexes().getIndex() == null )
		    	{
		    		IndexCollection colection = new IndexCollection();
		    		target.setIndexes(colection);
		    	}
				for (Index index : interf.getIndexes().getIndex()) {
	
					Index newIndex = new Index();
					newIndex.setFillFactor(80);
					newIndex.setName(target.getName().toUpperCase()+index.getName().toUpperCase());
					newIndex.setUnique(index.isUnique());
					newIndex.setAttributes(index.getAttributes());
					target.getIndexes().getIndex().add(newIndex);
				} 
			}
		}
	}
	
	
}
