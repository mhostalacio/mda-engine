package com.mda.engine.model2model;

import com.mda.engine.core.MdaLogger;
import com.mda.engine.core.Product;
import com.mda.engine.models.businessTransactionModel.BusinessTransaction;
import com.mda.engine.models.common.ModelAttribute;
import com.mda.engine.models.common.ModelAttributeObject;
import com.mda.engine.models.configurationManagementModel.Application;

public class BusinessTransactionAdapter extends MdaLogger {

	private Product product;
	
	public BusinessTransactionAdapter(Product product)
	{
		this.product = product;
	}
	
	public void adaptBTMModel()
	{
		for(Application app : product.getApplications())
		{
			for(BusinessTransaction btm : product.getApplicationBTMs(app))
			{
				if (btm.getOwnedBy() != null && btm.getOwner() == null)
				{
					btm.setOwner(product.getBOMByApplicationAndName(app, btm.getOwnedBy().getName()));
				}
				
				btm.setTransactionId(app.getUniqueName() + "_" + btm.getOwner().getName() + "_" + btm.getName());
				app.getProduct().addTransactionMapping(app, btm.getTransactionId(), btm.getDescription());
				
				if (btm.getInputs() != null && btm.getInputs().getAttributeList() != null)
				{
					for(ModelAttribute attribute : btm.getInputs().getAttributeList())
					{
						if (attribute instanceof ModelAttributeObject)
						{
							ModelAttributeObject attObj = (ModelAttributeObject)attribute;
							setBom(app, attObj);
						}
					}
				}
				if (btm.getOutputs() != null && btm.getOutputs().getAttributeList() != null)
				{
					for(ModelAttribute attribute : btm.getOutputs().getAttributeList())
					{
						if (attribute instanceof ModelAttributeObject)
						{
							ModelAttributeObject attObj = (ModelAttributeObject)attribute;
							setBom(app, attObj);
							
						}
					}
				}
			}
		}
	}

	private void setBom(Application app, ModelAttributeObject attObj) {
		if (attObj.getApplicationName() != null)
		{
			attObj.setBom(product.getBOMByApplicationAndName(product.getApplicationByName(attObj.getApplicationName()), attObj.getObjectType()));
		}else
		{
			attObj.setBom(product.getBOMByApplicationAndName(app, attObj.getObjectType()));
		}
	}
}
