package com.mda.engine.model2model;

import java.math.BigInteger;

import org.apache.commons.lang.NotImplementedException;
import org.jvnet.jaxb2_commons.lang.StringUtils;

import com.mda.engine.core.MdaLogger;
import com.mda.engine.core.Product;
import com.mda.engine.models.businessObjectModel.AttributeNumberDataType;
import com.mda.engine.models.businessObjectModel.BaseAttributeType;
import com.mda.engine.models.businessObjectModel.BaseAttributeWithTargetType;
import com.mda.engine.models.businessObjectModel.BinaryAttributeType;
import com.mda.engine.models.businessObjectModel.BooleanAttributeType;
import com.mda.engine.models.businessObjectModel.DateAttributeType;
import com.mda.engine.models.businessObjectModel.DatetimeAttributeType;
import com.mda.engine.models.businessObjectModel.EnumAttributeType;
import com.mda.engine.models.businessObjectModel.GuidAttributeType;
import com.mda.engine.models.businessObjectModel.IndentityAttributeType;
import com.mda.engine.models.businessObjectModel.LongtextAttributeType;
import com.mda.engine.models.businessObjectModel.MultiLanguageBaseAttributeType;
import com.mda.engine.models.businessObjectModel.NumberAttributeType;
import com.mda.engine.models.businessObjectModel.ObjectType;
import com.mda.engine.models.businessObjectModel.TextAttributeType;
import com.mda.engine.models.businessObjectModel.TimeAttributeType;
import com.mda.engine.models.businessObjectModel.TimedurationAttributeType;
import com.mda.engine.models.businessObjectModel.TimestampAttributeType;
import com.mda.engine.models.businessObjectModel.XmlAttributeType;
import com.mda.engine.models.configurationManagementModel.Application;
import com.mda.engine.models.dataModel.BigIntTableColumn;
import com.mda.engine.models.dataModel.BinaryTableColumn;
import com.mda.engine.models.dataModel.BitTableColumn;
import com.mda.engine.models.dataModel.ColumnCollection;
import com.mda.engine.models.dataModel.ColumnDirection;
import com.mda.engine.models.dataModel.ColumnReference;
import com.mda.engine.models.dataModel.DataTable;
import com.mda.engine.models.dataModel.DateTimeTableColumn;
import com.mda.engine.models.dataModel.DecimalTableColumn;
import com.mda.engine.models.dataModel.DoubleTableColumn;
import com.mda.engine.models.dataModel.FloatTableColumn;
import com.mda.engine.models.dataModel.ForeignKey;
import com.mda.engine.models.dataModel.ForeignKeyCollection;
import com.mda.engine.models.dataModel.IntTableColumn;
import com.mda.engine.models.dataModel.NvarcharTableColumn;
import com.mda.engine.models.dataModel.PrimaryKey;
import com.mda.engine.models.dataModel.SmallIntTableColumn;
import com.mda.engine.models.dataModel.StringTableColumn;
import com.mda.engine.models.dataModel.TableColumn;
import com.mda.engine.models.dataModel.TinyIntTableColumn;
import com.mda.engine.models.dataModel.UniqueIdentifierTableColumn;
import com.mda.engine.models.dataModel.VarcharTableColumn;
import com.mda.engine.models.dataModel.XMLTableColumn;
import com.mda.engine.utils.QueryHelper;

public class DataModelAdapter extends MdaLogger {
	
	private Product product;
	
	public DataModelAdapter(Product product)
	{
		this.product = product;
	}
	
	public void adaptDataModel()
	{
		for(Application app : product.getApplications())
		{
			for(ObjectType bom : product.getApplicationBOMs(app))
			{
				if (bom.isPersistable())
				{
					DataTable tb = new DataTable();
					tb.setName(bom.getName());
					tb.setOwner(bom);
					tb.setColumns(new ColumnCollection());
					tb.setForeignKeys(new ForeignKeyCollection());
					product.addTable(app, tb);
					
					if (bom.getPrimaryKey() != null)
					{
						//TODO
					}
					else //if (!bom.getIsAutoCreated())
					{
						PrimaryKey key = new PrimaryKey();
						tb.setPrimaryKey(key);
						ColumnReference colRef = new ColumnReference();
						colRef.setName("Id");
						colRef.setDirection(ColumnDirection.ASC);
						key.getColumns().add(colRef);
					}
					for(BaseAttributeType att : bom.getAttributes().getAttributeList())
					{
						if (att.isPersistable())
						{
							TableColumn col = createTableColumnFromAttribute(tb, att);
							att.setMatchingTableColumn(col);
							
							if (!StringUtils.isEmpty(att.getAttributeForTargetIdName()))
							{
								ForeignKey fk = QueryHelper.createForeignKey(tb.getName(), att.getTargetIdObject().getName(), att.getName(), att.getAttributeForTargetIdName());
								tb.getForeignKeys().getForeignKeyList().add(fk);
							}
						}
					}
					
					if (bom.hasMultiLanguageAttribute())
					{
						DataTable resourcesTable = new DataTable();
						resourcesTable.setIsResourcesTable(true);
						resourcesTable.setName(bom.getName() + "_Resources");
						resourcesTable.setOwner(bom);
						resourcesTable.setColumns(new ColumnCollection());
						product.addTable(app, resourcesTable);
						
						BigIntTableColumn colId = new BigIntTableColumn();
						colId.setName("ObjectId");
						resourcesTable.getColumns().getColumnList().add(colId);
						
						VarcharTableColumn colVarchar = new VarcharTableColumn();
						colVarchar.setName("Lang");
						colVarchar.setMaxSize(BigInteger.valueOf(5));
						resourcesTable.getColumns().getColumnList().add(colVarchar);
						
						colVarchar = new VarcharTableColumn();
						colVarchar.setName("MainLang");
						colVarchar.setMaxSize(BigInteger.valueOf(2));
						resourcesTable.getColumns().getColumnList().add(colVarchar);
						
						colVarchar = new VarcharTableColumn();
						colVarchar.setName("ColumnName");
						colVarchar.setMaxSize(BigInteger.valueOf(50));
						resourcesTable.getColumns().getColumnList().add(colVarchar);
						
						NvarcharTableColumn colNVarchar = new NvarcharTableColumn();
						colNVarchar.setName("Value");
						resourcesTable.getColumns().getColumnList().add(colNVarchar);
						
						
						IntTableColumn colVersion = new IntTableColumn();
						colVersion.setName(QueryHelper.AUDIT_FIELD_NAME_VERSION_NUMBER);
						colVersion.setAuditColumn(true);
						resourcesTable.getColumns().getColumnList().add(colVersion);
	
						
						PrimaryKey key = new PrimaryKey();
						resourcesTable.setPrimaryKey(key);
						ColumnReference colRef = new ColumnReference();
						colRef.setName("ObjectId");
						colRef.setDirection(ColumnDirection.ASC);
						key.getColumns().add(colRef);
						colRef = new ColumnReference();
						colRef.setName("Lang");
						colRef.setDirection(ColumnDirection.ASC);
						key.getColumns().add(colRef);
						colRef = new ColumnReference();
						colRef.setName("ColumnName");
						colRef.setDirection(ColumnDirection.ASC);
						key.getColumns().add(colRef);
					}
				}
			}
		}
	}
	
	private TableColumn createTableColumnFromAttribute(DataTable tb, BaseAttributeType att)
	{
		TableColumn col = null;

		if (att instanceof TimestampAttributeType || att instanceof DatetimeAttributeType || att instanceof DateAttributeType)
		{
			col = new DateTimeTableColumn();
		}
		else if (att instanceof IndentityAttributeType)
		{
			col = new BigIntTableColumn();
		}
		else if (att instanceof NumberAttributeType)
		{
			NumberAttributeType numberAtt = (NumberAttributeType)att;
			if(numberAtt.getDataType().equals(AttributeNumberDataType.DECIMAL))
			{
				col = new DecimalTableColumn();
				((DecimalTableColumn)col).setNumberOfDecimalPlaces(numberAtt.getDec().intValue());
				((DecimalTableColumn)col).setNumberOfIntegerPlaces(numberAtt.getLen().intValue());
			}
			else if(numberAtt.getDataType().equals(AttributeNumberDataType.DOUBLE))
			{
				col = new DoubleTableColumn();
			}
			else if(numberAtt.getDataType().equals(AttributeNumberDataType.FLOAT))
			{
				col = new FloatTableColumn();
			}
			else if(numberAtt.getDataType().equals(AttributeNumberDataType.INTEGER))
			{
				col = new IntTableColumn();
			}
			else if(numberAtt.getDataType().equals(AttributeNumberDataType.LONG))
			{
				col = new BigIntTableColumn();
			}
			else if(numberAtt.getDataType().equals(AttributeNumberDataType.SHORT))
			{
				col = new SmallIntTableColumn();
			}
			else
			{
				throw new NotImplementedException("TableColumn to attribute of type " + att.getClass().getName());
			}
		}
		else if (att instanceof XmlAttributeType)
		{
			col = new XMLTableColumn();
		}
		else if (att instanceof BaseAttributeWithTargetType)
		{
//			BaseAttributeWithTargetType targetAtt = (BaseAttributeWithTargetType)att;
//			if (targetAtt.getMultiplicityNumber() <= 1)
//			{
//				col = new BigIntTableColumn();
//			}
		}
		else if (att instanceof GuidAttributeType)
		{
			col = new UniqueIdentifierTableColumn();
		}
		else if (att instanceof EnumAttributeType)
		{
			col = new TinyIntTableColumn();
		}
		else if (att instanceof BooleanAttributeType)
		{
			col = new BitTableColumn();
		}
		else if (att instanceof BinaryAttributeType)
		{
			col = new BinaryTableColumn();
		}
		else if (att instanceof TextAttributeType || att instanceof LongtextAttributeType)
		{
			MultiLanguageBaseAttributeType attribute = (MultiLanguageBaseAttributeType)att;
			if (attribute.isUseNVARCHAR())
			{
				col = new NvarcharTableColumn();
			}
			else
			{
				col = new VarcharTableColumn();
			}
			if (att instanceof TextAttributeType)
			{
				if (((TextAttributeType)attribute).getMaxLength() != null)
				{
					StringTableColumn column = (StringTableColumn)col;
					column.setMaxSize(((TextAttributeType)attribute).getMaxLength());
				}
			}
		}
		else
		{
			throw new NotImplementedException("TableColumn to attribute of type " + att.getClass().getName());
		}
		
		if (col != null)
		{
			tb.getColumns().getColumnList().add(col);
			col.setAuditColumn(att.isAuditAttribute());
			col.setName(att.getName());
			col.setIsNullable(att.isIsNullable());
		}

		return col;
	}
}
