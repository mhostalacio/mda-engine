package com.mda.engine.model2model;

import com.mda.engine.core.MdaLogger;
import com.mda.engine.core.Product;
import com.mda.engine.models.businessObjectModel.AttributeCollectionType;
import com.mda.engine.models.businessObjectModel.ObjectType;
import com.mda.engine.models.businessTransactionModel.BusinessTransaction;
import com.mda.engine.models.common.ModelAttributeCollection;
import com.mda.engine.models.common.ModelAttributeObject;
import com.mda.engine.models.configurationManagementModel.Application;
import com.mda.engine.models.dataModel.DataBaseItem;
import com.mda.engine.models.queryModel.Query;
import com.mda.engine.models.queryModel.QueryParametersReference;
import com.mda.engine.models.queryModel.QueryResultSet;
import com.mda.engine.utils.CodeHelper;
import com.mda.engine.utils.QueryHelper;

public class QueryModelAdapter extends MdaLogger {

	private Product product;
	
	public QueryModelAdapter(Product product)
	{
		this.product = product;
	}
	
	public void adaptDataModel() throws Exception
	{
		for(Application app : product.getApplications())
		{
			for(Query query : product.getApplicationQueries(app))
			{				
				BusinessTransaction associatedBtm = new BusinessTransaction();
				associatedBtm.setOriginatorQuery(query);
				associatedBtm.setOwnedBy(query.getOwnedBy());
				associatedBtm.setName(query.getName());
				associatedBtm.setInputs(new ModelAttributeCollection());
				associatedBtm.setOutputs(new ModelAttributeCollection());
				associatedBtm.setApplication(app);
				
				if (query.getOwnedBy() != null && query.getOwner() == null)
				{
					query.setOwner(product.getBOMByApplicationAndName(app, query.getOwnedBy().getName()));
				}
				//Set extended name
				if (query.getOwner() != null)
				{
					query.setExtendedName(query.getOwner().getName() + query.getName());
					associatedBtm.setPackageName(query.getOwner().getPackageName());
					associatedBtm.setName(query.getOwner().getName() + query.getName());
				}
				else
				{
					query.setExtendedName(query.getName());
				}
				
				//Set referenced query
				if (query.getParameters() != null) {
					if (query.getParameters().getReference() != null) {
						QueryParametersReference prmsRef = query.getParameters().getReference();
						Query refQuery = product.getQueryByApplicationAndName(app, prmsRef.getQueryName());
						prmsRef.setReferencedQuery(refQuery);
					}
				}
				
				
				if(query.getResultType() != null && query.getResultType().getCustom()!= null && query.getResultType().getCustom().getColumnList() != null)
		    	{
					ObjectType resultObject = new ObjectType();
					resultObject.setIsQueryObject(true);
					resultObject.setPersistable(false);
					resultObject.setName(query.getResultSetSingleClassName());
					resultObject.setPackageName(query.getOwner().getPackageName());
					resultObject.setAttributes(new AttributeCollectionType());
					for(DataBaseItem item : query.getResultType().getCustom().getColumnList())
					{
						resultObject.getAttributes().getAttributeList().add(CodeHelper.createAttributeFromColumn(item));
					}
					product.addObject(app, resultObject);
					
					ModelAttributeObject att = new ModelAttributeObject();
					att.setName("Result");
					att.setBom(resultObject);
					att.setObjectType(resultObject.getName());
					att.setIsList(query.getResultSet().equals(QueryResultSet.LIST));
					associatedBtm.getOutputs().getAttributeList().add(att);
					
		    	} else if(query.getResultType() != null && query.getResultType().getObject() != null){
		    		
					ObjectType referencedBom = product.getBOMByApplicationAndName(app, query.getResultType().getObject().getName());
					
		    		ModelAttributeObject att = new ModelAttributeObject();
					att.setName("Result");
					att.setBom(referencedBom);
					att.setObjectType(referencedBom.getName());
					associatedBtm.getOutputs().getAttributeList().add(att);
					if (query.getResultSet().equals(QueryResultSet.SINGLE))
					{
						att.setIsList(false);
					}
					else
					{
						att.setIsList(true);
					}
		    	} else {
					
					ModelAttributeObject att = new ModelAttributeObject();
					att.setName("Result");
					att.setBom(query.getOwner());
					att.setObjectType(query.getOwner().getName());
					associatedBtm.getOutputs().getAttributeList().add(att);
					if (query.getResultSet().equals(QueryResultSet.SINGLE))
					{
						att.setIsList(false);
					}
					else
					{
						att.setIsList(true);
					}
				}
				if (query.getParameters() != null && query.getParameters().getDefinition() != null)
		    	{
					ObjectType argumenObject = new ObjectType();
					argumenObject.setIsQueryObject(true);
					argumenObject.setPersistable(false);
					argumenObject.setName(query.getArgumentsClassName());
					argumenObject.setPackageName(query.getOwner().getPackageName());
					argumenObject.setAttributes(new AttributeCollectionType());
					for(DataBaseItem item : query.getParameters().getDefinition().getColumnList())
					{
						argumenObject.getAttributes().getAttributeList().add(QueryHelper.createAttributeFromDataBaseItem(item, this.product, app));
					}
					product.addObject(app, argumenObject);
					
					ModelAttributeObject att = new ModelAttributeObject();
					att.setName("Arguments");
					att.setBom(argumenObject);
					att.setObjectType(argumenObject.getName());
					associatedBtm.getInputs().getAttributeList().add(att);
		    	}
				
				
				product.addBTM(app, associatedBtm);
			}

		}
	}
}
