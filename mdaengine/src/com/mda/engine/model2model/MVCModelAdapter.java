package com.mda.engine.model2model;

import java.util.ArrayList;

import org.apache.commons.lang.NullArgumentException;

import com.mda.engine.core.MdaLogger;
import com.mda.engine.core.Product;
import com.mda.engine.models.businessObjectModel.EnumAttributeType;
import com.mda.engine.models.businessObjectModel.LovDefinitionType;
import com.mda.engine.models.businessObjectModel.LovItem;
import com.mda.engine.models.businessObjectModel.LovItemCollection;
import com.mda.engine.models.businessObjectModel.ObjectType;
import com.mda.engine.models.businessTransactionModel.InitMVCBusinessTransaction;
import com.mda.engine.models.common.ModelAttribute;
import com.mda.engine.models.common.ModelAttributeCollection;
import com.mda.engine.models.common.ModelAttributeEnum;
import com.mda.engine.models.common.ModelAttributeObject;
import com.mda.engine.models.configurationManagementModel.Application;
import com.mda.engine.models.mvc.MVCControllerAction;
import com.mda.engine.models.mvc.MVCControllerActionCollection;
import com.mda.engine.models.mvc.MVCInstance;
import com.mda.engine.models.mvc.MVCModel;
import com.mda.engine.models.mvc.MVCViewElement;
import com.mda.engine.models.mvc.MVCViewLabel;
import com.mda.engine.models.mvc.MVCViewRepeater;

public class MVCModelAdapter extends MdaLogger {
	private Product product;
	
	public MVCModelAdapter(Product product)
	{
		this.product = product;
	}
	
	public void adaptMvcModel() throws Exception
	{
		for(Application app : product.getApplications())
		{
			for(MVCInstance mvc : product.getApplicationMvcs(app))
			{
				this.createMVCInitActionsEnum(app, mvc);
				this.createInitMVCBusinessTransaction(app, mvc);
				
				if (mvc.getModel() != null && mvc.getModel().getAttributes() != null)
				{
					mvc.getModel().setMvc(mvc);
					for(ModelAttribute attribute : mvc.getModel().getAttributes().getAttributeList())
					{
						if (attribute instanceof ModelAttributeObject)
						{
							ModelAttributeObject attObj = (ModelAttributeObject)attribute;
							if (attObj.getApplicationName() != null)
							{
								attObj.setBom(product.getBOMByApplicationAndName(product.getApplicationByName(attObj.getApplicationName()), attObj.getObjectType()));
							}else
							{
								attObj.setBom(product.getBOMByApplicationAndName(app, attObj.getObjectType()));
							}
							
						}
					}
				}
				if (mvc.getView() != null && mvc.getView().getElements() != null)
				{
					for(MVCViewElement element : mvc.getView().getElements().getElementList())
					{
						setElementBinds(mvc, element);
					}
				}
				if (mvc.getController() != null && mvc.getController().getActions() != null && mvc.getController().getActions().getActionList() != null)
				{
					for(MVCControllerAction action : mvc.getController().getActions().getActionList())
					{
						action.setActionId(app.getUniqueName() + "_" + mvc.getName() + "_" + action.getActionName());
						app.getProduct().addTransactionMapping(app, action.getActionId(), action.getDescription());
					}
				}
			}
		}
	}

	private void setElementBinds(MVCInstance mvc, MVCViewElement element)
			throws Exception {
		
    	if (element.getId() == null)
    	{
    		throw new Exception("Missing element id in MVC '" + mvc.getName() + "' type " + element.getClassName());
    	}
		
		
		element.setBinds(mvc.getModel());
		if (element instanceof MVCViewLabel)
		{
			MVCViewLabel label = (MVCViewLabel)element;
			if (label.getFor() != null)
			{
				MVCViewElement forElem = mvc.getView().getElements().getElementById(label.getFor());
				label.setForElement(forElem);
			}
		}

		
		if (element instanceof MVCViewRepeater)
		{
			MVCViewRepeater comp = (MVCViewRepeater)element;
    		if (comp.getTemplate() != null && comp.getTemplate().getElementList() != null)
    		{
    			for (MVCViewElement child : comp.getTemplate().getElementList())
    			{
    				setElementBinds(mvc, child);
    			}
    		}
		}
		
		if (element instanceof com.mda.engine.core.IMVCViewComposedElement)
    	{
    		com.mda.engine.core.IMVCViewComposedElement comp = (com.mda.engine.core.IMVCViewComposedElement)element;
    		if (comp.getChildElements() != null && comp.getChildElements().getElementList() != null)
    		{
    			for (MVCViewElement child : comp.getChildElements().getElementList())
    			{
    				setElementBinds(mvc, child);
    			}
    		}
    	}
	}
	
	private void createInitMVCBusinessTransaction(Application app, MVCInstance mvc){
		
		MVCModel model;
		String transactionName;
		String transactionDescription;
		InitMVCBusinessTransaction initTransaction;
		ModelAttributeCollection btmInputs;
		ModelAttributeCollection btmOutputs;
		LovDefinitionType initActionsLov;
		
		model = mvc.getModel();
		initActionsLov = mvc.getInitActionsLov();
		transactionName = com.mda.engine.utils.StringUtils.format("Init{0}", mvc.getName());
		transactionDescription = com.mda.engine.utils.StringUtils.format("Business transaction to perform the initialization of the %s model.", mvc.getName());
		
		btmInputs = new ModelAttributeCollection();
		btmOutputs = new ModelAttributeCollection();
		
		if(initActionsLov != null){
			ModelAttributeEnum initActionLovAttribute;
			
			initActionLovAttribute = new ModelAttributeEnum();
			initActionLovAttribute.setName("InitAction");
			initActionLovAttribute.setEnumType(initActionsLov.getName());
			
			btmInputs.getAttributeList().add(initActionLovAttribute);
		}
		
		if(model != null){
			
			if(model.getInitTransactionInputs() != null){
				for(ModelAttribute inputAttr : model.getInitTransactionInputs().getAttributeList()){
					btmInputs.getAttributeList().add(inputAttr);
				}
			}
			
			if(model.getAttributes() != null){
				for(ModelAttribute modelAttr : mvc.getModel().getAttributes().getAttributeList()){
					btmOutputs.getAttributeList().add(modelAttr);
				}				
			}
		}
		
		initTransaction = new InitMVCBusinessTransaction();
		initTransaction.setName(transactionName);
		initTransaction.setDescription(transactionDescription);
		initTransaction.setInputs(btmInputs);
		initTransaction.setOutputs(btmOutputs);
		initTransaction.setOwnedBy(mvc.getOwnedBy());
		initTransaction.setOwner(product.getBOMByApplicationAndName(app, mvc.getOwnedBy().getName()));
		initTransaction.setPackageName(mvc.getPackageName());
		
		mvc.setInitTransaction(initTransaction);
		this.product.addBTM(app, initTransaction);
	}
	
	private void createMVCInitActionsEnum(Application app, MVCInstance mvc){
		
		ObjectType owner;
		MVCControllerActionCollection controllerActions;
		LovItemCollection lovItems;
		LovDefinitionType initActionsLov;
		
		lovItems = new LovItemCollection();
		owner = product.getBOMByApplicationAndName(app, mvc.getOwnedBy().getName());
		initActionsLov = new LovDefinitionType();
		initActionsLov.setAcceptExtraValues(false);
		initActionsLov.setDescription(mvc.getName() + " Init Actions");
		initActionsLov.setLabel(mvc.getName() + " Init Actions");
		initActionsLov.setName(mvc.getName() + "InitActions");
		initActionsLov.setTranslatable(false);
		initActionsLov.setItems(lovItems);
		
		controllerActions = mvc.getController().getActions();
		
		if(controllerActions != null && controllerActions.getActionList() != null){
			int itemKey = 0;
			
			for(MVCControllerAction action : controllerActions.getActionList()){

				if(action.isIsInitAction() != null && action.isIsInitAction().booleanValue()){
					LovItem item;
					
					item = new LovItem();
					item.setCode(action.getActionName());
					item.setDescription(action.getActionName());
					item.setKey(itemKey);
					item.setLabel(action.getActionName());
					
					itemKey++;
					lovItems.getItem().add(item);
				}
			}
		}
		
		if(lovItems.getItem().size() == 0){
			LovItem item;
			
			item = new LovItem();
			item.setCode("Default");
			item.setDescription("Default");
			item.setKey(0);
			item.setLabel("Default");

			lovItems.getItem().add(item);		
		}
		
		mvc.setInitActionsLov(initActionsLov);
		owner.associateLov(initActionsLov);
	}
}
