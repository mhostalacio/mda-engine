package com.mda.engine.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.HashMap;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SpringLayout;
import javax.swing.TransferHandler;

import com.mda.engine.core.ApplicationScope;
import com.mda.engine.main.Starter;

import javax.swing.JLabel;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public class MainPanel extends JPanel implements ActionListener, MouseListener {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6506175461134818652L;
	private HashMap<ApplicationScope, Boolean> generateScopes = new HashMap<ApplicationScope, Boolean>();
	private JCheckBox checkDataLayer = new JCheckBox();;
	private JCheckBox checkEntitiesLayer = new JCheckBox();;
	private JCheckBox checkServicesLayer = new JCheckBox();;
	private JCheckBox checkMVCLayer = new JCheckBox();;
	private JButton butGenerate = new JButton();
	private JTextArea txtProps = new JTextArea();
	private JTextArea txtOutput = new JTextArea();
	
	public MainPanel()
	{
		generateScopes.put(ApplicationScope.DataLayer, true);
		generateScopes.put(ApplicationScope.EntitiesLayer, true);
		generateScopes.put(ApplicationScope.ServicesLayer, true);
		generateScopes.put(ApplicationScope.MVCLayer, true);
		setBackground(new Color(255, 255, 255));
		//this.setSize(new Dimension(400, 300));
		BoxLayout layout = new BoxLayout(this, BoxLayout.Y_AXIS);
		//layout.setAlignment(FlowLayout.TRAILING);
		setLayout(layout);
		createStandardComponents();
	}
	
	private void createStandardComponents() {
		
		//Default Properties Panel
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 255, 255));
		GridLayout layout = new GridLayout(0,1);
		panel.setLayout(layout);
		panel.setPreferredSize(new Dimension(380, 100));
		panel.setBorder(BorderFactory.createTitledBorder("Default Properties")); 
		txtProps.setText("appfolder=C:\\@work\\Certifiq\npropfolder=C:\\@work\\Certifiq\\Config\\Properties");
		txtProps.setEditable(true);
		txtProps.setPreferredSize(new Dimension(200, 90));
		panel.add(txtProps);
		add(panel);
		

		//Generation Scope Panel
		panel = new JPanel();
		panel.setBackground(new Color(255, 255, 255));
		layout = new GridLayout(0,2);
		panel.setLayout(layout);
		panel.setPreferredSize(new Dimension(380, 110));
		panel.setBorder(BorderFactory.createTitledBorder("Generation Scope")); 
		add(panel);

		setCheckBox(checkDataLayer, generateScopes.get(ApplicationScope.DataLayer), panel);
		addLabel("Data Layer", panel);
		setCheckBox(checkEntitiesLayer, generateScopes.get(ApplicationScope.EntitiesLayer), panel);
		addLabel("Entities Layer", panel);
		setCheckBox(checkServicesLayer, generateScopes.get(ApplicationScope.ServicesLayer), panel);
		addLabel("Services Layer", panel);
		setCheckBox(checkMVCLayer, generateScopes.get(ApplicationScope.MVCLayer), panel);
		addLabel("MVC Layer", panel);
		
		
		//Output Panel
		panel = new JPanel();
		panel.setBackground(new Color(255, 255, 255));
		layout = new GridLayout(0,1);
		panel.setLayout(layout);
		panel.setPreferredSize(new Dimension(380, 110));
		panel.setBorder(BorderFactory.createTitledBorder("Output")); 
		add(panel);
		txtOutput.setEditable(false);
		txtOutput.setPreferredSize(new Dimension(200, 90));
		panel.add(txtOutput);
		
		//Control Panel
		butGenerate.addActionListener(this);
		butGenerate.setText("Generate");
		//butGenerate.setSize(380, 20);
		butGenerate.setAlignmentX(Component.CENTER_ALIGNMENT);
		add(butGenerate);
	}

	private void setCheckBox(JCheckBox check, Boolean isSelected, JPanel parent) {
		check.setBackground(new Color(255, 255, 255));
		check.setSelected(isSelected);
		check.addActionListener(this);
		parent.add(check);
	}
	
	private void addLabel(String text, JPanel parent) {
		JLabel lbl = new JLabel();
		lbl.setText(text);
		parent.add(lbl);
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		final String actionPaste = (String) TransferHandler.getPasteAction().getValue(Action.NAME);

		if (e.getActionCommand().equals(actionPaste)) {
			Action a = getActionMap().get(actionPaste);
			if (a != null) {
				// a.actionPerformed(new ActionEvent(this.filePanel,
				// ActionEvent.ACTION_PERFORMED, e.getActionCommand()));
				// this.uploadPolicy.afterFileDropped(new
				// DropTargetDropEvent(new DropTarget(
				// this.filePanel.getDropComponent(),
				// this.dndListener).getDropTargetContext(),
				// new Point(), DnDConstants.ACTION_MOVE,
				// DnDConstants.ACTION_COPY_OR_MOVE ));
			}
		} else if (e.getActionCommand() == this.checkDataLayer.getActionCommand()) {
			generateScopes.put(ApplicationScope.DataLayer, checkDataLayer.isSelected());
		
		} else if (e.getActionCommand() == this.checkEntitiesLayer.getActionCommand()) {
			generateScopes.put(ApplicationScope.EntitiesLayer, checkEntitiesLayer.isSelected());

		} else if (e.getActionCommand() == this.checkServicesLayer.getActionCommand()) {
			generateScopes.put(ApplicationScope.ServicesLayer, checkServicesLayer.isSelected());

		} else if (e.getActionCommand() == this.checkMVCLayer.getActionCommand()) {
			generateScopes.put(ApplicationScope.MVCLayer, checkMVCLayer.isSelected());

		} else if (e.getActionCommand() == this.butGenerate.getActionCommand()) {
			try {
				generateCode();
			} catch (Exception e1) {
				txtOutput.setText(e1.getStackTrace().toString());
				txtOutput.updateUI();
				e1.printStackTrace();
			}

		}

	}

	private void generateCode() throws Exception {
		txtOutput.setText("Generating, please wait...");
		txtOutput.updateUI();
		Starter.ResetProduct();
		Starter.getProduct().setGenerationScopes(generateScopes);
		Starter.parseArguments(txtProps.getText().split("\n"));
		Starter.loadProperties();
		Starter.loadApplications();
		Starter.loadBusinessModel();
		Starter.adaptBusinessModel();
		Starter.generateCode();
		txtOutput.setText("code generated successfully!");
		txtOutput.updateUI();
	}

}
