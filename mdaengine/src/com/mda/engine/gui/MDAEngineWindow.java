package com.mda.engine.gui;

import java.applet.Applet;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class MDAEngineWindow extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public static void main(String[] args) {
        //Create and set up the window.
		MDAEngineWindow frame = new MDAEngineWindow();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
	

	public MDAEngineWindow()
	{
		createGUI();
	}
	
	private void createGUI() {
		try {
			// Set System L&F
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (UnsupportedLookAndFeelException e) {
			// handle exception
		} catch (ClassNotFoundException e) {
			// handle exception
		} catch (InstantiationException e) {
			// handle exception
		} catch (IllegalAccessException e) {
			// handle exception
		}

		setLayout(new FlowLayout(FlowLayout.LEFT));
		MainPanel p = new MainPanel();
		this.setSize(new Dimension(400, 400));
		add(p);
		p.setAlignmentX(Component.LEFT_ALIGNMENT);
	}
}
