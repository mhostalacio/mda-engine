package com.mda.engine.utils;

import java.util.ArrayList;
import java.util.regex.Pattern;



public class Stringcode {

	public static String COMMENTS_REGEX = "^\\s*//.*$";
	public static Pattern COMMENTS_PATTERN = Pattern.compile(COMMENTS_REGEX);
	public static boolean WRITE_COMMENTS = true;

	public static long CALL_COUNT = 0L;
	private int lineMaxLength = 2046;

	public int getLineMaxLength() {
		return lineMaxLength;
	}

	public void setLineMaxLength(int lineMaxLength) {
		this.lineMaxLength = lineMaxLength;
	}

	private ArrayList<String> lines = new ArrayList<String>();
	private ArrayList<String> tokens;
	public static String INDENT = "    ";
	private String indentValue = INDENT;

	public String getIndentValue() {
		return indentValue;
	}

	public void setIndentValue(String indentValue) {
		this.indentValue = indentValue;
	}

	public ArrayList<String> getLines() {
		ArrayList<String> returnLines = new ArrayList<String>();
		for (String line : lines) {
			returnLines.add(line);
		}

		// remove the last line if it's empty
		if (returnLines.size() > 0 && returnLines.get(returnLines.size() - 1) == "") {
			returnLines.remove(returnLines.size() - 1);
		}

		return returnLines;
	}

	public void AttachLines(ArrayList<String> newLines) {
		for (String line : newLines) {
			writeLine(line);
		}
	}

	public void line(Boolean condition, String text) {
		if(condition)
			line(text);
	}
	public void line(String text) {

		CALL_COUNT++;

		String toks = "";
		if (tokens != null) {
			for (String t : tokens) {
				toks = toks + t;
			}
		}

		text = toks + text;


			// sql file problem -> must have lines smaller than 2046 characters
			if (text.length() > lineMaxLength) {

				int index = 0;
				int total = text.length();

				while (index < total) {
					int end = -1;
					if ((total - index) > lineMaxLength) {
						end = index + (text.substring(index, index + lineMaxLength)).lastIndexOf(" ");
					}
					if (end == -1) {
						end = total;
					}

					lines.add(text.substring(index, end));
					index = end;
				}
			} else {
				lines.add(text);
			}

	}

	public void write(String text) {

		CALL_COUNT++;

		String toks = "";
		if (tokens != null) {
			for (String t : tokens) {
				toks = toks + t;
			}
		}

		if (lines.isEmpty()) {
			lines.add("");
		}

		int lastItemIndex = lines.size() - 1;
		String unfinishedStr = lines.remove(lastItemIndex);
		if (unfinishedStr == "") {
			unfinishedStr += toks + text;
		} else {
			unfinishedStr += text;
		}

		if ((unfinishedStr.length()) > lineMaxLength) {

			int index = 0;
			int total = unfinishedStr.length();
			while (index < total) {
				int end = -1;
				if ((total - index) > lineMaxLength) {
					end = index + (unfinishedStr.substring(index, index + lineMaxLength)).lastIndexOf(" ");
				}
				if (end == -1) {
					end = total;
				}

				String strPart = toks + unfinishedStr.substring(index, end);


					lines.add(strPart);

				
				index = end;
			}

		} else {


				lines.add(unfinishedStr);

		}
	}

	public void write(String string, Object... pN) {
		this.write(StringUtils.format(string, pN));
	}

	public void writeLine(String text) {
		this.write(text);
		lines.add("");
	}

	public void writeLine(String string, Object... pN) {
		this.write(StringUtils.format(string, pN));
		lines.add("");
	}

	public void writeLine() {
		lines.add("");
	}

	public void line() {
		lines.add("");
	}

	public void line(String string, Object... pN) {

		line(StringUtils.format(string, pN));
	}

	public void indentedLine(String string, Object... pN){

		tokenAdd2BeginOfLineIndent();
		line(StringUtils.format(string, pN));
		tokenRemove2BeginOfLine();
	}

	@Override
	public String toString() {
		StringBuffer toRet = new StringBuffer();
		for (String line : lines) {
			toRet.append(line).append(System.getProperty("line.separator"));
		}
		return toRet.toString();
	}
	
	public String toString(final int startLine, final int endLine) {
		
		StringBuffer toRet = new StringBuffer();
		
		for (int index = startLine; index <= endLine; index++) {
			
			toRet.append(this.lines.get(index)).append(System.getProperty("line.separator"));
		}
		
		return toRet.toString();
	}

	/**
	 * For now on add four spaces to the beginning of each line
	 * 
	 * @param string
	 */
	public void tokenAdd2BeginOfLineIndent() {

		tokenAdd2BeginOfLine(indentValue);
	}


	public void writeStatementWithExpression(String statement, String expression)
	{
		startStatement(statement);
		line(expression);
		endStatement();
	}
	
	public void startStatement(String statement)
	{
		line();
		line(statement);
		line("{");
		tokenAdd2BeginOfLineIndent();
	}
	
	public void endStatement()
	{
		tokenRemove2BeginOfLine();
		line("}");
		line();
	}
	
	public void endStatement(String text)
	{
		tokenRemove2BeginOfLine();
		line("}" + text);
		line();
	}
	
	/**
	 * For now on add the string parameter to the beginning of each line
	 * 
	 * @param string
	 */
	public void tokenAdd2BeginOfLine(String string) {

		if (tokens == null) {
			tokens = new ArrayList<String>();
		}
		tokens.add(string);
	}

	/**
	 * For now on remove the last string added on method Add2BeginOfLine
	 */
	public void tokenRemove2BeginOfLine() {

		tokens.remove(tokens.size() - 1);
	}

	/**
	 * Remove the last character on the last added line.
	 */
	public void removeLastChar() {

		if (!lines.isEmpty()) {
			String lastLine = lines.remove(lines.size() - 1);
			if (lastLine.length() > 0)
				lines.add(lastLine.substring(0, lastLine.length() - 1));
		}
	}

	/**
	 * Remove the last number of characters on the last line.
	 */
	public void removeLastChars(int numberOfChars) {

		if (!lines.isEmpty()) {
			String lastLine = lines.remove(lines.size() - 1);
			Boolean addEmptyLine = false;
			if (lastLine.isEmpty()) {
				lastLine = lines.remove(lines.size() - 1);
				addEmptyLine = true;
			}

			if (lastLine.length() > 0) {
				lines.add(lastLine.substring(0, lastLine.length() - numberOfChars));
			}

			if (addEmptyLine) {
				this.line();
			}
		}
	}

	/**
	 * Remove the last line.
	 */
	public void removeLastLine() {

		if (!lines.isEmpty()) {
			lines.remove(lines.size() - 1);
		}
	}
	
	/**
	 * Clears the lines and tokens buffer.
	 */
	public void clear() {
		lines.clear();
		if (tokens != null)
			tokens.clear();
	}

	/**
	 * Gets the current line count
	 */
	public int getLineCount() {
		return this.lines.size();
	}
}
