package com.mda.engine.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.mda.engine.core.IStringConverter;


public class StringUtils {

	public static String getPascalCase(String name) {
		
		return String.format("%s%s", name.substring(0, 1).toUpperCase(), name.substring(1));
	}
	
	public static String getCamelCase(String name) {
		
		return String.format("%s%s", name.substring(0, 1).toLowerCase(), name.substring(1));
	}
	
	/**
	 * Returns the line separator String for the current OS.
	 */
	public static final String LINE_SEPARATOR = System.getProperty("line.separator");

	/**
	 * Indicates if the supplied String is null or empty.
	 * 
	 * @param str
	 *            String to check.
	 * @return True or false.
	 */
	public static boolean isNullOrEmpty(String str) {

		return (str == null || str.length() == 0);
	}

	/**
	 * Creates a delimited list with the amount of numbers supplied.
	 * 
	 * @param itemCount
	 *            Number of items to create.
	 * @param delimiter
	 *            Delimiter to use between the items.
	 * @return Transformed string.
	 */
	public static String toDelimitedString(int itemCount, String delimiter) {
		
		ArrayList<Integer> items = new ArrayList<Integer>(itemCount);
		
		for (int i = 0; i < itemCount; i++) {
			
			items.add(i);
		}
		
		return toDelimitedString(items, delimiter);
	}
	
	/**
	 * Transforms the supplied list into a String delimited by the supplied
	 * delimiter. It will call the toString() method on each item of the list.
	 * 
	 * @param <T>
	 *            List type.
	 * @param list
	 *            List to transform.
	 * @param delimiter
	 *            Delimiter to use between the items.
	 * @return Transformed string.
	 */
	public static <T> String toDelimitedString(Collection<T> list, String delimiter) {

		return toDelimitedStringImplementation(list, delimiter, null, null);
	}

	/**
	 * Transforms the supplied list into a String delimited by the supplied
	 * delimiter. It will call the toString() method on each item of the list.
	 * 
	 * @param <T>
	 *            List type.
	 * @param list
	 *            List to transform.
	 * @param delimiter
	 *            Delimiter to use between the items.
	 * @param stringConverter
	 *            The IStringConverter instance that will convert the items from
	 *            the supplied list into the proper String representation.
	 * @return Transformed string.
	 */
	public static <T> String toDelimitedString(Collection<T> list, String delimiter, IStringConverter<T> stringConverter) {

		return toDelimitedStringImplementation(list, delimiter, null, stringConverter);
	}

	/**
	 * Transforms the supplied list into a String delimited by the supplied
	 * delimiter. It will call the toString() method on each item of the list.
	 * 
	 * @param <T>
	 *            List type.
	 * @param list
	 *            List to transform.
	 * @param delimiter
	 *            Delimiter to use between the items.
	 * @param lastItemDelimiter
	 *            Delimiter to use before the last item.
	 * @return Transformed string.
	 */
	public static <T> String toDelimitedString(Collection<T> list, String delimiter, String lastItemDelimiter) {

		return toDelimitedStringImplementation(list, delimiter, lastItemDelimiter, null);
	}

	/**
	 * Transforms the supplied list into a String delimited by the supplied
	 * delimiter. It will call the toString() method on each item of the list.
	 * 
	 * @param <T>
	 *            List type.
	 * @param list
	 *            List to transform.
	 * @param delimiter
	 *            Delimiter to use between the items.
	 * @param lastItemDelimiter
	 *            Delimiter to use before the last item.
	 * @param stringConverter
	 *            The IStringConverter instance that will convert the items from
	 *            the supplied list into the proper String representation.
	 * @return Transformed string.
	 */
	public static <T> String toDelimitedString(Collection<T> list, String delimiter, String lastItemDelimiter, IStringConverter<T> stringConverter) {

		return toDelimitedStringImplementation(list, delimiter, lastItemDelimiter, stringConverter);
	}

	/**
	 * Actual @see {@link StringUtils#toDelimitedString(List, String)}
	 * implementation.
	 * 
	 * @param <T>
	 *            List type.
	 * @param list
	 *            List to transform.
	 * @param delimiter
	 *            Delimiter to use between the items.
	 * @param lastItemDelimiter
	 *            Delimiter to use before the last item.
	 * @return Transformed string.
	 */
	private static <T> String toDelimitedStringImplementation(Collection<T> list, String delimiter, String lastItemDelimiter, IStringConverter<T> stringConverter) {

		String delimitedString = null;

		int listSize = list.size();

		if (listSize > 0) {

			StringBuilder sb = new StringBuilder();

			Iterator<T> iterator = list.iterator();
			
			
			// Appends the first item
			sb.append(getItemString(stringConverter, iterator.next()));

			// Checks if it should use a different delimiter on the last item
			if (!StringUtils.isNullOrEmpty(lastItemDelimiter)) {

				int firstDelimiterCount = listSize - 1;

				// Appends all resting items except the last
				for (int i = 1; i < firstDelimiterCount; i++) {

					sb.append(delimiter);
					sb.append(getItemString(stringConverter, iterator.next()));
				}

				if (listSize > 1) {

					// Appends the last item with the proper delimiter
					sb.append(lastItemDelimiter);
					sb.append(getItemString(stringConverter, iterator.next()));
				}

			} else {

				for (int i = 1; i < listSize; i++) {

					sb.append(delimiter);
					sb.append(getItemString(stringConverter, iterator.next()));
				}
			}

			delimitedString = sb.toString();
		}

		return delimitedString;
	}

	/**
	 * Formats the supplied String.
	 * 
	 * @param strToFormat
	 *            The string value to format. This value will have all
	 *            placeholders (Ex: {0}, {1}, {2}...) replaced by the
	 *            corresponding value from the arguments array.
	 * @param args
	 *            Arguments to use as replacement.
	 * @return
	 */
	public static String format(String strToFormat, Object... args) {

		if (strToFormat.length() < 3)
			throw new IllegalArgumentException(String.format("The value from the \"strToFormat\" argument cannot be a valid placeholder!", strToFormat));

		String resultString = null;

		if (args != null && args.length > 0) {

			StringBuffer sb = new StringBuffer(strToFormat.length());

			try {
				
				format(strToFormat, sb, args);
				
			} catch (Exception ex) {
				
				throw new RuntimeException(ex);
			}

			resultString = sb.toString();

		} else {

			resultString = strToFormat;
		}

		return resultString;
	}

	public static void format(String strToFormat, Appendable sb, Object... args)  throws IOException {
		
		int strLength = strToFormat.length();
		
		int maxIdx = strLength - 1;

		for (int i = 0; i < strLength; i++) {

			char ch = strToFormat.charAt(i);

			if (ch == '{' && i < maxIdx) {

				int nextCharOneIdx = i + 1;

				char nextCharOne = strToFormat.charAt(nextCharOneIdx);

				// If the character after the opening brackets its a digit, then it may be a placeholder reference
				if (Character.isDigit(nextCharOne)) {

					int nextCharTwoIdx = i + 2;

					char nextCharTwo = strToFormat.charAt(nextCharTwoIdx);

					// If the character after the first digit after the opening brackets its a closing bracket, then it is a placeholder reference
					if (nextCharTwo == '}') {

						int referencedArgIdx = Integer.parseInt(Character.toString(nextCharOne));

						appendArgument(sb, referencedArgIdx, args);

						// Advances the counter forward so it does not
						// process the number and the closing bracket
						i = nextCharTwoIdx;

					} else {

						boolean areAllDigits = false;
						boolean foundEnd = false;
						int currentCharIdx = nextCharTwoIdx + 1;

						// If the subsequent characters after the first digit are digits, then it may be a placeholder reference
						if (Character.isDigit(nextCharTwo)) {

							areAllDigits = true;
							foundEnd = false;

							while (currentCharIdx < strToFormat.length() && areAllDigits && !foundEnd) {

								char currentChar = strToFormat.charAt(currentCharIdx);

								// If the currentChar its a closing bracket and all characters between the opening bracket and the closing bracket are digits, then its a placeholder reference
								if (currentChar == '}') {
								
									foundEnd = true;
									
								} else {
								
									areAllDigits = Character.isDigit(currentChar);
									currentCharIdx++;
								}
							}
						}

						if (areAllDigits && foundEnd) {

							int argIdx = Integer.parseInt(strToFormat.substring(nextCharOneIdx, currentCharIdx));
							
							appendArgument(sb, argIdx, args);
							
							// Advances the counter forward so it does not
							// process the numbers and the closing bracket
							i = currentCharIdx;
							
						} else {
							
							// Appends the opening bracket and the number
							sb.append(ch);
							sb.append(nextCharOne);

							i = nextCharOneIdx;
						}
					}

				} else {

					sb.append(ch);
				}

			} else {

				sb.append(ch);
			}
		}
	}

	private static void appendArgument(Appendable sb, int referencedArgIdx, Object... args) throws IOException {
		if (referencedArgIdx >= args.length)
			throw new IndexOutOfBoundsException(String.format(
					"Cannot retrieve the value from argument at index \"%s\" because its length its only \"%s\"!", referencedArgIdx,
					args.length));

		// Gets the corresponding argument
		Object arg = args[referencedArgIdx];

		if (arg != null) {

			// Appends the argument value
			sb.append(arg.toString());
		}
	}

	private static <T> Object getItemString(IStringConverter<T> stringConverter, T item) {
		return (stringConverter == null) ? item : stringConverter.getString(item);
	}
	
	public static String replaceLast(String string, String from, String to) {
	     int lastIndex = string.lastIndexOf(from);
	     if (lastIndex < 0) return string;
	     String tail = string.substring(lastIndex).replaceFirst(from, to);
	     return string.substring(0, lastIndex) + tail;
	}
	

}
