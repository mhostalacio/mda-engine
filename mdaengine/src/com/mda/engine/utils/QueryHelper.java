package com.mda.engine.utils;


import org.apache.commons.lang.NotImplementedException;

import com.mda.engine.core.Product;
import com.mda.engine.models.businessObjectModel.AggregationAttributeType;
import com.mda.engine.models.businessObjectModel.AttributeNumberDataType;
import com.mda.engine.models.businessObjectModel.BaseAttributeType;
import com.mda.engine.models.businessObjectModel.BooleanAttributeType;
import com.mda.engine.models.businessObjectModel.DatetimeAttributeType;
import com.mda.engine.models.businessObjectModel.EnumAttributeType;
import com.mda.engine.models.businessObjectModel.GenerateQueryType;
import com.mda.engine.models.businessObjectModel.GuidAttributeType;
import com.mda.engine.models.businessObjectModel.IndentityAttributeType;
import com.mda.engine.models.businessObjectModel.IndexAttribute;
import com.mda.engine.models.businessObjectModel.NumberAttributeType;
import com.mda.engine.models.businessObjectModel.ObjectType;
import com.mda.engine.models.businessObjectModel.TextAttributeType;
import com.mda.engine.models.businessObjectModel.XmlAttributeType;
import com.mda.engine.models.common.BOMReferenceType;
import com.mda.engine.models.configurationManagementModel.Application;
import com.mda.engine.models.dataModel.BigIntTableColumn;
import com.mda.engine.models.dataModel.BinaryTableColumn;
import com.mda.engine.models.dataModel.BitTableColumn;
import com.mda.engine.models.dataModel.ColumnCollection;
import com.mda.engine.models.dataModel.DataBaseItem;
import com.mda.engine.models.dataModel.DateTimeTableColumn;
import com.mda.engine.models.dataModel.DecimalTableColumn;
import com.mda.engine.models.dataModel.DoubleTableColumn;
import com.mda.engine.models.dataModel.EnumTableColumn;
import com.mda.engine.models.dataModel.FloatTableColumn;
import com.mda.engine.models.dataModel.ForeignKey;
import com.mda.engine.models.dataModel.ForeignKeyTableReference;
import com.mda.engine.models.dataModel.IntTableColumn;
import com.mda.engine.models.dataModel.NvarcharTableColumn;
import com.mda.engine.models.dataModel.SmallIntTableColumn;
import com.mda.engine.models.dataModel.TableColumn;
import com.mda.engine.models.dataModel.TinyIntTableColumn;
import com.mda.engine.models.dataModel.UniqueIdentifierTableColumn;
import com.mda.engine.models.dataModel.VarcharTableColumn;
import com.mda.engine.models.dataModel.XMLTableColumn;
import com.mda.engine.models.expressionsModel.ExpressionChoice;
import com.mda.engine.models.expressionsModel.QueryParameterExpression;
import com.mda.engine.models.expressionsModel.TableColumnExpression;
import com.mda.engine.models.queryModel.BigIntList;
import com.mda.engine.models.queryModel.BigIntPairList;
import com.mda.engine.models.queryModel.FromClauseChoice;
import com.mda.engine.models.queryModel.ParameterCollection;
import com.mda.engine.models.queryModel.Query;
import com.mda.engine.models.queryModel.QueryDefinitionCollection;
import com.mda.engine.models.queryModel.QueryDefinitionSelect;
import com.mda.engine.models.queryModel.QueryParameters;
import com.mda.engine.models.queryModel.QueryResultSet;
import com.mda.engine.models.queryModel.WhereClause;

public class QueryHelper {
	
	public static final String GET_BY_ID_QUERY_NAME = "GetById";
	public static final String GET_ALL_QUERY_NAME = "GetAll";
	public static final String AUDIT_FIELD_NAME_CREATE_AUTHOR = "CreateAuthor";
	public static final String AUDIT_FIELD_NAME_VERSION_AUTHOR = "VersionAuthor";
	public static final String AUDIT_FIELD_NAME_CREATE_DATE = "CreateDate";
	public static final String AUDIT_FIELD_NAME_VERSION_DATE = "VersionDate";
	public static final String AUDIT_FIELD_NAME_VERSION_NUMBER = "VersionNumber";
	public static final String AUDIT_FIELD_NAME_PERSIST_ORDER = "PersistOrder";
	public static final String AUDIT_FIELD_DEFAULT_LANGUAGE = "DefaultLanguage";
	
	public static BigIntTableColumn createLongParameter(String name, boolean isNullable) {
		BigIntTableColumn prm = new BigIntTableColumn();
		prm.setName(name);
		prm.setIsNullable(isNullable);
		return prm;
    }
	
	
	public static Query createGetByIdQuery(Application app, ObjectType bom, GenerateQueryType config) {

		Query getByIdQuery = new Query();
		getByIdQuery.setOwner(bom);
		getByIdQuery.setOwnedBy(new BOMReferenceType());
		getByIdQuery.getOwnedBy().setName(bom.getName());
		getByIdQuery.setName(QueryHelper.GET_BY_ID_QUERY_NAME);
		getByIdQuery.setUseNolockHint(true);
		getByIdQuery.setResultSet(QueryResultSet.SINGLE);
		getByIdQuery.setDescription(String.format("Return a single instance of the \"%s\" class with the supplied Id.", bom.getName()));
		getByIdQuery.setParameters(new QueryParameters());
		getByIdQuery.getParameters().setDefinition(new ParameterCollection());
		
		getByIdQuery.setDefinition(new QueryDefinitionCollection());
		QueryDefinitionSelect select = new QueryDefinitionSelect();
		getByIdQuery.getDefinition().setSelect(select);
		select.setFrom(new FromClauseChoice());
		select.getFrom().setAlias("Ent");
		select.getFrom().setTable(new BOMReferenceType());
		select.getFrom().getTable().setName(bom.getName());
		WhereClause where = new WhereClause();
		getByIdQuery.getDefinition().getSelect().setWhere(where);
		
		
		if (bom.getIdentity().getMode().equals("Automatic")) 
		{
			//parameter
			getByIdQuery.getParameters().getDefinition().getColumnList().add(QueryHelper.createLongParameter("Id", false));
			//where
			com.mda.engine.models.expressionsModel.EqualsExpression equals = new com.mda.engine.models.expressionsModel.EqualsExpression();
			where.getExpressions().add(equals);
			TableColumnExpression colExp = new TableColumnExpression();
			colExp.setTableAlias("Ent");
			colExp.setColumnName("Id");
			equals.setFirstArg(new ExpressionChoice());
			equals.getFirstArg().setExpression(colExp);
			QueryParameterExpression paramExp = new QueryParameterExpression();
			paramExp.setParameterName("Id");
			equals.setSecondArg(new ExpressionChoice());
			equals.getSecondArg().setExpression(paramExp);
		} 
		else if (bom.getIdentity().getMode().equals("Manual")) {
			for(IndexAttribute att : bom.getPrimaryKey().getAttributes().getIndexAttribute())
			{
				//TODO
			}
		} else {

			throw new NotImplementedException("Attribute Identity Mode not implemented.");
		}
		
		
		return getByIdQuery;
	}
	
	
	public static Query createGetAllQuery(Application app, ObjectType bom, GenerateQueryType config) {
		Query getAllQuery = new Query();
		getAllQuery.setOwner(bom);
		getAllQuery.setOwnedBy(new BOMReferenceType());
		getAllQuery.getOwnedBy().setName(bom.getName());
		getAllQuery.setName(QueryHelper.GET_ALL_QUERY_NAME);
		getAllQuery.setUseNolockHint(true);
		getAllQuery.setResultSet(QueryResultSet.LIST);
		getAllQuery.setDescription(String.format("Return all instances of the \"%s\" class.", bom.getName()));
		getAllQuery.setParameters(new QueryParameters());
		getAllQuery.getParameters().setDefinition(new ParameterCollection());
		getAllQuery.setDefinition(new QueryDefinitionCollection());
		QueryDefinitionSelect select = new QueryDefinitionSelect();
		getAllQuery.getDefinition().setSelect(select);
		select.setFrom(new FromClauseChoice());
		select.getFrom().setAlias("Ent");
		select.getFrom().setTable(new BOMReferenceType());
		select.getFrom().getTable().setName(bom.getName());
		WhereClause where = new WhereClause();
		getAllQuery.getDefinition().getSelect().setWhere(where);
		return getAllQuery;
	}

	
	public static String getSQLType(DataBaseItem column) {
		String retType = null;

		if (column instanceof BigIntTableColumn)
			retType = "BIGINT";
		else if (column instanceof DateTimeTableColumn)
			retType = "DATETIME2";
		else if (column instanceof FloatTableColumn)
			retType = "FLOAT";
		else if (column instanceof DoubleTableColumn)
			retType = "FLOAT";
		else if (column instanceof UniqueIdentifierTableColumn)
			retType = "UNIQUEIDENTIFIER";
		else if (column instanceof IntTableColumn)
			retType = "INTEGER";
		else if (column instanceof SmallIntTableColumn)
			retType = "SMALLINT";
		else if (column instanceof NvarcharTableColumn)
		{
			NvarcharTableColumn col = (NvarcharTableColumn)column;
			if (col.getMaxSize() != null)
			{
				retType = String.format("NVARCHAR(%s)", col.getMaxSize());
			}
			else
			{
				retType = "NVARCHAR(MAX)";
			}
		}
		else if (column instanceof VarcharTableColumn)
		{
			VarcharTableColumn col = (VarcharTableColumn)column;
			if (col.getMaxSize() != null)
			{
				retType = String.format("VARCHAR(%s)", col.getMaxSize());
			}
			else
			{
				retType = "VARCHAR(MAX)";
			}
		}
		else if (column instanceof TinyIntTableColumn)
			retType = "TINYINT";
		else if (column instanceof XMLTableColumn)
			retType = "XML";
		else if (column instanceof BinaryTableColumn)
		{
			BinaryTableColumn col = (BinaryTableColumn)column;
			if (col.getMaxSize() != null)
			{
				retType = String.format("VARBINARY(%s)", col.getMaxSize());
			}
			else
			{
				retType = "VARBINARY(MAX)";
			}
		}
		else if (column instanceof BitTableColumn)
			retType = "BIT";
		else if (column instanceof BigIntList)
			retType = "AggregationList_Table READONLY";
		else if (column instanceof BigIntPairList)
			retType = "BigIntPairList_Table READONLY";
		else if (column instanceof DecimalTableColumn)
		{
			DecimalTableColumn decCol =(DecimalTableColumn)column;
			retType = String.format("DECIMAL(%s,%s)", decCol.getNumberOfIntegerPlaces(), decCol.getNumberOfDecimalPlaces());
			
		}else if(column instanceof EnumTableColumn){
			retType = "TINYINT";
		}
		else {
			throw new RuntimeException("ColumnBase Type not recognized ->"
					+ column.getClass().getCanonicalName() + "\n Column name= "
					+ column.getName());
		}
		return retType;
	}
	
	public static String getCode2SQLType(DataBaseItem column) {
		String retType = null;

		if (column instanceof BigIntTableColumn)
			retType = "BigInt";
		else if (column instanceof DateTimeTableColumn)
			retType = "DateTime2";
		else if (column instanceof FloatTableColumn)
			retType = "Float";
		else if (column instanceof DoubleTableColumn)
			retType = "Float";
		else if (column instanceof UniqueIdentifierTableColumn)
			retType = "UniqueIdentifier";
		else if (column instanceof IntTableColumn)
			retType = "Int";
		else if (column instanceof SmallIntTableColumn)
			retType = "SmallInt";
		else if (column instanceof NvarcharTableColumn)
		{
			retType = "NVarChar";
		}
		else if (column instanceof VarcharTableColumn)
		{
			retType = "VarChar";
		}
		else if (column instanceof TinyIntTableColumn)
			retType = "TinyInt";
		else if (column instanceof XMLTableColumn)
			retType = "Xml";
		else if (column instanceof BitTableColumn)
			retType = "Bit";
		else if (column instanceof BinaryTableColumn)
			retType = "Binary";
		else if (column instanceof DecimalTableColumn){
			retType = "Decimal";
		} else if (column instanceof EnumTableColumn){
			retType = "Byte";
		}else {
			throw new RuntimeException("ColumnBase Type not recognized ->"
					+ column.getClass().getCanonicalName() + "\n Column name= "
					+ column.getName());
		}
		return retType;
	}
	
	public static String getCode2SQLType2Set(DataBaseItem column) {
		String retType = null;

		if (column instanceof BigIntTableColumn)
			retType = "Int64";
		else if (column instanceof DateTimeTableColumn)
			retType = "DateTime";
		else if (column instanceof FloatTableColumn)
			retType = "Float";
		else if (column instanceof DoubleTableColumn)
			retType = "Double";
		else if (column instanceof UniqueIdentifierTableColumn)
			retType = "Guid";
		else if (column instanceof IntTableColumn)
			retType = "Int32";
		else if (column instanceof SmallIntTableColumn)
			retType = "Int16";
		else if (column instanceof NvarcharTableColumn)
		{
			retType = "String";
		}
		else if (column instanceof VarcharTableColumn)
		{
			retType = "String";
		}
		else if (column instanceof TinyIntTableColumn)
			retType = "Byte";
		else if (column instanceof XMLTableColumn)
			retType = "Xml";
		else if (column instanceof BitTableColumn)
			retType = "Boolean";
		else if (column instanceof BinaryTableColumn)
			retType = "Binary";
		else if (column instanceof DecimalTableColumn)
		{
			retType = "Decimal";
		} else if (column instanceof EnumTableColumn){
			retType = "Byte";
		}else {
			throw new RuntimeException("ColumnBase Type not recognized ->"
					+ column.getClass().getCanonicalName() + "\n Column name= "
					+ column.getName());
		}
		return retType;
	}
	
	public static BaseAttributeType createAttributeFromDataBaseItem(DataBaseItem item, Product product, Application app)
	{
		BaseAttributeType att = null;
		if (item.getName().equals("Id"))
		{
			att = new IndentityAttributeType();
			((IndentityAttributeType)att).setMode("Automatic");
		}
		else if (item instanceof BigIntTableColumn)
		{
			att = new NumberAttributeType();
			((NumberAttributeType)att).setDataType(AttributeNumberDataType.LONG);
		}
		else if (item instanceof DateTimeTableColumn)
		{
			att = new DatetimeAttributeType();
		}
		else if (item instanceof FloatTableColumn)
		{
			att = new NumberAttributeType();
			((NumberAttributeType)att).setDataType(AttributeNumberDataType.FLOAT);
		}
		else if (item instanceof UniqueIdentifierTableColumn)
		{
			att = new GuidAttributeType();
		}
		else if (item instanceof IntTableColumn)
		{
			att = new NumberAttributeType();
			((NumberAttributeType)att).setDataType(AttributeNumberDataType.INTEGER);
		}
		else if (item instanceof com.mda.engine.models.queryModel.BigIntList)
		{
			att = new AggregationAttributeType();
			att.setPersistable(false);
			((AggregationAttributeType)att).setIsQueryParameter(true);
			((AggregationAttributeType)att).setMultiplicity("0..*");
		}
		else if (item instanceof com.mda.engine.models.queryModel.BigIntPairList)
		{
			att = new AggregationAttributeType();
			att.setPersistable(false);
			ObjectType bigInt = product.getBOMByApplicationAndName(app, Product.BIG_INT_PAIR_BOM_NAME);
			((AggregationAttributeType)att).setTargetObject(bigInt);
			((AggregationAttributeType)att).setIsQueryParameter(true);
			((AggregationAttributeType)att).setMultiplicity("0..*");
		}
		else if (item instanceof SmallIntTableColumn)
		{
			att = new NumberAttributeType();
			((NumberAttributeType)att).setDataType(AttributeNumberDataType.SHORT);
		}
		else if (item instanceof NvarcharTableColumn)
		{
			att = new TextAttributeType();
			((TextAttributeType)att).setUseNVARCHAR(true);
		}
		else if (item instanceof VarcharTableColumn)
		{
			att = new TextAttributeType();
			((TextAttributeType)att).setUseNVARCHAR(false);
		}
		else if (item instanceof TinyIntTableColumn)
		{
			att = new NumberAttributeType();
			((NumberAttributeType)att).setDataType(AttributeNumberDataType.INTEGER);
		}
		else if (item instanceof XMLTableColumn)
		{
			att = new XmlAttributeType();
		}
		else if (item instanceof BitTableColumn)
		{
			att = new BooleanAttributeType();
		}
		else if (item instanceof DecimalTableColumn)
		{
			att = new NumberAttributeType();
			((NumberAttributeType)att).setDataType(AttributeNumberDataType.DECIMAL);
		}
		else if (item instanceof EnumTableColumn)
		{
			EnumTableColumn column;
			EnumAttributeType enumAtt;
			
			column = (EnumTableColumn)item;
			enumAtt = new EnumAttributeType();
			enumAtt.setLovName(column.getEnumName());
			
			att = enumAtt;
		}
		else {
			throw new RuntimeException("ColumnBase Type not recognized ->"
					+ item.getClass().getCanonicalName() + "\n Column name= "
					+ item.getName());
		}
		att.setName(item.getName());
		att.setIsNullable(item.isNullable());
		return att;
	}
	
	public static String getDBType(DataBaseItem column) {
		String retType = null;

		if (column instanceof BigIntTableColumn)
			retType = "Int64";
		else if (column instanceof DateTimeTableColumn)
			retType = "DateTime2";
		else if (column instanceof FloatTableColumn)
			retType = "Int64";
		else if (column instanceof DoubleTableColumn)
			retType = "Double";
		else if (column instanceof UniqueIdentifierTableColumn)
			retType = "Guid";
		else if (column instanceof IntTableColumn)
			retType = "Int32";
		else if (column instanceof SmallIntTableColumn)
			retType = "Int32";
		else if (column instanceof NvarcharTableColumn)
		{
			retType = "String";
		}
		else if (column instanceof VarcharTableColumn)
		{
			retType = "String";
		}
		else if (column instanceof TinyIntTableColumn)
			retType = "Byte";
		else if (column instanceof XMLTableColumn)
			retType = "Xml";
		else if (column instanceof BitTableColumn)
			retType = "Boolean";
		else if (column instanceof BinaryTableColumn)
			retType = "Binary";
		else if (column instanceof DecimalTableColumn){
			retType = "Decimal";
		}else if (column instanceof BigIntList || column instanceof BigIntPairList){
			retType = "Object";
		} else if(column instanceof EnumTableColumn){
			retType = "Byte";
		} else {
			throw new RuntimeException("ColumnBase Type not recognized ->"
					+ column.getClass().getCanonicalName() + "\n Column name= "
					+ column.getName());
		}
		return retType;
	}
	
	public static boolean isParameter4InsertTableType(TableColumn column) {
		
		return (!column.getName().equalsIgnoreCase("Id") && !column.getAuditColumn()) || column.getName().equalsIgnoreCase(AUDIT_FIELD_NAME_PERSIST_ORDER) || column.getName().equals(QueryHelper.AUDIT_FIELD_DEFAULT_LANGUAGE);
	}
	
	public static boolean isParameter4InsertQuery(TableColumn column) {
		
		return (!column.getName().equalsIgnoreCase("Id") && !column.getName().equalsIgnoreCase(AUDIT_FIELD_NAME_VERSION_AUTHOR) && !column.getName().equalsIgnoreCase(AUDIT_FIELD_NAME_VERSION_DATE) && !column.getName().equalsIgnoreCase(AUDIT_FIELD_NAME_VERSION_NUMBER));
	}
	
	public static boolean isParameter4UpdateQuery(TableColumn column) {
		
		return (!column.getName().equalsIgnoreCase(AUDIT_FIELD_NAME_CREATE_AUTHOR) && !column.getName().equalsIgnoreCase(AUDIT_FIELD_NAME_CREATE_DATE)  && !column.getName().equalsIgnoreCase(AUDIT_FIELD_NAME_PERSIST_ORDER));
	}

	public static boolean isParameter4UpdateTableType(TableColumn column) {
		
		return (!column.getAuditColumn()) || column.getName().equalsIgnoreCase(AUDIT_FIELD_NAME_VERSION_NUMBER) || column.getName().equals(QueryHelper.AUDIT_FIELD_DEFAULT_LANGUAGE);
	}
	
	public static boolean isAuditParameter4Update(TableColumn column) {
		
		return (column.getAuditColumn() && !column.getName().equalsIgnoreCase(AUDIT_FIELD_NAME_CREATE_DATE) && !column.getName().equalsIgnoreCase(AUDIT_FIELD_NAME_CREATE_AUTHOR)&& !column.getName().equalsIgnoreCase(AUDIT_FIELD_NAME_PERSIST_ORDER));
	}

	public static String getInsertValue(TableColumn column) {
		if (column.getName().equals(AUDIT_FIELD_NAME_VERSION_NUMBER))
			return "1";
		else if (column.getName().equals(AUDIT_FIELD_NAME_VERSION_AUTHOR))
			return "@CreateAuthor";
		else if (column.getName().equals(AUDIT_FIELD_NAME_VERSION_DATE))
			return "@CreateDate";
		else 
			return "@" + column.getName();
	}
	
	public static String getUpdateValue(TableColumn column) {
		if (column.getName().equals(AUDIT_FIELD_NAME_VERSION_NUMBER))
			return "[" + column.getName() + "] + 1" ;
		else 
			return "@" + column.getName();
	}
	
	public static ForeignKey createForeignKey(String originTableName, String targetTableName, String originColumnName, String targetColumnName) {

		TableColumn col = null;
		ForeignKey foreignKey = new ForeignKey();
		
		ForeignKeyTableReference originTable = new ForeignKeyTableReference();
		originTable.setTableName(originTableName);
		originTable.setColumns(new ColumnCollection());
		col = new TableColumn();
		col.setName(originColumnName);
		originTable.getColumns().getColumnList().add(col);
		foreignKey.setOrigin(originTable);
		
		ForeignKeyTableReference targetTable = new ForeignKeyTableReference();
		targetTable.setTableName(targetTableName);
		targetTable.setColumns(new ColumnCollection());
		col = new TableColumn();
		col.setName(targetColumnName);
		targetTable.getColumns().getColumnList().add(col);
		foreignKey.setTarget(targetTable);

		if (originTableName.length() > 60)
		{
			originTableName = originTableName.substring(0, 60);
		}
		if (originColumnName.length() > 60)
		{
			originColumnName = originColumnName.substring(0, 60);
		}
		if (targetTableName.length() > 60)
		{
			targetTableName = targetTableName.substring(0, 60);
		}
		if (targetColumnName.length() > 60)
		{
			targetColumnName = targetColumnName.substring(0, 60);
		}
		
		foreignKey.setName("FK_" + originTableName + "_" + originColumnName + "_" + targetTableName + "_" + targetColumnName);
		
		return foreignKey;
	}
}
