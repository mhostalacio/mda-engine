package com.mda.engine.utils;

import java.util.Comparator;

import com.mda.engine.models.dataModel.TableColumn;

public class TableColumnComparator implements Comparator<TableColumn> {
	
	public int compare(TableColumn att1, TableColumn att2)
	{
		return att1.getName().compareTo(att2.getName());
	}

}
