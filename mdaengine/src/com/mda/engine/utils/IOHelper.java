package com.mda.engine.utils;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.regex.Pattern;

import org.apache.commons.lang.NullArgumentException;



public class IOHelper {

	/**
	 * This function will recursivly delete directories and files.
	 * 
	 * @param path
	 *            File or Directory to be deleted
	 * @return true indicates success.
	 */
	public static boolean deleteFile(File path) {

		boolean allFilesDelete = true;

		if (path.exists()) {

			if (path.isDirectory()) {

				File[] files = path.listFiles();
				for (int i = 0; i < files.length; i++) {

					if (files[i].isDirectory()) {
						deleteFile(files[i]);
					} else {
						if (!files[i].delete())
							allFilesDelete = false;
					}
				}
			}
		}
		return (path.delete() || allFilesDelete);
	}

	public static File[] listFiles(File baseDir, FilenameFilter filter, boolean recursive) {

		return listFiles(baseDir, filter, null, recursive);
	}

	public static File[] listFiles(File baseDir, FilenameFilter filter, FilenameFilter dirFilter, boolean recursive) {

		if (baseDir == null)
			throw new NullArgumentException("baseDir");

		if (filter == null)
			throw new NullArgumentException("filter");

		if (!baseDir.isDirectory())
			throw new IllegalArgumentException(String.format("Base dir \"%s\" is not a directory", baseDir.getAbsolutePath()));

		ArrayList<File> allFiles = new ArrayList<File>();

		applyFilter(baseDir, filter, dirFilter, allFiles, recursive);

		return allFiles.toArray(new File[allFiles.size()]);
	}

	private static void applyFilter(File baseDir, FilenameFilter filter, FilenameFilter dirFilter, ArrayList<File> allFiles, boolean recursive) {

		File[] filesInDir = baseDir.listFiles();

		for (int i = 0; i < filesInDir.length; i++) {

			File file = filesInDir[i];

			if (filter.accept(file, file.getName()))
				allFiles.add(file);

			if (recursive && file.isDirectory() && (dirFilter == null || dirFilter.accept(file, file.getName())))
				applyFilter(file, filter, dirFilter, allFiles, recursive);
		}
	}

	public static void copyfile(String srFile, String dtFile) throws IOException {

		File f1 = new File(srFile);
		File f2 = new File(dtFile);

		InputStream in = null;
		OutputStream out = null;

		try {

			in = new FileInputStream(f1);
			out = new FileOutputStream(f2);

			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}

			in.close();
			out.close();

		} finally {

			if (in != null)
				in.close();

			if (out != null)
				out.close();
		}
	}

	/**
	 * Returns the resulting path from the combination of the supplied basePath
	 * and the supplied relativePath, if the later one its not absolute already.
	 * If it is, it will return itself.
	 * 
	 * @param basePath
	 *            Base path.
	 * @param relativePath
	 *            Relative path.
	 * @return The combined path or the relativePath if the later its already
	 *         absolute.
	 */
	public static String getAbsolutePath(String basePath, String relativePath) {

		String path = null;

		File relativeFile = new File(relativePath);

		if (relativeFile.isAbsolute()) {

			path = relativePath;

		} else {

			path = combine(basePath, relativePath);
		}

		return path;
	}

	public static String combine(String... paths) {

		int pathsLength = paths.length;

		if (pathsLength == 1)
			throw new IllegalArgumentException(String.format("You must supply more than one path to combine!"));

		StringBuilder sb = new StringBuilder();

		int maxIdx = pathsLength - 1;

		for (int i = 0; i < maxIdx; i++) {

			appendPath(sb, paths[i], i);

			sb.append(File.separator);
		}

		appendPath(sb, paths[maxIdx], maxIdx);

		return sb.toString();
	}

	public static void appendPath(StringBuilder sb, String path, int idx) {

		if (path == null)
			throw new IllegalArgumentException(String.format("The path argument at index \"%s\" its null!", idx));

		File pathFile = new File(path);

		if (idx > 0 && pathFile.isAbsolute()) {

			throw new IllegalArgumentException(String.format(
					"The path argument at index \"%s\" its absolute! You cannot supply an absolute path after the first index!", idx));
		}

		boolean hasBeginSeparator = path.startsWith(File.separator);
		boolean hasEndSeparator = path.endsWith(File.separator);

		if (hasBeginSeparator && hasEndSeparator)
			sb.append(path.substring(1, path.length() - 1));
		else if (hasBeginSeparator)
			sb.append(path.substring(1));
		else if (hasEndSeparator)
			sb.append(path.substring(0, path.length() - 1));
		else
			sb.append(path);
	}

	public static void ensureDirectoryExists(File directory) {

		if (!directory.exists()) {

			directory.mkdirs();
		}
	}

	public static void ensureFileDirectoriesExists(File fileEntity) {

		if (!fileEntity.exists()) {

			File parent = new File(fileEntity.getParent());

			// Creates directory
			if (!parent.exists())
				parent.mkdirs();
		}
	}

	/**
	 * Creates a file filter that ignores .svn directories.
	 * 
	 * @return FilenameFilter instance.
	 */
	public static FilenameFilter getSvnDirectoryFilter() {
		return new FilenameFilter() {

			public boolean accept(File f, String s) {
				return !f.getName().equals(".svn");
			}
		};
	}

	public static void writeToFile(String code, File fileEntity, boolean append) throws UnsupportedEncodingException, FileNotFoundException, IOException {

		ensureFileDirectoriesExists(fileEntity);

		// // Writer output = new BufferedWriter(new
		// // FileWriter(fileEntity));
		// BufferedWriter output = null;
		// FileOutputStream fstream = null;
		// OutputStreamWriter oStreamWriter = null;
		// // FileWriter always assumes default encoding!
		//
		// try {
		// fstream = new FileOutputStream(fileEntity, append);
		// oStreamWriter = new OutputStreamWriter(fstream, "UTF8");
		// output = new BufferedWriter(oStreamWriter);
		// output.write(code);
		// } finally {
		//
		// if (output != null)
		// output.close();
		// if (oStreamWriter != null)
		// oStreamWriter.close();
		// if (fstream != null)
		// fstream.close();
		//
		// }

		FileChannel wChannel = null;

		try {

			ByteBuffer buf = ByteBuffer.wrap(code.getBytes());

			// Create a writable file channel
			wChannel = new FileOutputStream(fileEntity, append).getChannel();

			// Write the ByteBuffer contents; the bytes between the ByteBuffer's
			// position and the limit is written to the file
			wChannel.write(buf);

		} finally {

			if (wChannel != null) {

				// Close the file
				wChannel.close();
			}
		}
	}

	public static void writeToFile(String code, File fileEntity) throws UnsupportedEncodingException, FileNotFoundException, IOException {

		writeToFile(code, fileEntity, false);
	}

	public static byte[] createChecksum(File file) throws Exception {

		InputStream fis = new FileInputStream(file);

		return createChecksum(fis);
	}

	public static byte[] createChecksum(byte[] data) throws NoSuchAlgorithmException, IOException {

		MessageDigest complete = MessageDigest.getInstance("MD5");

		complete.update(data);

		return complete.digest();
	}

	public static byte[] createChecksum(InputStream input) throws NoSuchAlgorithmException, IOException {

		byte[] buffer = new byte[1024];

		MessageDigest complete = MessageDigest.getInstance("MD5");

		int numRead;

		do {
			numRead = input.read(buffer);
			if (numRead > 0) {
				complete.update(buffer, 0, numRead);
			}
		} while (numRead != -1);

		input.close();

		return complete.digest();
	}

	static final String HEXES = "0123456789ABCDEF";

	public static String getHex(byte[] raw) {
		if (raw == null) {
			return null;
		}
		final StringBuilder hex = new StringBuilder(2 * raw.length);
		for (final byte b : raw) {
			hex.append(HEXES.charAt((b & 0xF0) >> 4)).append(HEXES.charAt((b & 0x0F)));
		}
		return hex.toString();
	}

	public static Object deserialize(File file) throws Exception {

		return deserialize(new BufferedInputStream(new FileInputStream(file)));
	}

	public static Object deserialize(InputStream stream) throws Exception {

		Object obj = null;

		java.io.ObjectInputStream objOut = null;

		try {

			objOut = new java.io.ObjectInputStream(stream);

			obj = objOut.readObject();
		} finally {

			if (objOut != null)
				objOut.close();

			if (stream != null)
				stream.close();
		}

		return obj;
	}

	public static void serialize(Serializable obj, File file) throws Exception {

		java.io.FileOutputStream stream = null;
		java.io.ObjectOutputStream objOut = null;
		java.io.BufferedOutputStream buf = null;

		try {

			stream = new java.io.FileOutputStream(file);
			buf = new BufferedOutputStream(stream);
			objOut = new java.io.ObjectOutputStream(buf);

			objOut.writeObject(obj);

			objOut.flush();
		} finally {

			if (objOut != null)
				objOut.close();

			if (buf != null)
				buf.close();

			if (stream != null)
				stream.close();
		}
	}

	public static byte[] serialize(Serializable obj) throws Exception {

		java.io.ByteArrayOutputStream stream = null;
		java.io.ObjectOutputStream objOut = null;
		java.io.BufferedOutputStream buf = null;

		try {

			stream = new java.io.ByteArrayOutputStream();
			buf = new BufferedOutputStream(stream);
			objOut = new java.io.ObjectOutputStream(buf);

			objOut.writeObject(obj);

			objOut.flush();

			return stream.toByteArray();
		} finally {

			if (objOut != null)
				objOut.close();

			if (buf != null)
				buf.close();

			if (stream != null)
				stream.close();
		}
	}

	public static String getRelativePath(String targetPath, String basePath) {

		String pathSeparator = File.separator;

		// We need the -1 argument to split to make sure we get a trailing
		// "" token if the base ends in the path separator and is therefore
		// a directory. We require directory paths to end in the path
		// separator -- otherwise they are indistinguishable from files.
		String[] base = basePath.split(Pattern.quote(pathSeparator), -1);
		String[] target = targetPath.split(Pattern.quote(pathSeparator), 0);

		// First get all the common elements. Store them as a string,
		// and also count how many of them there are.
		String common = "";
		int commonIndex = 0;
		for (int i = 0; i < target.length && i < base.length; i++) {
			if (target[i].equals(base[i])) {
				common += target[i] + pathSeparator;
				commonIndex++;
			} else
				break;
		}

		if (commonIndex == 0) {
			// Whoops -- not even a single common path element. This most
			// likely indicates differing drive letters, like C: and D:.
			// These paths cannot be relativized. Return the target path.
			return targetPath;
			// This should never happen when all absolute paths
			// begin with / as in *nix.
		}

		String relative = "";
		if (base.length == commonIndex) {
			// Comment this out if you prefer that a relative path not start
			// with ./
			// relative = "." + pathSeparator;
		} else {
			int numDirsUp = base.length - commonIndex - 1;
			// The number of directories we have to backtrack is the length of
			// the base path MINUS the number of common path elements, minus
			// one because the last element in the path isn't a directory.
			for (int i = 1; i <= (numDirsUp); i++) {
				relative += ".." + pathSeparator;
			}
		}
		relative += targetPath.substring(common.length());

		return relative;
	}

	public static void write(File file, byte[] data) throws Exception {

		FileOutputStream stream = null;
		BufferedOutputStream buffer = null;

		try {
			stream = new FileOutputStream(file);

			buffer = new BufferedOutputStream(stream);

			buffer.write(data);
		} finally {
			if (buffer != null) {

				buffer.close();
			}

			if (stream != null) {

				stream.close();
			}
		}
	}

	public static byte[] read(File file) throws Exception {

		byte[] fileData = null;
		FileInputStream stream = null;
		BufferedInputStream buffer = null;

		try {
			long fileLength = file.length();

			if (fileLength > Integer.MAX_VALUE)
				throw new IllegalArgumentException("File its too big!");

			fileData = new byte[(int) fileLength];

			stream = new FileInputStream(file);

			buffer = new BufferedInputStream(stream);

			buffer.read(fileData);
		} finally {
			if (buffer != null) {

				buffer.close();
			}

			if (stream != null) {

				stream.close();
			}
		}

		return fileData;
	}

	/**
	 * Creates a {@link File} instance with the supplied path. It will only use
	 * the "basePath" argument if the value from the "path" its not rooted.
	 * 
	 * @param basePath
	 *            Base path to use if the "path" argument value its not rooted.
	 * @param path
	 *            Path to the file.
	 * @return File instance.
	 */
	public static File createFile(String basePath, String path) {

		File file = new File(path);

		if (!file.isAbsolute())
			file = new File(basePath, path);

		return file;
	}

	/**
	 * Returns the last part of the supplied path. Its the last segment divided
	 * by the path separator char.
	 * 
	 * @param path
	 *            The path to retrieve the last part of.
	 * 
	 * @return Last part of the supplied path.
	 */
	public static String getLastPartFromPath(String path) {

		String[] segments = path.split(RegexHelper.escape(File.separator));

		return segments[segments.length - 1];
	}
}
