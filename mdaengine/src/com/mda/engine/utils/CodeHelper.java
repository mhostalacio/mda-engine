package com.mda.engine.utils;

import org.apache.commons.lang.NotImplementedException;
import org.jvnet.jaxb2_commons.lang.StringUtils;

import com.mda.engine.core.ApplicationScope;
import com.mda.engine.models.businessObjectModel.AttributeNumberDataType;
import com.mda.engine.models.businessObjectModel.BaseAttributeType;
import com.mda.engine.models.businessObjectModel.BaseAttributeWithTargetType;
import com.mda.engine.models.businessObjectModel.BinaryAttributeType;
import com.mda.engine.models.businessObjectModel.BooleanAttributeType;
import com.mda.engine.models.businessObjectModel.DateAttributeType;
import com.mda.engine.models.businessObjectModel.DatetimeAttributeType;
import com.mda.engine.models.businessObjectModel.EnumAttributeType;
import com.mda.engine.models.businessObjectModel.GuidAttributeType;
import com.mda.engine.models.businessObjectModel.IndentityAttributeType;
import com.mda.engine.models.businessObjectModel.MultiLanguageBaseAttributeType;
import com.mda.engine.models.businessObjectModel.NumberAttributeType;
import com.mda.engine.models.businessObjectModel.ObjectType;
import com.mda.engine.models.businessObjectModel.TextAttributeType;
import com.mda.engine.models.businessObjectModel.XmlAttributeType;
import com.mda.engine.models.dataModel.BigIntTableColumn;
import com.mda.engine.models.dataModel.BitTableColumn;
import com.mda.engine.models.dataModel.DataBaseItem;
import com.mda.engine.models.dataModel.DateTimeTableColumn;
import com.mda.engine.models.dataModel.DecimalTableColumn;
import com.mda.engine.models.dataModel.EnumTableColumn;
import com.mda.engine.models.dataModel.IntTableColumn;
import com.mda.engine.models.dataModel.NvarcharTableColumn;
import com.mda.engine.models.dataModel.TableColumn;
import com.mda.engine.models.dataModel.TinyIntTableColumn;
import com.mda.engine.models.dataModel.UniqueIdentifierTableColumn;
import com.mda.engine.models.dataModel.VarcharTableColumn;
import com.mda.engine.models.queryModel.BigIntList;
import com.mda.engine.models.queryModel.StringList;

public class CodeHelper {

	public static String getIndetityTypeName(ObjectType bom)
	{
		if (bom != null && bom.getIdentity().getMode().equals("Automatic"))
		{
			return "Int64";
		}
		else
		{
			return "String";
		}
	}
	
	public static String getClassName(BaseAttributeType att)
	{
		return getClassName(att, true, false, ApplicationScope.EntitiesLayer);
	}

	
	public static String getClassName(BaseAttributeType att, Boolean addNullableMark, Boolean ignoreLists, ApplicationScope scope)
	{
		if (att instanceof MultiLanguageBaseAttributeType)
		{
			MultiLanguageBaseAttributeType multi = (MultiLanguageBaseAttributeType)att;
			if (scope == ApplicationScope.ServicesLayer)
			{
				if (multi.isIsMultiLanguage())
					return "MultiLanguageDataContract<String>";
				else
					return "String";
				
			}
			else
			{
				if (multi.isIsMultiLanguage())
					return "MultiLanguageAttribute<String>";
				else
					return "String";
				
			}
		}
		else if (att instanceof IndentityAttributeType)
		{
			IndentityAttributeType id = (IndentityAttributeType)att;
			if (id.getMode().equals("Automatic"))
				return "Int64";
			else
				return "String";
		}
		else if (att instanceof GuidAttributeType)
		{
			return "Guid" + (att.isIsNullable() && addNullableMark ? "?" : "");
		}
		else if (att instanceof BinaryAttributeType)
		{
			return "byte[]";
		}
		else if (att instanceof EnumAttributeType)
		{
			EnumAttributeType enumAtt = (EnumAttributeType)att;
			if (scope == ApplicationScope.ServicesLayer)
			{
				return com.mda.engine.utils.StringUtils.getPascalCase(enumAtt.getLovName()) + "DataContract" + (att.isIsNullable() && addNullableMark ? "?" : "");
			}
			else
			{
				return com.mda.engine.utils.StringUtils.getPascalCase(enumAtt.getLovName()) + (att.isIsNullable() && addNullableMark ? "?" : "");
			}
		}
		else if (att instanceof BooleanAttributeType)
		{
			return "Boolean" + (att.isIsNullable() && addNullableMark ? "?" : "");
		}
		else if (att instanceof XmlAttributeType)
		{
			return "String";
		}
		else if (att instanceof DateAttributeType || att instanceof DatetimeAttributeType)
		{
			return "DateTime" + (att.isIsNullable() && addNullableMark ? "?" : "");
		}
		else if (att instanceof BaseAttributeWithTargetType)
		{
			BaseAttributeWithTargetType assoc = (BaseAttributeWithTargetType)att;
			if (assoc.getTargetObject() != null)
			{
				if (scope == ApplicationScope.ServicesLayer)
				{
					if (assoc.getMultiplicityNumber() > 1 && !ignoreLists)
					{
						return "CUDListDataContract<" + assoc.getTargetObject().getClassName() + "DataContract>";
					}
					else if (assoc.getMultiplicityNumber() <= 1 || ignoreLists)
					{
						return assoc.getTargetObject().getClassName() + "DataContract";
					}
				}
				else
				{
					if (assoc.getMultiplicityNumber() > 1 && !ignoreLists)
					{
						return "EntityList<" + assoc.getTargetObject().getClassName() + ">";
					}
					else if (assoc.getMultiplicityNumber() <= 1 || ignoreLists)
					{
						return assoc.getTargetObject().getClassName();
					}
				}
			}
		}
		else if (att instanceof NumberAttributeType)
		{
			NumberAttributeType numAtt =(NumberAttributeType)att;
			if (numAtt.getDataType().equals(AttributeNumberDataType.DECIMAL))
			{
				if (numAtt.isList())
					return "List<Decimal>";
				return "Decimal" + (att.isIsNullable() && addNullableMark ? "?" : "");
			}
			else if (numAtt.getDataType().equals(AttributeNumberDataType.DOUBLE))
			{
				if (numAtt.isList())
					return "List<Double>";
				return "Double" + (att.isIsNullable() && addNullableMark ? "?" : "");
			}
			else if (numAtt.getDataType().equals(AttributeNumberDataType.FLOAT))
			{
				if (numAtt.isList())
					return "List<Int64>";
				return "Int64" + (att.isIsNullable() && addNullableMark ? "?" : "");
			}
			else if (numAtt.getDataType().equals(AttributeNumberDataType.INTEGER))
			{
				if (numAtt.isList())
					return "List<Int32>";
				return "Int32" + (att.isIsNullable() && addNullableMark ? "?" : "");
			}
			else if (numAtt.getDataType().equals(AttributeNumberDataType.LONG))
			{
				if(!StringUtils.isEmpty(numAtt.getAttributeForTargetIdName()) && numAtt.isList())
					return "AggregationList";
				else if (numAtt.isList())
					return "List<Int64>";
				return "Int64" + (att.isIsNullable() && addNullableMark ? "?" : "");
			}
			else if (numAtt.getDataType().equals(AttributeNumberDataType.SHORT))
			{
				if (numAtt.isList())
					return "List<Int16>";
				return "Int16" + (att.isIsNullable() && addNullableMark ? "?" : "");
			}
			else if (numAtt.getDataType().equals(AttributeNumberDataType.BYTE))
			{
				if (numAtt.isList())
					return "List<Byte>";
				return "Byte" + (att.isIsNullable() && addNullableMark ? "?" : "");
			}
		}
		throw new NotImplementedException();
	}
	
	public static BaseAttributeType createAttributeFromColumn(DataBaseItem column)
	{
		BaseAttributeType att = null;
		if (column instanceof VarcharTableColumn)
		{
			att = new TextAttributeType();
			TextAttributeType convertedAtt = (TextAttributeType)att;
			convertedAtt.setUseNVARCHAR(false);
			convertedAtt.setMaxLength(((VarcharTableColumn) column).getMaxSize());
		}
		else if (column instanceof NvarcharTableColumn)
		{
			att = new TextAttributeType();
			TextAttributeType convertedAtt = (TextAttributeType)att;
			convertedAtt.setUseNVARCHAR(true);
			convertedAtt.setMaxLength(((NvarcharTableColumn) column).getMaxSize());
		}
		else if (column instanceof BigIntTableColumn)
		{
			if (column.getName().equals("Id"))
			{
				att = new IndentityAttributeType();
				((IndentityAttributeType)att).setMode("Automatic");
			}
			else
			{
				att = new NumberAttributeType();
				NumberAttributeType convertedAtt = (NumberAttributeType)att;
				convertedAtt.setDataType(AttributeNumberDataType.LONG);
			}
		}
		else if (column instanceof IntTableColumn)
		{
			att = new NumberAttributeType();
			NumberAttributeType convertedAtt = (NumberAttributeType)att;
			convertedAtt.setDataType(AttributeNumberDataType.INTEGER);
		}
		else if (column instanceof DecimalTableColumn)
		{
			att = new NumberAttributeType();
			NumberAttributeType convertedAtt = (NumberAttributeType)att;
			convertedAtt.setDataType(AttributeNumberDataType.DECIMAL);
		}
		else if (column instanceof BitTableColumn)
		{
			att = new BooleanAttributeType();
		}
		else if (column instanceof DateTimeTableColumn)
		{
			att = new DatetimeAttributeType();
		}
		else if (column instanceof UniqueIdentifierTableColumn)
		{
			att = new GuidAttributeType();
		}
		else if(column instanceof TinyIntTableColumn) {
			NumberAttributeType numAtt = new NumberAttributeType(); 
			numAtt.setDataType(AttributeNumberDataType.BYTE);
			
			att = numAtt;			
		}
		else if(column instanceof EnumTableColumn) {
			EnumAttributeType numAtt = new EnumAttributeType(); 
			numAtt.setLovName(((EnumTableColumn)column).getEnumName());
			
			att = numAtt;			
		}
		else if(column instanceof BigIntList) {
			att = new NumberAttributeType();
			att.setList(true);
			NumberAttributeType convertedAtt = (NumberAttributeType)att;
			convertedAtt.setDataType(AttributeNumberDataType.LONG);		
		}
		else if(column instanceof StringList) {
			att = new TextAttributeType();
			TextAttributeType convertedAtt = (TextAttributeType)att;
			convertedAtt.setUseNVARCHAR(false);
			convertedAtt.setMaxLength(((VarcharTableColumn) column).getMaxSize());
			att.setList(true);	
		}
		
		att.setName(column.getName());
		att.setIsNullable(column.isNullable());
		att.setPersistable(true);
		if (column instanceof TableColumn)
		{
			att.setMatchingTableColumn((TableColumn)column);
		}
		return att;
	}
}
