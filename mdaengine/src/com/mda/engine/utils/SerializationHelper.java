package com.mda.engine.utils;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import org.xml.sax.SAXException;

import com.mda.engine.models.businessObjectModel.BaseType;
import com.mda.engine.models.businessObjectModel.InterfaceType;
import com.mda.engine.models.businessObjectModel.ObjectType;
import com.mda.engine.models.businessTransactionModel.BusinessTransaction;
import com.mda.engine.models.configurationManagementModel.Application;
import com.mda.engine.models.mvc.MVCInstance;
import com.mda.engine.models.queryModel.Query;
import com.mda.engine.models.rulesModel.ObjectValidationScope;



public class SerializationHelper {

	private static Unmarshaller unmarshaller = null;
	private static Marshaller marshaller = null;
	
	private static JAXBContext createContext() throws JAXBException
	{
		return JAXBContext.newInstance("com.mda.engine.models.configurationManagementModel:com.mda.engine.models.businessObjectModel:com.mda.engine.models.queryModel:com.mda.engine.models.rulesModel:com.mda.engine.models.businessTransactionModel:com.mda.engine.models.mvc");
	}
	
	private static void InitializeMarshaller() throws JAXBException
	{
		JAXBContext ctx = createContext();
		marshaller = ctx.createMarshaller();
	}
	
	private static void InitializeUnmarshaller() throws JAXBException
	{
		JAXBContext ctx = createContext();
		unmarshaller = ctx.createUnmarshaller();
	}


	/**
	 * This method loads and parses XML document.
	 * 
	 * @param filePath
	 *            File path to load
	 * @return xml document.
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws SAXException 
	 */
	public static org.w3c.dom.Document disserializeXML(String filePath) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
		domFactory.setNamespaceAware(true); // never forget this!
		DocumentBuilder builder = domFactory.newDocumentBuilder();
		org.w3c.dom.Document doc = builder.parse(new FileInputStream(filePath),"UTF-8");
		return doc;
	}
	
	/**
	 * This method loads and parses Application document.
	 * 
	 * @param filePath
	 *            File path to load
	 * @return Application.
	 * @throws FileNotFoundException 
	 */
	public static Application deserializeApplication(String filePath) throws JAXBException, FileNotFoundException {
		Source source = new StreamSource(filePath);
		if(unmarshaller == null) {			
			InitializeUnmarshaller();
		}
		JAXBElement<Application> root = unmarshaller.unmarshal(source, Application.class);
		return (Application)root.getValue();
	}
	
	/**
	 * This method loads and parses BaseType document.
	 * 
	 * @param filePath
	 *            File path to load
	 * @return BaseType.
	 * @throws FileNotFoundException 
	 */
	@SuppressWarnings("unchecked")
	public static ObjectType deserializeBusinessObject(String filePath) throws JAXBException, FileNotFoundException {
		Source source = new StreamSource(filePath);
		if(unmarshaller == null) {			
			InitializeUnmarshaller();
		}
		JAXBElement root = (JAXBElement)unmarshaller.unmarshal(source);
		if (root.getValue() instanceof InterfaceType)
			return (InterfaceType)root.getValue();
		return (ObjectType)root.getValue();
	}
	
	/**
	 * This method loads and parses Query document.
	 * 
	 * @param filePath
	 *            File path to load
	 * @return Query.
	 * @throws FileNotFoundException 
	 */
	public static Query deserializeQuery(String filePath) throws JAXBException, FileNotFoundException {
		Source source = new StreamSource(filePath);
		if(unmarshaller == null) {			
			InitializeUnmarshaller();
		}
		JAXBElement<Query> root = unmarshaller.unmarshal(source, Query.class);
		return (Query)root.getValue();
	}
	
	/**
	 * This method loads and parses Rules document.
	 * 
	 * @param filePath
	 *            File path to load
	 * @return ObjectValidationScope.
	 * @throws FileNotFoundException 
	 */
	public static ObjectValidationScope deserializeRules(String filePath) throws JAXBException, FileNotFoundException {
		Source source = new StreamSource(filePath);
		if(unmarshaller == null) {			
			InitializeUnmarshaller();
		}
		JAXBElement<ObjectValidationScope> root = unmarshaller.unmarshal(source, ObjectValidationScope.class);
		return (ObjectValidationScope)root.getValue();
	}
	
	/**
	 * This method loads and parses BusinessTransaction document.
	 * 
	 * @param filePath
	 *            File path to load
	 * @return ObjectValidationScope.
	 * @throws FileNotFoundException 
	 */
	public static BusinessTransaction deserializeBusinessTransaction(String filePath) throws JAXBException, FileNotFoundException {
		Source source = new StreamSource(filePath);
		if(unmarshaller == null) {			
			InitializeUnmarshaller();
		}
		JAXBElement<BusinessTransaction> root = unmarshaller.unmarshal(source, BusinessTransaction.class);
		return (BusinessTransaction)root.getValue();
	}
	
	public static MVCInstance deserializeMVC(String filePath) throws JAXBException, FileNotFoundException {
		Source source = new StreamSource(filePath);
		if(unmarshaller == null) {			
			InitializeUnmarshaller();
		}
		JAXBElement<MVCInstance> root = unmarshaller.unmarshal(source, MVCInstance.class);
		return (MVCInstance)root.getValue();
	}
}
