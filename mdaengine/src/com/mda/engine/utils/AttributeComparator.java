package com.mda.engine.utils;

import java.util.Comparator;

import com.mda.engine.models.businessObjectModel.BaseAttributeType;

public class AttributeComparator implements Comparator<BaseAttributeType> {
	
	public int compare(BaseAttributeType att1, BaseAttributeType att2)
	{
		return att1.getName().compareTo(att2.getName());
	}

}
