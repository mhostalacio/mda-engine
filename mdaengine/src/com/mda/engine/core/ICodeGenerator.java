package com.mda.engine.core;

import java.io.File;
import java.util.List;

public interface ICodeGenerator {

	/**
	 * 
	 * @return One String with the code

	 */
	public void generateCode(File fileToWriteTo) throws Exception;

	/**
	 * 
	 * @return indicate If generator found errors in models
	 */
	public boolean hasErrors();
	
	/**
	 * 
	 * @return The list of errors were found in models by code generator 
	 */
	public List<String> getErrors();

}
