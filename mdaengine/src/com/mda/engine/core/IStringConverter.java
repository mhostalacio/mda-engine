package com.mda.engine.core;

public interface IStringConverter<T> {

	/**
	 * Returns the String representation of the supplied object.
	 * 
	 * @param obj
	 *            Object.
	 * @return String representation.
	 */
	public String getString(T obj);
}
