package com.mda.engine.core;

import com.mda.engine.models.mvc.MVCViewValueChoice;

public interface IMVCViewElementWithDatasource {

	MVCViewValueChoice getDataSource();
	String getDataSourceSingleClassName();
	String getId();
	com.mda.engine.core.IMVCViewElementWithDatasource getContextElement();
}
