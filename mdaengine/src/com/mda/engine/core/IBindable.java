package com.mda.engine.core;

import com.mda.engine.models.common.TypeBase;
import com.mda.engine.models.configurationManagementModel.Application;

public interface IBindable {

	/**
	 * @param name
	 * @param scope TODO
	 * @return
	 */
	IBindable getChild(String name, ApplicationScope scope, Application app);
	
	/**
	 * @param scope TODO
	 * @param release
	 * @return
	 */
	TypeBase getBindType(ApplicationScope scope, Application app);
	
	

}

