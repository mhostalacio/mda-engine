package com.mda.engine.core;

import com.mda.engine.models.mvc.MVCViewElementCollection;

public interface IMVCViewComposedElement {

	MVCViewElementCollection getChildElements();
}
