package com.mda.engine.core;

public interface IModelNode {
	
	IModelNode getParentElement();
	
	void setParentElement(IModelNode parentElem);
}
