package com.mda.engine.core;


import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import com.mda.engine.models.businessObjectModel.InterfaceType;
import com.mda.engine.models.businessObjectModel.LovDefinitionType;
import com.mda.engine.models.businessObjectModel.ObjectType;
import com.mda.engine.models.businessTransactionModel.BusinessTransaction;
import com.mda.engine.models.configurationManagementModel.Application;
import com.mda.engine.models.dataModel.DataTable;
import com.mda.engine.models.mvc.MVCInstance;
import com.mda.engine.models.queryModel.Query;
import com.mda.engine.models.rulesModel.ObjectValidationScope;
import com.mda.engine.utils.IOHelper;
import com.mda.engine.utils.QueryHelper;
import com.mda.engine.utils.StringUtils;


public class Product {

	private String name;
	private HashMap<ConfigurationKey, String> properties;
	private ArrayList<Application> applications;
	private HashMap<ApplicationScope, Boolean> scopes2Generate;
	private HashMap<String, ArrayList<ObjectType>> bomsByApp;
	private HashMap<String, ArrayList<InterfaceType>> interfacesByApp;
	private HashMap<String, ArrayList<Query>> queriesByApp;
	private HashMap<String, ArrayList<ObjectValidationScope>> rulesByApp;
	private HashMap<String, ArrayList<BusinessTransaction>> btmsByApp;
	private HashMap<String, ArrayList<DataTable>> tablesByApp;
	private HashMap<String, ArrayList<String>> packagesByApp;
	private HashMap<String, ArrayList<String>> transactionPackagesByApp;
	private HashMap<String, ArrayList<MVCInstance>> mvcsByApp;
	private HashMap<String, ArrayList<LovDefinitionType>> lovsByApp;
	private HashMap<String, HashMap<String, String>> transactionsMappingByApp;	
	
	public static String BIG_INT_PAIR_BOM_NAME = "BigIntPair";

	public Product(String name)
	{
		this.properties = new HashMap<ConfigurationKey, String>();
		this.name = name;
		this.applications = new ArrayList<Application>();
		this.scopes2Generate = new HashMap<ApplicationScope, Boolean>();
		this.scopes2Generate.put(ApplicationScope.DataLayer, true);
		this.scopes2Generate.put(ApplicationScope.EntitiesLayer, true);
		this.scopes2Generate.put(ApplicationScope.ServicesLayer, true);
		this.scopes2Generate.put(ApplicationScope.MVCLayer, true);
		bomsByApp = new HashMap<String, ArrayList<ObjectType>>();
		queriesByApp = new HashMap<String, ArrayList<Query>>();
		rulesByApp = new HashMap<String, ArrayList<ObjectValidationScope>>();
		btmsByApp = new HashMap<String, ArrayList<BusinessTransaction>>();
		interfacesByApp = new HashMap<String, ArrayList<InterfaceType>>();
		tablesByApp = new HashMap<String, ArrayList<DataTable>>();
		packagesByApp = new HashMap<String, ArrayList<String>>();
		transactionPackagesByApp = new HashMap<String, ArrayList<String>>();
		mvcsByApp =  new HashMap<String, ArrayList<MVCInstance>>(); 
		lovsByApp = new HashMap<String, ArrayList<LovDefinitionType>>();
		transactionsMappingByApp = new HashMap<String, HashMap<String, String>>();
	}
	
	public void setGenerationScopes(HashMap<ApplicationScope, Boolean> scopes)
	{
		this.scopes2Generate = scopes;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void addProperty(ConfigurationKey key, String value) {
		this.properties.put(key, value);
	}

	public String getProperty(ConfigurationKey key) {
		return this.properties.get(key);
	}
	
	public void addApplication(Application application) throws Exception {
		this.applications.add(application);
		application.setProduct(this);

		if (!StringUtils.isNullOrEmpty(application.getRelativeFolderPath())) {
			String baseDir = this.getProperty(ConfigurationKey.ApplicationsFolder);
			application.setGenerationInfoFolder(IOHelper.combine(baseDir, application.getRelativeFolderPath(), application.getGenerationInfoFolder(), application.getUniqueName().toString()));
			IOHelper.ensureDirectoryExists(new File(application.getGenerationInfoFolder()));
		}

	}
	
	public Boolean generateScope(ApplicationScope scope)
	{
		return this.scopes2Generate.get(scope);
	}
	
	public ArrayList<Application> getApplications()
	{
		return this.applications;
	}
	
	public Application getApplicationByName(String name)
	{
		for(Application app : this.applications)
		{
			if (app.getUniqueName().equals(name))
			{
				return app;
			}
		}
		return null;
	}
	
	public void addObject(Application app, ObjectType bom) {
		bom.setApplication(app);
		if (bom instanceof InterfaceType)
		{
			if (!interfacesByApp.containsKey(app.getUniqueName()))
			{
				interfacesByApp.put(app.getUniqueName(), new ArrayList<InterfaceType>());
			}
			interfacesByApp.get(app.getUniqueName()).add((InterfaceType)bom);
		}
		else
		{
			if (!bomsByApp.containsKey(app.getUniqueName()))
			{
				bomsByApp.put(app.getUniqueName(), new ArrayList<ObjectType>());
			}
			bomsByApp.get(app.getUniqueName()).add(bom);
		}
		if (!packagesByApp.containsKey(app.getUniqueName()))
		{
			packagesByApp.put(app.getUniqueName(), new ArrayList<String>());
		}
		if (bom.getPackageName() != null && !packagesByApp.get(app.getUniqueName()).contains(bom.getPackageName()))
		{
			packagesByApp.get(app.getUniqueName()).add(bom.getPackageName());
		}
		
		if (bom.getLovs() != null && bom.getLovs().getLovDefinition() != null)
		{
			if (!lovsByApp.containsKey(app.getUniqueName()))
			{
				lovsByApp.put(app.getUniqueName(), new ArrayList<LovDefinitionType>());
			}
			for (LovDefinitionType lov : bom.getLovs().getLovDefinition())
			{
				lovsByApp.get(app.getUniqueName()).add(lov);
			}
		}
	}
	
	public ArrayList<ObjectType> getApplicationBOMs(Application app)
	{
		if (bomsByApp.containsKey(app.getUniqueName()))
		{
			return bomsByApp.get(app.getUniqueName());
		}
		return new ArrayList<ObjectType>();
	}
	
	public ArrayList<ObjectType> getApplicationBOMs(Application app, String packageName)
	{
		if (bomsByApp.containsKey(app.getUniqueName()))
		{
			ArrayList<ObjectType> subSet =  bomsByApp.get(app.getUniqueName());
			ArrayList<ObjectType> toRet = new ArrayList<ObjectType>();
			for (ObjectType bom : subSet)
			{
				if (bom.getPackageName() != null && bom.getPackageName().equals(packageName))
					toRet.add(bom);
			}
			return toRet;
		}
		return new ArrayList<ObjectType>();
	}
	
	public ArrayList<BusinessTransaction> getApplicationBTMs(Application app)
	{
		if (btmsByApp.containsKey(app.getUniqueName()))
		{
			return btmsByApp.get(app.getUniqueName());
		}
		return new ArrayList<BusinessTransaction>();
	}
	
	public ArrayList<InterfaceType> getApplicationInterfaces(Application app)
	{
		if (interfacesByApp.containsKey(app.getUniqueName()))
		{
			return interfacesByApp.get(app.getUniqueName());
		}
		return new ArrayList<InterfaceType>();
	}
	
	public ArrayList<DataTable> getApplicationTables(Application app)
	{
		if (tablesByApp.containsKey(app.getUniqueName()))
		{
			return tablesByApp.get(app.getUniqueName());
		}
		return new ArrayList<DataTable>();
	}
	
	public ArrayList<Query> getApplicationQueries(Application app)
	{
		if (queriesByApp.containsKey(app.getUniqueName()))
		{
			return queriesByApp.get(app.getUniqueName());
		}
		return new ArrayList<Query>();
	}
	
	public ArrayList<String> getApplicationPackages(Application app)
	{
		if (packagesByApp.containsKey(app.getUniqueName()))
		{
			return packagesByApp.get(app.getUniqueName());
		}
		return new ArrayList<String>();
	}
	
	public ArrayList<String> getTransactionPackages(Application app){
		if(this.transactionPackagesByApp.containsKey(app.getUniqueName())){
			return this.transactionPackagesByApp.get(app.getUniqueName());
		}
		return new ArrayList<String>();
	}
	
	public ArrayList<MVCInstance> getApplicationMvcs(Application app)
	{
		if (mvcsByApp.containsKey(app.getUniqueName()))
		{
			return mvcsByApp.get(app.getUniqueName());
		}
		return new ArrayList<MVCInstance>();
	}
	
	public MVCInstance getMvcByName(String name)
	{
		for(Application app : this.applications)
		{
		     ArrayList<MVCInstance> list = mvcsByApp.get(app.getUniqueName());
		     if (list != null)
		     {
		    	 for(MVCInstance obj : list)
				 {
					 if (obj.getName().equals(name))
					 	return obj;
				 }
		     }
		}
		return null;
	}
	
	public ObjectType getBOMByApplicationAndName(Application app, String name)
	{
		if (bomsByApp.containsKey(app.getUniqueName()))
		{
			 for(ObjectType obj : bomsByApp.get(app.getUniqueName()))
			 {
				 if (obj.getName().equals(name))
				 	return obj;
			 }
		}
		return null;
	}
	
	public Query getQueryByApplicationAndName(Application app, String name)
	{
		if (queriesByApp.containsKey(app.getUniqueName()))
		{
			 for(Query query : queriesByApp.get(app.getUniqueName()))
			 {
				 if (query.getName().equals(name))
				 	return query;
			 }
		}
		return null;
	}
	
	public Query getQueryById(Application app, ObjectType bom)
	{
		if (queriesByApp.containsKey(app.getUniqueName()))
		{
			 for(Query query : queriesByApp.get(app.getUniqueName()))
			 {
				 if (query.getOwnedBy().getName().equals(bom.getName()) && query.getName().equals(QueryHelper.GET_BY_ID_QUERY_NAME))
				 	return query;
			 }
		}
		return null;
	}
	
//	public ObjectType getBOMByName(String name)
//	{
//		for(Application app : this.applications)
//		{
//			 java.util.ArrayList<ObjectType> list = bomsByApp.get(app.getUniqueName());
//			 if (list != null)
//			 {
//				 for(ObjectType obj : list)
//				 {
//					 if (obj.getName().equals(name))
//					 	return obj;
//				 }
//			 } 
//		}
//		return null;
//	}
	
	public LovDefinitionType getLovByName(String name)
	{
		for(Application app : this.applications)
		{
			if (lovsByApp.containsKey(app.getUniqueName()))
			{
				 for(LovDefinitionType obj : lovsByApp.get(app.getUniqueName()))
				 {
					 if (obj.getName().equals(name))
					 	return obj;
				 }
			}
		}
		return null;
	}
	
	public void addQuery(Application app, Query query) {
		if (!queriesByApp.containsKey(app.getUniqueName()))
		{
			queriesByApp.put(app.getUniqueName(), new ArrayList<Query>());
		}
		queriesByApp.get(app.getUniqueName()).add(query);
		if (query.getOwnedBy() != null && query.getOwner() == null)
		{
			query.setOwner(app.getProduct().getBOMByApplicationAndName(app, query.getOwnedBy().getName()));
		}
		if (query.getOwner() != null)
			query.getOwner().associateQuery(query);
	}
	
	public void addRules(Application app, ObjectValidationScope rules) {
		if (!rulesByApp.containsKey(app.getUniqueName()))
		{
			rulesByApp.put(app.getUniqueName(), new ArrayList<ObjectValidationScope>());
		}
		rulesByApp.get(app.getUniqueName()).add(rules);
	}
	
	public void addBTM(Application app, BusinessTransaction btm) {
		if (!btmsByApp.containsKey(app.getUniqueName()))
		{
			btmsByApp.put(app.getUniqueName(), new ArrayList<BusinessTransaction>());
		}
		
		if (!transactionPackagesByApp.containsKey(app.getUniqueName()))
		{
			transactionPackagesByApp.put(app.getUniqueName(), new ArrayList<String>());
		}
		if (btm.getPackageName() != null && !transactionPackagesByApp.get(app.getUniqueName()).contains(btm.getPackageName()))
		{
			transactionPackagesByApp.get(app.getUniqueName()).add(btm.getPackageName());
		}
		
		btm.setApplication(app);
		btmsByApp.get(app.getUniqueName()).add(btm);
	}
	
	public void addMVC(Application app, MVCInstance mvc) {
		mvc.setApplication(app);
		if (!mvcsByApp.containsKey(app.getUniqueName()))
		{
			mvcsByApp.put(app.getUniqueName(), new ArrayList<MVCInstance>());
		}
		mvcsByApp.get(app.getUniqueName()).add(mvc);
	}
	
	public void addTable(Application app, DataTable table) {
		if (!tablesByApp.containsKey(app.getUniqueName()))
		{
			tablesByApp.put(app.getUniqueName(), new ArrayList<DataTable>());
		}
		tablesByApp.get(app.getUniqueName()).add(table);
	}
	
	
	public void addTransactionMapping(Application app, String transactionID, String transactionLabel) {
		if (!transactionsMappingByApp.containsKey(app.getUniqueName()))
		{
			transactionsMappingByApp.put(app.getUniqueName(), new HashMap<String, String>());
		}
		HashMap<String, String> item = transactionsMappingByApp.get(app.getUniqueName());
		item.put(transactionID, transactionLabel);
	}
	
	public HashMap<String, String> getTransactionMappingByApplication(Application app)
	{
		if (transactionsMappingByApp.containsKey(app.getUniqueName()))
		{
			 return transactionsMappingByApp.get(app.getUniqueName());
		}
		return null;
	}
}
