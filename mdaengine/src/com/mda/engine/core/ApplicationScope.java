package com.mda.engine.core;

public enum ApplicationScope {

	DataLayer,
	EntitiesLayer,
	ServicesLayer,
	MVCLayer
}
