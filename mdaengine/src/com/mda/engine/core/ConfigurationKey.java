package com.mda.engine.core;

public enum ConfigurationKey {
	ApplicationsFolder,
	PropertiesFolder,
	GeneratedFilesExtension,
	WebFilesExtension,
	ServiceNamespace,
}
