package com.mda.engine.core;

import javax.xml.bind.annotation.XmlTransient;
import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.NullArgumentException;
import com.mda.engine.models.common.TypeBase;
import com.mda.engine.models.configurationManagementModel.Application;

public class ModelNodeBase extends MdaLogger implements IModelNode, IBindable {
	
	private transient IModelNode _parentElement;
	
	@XmlTransient
	public IModelNode getParentElement() {
		return _parentElement;
	}

	public void setParentElement(IModelNode element) {
		_parentElement = element;
	}
	
	public final void normalizeTree(IModelNode parentElement) {
		
		if (parentElement == null)
			throw new NullArgumentException("parentElement");
		
		_parentElement = parentElement;
		
		normalizeTreeInternal(parentElement);
	}

	protected void normalizeTreeInternal(IModelNode parentElement) {
		
	}
	
	@Override
	public IBindable getChild(String name, ApplicationScope scope, Application app) {

		throw new NotImplementedException(String.format("Type %s does not implement getChild method!", getClass().getName()));
	}
	
	@Override
	public TypeBase getBindType(ApplicationScope scope, Application app) {

		throw new NotImplementedException(String.format("Type %s does not implement getBindType method!", getClass().getName()));
	}
	
	
	public String getFullName() {
		
		return null;
	}
	
	


}
