package com.mda.engine.core;

import com.mda.engine.models.mvc.MVCTriggerCollection;

public interface IMVCViewClickableElement {
	MVCTriggerCollection getOnClick();
}
