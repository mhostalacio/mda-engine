package com.mda.engine.core;


import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public abstract class MdaLogger {

	private static Level defaultLevel = Level.DEBUG;

	private Logger logger = Logger.getLogger(this.getClass());

	public Logger getLogger() {
		
		return logger;
	}

	public void log() {

		log(null);
	}

	public void log(Object message) {

		log(defaultLevel, message);
	}

	public void log(Level level, Object message) {

		logger.log(level, message);
	}

	public static void log(Class<?> clazz) {

		log(clazz, defaultLevel, null);
	}

	public static void log(Class<?> clazz, Object message) {

		log(clazz, defaultLevel, message);
	}

	public static void log(Class<?> clazz, Level level, Object message) {

		Logger.getLogger(clazz).log(level, message);
	}

	public static void log(Class<?> clazz, Level level, Object message, Exception ex) {

		Logger.getLogger(clazz).log(level, message, ex);
	}
	
	public static void configureLog(String configFilename) {

		PropertyConfigurator.configure(configFilename);
	}

	
}
