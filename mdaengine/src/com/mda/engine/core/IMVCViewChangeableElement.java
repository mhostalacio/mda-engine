package com.mda.engine.core;

import com.mda.engine.models.mvc.MVCTriggerCollection;

public interface IMVCViewChangeableElement {
	MVCTriggerCollection getOnChange();
	MVCTriggerCollection getOnKeyPress();
	MVCTriggerCollection getOnKeyUp();
	MVCTriggerCollection getOnFocus();
	MVCTriggerCollection getOnKeyDown();
	MVCTriggerCollection getOnOutFocus();
}
