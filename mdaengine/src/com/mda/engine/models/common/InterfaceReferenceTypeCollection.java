
package com.mda.engine.models.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for InterfaceReferenceTypeCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InterfaceReferenceTypeCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Interface" type="{http://www.mdaengine.com/mdaengine/models/common}BOMReferenceType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InterfaceReferenceTypeCollection", propOrder = {
    "_interface"
})
public class InterfaceReferenceTypeCollection
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Interface", required = true)
    protected List<BOMReferenceType> _interface;

    /**
     * Gets the value of the interface property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the interface property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInterface().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BOMReferenceType }
     * 
     * 
     */
    public List<BOMReferenceType> getInterface() {
        if (_interface == null) {
            _interface = new ArrayList<BOMReferenceType>();
        }
        return this._interface;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof InterfaceReferenceTypeCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final InterfaceReferenceTypeCollection that = ((InterfaceReferenceTypeCollection) object);
        {
            List<BOMReferenceType> lhsInterface;
            lhsInterface = this.getInterface();
            List<BOMReferenceType> rhsInterface;
            rhsInterface = that.getInterface();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "_interface", lhsInterface), LocatorUtils.property(thatLocator, "_interface", rhsInterface), lhsInterface, rhsInterface)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof InterfaceReferenceTypeCollection) {
            final InterfaceReferenceTypeCollection copy = ((InterfaceReferenceTypeCollection) draftCopy);
            if ((this._interface!= null)&&(!this._interface.isEmpty())) {
                List<BOMReferenceType> sourceInterface;
                sourceInterface = this.getInterface();
                @SuppressWarnings("unchecked")
                List<BOMReferenceType> copyInterface = ((List<BOMReferenceType> ) strategy.copy(LocatorUtils.property(locator, "_interface", sourceInterface), sourceInterface));
                copy._interface = null;
                List<BOMReferenceType> uniqueInterfacel = copy.getInterface();
                uniqueInterfacel.addAll(copyInterface);
            } else {
                copy._interface = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new InterfaceReferenceTypeCollection();
    }

}
