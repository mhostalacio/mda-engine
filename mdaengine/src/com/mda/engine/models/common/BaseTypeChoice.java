
package com.mda.engine.models.common;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for BaseTypeChoice complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BaseTypeChoice">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="Literal" type="{http://www.mdaengine.com/mdaengine/models/common}LiteralType"/>
 *         &lt;element name="BOM" type="{http://www.mdaengine.com/mdaengine/models/common}BOMReferenceType"/>
 *         &lt;element name="DataContract" type="{http://www.mdaengine.com/mdaengine/models/common}DataContractReferenceType"/>
 *         &lt;element name="List" type="{http://www.mdaengine.com/mdaengine/models/common}ListType"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BaseTypeChoice", propOrder = {
    "type"
})
public class BaseTypeChoice
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElements({
        @XmlElement(name = "BOM", type = BOMReferenceType.class),
        @XmlElement(name = "Literal", type = LiteralType.class),
        @XmlElement(name = "List", type = ListType.class),
        @XmlElement(name = "DataContract", type = DataContractReferenceType.class)
    })
    protected TypeBase type;

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link BOMReferenceType }
     *     {@link LiteralType }
     *     {@link ListType }
     *     {@link DataContractReferenceType }
     *     
     */
    public TypeBase getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link BOMReferenceType }
     *     {@link LiteralType }
     *     {@link ListType }
     *     {@link DataContractReferenceType }
     *     
     */
    public void setType(TypeBase value) {
        this.type = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof BaseTypeChoice)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final BaseTypeChoice that = ((BaseTypeChoice) object);
        {
            TypeBase lhsType;
            lhsType = this.getType();
            TypeBase rhsType;
            rhsType = that.getType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "type", lhsType), LocatorUtils.property(thatLocator, "type", rhsType), lhsType, rhsType)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof BaseTypeChoice) {
            final BaseTypeChoice copy = ((BaseTypeChoice) draftCopy);
            if (this.type!= null) {
                TypeBase sourceType;
                sourceType = this.getType();
                TypeBase copyType = ((TypeBase) strategy.copy(LocatorUtils.property(locator, "type", sourceType), sourceType));
                copy.setType(copyType);
            } else {
                copy.type = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new BaseTypeChoice();
    }

}
