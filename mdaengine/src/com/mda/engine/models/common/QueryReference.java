
package com.mda.engine.models.common;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for QueryReference complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueryReference">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="PackageName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="BOMName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="QueryName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryReference")
public class QueryReference
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "PackageName")
    protected String packageName;
    @XmlAttribute(name = "BOMName", required = true)
    protected String bomName;
    @XmlAttribute(name = "QueryName", required = true)
    protected String queryName;

    /**
     * Gets the value of the packageName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackageName() {
        return packageName;
    }

    /**
     * Sets the value of the packageName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackageName(String value) {
        this.packageName = value;
    }

    /**
     * Gets the value of the bomName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBOMName() {
        return bomName;
    }

    /**
     * Sets the value of the bomName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBOMName(String value) {
        this.bomName = value;
    }

    /**
     * Gets the value of the queryName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQueryName() {
        return queryName;
    }

    /**
     * Sets the value of the queryName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQueryName(String value) {
        this.queryName = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof QueryReference)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final QueryReference that = ((QueryReference) object);
        {
            String lhsPackageName;
            lhsPackageName = this.getPackageName();
            String rhsPackageName;
            rhsPackageName = that.getPackageName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "packageName", lhsPackageName), LocatorUtils.property(thatLocator, "packageName", rhsPackageName), lhsPackageName, rhsPackageName)) {
                return false;
            }
        }
        {
            String lhsBOMName;
            lhsBOMName = this.getBOMName();
            String rhsBOMName;
            rhsBOMName = that.getBOMName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "bomName", lhsBOMName), LocatorUtils.property(thatLocator, "bomName", rhsBOMName), lhsBOMName, rhsBOMName)) {
                return false;
            }
        }
        {
            String lhsQueryName;
            lhsQueryName = this.getQueryName();
            String rhsQueryName;
            rhsQueryName = that.getQueryName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "queryName", lhsQueryName), LocatorUtils.property(thatLocator, "queryName", rhsQueryName), lhsQueryName, rhsQueryName)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof QueryReference) {
            final QueryReference copy = ((QueryReference) draftCopy);
            if (this.packageName!= null) {
                String sourcePackageName;
                sourcePackageName = this.getPackageName();
                String copyPackageName = ((String) strategy.copy(LocatorUtils.property(locator, "packageName", sourcePackageName), sourcePackageName));
                copy.setPackageName(copyPackageName);
            } else {
                copy.packageName = null;
            }
            if (this.bomName!= null) {
                String sourceBOMName;
                sourceBOMName = this.getBOMName();
                String copyBOMName = ((String) strategy.copy(LocatorUtils.property(locator, "bomName", sourceBOMName), sourceBOMName));
                copy.setBOMName(copyBOMName);
            } else {
                copy.bomName = null;
            }
            if (this.queryName!= null) {
                String sourceQueryName;
                sourceQueryName = this.getQueryName();
                String copyQueryName = ((String) strategy.copy(LocatorUtils.property(locator, "queryName", sourceQueryName), sourceQueryName));
                copy.setQueryName(copyQueryName);
            } else {
                copy.queryName = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new QueryReference();
    }
    
//--simple--preserve
    
    private transient com.mda.engine.models.queryModel.Query referencedQuery;
    
    public void setReferencedQuery(com.mda.engine.models.queryModel.Query referencedQuery) {
		this.referencedQuery = referencedQuery;
	}

	public com.mda.engine.models.queryModel.Query getReferencedQuery() {
		return referencedQuery;
	}

//--simple--preserve

}
