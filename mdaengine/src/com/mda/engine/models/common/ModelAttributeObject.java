
package com.mda.engine.models.common;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ModelAttributeObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ModelAttributeObject">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/common}ModelAttribute">
 *       &lt;attribute name="ObjectType" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="PackageName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="ApplicationName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ModelAttributeObject")
public class ModelAttributeObject
    extends ModelAttribute
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "ObjectType", required = true)
    protected String objectType;
    @XmlAttribute(name = "PackageName")
    protected String packageName;
    @XmlAttribute(name = "ApplicationName")
    protected String applicationName;

    /**
     * Gets the value of the objectType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectType() {
        return objectType;
    }

    /**
     * Sets the value of the objectType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectType(String value) {
        this.objectType = value;
    }

    /**
     * Gets the value of the packageName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackageName() {
        return packageName;
    }

    /**
     * Sets the value of the packageName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackageName(String value) {
        this.packageName = value;
    }

    /**
     * Gets the value of the applicationName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicationName() {
        return applicationName;
    }

    /**
     * Sets the value of the applicationName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicationName(String value) {
        this.applicationName = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ModelAttributeObject)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final ModelAttributeObject that = ((ModelAttributeObject) object);
        {
            String lhsObjectType;
            lhsObjectType = this.getObjectType();
            String rhsObjectType;
            rhsObjectType = that.getObjectType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "objectType", lhsObjectType), LocatorUtils.property(thatLocator, "objectType", rhsObjectType), lhsObjectType, rhsObjectType)) {
                return false;
            }
        }
        {
            String lhsPackageName;
            lhsPackageName = this.getPackageName();
            String rhsPackageName;
            rhsPackageName = that.getPackageName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "packageName", lhsPackageName), LocatorUtils.property(thatLocator, "packageName", rhsPackageName), lhsPackageName, rhsPackageName)) {
                return false;
            }
        }
        {
            String lhsApplicationName;
            lhsApplicationName = this.getApplicationName();
            String rhsApplicationName;
            rhsApplicationName = that.getApplicationName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "applicationName", lhsApplicationName), LocatorUtils.property(thatLocator, "applicationName", rhsApplicationName), lhsApplicationName, rhsApplicationName)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof ModelAttributeObject) {
            final ModelAttributeObject copy = ((ModelAttributeObject) draftCopy);
            if (this.objectType!= null) {
                String sourceObjectType;
                sourceObjectType = this.getObjectType();
                String copyObjectType = ((String) strategy.copy(LocatorUtils.property(locator, "objectType", sourceObjectType), sourceObjectType));
                copy.setObjectType(copyObjectType);
            } else {
                copy.objectType = null;
            }
            if (this.packageName!= null) {
                String sourcePackageName;
                sourcePackageName = this.getPackageName();
                String copyPackageName = ((String) strategy.copy(LocatorUtils.property(locator, "packageName", sourcePackageName), sourcePackageName));
                copy.setPackageName(copyPackageName);
            } else {
                copy.packageName = null;
            }
            if (this.applicationName!= null) {
                String sourceApplicationName;
                sourceApplicationName = this.getApplicationName();
                String copyApplicationName = ((String) strategy.copy(LocatorUtils.property(locator, "applicationName", sourceApplicationName), sourceApplicationName));
                copy.setApplicationName(copyApplicationName);
            } else {
                copy.applicationName = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ModelAttributeObject();
    }
    
//--simple--preserve
    
    private transient com.mda.engine.models.businessObjectModel.ObjectType bom;
    
    @Override
    public String getClassName()
    {
    	String className = "";

    	if (bom.getApplication().getEntitiesProjectNamespace() != null)
    	{
    		className = bom.getApplication().getEntitiesProjectNamespace() + "." + bom.getPackageName() + ".";
    	}
    	className += bom.getName();
    	if (isIsList())
    	{
    		return String.format("PagedList<%s>", className);
    	}
    	else
    	{
    		return className;
    	}

    }

    @Override
    public String getDataContractClassName()
    {
    	String className = "";
    	if (bom.getApplication().getEntitiesProjectNamespace() != null)
    	{
    		className = bom.getApplication().getServiceContractsProjectNamespace() + "." + bom.getPackageName() + ".";
    	}
    	className += (bom.getName() + "DataContract");
    	if (isIsList())
    	{
    		return String.format("CustomListDataContract<%s>", className);
    	}
    	else
    	{
    		return className;
    	}
    }
    
	public void setBom(com.mda.engine.models.businessObjectModel.ObjectType bom) {
		this.bom = bom;
	}

	public com.mda.engine.models.businessObjectModel.ObjectType getBom() {
		return bom;
	}
    
//--simple--preserve

}
