
package com.mda.engine.models.common;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for InheritanceDefinition complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InheritanceDefinition">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Base" type="{http://www.mdaengine.com/mdaengine/models/common}BaseTypeChoice" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InheritanceDefinition", propOrder = {
    "base"
})
public class InheritanceDefinition
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Base")
    protected BaseTypeChoice base;

    /**
     * Gets the value of the base property.
     * 
     * @return
     *     possible object is
     *     {@link BaseTypeChoice }
     *     
     */
    public BaseTypeChoice getBase() {
        return base;
    }

    /**
     * Sets the value of the base property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseTypeChoice }
     *     
     */
    public void setBase(BaseTypeChoice value) {
        this.base = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof InheritanceDefinition)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final InheritanceDefinition that = ((InheritanceDefinition) object);
        {
            BaseTypeChoice lhsBase;
            lhsBase = this.getBase();
            BaseTypeChoice rhsBase;
            rhsBase = that.getBase();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "base", lhsBase), LocatorUtils.property(thatLocator, "base", rhsBase), lhsBase, rhsBase)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof InheritanceDefinition) {
            final InheritanceDefinition copy = ((InheritanceDefinition) draftCopy);
            if (this.base!= null) {
                BaseTypeChoice sourceBase;
                sourceBase = this.getBase();
                BaseTypeChoice copyBase = ((BaseTypeChoice) strategy.copy(LocatorUtils.property(locator, "base", sourceBase), sourceBase));
                copy.setBase(copyBase);
            } else {
                copy.base = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new InheritanceDefinition();
    }

}
