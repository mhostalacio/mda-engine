
package com.mda.engine.models.common;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for DecimalPrimitiveType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DecimalPrimitiveType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/common}PrimitiveTypeBase">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="Precision" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Scale" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DecimalPrimitiveType", propOrder = {
    "precision",
    "scale"
})
public class DecimalPrimitiveType
    extends PrimitiveTypeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Precision")
    protected Integer precision;
    @XmlElement(name = "Scale")
    protected Integer scale;

    /**
     * Gets the value of the precision property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPrecision() {
        return precision;
    }

    /**
     * Sets the value of the precision property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPrecision(Integer value) {
        this.precision = value;
    }

    /**
     * Gets the value of the scale property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getScale() {
        return scale;
    }

    /**
     * Sets the value of the scale property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setScale(Integer value) {
        this.scale = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof DecimalPrimitiveType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final DecimalPrimitiveType that = ((DecimalPrimitiveType) object);
        {
            Integer lhsPrecision;
            lhsPrecision = this.getPrecision();
            Integer rhsPrecision;
            rhsPrecision = that.getPrecision();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "precision", lhsPrecision), LocatorUtils.property(thatLocator, "precision", rhsPrecision), lhsPrecision, rhsPrecision)) {
                return false;
            }
        }
        {
            Integer lhsScale;
            lhsScale = this.getScale();
            Integer rhsScale;
            rhsScale = that.getScale();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "scale", lhsScale), LocatorUtils.property(thatLocator, "scale", rhsScale), lhsScale, rhsScale)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof DecimalPrimitiveType) {
            final DecimalPrimitiveType copy = ((DecimalPrimitiveType) draftCopy);
            if (this.precision!= null) {
                Integer sourcePrecision;
                sourcePrecision = this.getPrecision();
                Integer copyPrecision = ((Integer) strategy.copy(LocatorUtils.property(locator, "precision", sourcePrecision), sourcePrecision));
                copy.setPrecision(copyPrecision);
            } else {
                copy.precision = null;
            }
            if (this.scale!= null) {
                Integer sourceScale;
                sourceScale = this.getScale();
                Integer copyScale = ((Integer) strategy.copy(LocatorUtils.property(locator, "scale", sourceScale), sourceScale));
                copy.setScale(copyScale);
            } else {
                copy.scale = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new DecimalPrimitiveType();
    }

}
