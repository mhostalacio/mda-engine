
package com.mda.engine.models.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ModelAttributeCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ModelAttributeCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded" minOccurs="0">
 *         &lt;element name="Text" type="{http://www.mdaengine.com/mdaengine/models/common}ModelAttributeText"/>
 *         &lt;element name="Boolean" type="{http://www.mdaengine.com/mdaengine/models/common}ModelAttributeBoolean"/>
 *         &lt;element name="Decimal" type="{http://www.mdaengine.com/mdaengine/models/common}ModelAttributeDecimal"/>
 *         &lt;element name="Long" type="{http://www.mdaengine.com/mdaengine/models/common}ModelAttributeLong"/>
 *         &lt;element name="Int" type="{http://www.mdaengine.com/mdaengine/models/common}ModelAttributeInt"/>
 *         &lt;element name="Object" type="{http://www.mdaengine.com/mdaengine/models/common}ModelAttributeObject"/>
 *         &lt;element name="Enum" type="{http://www.mdaengine.com/mdaengine/models/common}ModelAttributeEnum"/>
 *         &lt;element name="DateTime" type="{http://www.mdaengine.com/mdaengine/models/common}ModelAttributeDateTime"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ModelAttributeCollection", propOrder = {
    "attributeList"
})
public class ModelAttributeCollection
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElements({
        @XmlElement(name = "Enum", type = ModelAttributeEnum.class),
        @XmlElement(name = "Long", type = ModelAttributeLong.class),
        @XmlElement(name = "Int", type = ModelAttributeInt.class),
        @XmlElement(name = "DateTime", type = ModelAttributeDateTime.class),
        @XmlElement(name = "Boolean", type = ModelAttributeBoolean.class),
        @XmlElement(name = "Text", type = ModelAttributeText.class),
        @XmlElement(name = "Decimal", type = ModelAttributeDecimal.class),
        @XmlElement(name = "Object", type = ModelAttributeObject.class)
    })
    protected List<ModelAttribute> attributeList;

    /**
     * Gets the value of the attributeList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the attributeList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttributeList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ModelAttributeEnum }
     * {@link ModelAttributeLong }
     * {@link ModelAttributeInt }
     * {@link ModelAttributeDateTime }
     * {@link ModelAttributeBoolean }
     * {@link ModelAttributeText }
     * {@link ModelAttributeDecimal }
     * {@link ModelAttributeObject }
     * 
     * 
     */
    public List<ModelAttribute> getAttributeList() {
        if (attributeList == null) {
            attributeList = new ArrayList<ModelAttribute>();
        }
        return this.attributeList;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ModelAttributeCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ModelAttributeCollection that = ((ModelAttributeCollection) object);
        {
            List<ModelAttribute> lhsAttributeList;
            lhsAttributeList = this.getAttributeList();
            List<ModelAttribute> rhsAttributeList;
            rhsAttributeList = that.getAttributeList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "attributeList", lhsAttributeList), LocatorUtils.property(thatLocator, "attributeList", rhsAttributeList), lhsAttributeList, rhsAttributeList)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof ModelAttributeCollection) {
            final ModelAttributeCollection copy = ((ModelAttributeCollection) draftCopy);
            if ((this.attributeList!= null)&&(!this.attributeList.isEmpty())) {
                List<ModelAttribute> sourceAttributeList;
                sourceAttributeList = this.getAttributeList();
                @SuppressWarnings("unchecked")
                List<ModelAttribute> copyAttributeList = ((List<ModelAttribute> ) strategy.copy(LocatorUtils.property(locator, "attributeList", sourceAttributeList), sourceAttributeList));
                copy.attributeList = null;
                List<ModelAttribute> uniqueAttributeListl = copy.getAttributeList();
                uniqueAttributeListl.addAll(copyAttributeList);
            } else {
                copy.attributeList = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ModelAttributeCollection();
    }

}
