
package com.mda.engine.models.common;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for LiteralType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LiteralType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/common}TypeBase">
 *       &lt;attribute name="TypeFullName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LiteralType")
public class LiteralType
    extends TypeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "TypeFullName", required = true)
    protected String typeFullName;

    /**
     * Gets the value of the typeFullName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeFullName() {
        return typeFullName;
    }

    /**
     * Sets the value of the typeFullName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeFullName(String value) {
        this.typeFullName = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof LiteralType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final LiteralType that = ((LiteralType) object);
        {
            String lhsTypeFullName;
            lhsTypeFullName = this.getTypeFullName();
            String rhsTypeFullName;
            rhsTypeFullName = that.getTypeFullName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "typeFullName", lhsTypeFullName), LocatorUtils.property(thatLocator, "typeFullName", rhsTypeFullName), lhsTypeFullName, rhsTypeFullName)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof LiteralType) {
            final LiteralType copy = ((LiteralType) draftCopy);
            if (this.typeFullName!= null) {
                String sourceTypeFullName;
                sourceTypeFullName = this.getTypeFullName();
                String copyTypeFullName = ((String) strategy.copy(LocatorUtils.property(locator, "typeFullName", sourceTypeFullName), sourceTypeFullName));
                copy.setTypeFullName(copyTypeFullName);
            } else {
                copy.typeFullName = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new LiteralType();
    }

}
