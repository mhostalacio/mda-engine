
package com.mda.engine.models.common;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ModelAttribute complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ModelAttribute">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="Name" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="Nullable" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *       &lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="IsList" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ModelAttribute")
@XmlSeeAlso({
    ModelAttributeText.class,
    ModelAttributeDecimal.class,
    ModelAttributeObject.class,
    ModelAttributeBoolean.class,
    ModelAttributeLong.class,
    ModelAttributeInt.class,
    ModelAttributeEnum.class,
    ModelAttributeDateTime.class
})
public abstract class ModelAttribute
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "Name", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String name;
    @XmlAttribute(name = "Nullable")
    protected Boolean nullable;
    @XmlAttribute(name = "Description")
    protected String description;
    @XmlAttribute(name = "IsList")
    protected Boolean isList;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the nullable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isNullable() {
        if (nullable == null) {
            return true;
        } else {
            return nullable;
        }
    }

    /**
     * Sets the value of the nullable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNullable(Boolean value) {
        this.nullable = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the isList property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isIsList() {
        if (isList == null) {
            return false;
        } else {
            return isList;
        }
    }

    /**
     * Sets the value of the isList property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsList(Boolean value) {
        this.isList = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ModelAttribute)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ModelAttribute that = ((ModelAttribute) object);
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            boolean lhsNullable;
            lhsNullable = this.isNullable();
            boolean rhsNullable;
            rhsNullable = that.isNullable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "nullable", lhsNullable), LocatorUtils.property(thatLocator, "nullable", rhsNullable), lhsNullable, rhsNullable)) {
                return false;
            }
        }
        {
            String lhsDescription;
            lhsDescription = this.getDescription();
            String rhsDescription;
            rhsDescription = that.getDescription();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "description", lhsDescription), LocatorUtils.property(thatLocator, "description", rhsDescription), lhsDescription, rhsDescription)) {
                return false;
            }
        }
        {
            boolean lhsIsList;
            lhsIsList = this.isIsList();
            boolean rhsIsList;
            rhsIsList = that.isIsList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "isList", lhsIsList), LocatorUtils.property(thatLocator, "isList", rhsIsList), lhsIsList, rhsIsList)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        if (null == target) {
            throw new IllegalArgumentException("Target argument must not be null for abstract copyable classes.");
        }
        if (target instanceof ModelAttribute) {
            final ModelAttribute copy = ((ModelAttribute) target);
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.nullable!= null) {
                boolean sourceNullable;
                sourceNullable = this.isNullable();
                boolean copyNullable = strategy.copy(LocatorUtils.property(locator, "nullable", sourceNullable), sourceNullable);
                copy.setNullable(copyNullable);
            } else {
                copy.nullable = null;
            }
            if (this.description!= null) {
                String sourceDescription;
                sourceDescription = this.getDescription();
                String copyDescription = ((String) strategy.copy(LocatorUtils.property(locator, "description", sourceDescription), sourceDescription));
                copy.setDescription(copyDescription);
            } else {
                copy.description = null;
            }
            if (this.isList!= null) {
                boolean sourceIsList;
                sourceIsList = this.isIsList();
                boolean copyIsList = strategy.copy(LocatorUtils.property(locator, "isList", sourceIsList), sourceIsList);
                copy.setIsList(copyIsList);
            } else {
                copy.isList = null;
            }
        }
        return target;
    }
    
//--simple--preserve
    
    public String getClassName()
    {
    	return null;
    }
    
    public String getDataContractClassName() {
    	return null;
	}
    
    private transient java.util.ArrayList<com.mda.engine.models.mvc.MVCViewElement> elementsBinded2Me = new java.util.ArrayList<com.mda.engine.models.mvc.MVCViewElement>();
    
    public java.util.ArrayList<com.mda.engine.models.mvc.MVCViewElement> GetElementsBinded2Me()
    {
    	return elementsBinded2Me;
    }
    
//--simple--preserve

}
