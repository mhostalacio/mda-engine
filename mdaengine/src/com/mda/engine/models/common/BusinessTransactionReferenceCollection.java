
package com.mda.engine.models.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for BusinessTransactionReferenceCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BusinessTransactionReferenceCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Transaction" type="{http://www.mdaengine.com/mdaengine/models/common}BusinessTransactionReference" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BusinessTransactionReferenceCollection", propOrder = {
    "transaction"
})
public class BusinessTransactionReferenceCollection
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Transaction")
    protected List<BusinessTransactionReference> transaction;

    /**
     * Gets the value of the transaction property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transaction property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransaction().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BusinessTransactionReference }
     * 
     * 
     */
    public List<BusinessTransactionReference> getTransaction() {
        if (transaction == null) {
            transaction = new ArrayList<BusinessTransactionReference>();
        }
        return this.transaction;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof BusinessTransactionReferenceCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final BusinessTransactionReferenceCollection that = ((BusinessTransactionReferenceCollection) object);
        {
            List<BusinessTransactionReference> lhsTransaction;
            lhsTransaction = this.getTransaction();
            List<BusinessTransactionReference> rhsTransaction;
            rhsTransaction = that.getTransaction();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "transaction", lhsTransaction), LocatorUtils.property(thatLocator, "transaction", rhsTransaction), lhsTransaction, rhsTransaction)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof BusinessTransactionReferenceCollection) {
            final BusinessTransactionReferenceCollection copy = ((BusinessTransactionReferenceCollection) draftCopy);
            if ((this.transaction!= null)&&(!this.transaction.isEmpty())) {
                List<BusinessTransactionReference> sourceTransaction;
                sourceTransaction = this.getTransaction();
                @SuppressWarnings("unchecked")
                List<BusinessTransactionReference> copyTransaction = ((List<BusinessTransactionReference> ) strategy.copy(LocatorUtils.property(locator, "transaction", sourceTransaction), sourceTransaction));
                copy.transaction = null;
                List<BusinessTransactionReference> uniqueTransactionl = copy.getTransaction();
                uniqueTransactionl.addAll(copyTransaction);
            } else {
                copy.transaction = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new BusinessTransactionReferenceCollection();
    }

}
