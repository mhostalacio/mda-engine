
package com.mda.engine.models.common;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCInstanceReference complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCInstanceReference">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="ApplicationName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="PackageName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCInstanceReference")
public class MVCInstanceReference
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "ApplicationName", required = true)
    protected String applicationName;
    @XmlAttribute(name = "PackageName", required = true)
    protected String packageName;
    @XmlAttribute(name = "Name", required = true)
    protected String name;

    /**
     * Gets the value of the applicationName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicationName() {
        return applicationName;
    }

    /**
     * Sets the value of the applicationName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicationName(String value) {
        this.applicationName = value;
    }

    /**
     * Gets the value of the packageName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackageName() {
        return packageName;
    }

    /**
     * Sets the value of the packageName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackageName(String value) {
        this.packageName = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCInstanceReference)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final MVCInstanceReference that = ((MVCInstanceReference) object);
        {
            String lhsApplicationName;
            lhsApplicationName = this.getApplicationName();
            String rhsApplicationName;
            rhsApplicationName = that.getApplicationName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "applicationName", lhsApplicationName), LocatorUtils.property(thatLocator, "applicationName", rhsApplicationName), lhsApplicationName, rhsApplicationName)) {
                return false;
            }
        }
        {
            String lhsPackageName;
            lhsPackageName = this.getPackageName();
            String rhsPackageName;
            rhsPackageName = that.getPackageName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "packageName", lhsPackageName), LocatorUtils.property(thatLocator, "packageName", rhsPackageName), lhsPackageName, rhsPackageName)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof MVCInstanceReference) {
            final MVCInstanceReference copy = ((MVCInstanceReference) draftCopy);
            if (this.applicationName!= null) {
                String sourceApplicationName;
                sourceApplicationName = this.getApplicationName();
                String copyApplicationName = ((String) strategy.copy(LocatorUtils.property(locator, "applicationName", sourceApplicationName), sourceApplicationName));
                copy.setApplicationName(copyApplicationName);
            } else {
                copy.applicationName = null;
            }
            if (this.packageName!= null) {
                String sourcePackageName;
                sourcePackageName = this.getPackageName();
                String copyPackageName = ((String) strategy.copy(LocatorUtils.property(locator, "packageName", sourcePackageName), sourcePackageName));
                copy.setPackageName(copyPackageName);
            } else {
                copy.packageName = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCInstanceReference();
    }

}
