
package com.mda.engine.models.common;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCModelType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCModelType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/common}TypeBase">
 *       &lt;attribute name="ModelTypeName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCModelType")
public class MVCModelType
    extends TypeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "ModelTypeName", required = true)
    protected String modelTypeName;

    /**
     * Gets the value of the modelTypeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelTypeName() {
        return modelTypeName;
    }

    /**
     * Sets the value of the modelTypeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelTypeName(String value) {
        this.modelTypeName = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCModelType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCModelType that = ((MVCModelType) object);
        {
            String lhsModelTypeName;
            lhsModelTypeName = this.getModelTypeName();
            String rhsModelTypeName;
            rhsModelTypeName = that.getModelTypeName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "modelTypeName", lhsModelTypeName), LocatorUtils.property(thatLocator, "modelTypeName", rhsModelTypeName), lhsModelTypeName, rhsModelTypeName)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCModelType) {
            final MVCModelType copy = ((MVCModelType) draftCopy);
            if (this.modelTypeName!= null) {
                String sourceModelTypeName;
                sourceModelTypeName = this.getModelTypeName();
                String copyModelTypeName = ((String) strategy.copy(LocatorUtils.property(locator, "modelTypeName", sourceModelTypeName), sourceModelTypeName));
                copy.setModelTypeName(copyModelTypeName);
            } else {
                copy.modelTypeName = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCModelType();
    }

}
