
package com.mda.engine.models.common;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for DictionaryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DictionaryType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/common}TypeBase">
 *       &lt;sequence>
 *         &lt;element name="Key" type="{http://www.mdaengine.com/mdaengine/models/common}ModelAttributeCollection"/>
 *         &lt;element name="Value" type="{http://www.mdaengine.com/mdaengine/models/common}ModelAttributeCollection"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DictionaryType", propOrder = {
    "key",
    "value"
})
public class DictionaryType
    extends TypeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Key", required = true)
    protected ModelAttributeCollection key;
    @XmlElement(name = "Value", required = true)
    protected ModelAttributeCollection value;

    /**
     * Gets the value of the key property.
     * 
     * @return
     *     possible object is
     *     {@link ModelAttributeCollection }
     *     
     */
    public ModelAttributeCollection getKey() {
        return key;
    }

    /**
     * Sets the value of the key property.
     * 
     * @param value
     *     allowed object is
     *     {@link ModelAttributeCollection }
     *     
     */
    public void setKey(ModelAttributeCollection value) {
        this.key = value;
    }

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link ModelAttributeCollection }
     *     
     */
    public ModelAttributeCollection getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link ModelAttributeCollection }
     *     
     */
    public void setValue(ModelAttributeCollection value) {
        this.value = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof DictionaryType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final DictionaryType that = ((DictionaryType) object);
        {
            ModelAttributeCollection lhsKey;
            lhsKey = this.getKey();
            ModelAttributeCollection rhsKey;
            rhsKey = that.getKey();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "key", lhsKey), LocatorUtils.property(thatLocator, "key", rhsKey), lhsKey, rhsKey)) {
                return false;
            }
        }
        {
            ModelAttributeCollection lhsValue;
            lhsValue = this.getValue();
            ModelAttributeCollection rhsValue;
            rhsValue = that.getValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "value", lhsValue), LocatorUtils.property(thatLocator, "value", rhsValue), lhsValue, rhsValue)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof DictionaryType) {
            final DictionaryType copy = ((DictionaryType) draftCopy);
            if (this.key!= null) {
                ModelAttributeCollection sourceKey;
                sourceKey = this.getKey();
                ModelAttributeCollection copyKey = ((ModelAttributeCollection) strategy.copy(LocatorUtils.property(locator, "key", sourceKey), sourceKey));
                copy.setKey(copyKey);
            } else {
                copy.key = null;
            }
            if (this.value!= null) {
                ModelAttributeCollection sourceValue;
                sourceValue = this.getValue();
                ModelAttributeCollection copyValue = ((ModelAttributeCollection) strategy.copy(LocatorUtils.property(locator, "value", sourceValue), sourceValue));
                copy.setValue(copyValue);
            } else {
                copy.value = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new DictionaryType();
    }

}
