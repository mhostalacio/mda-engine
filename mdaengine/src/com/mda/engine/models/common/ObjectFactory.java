
package com.mda.engine.models.common;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.mda.engine.models.common package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.mda.engine.models.common
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GuidPrimitiveType }
     * 
     */
    public GuidPrimitiveType createGuidPrimitiveType() {
        return new GuidPrimitiveType();
    }

    /**
     * Create an instance of {@link BOMIdType }
     * 
     */
    public BOMIdType createBOMIdType() {
        return new BOMIdType();
    }

    /**
     * Create an instance of {@link StringKeyValuePair }
     * 
     */
    public StringKeyValuePair createStringKeyValuePair() {
        return new StringKeyValuePair();
    }

    /**
     * Create an instance of {@link ModelAttributeBoolean }
     * 
     */
    public ModelAttributeBoolean createModelAttributeBoolean() {
        return new ModelAttributeBoolean();
    }

    /**
     * Create an instance of {@link BOMSelectType }
     * 
     */
    public BOMSelectType createBOMSelectType() {
        return new BOMSelectType();
    }

    /**
     * Create an instance of {@link TypeBase }
     * 
     */
    public TypeBase createTypeBase() {
        return new TypeBase();
    }

    /**
     * Create an instance of {@link ModelAttributeDateTime }
     * 
     */
    public ModelAttributeDateTime createModelAttributeDateTime() {
        return new ModelAttributeDateTime();
    }

    /**
     * Create an instance of {@link DateTimePrimitiveType }
     * 
     */
    public DateTimePrimitiveType createDateTimePrimitiveType() {
        return new DateTimePrimitiveType();
    }

    /**
     * Create an instance of {@link DatePrimitiveType }
     * 
     */
    public DatePrimitiveType createDatePrimitiveType() {
        return new DatePrimitiveType();
    }

    /**
     * Create an instance of {@link MVCInstanceReferenceCollection }
     * 
     */
    public MVCInstanceReferenceCollection createMVCInstanceReferenceCollection() {
        return new MVCInstanceReferenceCollection();
    }

    /**
     * Create an instance of {@link InterfaceReferenceType }
     * 
     */
    public InterfaceReferenceType createInterfaceReferenceType() {
        return new InterfaceReferenceType();
    }

    /**
     * Create an instance of {@link ModelAttributeEnum }
     * 
     */
    public ModelAttributeEnum createModelAttributeEnum() {
        return new ModelAttributeEnum();
    }

    /**
     * Create an instance of {@link StringPrimitiveType }
     * 
     */
    public StringPrimitiveType createStringPrimitiveType() {
        return new StringPrimitiveType();
    }

    /**
     * Create an instance of {@link ModelAttributeInt }
     * 
     */
    public ModelAttributeInt createModelAttributeInt() {
        return new ModelAttributeInt();
    }

    /**
     * Create an instance of {@link LovReferenceType }
     * 
     */
    public LovReferenceType createLovReferenceType() {
        return new LovReferenceType();
    }

    /**
     * Create an instance of {@link ListTableType }
     * 
     */
    public ListTableType createListTableType() {
        return new ListTableType();
    }

    /**
     * Create an instance of {@link LongIntPair }
     * 
     */
    public LongIntPair createLongIntPair() {
        return new LongIntPair();
    }

    /**
     * Create an instance of {@link OutputTransactionExposedAttribute }
     * 
     */
    public OutputTransactionExposedAttribute createOutputTransactionExposedAttribute() {
        return new OutputTransactionExposedAttribute();
    }

    /**
     * Create an instance of {@link ShortPrimitiveType }
     * 
     */
    public ShortPrimitiveType createShortPrimitiveType() {
        return new ShortPrimitiveType();
    }

    /**
     * Create an instance of {@link DataContractReferenceType }
     * 
     */
    public DataContractReferenceType createDataContractReferenceType() {
        return new DataContractReferenceType();
    }

    /**
     * Create an instance of {@link BOMAttributeReferenceType }
     * 
     */
    public BOMAttributeReferenceType createBOMAttributeReferenceType() {
        return new BOMAttributeReferenceType();
    }

    /**
     * Create an instance of {@link InheritanceDefinition }
     * 
     */
    public InheritanceDefinition createInheritanceDefinition() {
        return new InheritanceDefinition();
    }

    /**
     * Create an instance of {@link LongKeyValuePairType }
     * 
     */
    public LongKeyValuePairType createLongKeyValuePairType() {
        return new LongKeyValuePairType();
    }

    /**
     * Create an instance of {@link TransactionExposedAttributeCollection }
     * 
     */
    public TransactionExposedAttributeCollection createTransactionExposedAttributeCollection() {
        return new TransactionExposedAttributeCollection();
    }

    /**
     * Create an instance of {@link BOMReferenceTypeCollection }
     * 
     */
    public BOMReferenceTypeCollection createBOMReferenceTypeCollection() {
        return new BOMReferenceTypeCollection();
    }

    /**
     * Create an instance of {@link BusinessTransactionReferenceCollection }
     * 
     */
    public BusinessTransactionReferenceCollection createBusinessTransactionReferenceCollection() {
        return new BusinessTransactionReferenceCollection();
    }

    /**
     * Create an instance of {@link ModelAttributeDecimal }
     * 
     */
    public ModelAttributeDecimal createModelAttributeDecimal() {
        return new ModelAttributeDecimal();
    }

    /**
     * Create an instance of {@link BusinessTransactionReference }
     * 
     */
    public BusinessTransactionReference createBusinessTransactionReference() {
        return new BusinessTransactionReference();
    }

    /**
     * Create an instance of {@link DoublePrimitiveType }
     * 
     */
    public DoublePrimitiveType createDoublePrimitiveType() {
        return new DoublePrimitiveType();
    }

    /**
     * Create an instance of {@link SinglePrimitiveType }
     * 
     */
    public SinglePrimitiveType createSinglePrimitiveType() {
        return new SinglePrimitiveType();
    }

    /**
     * Create an instance of {@link FloatPrimitiveType }
     * 
     */
    public FloatPrimitiveType createFloatPrimitiveType() {
        return new FloatPrimitiveType();
    }

    /**
     * Create an instance of {@link MVCInstanceReference }
     * 
     */
    public MVCInstanceReference createMVCInstanceReference() {
        return new MVCInstanceReference();
    }

    /**
     * Create an instance of {@link QueryCustomResultSetType }
     * 
     */
    public QueryCustomResultSetType createQueryCustomResultSetType() {
        return new QueryCustomResultSetType();
    }

    /**
     * Create an instance of {@link PrimitiveMultiLanguageTypeBase }
     * 
     */
    public PrimitiveMultiLanguageTypeBase createPrimitiveMultiLanguageTypeBase() {
        return new PrimitiveMultiLanguageTypeBase();
    }

    /**
     * Create an instance of {@link BOMCodeType }
     * 
     */
    public BOMCodeType createBOMCodeType() {
        return new BOMCodeType();
    }

    /**
     * Create an instance of {@link BaseTypeChoice }
     * 
     */
    public BaseTypeChoice createBaseTypeChoice() {
        return new BaseTypeChoice();
    }

    /**
     * Create an instance of {@link AggregationListType }
     * 
     */
    public AggregationListType createAggregationListType() {
        return new AggregationListType();
    }

    /**
     * Create an instance of {@link DecimalPrimitiveType }
     * 
     */
    public DecimalPrimitiveType createDecimalPrimitiveType() {
        return new DecimalPrimitiveType();
    }

    /**
     * Create an instance of {@link ModelAttributeLong }
     * 
     */
    public ModelAttributeLong createModelAttributeLong() {
        return new ModelAttributeLong();
    }

    /**
     * Create an instance of {@link LongPrimitiveType }
     * 
     */
    public LongPrimitiveType createLongPrimitiveType() {
        return new LongPrimitiveType();
    }

    /**
     * Create an instance of {@link QueryParametersReferenceType }
     * 
     */
    public QueryParametersReferenceType createQueryParametersReferenceType() {
        return new QueryParametersReferenceType();
    }

    /**
     * Create an instance of {@link ModelAttributeObject }
     * 
     */
    public ModelAttributeObject createModelAttributeObject() {
        return new ModelAttributeObject();
    }

    /**
     * Create an instance of {@link InputTransactionExposedAttribute }
     * 
     */
    public InputTransactionExposedAttribute createInputTransactionExposedAttribute() {
        return new InputTransactionExposedAttribute();
    }

    /**
     * Create an instance of {@link ModelAttributeText }
     * 
     */
    public ModelAttributeText createModelAttributeText() {
        return new ModelAttributeText();
    }

    /**
     * Create an instance of {@link QueryReference }
     * 
     */
    public QueryReference createQueryReference() {
        return new QueryReference();
    }

    /**
     * Create an instance of {@link InterfaceReferenceTypeCollection }
     * 
     */
    public InterfaceReferenceTypeCollection createInterfaceReferenceTypeCollection() {
        return new InterfaceReferenceTypeCollection();
    }

    /**
     * Create an instance of {@link ListType }
     * 
     */
    public ListType createListType() {
        return new ListType();
    }

    /**
     * Create an instance of {@link BinaryType }
     * 
     */
    public BinaryType createBinaryType() {
        return new BinaryType();
    }

    /**
     * Create an instance of {@link IntPrimitiveType }
     * 
     */
    public IntPrimitiveType createIntPrimitiveType() {
        return new IntPrimitiveType();
    }

    /**
     * Create an instance of {@link TransactionExposedAttribute }
     * 
     */
    public TransactionExposedAttribute createTransactionExposedAttribute() {
        return new TransactionExposedAttribute();
    }

    /**
     * Create an instance of {@link BooleanPrimitiveType }
     * 
     */
    public BooleanPrimitiveType createBooleanPrimitiveType() {
        return new BooleanPrimitiveType();
    }

    /**
     * Create an instance of {@link DictionaryType }
     * 
     */
    public DictionaryType createDictionaryType() {
        return new DictionaryType();
    }

    /**
     * Create an instance of {@link MVCModelType }
     * 
     */
    public MVCModelType createMVCModelType() {
        return new MVCModelType();
    }

    /**
     * Create an instance of {@link BOMReferenceType }
     * 
     */
    public BOMReferenceType createBOMReferenceType() {
        return new BOMReferenceType();
    }

    /**
     * Create an instance of {@link ModelAttributeCollection }
     * 
     */
    public ModelAttributeCollection createModelAttributeCollection() {
        return new ModelAttributeCollection();
    }

    /**
     * Create an instance of {@link PrimitiveTypeBase }
     * 
     */
    public PrimitiveTypeBase createPrimitiveTypeBase() {
        return new PrimitiveTypeBase();
    }

    /**
     * Create an instance of {@link QueryResultSetReferenceType }
     * 
     */
    public QueryResultSetReferenceType createQueryResultSetReferenceType() {
        return new QueryResultSetReferenceType();
    }

    /**
     * Create an instance of {@link LiteralType }
     * 
     */
    public LiteralType createLiteralType() {
        return new LiteralType();
    }

}
