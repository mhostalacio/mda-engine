
package com.mda.engine.models.common;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ModelAttributeEnum complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ModelAttributeEnum">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/common}ModelAttribute">
 *       &lt;attribute name="EnumType" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ModelAttributeEnum")
public class ModelAttributeEnum
    extends ModelAttribute
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "EnumType", required = true)
    protected String enumType;

    /**
     * Gets the value of the enumType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnumType() {
        return enumType;
    }

    /**
     * Sets the value of the enumType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnumType(String value) {
        this.enumType = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ModelAttributeEnum)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final ModelAttributeEnum that = ((ModelAttributeEnum) object);
        {
            String lhsEnumType;
            lhsEnumType = this.getEnumType();
            String rhsEnumType;
            rhsEnumType = that.getEnumType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "enumType", lhsEnumType), LocatorUtils.property(thatLocator, "enumType", rhsEnumType), lhsEnumType, rhsEnumType)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof ModelAttributeEnum) {
            final ModelAttributeEnum copy = ((ModelAttributeEnum) draftCopy);
            if (this.enumType!= null) {
                String sourceEnumType;
                sourceEnumType = this.getEnumType();
                String copyEnumType = ((String) strategy.copy(LocatorUtils.property(locator, "enumType", sourceEnumType), sourceEnumType));
                copy.setEnumType(copyEnumType);
            } else {
                copy.enumType = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ModelAttributeEnum();
    }
    
//--simple--preserve
    

    @Override
    public String getClassName()
    {
    	if (isIsList())
    	{
    		return String.format("IList<%s>", this.getEnumType());
    	}
    	else
    	{
    		return this.getEnumType() + (this.isNullable() ? "?" : "");
    	}
    }

    @Override
    public String getDataContractClassName()
    {
    	if (isIsList())
    	{
    		return String.format("IList<%sDataContract>", this.getEnumType());
    	}
    	else
    	{
    		return this.getEnumType() + "DataContract" + (this.isNullable() ? "?" : "");
    	}
    }
    
   
    
//--simple--preserve

}
