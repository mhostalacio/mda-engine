
package com.mda.engine.models.common;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ListType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/common}TypeBase">
 *       &lt;sequence>
 *         &lt;element name="ItemType" type="{http://www.mdaengine.com/mdaengine/models/common}ModelAttributeCollection"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListType", propOrder = {
    "itemType"
})
public class ListType
    extends TypeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "ItemType", required = true)
    protected ModelAttributeCollection itemType;

    /**
     * Gets the value of the itemType property.
     * 
     * @return
     *     possible object is
     *     {@link ModelAttributeCollection }
     *     
     */
    public ModelAttributeCollection getItemType() {
        return itemType;
    }

    /**
     * Sets the value of the itemType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ModelAttributeCollection }
     *     
     */
    public void setItemType(ModelAttributeCollection value) {
        this.itemType = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ListType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final ListType that = ((ListType) object);
        {
            ModelAttributeCollection lhsItemType;
            lhsItemType = this.getItemType();
            ModelAttributeCollection rhsItemType;
            rhsItemType = that.getItemType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "itemType", lhsItemType), LocatorUtils.property(thatLocator, "itemType", rhsItemType), lhsItemType, rhsItemType)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof ListType) {
            final ListType copy = ((ListType) draftCopy);
            if (this.itemType!= null) {
                ModelAttributeCollection sourceItemType;
                sourceItemType = this.getItemType();
                ModelAttributeCollection copyItemType = ((ModelAttributeCollection) strategy.copy(LocatorUtils.property(locator, "itemType", sourceItemType), sourceItemType));
                copy.setItemType(copyItemType);
            } else {
                copy.itemType = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ListType();
    }

}
