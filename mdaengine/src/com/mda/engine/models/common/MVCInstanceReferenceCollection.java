
package com.mda.engine.models.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCInstanceReferenceCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCInstanceReferenceCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MVCInstance" type="{http://www.mdaengine.com/mdaengine/models/common}MVCInstanceReference" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCInstanceReferenceCollection", propOrder = {
    "mvcInstance"
})
public class MVCInstanceReferenceCollection
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "MVCInstance", required = true)
    protected List<MVCInstanceReference> mvcInstance;

    /**
     * Gets the value of the mvcInstance property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mvcInstance property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMVCInstance().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MVCInstanceReference }
     * 
     * 
     */
    public List<MVCInstanceReference> getMVCInstance() {
        if (mvcInstance == null) {
            mvcInstance = new ArrayList<MVCInstanceReference>();
        }
        return this.mvcInstance;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCInstanceReferenceCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final MVCInstanceReferenceCollection that = ((MVCInstanceReferenceCollection) object);
        {
            List<MVCInstanceReference> lhsMVCInstance;
            lhsMVCInstance = this.getMVCInstance();
            List<MVCInstanceReference> rhsMVCInstance;
            rhsMVCInstance = that.getMVCInstance();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "mvcInstance", lhsMVCInstance), LocatorUtils.property(thatLocator, "mvcInstance", rhsMVCInstance), lhsMVCInstance, rhsMVCInstance)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof MVCInstanceReferenceCollection) {
            final MVCInstanceReferenceCollection copy = ((MVCInstanceReferenceCollection) draftCopy);
            if ((this.mvcInstance!= null)&&(!this.mvcInstance.isEmpty())) {
                List<MVCInstanceReference> sourceMVCInstance;
                sourceMVCInstance = this.getMVCInstance();
                @SuppressWarnings("unchecked")
                List<MVCInstanceReference> copyMVCInstance = ((List<MVCInstanceReference> ) strategy.copy(LocatorUtils.property(locator, "mvcInstance", sourceMVCInstance), sourceMVCInstance));
                copy.mvcInstance = null;
                List<MVCInstanceReference> uniqueMVCInstancel = copy.getMVCInstance();
                uniqueMVCInstancel.addAll(copyMVCInstance);
            } else {
                copy.mvcInstance = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCInstanceReferenceCollection();
    }

}
