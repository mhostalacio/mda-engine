
package com.mda.engine.models.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for TransactionExposedAttributeCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransactionExposedAttributeCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="Input" type="{http://www.mdaengine.com/mdaengine/models/common}InputTransactionExposedAttribute" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Output" type="{http://www.mdaengine.com/mdaengine/models/common}OutputTransactionExposedAttribute" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionExposedAttributeCollection", propOrder = {
    "attributes"
})
public class TransactionExposedAttributeCollection
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElements({
        @XmlElement(name = "Input", type = InputTransactionExposedAttribute.class),
        @XmlElement(name = "Output", type = OutputTransactionExposedAttribute.class)
    })
    protected List<TransactionExposedAttribute> attributes;

    /**
     * Gets the value of the attributes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the attributes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttributes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InputTransactionExposedAttribute }
     * {@link OutputTransactionExposedAttribute }
     * 
     * 
     */
    public List<TransactionExposedAttribute> getAttributes() {
        if (attributes == null) {
            attributes = new ArrayList<TransactionExposedAttribute>();
        }
        return this.attributes;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof TransactionExposedAttributeCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final TransactionExposedAttributeCollection that = ((TransactionExposedAttributeCollection) object);
        {
            List<TransactionExposedAttribute> lhsAttributes;
            lhsAttributes = this.getAttributes();
            List<TransactionExposedAttribute> rhsAttributes;
            rhsAttributes = that.getAttributes();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "attributes", lhsAttributes), LocatorUtils.property(thatLocator, "attributes", rhsAttributes), lhsAttributes, rhsAttributes)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof TransactionExposedAttributeCollection) {
            final TransactionExposedAttributeCollection copy = ((TransactionExposedAttributeCollection) draftCopy);
            if ((this.attributes!= null)&&(!this.attributes.isEmpty())) {
                List<TransactionExposedAttribute> sourceAttributes;
                sourceAttributes = this.getAttributes();
                @SuppressWarnings("unchecked")
                List<TransactionExposedAttribute> copyAttributes = ((List<TransactionExposedAttribute> ) strategy.copy(LocatorUtils.property(locator, "attributes", sourceAttributes), sourceAttributes));
                copy.attributes = null;
                List<TransactionExposedAttribute> uniqueAttributesl = copy.getAttributes();
                uniqueAttributesl.addAll(copyAttributes);
            } else {
                copy.attributes = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new TransactionExposedAttributeCollection();
    }

}
