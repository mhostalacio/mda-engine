
package com.mda.engine.models.common;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for BOMReferenceType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BOMReferenceType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/common}ReferenceTypeBase">
 *       &lt;attribute name="AttributeGroupName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BOMReferenceType")
@XmlSeeAlso({
    BOMAttributeReferenceType.class
})
public class BOMReferenceType
    extends ReferenceTypeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "AttributeGroupName")
    protected String attributeGroupName;

    /**
     * Gets the value of the attributeGroupName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeGroupName() {
        return attributeGroupName;
    }

    /**
     * Sets the value of the attributeGroupName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeGroupName(String value) {
        this.attributeGroupName = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof BOMReferenceType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final BOMReferenceType that = ((BOMReferenceType) object);
        {
            String lhsAttributeGroupName;
            lhsAttributeGroupName = this.getAttributeGroupName();
            String rhsAttributeGroupName;
            rhsAttributeGroupName = that.getAttributeGroupName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "attributeGroupName", lhsAttributeGroupName), LocatorUtils.property(thatLocator, "attributeGroupName", rhsAttributeGroupName), lhsAttributeGroupName, rhsAttributeGroupName)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof BOMReferenceType) {
            final BOMReferenceType copy = ((BOMReferenceType) draftCopy);
            if (this.attributeGroupName!= null) {
                String sourceAttributeGroupName;
                sourceAttributeGroupName = this.getAttributeGroupName();
                String copyAttributeGroupName = ((String) strategy.copy(LocatorUtils.property(locator, "attributeGroupName", sourceAttributeGroupName), sourceAttributeGroupName));
                copy.setAttributeGroupName(copyAttributeGroupName);
            } else {
                copy.attributeGroupName = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new BOMReferenceType();
    }
    
//--simple--preserve
    
    
    public static BOMReferenceType createInstance(com.mda.engine.models.businessObjectModel.BaseType type) {

		return createInstance(type, null);
	}

	public static BOMReferenceType createInstance(com.mda.engine.models.businessObjectModel.BaseType type, String attributeGroupName) {

		BOMReferenceType nieRef = new BOMReferenceType();
		nieRef.setName(type.getName());
		nieRef.setAttributeGroupName(attributeGroupName);

		return nieRef;
	}

    
//--simple--preserve

}
