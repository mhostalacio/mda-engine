
package com.mda.engine.models.common;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for PrimitiveMultiLanguageTypeBase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PrimitiveMultiLanguageTypeBase">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/common}PrimitiveTypeBase">
 *       &lt;attribute name="IsMultiLanguage" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PrimitiveMultiLanguageTypeBase")
@XmlSeeAlso({
    StringPrimitiveType.class
})
public class PrimitiveMultiLanguageTypeBase
    extends PrimitiveTypeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "IsMultiLanguage")
    protected Boolean isMultiLanguage;

    /**
     * Gets the value of the isMultiLanguage property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isIsMultiLanguage() {
        if (isMultiLanguage == null) {
            return false;
        } else {
            return isMultiLanguage;
        }
    }

    /**
     * Sets the value of the isMultiLanguage property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsMultiLanguage(Boolean value) {
        this.isMultiLanguage = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof PrimitiveMultiLanguageTypeBase)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final PrimitiveMultiLanguageTypeBase that = ((PrimitiveMultiLanguageTypeBase) object);
        {
            boolean lhsIsMultiLanguage;
            lhsIsMultiLanguage = this.isIsMultiLanguage();
            boolean rhsIsMultiLanguage;
            rhsIsMultiLanguage = that.isIsMultiLanguage();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "isMultiLanguage", lhsIsMultiLanguage), LocatorUtils.property(thatLocator, "isMultiLanguage", rhsIsMultiLanguage), lhsIsMultiLanguage, rhsIsMultiLanguage)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof PrimitiveMultiLanguageTypeBase) {
            final PrimitiveMultiLanguageTypeBase copy = ((PrimitiveMultiLanguageTypeBase) draftCopy);
            if (this.isMultiLanguage!= null) {
                boolean sourceIsMultiLanguage;
                sourceIsMultiLanguage = this.isIsMultiLanguage();
                boolean copyIsMultiLanguage = strategy.copy(LocatorUtils.property(locator, "isMultiLanguage", sourceIsMultiLanguage), sourceIsMultiLanguage);
                copy.setIsMultiLanguage(copyIsMultiLanguage);
            } else {
                copy.isMultiLanguage = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new PrimitiveMultiLanguageTypeBase();
    }

}
