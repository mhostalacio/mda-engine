
package com.mda.engine.models.businessTransactionModel;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.mda.engine.models.businessTransactionModel package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _BusinessTransaction_QNAME = new QName("http://www.mdaengine.com/mdaengine/models/businessTransactionModel", "BusinessTransaction");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.mda.engine.models.businessTransactionModel
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link InitMVCBusinessTransaction }
     * 
     */
    public InitMVCBusinessTransaction createInitMVCBusinessTransaction() {
        return new InitMVCBusinessTransaction();
    }

    /**
     * Create an instance of {@link BusinessTransaction }
     * 
     */
    public BusinessTransaction createBusinessTransaction() {
        return new BusinessTransaction();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BusinessTransaction }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.mdaengine.com/mdaengine/models/businessTransactionModel", name = "BusinessTransaction")
    public JAXBElement<BusinessTransaction> createBusinessTransaction(BusinessTransaction value) {
        return new JAXBElement<BusinessTransaction>(_BusinessTransaction_QNAME, BusinessTransaction.class, null, value);
    }

}
