
package com.mda.engine.models.businessTransactionModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.models.common.BOMReferenceType;
import com.mda.engine.models.common.ModelAttributeCollection;
import com.mda.engine.models.common.ModelInPackageBase;
import com.mda.engine.models.common.TransactionExposedAttributeCollection;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for BusinessTransaction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BusinessTransaction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/common}ModelInPackageBase">
 *       &lt;sequence>
 *         &lt;element name="OwnedBy" type="{http://www.mdaengine.com/mdaengine/models/common}BOMReferenceType"/>
 *         &lt;element name="Inputs" type="{http://www.mdaengine.com/mdaengine/models/common}ModelAttributeCollection"/>
 *         &lt;element name="Outputs" type="{http://www.mdaengine.com/mdaengine/models/common}ModelAttributeCollection"/>
 *         &lt;element name="ExposedAttributes" type="{http://www.mdaengine.com/mdaengine/models/common}TransactionExposedAttributeCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Description" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BusinessTransaction", propOrder = {
    "ownedBy",
    "inputs",
    "outputs",
    "exposedAttributes"
})
@XmlSeeAlso({
    InitMVCBusinessTransaction.class
})
@XmlRootElement
public class BusinessTransaction
    extends ModelInPackageBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "OwnedBy", required = true)
    protected BOMReferenceType ownedBy;
    @XmlElement(name = "Inputs", required = true)
    protected ModelAttributeCollection inputs;
    @XmlElement(name = "Outputs", required = true)
    protected ModelAttributeCollection outputs;
    @XmlElement(name = "ExposedAttributes")
    protected TransactionExposedAttributeCollection exposedAttributes;
    @XmlAttribute(name = "Description", required = true)
    protected String description;

    /**
     * Gets the value of the ownedBy property.
     * 
     * @return
     *     possible object is
     *     {@link BOMReferenceType }
     *     
     */
    public BOMReferenceType getOwnedBy() {
        return ownedBy;
    }

    /**
     * Sets the value of the ownedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link BOMReferenceType }
     *     
     */
    public void setOwnedBy(BOMReferenceType value) {
        this.ownedBy = value;
    }

    /**
     * Gets the value of the inputs property.
     * 
     * @return
     *     possible object is
     *     {@link ModelAttributeCollection }
     *     
     */
    public ModelAttributeCollection getInputs() {
        return inputs;
    }

    /**
     * Sets the value of the inputs property.
     * 
     * @param value
     *     allowed object is
     *     {@link ModelAttributeCollection }
     *     
     */
    public void setInputs(ModelAttributeCollection value) {
        this.inputs = value;
    }

    /**
     * Gets the value of the outputs property.
     * 
     * @return
     *     possible object is
     *     {@link ModelAttributeCollection }
     *     
     */
    public ModelAttributeCollection getOutputs() {
        return outputs;
    }

    /**
     * Sets the value of the outputs property.
     * 
     * @param value
     *     allowed object is
     *     {@link ModelAttributeCollection }
     *     
     */
    public void setOutputs(ModelAttributeCollection value) {
        this.outputs = value;
    }

    /**
     * Gets the value of the exposedAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionExposedAttributeCollection }
     *     
     */
    public TransactionExposedAttributeCollection getExposedAttributes() {
        return exposedAttributes;
    }

    /**
     * Sets the value of the exposedAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionExposedAttributeCollection }
     *     
     */
    public void setExposedAttributes(TransactionExposedAttributeCollection value) {
        this.exposedAttributes = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof BusinessTransaction)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final BusinessTransaction that = ((BusinessTransaction) object);
        {
            BOMReferenceType lhsOwnedBy;
            lhsOwnedBy = this.getOwnedBy();
            BOMReferenceType rhsOwnedBy;
            rhsOwnedBy = that.getOwnedBy();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ownedBy", lhsOwnedBy), LocatorUtils.property(thatLocator, "ownedBy", rhsOwnedBy), lhsOwnedBy, rhsOwnedBy)) {
                return false;
            }
        }
        {
            ModelAttributeCollection lhsInputs;
            lhsInputs = this.getInputs();
            ModelAttributeCollection rhsInputs;
            rhsInputs = that.getInputs();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "inputs", lhsInputs), LocatorUtils.property(thatLocator, "inputs", rhsInputs), lhsInputs, rhsInputs)) {
                return false;
            }
        }
        {
            ModelAttributeCollection lhsOutputs;
            lhsOutputs = this.getOutputs();
            ModelAttributeCollection rhsOutputs;
            rhsOutputs = that.getOutputs();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "outputs", lhsOutputs), LocatorUtils.property(thatLocator, "outputs", rhsOutputs), lhsOutputs, rhsOutputs)) {
                return false;
            }
        }
        {
            TransactionExposedAttributeCollection lhsExposedAttributes;
            lhsExposedAttributes = this.getExposedAttributes();
            TransactionExposedAttributeCollection rhsExposedAttributes;
            rhsExposedAttributes = that.getExposedAttributes();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "exposedAttributes", lhsExposedAttributes), LocatorUtils.property(thatLocator, "exposedAttributes", rhsExposedAttributes), lhsExposedAttributes, rhsExposedAttributes)) {
                return false;
            }
        }
        {
            String lhsDescription;
            lhsDescription = this.getDescription();
            String rhsDescription;
            rhsDescription = that.getDescription();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "description", lhsDescription), LocatorUtils.property(thatLocator, "description", rhsDescription), lhsDescription, rhsDescription)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof BusinessTransaction) {
            final BusinessTransaction copy = ((BusinessTransaction) draftCopy);
            if (this.ownedBy!= null) {
                BOMReferenceType sourceOwnedBy;
                sourceOwnedBy = this.getOwnedBy();
                BOMReferenceType copyOwnedBy = ((BOMReferenceType) strategy.copy(LocatorUtils.property(locator, "ownedBy", sourceOwnedBy), sourceOwnedBy));
                copy.setOwnedBy(copyOwnedBy);
            } else {
                copy.ownedBy = null;
            }
            if (this.inputs!= null) {
                ModelAttributeCollection sourceInputs;
                sourceInputs = this.getInputs();
                ModelAttributeCollection copyInputs = ((ModelAttributeCollection) strategy.copy(LocatorUtils.property(locator, "inputs", sourceInputs), sourceInputs));
                copy.setInputs(copyInputs);
            } else {
                copy.inputs = null;
            }
            if (this.outputs!= null) {
                ModelAttributeCollection sourceOutputs;
                sourceOutputs = this.getOutputs();
                ModelAttributeCollection copyOutputs = ((ModelAttributeCollection) strategy.copy(LocatorUtils.property(locator, "outputs", sourceOutputs), sourceOutputs));
                copy.setOutputs(copyOutputs);
            } else {
                copy.outputs = null;
            }
            if (this.exposedAttributes!= null) {
                TransactionExposedAttributeCollection sourceExposedAttributes;
                sourceExposedAttributes = this.getExposedAttributes();
                TransactionExposedAttributeCollection copyExposedAttributes = ((TransactionExposedAttributeCollection) strategy.copy(LocatorUtils.property(locator, "exposedAttributes", sourceExposedAttributes), sourceExposedAttributes));
                copy.setExposedAttributes(copyExposedAttributes);
            } else {
                copy.exposedAttributes = null;
            }
            if (this.description!= null) {
                String sourceDescription;
                sourceDescription = this.getDescription();
                String copyDescription = ((String) strategy.copy(LocatorUtils.property(locator, "description", sourceDescription), sourceDescription));
                copy.setDescription(copyDescription);
            } else {
                copy.description = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new BusinessTransaction();
    }
    
//--simple--preserve
    
    private transient String packageName;
    private transient com.mda.engine.models.configurationManagementModel.Application application;
    private transient com.mda.engine.models.queryModel.Query originatorQuery;
    private transient com.mda.engine.models.businessObjectModel.ObjectType owner;
    private transient String transactionId;
    
    public void setPackageName(String packageName) {
		this.packageName = com.mda.engine.utils.StringUtils.getPascalCase(packageName);
	}

	public String getPackageName() {
		return packageName;
	}

	public void setApplication(com.mda.engine.models.configurationManagementModel.Application application) {
		this.application = application;
	}

	public com.mda.engine.models.configurationManagementModel.Application getApplication() {
		return application;
	}

	public void setOriginatorQuery(com.mda.engine.models.queryModel.Query originatorQuery) {
		this.originatorQuery = originatorQuery;
	}

	public com.mda.engine.models.queryModel.Query getOriginatorQuery() {
		return originatorQuery;
	}

	public void setOwner(com.mda.engine.models.businessObjectModel.ObjectType owner) {
		this.owner = owner;
		owner.associateTransaction(this);
	}

	public com.mda.engine.models.businessObjectModel.ObjectType getOwner() {
		return owner;
	}
	
    public void setTransactionId(String transactionId)
    {
    	this.transactionId = transactionId;
    }
    
    public String getTransactionId()
    {
    	return this.transactionId;
    }
	
//--simple--preserve

}
