
package com.mda.engine.models.configurationManagementModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ArtifactCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArtifactCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransactionsMapping" type="{http://www.mdaengine.com/mdaengine/models/configurationManagementModel}TransactionsMapping" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArtifactCollection", propOrder = {
    "transactionsMapping"
})
public class ArtifactCollection
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "TransactionsMapping")
    protected TransactionsMapping transactionsMapping;

    /**
     * Gets the value of the transactionsMapping property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionsMapping }
     *     
     */
    public TransactionsMapping getTransactionsMapping() {
        return transactionsMapping;
    }

    /**
     * Sets the value of the transactionsMapping property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionsMapping }
     *     
     */
    public void setTransactionsMapping(TransactionsMapping value) {
        this.transactionsMapping = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ArtifactCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ArtifactCollection that = ((ArtifactCollection) object);
        {
            TransactionsMapping lhsTransactionsMapping;
            lhsTransactionsMapping = this.getTransactionsMapping();
            TransactionsMapping rhsTransactionsMapping;
            rhsTransactionsMapping = that.getTransactionsMapping();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "transactionsMapping", lhsTransactionsMapping), LocatorUtils.property(thatLocator, "transactionsMapping", rhsTransactionsMapping), lhsTransactionsMapping, rhsTransactionsMapping)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof ArtifactCollection) {
            final ArtifactCollection copy = ((ArtifactCollection) draftCopy);
            if (this.transactionsMapping!= null) {
                TransactionsMapping sourceTransactionsMapping;
                sourceTransactionsMapping = this.getTransactionsMapping();
                TransactionsMapping copyTransactionsMapping = ((TransactionsMapping) strategy.copy(LocatorUtils.property(locator, "transactionsMapping", sourceTransactionsMapping), sourceTransactionsMapping));
                copy.setTransactionsMapping(copyTransactionsMapping);
            } else {
                copy.transactionsMapping = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ArtifactCollection();
    }

}
