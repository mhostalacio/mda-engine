
package com.mda.engine.models.configurationManagementModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for Project complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Project">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Dependencies" type="{http://www.mdaengine.com/mdaengine/models/configurationManagementModel}ReferenceCollection" minOccurs="0"/>
 *         &lt;element name="Artifacts" type="{http://www.mdaengine.com/mdaengine/models/configurationManagementModel}ArtifactCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="AssemblyName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Namespace" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Description" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="RelativePath" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="HasEnvRides" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Project", propOrder = {
    "dependencies",
    "artifacts"
})
@XmlSeeAlso({
    WebApplicationProject.class,
    WindowsServiceProject.class,
    WebServiceHostProject.class,
    DataBaseLoaderProject.class,
    TestsProject.class,
    ConsoleApplicationProject.class,
    ClassLibraryProject.class
})
public class Project
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Dependencies")
    protected ReferenceCollection dependencies;
    @XmlElement(name = "Artifacts")
    protected ArtifactCollection artifacts;
    @XmlAttribute(name = "AssemblyName", required = true)
    protected String assemblyName;
    @XmlAttribute(name = "Namespace", required = true)
    protected String namespace;
    @XmlAttribute(name = "Description", required = true)
    protected String description;
    @XmlAttribute(name = "RelativePath", required = true)
    protected String relativePath;
    @XmlAttribute(name = "HasEnvRides", required = true)
    protected boolean hasEnvRides;

    /**
     * Gets the value of the dependencies property.
     * 
     * @return
     *     possible object is
     *     {@link ReferenceCollection }
     *     
     */
    public ReferenceCollection getDependencies() {
        return dependencies;
    }

    /**
     * Sets the value of the dependencies property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferenceCollection }
     *     
     */
    public void setDependencies(ReferenceCollection value) {
        this.dependencies = value;
    }

    /**
     * Gets the value of the artifacts property.
     * 
     * @return
     *     possible object is
     *     {@link ArtifactCollection }
     *     
     */
    public ArtifactCollection getArtifacts() {
        return artifacts;
    }

    /**
     * Sets the value of the artifacts property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArtifactCollection }
     *     
     */
    public void setArtifacts(ArtifactCollection value) {
        this.artifacts = value;
    }

    /**
     * Gets the value of the assemblyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssemblyName() {
        return assemblyName;
    }

    /**
     * Sets the value of the assemblyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssemblyName(String value) {
        this.assemblyName = value;
    }

    /**
     * Gets the value of the namespace property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNamespace() {
        return namespace;
    }

    /**
     * Sets the value of the namespace property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNamespace(String value) {
        this.namespace = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the relativePath property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelativePath() {
        return relativePath;
    }

    /**
     * Sets the value of the relativePath property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelativePath(String value) {
        this.relativePath = value;
    }

    /**
     * Gets the value of the hasEnvRides property.
     * 
     */
    public boolean isHasEnvRides() {
        return hasEnvRides;
    }

    /**
     * Sets the value of the hasEnvRides property.
     * 
     */
    public void setHasEnvRides(boolean value) {
        this.hasEnvRides = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Project)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Project that = ((Project) object);
        {
            ReferenceCollection lhsDependencies;
            lhsDependencies = this.getDependencies();
            ReferenceCollection rhsDependencies;
            rhsDependencies = that.getDependencies();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dependencies", lhsDependencies), LocatorUtils.property(thatLocator, "dependencies", rhsDependencies), lhsDependencies, rhsDependencies)) {
                return false;
            }
        }
        {
            ArtifactCollection lhsArtifacts;
            lhsArtifacts = this.getArtifacts();
            ArtifactCollection rhsArtifacts;
            rhsArtifacts = that.getArtifacts();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "artifacts", lhsArtifacts), LocatorUtils.property(thatLocator, "artifacts", rhsArtifacts), lhsArtifacts, rhsArtifacts)) {
                return false;
            }
        }
        {
            String lhsAssemblyName;
            lhsAssemblyName = this.getAssemblyName();
            String rhsAssemblyName;
            rhsAssemblyName = that.getAssemblyName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "assemblyName", lhsAssemblyName), LocatorUtils.property(thatLocator, "assemblyName", rhsAssemblyName), lhsAssemblyName, rhsAssemblyName)) {
                return false;
            }
        }
        {
            String lhsNamespace;
            lhsNamespace = this.getNamespace();
            String rhsNamespace;
            rhsNamespace = that.getNamespace();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "namespace", lhsNamespace), LocatorUtils.property(thatLocator, "namespace", rhsNamespace), lhsNamespace, rhsNamespace)) {
                return false;
            }
        }
        {
            String lhsDescription;
            lhsDescription = this.getDescription();
            String rhsDescription;
            rhsDescription = that.getDescription();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "description", lhsDescription), LocatorUtils.property(thatLocator, "description", rhsDescription), lhsDescription, rhsDescription)) {
                return false;
            }
        }
        {
            String lhsRelativePath;
            lhsRelativePath = this.getRelativePath();
            String rhsRelativePath;
            rhsRelativePath = that.getRelativePath();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "relativePath", lhsRelativePath), LocatorUtils.property(thatLocator, "relativePath", rhsRelativePath), lhsRelativePath, rhsRelativePath)) {
                return false;
            }
        }
        {
            boolean lhsHasEnvRides;
            lhsHasEnvRides = this.isHasEnvRides();
            boolean rhsHasEnvRides;
            rhsHasEnvRides = that.isHasEnvRides();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "hasEnvRides", lhsHasEnvRides), LocatorUtils.property(thatLocator, "hasEnvRides", rhsHasEnvRides), lhsHasEnvRides, rhsHasEnvRides)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Project) {
            final Project copy = ((Project) draftCopy);
            if (this.dependencies!= null) {
                ReferenceCollection sourceDependencies;
                sourceDependencies = this.getDependencies();
                ReferenceCollection copyDependencies = ((ReferenceCollection) strategy.copy(LocatorUtils.property(locator, "dependencies", sourceDependencies), sourceDependencies));
                copy.setDependencies(copyDependencies);
            } else {
                copy.dependencies = null;
            }
            if (this.artifacts!= null) {
                ArtifactCollection sourceArtifacts;
                sourceArtifacts = this.getArtifacts();
                ArtifactCollection copyArtifacts = ((ArtifactCollection) strategy.copy(LocatorUtils.property(locator, "artifacts", sourceArtifacts), sourceArtifacts));
                copy.setArtifacts(copyArtifacts);
            } else {
                copy.artifacts = null;
            }
            if (this.assemblyName!= null) {
                String sourceAssemblyName;
                sourceAssemblyName = this.getAssemblyName();
                String copyAssemblyName = ((String) strategy.copy(LocatorUtils.property(locator, "assemblyName", sourceAssemblyName), sourceAssemblyName));
                copy.setAssemblyName(copyAssemblyName);
            } else {
                copy.assemblyName = null;
            }
            if (this.namespace!= null) {
                String sourceNamespace;
                sourceNamespace = this.getNamespace();
                String copyNamespace = ((String) strategy.copy(LocatorUtils.property(locator, "namespace", sourceNamespace), sourceNamespace));
                copy.setNamespace(copyNamespace);
            } else {
                copy.namespace = null;
            }
            if (this.description!= null) {
                String sourceDescription;
                sourceDescription = this.getDescription();
                String copyDescription = ((String) strategy.copy(LocatorUtils.property(locator, "description", sourceDescription), sourceDescription));
                copy.setDescription(copyDescription);
            } else {
                copy.description = null;
            }
            if (this.relativePath!= null) {
                String sourceRelativePath;
                sourceRelativePath = this.getRelativePath();
                String copyRelativePath = ((String) strategy.copy(LocatorUtils.property(locator, "relativePath", sourceRelativePath), sourceRelativePath));
                copy.setRelativePath(copyRelativePath);
            } else {
                copy.relativePath = null;
            }
            boolean sourceHasEnvRides;
            sourceHasEnvRides = this.isHasEnvRides();
            boolean copyHasEnvRides = strategy.copy(LocatorUtils.property(locator, "hasEnvRides", sourceHasEnvRides), sourceHasEnvRides);
            copy.setHasEnvRides(copyHasEnvRides);
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Project();
    }
    
//--simple--preserve

    private transient String guid;
    
    public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getProjectFileName() {

		return getAssemblyName() + ".gen.csproj";
	}
     
//--simple--preserve

}
