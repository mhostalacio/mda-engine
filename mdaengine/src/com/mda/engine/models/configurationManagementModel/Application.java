
package com.mda.engine.models.configurationManagementModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ITransformerSource;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for Application complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Application">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/configurationManagementModel}ConfigurationManagementObject">
 *       &lt;sequence>
 *         &lt;element name="Projects" type="{http://www.mdaengine.com/mdaengine/models/configurationManagementModel}ProjectCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="UniqueName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Active" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *       &lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="RelativeFolderPath" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="GenerationInfoFolder" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Application", propOrder = {
    "projects"
})
@XmlRootElement
public class Application
    extends ConfigurationManagementObject
    implements Serializable, Cloneable, ITransformerSource, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Projects")
    protected ProjectCollection projects;
    @XmlAttribute(name = "UniqueName", required = true)
    protected String uniqueName;
    @XmlAttribute(name = "Active")
    protected Boolean active;
    @XmlAttribute(name = "Description")
    protected String description;
    @XmlAttribute(name = "RelativeFolderPath", required = true)
    protected String relativeFolderPath;
    @XmlAttribute(name = "GenerationInfoFolder", required = true)
    protected String generationInfoFolder;

    /**
     * Gets the value of the projects property.
     * 
     * @return
     *     possible object is
     *     {@link ProjectCollection }
     *     
     */
    public ProjectCollection getProjects() {
        return projects;
    }

    /**
     * Sets the value of the projects property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProjectCollection }
     *     
     */
    public void setProjects(ProjectCollection value) {
        this.projects = value;
    }

    /**
     * Gets the value of the uniqueName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUniqueName() {
        return uniqueName;
    }

    /**
     * Sets the value of the uniqueName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUniqueName(String value) {
        this.uniqueName = value;
    }

    /**
     * Gets the value of the active property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isActive() {
        if (active == null) {
            return true;
        } else {
            return active;
        }
    }

    /**
     * Sets the value of the active property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setActive(Boolean value) {
        this.active = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the relativeFolderPath property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelativeFolderPath() {
        return relativeFolderPath;
    }

    /**
     * Sets the value of the relativeFolderPath property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelativeFolderPath(String value) {
        this.relativeFolderPath = value;
    }

    /**
     * Gets the value of the generationInfoFolder property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGenerationInfoFolder() {
        return generationInfoFolder;
    }

    /**
     * Sets the value of the generationInfoFolder property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGenerationInfoFolder(String value) {
        this.generationInfoFolder = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Application)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final Application that = ((Application) object);
        {
            ProjectCollection lhsProjects;
            lhsProjects = this.getProjects();
            ProjectCollection rhsProjects;
            rhsProjects = that.getProjects();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "projects", lhsProjects), LocatorUtils.property(thatLocator, "projects", rhsProjects), lhsProjects, rhsProjects)) {
                return false;
            }
        }
        {
            String lhsUniqueName;
            lhsUniqueName = this.getUniqueName();
            String rhsUniqueName;
            rhsUniqueName = that.getUniqueName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "uniqueName", lhsUniqueName), LocatorUtils.property(thatLocator, "uniqueName", rhsUniqueName), lhsUniqueName, rhsUniqueName)) {
                return false;
            }
        }
        {
            boolean lhsActive;
            lhsActive = this.isActive();
            boolean rhsActive;
            rhsActive = that.isActive();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "active", lhsActive), LocatorUtils.property(thatLocator, "active", rhsActive), lhsActive, rhsActive)) {
                return false;
            }
        }
        {
            String lhsDescription;
            lhsDescription = this.getDescription();
            String rhsDescription;
            rhsDescription = that.getDescription();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "description", lhsDescription), LocatorUtils.property(thatLocator, "description", rhsDescription), lhsDescription, rhsDescription)) {
                return false;
            }
        }
        {
            String lhsRelativeFolderPath;
            lhsRelativeFolderPath = this.getRelativeFolderPath();
            String rhsRelativeFolderPath;
            rhsRelativeFolderPath = that.getRelativeFolderPath();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "relativeFolderPath", lhsRelativeFolderPath), LocatorUtils.property(thatLocator, "relativeFolderPath", rhsRelativeFolderPath), lhsRelativeFolderPath, rhsRelativeFolderPath)) {
                return false;
            }
        }
        {
            String lhsGenerationInfoFolder;
            lhsGenerationInfoFolder = this.getGenerationInfoFolder();
            String rhsGenerationInfoFolder;
            rhsGenerationInfoFolder = that.getGenerationInfoFolder();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "generationInfoFolder", lhsGenerationInfoFolder), LocatorUtils.property(thatLocator, "generationInfoFolder", rhsGenerationInfoFolder), lhsGenerationInfoFolder, rhsGenerationInfoFolder)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof Application) {
            final Application copy = ((Application) draftCopy);
            if (this.projects!= null) {
                ProjectCollection sourceProjects;
                sourceProjects = this.getProjects();
                ProjectCollection copyProjects = ((ProjectCollection) strategy.copy(LocatorUtils.property(locator, "projects", sourceProjects), sourceProjects));
                copy.setProjects(copyProjects);
            } else {
                copy.projects = null;
            }
            if (this.uniqueName!= null) {
                String sourceUniqueName;
                sourceUniqueName = this.getUniqueName();
                String copyUniqueName = ((String) strategy.copy(LocatorUtils.property(locator, "uniqueName", sourceUniqueName), sourceUniqueName));
                copy.setUniqueName(copyUniqueName);
            } else {
                copy.uniqueName = null;
            }
            if (this.active!= null) {
                boolean sourceActive;
                sourceActive = this.isActive();
                boolean copyActive = strategy.copy(LocatorUtils.property(locator, "active", sourceActive), sourceActive);
                copy.setActive(copyActive);
            } else {
                copy.active = null;
            }
            if (this.description!= null) {
                String sourceDescription;
                sourceDescription = this.getDescription();
                String copyDescription = ((String) strategy.copy(LocatorUtils.property(locator, "description", sourceDescription), sourceDescription));
                copy.setDescription(copyDescription);
            } else {
                copy.description = null;
            }
            if (this.relativeFolderPath!= null) {
                String sourceRelativeFolderPath;
                sourceRelativeFolderPath = this.getRelativeFolderPath();
                String copyRelativeFolderPath = ((String) strategy.copy(LocatorUtils.property(locator, "relativeFolderPath", sourceRelativeFolderPath), sourceRelativeFolderPath));
                copy.setRelativeFolderPath(copyRelativeFolderPath);
            } else {
                copy.relativeFolderPath = null;
            }
            if (this.generationInfoFolder!= null) {
                String sourceGenerationInfoFolder;
                sourceGenerationInfoFolder = this.getGenerationInfoFolder();
                String copyGenerationInfoFolder = ((String) strategy.copy(LocatorUtils.property(locator, "generationInfoFolder", sourceGenerationInfoFolder), sourceGenerationInfoFolder));
                copy.setGenerationInfoFolder(copyGenerationInfoFolder);
            } else {
                copy.generationInfoFolder = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Application();
    }
    
//--simple--preserve

    private transient com.mda.engine.core.Product product;
    private transient String entitiesProjectNamespace;
    private transient String repositoryProjectNamespace;
    private transient String serviceContractsProjectNamespace;
    private transient String serviceInterfaceProjectNamespace;
    private transient String entitiesConverterProjectNamespace;
    private transient String transactionsConverterProjectNamespace;
    private transient String transactionsProjectNamespace;
    private transient String serviceImplementationProjectNamespace;
    private transient String serviceHostProjectNamespace;
    private transient String wcfImplementationProjectNamespace;
    
	private transient Boolean hasTransactionsMapping = false;
	private transient String transactionsMappingClassName = null;
    
   public com.mda.engine.models.configurationManagementModel.EntitiesProject getEntitiesProject()
   {
	   java.util.ArrayList<com.mda.engine.models.configurationManagementModel.Project> list = getProjectsByType(EntitiesProject.class);
	   if (list.size() > 0)
		   return (com.mda.engine.models.configurationManagementModel.EntitiesProject)list.get(0);
	   else
		   return null;
   }
   
   public com.mda.engine.models.configurationManagementModel.RepositoryProject getRepositoryProject()
   {
	   java.util.ArrayList<com.mda.engine.models.configurationManagementModel.Project> list = getProjectsByType(RepositoryProject.class);
	   if (list.size() > 0)
		   return (com.mda.engine.models.configurationManagementModel.RepositoryProject)list.get(0);
	   else
		   return null;
   }
   
   public com.mda.engine.models.configurationManagementModel.ServiceContractsClassLibraryProject getServiceContractsProject()
   {
	   java.util.ArrayList<com.mda.engine.models.configurationManagementModel.Project> list = getProjectsByType(ServiceContractsClassLibraryProject.class);
	   if (list.size() > 0)
		   return (com.mda.engine.models.configurationManagementModel.ServiceContractsClassLibraryProject)list.get(0);
	   else
		   return null;
   }
   
   public com.mda.engine.models.configurationManagementModel.ServiceInterfaceProject getServiceInterfaceProject()
   {
	   java.util.ArrayList<com.mda.engine.models.configurationManagementModel.Project> list = getProjectsByType(ServiceInterfaceProject.class);
	   if (list.size() > 0)
		   return (com.mda.engine.models.configurationManagementModel.ServiceInterfaceProject)list.get(0);
	   else
		   return null;
   }
   
   public com.mda.engine.models.configurationManagementModel.ServiceImplementationProject getServiceImplementationProject()
   {
	   java.util.ArrayList<com.mda.engine.models.configurationManagementModel.Project> list = getProjectsByType(ServiceImplementationProject.class);
	   if (list.size() > 0)
		   return (com.mda.engine.models.configurationManagementModel.ServiceImplementationProject)list.get(0);
	   else
		   return null;
   }
   
   public com.mda.engine.models.configurationManagementModel.WebServiceHostProject getServiceHostProject()
   {
	   java.util.ArrayList<com.mda.engine.models.configurationManagementModel.Project> list = getProjectsByType(WebServiceHostProject.class);
	   if (list.size() > 0)
		   return (com.mda.engine.models.configurationManagementModel.WebServiceHostProject)list.get(0);
	   else
		   return null;
   }
   
   public com.mda.engine.models.configurationManagementModel.WCFImplementationProject getWCFImplementationProject()
   {
	   java.util.ArrayList<com.mda.engine.models.configurationManagementModel.Project> list = getProjectsByType(WCFImplementationProject.class);
	   if (list.size() > 0)
		   return (com.mda.engine.models.configurationManagementModel.WCFImplementationProject)list.get(0);
	   else
		   return null;
   }
   
   public com.mda.engine.models.configurationManagementModel.EntitiesConverterProject getEntitiesConverterProject()
   {
	   java.util.ArrayList<com.mda.engine.models.configurationManagementModel.Project> list = getProjectsByType(EntitiesConverterProject.class);
	   if (list.size() > 0)
		   return (com.mda.engine.models.configurationManagementModel.EntitiesConverterProject)list.get(0);
	   else
		   return null;
   }
   
   public com.mda.engine.models.configurationManagementModel.TransactionsConverterProject getTransactionsConverterProject()
   {
	   java.util.ArrayList<com.mda.engine.models.configurationManagementModel.Project> list = getProjectsByType(TransactionsConverterProject.class);
	   if (list.size() > 0)
		   return (com.mda.engine.models.configurationManagementModel.TransactionsConverterProject)list.get(0);
	   else
		   return null;
   }
   
   public com.mda.engine.models.configurationManagementModel.TransactionsProject getTransactionsProject()
   {
	   java.util.ArrayList<com.mda.engine.models.configurationManagementModel.Project> list = getProjectsByType(TransactionsProject.class);
	   if (list.size() > 0)
		   return (com.mda.engine.models.configurationManagementModel.TransactionsProject)list.get(0);
	   else
		   return null;
   }
   
   public com.mda.engine.models.configurationManagementModel.ViewControllerClassLibraryProject getViewControllerProject()
   {
	   java.util.ArrayList<com.mda.engine.models.configurationManagementModel.Project> list = getProjectsByType(ViewControllerClassLibraryProject.class);
	   if (list.size() > 0)
		   return (com.mda.engine.models.configurationManagementModel.ViewControllerClassLibraryProject)list.get(0);
	   else
		   return null;
   }
   
   public com.mda.engine.models.configurationManagementModel.ModelClassLibraryProject getModelsProject()
   {
	   java.util.ArrayList<com.mda.engine.models.configurationManagementModel.Project> list = getProjectsByType(ModelClassLibraryProject.class);
	   if (list.size() > 0)
		   return (com.mda.engine.models.configurationManagementModel.ModelClassLibraryProject)list.get(0);
	   else
		   return null;
   }
   
   public com.mda.engine.models.configurationManagementModel.WebApplicationProject getWebProject()
   {
	   java.util.ArrayList<com.mda.engine.models.configurationManagementModel.Project> list = getProjectsByType(WebApplicationProject.class);
	   if (list.size() > 0)
		   return (com.mda.engine.models.configurationManagementModel.WebApplicationProject)list.get(0);
	   else
		   return null;
   }
   
   public java.util.ArrayList<com.mda.engine.models.configurationManagementModel.Project> getProjectsByType(Class<? extends Project> projClass)
   {
	   java.util.ArrayList<com.mda.engine.models.configurationManagementModel.Project> list = new java.util.ArrayList<com.mda.engine.models.configurationManagementModel.Project>();
	   for(Project proj : this.getProjects().getProject())
	   {
		   if (projClass.isInstance(proj))
			   list.add(proj);
	   }
	   return list;
   }
   
   public java.util.ArrayList<com.mda.engine.models.configurationManagementModel.ClassLibraryProject> getClassLibraryProjects()
   {
	   java.util.ArrayList<com.mda.engine.models.configurationManagementModel.ClassLibraryProject> list = new java.util.ArrayList<com.mda.engine.models.configurationManagementModel.ClassLibraryProject>();
	   for(Project proj : this.getProjects().getProject())
	   {
		   if (proj instanceof com.mda.engine.models.configurationManagementModel.ClassLibraryProject)
			   list.add((ClassLibraryProject)proj);
	   }
	   return list;
   }
   
   public java.util.ArrayList<com.mda.engine.models.configurationManagementModel.WindowsServiceProject> getWindowsServiceProjects()
   {
	   java.util.ArrayList<com.mda.engine.models.configurationManagementModel.WindowsServiceProject> list = new java.util.ArrayList<com.mda.engine.models.configurationManagementModel.WindowsServiceProject>();
	   for(Project proj : this.getProjects().getProject())
	   {
		   if (proj instanceof com.mda.engine.models.configurationManagementModel.WindowsServiceProject)
			   list.add((WindowsServiceProject)proj);
	   }
	   return list;
   }
   
   public java.util.ArrayList<com.mda.engine.models.configurationManagementModel.ConsoleApplicationProject> getConsoleApplicationProjects(){
	   java.util.ArrayList<com.mda.engine.models.configurationManagementModel.ConsoleApplicationProject> projects;
	   
	   projects = new java.util.ArrayList<com.mda.engine.models.configurationManagementModel.ConsoleApplicationProject>();
	   
	   for(Project proj : this.getProjects().getProject()){
		   if(proj instanceof com.mda.engine.models.configurationManagementModel.ConsoleApplicationProject){
			   projects.add((com.mda.engine.models.configurationManagementModel.ConsoleApplicationProject)proj);
		   }
	   }

	   return projects;
   }
    
   public void setProduct(com.mda.engine.core.Product product) {
	   this.product = product;
   }
	
	public com.mda.engine.core.Product getProduct() {
		return product;
	}

	public void setEntitiesProjectNamespace(String entitiesProjectNamespace) {
		this.entitiesProjectNamespace = entitiesProjectNamespace;
	}

	public String getEntitiesProjectNamespace() {
		return entitiesProjectNamespace;
	}

	public void setServiceContractsProjectNamespace(
			String serviceContractsProjectNamespace) {
		this.serviceContractsProjectNamespace = serviceContractsProjectNamespace;
	}

	public String getServiceContractsProjectNamespace() {
		return serviceContractsProjectNamespace;
	}

	public void setRepositoryProjectNamespace(String repositoryProjectNamespace) {
		this.repositoryProjectNamespace = repositoryProjectNamespace;
	}

	public String getRepositoryProjectNamespace() {
		return repositoryProjectNamespace;
	}

	public void setServiceInterfaceProjectNamespace(
			String serviceInterfaceProjectNamespace) {
		this.serviceInterfaceProjectNamespace = serviceInterfaceProjectNamespace;
	}

	public String getServiceInterfaceProjectNamespace() {
		return serviceInterfaceProjectNamespace;
	}

	public void setEntitiesConverterProjectNamespace(
			String entitiesConverterProjectNamespace) {
		this.entitiesConverterProjectNamespace = entitiesConverterProjectNamespace;
	}

	public String getEntitiesConverterProjectNamespace() {
		return entitiesConverterProjectNamespace;
	}

	public void setTransactionsConverterProjectNamespace(
			String transactionsConverterProjectNamespace) {
		this.transactionsConverterProjectNamespace = transactionsConverterProjectNamespace;
	}

	public String getTransactionsConverterProjectNamespace() {
		return transactionsConverterProjectNamespace;
	}

	public void setTransactionsProjectNamespace(
			String transactionsProjectNamespace) {
		this.transactionsProjectNamespace = transactionsProjectNamespace;
	}

	public String getTransactionsProjectNamespace() {
		return transactionsProjectNamespace;
	}

	public void setServiceImplementationProjectNamespace(
			String serviceImplementationProjectNamespace) {
		this.serviceImplementationProjectNamespace = serviceImplementationProjectNamespace;
	}

	public String getServiceImplementationProjectNamespace() {
		return serviceImplementationProjectNamespace;
	}

	public void setServiceHostProjectNamespace(
			String serviceHostProjectNamespace) {
		this.serviceHostProjectNamespace = serviceHostProjectNamespace;
	}

	public String getServiceHostProjectNamespace() {
		return serviceHostProjectNamespace;
	}

	public String getWcfImplementationProjectNamespace() {
		return wcfImplementationProjectNamespace;
	}

	public void setWcfImplementationProjectNamespace(
			String wcfImplementationProjectNamespace) {
		this.wcfImplementationProjectNamespace = wcfImplementationProjectNamespace;
	}
	
	public Boolean getHasTransactionsMapping() {
		return hasTransactionsMapping;
	}

	public void setHasTransactionsMapping(Boolean hasTransactionsMapping) {
		this.hasTransactionsMapping = hasTransactionsMapping;
	}

	public String getTransactionsMappingClassName() {
		return transactionsMappingClassName;
	}

	public void setTransactionsMappingClassName(
			String transactionsMappingClassName) {
		this.transactionsMappingClassName = transactionsMappingClassName;
	}
   
//--simple--preserve

}
