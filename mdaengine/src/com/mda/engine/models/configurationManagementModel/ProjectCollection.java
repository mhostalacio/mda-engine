
package com.mda.engine.models.configurationManagementModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ProjectCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProjectCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded" minOccurs="0">
 *         &lt;element name="WebApplication" type="{http://www.mdaengine.com/mdaengine/models/configurationManagementModel}WebApplicationProject" minOccurs="0"/>
 *         &lt;element name="ServiceHost" type="{http://www.mdaengine.com/mdaengine/models/configurationManagementModel}WebServiceHostProject" minOccurs="0"/>
 *         &lt;element name="ServiceImplementation" type="{http://www.mdaengine.com/mdaengine/models/configurationManagementModel}ServiceImplementationProject" minOccurs="0"/>
 *         &lt;element name="WCFImplementation" type="{http://www.mdaengine.com/mdaengine/models/configurationManagementModel}WCFImplementationProject" minOccurs="0"/>
 *         &lt;element name="DataBaseLoader" type="{http://www.mdaengine.com/mdaengine/models/configurationManagementModel}DataBaseLoaderProject" minOccurs="0"/>
 *         &lt;element name="WindowsService" type="{http://www.mdaengine.com/mdaengine/models/configurationManagementModel}WindowsServiceProject" minOccurs="0"/>
 *         &lt;element name="ClassLibrary" type="{http://www.mdaengine.com/mdaengine/models/configurationManagementModel}ClassLibraryProject" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TestsProject" type="{http://www.mdaengine.com/mdaengine/models/configurationManagementModel}TestsProject" minOccurs="0"/>
 *         &lt;element name="ConsoleApplication" type="{http://www.mdaengine.com/mdaengine/models/configurationManagementModel}ConsoleApplicationProject" minOccurs="0"/>
 *         &lt;element name="ModelClassLibrary" type="{http://www.mdaengine.com/mdaengine/models/configurationManagementModel}ModelClassLibraryProject" minOccurs="0"/>
 *         &lt;element name="ViewControllerClassLibrary" type="{http://www.mdaengine.com/mdaengine/models/configurationManagementModel}ViewControllerClassLibraryProject" minOccurs="0"/>
 *         &lt;element name="ServiceContractsClassLibrary" type="{http://www.mdaengine.com/mdaengine/models/configurationManagementModel}ServiceContractsClassLibraryProject" minOccurs="0"/>
 *         &lt;element name="EntitiesProject" type="{http://www.mdaengine.com/mdaengine/models/configurationManagementModel}EntitiesProject" minOccurs="0"/>
 *         &lt;element name="RepositoryProject" type="{http://www.mdaengine.com/mdaengine/models/configurationManagementModel}RepositoryProject" minOccurs="0"/>
 *         &lt;element name="ServiceInterfaceProject" type="{http://www.mdaengine.com/mdaengine/models/configurationManagementModel}ServiceInterfaceProject" minOccurs="0"/>
 *         &lt;element name="EntitiesConverterProject" type="{http://www.mdaengine.com/mdaengine/models/configurationManagementModel}EntitiesConverterProject" minOccurs="0"/>
 *         &lt;element name="TransactionsConverterProject" type="{http://www.mdaengine.com/mdaengine/models/configurationManagementModel}TransactionsConverterProject" minOccurs="0"/>
 *         &lt;element name="TransactionsProject" type="{http://www.mdaengine.com/mdaengine/models/configurationManagementModel}TransactionsProject" minOccurs="0"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProjectCollection", propOrder = {
    "project"
})
public class ProjectCollection
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElements({
        @XmlElement(name = "ConsoleApplication", type = ConsoleApplicationProject.class),
        @XmlElement(name = "ServiceContractsClassLibrary", type = ServiceContractsClassLibraryProject.class),
        @XmlElement(name = "TransactionsConverterProject", type = TransactionsConverterProject.class),
        @XmlElement(name = "WCFImplementation", type = WCFImplementationProject.class),
        @XmlElement(name = "WebApplication", type = WebApplicationProject.class),
        @XmlElement(name = "DataBaseLoader", type = DataBaseLoaderProject.class),
        @XmlElement(name = "ModelClassLibrary", type = ModelClassLibraryProject.class),
        @XmlElement(name = "EntitiesConverterProject", type = EntitiesConverterProject.class),
        @XmlElement(name = "EntitiesProject", type = EntitiesProject.class),
        @XmlElement(name = "RepositoryProject", type = RepositoryProject.class),
        @XmlElement(name = "TestsProject", type = TestsProject.class),
        @XmlElement(name = "WindowsService", type = WindowsServiceProject.class),
        @XmlElement(name = "ServiceHost", type = WebServiceHostProject.class),
        @XmlElement(name = "ServiceInterfaceProject", type = ServiceInterfaceProject.class),
        @XmlElement(name = "TransactionsProject", type = TransactionsProject.class),
        @XmlElement(name = "ClassLibrary", type = ClassLibraryProject.class),
        @XmlElement(name = "ViewControllerClassLibrary", type = ViewControllerClassLibraryProject.class),
        @XmlElement(name = "ServiceImplementation", type = ServiceImplementationProject.class)
    })
    protected List<Project> project;

    /**
     * Gets the value of the project property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the project property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProject().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConsoleApplicationProject }
     * {@link ServiceContractsClassLibraryProject }
     * {@link TransactionsConverterProject }
     * {@link WCFImplementationProject }
     * {@link WebApplicationProject }
     * {@link DataBaseLoaderProject }
     * {@link ModelClassLibraryProject }
     * {@link EntitiesConverterProject }
     * {@link EntitiesProject }
     * {@link RepositoryProject }
     * {@link TestsProject }
     * {@link WindowsServiceProject }
     * {@link WebServiceHostProject }
     * {@link ServiceInterfaceProject }
     * {@link TransactionsProject }
     * {@link ClassLibraryProject }
     * {@link ViewControllerClassLibraryProject }
     * {@link ServiceImplementationProject }
     * 
     * 
     */
    public List<Project> getProject() {
        if (project == null) {
            project = new ArrayList<Project>();
        }
        return this.project;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ProjectCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ProjectCollection that = ((ProjectCollection) object);
        {
            List<Project> lhsProject;
            lhsProject = this.getProject();
            List<Project> rhsProject;
            rhsProject = that.getProject();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "project", lhsProject), LocatorUtils.property(thatLocator, "project", rhsProject), lhsProject, rhsProject)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof ProjectCollection) {
            final ProjectCollection copy = ((ProjectCollection) draftCopy);
            if ((this.project!= null)&&(!this.project.isEmpty())) {
                List<Project> sourceProject;
                sourceProject = this.getProject();
                @SuppressWarnings("unchecked")
                List<Project> copyProject = ((List<Project> ) strategy.copy(LocatorUtils.property(locator, "project", sourceProject), sourceProject));
                copy.project = null;
                List<Project> uniqueProjectl = copy.getProject();
                uniqueProjectl.addAll(copyProject);
            } else {
                copy.project = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ProjectCollection();
    }

}
