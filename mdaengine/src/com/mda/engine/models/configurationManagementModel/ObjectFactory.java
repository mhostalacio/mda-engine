
package com.mda.engine.models.configurationManagementModel;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.mda.engine.models.configurationManagementModel package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Application_QNAME = new QName("http://www.mdaengine.com/mdaengine/models/configurationManagementModel", "Application");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.mda.engine.models.configurationManagementModel
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ArtifactCollection }
     * 
     */
    public ArtifactCollection createArtifactCollection() {
        return new ArtifactCollection();
    }

    /**
     * Create an instance of {@link EntitiesConverterProject }
     * 
     */
    public EntitiesConverterProject createEntitiesConverterProject() {
        return new EntitiesConverterProject();
    }

    /**
     * Create an instance of {@link Project }
     * 
     */
    public Project createProject() {
        return new Project();
    }

    /**
     * Create an instance of {@link ViewControllerClassLibraryProject }
     * 
     */
    public ViewControllerClassLibraryProject createViewControllerClassLibraryProject() {
        return new ViewControllerClassLibraryProject();
    }

    /**
     * Create an instance of {@link ClassLibraryProject }
     * 
     */
    public ClassLibraryProject createClassLibraryProject() {
        return new ClassLibraryProject();
    }

    /**
     * Create an instance of {@link ServiceContractsClassLibraryProject }
     * 
     */
    public ServiceContractsClassLibraryProject createServiceContractsClassLibraryProject() {
        return new ServiceContractsClassLibraryProject();
    }

    /**
     * Create an instance of {@link WCFImplementationProject }
     * 
     */
    public WCFImplementationProject createWCFImplementationProject() {
        return new WCFImplementationProject();
    }

    /**
     * Create an instance of {@link ConsoleApplicationProject }
     * 
     */
    public ConsoleApplicationProject createConsoleApplicationProject() {
        return new ConsoleApplicationProject();
    }

    /**
     * Create an instance of {@link ServiceInterfaceProject }
     * 
     */
    public ServiceInterfaceProject createServiceInterfaceProject() {
        return new ServiceInterfaceProject();
    }

    /**
     * Create an instance of {@link EntitiesProject }
     * 
     */
    public EntitiesProject createEntitiesProject() {
        return new EntitiesProject();
    }

    /**
     * Create an instance of {@link TransactionsProject }
     * 
     */
    public TransactionsProject createTransactionsProject() {
        return new TransactionsProject();
    }

    /**
     * Create an instance of {@link Application }
     * 
     */
    public Application createApplication() {
        return new Application();
    }

    /**
     * Create an instance of {@link ModelClassLibraryProject }
     * 
     */
    public ModelClassLibraryProject createModelClassLibraryProject() {
        return new ModelClassLibraryProject();
    }

    /**
     * Create an instance of {@link WebServiceHostProject }
     * 
     */
    public WebServiceHostProject createWebServiceHostProject() {
        return new WebServiceHostProject();
    }

    /**
     * Create an instance of {@link Reference }
     * 
     */
    public Reference createReference() {
        return new Reference();
    }

    /**
     * Create an instance of {@link TransactionsMapping }
     * 
     */
    public TransactionsMapping createTransactionsMapping() {
        return new TransactionsMapping();
    }

    /**
     * Create an instance of {@link WindowsServiceProject }
     * 
     */
    public WindowsServiceProject createWindowsServiceProject() {
        return new WindowsServiceProject();
    }

    /**
     * Create an instance of {@link ServiceImplementationProject }
     * 
     */
    public ServiceImplementationProject createServiceImplementationProject() {
        return new ServiceImplementationProject();
    }

    /**
     * Create an instance of {@link WebApplicationProject }
     * 
     */
    public WebApplicationProject createWebApplicationProject() {
        return new WebApplicationProject();
    }

    /**
     * Create an instance of {@link ReferenceCollection }
     * 
     */
    public ReferenceCollection createReferenceCollection() {
        return new ReferenceCollection();
    }

    /**
     * Create an instance of {@link TransactionsConverterProject }
     * 
     */
    public TransactionsConverterProject createTransactionsConverterProject() {
        return new TransactionsConverterProject();
    }

    /**
     * Create an instance of {@link ProjectReference }
     * 
     */
    public ProjectReference createProjectReference() {
        return new ProjectReference();
    }

    /**
     * Create an instance of {@link RepositoryProject }
     * 
     */
    public RepositoryProject createRepositoryProject() {
        return new RepositoryProject();
    }

    /**
     * Create an instance of {@link DataBaseLoaderProject }
     * 
     */
    public DataBaseLoaderProject createDataBaseLoaderProject() {
        return new DataBaseLoaderProject();
    }

    /**
     * Create an instance of {@link LibraryReference }
     * 
     */
    public LibraryReference createLibraryReference() {
        return new LibraryReference();
    }

    /**
     * Create an instance of {@link ProjectCollection }
     * 
     */
    public ProjectCollection createProjectCollection() {
        return new ProjectCollection();
    }

    /**
     * Create an instance of {@link TestsProject }
     * 
     */
    public TestsProject createTestsProject() {
        return new TestsProject();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Application }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.mdaengine.com/mdaengine/models/configurationManagementModel", name = "Application")
    public JAXBElement<Application> createApplication(Application value) {
        return new JAXBElement<Application>(_Application_QNAME, Application.class, null, value);
    }

}
