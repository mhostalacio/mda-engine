
package com.mda.engine.models.buildConfig;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for BuildPackageConfigCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BuildPackageConfigCollection">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/buildConfig}InclusionBase">
 *       &lt;sequence>
 *         &lt;element name="Package" type="{http://www.mdaengine.com/mdaengine/models/buildConfig}BuildPackageConfig" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BuildPackageConfigCollection", propOrder = {
    "_package"
})
public class BuildPackageConfigCollection
    extends InclusionBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Package")
    protected List<BuildPackageConfig> _package;

    /**
     * Gets the value of the package property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the package property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPackage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BuildPackageConfig }
     * 
     * 
     */
    public List<BuildPackageConfig> getPackage() {
        if (_package == null) {
            _package = new ArrayList<BuildPackageConfig>();
        }
        return this._package;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof BuildPackageConfigCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final BuildPackageConfigCollection that = ((BuildPackageConfigCollection) object);
        {
            List<BuildPackageConfig> lhsPackage;
            lhsPackage = this.getPackage();
            List<BuildPackageConfig> rhsPackage;
            rhsPackage = that.getPackage();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "_package", lhsPackage), LocatorUtils.property(thatLocator, "_package", rhsPackage), lhsPackage, rhsPackage)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof BuildPackageConfigCollection) {
            final BuildPackageConfigCollection copy = ((BuildPackageConfigCollection) draftCopy);
            if ((this._package!= null)&&(!this._package.isEmpty())) {
                List<BuildPackageConfig> sourcePackage;
                sourcePackage = this.getPackage();
                @SuppressWarnings("unchecked")
                List<BuildPackageConfig> copyPackage = ((List<BuildPackageConfig> ) strategy.copy(LocatorUtils.property(locator, "_package", sourcePackage), sourcePackage));
                copy._package = null;
                List<BuildPackageConfig> uniquePackagel = copy.getPackage();
                uniquePackagel.addAll(copyPackage);
            } else {
                copy._package = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new BuildPackageConfigCollection();
    }

}
