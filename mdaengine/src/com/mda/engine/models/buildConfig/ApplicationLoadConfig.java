
package com.mda.engine.models.buildConfig;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ApplicationLoadConfig complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ApplicationLoadConfig">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="Name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Load" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="Process" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApplicationLoadConfig")
public class ApplicationLoadConfig
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "Name", required = true)
    protected String name;
    @XmlAttribute(name = "Load", required = true)
    protected boolean load;
    @XmlAttribute(name = "Process", required = true)
    protected boolean process;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the load property.
     * 
     */
    public boolean isLoad() {
        return load;
    }

    /**
     * Sets the value of the load property.
     * 
     */
    public void setLoad(boolean value) {
        this.load = value;
    }

    /**
     * Gets the value of the process property.
     * 
     */
    public boolean isProcess() {
        return process;
    }

    /**
     * Sets the value of the process property.
     * 
     */
    public void setProcess(boolean value) {
        this.process = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ApplicationLoadConfig)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ApplicationLoadConfig that = ((ApplicationLoadConfig) object);
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            boolean lhsLoad;
            lhsLoad = this.isLoad();
            boolean rhsLoad;
            rhsLoad = that.isLoad();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "load", lhsLoad), LocatorUtils.property(thatLocator, "load", rhsLoad), lhsLoad, rhsLoad)) {
                return false;
            }
        }
        {
            boolean lhsProcess;
            lhsProcess = this.isProcess();
            boolean rhsProcess;
            rhsProcess = that.isProcess();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "process", lhsProcess), LocatorUtils.property(thatLocator, "process", rhsProcess), lhsProcess, rhsProcess)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof ApplicationLoadConfig) {
            final ApplicationLoadConfig copy = ((ApplicationLoadConfig) draftCopy);
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            boolean sourceLoad;
            sourceLoad = this.isLoad();
            boolean copyLoad = strategy.copy(LocatorUtils.property(locator, "load", sourceLoad), sourceLoad);
            copy.setLoad(copyLoad);
            boolean sourceProcess;
            sourceProcess = this.isProcess();
            boolean copyProcess = strategy.copy(LocatorUtils.property(locator, "process", sourceProcess), sourceProcess);
            copy.setProcess(copyProcess);
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ApplicationLoadConfig();
    }

}
