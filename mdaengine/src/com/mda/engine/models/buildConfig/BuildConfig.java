
package com.mda.engine.models.buildConfig;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for BuildConfig complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BuildConfig">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Applications" type="{http://www.mdaengine.com/mdaengine/models/buildConfig}BuildApplicationConfigCollection" minOccurs="0"/>
 *         &lt;element name="Layers" type="{http://www.mdaengine.com/mdaengine/models/buildConfig}BuildLayerConfigCollection" minOccurs="0"/>
 *         &lt;element name="Transformers" type="{http://www.mdaengine.com/mdaengine/models/buildConfig}TransformerConfigCollection" minOccurs="0"/>
 *         &lt;element name="ApplicationsLoadConfig" type="{http://www.mdaengine.com/mdaengine/models/buildConfig}ApplicationLoadConfigCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Mode" use="required" type="{http://www.mdaengine.com/mdaengine/models/buildConfig}BuildMode" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BuildConfig", propOrder = {
    "applications",
    "layers",
    "transformers",
    "applicationsLoadConfig"
})
public class BuildConfig
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Applications")
    protected BuildApplicationConfigCollection applications;
    @XmlElement(name = "Layers")
    protected BuildLayerConfigCollection layers;
    @XmlElement(name = "Transformers")
    protected TransformerConfigCollection transformers;
    @XmlElement(name = "ApplicationsLoadConfig")
    protected ApplicationLoadConfigCollection applicationsLoadConfig;
    @XmlAttribute(name = "Mode", required = true)
    protected BuildMode mode;

    /**
     * Gets the value of the applications property.
     * 
     * @return
     *     possible object is
     *     {@link BuildApplicationConfigCollection }
     *     
     */
    public BuildApplicationConfigCollection getApplications() {
        return applications;
    }

    /**
     * Sets the value of the applications property.
     * 
     * @param value
     *     allowed object is
     *     {@link BuildApplicationConfigCollection }
     *     
     */
    public void setApplications(BuildApplicationConfigCollection value) {
        this.applications = value;
    }

    /**
     * Gets the value of the layers property.
     * 
     * @return
     *     possible object is
     *     {@link BuildLayerConfigCollection }
     *     
     */
    public BuildLayerConfigCollection getLayers() {
        return layers;
    }

    /**
     * Sets the value of the layers property.
     * 
     * @param value
     *     allowed object is
     *     {@link BuildLayerConfigCollection }
     *     
     */
    public void setLayers(BuildLayerConfigCollection value) {
        this.layers = value;
    }

    /**
     * Gets the value of the transformers property.
     * 
     * @return
     *     possible object is
     *     {@link TransformerConfigCollection }
     *     
     */
    public TransformerConfigCollection getTransformers() {
        return transformers;
    }

    /**
     * Sets the value of the transformers property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransformerConfigCollection }
     *     
     */
    public void setTransformers(TransformerConfigCollection value) {
        this.transformers = value;
    }

    /**
     * Gets the value of the applicationsLoadConfig property.
     * 
     * @return
     *     possible object is
     *     {@link ApplicationLoadConfigCollection }
     *     
     */
    public ApplicationLoadConfigCollection getApplicationsLoadConfig() {
        return applicationsLoadConfig;
    }

    /**
     * Sets the value of the applicationsLoadConfig property.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplicationLoadConfigCollection }
     *     
     */
    public void setApplicationsLoadConfig(ApplicationLoadConfigCollection value) {
        this.applicationsLoadConfig = value;
    }

    /**
     * Gets the value of the mode property.
     * 
     * @return
     *     possible object is
     *     {@link BuildMode }
     *     
     */
    public BuildMode getMode() {
        return mode;
    }

    /**
     * Sets the value of the mode property.
     * 
     * @param value
     *     allowed object is
     *     {@link BuildMode }
     *     
     */
    public void setMode(BuildMode value) {
        this.mode = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof BuildConfig)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final BuildConfig that = ((BuildConfig) object);
        {
            BuildApplicationConfigCollection lhsApplications;
            lhsApplications = this.getApplications();
            BuildApplicationConfigCollection rhsApplications;
            rhsApplications = that.getApplications();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "applications", lhsApplications), LocatorUtils.property(thatLocator, "applications", rhsApplications), lhsApplications, rhsApplications)) {
                return false;
            }
        }
        {
            BuildLayerConfigCollection lhsLayers;
            lhsLayers = this.getLayers();
            BuildLayerConfigCollection rhsLayers;
            rhsLayers = that.getLayers();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "layers", lhsLayers), LocatorUtils.property(thatLocator, "layers", rhsLayers), lhsLayers, rhsLayers)) {
                return false;
            }
        }
        {
            TransformerConfigCollection lhsTransformers;
            lhsTransformers = this.getTransformers();
            TransformerConfigCollection rhsTransformers;
            rhsTransformers = that.getTransformers();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "transformers", lhsTransformers), LocatorUtils.property(thatLocator, "transformers", rhsTransformers), lhsTransformers, rhsTransformers)) {
                return false;
            }
        }
        {
            ApplicationLoadConfigCollection lhsApplicationsLoadConfig;
            lhsApplicationsLoadConfig = this.getApplicationsLoadConfig();
            ApplicationLoadConfigCollection rhsApplicationsLoadConfig;
            rhsApplicationsLoadConfig = that.getApplicationsLoadConfig();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "applicationsLoadConfig", lhsApplicationsLoadConfig), LocatorUtils.property(thatLocator, "applicationsLoadConfig", rhsApplicationsLoadConfig), lhsApplicationsLoadConfig, rhsApplicationsLoadConfig)) {
                return false;
            }
        }
        {
            BuildMode lhsMode;
            lhsMode = this.getMode();
            BuildMode rhsMode;
            rhsMode = that.getMode();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "mode", lhsMode), LocatorUtils.property(thatLocator, "mode", rhsMode), lhsMode, rhsMode)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof BuildConfig) {
            final BuildConfig copy = ((BuildConfig) draftCopy);
            if (this.applications!= null) {
                BuildApplicationConfigCollection sourceApplications;
                sourceApplications = this.getApplications();
                BuildApplicationConfigCollection copyApplications = ((BuildApplicationConfigCollection) strategy.copy(LocatorUtils.property(locator, "applications", sourceApplications), sourceApplications));
                copy.setApplications(copyApplications);
            } else {
                copy.applications = null;
            }
            if (this.layers!= null) {
                BuildLayerConfigCollection sourceLayers;
                sourceLayers = this.getLayers();
                BuildLayerConfigCollection copyLayers = ((BuildLayerConfigCollection) strategy.copy(LocatorUtils.property(locator, "layers", sourceLayers), sourceLayers));
                copy.setLayers(copyLayers);
            } else {
                copy.layers = null;
            }
            if (this.transformers!= null) {
                TransformerConfigCollection sourceTransformers;
                sourceTransformers = this.getTransformers();
                TransformerConfigCollection copyTransformers = ((TransformerConfigCollection) strategy.copy(LocatorUtils.property(locator, "transformers", sourceTransformers), sourceTransformers));
                copy.setTransformers(copyTransformers);
            } else {
                copy.transformers = null;
            }
            if (this.applicationsLoadConfig!= null) {
                ApplicationLoadConfigCollection sourceApplicationsLoadConfig;
                sourceApplicationsLoadConfig = this.getApplicationsLoadConfig();
                ApplicationLoadConfigCollection copyApplicationsLoadConfig = ((ApplicationLoadConfigCollection) strategy.copy(LocatorUtils.property(locator, "applicationsLoadConfig", sourceApplicationsLoadConfig), sourceApplicationsLoadConfig));
                copy.setApplicationsLoadConfig(copyApplicationsLoadConfig);
            } else {
                copy.applicationsLoadConfig = null;
            }
            if (this.mode!= null) {
                BuildMode sourceMode;
                sourceMode = this.getMode();
                BuildMode copyMode = ((BuildMode) strategy.copy(LocatorUtils.property(locator, "mode", sourceMode), sourceMode));
                copy.setMode(copyMode);
            } else {
                copy.mode = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new BuildConfig();
    }

}
