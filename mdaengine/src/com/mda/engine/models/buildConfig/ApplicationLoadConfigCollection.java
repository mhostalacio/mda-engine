
package com.mda.engine.models.buildConfig;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ApplicationLoadConfigCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ApplicationLoadConfigCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Application" type="{http://www.mdaengine.com/mdaengine/models/buildConfig}ApplicationLoadConfig" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApplicationLoadConfigCollection", propOrder = {
    "application"
})
public class ApplicationLoadConfigCollection
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Application")
    protected List<ApplicationLoadConfig> application;

    /**
     * Gets the value of the application property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the application property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getApplication().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ApplicationLoadConfig }
     * 
     * 
     */
    public List<ApplicationLoadConfig> getApplication() {
        if (application == null) {
            application = new ArrayList<ApplicationLoadConfig>();
        }
        return this.application;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ApplicationLoadConfigCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ApplicationLoadConfigCollection that = ((ApplicationLoadConfigCollection) object);
        {
            List<ApplicationLoadConfig> lhsApplication;
            lhsApplication = this.getApplication();
            List<ApplicationLoadConfig> rhsApplication;
            rhsApplication = that.getApplication();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "application", lhsApplication), LocatorUtils.property(thatLocator, "application", rhsApplication), lhsApplication, rhsApplication)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof ApplicationLoadConfigCollection) {
            final ApplicationLoadConfigCollection copy = ((ApplicationLoadConfigCollection) draftCopy);
            if ((this.application!= null)&&(!this.application.isEmpty())) {
                List<ApplicationLoadConfig> sourceApplication;
                sourceApplication = this.getApplication();
                @SuppressWarnings("unchecked")
                List<ApplicationLoadConfig> copyApplication = ((List<ApplicationLoadConfig> ) strategy.copy(LocatorUtils.property(locator, "application", sourceApplication), sourceApplication));
                copy.application = null;
                List<ApplicationLoadConfig> uniqueApplicationl = copy.getApplication();
                uniqueApplicationl.addAll(copyApplication);
            } else {
                copy.application = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ApplicationLoadConfigCollection();
    }

}
