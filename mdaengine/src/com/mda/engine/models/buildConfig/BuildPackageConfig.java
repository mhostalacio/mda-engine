
package com.mda.engine.models.buildConfig;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for BuildPackageConfig complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BuildPackageConfig">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Layers" type="{http://www.mdaengine.com/mdaengine/models/buildConfig}BuildLayerConfigCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BuildPackageConfig", propOrder = {
    "layers"
})
public class BuildPackageConfig
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Layers")
    protected BuildLayerConfigCollection layers;
    @XmlAttribute(name = "Name", required = true)
    protected String name;

    /**
     * Gets the value of the layers property.
     * 
     * @return
     *     possible object is
     *     {@link BuildLayerConfigCollection }
     *     
     */
    public BuildLayerConfigCollection getLayers() {
        return layers;
    }

    /**
     * Sets the value of the layers property.
     * 
     * @param value
     *     allowed object is
     *     {@link BuildLayerConfigCollection }
     *     
     */
    public void setLayers(BuildLayerConfigCollection value) {
        this.layers = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof BuildPackageConfig)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final BuildPackageConfig that = ((BuildPackageConfig) object);
        {
            BuildLayerConfigCollection lhsLayers;
            lhsLayers = this.getLayers();
            BuildLayerConfigCollection rhsLayers;
            rhsLayers = that.getLayers();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "layers", lhsLayers), LocatorUtils.property(thatLocator, "layers", rhsLayers), lhsLayers, rhsLayers)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof BuildPackageConfig) {
            final BuildPackageConfig copy = ((BuildPackageConfig) draftCopy);
            if (this.layers!= null) {
                BuildLayerConfigCollection sourceLayers;
                sourceLayers = this.getLayers();
                BuildLayerConfigCollection copyLayers = ((BuildLayerConfigCollection) strategy.copy(LocatorUtils.property(locator, "layers", sourceLayers), sourceLayers));
                copy.setLayers(copyLayers);
            } else {
                copy.layers = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new BuildPackageConfig();
    }

}
