
package com.mda.engine.models.buildConfig;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for BuildLayerConfigCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BuildLayerConfigCollection">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/buildConfig}InclusionBase">
 *       &lt;sequence>
 *         &lt;element name="Layer" type="{http://www.mdaengine.com/mdaengine/models/buildConfig}BuildLayerConfig" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BuildLayerConfigCollection", propOrder = {
    "layer"
})
public class BuildLayerConfigCollection
    extends InclusionBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Layer")
    protected List<BuildLayerConfig> layer;

    /**
     * Gets the value of the layer property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the layer property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLayer().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BuildLayerConfig }
     * 
     * 
     */
    public List<BuildLayerConfig> getLayer() {
        if (layer == null) {
            layer = new ArrayList<BuildLayerConfig>();
        }
        return this.layer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof BuildLayerConfigCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final BuildLayerConfigCollection that = ((BuildLayerConfigCollection) object);
        {
            List<BuildLayerConfig> lhsLayer;
            lhsLayer = this.getLayer();
            List<BuildLayerConfig> rhsLayer;
            rhsLayer = that.getLayer();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "layer", lhsLayer), LocatorUtils.property(thatLocator, "layer", rhsLayer), lhsLayer, rhsLayer)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof BuildLayerConfigCollection) {
            final BuildLayerConfigCollection copy = ((BuildLayerConfigCollection) draftCopy);
            if ((this.layer!= null)&&(!this.layer.isEmpty())) {
                List<BuildLayerConfig> sourceLayer;
                sourceLayer = this.getLayer();
                @SuppressWarnings("unchecked")
                List<BuildLayerConfig> copyLayer = ((List<BuildLayerConfig> ) strategy.copy(LocatorUtils.property(locator, "layer", sourceLayer), sourceLayer));
                copy.layer = null;
                List<BuildLayerConfig> uniqueLayerl = copy.getLayer();
                uniqueLayerl.addAll(copyLayer);
            } else {
                copy.layer = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new BuildLayerConfigCollection();
    }

}
