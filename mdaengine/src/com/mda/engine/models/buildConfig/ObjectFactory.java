
package com.mda.engine.models.buildConfig;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.mda.engine.models.buildConfig package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Config_QNAME = new QName("http://www.mdaengine.com/mdaengine/models/buildConfig", "Config");
    private final static QName _GenerationInfo_QNAME = new QName("http://www.mdaengine.com/mdaengine/models/buildConfig", "GenerationInfo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.mda.engine.models.buildConfig
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link BuildPackageConfigCollection }
     * 
     */
    public BuildPackageConfigCollection createBuildPackageConfigCollection() {
        return new BuildPackageConfigCollection();
    }

    /**
     * Create an instance of {@link ApplicationLoadConfigCollection }
     * 
     */
    public ApplicationLoadConfigCollection createApplicationLoadConfigCollection() {
        return new ApplicationLoadConfigCollection();
    }

    /**
     * Create an instance of {@link BuildConfig }
     * 
     */
    public BuildConfig createBuildConfig() {
        return new BuildConfig();
    }

    /**
     * Create an instance of {@link BuildLayerConfigCollection }
     * 
     */
    public BuildLayerConfigCollection createBuildLayerConfigCollection() {
        return new BuildLayerConfigCollection();
    }

    /**
     * Create an instance of {@link ApplicationLoadConfig }
     * 
     */
    public ApplicationLoadConfig createApplicationLoadConfig() {
        return new ApplicationLoadConfig();
    }

    /**
     * Create an instance of {@link TransformerConfigCollection }
     * 
     */
    public TransformerConfigCollection createTransformerConfigCollection() {
        return new TransformerConfigCollection();
    }

    /**
     * Create an instance of {@link GenerationInfo }
     * 
     */
    public GenerationInfo createGenerationInfo() {
        return new GenerationInfo();
    }

    /**
     * Create an instance of {@link BuildPackageConfig }
     * 
     */
    public BuildPackageConfig createBuildPackageConfig() {
        return new BuildPackageConfig();
    }

    /**
     * Create an instance of {@link BuildApplicationConfig }
     * 
     */
    public BuildApplicationConfig createBuildApplicationConfig() {
        return new BuildApplicationConfig();
    }

    /**
     * Create an instance of {@link InclusionBase }
     * 
     */
    public InclusionBase createInclusionBase() {
        return new InclusionBase();
    }

    /**
     * Create an instance of {@link BuildLayerConfig }
     * 
     */
    public BuildLayerConfig createBuildLayerConfig() {
        return new BuildLayerConfig();
    }

    /**
     * Create an instance of {@link BuildApplicationConfigCollection }
     * 
     */
    public BuildApplicationConfigCollection createBuildApplicationConfigCollection() {
        return new BuildApplicationConfigCollection();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuildConfig }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.mdaengine.com/mdaengine/models/buildConfig", name = "Config")
    public JAXBElement<BuildConfig> createConfig(BuildConfig value) {
        return new JAXBElement<BuildConfig>(_Config_QNAME, BuildConfig.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerationInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.mdaengine.com/mdaengine/models/buildConfig", name = "GenerationInfo")
    public JAXBElement<GenerationInfo> createGenerationInfo(GenerationInfo value) {
        return new JAXBElement<GenerationInfo>(_GenerationInfo_QNAME, GenerationInfo.class, null, value);
    }

}
