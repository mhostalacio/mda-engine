
package com.mda.engine.models.buildConfig;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BuildMode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BuildMode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Full"/>
 *     &lt;enumeration value="Incremental"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BuildMode")
@XmlEnum
public enum BuildMode {

    @XmlEnumValue("Full")
    FULL("Full"),
    @XmlEnumValue("Incremental")
    INCREMENTAL("Incremental");
    private final String value;

    BuildMode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BuildMode fromValue(String v) {
        for (BuildMode c: BuildMode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
