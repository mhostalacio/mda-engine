
package com.mda.engine.models.buildConfig;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for InclusionBase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InclusionBase">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="Mode" type="{http://www.mdaengine.com/mdaengine/models/buildConfig}BuildInclusionMode" default="Inclusive" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InclusionBase")
@XmlSeeAlso({
    BuildApplicationConfig.class,
    BuildLayerConfigCollection.class,
    BuildPackageConfigCollection.class
})
public class InclusionBase
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "Mode")
    protected BuildInclusionMode mode;

    /**
     * Gets the value of the mode property.
     * 
     * @return
     *     possible object is
     *     {@link BuildInclusionMode }
     *     
     */
    public BuildInclusionMode getMode() {
        if (mode == null) {
            return BuildInclusionMode.INCLUSIVE;
        } else {
            return mode;
        }
    }

    /**
     * Sets the value of the mode property.
     * 
     * @param value
     *     allowed object is
     *     {@link BuildInclusionMode }
     *     
     */
    public void setMode(BuildInclusionMode value) {
        this.mode = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof InclusionBase)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final InclusionBase that = ((InclusionBase) object);
        {
            BuildInclusionMode lhsMode;
            lhsMode = this.getMode();
            BuildInclusionMode rhsMode;
            rhsMode = that.getMode();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "mode", lhsMode), LocatorUtils.property(thatLocator, "mode", rhsMode), lhsMode, rhsMode)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof InclusionBase) {
            final InclusionBase copy = ((InclusionBase) draftCopy);
            if (this.mode!= null) {
                BuildInclusionMode sourceMode;
                sourceMode = this.getMode();
                BuildInclusionMode copyMode = ((BuildInclusionMode) strategy.copy(LocatorUtils.property(locator, "mode", sourceMode), sourceMode));
                copy.setMode(copyMode);
            } else {
                copy.mode = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new InclusionBase();
    }

}
