
package com.mda.engine.models.buildConfig;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BuildLayer.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BuildLayer">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DataBase"/>
 *     &lt;enumeration value="EntitiesAndRepositories"/>
 *     &lt;enumeration value="BusinessTransactions"/>
 *     &lt;enumeration value="Services"/>
 *     &lt;enumeration value="MVC"/>
 *     &lt;enumeration value="Tests"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BuildLayer")
@XmlEnum
public enum BuildLayer {

    @XmlEnumValue("DataBase")
    DATA_BASE("DataBase"),
    @XmlEnumValue("EntitiesAndRepositories")
    ENTITIES_AND_REPOSITORIES("EntitiesAndRepositories"),
    @XmlEnumValue("BusinessTransactions")
    BUSINESS_TRANSACTIONS("BusinessTransactions"),
    @XmlEnumValue("Services")
    SERVICES("Services"),
    MVC("MVC"),
    @XmlEnumValue("Tests")
    TESTS("Tests");
    private final String value;

    BuildLayer(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BuildLayer fromValue(String v) {
        for (BuildLayer c: BuildLayer.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
