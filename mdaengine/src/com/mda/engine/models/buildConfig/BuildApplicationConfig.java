
package com.mda.engine.models.buildConfig;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for BuildApplicationConfig complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BuildApplicationConfig">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/buildConfig}InclusionBase">
 *       &lt;sequence>
 *         &lt;element name="Layers" type="{http://www.mdaengine.com/mdaengine/models/buildConfig}BuildLayerConfigCollection" minOccurs="0"/>
 *         &lt;element name="Packages" type="{http://www.mdaengine.com/mdaengine/models/buildConfig}BuildPackageConfigCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="IncludeDependencies" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BuildApplicationConfig", propOrder = {
    "layers",
    "packages"
})
public class BuildApplicationConfig
    extends InclusionBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Layers")
    protected BuildLayerConfigCollection layers;
    @XmlElement(name = "Packages")
    protected BuildPackageConfigCollection packages;
    @XmlAttribute(name = "Name", required = true)
    protected String name;
    @XmlAttribute(name = "IncludeDependencies")
    protected Boolean includeDependencies;

    /**
     * Gets the value of the layers property.
     * 
     * @return
     *     possible object is
     *     {@link BuildLayerConfigCollection }
     *     
     */
    public BuildLayerConfigCollection getLayers() {
        return layers;
    }

    /**
     * Sets the value of the layers property.
     * 
     * @param value
     *     allowed object is
     *     {@link BuildLayerConfigCollection }
     *     
     */
    public void setLayers(BuildLayerConfigCollection value) {
        this.layers = value;
    }

    /**
     * Gets the value of the packages property.
     * 
     * @return
     *     possible object is
     *     {@link BuildPackageConfigCollection }
     *     
     */
    public BuildPackageConfigCollection getPackages() {
        return packages;
    }

    /**
     * Sets the value of the packages property.
     * 
     * @param value
     *     allowed object is
     *     {@link BuildPackageConfigCollection }
     *     
     */
    public void setPackages(BuildPackageConfigCollection value) {
        this.packages = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the includeDependencies property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isIncludeDependencies() {
        if (includeDependencies == null) {
            return false;
        } else {
            return includeDependencies;
        }
    }

    /**
     * Sets the value of the includeDependencies property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeDependencies(Boolean value) {
        this.includeDependencies = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof BuildApplicationConfig)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final BuildApplicationConfig that = ((BuildApplicationConfig) object);
        {
            BuildLayerConfigCollection lhsLayers;
            lhsLayers = this.getLayers();
            BuildLayerConfigCollection rhsLayers;
            rhsLayers = that.getLayers();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "layers", lhsLayers), LocatorUtils.property(thatLocator, "layers", rhsLayers), lhsLayers, rhsLayers)) {
                return false;
            }
        }
        {
            BuildPackageConfigCollection lhsPackages;
            lhsPackages = this.getPackages();
            BuildPackageConfigCollection rhsPackages;
            rhsPackages = that.getPackages();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "packages", lhsPackages), LocatorUtils.property(thatLocator, "packages", rhsPackages), lhsPackages, rhsPackages)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            boolean lhsIncludeDependencies;
            lhsIncludeDependencies = this.isIncludeDependencies();
            boolean rhsIncludeDependencies;
            rhsIncludeDependencies = that.isIncludeDependencies();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "includeDependencies", lhsIncludeDependencies), LocatorUtils.property(thatLocator, "includeDependencies", rhsIncludeDependencies), lhsIncludeDependencies, rhsIncludeDependencies)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof BuildApplicationConfig) {
            final BuildApplicationConfig copy = ((BuildApplicationConfig) draftCopy);
            if (this.layers!= null) {
                BuildLayerConfigCollection sourceLayers;
                sourceLayers = this.getLayers();
                BuildLayerConfigCollection copyLayers = ((BuildLayerConfigCollection) strategy.copy(LocatorUtils.property(locator, "layers", sourceLayers), sourceLayers));
                copy.setLayers(copyLayers);
            } else {
                copy.layers = null;
            }
            if (this.packages!= null) {
                BuildPackageConfigCollection sourcePackages;
                sourcePackages = this.getPackages();
                BuildPackageConfigCollection copyPackages = ((BuildPackageConfigCollection) strategy.copy(LocatorUtils.property(locator, "packages", sourcePackages), sourcePackages));
                copy.setPackages(copyPackages);
            } else {
                copy.packages = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.includeDependencies!= null) {
                boolean sourceIncludeDependencies;
                sourceIncludeDependencies = this.isIncludeDependencies();
                boolean copyIncludeDependencies = strategy.copy(LocatorUtils.property(locator, "includeDependencies", sourceIncludeDependencies), sourceIncludeDependencies);
                copy.setIncludeDependencies(copyIncludeDependencies);
            } else {
                copy.includeDependencies = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new BuildApplicationConfig();
    }

}
