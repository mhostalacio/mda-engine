
package com.mda.engine.models.buildConfig;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for M2TTransformerName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="M2TTransformerName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Database"/>
 *     &lt;enumeration value="Entity"/>
 *     &lt;enumeration value="Interface"/>
 *     &lt;enumeration value="EntityHelper"/>
 *     &lt;enumeration value="EntityRepository"/>
 *     &lt;enumeration value="EntitiesXsd"/>
 *     &lt;enumeration value="BusinessTransactions"/>
 *     &lt;enumeration value="DataContract"/>
 *     &lt;enumeration value="ServiceContract"/>
 *     &lt;enumeration value="DataContractsAdapter"/>
 *     &lt;enumeration value="ServiceImplementation"/>
 *     &lt;enumeration value="ServiceAdapter"/>
 *     &lt;enumeration value="ServiceHost"/>
 *     &lt;enumeration value="ServiceClient"/>
 *     &lt;enumeration value="MVCHelper"/>
 *     &lt;enumeration value="MVCModels"/>
 *     &lt;enumeration value="MVCInterfaceModels"/>
 *     &lt;enumeration value="MVCLovModels"/>
 *     &lt;enumeration value="MVCControllers"/>
 *     &lt;enumeration value="MVCRDIControllers"/>
 *     &lt;enumeration value="MVCViewCSharp"/>
 *     &lt;enumeration value="MVCViewAspx"/>
 *     &lt;enumeration value="MVCAreaWebConfig"/>
 *     &lt;enumeration value="MVCRDIViewAspx"/>
 *     &lt;enumeration value="MVCModelsAdapter"/>
 *     &lt;enumeration value="CM_MS_Build"/>
 *     &lt;enumeration value="CMMApplicationsToHTMLDocumentionTransformer"/>
 *     &lt;enumeration value="CM_NANT_BuildTargets"/>
 *     &lt;enumeration value="CM_NANT_DatabaseTargets"/>
 *     &lt;enumeration value="CM_NANT_DeployTargets"/>
 *     &lt;enumeration value="CM_NANT_IISTargets"/>
 *     &lt;enumeration value="DatabaseSchemaProjectToBuildMarkerFileTransformer"/>
 *     &lt;enumeration value="DatabaseLoaderProjectToBuildMarkerFileTransformer"/>
 *     &lt;enumeration value="MDACSharpApplicationToServicesEnvRideEndpointsFileTransformer"/>
 *     &lt;enumeration value="PropertiesFileTransformer"/>
 *     &lt;enumeration value="MVCViewModelToCSharpTransformer"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "M2TTransformerName")
@XmlEnum
public enum M2TTransformerName {

    @XmlEnumValue("Database")
    DATABASE("Database"),
    @XmlEnumValue("Entity")
    ENTITY("Entity"),
    @XmlEnumValue("Interface")
    INTERFACE("Interface"),
    @XmlEnumValue("EntityHelper")
    ENTITY_HELPER("EntityHelper"),
    @XmlEnumValue("EntityRepository")
    ENTITY_REPOSITORY("EntityRepository"),
    @XmlEnumValue("EntitiesXsd")
    ENTITIES_XSD("EntitiesXsd"),
    @XmlEnumValue("BusinessTransactions")
    BUSINESS_TRANSACTIONS("BusinessTransactions"),
    @XmlEnumValue("DataContract")
    DATA_CONTRACT("DataContract"),
    @XmlEnumValue("ServiceContract")
    SERVICE_CONTRACT("ServiceContract"),
    @XmlEnumValue("DataContractsAdapter")
    DATA_CONTRACTS_ADAPTER("DataContractsAdapter"),
    @XmlEnumValue("ServiceImplementation")
    SERVICE_IMPLEMENTATION("ServiceImplementation"),
    @XmlEnumValue("ServiceAdapter")
    SERVICE_ADAPTER("ServiceAdapter"),
    @XmlEnumValue("ServiceHost")
    SERVICE_HOST("ServiceHost"),
    @XmlEnumValue("ServiceClient")
    SERVICE_CLIENT("ServiceClient"),
    @XmlEnumValue("MVCHelper")
    MVC_HELPER("MVCHelper"),
    @XmlEnumValue("MVCModels")
    MVC_MODELS("MVCModels"),
    @XmlEnumValue("MVCInterfaceModels")
    MVC_INTERFACE_MODELS("MVCInterfaceModels"),
    @XmlEnumValue("MVCLovModels")
    MVC_LOV_MODELS("MVCLovModels"),
    @XmlEnumValue("MVCControllers")
    MVC_CONTROLLERS("MVCControllers"),
    @XmlEnumValue("MVCRDIControllers")
    MVCRDI_CONTROLLERS("MVCRDIControllers"),
    @XmlEnumValue("MVCViewCSharp")
    MVC_VIEW_C_SHARP("MVCViewCSharp"),
    @XmlEnumValue("MVCViewAspx")
    MVC_VIEW_ASPX("MVCViewAspx"),
    @XmlEnumValue("MVCAreaWebConfig")
    MVC_AREA_WEB_CONFIG("MVCAreaWebConfig"),
    @XmlEnumValue("MVCRDIViewAspx")
    MVCRDI_VIEW_ASPX("MVCRDIViewAspx"),
    @XmlEnumValue("MVCModelsAdapter")
    MVC_MODELS_ADAPTER("MVCModelsAdapter"),
    @XmlEnumValue("CM_MS_Build")
    CM_MS_BUILD("CM_MS_Build"),
    @XmlEnumValue("CMMApplicationsToHTMLDocumentionTransformer")
    CMM_APPLICATIONS_TO_HTML_DOCUMENTION_TRANSFORMER("CMMApplicationsToHTMLDocumentionTransformer"),
    @XmlEnumValue("CM_NANT_BuildTargets")
    CM_NANT_BUILD_TARGETS("CM_NANT_BuildTargets"),
    @XmlEnumValue("CM_NANT_DatabaseTargets")
    CM_NANT_DATABASE_TARGETS("CM_NANT_DatabaseTargets"),
    @XmlEnumValue("CM_NANT_DeployTargets")
    CM_NANT_DEPLOY_TARGETS("CM_NANT_DeployTargets"),
    @XmlEnumValue("CM_NANT_IISTargets")
    CM_NANT_IIS_TARGETS("CM_NANT_IISTargets"),
    @XmlEnumValue("DatabaseSchemaProjectToBuildMarkerFileTransformer")
    DATABASE_SCHEMA_PROJECT_TO_BUILD_MARKER_FILE_TRANSFORMER("DatabaseSchemaProjectToBuildMarkerFileTransformer"),
    @XmlEnumValue("DatabaseLoaderProjectToBuildMarkerFileTransformer")
    DATABASE_LOADER_PROJECT_TO_BUILD_MARKER_FILE_TRANSFORMER("DatabaseLoaderProjectToBuildMarkerFileTransformer"),
    @XmlEnumValue("MDACSharpApplicationToServicesEnvRideEndpointsFileTransformer")
    MDAC_SHARP_APPLICATION_TO_SERVICES_ENV_RIDE_ENDPOINTS_FILE_TRANSFORMER("MDACSharpApplicationToServicesEnvRideEndpointsFileTransformer"),
    @XmlEnumValue("PropertiesFileTransformer")
    PROPERTIES_FILE_TRANSFORMER("PropertiesFileTransformer"),
    @XmlEnumValue("MVCViewModelToCSharpTransformer")
    MVC_VIEW_MODEL_TO_C_SHARP_TRANSFORMER("MVCViewModelToCSharpTransformer");
    private final String value;

    M2TTransformerName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static M2TTransformerName fromValue(String v) {
        for (M2TTransformerName c: M2TTransformerName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
