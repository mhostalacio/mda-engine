
package com.mda.engine.models.buildConfig;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for TransformerConfigCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransformerConfigCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Transformer" type="{http://www.mdaengine.com/mdaengine/models/buildConfig}M2TTransformerName" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Mode" type="{http://www.mdaengine.com/mdaengine/models/buildConfig}BuildInclusionMode" default="Inclusive" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransformerConfigCollection", propOrder = {
    "transformer"
})
public class TransformerConfigCollection
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Transformer")
    protected List<M2TTransformerName> transformer;
    @XmlAttribute(name = "Mode")
    protected BuildInclusionMode mode;

    /**
     * Gets the value of the transformer property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transformer property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransformer().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link M2TTransformerName }
     * 
     * 
     */
    public List<M2TTransformerName> getTransformer() {
        if (transformer == null) {
            transformer = new ArrayList<M2TTransformerName>();
        }
        return this.transformer;
    }

    /**
     * Gets the value of the mode property.
     * 
     * @return
     *     possible object is
     *     {@link BuildInclusionMode }
     *     
     */
    public BuildInclusionMode getMode() {
        if (mode == null) {
            return BuildInclusionMode.INCLUSIVE;
        } else {
            return mode;
        }
    }

    /**
     * Sets the value of the mode property.
     * 
     * @param value
     *     allowed object is
     *     {@link BuildInclusionMode }
     *     
     */
    public void setMode(BuildInclusionMode value) {
        this.mode = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof TransformerConfigCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final TransformerConfigCollection that = ((TransformerConfigCollection) object);
        {
            List<M2TTransformerName> lhsTransformer;
            lhsTransformer = this.getTransformer();
            List<M2TTransformerName> rhsTransformer;
            rhsTransformer = that.getTransformer();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "transformer", lhsTransformer), LocatorUtils.property(thatLocator, "transformer", rhsTransformer), lhsTransformer, rhsTransformer)) {
                return false;
            }
        }
        {
            BuildInclusionMode lhsMode;
            lhsMode = this.getMode();
            BuildInclusionMode rhsMode;
            rhsMode = that.getMode();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "mode", lhsMode), LocatorUtils.property(thatLocator, "mode", rhsMode), lhsMode, rhsMode)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof TransformerConfigCollection) {
            final TransformerConfigCollection copy = ((TransformerConfigCollection) draftCopy);
            if ((this.transformer!= null)&&(!this.transformer.isEmpty())) {
                List<M2TTransformerName> sourceTransformer;
                sourceTransformer = this.getTransformer();
                @SuppressWarnings("unchecked")
                List<M2TTransformerName> copyTransformer = ((List<M2TTransformerName> ) strategy.copy(LocatorUtils.property(locator, "transformer", sourceTransformer), sourceTransformer));
                copy.transformer = null;
                List<M2TTransformerName> uniqueTransformerl = copy.getTransformer();
                uniqueTransformerl.addAll(copyTransformer);
            } else {
                copy.transformer = null;
            }
            if (this.mode!= null) {
                BuildInclusionMode sourceMode;
                sourceMode = this.getMode();
                BuildInclusionMode copyMode = ((BuildInclusionMode) strategy.copy(LocatorUtils.property(locator, "mode", sourceMode), sourceMode));
                copy.setMode(copyMode);
            } else {
                copy.mode = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new TransformerConfigCollection();
    }

}
