
package com.mda.engine.models.rulesModel;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.mda.engine.models.rulesModel package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ObjectValidation_QNAME = new QName("http://www.mdaengine.com/mdaengine/models/rulesModel", "ObjectValidation");
    private final static QName _Rule_QNAME = new QName("http://www.mdaengine.com/mdaengine/models/rulesModel", "Rule");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.mda.engine.models.rulesModel
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RegexValidationRule }
     * 
     */
    public RegexValidationRule createRegexValidationRule() {
        return new RegexValidationRule();
    }

    /**
     * Create an instance of {@link GreaterOrEqualThanValidationRule }
     * 
     */
    public GreaterOrEqualThanValidationRule createGreaterOrEqualThanValidationRule() {
        return new GreaterOrEqualThanValidationRule();
    }

    /**
     * Create an instance of {@link ValueNodeValidationRule }
     * 
     */
    public ValueNodeValidationRule createValueNodeValidationRule() {
        return new ValueNodeValidationRule();
    }

    /**
     * Create an instance of {@link EqualsValidationRule }
     * 
     */
    public EqualsValidationRule createEqualsValidationRule() {
        return new EqualsValidationRule();
    }

    /**
     * Create an instance of {@link ValidationRule }
     * 
     */
    public ValidationRule createValidationRule() {
        return new ValidationRule();
    }

    /**
     * Create an instance of {@link RequiredValidationRule }
     * 
     */
    public RequiredValidationRule createRequiredValidationRule() {
        return new RequiredValidationRule();
    }

    /**
     * Create an instance of {@link SetVisibleRuleAction }
     * 
     */
    public SetVisibleRuleAction createSetVisibleRuleAction() {
        return new SetVisibleRuleAction();
    }

    /**
     * Create an instance of {@link RangeValidationRule }
     * 
     */
    public RangeValidationRule createRangeValidationRule() {
        return new RangeValidationRule();
    }

    /**
     * Create an instance of {@link LowerOrEqualThanValidationRule }
     * 
     */
    public LowerOrEqualThanValidationRule createLowerOrEqualThanValidationRule() {
        return new LowerOrEqualThanValidationRule();
    }

    /**
     * Create an instance of {@link AttributeValidationScope }
     * 
     */
    public AttributeValidationScope createAttributeValidationScope() {
        return new AttributeValidationScope();
    }

    /**
     * Create an instance of {@link ComparisonValidationRule }
     * 
     */
    public ComparisonValidationRule createComparisonValidationRule() {
        return new ComparisonValidationRule();
    }

    /**
     * Create an instance of {@link SetOptionalRuleAction }
     * 
     */
    public SetOptionalRuleAction createSetOptionalRuleAction() {
        return new SetOptionalRuleAction();
    }

    /**
     * Create an instance of {@link ExpressionValidationRule }
     * 
     */
    public ExpressionValidationRule createExpressionValidationRule() {
        return new ExpressionValidationRule();
    }

    /**
     * Create an instance of {@link AttributeValidationScopeCollection }
     * 
     */
    public AttributeValidationScopeCollection createAttributeValidationScopeCollection() {
        return new AttributeValidationScopeCollection();
    }

    /**
     * Create an instance of {@link ValidationScope }
     * 
     */
    public ValidationScope createValidationScope() {
        return new ValidationScope();
    }

    /**
     * Create an instance of {@link UrlRegexValidationRule }
     * 
     */
    public UrlRegexValidationRule createUrlRegexValidationRule() {
        return new UrlRegexValidationRule();
    }

    /**
     * Create an instance of {@link ValidationRuleCollection }
     * 
     */
    public ValidationRuleCollection createValidationRuleCollection() {
        return new ValidationRuleCollection();
    }

    /**
     * Create an instance of {@link Rule }
     * 
     */
    public Rule createRule() {
        return new Rule();
    }

    /**
     * Create an instance of {@link SetRequiredRuleAction }
     * 
     */
    public SetRequiredRuleAction createSetRequiredRuleAction() {
        return new SetRequiredRuleAction();
    }

    /**
     * Create an instance of {@link ObjectValidationScope }
     * 
     */
    public ObjectValidationScope createObjectValidationScope() {
        return new ObjectValidationScope();
    }

    /**
     * Create an instance of {@link SetHiddenRuleAction }
     * 
     */
    public SetHiddenRuleAction createSetHiddenRuleAction() {
        return new SetHiddenRuleAction();
    }

    /**
     * Create an instance of {@link CustomRegexValidationRule }
     * 
     */
    public CustomRegexValidationRule createCustomRegexValidationRule() {
        return new CustomRegexValidationRule();
    }

    /**
     * Create an instance of {@link SetInvalidRuleAction }
     * 
     */
    public SetInvalidRuleAction createSetInvalidRuleAction() {
        return new SetInvalidRuleAction();
    }

    /**
     * Create an instance of {@link LowerThanValidationRule }
     * 
     */
    public LowerThanValidationRule createLowerThanValidationRule() {
        return new LowerThanValidationRule();
    }

    /**
     * Create an instance of {@link RuleActionCollection }
     * 
     */
    public RuleActionCollection createRuleActionCollection() {
        return new RuleActionCollection();
    }

    /**
     * Create an instance of {@link EntityValidationScope }
     * 
     */
    public EntityValidationScope createEntityValidationScope() {
        return new EntityValidationScope();
    }

    /**
     * Create an instance of {@link GreaterThanValidationRule }
     * 
     */
    public GreaterThanValidationRule createGreaterThanValidationRule() {
        return new GreaterThanValidationRule();
    }

    /**
     * Create an instance of {@link RequiredMultiLanguageValidationRule }
     * 
     */
    public RequiredMultiLanguageValidationRule createRequiredMultiLanguageValidationRule() {
        return new RequiredMultiLanguageValidationRule();
    }

    /**
     * Create an instance of {@link RuleAction }
     * 
     */
    public RuleAction createRuleAction() {
        return new RuleAction();
    }

    /**
     * Create an instance of {@link EmailRegexValidationRule }
     * 
     */
    public EmailRegexValidationRule createEmailRegexValidationRule() {
        return new EmailRegexValidationRule();
    }

    /**
     * Create an instance of {@link CustomValidationRule }
     * 
     */
    public CustomValidationRule createCustomValidationRule() {
        return new CustomValidationRule();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectValidationScope }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.mdaengine.com/mdaengine/models/rulesModel", name = "ObjectValidation")
    public JAXBElement<ObjectValidationScope> createObjectValidation(ObjectValidationScope value) {
        return new JAXBElement<ObjectValidationScope>(_ObjectValidation_QNAME, ObjectValidationScope.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Rule }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.mdaengine.com/mdaengine/models/rulesModel", name = "Rule")
    public JAXBElement<Rule> createRule(Rule value) {
        return new JAXBElement<Rule>(_Rule_QNAME, Rule.class, null, value);
    }

}
