
package com.mda.engine.models.rulesModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for RuleActionCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RuleActionCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;element name="SetInvalid" type="{http://www.mdaengine.com/mdaengine/models/rulesModel}SetInvalidRuleAction"/>
 *         &lt;element name="SetHidden" type="{http://www.mdaengine.com/mdaengine/models/rulesModel}SetHiddenRuleAction"/>
 *         &lt;element name="SetVisible" type="{http://www.mdaengine.com/mdaengine/models/rulesModel}SetVisibleRuleAction"/>
 *         &lt;element name="SetOptional" type="{http://www.mdaengine.com/mdaengine/models/rulesModel}SetOptionalRuleAction"/>
 *         &lt;element name="SetRequired" type="{http://www.mdaengine.com/mdaengine/models/rulesModel}SetRequiredRuleAction"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RuleActionCollection", propOrder = {
    "setInvalidOrSetHiddenOrSetVisible"
})
public class RuleActionCollection
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElements({
        @XmlElement(name = "SetHidden", type = SetHiddenRuleAction.class),
        @XmlElement(name = "SetRequired", type = SetRequiredRuleAction.class),
        @XmlElement(name = "SetInvalid", type = SetInvalidRuleAction.class),
        @XmlElement(name = "SetOptional", type = SetOptionalRuleAction.class),
        @XmlElement(name = "SetVisible", type = SetVisibleRuleAction.class)
    })
    protected List<RuleAction> setInvalidOrSetHiddenOrSetVisible;

    /**
     * Gets the value of the setInvalidOrSetHiddenOrSetVisible property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the setInvalidOrSetHiddenOrSetVisible property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSetInvalidOrSetHiddenOrSetVisible().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SetHiddenRuleAction }
     * {@link SetRequiredRuleAction }
     * {@link SetInvalidRuleAction }
     * {@link SetOptionalRuleAction }
     * {@link SetVisibleRuleAction }
     * 
     * 
     */
    public List<RuleAction> getSetInvalidOrSetHiddenOrSetVisible() {
        if (setInvalidOrSetHiddenOrSetVisible == null) {
            setInvalidOrSetHiddenOrSetVisible = new ArrayList<RuleAction>();
        }
        return this.setInvalidOrSetHiddenOrSetVisible;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof RuleActionCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final RuleActionCollection that = ((RuleActionCollection) object);
        {
            List<RuleAction> lhsSetInvalidOrSetHiddenOrSetVisible;
            lhsSetInvalidOrSetHiddenOrSetVisible = this.getSetInvalidOrSetHiddenOrSetVisible();
            List<RuleAction> rhsSetInvalidOrSetHiddenOrSetVisible;
            rhsSetInvalidOrSetHiddenOrSetVisible = that.getSetInvalidOrSetHiddenOrSetVisible();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "setInvalidOrSetHiddenOrSetVisible", lhsSetInvalidOrSetHiddenOrSetVisible), LocatorUtils.property(thatLocator, "setInvalidOrSetHiddenOrSetVisible", rhsSetInvalidOrSetHiddenOrSetVisible), lhsSetInvalidOrSetHiddenOrSetVisible, rhsSetInvalidOrSetHiddenOrSetVisible)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof RuleActionCollection) {
            final RuleActionCollection copy = ((RuleActionCollection) draftCopy);
            if ((this.setInvalidOrSetHiddenOrSetVisible!= null)&&(!this.setInvalidOrSetHiddenOrSetVisible.isEmpty())) {
                List<RuleAction> sourceSetInvalidOrSetHiddenOrSetVisible;
                sourceSetInvalidOrSetHiddenOrSetVisible = this.getSetInvalidOrSetHiddenOrSetVisible();
                @SuppressWarnings("unchecked")
                List<RuleAction> copySetInvalidOrSetHiddenOrSetVisible = ((List<RuleAction> ) strategy.copy(LocatorUtils.property(locator, "setInvalidOrSetHiddenOrSetVisible", sourceSetInvalidOrSetHiddenOrSetVisible), sourceSetInvalidOrSetHiddenOrSetVisible));
                copy.setInvalidOrSetHiddenOrSetVisible = null;
                List<RuleAction> uniqueSetInvalidOrSetHiddenOrSetVisiblel = copy.getSetInvalidOrSetHiddenOrSetVisible();
                uniqueSetInvalidOrSetHiddenOrSetVisiblel.addAll(copySetInvalidOrSetHiddenOrSetVisible);
            } else {
                copy.setInvalidOrSetHiddenOrSetVisible = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new RuleActionCollection();
    }

}
