
package com.mda.engine.models.rulesModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ValidationRuleCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValidationRuleCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;element name="Required" type="{http://www.mdaengine.com/mdaengine/models/rulesModel}RequiredValidationRule"/>
 *         &lt;element name="Regex" type="{http://www.mdaengine.com/mdaengine/models/rulesModel}CustomRegexValidationRule"/>
 *         &lt;element name="Email" type="{http://www.mdaengine.com/mdaengine/models/rulesModel}EmailRegexValidationRule"/>
 *         &lt;element name="Url" type="{http://www.mdaengine.com/mdaengine/models/rulesModel}UrlRegexValidationRule"/>
 *         &lt;element name="Range" type="{http://www.mdaengine.com/mdaengine/models/rulesModel}RangeValidationRule"/>
 *         &lt;element name="ExpressionValidation" type="{http://www.mdaengine.com/mdaengine/models/rulesModel}ExpressionValidationRule"/>
 *         &lt;element name="CustomValidation" type="{http://www.mdaengine.com/mdaengine/models/rulesModel}CustomValidationRule"/>
 *         &lt;element name="GreaterThan" type="{http://www.mdaengine.com/mdaengine/models/rulesModel}GreaterThanValidationRule"/>
 *         &lt;element name="GreaterOrEqualThan" type="{http://www.mdaengine.com/mdaengine/models/rulesModel}GreaterOrEqualThanValidationRule"/>
 *         &lt;element name="LowerThan" type="{http://www.mdaengine.com/mdaengine/models/rulesModel}LowerThanValidationRule"/>
 *         &lt;element name="LowerOrEqualThan" type="{http://www.mdaengine.com/mdaengine/models/rulesModel}LowerOrEqualThanValidationRule"/>
 *         &lt;element name="Equals" type="{http://www.mdaengine.com/mdaengine/models/rulesModel}EqualsValidationRule"/>
 *         &lt;element name="RequiredMultiLanguage" type="{http://www.mdaengine.com/mdaengine/models/rulesModel}RequiredMultiLanguageValidationRule"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidationRuleCollection", propOrder = {
    "requiredOrRegexOrEmail"
})
public class ValidationRuleCollection
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElements({
        @XmlElement(name = "GreaterThan", type = GreaterThanValidationRule.class),
        @XmlElement(name = "GreaterOrEqualThan", type = GreaterOrEqualThanValidationRule.class),
        @XmlElement(name = "Regex", type = CustomRegexValidationRule.class),
        @XmlElement(name = "Equals", type = EqualsValidationRule.class),
        @XmlElement(name = "LowerOrEqualThan", type = LowerOrEqualThanValidationRule.class),
        @XmlElement(name = "RequiredMultiLanguage", type = RequiredMultiLanguageValidationRule.class),
        @XmlElement(name = "Email", type = EmailRegexValidationRule.class),
        @XmlElement(name = "CustomValidation", type = CustomValidationRule.class),
        @XmlElement(name = "Required", type = RequiredValidationRule.class),
        @XmlElement(name = "Range", type = RangeValidationRule.class),
        @XmlElement(name = "LowerThan", type = LowerThanValidationRule.class),
        @XmlElement(name = "ExpressionValidation", type = ExpressionValidationRule.class),
        @XmlElement(name = "Url", type = UrlRegexValidationRule.class)
    })
    protected List<ValidationRule> requiredOrRegexOrEmail;

    /**
     * Gets the value of the requiredOrRegexOrEmail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the requiredOrRegexOrEmail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRequiredOrRegexOrEmail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GreaterThanValidationRule }
     * {@link GreaterOrEqualThanValidationRule }
     * {@link CustomRegexValidationRule }
     * {@link EqualsValidationRule }
     * {@link LowerOrEqualThanValidationRule }
     * {@link RequiredMultiLanguageValidationRule }
     * {@link EmailRegexValidationRule }
     * {@link CustomValidationRule }
     * {@link RequiredValidationRule }
     * {@link RangeValidationRule }
     * {@link LowerThanValidationRule }
     * {@link ExpressionValidationRule }
     * {@link UrlRegexValidationRule }
     * 
     * 
     */
    public List<ValidationRule> getRequiredOrRegexOrEmail() {
        if (requiredOrRegexOrEmail == null) {
            requiredOrRegexOrEmail = new ArrayList<ValidationRule>();
        }
        return this.requiredOrRegexOrEmail;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ValidationRuleCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ValidationRuleCollection that = ((ValidationRuleCollection) object);
        {
            List<ValidationRule> lhsRequiredOrRegexOrEmail;
            lhsRequiredOrRegexOrEmail = this.getRequiredOrRegexOrEmail();
            List<ValidationRule> rhsRequiredOrRegexOrEmail;
            rhsRequiredOrRegexOrEmail = that.getRequiredOrRegexOrEmail();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "requiredOrRegexOrEmail", lhsRequiredOrRegexOrEmail), LocatorUtils.property(thatLocator, "requiredOrRegexOrEmail", rhsRequiredOrRegexOrEmail), lhsRequiredOrRegexOrEmail, rhsRequiredOrRegexOrEmail)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof ValidationRuleCollection) {
            final ValidationRuleCollection copy = ((ValidationRuleCollection) draftCopy);
            if ((this.requiredOrRegexOrEmail!= null)&&(!this.requiredOrRegexOrEmail.isEmpty())) {
                List<ValidationRule> sourceRequiredOrRegexOrEmail;
                sourceRequiredOrRegexOrEmail = this.getRequiredOrRegexOrEmail();
                @SuppressWarnings("unchecked")
                List<ValidationRule> copyRequiredOrRegexOrEmail = ((List<ValidationRule> ) strategy.copy(LocatorUtils.property(locator, "requiredOrRegexOrEmail", sourceRequiredOrRegexOrEmail), sourceRequiredOrRegexOrEmail));
                copy.requiredOrRegexOrEmail = null;
                List<ValidationRule> uniqueRequiredOrRegexOrEmaill = copy.getRequiredOrRegexOrEmail();
                uniqueRequiredOrRegexOrEmaill.addAll(copyRequiredOrRegexOrEmail);
            } else {
                copy.requiredOrRegexOrEmail = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ValidationRuleCollection();
    }

}
