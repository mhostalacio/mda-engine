
package com.mda.engine.models.rulesModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.models.expressionsModel.ExpressionChoice;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for RangeValidationRule complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RangeValidationRule">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/rulesModel}ValidationRule">
 *       &lt;sequence>
 *         &lt;element name="MinValue" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ExpressionChoice"/>
 *         &lt;element name="MinValueMode" type="{http://www.mdaengine.com/mdaengine/models/rulesModel}ValueMode"/>
 *         &lt;element name="MaxValue" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ExpressionChoice"/>
 *         &lt;element name="MaxValueMode" type="{http://www.mdaengine.com/mdaengine/models/rulesModel}ValueMode"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RangeValidationRule", propOrder = {
    "minValue",
    "minValueMode",
    "maxValue",
    "maxValueMode"
})
public class RangeValidationRule
    extends ValidationRule
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "MinValue", required = true)
    protected ExpressionChoice minValue;
    @XmlElement(name = "MinValueMode", required = true, defaultValue = "inclusive")
    protected ValueMode minValueMode;
    @XmlElement(name = "MaxValue", required = true)
    protected ExpressionChoice maxValue;
    @XmlElement(name = "MaxValueMode", required = true, defaultValue = "inclusive")
    protected ValueMode maxValueMode;

    /**
     * Gets the value of the minValue property.
     * 
     * @return
     *     possible object is
     *     {@link ExpressionChoice }
     *     
     */
    public ExpressionChoice getMinValue() {
        return minValue;
    }

    /**
     * Sets the value of the minValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpressionChoice }
     *     
     */
    public void setMinValue(ExpressionChoice value) {
        this.minValue = value;
    }

    /**
     * Gets the value of the minValueMode property.
     * 
     * @return
     *     possible object is
     *     {@link ValueMode }
     *     
     */
    public ValueMode getMinValueMode() {
        return minValueMode;
    }

    /**
     * Sets the value of the minValueMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValueMode }
     *     
     */
    public void setMinValueMode(ValueMode value) {
        this.minValueMode = value;
    }

    /**
     * Gets the value of the maxValue property.
     * 
     * @return
     *     possible object is
     *     {@link ExpressionChoice }
     *     
     */
    public ExpressionChoice getMaxValue() {
        return maxValue;
    }

    /**
     * Sets the value of the maxValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpressionChoice }
     *     
     */
    public void setMaxValue(ExpressionChoice value) {
        this.maxValue = value;
    }

    /**
     * Gets the value of the maxValueMode property.
     * 
     * @return
     *     possible object is
     *     {@link ValueMode }
     *     
     */
    public ValueMode getMaxValueMode() {
        return maxValueMode;
    }

    /**
     * Sets the value of the maxValueMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValueMode }
     *     
     */
    public void setMaxValueMode(ValueMode value) {
        this.maxValueMode = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof RangeValidationRule)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final RangeValidationRule that = ((RangeValidationRule) object);
        {
            ExpressionChoice lhsMinValue;
            lhsMinValue = this.getMinValue();
            ExpressionChoice rhsMinValue;
            rhsMinValue = that.getMinValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "minValue", lhsMinValue), LocatorUtils.property(thatLocator, "minValue", rhsMinValue), lhsMinValue, rhsMinValue)) {
                return false;
            }
        }
        {
            ValueMode lhsMinValueMode;
            lhsMinValueMode = this.getMinValueMode();
            ValueMode rhsMinValueMode;
            rhsMinValueMode = that.getMinValueMode();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "minValueMode", lhsMinValueMode), LocatorUtils.property(thatLocator, "minValueMode", rhsMinValueMode), lhsMinValueMode, rhsMinValueMode)) {
                return false;
            }
        }
        {
            ExpressionChoice lhsMaxValue;
            lhsMaxValue = this.getMaxValue();
            ExpressionChoice rhsMaxValue;
            rhsMaxValue = that.getMaxValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "maxValue", lhsMaxValue), LocatorUtils.property(thatLocator, "maxValue", rhsMaxValue), lhsMaxValue, rhsMaxValue)) {
                return false;
            }
        }
        {
            ValueMode lhsMaxValueMode;
            lhsMaxValueMode = this.getMaxValueMode();
            ValueMode rhsMaxValueMode;
            rhsMaxValueMode = that.getMaxValueMode();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "maxValueMode", lhsMaxValueMode), LocatorUtils.property(thatLocator, "maxValueMode", rhsMaxValueMode), lhsMaxValueMode, rhsMaxValueMode)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof RangeValidationRule) {
            final RangeValidationRule copy = ((RangeValidationRule) draftCopy);
            if (this.minValue!= null) {
                ExpressionChoice sourceMinValue;
                sourceMinValue = this.getMinValue();
                ExpressionChoice copyMinValue = ((ExpressionChoice) strategy.copy(LocatorUtils.property(locator, "minValue", sourceMinValue), sourceMinValue));
                copy.setMinValue(copyMinValue);
            } else {
                copy.minValue = null;
            }
            if (this.minValueMode!= null) {
                ValueMode sourceMinValueMode;
                sourceMinValueMode = this.getMinValueMode();
                ValueMode copyMinValueMode = ((ValueMode) strategy.copy(LocatorUtils.property(locator, "minValueMode", sourceMinValueMode), sourceMinValueMode));
                copy.setMinValueMode(copyMinValueMode);
            } else {
                copy.minValueMode = null;
            }
            if (this.maxValue!= null) {
                ExpressionChoice sourceMaxValue;
                sourceMaxValue = this.getMaxValue();
                ExpressionChoice copyMaxValue = ((ExpressionChoice) strategy.copy(LocatorUtils.property(locator, "maxValue", sourceMaxValue), sourceMaxValue));
                copy.setMaxValue(copyMaxValue);
            } else {
                copy.maxValue = null;
            }
            if (this.maxValueMode!= null) {
                ValueMode sourceMaxValueMode;
                sourceMaxValueMode = this.getMaxValueMode();
                ValueMode copyMaxValueMode = ((ValueMode) strategy.copy(LocatorUtils.property(locator, "maxValueMode", sourceMaxValueMode), sourceMaxValueMode));
                copy.setMaxValueMode(copyMaxValueMode);
            } else {
                copy.maxValueMode = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new RangeValidationRule();
    }

}
