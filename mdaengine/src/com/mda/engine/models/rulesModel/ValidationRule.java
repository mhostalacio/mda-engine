
package com.mda.engine.models.rulesModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.models.mvc.MVCViewElementChoice;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ValidationRule complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValidationRule">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/rulesModel}Rule">
 *       &lt;sequence>
 *         &lt;element name="Message" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewElementChoice" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidationRule", propOrder = {
    "message"
})
@XmlSeeAlso({
    CustomValidationRule.class,
    RangeValidationRule.class,
    ValueNodeValidationRule.class,
    RegexValidationRule.class,
    RequiredValidationRule.class,
    ExpressionValidationRule.class,
    ComparisonValidationRule.class
})
public class ValidationRule
    extends Rule
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Message")
    protected MVCViewElementChoice message;

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewElementChoice }
     *     
     */
    public MVCViewElementChoice getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewElementChoice }
     *     
     */
    public void setMessage(MVCViewElementChoice value) {
        this.message = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ValidationRule)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final ValidationRule that = ((ValidationRule) object);
        {
            MVCViewElementChoice lhsMessage;
            lhsMessage = this.getMessage();
            MVCViewElementChoice rhsMessage;
            rhsMessage = that.getMessage();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "message", lhsMessage), LocatorUtils.property(thatLocator, "message", rhsMessage), lhsMessage, rhsMessage)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof ValidationRule) {
            final ValidationRule copy = ((ValidationRule) draftCopy);
            if (this.message!= null) {
                MVCViewElementChoice sourceMessage;
                sourceMessage = this.getMessage();
                MVCViewElementChoice copyMessage = ((MVCViewElementChoice) strategy.copy(LocatorUtils.property(locator, "message", sourceMessage), sourceMessage));
                copy.setMessage(copyMessage);
            } else {
                copy.message = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ValidationRule();
    }

}
