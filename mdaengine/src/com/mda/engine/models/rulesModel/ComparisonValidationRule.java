
package com.mda.engine.models.rulesModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.models.expressionsModel.ExpressionChoice;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ComparisonValidationRule complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ComparisonValidationRule">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/rulesModel}ValidationRule">
 *       &lt;sequence>
 *         &lt;element name="CompareTo" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ExpressionChoice" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComparisonValidationRule", propOrder = {
    "compareTo"
})
@XmlSeeAlso({
    GreaterOrEqualThanValidationRule.class,
    EqualsValidationRule.class,
    LowerOrEqualThanValidationRule.class,
    LowerThanValidationRule.class,
    GreaterThanValidationRule.class
})
public class ComparisonValidationRule
    extends ValidationRule
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "CompareTo")
    protected ExpressionChoice compareTo;

    /**
     * Gets the value of the compareTo property.
     * 
     * @return
     *     possible object is
     *     {@link ExpressionChoice }
     *     
     */
    public ExpressionChoice getCompareTo() {
        return compareTo;
    }

    /**
     * Sets the value of the compareTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpressionChoice }
     *     
     */
    public void setCompareTo(ExpressionChoice value) {
        this.compareTo = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ComparisonValidationRule)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final ComparisonValidationRule that = ((ComparisonValidationRule) object);
        {
            ExpressionChoice lhsCompareTo;
            lhsCompareTo = this.getCompareTo();
            ExpressionChoice rhsCompareTo;
            rhsCompareTo = that.getCompareTo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "compareTo", lhsCompareTo), LocatorUtils.property(thatLocator, "compareTo", rhsCompareTo), lhsCompareTo, rhsCompareTo)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof ComparisonValidationRule) {
            final ComparisonValidationRule copy = ((ComparisonValidationRule) draftCopy);
            if (this.compareTo!= null) {
                ExpressionChoice sourceCompareTo;
                sourceCompareTo = this.getCompareTo();
                ExpressionChoice copyCompareTo = ((ExpressionChoice) strategy.copy(LocatorUtils.property(locator, "compareTo", sourceCompareTo), sourceCompareTo));
                copy.setCompareTo(copyCompareTo);
            } else {
                copy.compareTo = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ComparisonValidationRule();
    }

}
