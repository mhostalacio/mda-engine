
package com.mda.engine.models.rulesModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import com.mda.engine.models.expressionsModel.ExpressionChoice;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ValidationScope complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValidationScope">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="If" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ExpressionChoice" minOccurs="0"/>
 *         &lt;element name="Rules" type="{http://www.mdaengine.com/mdaengine/models/rulesModel}ValidationRuleCollection"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidationScope", propOrder = {
    "_if",
    "rules"
})
@XmlSeeAlso({
    AttributeValidationScope.class,
    EntityValidationScope.class
})
public class ValidationScope
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "If")
    protected ExpressionChoice _if;
    @XmlElement(name = "Rules", required = true)
    protected ValidationRuleCollection rules;

    /**
     * Gets the value of the if property.
     * 
     * @return
     *     possible object is
     *     {@link ExpressionChoice }
     *     
     */
    public ExpressionChoice getIf() {
        return _if;
    }

    /**
     * Sets the value of the if property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpressionChoice }
     *     
     */
    public void setIf(ExpressionChoice value) {
        this._if = value;
    }

    /**
     * Gets the value of the rules property.
     * 
     * @return
     *     possible object is
     *     {@link ValidationRuleCollection }
     *     
     */
    public ValidationRuleCollection getRules() {
        return rules;
    }

    /**
     * Sets the value of the rules property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidationRuleCollection }
     *     
     */
    public void setRules(ValidationRuleCollection value) {
        this.rules = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ValidationScope)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ValidationScope that = ((ValidationScope) object);
        {
            ExpressionChoice lhsIf;
            lhsIf = this.getIf();
            ExpressionChoice rhsIf;
            rhsIf = that.getIf();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "_if", lhsIf), LocatorUtils.property(thatLocator, "_if", rhsIf), lhsIf, rhsIf)) {
                return false;
            }
        }
        {
            ValidationRuleCollection lhsRules;
            lhsRules = this.getRules();
            ValidationRuleCollection rhsRules;
            rhsRules = that.getRules();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "rules", lhsRules), LocatorUtils.property(thatLocator, "rules", rhsRules), lhsRules, rhsRules)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof ValidationScope) {
            final ValidationScope copy = ((ValidationScope) draftCopy);
            if (this._if!= null) {
                ExpressionChoice sourceIf;
                sourceIf = this.getIf();
                ExpressionChoice copyIf = ((ExpressionChoice) strategy.copy(LocatorUtils.property(locator, "_if", sourceIf), sourceIf));
                copy.setIf(copyIf);
            } else {
                copy._if = null;
            }
            if (this.rules!= null) {
                ValidationRuleCollection sourceRules;
                sourceRules = this.getRules();
                ValidationRuleCollection copyRules = ((ValidationRuleCollection) strategy.copy(LocatorUtils.property(locator, "rules", sourceRules), sourceRules));
                copy.setRules(copyRules);
            } else {
                copy.rules = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ValidationScope();
    }

}
