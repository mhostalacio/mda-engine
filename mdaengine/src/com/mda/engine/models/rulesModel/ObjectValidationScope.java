
package com.mda.engine.models.rulesModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ITransformerSource;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ObjectValidationScope complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ObjectValidationScope">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Object" type="{http://www.mdaengine.com/mdaengine/models/rulesModel}EntityValidationScope" minOccurs="0"/>
 *         &lt;element name="Attributes" type="{http://www.mdaengine.com/mdaengine/models/rulesModel}AttributeValidationScopeCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectValidationScope", propOrder = {
    "object",
    "attributes"
})
public class ObjectValidationScope
    extends ModelNodeBase
    implements Serializable, Cloneable, ITransformerSource, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Object")
    protected EntityValidationScope object;
    @XmlElement(name = "Attributes")
    protected AttributeValidationScopeCollection attributes;
    @XmlAttribute(name = "Name", required = true)
    protected String name;

    /**
     * Gets the value of the object property.
     * 
     * @return
     *     possible object is
     *     {@link EntityValidationScope }
     *     
     */
    public EntityValidationScope getObject() {
        return object;
    }

    /**
     * Sets the value of the object property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntityValidationScope }
     *     
     */
    public void setObject(EntityValidationScope value) {
        this.object = value;
    }

    /**
     * Gets the value of the attributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeValidationScopeCollection }
     *     
     */
    public AttributeValidationScopeCollection getAttributes() {
        return attributes;
    }

    /**
     * Sets the value of the attributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeValidationScopeCollection }
     *     
     */
    public void setAttributes(AttributeValidationScopeCollection value) {
        this.attributes = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ObjectValidationScope)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ObjectValidationScope that = ((ObjectValidationScope) object);
        {
            EntityValidationScope lhsObject;
            lhsObject = this.getObject();
            EntityValidationScope rhsObject;
            rhsObject = that.getObject();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "object", lhsObject), LocatorUtils.property(thatLocator, "object", rhsObject), lhsObject, rhsObject)) {
                return false;
            }
        }
        {
            AttributeValidationScopeCollection lhsAttributes;
            lhsAttributes = this.getAttributes();
            AttributeValidationScopeCollection rhsAttributes;
            rhsAttributes = that.getAttributes();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "attributes", lhsAttributes), LocatorUtils.property(thatLocator, "attributes", rhsAttributes), lhsAttributes, rhsAttributes)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof ObjectValidationScope) {
            final ObjectValidationScope copy = ((ObjectValidationScope) draftCopy);
            if (this.object!= null) {
                EntityValidationScope sourceObject;
                sourceObject = this.getObject();
                EntityValidationScope copyObject = ((EntityValidationScope) strategy.copy(LocatorUtils.property(locator, "object", sourceObject), sourceObject));
                copy.setObject(copyObject);
            } else {
                copy.object = null;
            }
            if (this.attributes!= null) {
                AttributeValidationScopeCollection sourceAttributes;
                sourceAttributes = this.getAttributes();
                AttributeValidationScopeCollection copyAttributes = ((AttributeValidationScopeCollection) strategy.copy(LocatorUtils.property(locator, "attributes", sourceAttributes), sourceAttributes));
                copy.setAttributes(copyAttributes);
            } else {
                copy.attributes = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ObjectValidationScope();
    }

}
