
package com.mda.engine.models.rulesModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.models.expressionsModel.BooleanExpressionChoice;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ExpressionValidationRule complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ExpressionValidationRule">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/rulesModel}ValidationRule">
 *       &lt;sequence>
 *         &lt;element name="Expression" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}BooleanExpressionChoice"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExpressionValidationRule", propOrder = {
    "expression"
})
public class ExpressionValidationRule
    extends ValidationRule
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Expression", required = true)
    protected BooleanExpressionChoice expression;

    /**
     * Gets the value of the expression property.
     * 
     * @return
     *     possible object is
     *     {@link BooleanExpressionChoice }
     *     
     */
    public BooleanExpressionChoice getExpression() {
        return expression;
    }

    /**
     * Sets the value of the expression property.
     * 
     * @param value
     *     allowed object is
     *     {@link BooleanExpressionChoice }
     *     
     */
    public void setExpression(BooleanExpressionChoice value) {
        this.expression = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ExpressionValidationRule)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final ExpressionValidationRule that = ((ExpressionValidationRule) object);
        {
            BooleanExpressionChoice lhsExpression;
            lhsExpression = this.getExpression();
            BooleanExpressionChoice rhsExpression;
            rhsExpression = that.getExpression();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "expression", lhsExpression), LocatorUtils.property(thatLocator, "expression", rhsExpression), lhsExpression, rhsExpression)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof ExpressionValidationRule) {
            final ExpressionValidationRule copy = ((ExpressionValidationRule) draftCopy);
            if (this.expression!= null) {
                BooleanExpressionChoice sourceExpression;
                sourceExpression = this.getExpression();
                BooleanExpressionChoice copyExpression = ((BooleanExpressionChoice) strategy.copy(LocatorUtils.property(locator, "expression", sourceExpression), sourceExpression));
                copy.setExpression(copyExpression);
            } else {
                copy.expression = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ExpressionValidationRule();
    }

}
