
package com.mda.engine.models.dataModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ForeignKeyTableReference complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ForeignKeyTableReference">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Columns" type="{http://www.mdaengine.com/mdaengine/models/dataModel}ColumnCollection"/>
 *       &lt;/sequence>
 *       &lt;attribute name="TableName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ForeignKeyTableReference", propOrder = {
    "columns"
})
public class ForeignKeyTableReference
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Columns", required = true)
    protected ColumnCollection columns;
    @XmlAttribute(name = "TableName", required = true)
    protected String tableName;

    /**
     * Gets the value of the columns property.
     * 
     * @return
     *     possible object is
     *     {@link ColumnCollection }
     *     
     */
    public ColumnCollection getColumns() {
        return columns;
    }

    /**
     * Sets the value of the columns property.
     * 
     * @param value
     *     allowed object is
     *     {@link ColumnCollection }
     *     
     */
    public void setColumns(ColumnCollection value) {
        this.columns = value;
    }

    /**
     * Gets the value of the tableName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTableName() {
        return tableName;
    }

    /**
     * Sets the value of the tableName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTableName(String value) {
        this.tableName = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ForeignKeyTableReference)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ForeignKeyTableReference that = ((ForeignKeyTableReference) object);
        {
            ColumnCollection lhsColumns;
            lhsColumns = this.getColumns();
            ColumnCollection rhsColumns;
            rhsColumns = that.getColumns();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "columns", lhsColumns), LocatorUtils.property(thatLocator, "columns", rhsColumns), lhsColumns, rhsColumns)) {
                return false;
            }
        }
        {
            String lhsTableName;
            lhsTableName = this.getTableName();
            String rhsTableName;
            rhsTableName = that.getTableName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "tableName", lhsTableName), LocatorUtils.property(thatLocator, "tableName", rhsTableName), lhsTableName, rhsTableName)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof ForeignKeyTableReference) {
            final ForeignKeyTableReference copy = ((ForeignKeyTableReference) draftCopy);
            if (this.columns!= null) {
                ColumnCollection sourceColumns;
                sourceColumns = this.getColumns();
                ColumnCollection copyColumns = ((ColumnCollection) strategy.copy(LocatorUtils.property(locator, "columns", sourceColumns), sourceColumns));
                copy.setColumns(copyColumns);
            } else {
                copy.columns = null;
            }
            if (this.tableName!= null) {
                String sourceTableName;
                sourceTableName = this.getTableName();
                String copyTableName = ((String) strategy.copy(LocatorUtils.property(locator, "tableName", sourceTableName), sourceTableName));
                copy.setTableName(copyTableName);
            } else {
                copy.tableName = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ForeignKeyTableReference();
    }

}
