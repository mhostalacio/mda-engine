
package com.mda.engine.models.dataModel;

import java.io.Serializable;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for BinaryTableColumn complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BinaryTableColumn">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/dataModel}TableColumn">
 *       &lt;attribute name="MaxSize" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BinaryTableColumn")
public class BinaryTableColumn
    extends TableColumn
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "MaxSize")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger maxSize;

    /**
     * Gets the value of the maxSize property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaxSize() {
        return maxSize;
    }

    /**
     * Sets the value of the maxSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaxSize(BigInteger value) {
        this.maxSize = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof BinaryTableColumn)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final BinaryTableColumn that = ((BinaryTableColumn) object);
        {
            BigInteger lhsMaxSize;
            lhsMaxSize = this.getMaxSize();
            BigInteger rhsMaxSize;
            rhsMaxSize = that.getMaxSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "maxSize", lhsMaxSize), LocatorUtils.property(thatLocator, "maxSize", rhsMaxSize), lhsMaxSize, rhsMaxSize)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof BinaryTableColumn) {
            final BinaryTableColumn copy = ((BinaryTableColumn) draftCopy);
            if (this.maxSize!= null) {
                BigInteger sourceMaxSize;
                sourceMaxSize = this.getMaxSize();
                BigInteger copyMaxSize = ((BigInteger) strategy.copy(LocatorUtils.property(locator, "maxSize", sourceMaxSize), sourceMaxSize));
                copy.setMaxSize(copyMaxSize);
            } else {
                copy.maxSize = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new BinaryTableColumn();
    }

}
