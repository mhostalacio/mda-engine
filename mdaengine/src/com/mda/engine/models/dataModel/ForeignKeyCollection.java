
package com.mda.engine.models.dataModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ForeignKeyCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ForeignKeyCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded" minOccurs="0">
 *         &lt;element name="ForeignKey" type="{http://www.mdaengine.com/mdaengine/models/dataModel}ForeignKey" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ForeignKeyCollection", propOrder = {
    "foreignKeyList"
})
public class ForeignKeyCollection
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "ForeignKey")
    protected List<ForeignKey> foreignKeyList;

    /**
     * Gets the value of the foreignKeyList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the foreignKeyList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getForeignKeyList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ForeignKey }
     * 
     * 
     */
    public List<ForeignKey> getForeignKeyList() {
        if (foreignKeyList == null) {
            foreignKeyList = new ArrayList<ForeignKey>();
        }
        return this.foreignKeyList;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ForeignKeyCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ForeignKeyCollection that = ((ForeignKeyCollection) object);
        {
            List<ForeignKey> lhsForeignKeyList;
            lhsForeignKeyList = this.getForeignKeyList();
            List<ForeignKey> rhsForeignKeyList;
            rhsForeignKeyList = that.getForeignKeyList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "foreignKeyList", lhsForeignKeyList), LocatorUtils.property(thatLocator, "foreignKeyList", rhsForeignKeyList), lhsForeignKeyList, rhsForeignKeyList)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof ForeignKeyCollection) {
            final ForeignKeyCollection copy = ((ForeignKeyCollection) draftCopy);
            if ((this.foreignKeyList!= null)&&(!this.foreignKeyList.isEmpty())) {
                List<ForeignKey> sourceForeignKeyList;
                sourceForeignKeyList = this.getForeignKeyList();
                @SuppressWarnings("unchecked")
                List<ForeignKey> copyForeignKeyList = ((List<ForeignKey> ) strategy.copy(LocatorUtils.property(locator, "foreignKeyList", sourceForeignKeyList), sourceForeignKeyList));
                copy.foreignKeyList = null;
                List<ForeignKey> uniqueForeignKeyListl = copy.getForeignKeyList();
                uniqueForeignKeyListl.addAll(copyForeignKeyList);
            } else {
                copy.foreignKeyList = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ForeignKeyCollection();
    }

}
