
package com.mda.engine.models.dataModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for PrimaryKey complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PrimaryKey">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/dataModel}ColumnReferenceCollection">
 *       &lt;attribute name="AllowPageLocks" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *       &lt;attribute name="AllowRowLocks" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PrimaryKey")
public class PrimaryKey
    extends ColumnReferenceCollection
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "AllowPageLocks")
    protected Boolean allowPageLocks;
    @XmlAttribute(name = "AllowRowLocks")
    protected Boolean allowRowLocks;

    /**
     * Gets the value of the allowPageLocks property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isAllowPageLocks() {
        if (allowPageLocks == null) {
            return true;
        } else {
            return allowPageLocks;
        }
    }

    /**
     * Sets the value of the allowPageLocks property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowPageLocks(Boolean value) {
        this.allowPageLocks = value;
    }

    /**
     * Gets the value of the allowRowLocks property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isAllowRowLocks() {
        if (allowRowLocks == null) {
            return true;
        } else {
            return allowRowLocks;
        }
    }

    /**
     * Sets the value of the allowRowLocks property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowRowLocks(Boolean value) {
        this.allowRowLocks = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof PrimaryKey)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final PrimaryKey that = ((PrimaryKey) object);
        {
            boolean lhsAllowPageLocks;
            lhsAllowPageLocks = this.isAllowPageLocks();
            boolean rhsAllowPageLocks;
            rhsAllowPageLocks = that.isAllowPageLocks();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "allowPageLocks", lhsAllowPageLocks), LocatorUtils.property(thatLocator, "allowPageLocks", rhsAllowPageLocks), lhsAllowPageLocks, rhsAllowPageLocks)) {
                return false;
            }
        }
        {
            boolean lhsAllowRowLocks;
            lhsAllowRowLocks = this.isAllowRowLocks();
            boolean rhsAllowRowLocks;
            rhsAllowRowLocks = that.isAllowRowLocks();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "allowRowLocks", lhsAllowRowLocks), LocatorUtils.property(thatLocator, "allowRowLocks", rhsAllowRowLocks), lhsAllowRowLocks, rhsAllowRowLocks)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof PrimaryKey) {
            final PrimaryKey copy = ((PrimaryKey) draftCopy);
            if (this.allowPageLocks!= null) {
                boolean sourceAllowPageLocks;
                sourceAllowPageLocks = this.isAllowPageLocks();
                boolean copyAllowPageLocks = strategy.copy(LocatorUtils.property(locator, "allowPageLocks", sourceAllowPageLocks), sourceAllowPageLocks);
                copy.setAllowPageLocks(copyAllowPageLocks);
            } else {
                copy.allowPageLocks = null;
            }
            if (this.allowRowLocks!= null) {
                boolean sourceAllowRowLocks;
                sourceAllowRowLocks = this.isAllowRowLocks();
                boolean copyAllowRowLocks = strategy.copy(LocatorUtils.property(locator, "allowRowLocks", sourceAllowRowLocks), sourceAllowRowLocks);
                copy.setAllowRowLocks(copyAllowRowLocks);
            } else {
                copy.allowRowLocks = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new PrimaryKey();
    }

}
