
package com.mda.engine.models.dataModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for EnumTableColumn complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EnumTableColumn">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/dataModel}TableColumn">
 *       &lt;attribute name="EnumName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnumTableColumn")
public class EnumTableColumn
    extends TableColumn
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "EnumName")
    protected String enumName;

    /**
     * Gets the value of the enumName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnumName() {
        return enumName;
    }

    /**
     * Sets the value of the enumName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnumName(String value) {
        this.enumName = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof EnumTableColumn)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final EnumTableColumn that = ((EnumTableColumn) object);
        {
            String lhsEnumName;
            lhsEnumName = this.getEnumName();
            String rhsEnumName;
            rhsEnumName = that.getEnumName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "enumName", lhsEnumName), LocatorUtils.property(thatLocator, "enumName", rhsEnumName), lhsEnumName, rhsEnumName)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof EnumTableColumn) {
            final EnumTableColumn copy = ((EnumTableColumn) draftCopy);
            if (this.enumName!= null) {
                String sourceEnumName;
                sourceEnumName = this.getEnumName();
                String copyEnumName = ((String) strategy.copy(LocatorUtils.property(locator, "enumName", sourceEnumName), sourceEnumName));
                copy.setEnumName(copyEnumName);
            } else {
                copy.enumName = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new EnumTableColumn();
    }

}
