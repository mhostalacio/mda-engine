
package com.mda.engine.models.dataModel;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.mda.engine.models.dataModel package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Table_QNAME = new QName("http://www.mdaengine.com/mdaengine/models/dataModel", "Table");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.mda.engine.models.dataModel
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link BinaryTableColumn }
     * 
     */
    public BinaryTableColumn createBinaryTableColumn() {
        return new BinaryTableColumn();
    }

    /**
     * Create an instance of {@link DataBaseItem }
     * 
     */
    public DataBaseItem createDataBaseItem() {
        return new DataBaseItem();
    }

    /**
     * Create an instance of {@link DoubleTableColumn }
     * 
     */
    public DoubleTableColumn createDoubleTableColumn() {
        return new DoubleTableColumn();
    }

    /**
     * Create an instance of {@link UniqueIdentifierTableColumn }
     * 
     */
    public UniqueIdentifierTableColumn createUniqueIdentifierTableColumn() {
        return new UniqueIdentifierTableColumn();
    }

    /**
     * Create an instance of {@link ForeignKeyTableReference }
     * 
     */
    public ForeignKeyTableReference createForeignKeyTableReference() {
        return new ForeignKeyTableReference();
    }

    /**
     * Create an instance of {@link NvarcharTableColumn }
     * 
     */
    public NvarcharTableColumn createNvarcharTableColumn() {
        return new NvarcharTableColumn();
    }

    /**
     * Create an instance of {@link SmallIntTableColumn }
     * 
     */
    public SmallIntTableColumn createSmallIntTableColumn() {
        return new SmallIntTableColumn();
    }

    /**
     * Create an instance of {@link ColumnReferenceCollection }
     * 
     */
    public ColumnReferenceCollection createColumnReferenceCollection() {
        return new ColumnReferenceCollection();
    }

    /**
     * Create an instance of {@link DateTimeTableColumn }
     * 
     */
    public DateTimeTableColumn createDateTimeTableColumn() {
        return new DateTimeTableColumn();
    }

    /**
     * Create an instance of {@link DataTable }
     * 
     */
    public DataTable createDataTable() {
        return new DataTable();
    }

    /**
     * Create an instance of {@link FloatTableColumn }
     * 
     */
    public FloatTableColumn createFloatTableColumn() {
        return new FloatTableColumn();
    }

    /**
     * Create an instance of {@link DecimalTableColumn }
     * 
     */
    public DecimalTableColumn createDecimalTableColumn() {
        return new DecimalTableColumn();
    }

    /**
     * Create an instance of {@link VarcharTableColumn }
     * 
     */
    public VarcharTableColumn createVarcharTableColumn() {
        return new VarcharTableColumn();
    }

    /**
     * Create an instance of {@link BitTableColumn }
     * 
     */
    public BitTableColumn createBitTableColumn() {
        return new BitTableColumn();
    }

    /**
     * Create an instance of {@link EnumTableColumn }
     * 
     */
    public EnumTableColumn createEnumTableColumn() {
        return new EnumTableColumn();
    }

    /**
     * Create an instance of {@link PrimaryKey }
     * 
     */
    public PrimaryKey createPrimaryKey() {
        return new PrimaryKey();
    }

    /**
     * Create an instance of {@link ForeignKey }
     * 
     */
    public ForeignKey createForeignKey() {
        return new ForeignKey();
    }

    /**
     * Create an instance of {@link ColumnReference }
     * 
     */
    public ColumnReference createColumnReference() {
        return new ColumnReference();
    }

    /**
     * Create an instance of {@link TableColumn }
     * 
     */
    public TableColumn createTableColumn() {
        return new TableColumn();
    }

    /**
     * Create an instance of {@link TinyIntTableColumn }
     * 
     */
    public TinyIntTableColumn createTinyIntTableColumn() {
        return new TinyIntTableColumn();
    }

    /**
     * Create an instance of {@link XMLTableColumn }
     * 
     */
    public XMLTableColumn createXMLTableColumn() {
        return new XMLTableColumn();
    }

    /**
     * Create an instance of {@link IntTableColumn }
     * 
     */
    public IntTableColumn createIntTableColumn() {
        return new IntTableColumn();
    }

    /**
     * Create an instance of {@link ColumnCollection }
     * 
     */
    public ColumnCollection createColumnCollection() {
        return new ColumnCollection();
    }

    /**
     * Create an instance of {@link ForeignKeyCollection }
     * 
     */
    public ForeignKeyCollection createForeignKeyCollection() {
        return new ForeignKeyCollection();
    }

    /**
     * Create an instance of {@link BigIntTableColumn }
     * 
     */
    public BigIntTableColumn createBigIntTableColumn() {
        return new BigIntTableColumn();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.mdaengine.com/mdaengine/models/dataModel", name = "Table")
    public JAXBElement<DataTable> createTable(DataTable value) {
        return new JAXBElement<DataTable>(_Table_QNAME, DataTable.class, null, value);
    }

}
