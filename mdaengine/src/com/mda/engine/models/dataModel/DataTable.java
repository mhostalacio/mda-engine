
package com.mda.engine.models.dataModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for DataTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DataTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PrimaryKey" type="{http://www.mdaengine.com/mdaengine/models/dataModel}PrimaryKey"/>
 *         &lt;element name="Columns" type="{http://www.mdaengine.com/mdaengine/models/dataModel}ColumnCollection"/>
 *         &lt;element name="ForeignKeys" type="{http://www.mdaengine.com/mdaengine/models/dataModel}ForeignKeyCollection"/>
 *       &lt;/sequence>
 *       &lt;attribute name="DefaultCollation" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataTable", propOrder = {
    "primaryKey",
    "columns",
    "foreignKeys"
})
@XmlRootElement(name = "Table")
public class DataTable
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "PrimaryKey", required = true)
    protected PrimaryKey primaryKey;
    @XmlElement(name = "Columns", required = true)
    protected ColumnCollection columns;
    @XmlElement(name = "ForeignKeys", required = true)
    protected ForeignKeyCollection foreignKeys;
    @XmlAttribute(name = "DefaultCollation", required = true)
    protected String defaultCollation;
    @XmlAttribute(name = "Name", required = true)
    protected String name;

    /**
     * Gets the value of the primaryKey property.
     * 
     * @return
     *     possible object is
     *     {@link PrimaryKey }
     *     
     */
    public PrimaryKey getPrimaryKey() {
        return primaryKey;
    }

    /**
     * Sets the value of the primaryKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrimaryKey }
     *     
     */
    public void setPrimaryKey(PrimaryKey value) {
        this.primaryKey = value;
    }

    /**
     * Gets the value of the columns property.
     * 
     * @return
     *     possible object is
     *     {@link ColumnCollection }
     *     
     */
    public ColumnCollection getColumns() {
        return columns;
    }

    /**
     * Sets the value of the columns property.
     * 
     * @param value
     *     allowed object is
     *     {@link ColumnCollection }
     *     
     */
    public void setColumns(ColumnCollection value) {
        this.columns = value;
    }

    /**
     * Gets the value of the foreignKeys property.
     * 
     * @return
     *     possible object is
     *     {@link ForeignKeyCollection }
     *     
     */
    public ForeignKeyCollection getForeignKeys() {
        return foreignKeys;
    }

    /**
     * Sets the value of the foreignKeys property.
     * 
     * @param value
     *     allowed object is
     *     {@link ForeignKeyCollection }
     *     
     */
    public void setForeignKeys(ForeignKeyCollection value) {
        this.foreignKeys = value;
    }

    /**
     * Gets the value of the defaultCollation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultCollation() {
        return defaultCollation;
    }

    /**
     * Sets the value of the defaultCollation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultCollation(String value) {
        this.defaultCollation = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof DataTable)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final DataTable that = ((DataTable) object);
        {
            PrimaryKey lhsPrimaryKey;
            lhsPrimaryKey = this.getPrimaryKey();
            PrimaryKey rhsPrimaryKey;
            rhsPrimaryKey = that.getPrimaryKey();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "primaryKey", lhsPrimaryKey), LocatorUtils.property(thatLocator, "primaryKey", rhsPrimaryKey), lhsPrimaryKey, rhsPrimaryKey)) {
                return false;
            }
        }
        {
            ColumnCollection lhsColumns;
            lhsColumns = this.getColumns();
            ColumnCollection rhsColumns;
            rhsColumns = that.getColumns();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "columns", lhsColumns), LocatorUtils.property(thatLocator, "columns", rhsColumns), lhsColumns, rhsColumns)) {
                return false;
            }
        }
        {
            ForeignKeyCollection lhsForeignKeys;
            lhsForeignKeys = this.getForeignKeys();
            ForeignKeyCollection rhsForeignKeys;
            rhsForeignKeys = that.getForeignKeys();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "foreignKeys", lhsForeignKeys), LocatorUtils.property(thatLocator, "foreignKeys", rhsForeignKeys), lhsForeignKeys, rhsForeignKeys)) {
                return false;
            }
        }
        {
            String lhsDefaultCollation;
            lhsDefaultCollation = this.getDefaultCollation();
            String rhsDefaultCollation;
            rhsDefaultCollation = that.getDefaultCollation();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "defaultCollation", lhsDefaultCollation), LocatorUtils.property(thatLocator, "defaultCollation", rhsDefaultCollation), lhsDefaultCollation, rhsDefaultCollation)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof DataTable) {
            final DataTable copy = ((DataTable) draftCopy);
            if (this.primaryKey!= null) {
                PrimaryKey sourcePrimaryKey;
                sourcePrimaryKey = this.getPrimaryKey();
                PrimaryKey copyPrimaryKey = ((PrimaryKey) strategy.copy(LocatorUtils.property(locator, "primaryKey", sourcePrimaryKey), sourcePrimaryKey));
                copy.setPrimaryKey(copyPrimaryKey);
            } else {
                copy.primaryKey = null;
            }
            if (this.columns!= null) {
                ColumnCollection sourceColumns;
                sourceColumns = this.getColumns();
                ColumnCollection copyColumns = ((ColumnCollection) strategy.copy(LocatorUtils.property(locator, "columns", sourceColumns), sourceColumns));
                copy.setColumns(copyColumns);
            } else {
                copy.columns = null;
            }
            if (this.foreignKeys!= null) {
                ForeignKeyCollection sourceForeignKeys;
                sourceForeignKeys = this.getForeignKeys();
                ForeignKeyCollection copyForeignKeys = ((ForeignKeyCollection) strategy.copy(LocatorUtils.property(locator, "foreignKeys", sourceForeignKeys), sourceForeignKeys));
                copy.setForeignKeys(copyForeignKeys);
            } else {
                copy.foreignKeys = null;
            }
            if (this.defaultCollation!= null) {
                String sourceDefaultCollation;
                sourceDefaultCollation = this.getDefaultCollation();
                String copyDefaultCollation = ((String) strategy.copy(LocatorUtils.property(locator, "defaultCollation", sourceDefaultCollation), sourceDefaultCollation));
                copy.setDefaultCollation(copyDefaultCollation);
            } else {
                copy.defaultCollation = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new DataTable();
    }
    
//--simple--preserve
    
    
    private transient com.mda.engine.models.businessObjectModel.ObjectType owner;
    private transient Boolean isResourcesTable;

    public com.mda.engine.models.dataModel.TableColumn getColumnByName(String name) {
    	for (com.mda.engine.models.dataModel.TableColumn col : this.getColumns().getColumnList())
    	{
    		if (col.getName().equals(name))
    			return col;
    	}
    	return null;
	}
    
    public com.mda.engine.models.businessObjectModel.ObjectType getOwner() {

		return this.owner;
	}
    
    public void setOwner(com.mda.engine.models.businessObjectModel.ObjectType owner) {

		this.owner = owner;
	}

    public java.util.ArrayList<TableColumn> getFillableAuditFields()
	{
    	java.util.ArrayList<TableColumn> list = new java.util.ArrayList<TableColumn>();
		for (TableColumn col : this.getColumns().getColumnList())
		{
			if (col.getAuditColumn())
			{
				if (!col.getName().equals(com.mda.engine.utils.QueryHelper.AUDIT_FIELD_NAME_VERSION_NUMBER)
						&& !col.getName().equals(com.mda.engine.utils.QueryHelper.AUDIT_FIELD_NAME_PERSIST_ORDER)
						&& !col.getName().equalsIgnoreCase(com.mda.engine.utils.QueryHelper.AUDIT_FIELD_DEFAULT_LANGUAGE))
					list.add(col);
			}
		}
		return list;
	}
    
    public java.util.ArrayList<TableColumn> getNonAuditFields()
	{
    	java.util.ArrayList<TableColumn> list = new java.util.ArrayList<TableColumn>();
		for (TableColumn col : this.getColumns().getColumnList())
		{
			if (!col.getAuditColumn())
			{
				if (!col.getName().equals("Id"))
					list.add(col);
			}
		}
		return list;
	}

	public void setIsResourcesTable(Boolean isResourcesTable) {
		this.isResourcesTable = isResourcesTable;
	}

	public Boolean getIsResourcesTable() {
		if (this.isResourcesTable == null)
			this.isResourcesTable = false;
		return isResourcesTable;
	}
	
//--simple--preserve

}
