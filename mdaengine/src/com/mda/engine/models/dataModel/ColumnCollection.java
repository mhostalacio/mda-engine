
package com.mda.engine.models.dataModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ColumnCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ColumnCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded" minOccurs="0">
 *         &lt;element name="Varchar" type="{http://www.mdaengine.com/mdaengine/models/dataModel}VarcharTableColumn" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Nvarchar" type="{http://www.mdaengine.com/mdaengine/models/dataModel}NvarcharTableColumn" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Datetime" type="{http://www.mdaengine.com/mdaengine/models/dataModel}DateTimeTableColumn" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Int" type="{http://www.mdaengine.com/mdaengine/models/dataModel}IntTableColumn" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TinyInt" type="{http://www.mdaengine.com/mdaengine/models/dataModel}TinyIntTableColumn" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SmallInt" type="{http://www.mdaengine.com/mdaengine/models/dataModel}SmallIntTableColumn" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Float" type="{http://www.mdaengine.com/mdaengine/models/dataModel}FloatTableColumn" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="BigInt" type="{http://www.mdaengine.com/mdaengine/models/dataModel}BigIntTableColumn" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Decimal" type="{http://www.mdaengine.com/mdaengine/models/dataModel}DecimalTableColumn" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Bit" type="{http://www.mdaengine.com/mdaengine/models/dataModel}BitTableColumn" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="UniqueIdentifier" type="{http://www.mdaengine.com/mdaengine/models/dataModel}UniqueIdentifierTableColumn" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="XML" type="{http://www.mdaengine.com/mdaengine/models/dataModel}XMLTableColumn" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Binary" type="{http://www.mdaengine.com/mdaengine/models/dataModel}BinaryTableColumn" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ColumnCollection", propOrder = {
    "columnList"
})
public class ColumnCollection
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElements({
        @XmlElement(name = "Bit", type = BitTableColumn.class),
        @XmlElement(name = "XML", type = XMLTableColumn.class),
        @XmlElement(name = "Varchar", type = VarcharTableColumn.class),
        @XmlElement(name = "TinyInt", type = TinyIntTableColumn.class),
        @XmlElement(name = "Nvarchar", type = NvarcharTableColumn.class),
        @XmlElement(name = "Binary", type = BinaryTableColumn.class),
        @XmlElement(name = "SmallInt", type = SmallIntTableColumn.class),
        @XmlElement(name = "Float", type = FloatTableColumn.class),
        @XmlElement(name = "Datetime", type = DateTimeTableColumn.class),
        @XmlElement(name = "BigInt", type = BigIntTableColumn.class),
        @XmlElement(name = "UniqueIdentifier", type = UniqueIdentifierTableColumn.class),
        @XmlElement(name = "Decimal", type = DecimalTableColumn.class),
        @XmlElement(name = "Int", type = IntTableColumn.class)
    })
    protected List<TableColumn> columnList;

    /**
     * Gets the value of the columnList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the columnList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getColumnList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BitTableColumn }
     * {@link XMLTableColumn }
     * {@link VarcharTableColumn }
     * {@link TinyIntTableColumn }
     * {@link NvarcharTableColumn }
     * {@link BinaryTableColumn }
     * {@link SmallIntTableColumn }
     * {@link FloatTableColumn }
     * {@link DateTimeTableColumn }
     * {@link BigIntTableColumn }
     * {@link UniqueIdentifierTableColumn }
     * {@link DecimalTableColumn }
     * {@link IntTableColumn }
     * 
     * 
     */
    public List<TableColumn> getColumnList() {
        if (columnList == null) {
            columnList = new ArrayList<TableColumn>();
        }
        return this.columnList;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ColumnCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ColumnCollection that = ((ColumnCollection) object);
        {
            List<TableColumn> lhsColumnList;
            lhsColumnList = this.getColumnList();
            List<TableColumn> rhsColumnList;
            rhsColumnList = that.getColumnList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "columnList", lhsColumnList), LocatorUtils.property(thatLocator, "columnList", rhsColumnList), lhsColumnList, rhsColumnList)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof ColumnCollection) {
            final ColumnCollection copy = ((ColumnCollection) draftCopy);
            if ((this.columnList!= null)&&(!this.columnList.isEmpty())) {
                List<TableColumn> sourceColumnList;
                sourceColumnList = this.getColumnList();
                @SuppressWarnings("unchecked")
                List<TableColumn> copyColumnList = ((List<TableColumn> ) strategy.copy(LocatorUtils.property(locator, "columnList", sourceColumnList), sourceColumnList));
                copy.columnList = null;
                List<TableColumn> uniqueColumnListl = copy.getColumnList();
                uniqueColumnListl.addAll(copyColumnList);
            } else {
                copy.columnList = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ColumnCollection();
    }
    
//--simple--preserve
    public List<TableColumn> getOrderedColumnList() {
    	List<TableColumn> list = getColumnList();
    	java.util.Collections.sort(list, new com.mda.engine.utils.TableColumnComparator());
        return list;
    }
//--simple--preserve

}
