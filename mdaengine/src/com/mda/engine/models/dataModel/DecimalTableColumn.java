
package com.mda.engine.models.dataModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for DecimalTableColumn complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DecimalTableColumn">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/dataModel}NumberTableColumn">
 *       &lt;attribute name="NumberOfDecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="NumberOfIntegerPlaces" type="{http://www.w3.org/2001/XMLSchema}int" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DecimalTableColumn")
public class DecimalTableColumn
    extends NumberTableColumn
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "NumberOfDecimalPlaces")
    protected Integer numberOfDecimalPlaces;
    @XmlAttribute(name = "NumberOfIntegerPlaces")
    protected Integer numberOfIntegerPlaces;

    /**
     * Gets the value of the numberOfDecimalPlaces property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfDecimalPlaces() {
        return numberOfDecimalPlaces;
    }

    /**
     * Sets the value of the numberOfDecimalPlaces property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfDecimalPlaces(Integer value) {
        this.numberOfDecimalPlaces = value;
    }

    /**
     * Gets the value of the numberOfIntegerPlaces property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfIntegerPlaces() {
        return numberOfIntegerPlaces;
    }

    /**
     * Sets the value of the numberOfIntegerPlaces property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfIntegerPlaces(Integer value) {
        this.numberOfIntegerPlaces = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof DecimalTableColumn)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final DecimalTableColumn that = ((DecimalTableColumn) object);
        {
            Integer lhsNumberOfDecimalPlaces;
            lhsNumberOfDecimalPlaces = this.getNumberOfDecimalPlaces();
            Integer rhsNumberOfDecimalPlaces;
            rhsNumberOfDecimalPlaces = that.getNumberOfDecimalPlaces();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "numberOfDecimalPlaces", lhsNumberOfDecimalPlaces), LocatorUtils.property(thatLocator, "numberOfDecimalPlaces", rhsNumberOfDecimalPlaces), lhsNumberOfDecimalPlaces, rhsNumberOfDecimalPlaces)) {
                return false;
            }
        }
        {
            Integer lhsNumberOfIntegerPlaces;
            lhsNumberOfIntegerPlaces = this.getNumberOfIntegerPlaces();
            Integer rhsNumberOfIntegerPlaces;
            rhsNumberOfIntegerPlaces = that.getNumberOfIntegerPlaces();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "numberOfIntegerPlaces", lhsNumberOfIntegerPlaces), LocatorUtils.property(thatLocator, "numberOfIntegerPlaces", rhsNumberOfIntegerPlaces), lhsNumberOfIntegerPlaces, rhsNumberOfIntegerPlaces)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof DecimalTableColumn) {
            final DecimalTableColumn copy = ((DecimalTableColumn) draftCopy);
            if (this.numberOfDecimalPlaces!= null) {
                Integer sourceNumberOfDecimalPlaces;
                sourceNumberOfDecimalPlaces = this.getNumberOfDecimalPlaces();
                Integer copyNumberOfDecimalPlaces = ((Integer) strategy.copy(LocatorUtils.property(locator, "numberOfDecimalPlaces", sourceNumberOfDecimalPlaces), sourceNumberOfDecimalPlaces));
                copy.setNumberOfDecimalPlaces(copyNumberOfDecimalPlaces);
            } else {
                copy.numberOfDecimalPlaces = null;
            }
            if (this.numberOfIntegerPlaces!= null) {
                Integer sourceNumberOfIntegerPlaces;
                sourceNumberOfIntegerPlaces = this.getNumberOfIntegerPlaces();
                Integer copyNumberOfIntegerPlaces = ((Integer) strategy.copy(LocatorUtils.property(locator, "numberOfIntegerPlaces", sourceNumberOfIntegerPlaces), sourceNumberOfIntegerPlaces));
                copy.setNumberOfIntegerPlaces(copyNumberOfIntegerPlaces);
            } else {
                copy.numberOfIntegerPlaces = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new DecimalTableColumn();
    }

}
