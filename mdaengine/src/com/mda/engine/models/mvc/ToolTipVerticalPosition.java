
package com.mda.engine.models.mvc;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ToolTipVerticalPosition.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ToolTipVerticalPosition">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Top"/>
 *     &lt;enumeration value="Bottom"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ToolTipVerticalPosition")
@XmlEnum
public enum ToolTipVerticalPosition {

    @XmlEnumValue("Top")
    TOP("Top"),
    @XmlEnumValue("Bottom")
    BOTTOM("Bottom");
    private final String value;

    ToolTipVerticalPosition(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ToolTipVerticalPosition fromValue(String v) {
        for (ToolTipVerticalPosition c: ToolTipVerticalPosition.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
