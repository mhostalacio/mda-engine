
package com.mda.engine.models.mvc;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MVCViewPaginatorType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MVCViewPaginatorType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NUMBERED"/>
 *     &lt;enumeration value="INCREMENTAL"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MVCViewPaginatorType")
@XmlEnum
public enum MVCViewPaginatorType {

    NUMBERED,
    INCREMENTAL;

    public String value() {
        return name();
    }

    public static MVCViewPaginatorType fromValue(String v) {
        return valueOf(v);
    }

}
