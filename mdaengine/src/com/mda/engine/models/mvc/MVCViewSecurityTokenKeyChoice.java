
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import com.mda.engine.models.expressionsModel.EnumValueExpression;
import com.mda.engine.models.expressionsModel.StringValueExpression;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewSecurityTokenKeyChoice complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewSecurityTokenKeyChoice">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="Text" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}StringValueExpression"/>
 *         &lt;element name="Enum" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}EnumValueExpression"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewSecurityTokenKeyChoice", propOrder = {
    "text",
    "_enum"
})
public class MVCViewSecurityTokenKeyChoice
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Text")
    protected StringValueExpression text;
    @XmlElement(name = "Enum")
    protected EnumValueExpression _enum;

    /**
     * Gets the value of the text property.
     * 
     * @return
     *     possible object is
     *     {@link StringValueExpression }
     *     
     */
    public StringValueExpression getText() {
        return text;
    }

    /**
     * Sets the value of the text property.
     * 
     * @param value
     *     allowed object is
     *     {@link StringValueExpression }
     *     
     */
    public void setText(StringValueExpression value) {
        this.text = value;
    }

    /**
     * Gets the value of the enum property.
     * 
     * @return
     *     possible object is
     *     {@link EnumValueExpression }
     *     
     */
    public EnumValueExpression getEnum() {
        return _enum;
    }

    /**
     * Sets the value of the enum property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnumValueExpression }
     *     
     */
    public void setEnum(EnumValueExpression value) {
        this._enum = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewSecurityTokenKeyChoice)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final MVCViewSecurityTokenKeyChoice that = ((MVCViewSecurityTokenKeyChoice) object);
        {
            StringValueExpression lhsText;
            lhsText = this.getText();
            StringValueExpression rhsText;
            rhsText = that.getText();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "text", lhsText), LocatorUtils.property(thatLocator, "text", rhsText), lhsText, rhsText)) {
                return false;
            }
        }
        {
            EnumValueExpression lhsEnum;
            lhsEnum = this.getEnum();
            EnumValueExpression rhsEnum;
            rhsEnum = that.getEnum();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "_enum", lhsEnum), LocatorUtils.property(thatLocator, "_enum", rhsEnum), lhsEnum, rhsEnum)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof MVCViewSecurityTokenKeyChoice) {
            final MVCViewSecurityTokenKeyChoice copy = ((MVCViewSecurityTokenKeyChoice) draftCopy);
            if (this.text!= null) {
                StringValueExpression sourceText;
                sourceText = this.getText();
                StringValueExpression copyText = ((StringValueExpression) strategy.copy(LocatorUtils.property(locator, "text", sourceText), sourceText));
                copy.setText(copyText);
            } else {
                copy.text = null;
            }
            if (this._enum!= null) {
                EnumValueExpression sourceEnum;
                sourceEnum = this.getEnum();
                EnumValueExpression copyEnum = ((EnumValueExpression) strategy.copy(LocatorUtils.property(locator, "_enum", sourceEnum), sourceEnum));
                copy.setEnum(copyEnum);
            } else {
                copy._enum = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewSecurityTokenKeyChoice();
    }
    
//--simple--preserve

    public String getCSharpExpression(com.mda.engine.core.Product product){
  		String expression = "";
  		
  		if(this.text != null){
  			expression = this.text.getCSharpExpression(product);
  		} else if(this._enum != null){
  			expression = this._enum.getCSharpExpression(product);
  		}
  		
  		return expression;
  	}

//--simple--preserve

}
