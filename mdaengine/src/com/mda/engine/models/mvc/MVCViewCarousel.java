
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewCarousel complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewCarousel">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewElement">
 *       &lt;sequence>
 *         &lt;element name="SlidesToShow" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="SlidesToScroll" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="InfiniteSlide" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewCarousel", propOrder = {
    "slidesToShow",
    "slidesToScroll",
    "infiniteSlide"
})
public class MVCViewCarousel
    extends MVCViewElement
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "SlidesToShow")
    protected Integer slidesToShow;
    @XmlElement(name = "SlidesToScroll")
    protected Integer slidesToScroll;
    @XmlElement(name = "InfiniteSlide", defaultValue = "false")
    protected Boolean infiniteSlide;

    /**
     * Gets the value of the slidesToShow property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSlidesToShow() {
        return slidesToShow;
    }

    /**
     * Sets the value of the slidesToShow property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSlidesToShow(Integer value) {
        this.slidesToShow = value;
    }

    /**
     * Gets the value of the slidesToScroll property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSlidesToScroll() {
        return slidesToScroll;
    }

    /**
     * Sets the value of the slidesToScroll property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSlidesToScroll(Integer value) {
        this.slidesToScroll = value;
    }

    /**
     * Gets the value of the infiniteSlide property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isInfiniteSlide() {
        return infiniteSlide;
    }

    /**
     * Sets the value of the infiniteSlide property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInfiniteSlide(Boolean value) {
        this.infiniteSlide = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewCarousel)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewCarousel that = ((MVCViewCarousel) object);
        {
            Integer lhsSlidesToShow;
            lhsSlidesToShow = this.getSlidesToShow();
            Integer rhsSlidesToShow;
            rhsSlidesToShow = that.getSlidesToShow();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "slidesToShow", lhsSlidesToShow), LocatorUtils.property(thatLocator, "slidesToShow", rhsSlidesToShow), lhsSlidesToShow, rhsSlidesToShow)) {
                return false;
            }
        }
        {
            Integer lhsSlidesToScroll;
            lhsSlidesToScroll = this.getSlidesToScroll();
            Integer rhsSlidesToScroll;
            rhsSlidesToScroll = that.getSlidesToScroll();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "slidesToScroll", lhsSlidesToScroll), LocatorUtils.property(thatLocator, "slidesToScroll", rhsSlidesToScroll), lhsSlidesToScroll, rhsSlidesToScroll)) {
                return false;
            }
        }
        {
            Boolean lhsInfiniteSlide;
            lhsInfiniteSlide = this.isInfiniteSlide();
            Boolean rhsInfiniteSlide;
            rhsInfiniteSlide = that.isInfiniteSlide();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "infiniteSlide", lhsInfiniteSlide), LocatorUtils.property(thatLocator, "infiniteSlide", rhsInfiniteSlide), lhsInfiniteSlide, rhsInfiniteSlide)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCViewCarousel) {
            final MVCViewCarousel copy = ((MVCViewCarousel) draftCopy);
            if (this.slidesToShow!= null) {
                Integer sourceSlidesToShow;
                sourceSlidesToShow = this.getSlidesToShow();
                Integer copySlidesToShow = ((Integer) strategy.copy(LocatorUtils.property(locator, "slidesToShow", sourceSlidesToShow), sourceSlidesToShow));
                copy.setSlidesToShow(copySlidesToShow);
            } else {
                copy.slidesToShow = null;
            }
            if (this.slidesToScroll!= null) {
                Integer sourceSlidesToScroll;
                sourceSlidesToScroll = this.getSlidesToScroll();
                Integer copySlidesToScroll = ((Integer) strategy.copy(LocatorUtils.property(locator, "slidesToScroll", sourceSlidesToScroll), sourceSlidesToScroll));
                copy.setSlidesToScroll(copySlidesToScroll);
            } else {
                copy.slidesToScroll = null;
            }
            if (this.infiniteSlide!= null) {
                Boolean sourceInfiniteSlide;
                sourceInfiniteSlide = this.isInfiniteSlide();
                Boolean copyInfiniteSlide = ((Boolean) strategy.copy(LocatorUtils.property(locator, "infiniteSlide", sourceInfiniteSlide), sourceInfiniteSlide));
                copy.setInfiniteSlide(copyInfiniteSlide);
            } else {
                copy.infiniteSlide = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewCarousel();
    }
    
//--simple--preserve
    
    @Override
    public String getClassName() {
    	return "MVCViewCarousel";
    }
    
    @Override
    public void writeInitializeMethodInternal(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product) throws Exception {
    	
    	String pascalCaseId = com.mda.engine.utils.StringUtils.getPascalCase(this.getId());
    	String elementPropertyName = pascalCaseId + "Element";
    	
    	if(this.infiniteSlide != null && this.infiniteSlide.booleanValue()){
    		c.line("{0}.InfiniteSlide = true;", elementPropertyName);
    	}
    	
    	if(this.slidesToScroll != null){
    		c.line("{0}.SlidesToScroll = {1};", elementPropertyName, this.slidesToScroll.intValue());
    	}
    	
    	if(this.slidesToShow != null){
    		c.line("{0}.SlidesToShow = {1};", elementPropertyName, this.slidesToShow.intValue());
    	}
    	
    	c.line();
    	c.line("this.Initialize{0}CarouselItems(this.{0}Element);", pascalCaseId);
    	c.line();
    }
    
    @Override
    public void writeExtraMethodsToInitialize(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product) throws Exception {
    	super.writeExtraMethodsToInitialize(c, product);
    	
    	String pascalCaseId = com.mda.engine.utils.StringUtils.getPascalCase(this.getId());
    	
    	c.line();
		c.line("/// <summary>");
        c.line("/// Method to initialize the element {0} carousel items.", pascalCaseId);
        c.line("/// </summary>");
    	c.line("protected virtual void Initialize{0}CarouselItems({1} carousel)", pascalCaseId, this.getClassName());
    	c.line("{ }");
    }
    
//--simple--preserve

}
