
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewCheckBox complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewCheckBox">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewInput">
 *       &lt;sequence>
 *         &lt;element name="Label" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValueChoice" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Style" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewRadioButtonGroupStyle" default="Regular" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewCheckBox", propOrder = {
    "label"
})
public class MVCViewCheckBox
    extends MVCViewInput
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Label")
    protected MVCViewValueChoice label;
    @XmlAttribute(name = "Style")
    protected MVCViewRadioButtonGroupStyle style;

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public MVCViewValueChoice getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public void setLabel(MVCViewValueChoice value) {
        this.label = value;
    }

    /**
     * Gets the value of the style property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewRadioButtonGroupStyle }
     *     
     */
    public MVCViewRadioButtonGroupStyle getStyle() {
        if (style == null) {
            return MVCViewRadioButtonGroupStyle.REGULAR;
        } else {
            return style;
        }
    }

    /**
     * Sets the value of the style property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewRadioButtonGroupStyle }
     *     
     */
    public void setStyle(MVCViewRadioButtonGroupStyle value) {
        this.style = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewCheckBox)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewCheckBox that = ((MVCViewCheckBox) object);
        {
            MVCViewValueChoice lhsLabel;
            lhsLabel = this.getLabel();
            MVCViewValueChoice rhsLabel;
            rhsLabel = that.getLabel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "label", lhsLabel), LocatorUtils.property(thatLocator, "label", rhsLabel), lhsLabel, rhsLabel)) {
                return false;
            }
        }
        {
            MVCViewRadioButtonGroupStyle lhsStyle;
            lhsStyle = this.getStyle();
            MVCViewRadioButtonGroupStyle rhsStyle;
            rhsStyle = that.getStyle();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "style", lhsStyle), LocatorUtils.property(thatLocator, "style", rhsStyle), lhsStyle, rhsStyle)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCViewCheckBox) {
            final MVCViewCheckBox copy = ((MVCViewCheckBox) draftCopy);
            if (this.label!= null) {
                MVCViewValueChoice sourceLabel;
                sourceLabel = this.getLabel();
                MVCViewValueChoice copyLabel = ((MVCViewValueChoice) strategy.copy(LocatorUtils.property(locator, "label", sourceLabel), sourceLabel));
                copy.setLabel(copyLabel);
            } else {
                copy.label = null;
            }
            if (this.style!= null) {
                MVCViewRadioButtonGroupStyle sourceStyle;
                sourceStyle = this.getStyle();
                MVCViewRadioButtonGroupStyle copyStyle = ((MVCViewRadioButtonGroupStyle) strategy.copy(LocatorUtils.property(locator, "style", sourceStyle), sourceStyle));
                copy.setStyle(copyStyle);
            } else {
                copy.style = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewCheckBox();
    }
    
//--simple--preserve
    
    @Override
    public String getClassName()
    {
    	if (getValue() != null)
    	{
    		return "MVCViewCheckBox<"+getValue().getSingleClassName() + ">";
    	}
    	return "MVCViewCheckBox<Boolean>";
    }
    
    @Override
    public void writeHtml(com.mda.engine.utils.Stringcode c)
    {
    	c.write("<input type=\"check\"");
    	if (getId() != null)
    	{
    		c.write(" id=\"{0}\"", getId());
    	}
    	if (getCss() != null)
    	{
    		c.write(" class=\"{0}\"", getCss());
    	}
    	if (getValue() != null)
    	{
    		c.write(" value=\"");
    		writeHtmlInputValue(c);
    		c.write("\"");
    	}
    	c.write("/>");
    	c.writeLine();
    }
    
    @Override
    public void writeInitializeMethodInternal(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product)
    {
    	String elementId = getViewElementId();
     	super.writeInitializeMethodInternal(c, product);
    	if (getValue() != null)
    	{
    		getValue().writeBind(c, elementId, "IsCheckedValue", false);
    	}
    	if (getLabel() != null)
    	{
    		getLabel().writeBind(c, elementId, "LabelBind", false);
    	}
    	
    	c.line("{1}.RenderStyle = MVCViewRadioButtonRenderStyle.{0};", this.getStyle().value(), elementId);
    }
    
    @Override
    public void setBinds(MVCModel model) throws Exception
    {
    	super.setBinds(model);
    	if (getLabel() != null)
    	{
    		getLabel().setBinds(model, getContextElement(), this);
    	}
    }
//--simple--preserve

}
