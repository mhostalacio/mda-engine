
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.IMVCViewElementWithDatasource;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewLookup complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewLookup">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewElement">
 *       &lt;sequence>
 *         &lt;element name="DataSource" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValueChoice"/>
 *         &lt;element name="HiddenValue" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValueChoice"/>
 *         &lt;element name="DisplayValue" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValueChoice"/>
 *         &lt;element name="AutoCompleteTemplate" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewRepeaterTemplate"/>
 *         &lt;element name="AutoCompleteAction" type="{http://www.mdaengine.com/mdaengine/models/mvc}CallActionTrigger" minOccurs="0"/>
 *         &lt;element name="OnSelect" type="{http://www.mdaengine.com/mdaengine/models/mvc}OnSelectTrigger" minOccurs="0"/>
 *         &lt;element name="OpenPopup" type="{http://www.mdaengine.com/mdaengine/models/mvc}OpenPopupTrigger" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewLookup", propOrder = {
    "dataSource",
    "hiddenValue",
    "displayValue",
    "autoCompleteTemplate",
    "autoCompleteAction",
    "onSelect",
    "openPopup"
})
public class MVCViewLookup
    extends MVCViewElement
    implements Serializable, Cloneable, IMVCViewElementWithDatasource, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "DataSource", required = true)
    protected MVCViewValueChoice dataSource;
    @XmlElement(name = "HiddenValue", required = true)
    protected MVCViewValueChoice hiddenValue;
    @XmlElement(name = "DisplayValue", required = true)
    protected MVCViewValueChoice displayValue;
    @XmlElement(name = "AutoCompleteTemplate", required = true)
    protected MVCViewRepeaterTemplate autoCompleteTemplate;
    @XmlElement(name = "AutoCompleteAction")
    protected CallActionTrigger autoCompleteAction;
    @XmlElement(name = "OnSelect")
    protected OnSelectTrigger onSelect;
    @XmlElement(name = "OpenPopup")
    protected OpenPopupTrigger openPopup;

    /**
     * Gets the value of the dataSource property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public MVCViewValueChoice getDataSource() {
        return dataSource;
    }

    /**
     * Sets the value of the dataSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public void setDataSource(MVCViewValueChoice value) {
        this.dataSource = value;
    }

    /**
     * Gets the value of the hiddenValue property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public MVCViewValueChoice getHiddenValue() {
        return hiddenValue;
    }

    /**
     * Sets the value of the hiddenValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public void setHiddenValue(MVCViewValueChoice value) {
        this.hiddenValue = value;
    }

    /**
     * Gets the value of the displayValue property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public MVCViewValueChoice getDisplayValue() {
        return displayValue;
    }

    /**
     * Sets the value of the displayValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public void setDisplayValue(MVCViewValueChoice value) {
        this.displayValue = value;
    }

    /**
     * Gets the value of the autoCompleteTemplate property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewRepeaterTemplate }
     *     
     */
    public MVCViewRepeaterTemplate getAutoCompleteTemplate() {
        return autoCompleteTemplate;
    }

    /**
     * Sets the value of the autoCompleteTemplate property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewRepeaterTemplate }
     *     
     */
    public void setAutoCompleteTemplate(MVCViewRepeaterTemplate value) {
        this.autoCompleteTemplate = value;
    }

    /**
     * Gets the value of the autoCompleteAction property.
     * 
     * @return
     *     possible object is
     *     {@link CallActionTrigger }
     *     
     */
    public CallActionTrigger getAutoCompleteAction() {
        return autoCompleteAction;
    }

    /**
     * Sets the value of the autoCompleteAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link CallActionTrigger }
     *     
     */
    public void setAutoCompleteAction(CallActionTrigger value) {
        this.autoCompleteAction = value;
    }

    /**
     * Gets the value of the onSelect property.
     * 
     * @return
     *     possible object is
     *     {@link OnSelectTrigger }
     *     
     */
    public OnSelectTrigger getOnSelect() {
        return onSelect;
    }

    /**
     * Sets the value of the onSelect property.
     * 
     * @param value
     *     allowed object is
     *     {@link OnSelectTrigger }
     *     
     */
    public void setOnSelect(OnSelectTrigger value) {
        this.onSelect = value;
    }

    /**
     * Gets the value of the openPopup property.
     * 
     * @return
     *     possible object is
     *     {@link OpenPopupTrigger }
     *     
     */
    public OpenPopupTrigger getOpenPopup() {
        return openPopup;
    }

    /**
     * Sets the value of the openPopup property.
     * 
     * @param value
     *     allowed object is
     *     {@link OpenPopupTrigger }
     *     
     */
    public void setOpenPopup(OpenPopupTrigger value) {
        this.openPopup = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewLookup)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewLookup that = ((MVCViewLookup) object);
        {
            MVCViewValueChoice lhsDataSource;
            lhsDataSource = this.getDataSource();
            MVCViewValueChoice rhsDataSource;
            rhsDataSource = that.getDataSource();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dataSource", lhsDataSource), LocatorUtils.property(thatLocator, "dataSource", rhsDataSource), lhsDataSource, rhsDataSource)) {
                return false;
            }
        }
        {
            MVCViewValueChoice lhsHiddenValue;
            lhsHiddenValue = this.getHiddenValue();
            MVCViewValueChoice rhsHiddenValue;
            rhsHiddenValue = that.getHiddenValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "hiddenValue", lhsHiddenValue), LocatorUtils.property(thatLocator, "hiddenValue", rhsHiddenValue), lhsHiddenValue, rhsHiddenValue)) {
                return false;
            }
        }
        {
            MVCViewValueChoice lhsDisplayValue;
            lhsDisplayValue = this.getDisplayValue();
            MVCViewValueChoice rhsDisplayValue;
            rhsDisplayValue = that.getDisplayValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "displayValue", lhsDisplayValue), LocatorUtils.property(thatLocator, "displayValue", rhsDisplayValue), lhsDisplayValue, rhsDisplayValue)) {
                return false;
            }
        }
        {
            MVCViewRepeaterTemplate lhsAutoCompleteTemplate;
            lhsAutoCompleteTemplate = this.getAutoCompleteTemplate();
            MVCViewRepeaterTemplate rhsAutoCompleteTemplate;
            rhsAutoCompleteTemplate = that.getAutoCompleteTemplate();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "autoCompleteTemplate", lhsAutoCompleteTemplate), LocatorUtils.property(thatLocator, "autoCompleteTemplate", rhsAutoCompleteTemplate), lhsAutoCompleteTemplate, rhsAutoCompleteTemplate)) {
                return false;
            }
        }
        {
            CallActionTrigger lhsAutoCompleteAction;
            lhsAutoCompleteAction = this.getAutoCompleteAction();
            CallActionTrigger rhsAutoCompleteAction;
            rhsAutoCompleteAction = that.getAutoCompleteAction();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "autoCompleteAction", lhsAutoCompleteAction), LocatorUtils.property(thatLocator, "autoCompleteAction", rhsAutoCompleteAction), lhsAutoCompleteAction, rhsAutoCompleteAction)) {
                return false;
            }
        }
        {
            OnSelectTrigger lhsOnSelect;
            lhsOnSelect = this.getOnSelect();
            OnSelectTrigger rhsOnSelect;
            rhsOnSelect = that.getOnSelect();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "onSelect", lhsOnSelect), LocatorUtils.property(thatLocator, "onSelect", rhsOnSelect), lhsOnSelect, rhsOnSelect)) {
                return false;
            }
        }
        {
            OpenPopupTrigger lhsOpenPopup;
            lhsOpenPopup = this.getOpenPopup();
            OpenPopupTrigger rhsOpenPopup;
            rhsOpenPopup = that.getOpenPopup();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "openPopup", lhsOpenPopup), LocatorUtils.property(thatLocator, "openPopup", rhsOpenPopup), lhsOpenPopup, rhsOpenPopup)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCViewLookup) {
            final MVCViewLookup copy = ((MVCViewLookup) draftCopy);
            if (this.dataSource!= null) {
                MVCViewValueChoice sourceDataSource;
                sourceDataSource = this.getDataSource();
                MVCViewValueChoice copyDataSource = ((MVCViewValueChoice) strategy.copy(LocatorUtils.property(locator, "dataSource", sourceDataSource), sourceDataSource));
                copy.setDataSource(copyDataSource);
            } else {
                copy.dataSource = null;
            }
            if (this.hiddenValue!= null) {
                MVCViewValueChoice sourceHiddenValue;
                sourceHiddenValue = this.getHiddenValue();
                MVCViewValueChoice copyHiddenValue = ((MVCViewValueChoice) strategy.copy(LocatorUtils.property(locator, "hiddenValue", sourceHiddenValue), sourceHiddenValue));
                copy.setHiddenValue(copyHiddenValue);
            } else {
                copy.hiddenValue = null;
            }
            if (this.displayValue!= null) {
                MVCViewValueChoice sourceDisplayValue;
                sourceDisplayValue = this.getDisplayValue();
                MVCViewValueChoice copyDisplayValue = ((MVCViewValueChoice) strategy.copy(LocatorUtils.property(locator, "displayValue", sourceDisplayValue), sourceDisplayValue));
                copy.setDisplayValue(copyDisplayValue);
            } else {
                copy.displayValue = null;
            }
            if (this.autoCompleteTemplate!= null) {
                MVCViewRepeaterTemplate sourceAutoCompleteTemplate;
                sourceAutoCompleteTemplate = this.getAutoCompleteTemplate();
                MVCViewRepeaterTemplate copyAutoCompleteTemplate = ((MVCViewRepeaterTemplate) strategy.copy(LocatorUtils.property(locator, "autoCompleteTemplate", sourceAutoCompleteTemplate), sourceAutoCompleteTemplate));
                copy.setAutoCompleteTemplate(copyAutoCompleteTemplate);
            } else {
                copy.autoCompleteTemplate = null;
            }
            if (this.autoCompleteAction!= null) {
                CallActionTrigger sourceAutoCompleteAction;
                sourceAutoCompleteAction = this.getAutoCompleteAction();
                CallActionTrigger copyAutoCompleteAction = ((CallActionTrigger) strategy.copy(LocatorUtils.property(locator, "autoCompleteAction", sourceAutoCompleteAction), sourceAutoCompleteAction));
                copy.setAutoCompleteAction(copyAutoCompleteAction);
            } else {
                copy.autoCompleteAction = null;
            }
            if (this.onSelect!= null) {
                OnSelectTrigger sourceOnSelect;
                sourceOnSelect = this.getOnSelect();
                OnSelectTrigger copyOnSelect = ((OnSelectTrigger) strategy.copy(LocatorUtils.property(locator, "onSelect", sourceOnSelect), sourceOnSelect));
                copy.setOnSelect(copyOnSelect);
            } else {
                copy.onSelect = null;
            }
            if (this.openPopup!= null) {
                OpenPopupTrigger sourceOpenPopup;
                sourceOpenPopup = this.getOpenPopup();
                OpenPopupTrigger copyOpenPopup = ((OpenPopupTrigger) strategy.copy(LocatorUtils.property(locator, "openPopup", sourceOpenPopup), sourceOpenPopup));
                copy.setOpenPopup(copyOpenPopup);
            } else {
                copy.openPopup = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewLookup();
    }
    
//--simple--preserve

    private transient MVCViewHidden hiddenField;
	private transient MVCViewTextBox textBox;
	private transient MVCViewButton button;

	public MVCViewButton getButton() {
		return button;
	}
	
    public MVCViewHidden getHiddenField() {
		return hiddenField;
	}

    public MVCViewTextBox getTextBox() {
		return textBox;
	}
    
	@Override
	public String getDataSourceSingleClassName()
	{
		return this.getDataSource().getSingleClassName();
	}
	
	@Override
    public String getClassName() throws Exception
    {
    	if (getDataSource() != null)
    	{
    		return "MVCViewLookup<"+getDataSource().getSingleClassName()+","+ getHiddenValue().getSingleClassName()+">";
    	}
    	throw new Exception("Datasource definition is missing");
    }
	
	@Override
    public void writeInitializeMethodInternal(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product) throws Exception
    {
    	String elementId = getViewElementId();
    	String camelName = com.mda.engine.utils.StringUtils.getCamelCase(getId());
    	java.util.HashMap<String, String> args = this.getAutoCompleteTemplate().getElementList().get(0).parseInitializeArgumentsCall(elementId, false);
    	String extraArguments = args.get("extraArguments");
    	String dataArgument = args.get("dataArgument");
    	//c.line("{0} {1} = null;", this.getDataSource().getSingleClassName(), camelName);
    	c.line("int {0}Index = 0;", camelName);
    	c.writeLine("");
    	c.write("{0}.HiddenField = ", elementId);
    	hiddenField.writeInitializeMethodCall(c, null, true, product, false);
    	c.writeLine("");
    	c.write("{0}.TextBox = ", elementId);
    	textBox.writeInitializeMethodCall(c, null, true, product, false);
    	c.writeLine("");
    	c.write("{0}.Button = ", elementId);
    	button.writeInitializeMethodCall(c, null, true, product, false);
    	c.line("{0}.ElementCreator = delegate({1} {4}, IMVCViewElement parentElement, int index) { return {2}CreateElement({3}); };", elementId, getDataSource().getSingleClassName(), com.mda.engine.utils.StringUtils.getPascalCase(getId()), extraArguments, camelName);
    	if (getOnSelect() != null)
    	{
    		c.line("{0}.DataKeyValues = delegate({1} {4}) { return new Dictionary<String, String>() { { \"HiddenValue\", Convert.ToString({2}) }, { \"DisplayValue\", Convert.ToString({3}) }}; };", elementId, getDataSource().getSingleClassName(), getOnSelect().getSetHiddenValue().getModelAttribute().getPath(), getOnSelect().getSetDisplayValue().getModelAttribute().getPath(), dataArgument);
    		if (getOnSelect().getTriggers() != null && getOnSelect().getTriggers().getTriggerList() != null && getOnSelect().getTriggers().getTriggerList().size() > 0)
    		{
    			for (com.mda.engine.models.mvc.MVCTrigger trigger : getOnSelect().getTriggers().getTriggerList()) {
    				trigger.writeInitializeTrigger(c, product, elementId, "OnSelect");
    			}
    		}
    	}
    }
	
	@Override
    public void setBinds(MVCModel model) throws Exception
    {
		hiddenField = new MVCViewHidden();
		textBox = new MVCViewTextBox();
		button = new MVCViewButton();
		
		hiddenField.setId(getId() + "LookupHidden");
		textBox.setId(getId() + "LookupText");
		button.setId(getId() + "LookupButton");
		
		hiddenField.setContextElement(this.getContextElement());
		textBox.setContextElement(this.getContextElement());
		button.setContextElement(this.getContextElement());
		
		textBox.setHelpText(getHelpText());
		textBox.setValue(this.getDisplayValue());
		hiddenField.setValue(this.getHiddenValue());
		
    	super.setBinds(model);
    	if (getDataSource() != null)
    	{
    		getDataSource().setBinds(model, this, this);
    	}
    	if (getAutoCompleteTemplate() != null)
    	{
    		if (getAutoCompleteTemplate().getElementList() != null)
    		{
    			for (MVCViewElement child : getAutoCompleteTemplate().getElementList())
    			{
    				child.setContextElement(this);
    				child.setBinds(model);
    			}
    		}
    	}
    	
    	if (this.getOpenPopup() != null)
		{
    		this.getButton().setOnClick(new MVCTriggerCollection());
    		this.getButton().getOnClick().getTriggerList().add(this.getOpenPopup());
    		this.getButton().setBinds(model);
		}
    	
    	if (getOnSelect() != null)
    	{
    		com.mda.engine.core.IMVCViewElementWithDatasource previous = getContextElement();
    		setContextElement(this);
    		getOnSelect().getSetHiddenValue().setBinds(model, getContextElement(), this);
    		getOnSelect().getSetDisplayValue().setBinds(model, getContextElement(), this);
    		if (getOnSelect().getTriggers() != null && getOnSelect().getTriggers().getTriggerList() != null && getOnSelect().getTriggers().getTriggerList().size() > 0)
    		{
    			for (com.mda.engine.models.mvc.MVCTrigger trigger : getOnSelect().getTriggers().getTriggerList()) {
    				if (trigger.getArguments() != null && trigger.getArguments().getArgumentList() != null)
    				{
    					for (com.mda.engine.models.mvc.TriggerArgument arg : trigger.getArguments().getArgumentList()) {
    						arg.getValue().setBinds(model, this.getContextElement(), this);
    					}
    				}
    			}
    		}
    		setContextElement(previous);
    	}
    	
    	textBox.setBinds(model);
    	
		hiddenField.setBinds(model);
    }
	
	@Override
    public void writeExtraMethodsToInitialize(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product) throws Exception
    {
		java.util.HashMap<String, String> args = this.getAutoCompleteTemplate().getElementList().get(0).parseInitializeArguments("IMVCViewElement " + getViewElementId(),"");
    	String extraArguments = args.get("extraArguments");

    	c.line();
    	c.line("/// <summary>");
        c.line("/// Method to initialize the elements of '{0}'", this.getId());
        c.line("/// </summary>");
    	c.line("protected virtual IList<IMVCViewElement> {0}CreateElement({1})", com.mda.engine.utils.StringUtils.getPascalCase(getId()), extraArguments);
    	c.line("{");
    	c.tokenAdd2BeginOfLineIndent();
    	c.line("List<IMVCViewElement> elementList = new List<IMVCViewElement>();");
    	for (MVCViewElement elem : this.getAutoCompleteTemplate().getElementList())
		{
    		c.writeLine();
        	c.write("{0} {1} = ",elem.getClassName(), elem.getViewElementId());
        	elem.writeInitializeMethodCall(c, this.getViewElementId(), true, product, false);
        	c.writeLine("");
        	c.writeLine("elementList.Add({0});", elem.getViewElementId());
		}
    	c.writeLine("return elementList;");
    	c.tokenRemove2BeginOfLine();
    	c.line("}");
    	
    	c.line();
		for (MVCViewElement elem : this.getAutoCompleteTemplate().getElementList())
		{
			c.line("/// <summary>");
	        c.line("/// Method to initialize the element '{0}'", elem.getId());
	        c.line("/// </summary>");
			elem.writeInitializeMethod(c, product);
			c.line();
		}
		
		hiddenField.writeInitializeMethod(c, product);
		c.line();
		textBox.writeInitializeMethod(c, product);
		c.line();
		button.writeInitializeMethod(c, product);
    }


	
//--simple--preserve

}
