
package com.mda.engine.models.mvc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for TriggerArgumentsCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TriggerArgumentsCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded" minOccurs="0">
 *         &lt;element name="Argument" type="{http://www.mdaengine.com/mdaengine/models/mvc}TriggerArgument" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TriggerArgumentsCollection", propOrder = {
    "argumentList"
})
public class TriggerArgumentsCollection
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Argument")
    protected List<TriggerArgument> argumentList;

    /**
     * Gets the value of the argumentList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the argumentList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getArgumentList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TriggerArgument }
     * 
     * 
     */
    public List<TriggerArgument> getArgumentList() {
        if (argumentList == null) {
            argumentList = new ArrayList<TriggerArgument>();
        }
        return this.argumentList;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof TriggerArgumentsCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final TriggerArgumentsCollection that = ((TriggerArgumentsCollection) object);
        {
            List<TriggerArgument> lhsArgumentList;
            lhsArgumentList = this.getArgumentList();
            List<TriggerArgument> rhsArgumentList;
            rhsArgumentList = that.getArgumentList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "argumentList", lhsArgumentList), LocatorUtils.property(thatLocator, "argumentList", rhsArgumentList), lhsArgumentList, rhsArgumentList)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof TriggerArgumentsCollection) {
            final TriggerArgumentsCollection copy = ((TriggerArgumentsCollection) draftCopy);
            if ((this.argumentList!= null)&&(!this.argumentList.isEmpty())) {
                List<TriggerArgument> sourceArgumentList;
                sourceArgumentList = this.getArgumentList();
                @SuppressWarnings("unchecked")
                List<TriggerArgument> copyArgumentList = ((List<TriggerArgument> ) strategy.copy(LocatorUtils.property(locator, "argumentList", sourceArgumentList), sourceArgumentList));
                copy.argumentList = null;
                List<TriggerArgument> uniqueArgumentListl = copy.getArgumentList();
                uniqueArgumentListl.addAll(copyArgumentList);
            } else {
                copy.argumentList = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new TriggerArgumentsCollection();
    }

}
