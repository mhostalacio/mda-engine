
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewElementChoice complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewElementChoice">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="ElementReference" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewElementReference" minOccurs="0"/>
 *         &lt;element name="Form" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewForm" minOccurs="0"/>
 *         &lt;element name="Label" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewLabel" minOccurs="0"/>
 *         &lt;element name="Span" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewSpan" minOccurs="0"/>
 *         &lt;element name="H1" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewH1" minOccurs="0"/>
 *         &lt;element name="H2" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewH2" minOccurs="0"/>
 *         &lt;element name="H3" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewH3" minOccurs="0"/>
 *         &lt;element name="H4" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewH4" minOccurs="0"/>
 *         &lt;element name="H5" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewH5" minOccurs="0"/>
 *         &lt;element name="H6" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewH6" minOccurs="0"/>
 *         &lt;element name="P" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewP" minOccurs="0"/>
 *         &lt;element name="Aside" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewAside" minOccurs="0"/>
 *         &lt;element name="Article" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewArticle" minOccurs="0"/>
 *         &lt;element name="Header" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewHeader" minOccurs="0"/>
 *         &lt;element name="Nav" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewNav" minOccurs="0"/>
 *         &lt;element name="TextBox" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewTextBox" minOccurs="0"/>
 *         &lt;element name="NumericBox" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewNumericBox" minOccurs="0"/>
 *         &lt;element name="Lookup" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewLookup" minOccurs="0"/>
 *         &lt;element name="Include" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewInclude" minOccurs="0"/>
 *         &lt;element name="Repeater" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewRepeater" minOccurs="0"/>
 *         &lt;element name="TextArea" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewTextArea" minOccurs="0"/>
 *         &lt;element name="DateBox" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewDateBox" minOccurs="0"/>
 *         &lt;element name="DateTimeBox" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewDateTimeBox" minOccurs="0"/>
 *         &lt;element name="CheckBox" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewCheckBox" minOccurs="0"/>
 *         &lt;element name="RadioButton" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewRadioButton" minOccurs="0"/>
 *         &lt;element name="RadioButtonGroup" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewRadioButtonGroup" minOccurs="0"/>
 *         &lt;element name="DropDown" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewDropDown" minOccurs="0"/>
 *         &lt;element name="Link" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewLink" minOccurs="0"/>
 *         &lt;element name="File" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewFile" minOccurs="0"/>
 *         &lt;element name="Hidden" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewHidden" minOccurs="0"/>
 *         &lt;element name="Image" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewImage" minOccurs="0"/>
 *         &lt;element name="Object" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewObject" minOccurs="0"/>
 *         &lt;element name="Div" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewDiv" minOccurs="0"/>
 *         &lt;element name="Table" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewTable" minOccurs="0"/>
 *         &lt;element name="Row" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewTableRow" minOccurs="0"/>
 *         &lt;element name="Cell" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewTableCell" minOccurs="0"/>
 *         &lt;element name="Button" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewButton" minOccurs="0"/>
 *         &lt;element name="FieldSet" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewFieldSet" minOccurs="0"/>
 *         &lt;element name="Literal" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewLiteral" minOccurs="0"/>
 *         &lt;element name="Script" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewScript" minOccurs="0"/>
 *         &lt;element name="BreakLine" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewBreakLine" minOccurs="0"/>
 *         &lt;element name="HTMLEditor" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewHtmlEditor" minOccurs="0"/>
 *         &lt;element name="TreeView" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewTreeView" minOccurs="0"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewElementChoice", propOrder = {
    "elementReference",
    "form",
    "label",
    "span",
    "h1",
    "h2",
    "h3",
    "h4",
    "h5",
    "h6",
    "p",
    "aside",
    "article",
    "header",
    "nav",
    "textBox",
    "numericBox",
    "lookup",
    "include",
    "repeater",
    "textArea",
    "dateBox",
    "dateTimeBox",
    "checkBox",
    "radioButton",
    "radioButtonGroup",
    "dropDown",
    "link",
    "file",
    "hidden",
    "image",
    "object",
    "div",
    "table",
    "row",
    "cell",
    "button",
    "fieldSet",
    "literal",
    "script",
    "breakLine",
    "htmlEditor",
    "treeView"
})
public class MVCViewElementChoice
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "ElementReference")
    protected MVCViewElementReference elementReference;
    @XmlElement(name = "Form")
    protected MVCViewForm form;
    @XmlElement(name = "Label")
    protected MVCViewLabel label;
    @XmlElement(name = "Span")
    protected MVCViewSpan span;
    @XmlElement(name = "H1")
    protected MVCViewH1 h1;
    @XmlElement(name = "H2")
    protected MVCViewH2 h2;
    @XmlElement(name = "H3")
    protected MVCViewH3 h3;
    @XmlElement(name = "H4")
    protected MVCViewH4 h4;
    @XmlElement(name = "H5")
    protected MVCViewH5 h5;
    @XmlElement(name = "H6")
    protected MVCViewH6 h6;
    @XmlElement(name = "P")
    protected MVCViewP p;
    @XmlElement(name = "Aside")
    protected MVCViewAside aside;
    @XmlElement(name = "Article")
    protected MVCViewArticle article;
    @XmlElement(name = "Header")
    protected MVCViewHeader header;
    @XmlElement(name = "Nav")
    protected MVCViewNav nav;
    @XmlElement(name = "TextBox")
    protected MVCViewTextBox textBox;
    @XmlElement(name = "NumericBox")
    protected MVCViewNumericBox numericBox;
    @XmlElement(name = "Lookup")
    protected MVCViewLookup lookup;
    @XmlElement(name = "Include")
    protected MVCViewInclude include;
    @XmlElement(name = "Repeater")
    protected MVCViewRepeater repeater;
    @XmlElement(name = "TextArea")
    protected MVCViewTextArea textArea;
    @XmlElement(name = "DateBox")
    protected MVCViewDateBox dateBox;
    @XmlElement(name = "DateTimeBox")
    protected MVCViewDateTimeBox dateTimeBox;
    @XmlElement(name = "CheckBox")
    protected MVCViewCheckBox checkBox;
    @XmlElement(name = "RadioButton")
    protected MVCViewRadioButton radioButton;
    @XmlElement(name = "RadioButtonGroup")
    protected MVCViewRadioButtonGroup radioButtonGroup;
    @XmlElement(name = "DropDown")
    protected MVCViewDropDown dropDown;
    @XmlElement(name = "Link")
    protected MVCViewLink link;
    @XmlElement(name = "File")
    protected MVCViewFile file;
    @XmlElement(name = "Hidden")
    protected MVCViewHidden hidden;
    @XmlElement(name = "Image")
    protected MVCViewImage image;
    @XmlElement(name = "Object")
    protected MVCViewObject object;
    @XmlElement(name = "Div")
    protected MVCViewDiv div;
    @XmlElement(name = "Table")
    protected MVCViewTable table;
    @XmlElement(name = "Row")
    protected MVCViewTableRow row;
    @XmlElement(name = "Cell")
    protected MVCViewTableCell cell;
    @XmlElement(name = "Button")
    protected MVCViewButton button;
    @XmlElement(name = "FieldSet")
    protected MVCViewFieldSet fieldSet;
    @XmlElement(name = "Literal")
    protected MVCViewLiteral literal;
    @XmlElement(name = "Script")
    protected MVCViewScript script;
    @XmlElement(name = "BreakLine")
    protected MVCViewBreakLine breakLine;
    @XmlElement(name = "HTMLEditor")
    protected MVCViewHtmlEditor htmlEditor;
    @XmlElement(name = "TreeView")
    protected MVCViewTreeView treeView;

    /**
     * Gets the value of the elementReference property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewElementReference }
     *     
     */
    public MVCViewElementReference getElementReference() {
        return elementReference;
    }

    /**
     * Sets the value of the elementReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewElementReference }
     *     
     */
    public void setElementReference(MVCViewElementReference value) {
        this.elementReference = value;
    }

    /**
     * Gets the value of the form property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewForm }
     *     
     */
    public MVCViewForm getForm() {
        return form;
    }

    /**
     * Sets the value of the form property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewForm }
     *     
     */
    public void setForm(MVCViewForm value) {
        this.form = value;
    }

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewLabel }
     *     
     */
    public MVCViewLabel getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewLabel }
     *     
     */
    public void setLabel(MVCViewLabel value) {
        this.label = value;
    }

    /**
     * Gets the value of the span property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewSpan }
     *     
     */
    public MVCViewSpan getSpan() {
        return span;
    }

    /**
     * Sets the value of the span property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewSpan }
     *     
     */
    public void setSpan(MVCViewSpan value) {
        this.span = value;
    }

    /**
     * Gets the value of the h1 property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewH1 }
     *     
     */
    public MVCViewH1 getH1() {
        return h1;
    }

    /**
     * Sets the value of the h1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewH1 }
     *     
     */
    public void setH1(MVCViewH1 value) {
        this.h1 = value;
    }

    /**
     * Gets the value of the h2 property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewH2 }
     *     
     */
    public MVCViewH2 getH2() {
        return h2;
    }

    /**
     * Sets the value of the h2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewH2 }
     *     
     */
    public void setH2(MVCViewH2 value) {
        this.h2 = value;
    }

    /**
     * Gets the value of the h3 property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewH3 }
     *     
     */
    public MVCViewH3 getH3() {
        return h3;
    }

    /**
     * Sets the value of the h3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewH3 }
     *     
     */
    public void setH3(MVCViewH3 value) {
        this.h3 = value;
    }

    /**
     * Gets the value of the h4 property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewH4 }
     *     
     */
    public MVCViewH4 getH4() {
        return h4;
    }

    /**
     * Sets the value of the h4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewH4 }
     *     
     */
    public void setH4(MVCViewH4 value) {
        this.h4 = value;
    }

    /**
     * Gets the value of the h5 property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewH5 }
     *     
     */
    public MVCViewH5 getH5() {
        return h5;
    }

    /**
     * Sets the value of the h5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewH5 }
     *     
     */
    public void setH5(MVCViewH5 value) {
        this.h5 = value;
    }

    /**
     * Gets the value of the h6 property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewH6 }
     *     
     */
    public MVCViewH6 getH6() {
        return h6;
    }

    /**
     * Sets the value of the h6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewH6 }
     *     
     */
    public void setH6(MVCViewH6 value) {
        this.h6 = value;
    }

    /**
     * Gets the value of the p property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewP }
     *     
     */
    public MVCViewP getP() {
        return p;
    }

    /**
     * Sets the value of the p property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewP }
     *     
     */
    public void setP(MVCViewP value) {
        this.p = value;
    }

    /**
     * Gets the value of the aside property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewAside }
     *     
     */
    public MVCViewAside getAside() {
        return aside;
    }

    /**
     * Sets the value of the aside property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewAside }
     *     
     */
    public void setAside(MVCViewAside value) {
        this.aside = value;
    }

    /**
     * Gets the value of the article property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewArticle }
     *     
     */
    public MVCViewArticle getArticle() {
        return article;
    }

    /**
     * Sets the value of the article property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewArticle }
     *     
     */
    public void setArticle(MVCViewArticle value) {
        this.article = value;
    }

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewHeader }
     *     
     */
    public MVCViewHeader getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewHeader }
     *     
     */
    public void setHeader(MVCViewHeader value) {
        this.header = value;
    }

    /**
     * Gets the value of the nav property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewNav }
     *     
     */
    public MVCViewNav getNav() {
        return nav;
    }

    /**
     * Sets the value of the nav property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewNav }
     *     
     */
    public void setNav(MVCViewNav value) {
        this.nav = value;
    }

    /**
     * Gets the value of the textBox property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewTextBox }
     *     
     */
    public MVCViewTextBox getTextBox() {
        return textBox;
    }

    /**
     * Sets the value of the textBox property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewTextBox }
     *     
     */
    public void setTextBox(MVCViewTextBox value) {
        this.textBox = value;
    }

    /**
     * Gets the value of the numericBox property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewNumericBox }
     *     
     */
    public MVCViewNumericBox getNumericBox() {
        return numericBox;
    }

    /**
     * Sets the value of the numericBox property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewNumericBox }
     *     
     */
    public void setNumericBox(MVCViewNumericBox value) {
        this.numericBox = value;
    }

    /**
     * Gets the value of the lookup property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewLookup }
     *     
     */
    public MVCViewLookup getLookup() {
        return lookup;
    }

    /**
     * Sets the value of the lookup property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewLookup }
     *     
     */
    public void setLookup(MVCViewLookup value) {
        this.lookup = value;
    }

    /**
     * Gets the value of the include property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewInclude }
     *     
     */
    public MVCViewInclude getInclude() {
        return include;
    }

    /**
     * Sets the value of the include property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewInclude }
     *     
     */
    public void setInclude(MVCViewInclude value) {
        this.include = value;
    }

    /**
     * Gets the value of the repeater property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewRepeater }
     *     
     */
    public MVCViewRepeater getRepeater() {
        return repeater;
    }

    /**
     * Sets the value of the repeater property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewRepeater }
     *     
     */
    public void setRepeater(MVCViewRepeater value) {
        this.repeater = value;
    }

    /**
     * Gets the value of the textArea property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewTextArea }
     *     
     */
    public MVCViewTextArea getTextArea() {
        return textArea;
    }

    /**
     * Sets the value of the textArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewTextArea }
     *     
     */
    public void setTextArea(MVCViewTextArea value) {
        this.textArea = value;
    }

    /**
     * Gets the value of the dateBox property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewDateBox }
     *     
     */
    public MVCViewDateBox getDateBox() {
        return dateBox;
    }

    /**
     * Sets the value of the dateBox property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewDateBox }
     *     
     */
    public void setDateBox(MVCViewDateBox value) {
        this.dateBox = value;
    }

    /**
     * Gets the value of the dateTimeBox property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewDateTimeBox }
     *     
     */
    public MVCViewDateTimeBox getDateTimeBox() {
        return dateTimeBox;
    }

    /**
     * Sets the value of the dateTimeBox property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewDateTimeBox }
     *     
     */
    public void setDateTimeBox(MVCViewDateTimeBox value) {
        this.dateTimeBox = value;
    }

    /**
     * Gets the value of the checkBox property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewCheckBox }
     *     
     */
    public MVCViewCheckBox getCheckBox() {
        return checkBox;
    }

    /**
     * Sets the value of the checkBox property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewCheckBox }
     *     
     */
    public void setCheckBox(MVCViewCheckBox value) {
        this.checkBox = value;
    }

    /**
     * Gets the value of the radioButton property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewRadioButton }
     *     
     */
    public MVCViewRadioButton getRadioButton() {
        return radioButton;
    }

    /**
     * Sets the value of the radioButton property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewRadioButton }
     *     
     */
    public void setRadioButton(MVCViewRadioButton value) {
        this.radioButton = value;
    }

    /**
     * Gets the value of the radioButtonGroup property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewRadioButtonGroup }
     *     
     */
    public MVCViewRadioButtonGroup getRadioButtonGroup() {
        return radioButtonGroup;
    }

    /**
     * Sets the value of the radioButtonGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewRadioButtonGroup }
     *     
     */
    public void setRadioButtonGroup(MVCViewRadioButtonGroup value) {
        this.radioButtonGroup = value;
    }

    /**
     * Gets the value of the dropDown property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewDropDown }
     *     
     */
    public MVCViewDropDown getDropDown() {
        return dropDown;
    }

    /**
     * Sets the value of the dropDown property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewDropDown }
     *     
     */
    public void setDropDown(MVCViewDropDown value) {
        this.dropDown = value;
    }

    /**
     * Gets the value of the link property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewLink }
     *     
     */
    public MVCViewLink getLink() {
        return link;
    }

    /**
     * Sets the value of the link property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewLink }
     *     
     */
    public void setLink(MVCViewLink value) {
        this.link = value;
    }

    /**
     * Gets the value of the file property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewFile }
     *     
     */
    public MVCViewFile getFile() {
        return file;
    }

    /**
     * Sets the value of the file property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewFile }
     *     
     */
    public void setFile(MVCViewFile value) {
        this.file = value;
    }

    /**
     * Gets the value of the hidden property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewHidden }
     *     
     */
    public MVCViewHidden getHidden() {
        return hidden;
    }

    /**
     * Sets the value of the hidden property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewHidden }
     *     
     */
    public void setHidden(MVCViewHidden value) {
        this.hidden = value;
    }

    /**
     * Gets the value of the image property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewImage }
     *     
     */
    public MVCViewImage getImage() {
        return image;
    }

    /**
     * Sets the value of the image property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewImage }
     *     
     */
    public void setImage(MVCViewImage value) {
        this.image = value;
    }

    /**
     * Gets the value of the object property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewObject }
     *     
     */
    public MVCViewObject getObject() {
        return object;
    }

    /**
     * Sets the value of the object property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewObject }
     *     
     */
    public void setObject(MVCViewObject value) {
        this.object = value;
    }

    /**
     * Gets the value of the div property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewDiv }
     *     
     */
    public MVCViewDiv getDiv() {
        return div;
    }

    /**
     * Sets the value of the div property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewDiv }
     *     
     */
    public void setDiv(MVCViewDiv value) {
        this.div = value;
    }

    /**
     * Gets the value of the table property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewTable }
     *     
     */
    public MVCViewTable getTable() {
        return table;
    }

    /**
     * Sets the value of the table property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewTable }
     *     
     */
    public void setTable(MVCViewTable value) {
        this.table = value;
    }

    /**
     * Gets the value of the row property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewTableRow }
     *     
     */
    public MVCViewTableRow getRow() {
        return row;
    }

    /**
     * Sets the value of the row property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewTableRow }
     *     
     */
    public void setRow(MVCViewTableRow value) {
        this.row = value;
    }

    /**
     * Gets the value of the cell property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewTableCell }
     *     
     */
    public MVCViewTableCell getCell() {
        return cell;
    }

    /**
     * Sets the value of the cell property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewTableCell }
     *     
     */
    public void setCell(MVCViewTableCell value) {
        this.cell = value;
    }

    /**
     * Gets the value of the button property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewButton }
     *     
     */
    public MVCViewButton getButton() {
        return button;
    }

    /**
     * Sets the value of the button property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewButton }
     *     
     */
    public void setButton(MVCViewButton value) {
        this.button = value;
    }

    /**
     * Gets the value of the fieldSet property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewFieldSet }
     *     
     */
    public MVCViewFieldSet getFieldSet() {
        return fieldSet;
    }

    /**
     * Sets the value of the fieldSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewFieldSet }
     *     
     */
    public void setFieldSet(MVCViewFieldSet value) {
        this.fieldSet = value;
    }

    /**
     * Gets the value of the literal property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewLiteral }
     *     
     */
    public MVCViewLiteral getLiteral() {
        return literal;
    }

    /**
     * Sets the value of the literal property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewLiteral }
     *     
     */
    public void setLiteral(MVCViewLiteral value) {
        this.literal = value;
    }

    /**
     * Gets the value of the script property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewScript }
     *     
     */
    public MVCViewScript getScript() {
        return script;
    }

    /**
     * Sets the value of the script property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewScript }
     *     
     */
    public void setScript(MVCViewScript value) {
        this.script = value;
    }

    /**
     * Gets the value of the breakLine property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewBreakLine }
     *     
     */
    public MVCViewBreakLine getBreakLine() {
        return breakLine;
    }

    /**
     * Sets the value of the breakLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewBreakLine }
     *     
     */
    public void setBreakLine(MVCViewBreakLine value) {
        this.breakLine = value;
    }

    /**
     * Gets the value of the htmlEditor property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewHtmlEditor }
     *     
     */
    public MVCViewHtmlEditor getHTMLEditor() {
        return htmlEditor;
    }

    /**
     * Sets the value of the htmlEditor property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewHtmlEditor }
     *     
     */
    public void setHTMLEditor(MVCViewHtmlEditor value) {
        this.htmlEditor = value;
    }

    /**
     * Gets the value of the treeView property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewTreeView }
     *     
     */
    public MVCViewTreeView getTreeView() {
        return treeView;
    }

    /**
     * Sets the value of the treeView property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewTreeView }
     *     
     */
    public void setTreeView(MVCViewTreeView value) {
        this.treeView = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewElementChoice)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final MVCViewElementChoice that = ((MVCViewElementChoice) object);
        {
            MVCViewElementReference lhsElementReference;
            lhsElementReference = this.getElementReference();
            MVCViewElementReference rhsElementReference;
            rhsElementReference = that.getElementReference();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "elementReference", lhsElementReference), LocatorUtils.property(thatLocator, "elementReference", rhsElementReference), lhsElementReference, rhsElementReference)) {
                return false;
            }
        }
        {
            MVCViewForm lhsForm;
            lhsForm = this.getForm();
            MVCViewForm rhsForm;
            rhsForm = that.getForm();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "form", lhsForm), LocatorUtils.property(thatLocator, "form", rhsForm), lhsForm, rhsForm)) {
                return false;
            }
        }
        {
            MVCViewLabel lhsLabel;
            lhsLabel = this.getLabel();
            MVCViewLabel rhsLabel;
            rhsLabel = that.getLabel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "label", lhsLabel), LocatorUtils.property(thatLocator, "label", rhsLabel), lhsLabel, rhsLabel)) {
                return false;
            }
        }
        {
            MVCViewSpan lhsSpan;
            lhsSpan = this.getSpan();
            MVCViewSpan rhsSpan;
            rhsSpan = that.getSpan();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "span", lhsSpan), LocatorUtils.property(thatLocator, "span", rhsSpan), lhsSpan, rhsSpan)) {
                return false;
            }
        }
        {
            MVCViewH1 lhsH1;
            lhsH1 = this.getH1();
            MVCViewH1 rhsH1;
            rhsH1 = that.getH1();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "h1", lhsH1), LocatorUtils.property(thatLocator, "h1", rhsH1), lhsH1, rhsH1)) {
                return false;
            }
        }
        {
            MVCViewH2 lhsH2;
            lhsH2 = this.getH2();
            MVCViewH2 rhsH2;
            rhsH2 = that.getH2();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "h2", lhsH2), LocatorUtils.property(thatLocator, "h2", rhsH2), lhsH2, rhsH2)) {
                return false;
            }
        }
        {
            MVCViewH3 lhsH3;
            lhsH3 = this.getH3();
            MVCViewH3 rhsH3;
            rhsH3 = that.getH3();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "h3", lhsH3), LocatorUtils.property(thatLocator, "h3", rhsH3), lhsH3, rhsH3)) {
                return false;
            }
        }
        {
            MVCViewH4 lhsH4;
            lhsH4 = this.getH4();
            MVCViewH4 rhsH4;
            rhsH4 = that.getH4();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "h4", lhsH4), LocatorUtils.property(thatLocator, "h4", rhsH4), lhsH4, rhsH4)) {
                return false;
            }
        }
        {
            MVCViewH5 lhsH5;
            lhsH5 = this.getH5();
            MVCViewH5 rhsH5;
            rhsH5 = that.getH5();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "h5", lhsH5), LocatorUtils.property(thatLocator, "h5", rhsH5), lhsH5, rhsH5)) {
                return false;
            }
        }
        {
            MVCViewH6 lhsH6;
            lhsH6 = this.getH6();
            MVCViewH6 rhsH6;
            rhsH6 = that.getH6();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "h6", lhsH6), LocatorUtils.property(thatLocator, "h6", rhsH6), lhsH6, rhsH6)) {
                return false;
            }
        }
        {
            MVCViewP lhsP;
            lhsP = this.getP();
            MVCViewP rhsP;
            rhsP = that.getP();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "p", lhsP), LocatorUtils.property(thatLocator, "p", rhsP), lhsP, rhsP)) {
                return false;
            }
        }
        {
            MVCViewAside lhsAside;
            lhsAside = this.getAside();
            MVCViewAside rhsAside;
            rhsAside = that.getAside();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "aside", lhsAside), LocatorUtils.property(thatLocator, "aside", rhsAside), lhsAside, rhsAside)) {
                return false;
            }
        }
        {
            MVCViewArticle lhsArticle;
            lhsArticle = this.getArticle();
            MVCViewArticle rhsArticle;
            rhsArticle = that.getArticle();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "article", lhsArticle), LocatorUtils.property(thatLocator, "article", rhsArticle), lhsArticle, rhsArticle)) {
                return false;
            }
        }
        {
            MVCViewHeader lhsHeader;
            lhsHeader = this.getHeader();
            MVCViewHeader rhsHeader;
            rhsHeader = that.getHeader();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "header", lhsHeader), LocatorUtils.property(thatLocator, "header", rhsHeader), lhsHeader, rhsHeader)) {
                return false;
            }
        }
        {
            MVCViewNav lhsNav;
            lhsNav = this.getNav();
            MVCViewNav rhsNav;
            rhsNav = that.getNav();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "nav", lhsNav), LocatorUtils.property(thatLocator, "nav", rhsNav), lhsNav, rhsNav)) {
                return false;
            }
        }
        {
            MVCViewTextBox lhsTextBox;
            lhsTextBox = this.getTextBox();
            MVCViewTextBox rhsTextBox;
            rhsTextBox = that.getTextBox();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "textBox", lhsTextBox), LocatorUtils.property(thatLocator, "textBox", rhsTextBox), lhsTextBox, rhsTextBox)) {
                return false;
            }
        }
        {
            MVCViewNumericBox lhsNumericBox;
            lhsNumericBox = this.getNumericBox();
            MVCViewNumericBox rhsNumericBox;
            rhsNumericBox = that.getNumericBox();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "numericBox", lhsNumericBox), LocatorUtils.property(thatLocator, "numericBox", rhsNumericBox), lhsNumericBox, rhsNumericBox)) {
                return false;
            }
        }
        {
            MVCViewLookup lhsLookup;
            lhsLookup = this.getLookup();
            MVCViewLookup rhsLookup;
            rhsLookup = that.getLookup();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "lookup", lhsLookup), LocatorUtils.property(thatLocator, "lookup", rhsLookup), lhsLookup, rhsLookup)) {
                return false;
            }
        }
        {
            MVCViewInclude lhsInclude;
            lhsInclude = this.getInclude();
            MVCViewInclude rhsInclude;
            rhsInclude = that.getInclude();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "include", lhsInclude), LocatorUtils.property(thatLocator, "include", rhsInclude), lhsInclude, rhsInclude)) {
                return false;
            }
        }
        {
            MVCViewRepeater lhsRepeater;
            lhsRepeater = this.getRepeater();
            MVCViewRepeater rhsRepeater;
            rhsRepeater = that.getRepeater();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "repeater", lhsRepeater), LocatorUtils.property(thatLocator, "repeater", rhsRepeater), lhsRepeater, rhsRepeater)) {
                return false;
            }
        }
        {
            MVCViewTextArea lhsTextArea;
            lhsTextArea = this.getTextArea();
            MVCViewTextArea rhsTextArea;
            rhsTextArea = that.getTextArea();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "textArea", lhsTextArea), LocatorUtils.property(thatLocator, "textArea", rhsTextArea), lhsTextArea, rhsTextArea)) {
                return false;
            }
        }
        {
            MVCViewDateBox lhsDateBox;
            lhsDateBox = this.getDateBox();
            MVCViewDateBox rhsDateBox;
            rhsDateBox = that.getDateBox();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dateBox", lhsDateBox), LocatorUtils.property(thatLocator, "dateBox", rhsDateBox), lhsDateBox, rhsDateBox)) {
                return false;
            }
        }
        {
            MVCViewDateTimeBox lhsDateTimeBox;
            lhsDateTimeBox = this.getDateTimeBox();
            MVCViewDateTimeBox rhsDateTimeBox;
            rhsDateTimeBox = that.getDateTimeBox();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dateTimeBox", lhsDateTimeBox), LocatorUtils.property(thatLocator, "dateTimeBox", rhsDateTimeBox), lhsDateTimeBox, rhsDateTimeBox)) {
                return false;
            }
        }
        {
            MVCViewCheckBox lhsCheckBox;
            lhsCheckBox = this.getCheckBox();
            MVCViewCheckBox rhsCheckBox;
            rhsCheckBox = that.getCheckBox();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "checkBox", lhsCheckBox), LocatorUtils.property(thatLocator, "checkBox", rhsCheckBox), lhsCheckBox, rhsCheckBox)) {
                return false;
            }
        }
        {
            MVCViewRadioButton lhsRadioButton;
            lhsRadioButton = this.getRadioButton();
            MVCViewRadioButton rhsRadioButton;
            rhsRadioButton = that.getRadioButton();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "radioButton", lhsRadioButton), LocatorUtils.property(thatLocator, "radioButton", rhsRadioButton), lhsRadioButton, rhsRadioButton)) {
                return false;
            }
        }
        {
            MVCViewRadioButtonGroup lhsRadioButtonGroup;
            lhsRadioButtonGroup = this.getRadioButtonGroup();
            MVCViewRadioButtonGroup rhsRadioButtonGroup;
            rhsRadioButtonGroup = that.getRadioButtonGroup();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "radioButtonGroup", lhsRadioButtonGroup), LocatorUtils.property(thatLocator, "radioButtonGroup", rhsRadioButtonGroup), lhsRadioButtonGroup, rhsRadioButtonGroup)) {
                return false;
            }
        }
        {
            MVCViewDropDown lhsDropDown;
            lhsDropDown = this.getDropDown();
            MVCViewDropDown rhsDropDown;
            rhsDropDown = that.getDropDown();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dropDown", lhsDropDown), LocatorUtils.property(thatLocator, "dropDown", rhsDropDown), lhsDropDown, rhsDropDown)) {
                return false;
            }
        }
        {
            MVCViewLink lhsLink;
            lhsLink = this.getLink();
            MVCViewLink rhsLink;
            rhsLink = that.getLink();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "link", lhsLink), LocatorUtils.property(thatLocator, "link", rhsLink), lhsLink, rhsLink)) {
                return false;
            }
        }
        {
            MVCViewFile lhsFile;
            lhsFile = this.getFile();
            MVCViewFile rhsFile;
            rhsFile = that.getFile();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "file", lhsFile), LocatorUtils.property(thatLocator, "file", rhsFile), lhsFile, rhsFile)) {
                return false;
            }
        }
        {
            MVCViewHidden lhsHidden;
            lhsHidden = this.getHidden();
            MVCViewHidden rhsHidden;
            rhsHidden = that.getHidden();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "hidden", lhsHidden), LocatorUtils.property(thatLocator, "hidden", rhsHidden), lhsHidden, rhsHidden)) {
                return false;
            }
        }
        {
            MVCViewImage lhsImage;
            lhsImage = this.getImage();
            MVCViewImage rhsImage;
            rhsImage = that.getImage();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "image", lhsImage), LocatorUtils.property(thatLocator, "image", rhsImage), lhsImage, rhsImage)) {
                return false;
            }
        }
        {
            MVCViewObject lhsObject;
            lhsObject = this.getObject();
            MVCViewObject rhsObject;
            rhsObject = that.getObject();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "object", lhsObject), LocatorUtils.property(thatLocator, "object", rhsObject), lhsObject, rhsObject)) {
                return false;
            }
        }
        {
            MVCViewDiv lhsDiv;
            lhsDiv = this.getDiv();
            MVCViewDiv rhsDiv;
            rhsDiv = that.getDiv();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "div", lhsDiv), LocatorUtils.property(thatLocator, "div", rhsDiv), lhsDiv, rhsDiv)) {
                return false;
            }
        }
        {
            MVCViewTable lhsTable;
            lhsTable = this.getTable();
            MVCViewTable rhsTable;
            rhsTable = that.getTable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "table", lhsTable), LocatorUtils.property(thatLocator, "table", rhsTable), lhsTable, rhsTable)) {
                return false;
            }
        }
        {
            MVCViewTableRow lhsRow;
            lhsRow = this.getRow();
            MVCViewTableRow rhsRow;
            rhsRow = that.getRow();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "row", lhsRow), LocatorUtils.property(thatLocator, "row", rhsRow), lhsRow, rhsRow)) {
                return false;
            }
        }
        {
            MVCViewTableCell lhsCell;
            lhsCell = this.getCell();
            MVCViewTableCell rhsCell;
            rhsCell = that.getCell();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "cell", lhsCell), LocatorUtils.property(thatLocator, "cell", rhsCell), lhsCell, rhsCell)) {
                return false;
            }
        }
        {
            MVCViewButton lhsButton;
            lhsButton = this.getButton();
            MVCViewButton rhsButton;
            rhsButton = that.getButton();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "button", lhsButton), LocatorUtils.property(thatLocator, "button", rhsButton), lhsButton, rhsButton)) {
                return false;
            }
        }
        {
            MVCViewFieldSet lhsFieldSet;
            lhsFieldSet = this.getFieldSet();
            MVCViewFieldSet rhsFieldSet;
            rhsFieldSet = that.getFieldSet();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "fieldSet", lhsFieldSet), LocatorUtils.property(thatLocator, "fieldSet", rhsFieldSet), lhsFieldSet, rhsFieldSet)) {
                return false;
            }
        }
        {
            MVCViewLiteral lhsLiteral;
            lhsLiteral = this.getLiteral();
            MVCViewLiteral rhsLiteral;
            rhsLiteral = that.getLiteral();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "literal", lhsLiteral), LocatorUtils.property(thatLocator, "literal", rhsLiteral), lhsLiteral, rhsLiteral)) {
                return false;
            }
        }
        {
            MVCViewScript lhsScript;
            lhsScript = this.getScript();
            MVCViewScript rhsScript;
            rhsScript = that.getScript();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "script", lhsScript), LocatorUtils.property(thatLocator, "script", rhsScript), lhsScript, rhsScript)) {
                return false;
            }
        }
        {
            MVCViewBreakLine lhsBreakLine;
            lhsBreakLine = this.getBreakLine();
            MVCViewBreakLine rhsBreakLine;
            rhsBreakLine = that.getBreakLine();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "breakLine", lhsBreakLine), LocatorUtils.property(thatLocator, "breakLine", rhsBreakLine), lhsBreakLine, rhsBreakLine)) {
                return false;
            }
        }
        {
            MVCViewHtmlEditor lhsHTMLEditor;
            lhsHTMLEditor = this.getHTMLEditor();
            MVCViewHtmlEditor rhsHTMLEditor;
            rhsHTMLEditor = that.getHTMLEditor();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "htmlEditor", lhsHTMLEditor), LocatorUtils.property(thatLocator, "htmlEditor", rhsHTMLEditor), lhsHTMLEditor, rhsHTMLEditor)) {
                return false;
            }
        }
        {
            MVCViewTreeView lhsTreeView;
            lhsTreeView = this.getTreeView();
            MVCViewTreeView rhsTreeView;
            rhsTreeView = that.getTreeView();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "treeView", lhsTreeView), LocatorUtils.property(thatLocator, "treeView", rhsTreeView), lhsTreeView, rhsTreeView)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof MVCViewElementChoice) {
            final MVCViewElementChoice copy = ((MVCViewElementChoice) draftCopy);
            if (this.elementReference!= null) {
                MVCViewElementReference sourceElementReference;
                sourceElementReference = this.getElementReference();
                MVCViewElementReference copyElementReference = ((MVCViewElementReference) strategy.copy(LocatorUtils.property(locator, "elementReference", sourceElementReference), sourceElementReference));
                copy.setElementReference(copyElementReference);
            } else {
                copy.elementReference = null;
            }
            if (this.form!= null) {
                MVCViewForm sourceForm;
                sourceForm = this.getForm();
                MVCViewForm copyForm = ((MVCViewForm) strategy.copy(LocatorUtils.property(locator, "form", sourceForm), sourceForm));
                copy.setForm(copyForm);
            } else {
                copy.form = null;
            }
            if (this.label!= null) {
                MVCViewLabel sourceLabel;
                sourceLabel = this.getLabel();
                MVCViewLabel copyLabel = ((MVCViewLabel) strategy.copy(LocatorUtils.property(locator, "label", sourceLabel), sourceLabel));
                copy.setLabel(copyLabel);
            } else {
                copy.label = null;
            }
            if (this.span!= null) {
                MVCViewSpan sourceSpan;
                sourceSpan = this.getSpan();
                MVCViewSpan copySpan = ((MVCViewSpan) strategy.copy(LocatorUtils.property(locator, "span", sourceSpan), sourceSpan));
                copy.setSpan(copySpan);
            } else {
                copy.span = null;
            }
            if (this.h1 != null) {
                MVCViewH1 sourceH1;
                sourceH1 = this.getH1();
                MVCViewH1 copyH1 = ((MVCViewH1) strategy.copy(LocatorUtils.property(locator, "h1", sourceH1), sourceH1));
                copy.setH1(copyH1);
            } else {
                copy.h1 = null;
            }
            if (this.h2 != null) {
                MVCViewH2 sourceH2;
                sourceH2 = this.getH2();
                MVCViewH2 copyH2 = ((MVCViewH2) strategy.copy(LocatorUtils.property(locator, "h2", sourceH2), sourceH2));
                copy.setH2(copyH2);
            } else {
                copy.h2 = null;
            }
            if (this.h3 != null) {
                MVCViewH3 sourceH3;
                sourceH3 = this.getH3();
                MVCViewH3 copyH3 = ((MVCViewH3) strategy.copy(LocatorUtils.property(locator, "h3", sourceH3), sourceH3));
                copy.setH3(copyH3);
            } else {
                copy.h3 = null;
            }
            if (this.h4 != null) {
                MVCViewH4 sourceH4;
                sourceH4 = this.getH4();
                MVCViewH4 copyH4 = ((MVCViewH4) strategy.copy(LocatorUtils.property(locator, "h4", sourceH4), sourceH4));
                copy.setH4(copyH4);
            } else {
                copy.h4 = null;
            }
            if (this.h5 != null) {
                MVCViewH5 sourceH5;
                sourceH5 = this.getH5();
                MVCViewH5 copyH5 = ((MVCViewH5) strategy.copy(LocatorUtils.property(locator, "h5", sourceH5), sourceH5));
                copy.setH5(copyH5);
            } else {
                copy.h5 = null;
            }
            if (this.h6 != null) {
                MVCViewH6 sourceH6;
                sourceH6 = this.getH6();
                MVCViewH6 copyH6 = ((MVCViewH6) strategy.copy(LocatorUtils.property(locator, "h6", sourceH6), sourceH6));
                copy.setH6(copyH6);
            } else {
                copy.h6 = null;
            }
            if (this.p!= null) {
                MVCViewP sourceP;
                sourceP = this.getP();
                MVCViewP copyP = ((MVCViewP) strategy.copy(LocatorUtils.property(locator, "p", sourceP), sourceP));
                copy.setP(copyP);
            } else {
                copy.p = null;
            }
            if (this.aside!= null) {
                MVCViewAside sourceAside;
                sourceAside = this.getAside();
                MVCViewAside copyAside = ((MVCViewAside) strategy.copy(LocatorUtils.property(locator, "aside", sourceAside), sourceAside));
                copy.setAside(copyAside);
            } else {
                copy.aside = null;
            }
            if (this.article!= null) {
                MVCViewArticle sourceArticle;
                sourceArticle = this.getArticle();
                MVCViewArticle copyArticle = ((MVCViewArticle) strategy.copy(LocatorUtils.property(locator, "article", sourceArticle), sourceArticle));
                copy.setArticle(copyArticle);
            } else {
                copy.article = null;
            }
            if (this.header!= null) {
                MVCViewHeader sourceHeader;
                sourceHeader = this.getHeader();
                MVCViewHeader copyHeader = ((MVCViewHeader) strategy.copy(LocatorUtils.property(locator, "header", sourceHeader), sourceHeader));
                copy.setHeader(copyHeader);
            } else {
                copy.header = null;
            }
            if (this.nav!= null) {
                MVCViewNav sourceNav;
                sourceNav = this.getNav();
                MVCViewNav copyNav = ((MVCViewNav) strategy.copy(LocatorUtils.property(locator, "nav", sourceNav), sourceNav));
                copy.setNav(copyNav);
            } else {
                copy.nav = null;
            }
            if (this.textBox!= null) {
                MVCViewTextBox sourceTextBox;
                sourceTextBox = this.getTextBox();
                MVCViewTextBox copyTextBox = ((MVCViewTextBox) strategy.copy(LocatorUtils.property(locator, "textBox", sourceTextBox), sourceTextBox));
                copy.setTextBox(copyTextBox);
            } else {
                copy.textBox = null;
            }
            if (this.numericBox!= null) {
                MVCViewNumericBox sourceNumericBox;
                sourceNumericBox = this.getNumericBox();
                MVCViewNumericBox copyNumericBox = ((MVCViewNumericBox) strategy.copy(LocatorUtils.property(locator, "numericBox", sourceNumericBox), sourceNumericBox));
                copy.setNumericBox(copyNumericBox);
            } else {
                copy.numericBox = null;
            }
            if (this.lookup!= null) {
                MVCViewLookup sourceLookup;
                sourceLookup = this.getLookup();
                MVCViewLookup copyLookup = ((MVCViewLookup) strategy.copy(LocatorUtils.property(locator, "lookup", sourceLookup), sourceLookup));
                copy.setLookup(copyLookup);
            } else {
                copy.lookup = null;
            }
            if (this.include!= null) {
                MVCViewInclude sourceInclude;
                sourceInclude = this.getInclude();
                MVCViewInclude copyInclude = ((MVCViewInclude) strategy.copy(LocatorUtils.property(locator, "include", sourceInclude), sourceInclude));
                copy.setInclude(copyInclude);
            } else {
                copy.include = null;
            }
            if (this.repeater!= null) {
                MVCViewRepeater sourceRepeater;
                sourceRepeater = this.getRepeater();
                MVCViewRepeater copyRepeater = ((MVCViewRepeater) strategy.copy(LocatorUtils.property(locator, "repeater", sourceRepeater), sourceRepeater));
                copy.setRepeater(copyRepeater);
            } else {
                copy.repeater = null;
            }
            if (this.textArea!= null) {
                MVCViewTextArea sourceTextArea;
                sourceTextArea = this.getTextArea();
                MVCViewTextArea copyTextArea = ((MVCViewTextArea) strategy.copy(LocatorUtils.property(locator, "textArea", sourceTextArea), sourceTextArea));
                copy.setTextArea(copyTextArea);
            } else {
                copy.textArea = null;
            }
            if (this.dateBox!= null) {
                MVCViewDateBox sourceDateBox;
                sourceDateBox = this.getDateBox();
                MVCViewDateBox copyDateBox = ((MVCViewDateBox) strategy.copy(LocatorUtils.property(locator, "dateBox", sourceDateBox), sourceDateBox));
                copy.setDateBox(copyDateBox);
            } else {
                copy.dateBox = null;
            }
            if (this.dateTimeBox!= null) {
                MVCViewDateTimeBox sourceDateTimeBox;
                sourceDateTimeBox = this.getDateTimeBox();
                MVCViewDateTimeBox copyDateTimeBox = ((MVCViewDateTimeBox) strategy.copy(LocatorUtils.property(locator, "dateTimeBox", sourceDateTimeBox), sourceDateTimeBox));
                copy.setDateTimeBox(copyDateTimeBox);
            } else {
                copy.dateTimeBox = null;
            }
            if (this.checkBox!= null) {
                MVCViewCheckBox sourceCheckBox;
                sourceCheckBox = this.getCheckBox();
                MVCViewCheckBox copyCheckBox = ((MVCViewCheckBox) strategy.copy(LocatorUtils.property(locator, "checkBox", sourceCheckBox), sourceCheckBox));
                copy.setCheckBox(copyCheckBox);
            } else {
                copy.checkBox = null;
            }
            if (this.radioButton!= null) {
                MVCViewRadioButton sourceRadioButton;
                sourceRadioButton = this.getRadioButton();
                MVCViewRadioButton copyRadioButton = ((MVCViewRadioButton) strategy.copy(LocatorUtils.property(locator, "radioButton", sourceRadioButton), sourceRadioButton));
                copy.setRadioButton(copyRadioButton);
            } else {
                copy.radioButton = null;
            }
            if (this.radioButtonGroup!= null) {
                MVCViewRadioButtonGroup sourceRadioButtonGroup;
                sourceRadioButtonGroup = this.getRadioButtonGroup();
                MVCViewRadioButtonGroup copyRadioButtonGroup = ((MVCViewRadioButtonGroup) strategy.copy(LocatorUtils.property(locator, "radioButtonGroup", sourceRadioButtonGroup), sourceRadioButtonGroup));
                copy.setRadioButtonGroup(copyRadioButtonGroup);
            } else {
                copy.radioButtonGroup = null;
            }
            if (this.dropDown!= null) {
                MVCViewDropDown sourceDropDown;
                sourceDropDown = this.getDropDown();
                MVCViewDropDown copyDropDown = ((MVCViewDropDown) strategy.copy(LocatorUtils.property(locator, "dropDown", sourceDropDown), sourceDropDown));
                copy.setDropDown(copyDropDown);
            } else {
                copy.dropDown = null;
            }
            if (this.link!= null) {
                MVCViewLink sourceLink;
                sourceLink = this.getLink();
                MVCViewLink copyLink = ((MVCViewLink) strategy.copy(LocatorUtils.property(locator, "link", sourceLink), sourceLink));
                copy.setLink(copyLink);
            } else {
                copy.link = null;
            }
            if (this.file!= null) {
                MVCViewFile sourceFile;
                sourceFile = this.getFile();
                MVCViewFile copyFile = ((MVCViewFile) strategy.copy(LocatorUtils.property(locator, "file", sourceFile), sourceFile));
                copy.setFile(copyFile);
            } else {
                copy.file = null;
            }
            if (this.hidden!= null) {
                MVCViewHidden sourceHidden;
                sourceHidden = this.getHidden();
                MVCViewHidden copyHidden = ((MVCViewHidden) strategy.copy(LocatorUtils.property(locator, "hidden", sourceHidden), sourceHidden));
                copy.setHidden(copyHidden);
            } else {
                copy.hidden = null;
            }
            if (this.image!= null) {
                MVCViewImage sourceImage;
                sourceImage = this.getImage();
                MVCViewImage copyImage = ((MVCViewImage) strategy.copy(LocatorUtils.property(locator, "image", sourceImage), sourceImage));
                copy.setImage(copyImage);
            } else {
                copy.image = null;
            }
            if (this.object!= null) {
                MVCViewObject sourceObject;
                sourceObject = this.getObject();
                MVCViewObject copyObject = ((MVCViewObject) strategy.copy(LocatorUtils.property(locator, "object", sourceObject), sourceObject));
                copy.setObject(copyObject);
            } else {
                copy.object = null;
            }
            if (this.div!= null) {
                MVCViewDiv sourceDiv;
                sourceDiv = this.getDiv();
                MVCViewDiv copyDiv = ((MVCViewDiv) strategy.copy(LocatorUtils.property(locator, "div", sourceDiv), sourceDiv));
                copy.setDiv(copyDiv);
            } else {
                copy.div = null;
            }
            if (this.table!= null) {
                MVCViewTable sourceTable;
                sourceTable = this.getTable();
                MVCViewTable copyTable = ((MVCViewTable) strategy.copy(LocatorUtils.property(locator, "table", sourceTable), sourceTable));
                copy.setTable(copyTable);
            } else {
                copy.table = null;
            }
            if (this.row!= null) {
                MVCViewTableRow sourceRow;
                sourceRow = this.getRow();
                MVCViewTableRow copyRow = ((MVCViewTableRow) strategy.copy(LocatorUtils.property(locator, "row", sourceRow), sourceRow));
                copy.setRow(copyRow);
            } else {
                copy.row = null;
            }
            if (this.cell!= null) {
                MVCViewTableCell sourceCell;
                sourceCell = this.getCell();
                MVCViewTableCell copyCell = ((MVCViewTableCell) strategy.copy(LocatorUtils.property(locator, "cell", sourceCell), sourceCell));
                copy.setCell(copyCell);
            } else {
                copy.cell = null;
            }
            if (this.button!= null) {
                MVCViewButton sourceButton;
                sourceButton = this.getButton();
                MVCViewButton copyButton = ((MVCViewButton) strategy.copy(LocatorUtils.property(locator, "button", sourceButton), sourceButton));
                copy.setButton(copyButton);
            } else {
                copy.button = null;
            }
            if (this.fieldSet!= null) {
                MVCViewFieldSet sourceFieldSet;
                sourceFieldSet = this.getFieldSet();
                MVCViewFieldSet copyFieldSet = ((MVCViewFieldSet) strategy.copy(LocatorUtils.property(locator, "fieldSet", sourceFieldSet), sourceFieldSet));
                copy.setFieldSet(copyFieldSet);
            } else {
                copy.fieldSet = null;
            }
            if (this.literal!= null) {
                MVCViewLiteral sourceLiteral;
                sourceLiteral = this.getLiteral();
                MVCViewLiteral copyLiteral = ((MVCViewLiteral) strategy.copy(LocatorUtils.property(locator, "literal", sourceLiteral), sourceLiteral));
                copy.setLiteral(copyLiteral);
            } else {
                copy.literal = null;
            }
            if (this.script!= null) {
                MVCViewScript sourceScript;
                sourceScript = this.getScript();
                MVCViewScript copyScript = ((MVCViewScript) strategy.copy(LocatorUtils.property(locator, "script", sourceScript), sourceScript));
                copy.setScript(copyScript);
            } else {
                copy.script = null;
            }
            if (this.breakLine!= null) {
                MVCViewBreakLine sourceBreakLine;
                sourceBreakLine = this.getBreakLine();
                MVCViewBreakLine copyBreakLine = ((MVCViewBreakLine) strategy.copy(LocatorUtils.property(locator, "breakLine", sourceBreakLine), sourceBreakLine));
                copy.setBreakLine(copyBreakLine);
            } else {
                copy.breakLine = null;
            }
            if (this.htmlEditor!= null) {
                MVCViewHtmlEditor sourceHTMLEditor;
                sourceHTMLEditor = this.getHTMLEditor();
                MVCViewHtmlEditor copyHTMLEditor = ((MVCViewHtmlEditor) strategy.copy(LocatorUtils.property(locator, "htmlEditor", sourceHTMLEditor), sourceHTMLEditor));
                copy.setHTMLEditor(copyHTMLEditor);
            } else {
                copy.htmlEditor = null;
            }
            if (this.treeView!= null) {
                MVCViewTreeView sourceTreeView;
                sourceTreeView = this.getTreeView();
                MVCViewTreeView copyTreeView = ((MVCViewTreeView) strategy.copy(LocatorUtils.property(locator, "treeView", sourceTreeView), sourceTreeView));
                copy.setTreeView(copyTreeView);
            } else {
                copy.treeView = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewElementChoice();
    }
    
//--simple--preserve

    public void writeHtml(com.mda.engine.utils.Stringcode c)
    {
    	if (this.elementReference != null)
    		this.elementReference.writeHtml(c);
    	else if (getLabel() != null)
    		this.getLabel().writeHtml(c);
    		
    }
//--simple--preserve

}
