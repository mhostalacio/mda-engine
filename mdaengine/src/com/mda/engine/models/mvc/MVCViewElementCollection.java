
package com.mda.engine.models.mvc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewElementCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewElementCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded" minOccurs="0">
 *         &lt;element name="ElementReference" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewElementReference"/>
 *         &lt;element name="ContentPlaceHolder" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewContentPlaceHolder"/>
 *         &lt;element name="Form" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewForm"/>
 *         &lt;element name="Label" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewLabel"/>
 *         &lt;element name="Span" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewSpan"/>
 *         &lt;element name="H1" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewH1"/>
 *         &lt;element name="H2" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewH2"/>
 *         &lt;element name="H3" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewH3"/>
 *         &lt;element name="H4" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewH4"/>
 *         &lt;element name="H5" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewH5"/>
 *         &lt;element name="H6" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewH6"/>
 *         &lt;element name="P" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewP"/>
 *         &lt;element name="Article" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewArticle"/>
 *         &lt;element name="Aside" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewAside"/>
 *         &lt;element name="Header" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewHeader"/>
 *         &lt;element name="Nav" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewNav"/>
 *         &lt;element name="TextBox" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewTextBox"/>
 *         &lt;element name="NumericBox" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewNumericBox"/>
 *         &lt;element name="Lookup" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewLookup"/>
 *         &lt;element name="Include" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewInclude"/>
 *         &lt;element name="Repeater" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewRepeater"/>
 *         &lt;element name="TextArea" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewTextArea"/>
 *         &lt;element name="DateBox" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewDateBox"/>
 *         &lt;element name="DateTimeBox" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewDateTimeBox"/>
 *         &lt;element name="CheckBox" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewCheckBox"/>
 *         &lt;element name="RadioButton" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewRadioButton"/>
 *         &lt;element name="RadioButtonGroup" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewRadioButtonGroup"/>
 *         &lt;element name="DropDown" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewDropDown"/>
 *         &lt;element name="Link" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewLink"/>
 *         &lt;element name="File" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewFile"/>
 *         &lt;element name="Hidden" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewHidden"/>
 *         &lt;element name="Image" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewImage"/>
 *         &lt;element name="Object" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewObject"/>
 *         &lt;element name="Div" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewDiv"/>
 *         &lt;element name="Table" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewTable"/>
 *         &lt;element name="Row" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewTableRow"/>
 *         &lt;element name="Cell" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewTableCell"/>
 *         &lt;element name="Button" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewButton"/>
 *         &lt;element name="FieldSet" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewFieldSet"/>
 *         &lt;element name="Literal" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewLiteral"/>
 *         &lt;element name="Script" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewScript"/>
 *         &lt;element name="BreakLine" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewBreakLine"/>
 *         &lt;element name="UL" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewUL"/>
 *         &lt;element name="LI" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewLI"/>
 *         &lt;element name="Paginator" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewPaginator"/>
 *         &lt;element name="HTMLEditor" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewHtmlEditor"/>
 *         &lt;element name="TreeView" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewTreeView"/>
 *         &lt;element name="Timeline" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewTimeline"/>
 *         &lt;element name="Carousel" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewCarousel"/>
 *         &lt;element name="Calendar" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewCalendar"/>
 *         &lt;element name="HR" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewHR"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewElementCollection", propOrder = {
    "elementList"
})
@XmlSeeAlso({
    MVCViewRepeaterTemplate.class
})
public class MVCViewElementCollection
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElements({
        @XmlElement(name = "Table", type = MVCViewTable.class),
        @XmlElement(name = "FieldSet", type = MVCViewFieldSet.class),
        @XmlElement(name = "Script", type = MVCViewScript.class),
        @XmlElement(name = "Repeater", type = MVCViewRepeater.class),
        @XmlElement(name = "RadioButton", type = MVCViewRadioButton.class),
        @XmlElement(name = "DateBox", type = MVCViewDateBox.class),
        @XmlElement(name = "HTMLEditor", type = MVCViewHtmlEditor.class),
        @XmlElement(name = "Aside", type = MVCViewAside.class),
        @XmlElement(name = "Label", type = MVCViewLabel.class),
        @XmlElement(name = "Literal", type = MVCViewLiteral.class),
        @XmlElement(name = "Hidden", type = MVCViewHidden.class),
        @XmlElement(name = "H2", type = MVCViewH2 .class),
        @XmlElement(name = "Lookup", type = MVCViewLookup.class),
        @XmlElement(name = "Form", type = MVCViewForm.class),
        @XmlElement(name = "RadioButtonGroup", type = MVCViewRadioButtonGroup.class),
        @XmlElement(name = "Button", type = MVCViewButton.class),
        @XmlElement(name = "TextArea", type = MVCViewTextArea.class),
        @XmlElement(name = "ContentPlaceHolder", type = MVCViewContentPlaceHolder.class),
        @XmlElement(name = "Div", type = MVCViewDiv.class),
        @XmlElement(name = "Calendar", type = MVCViewCalendar.class),
        @XmlElement(name = "UL", type = MVCViewUL.class),
        @XmlElement(name = "Header", type = MVCViewHeader.class),
        @XmlElement(name = "P", type = MVCViewP.class),
        @XmlElement(name = "BreakLine", type = MVCViewBreakLine.class),
        @XmlElement(name = "LI", type = MVCViewLI.class),
        @XmlElement(name = "Article", type = MVCViewArticle.class),
        @XmlElement(name = "Row", type = MVCViewTableRow.class),
        @XmlElement(name = "Cell", type = MVCViewTableCell.class),
        @XmlElement(name = "Timeline", type = MVCViewTimeline.class),
        @XmlElement(name = "H5", type = MVCViewH5 .class),
        @XmlElement(name = "Nav", type = MVCViewNav.class),
        @XmlElement(name = "Span", type = MVCViewSpan.class),
        @XmlElement(name = "HR", type = MVCViewHR.class),
        @XmlElement(name = "Object", type = MVCViewObject.class),
        @XmlElement(name = "H3", type = MVCViewH3 .class),
        @XmlElement(name = "DateTimeBox", type = MVCViewDateTimeBox.class),
        @XmlElement(name = "H1", type = MVCViewH1 .class),
        @XmlElement(name = "H6", type = MVCViewH6 .class),
        @XmlElement(name = "TextBox", type = MVCViewTextBox.class),
        @XmlElement(name = "Link", type = MVCViewLink.class),
        @XmlElement(name = "Carousel", type = MVCViewCarousel.class),
        @XmlElement(name = "File", type = MVCViewFile.class),
        @XmlElement(name = "CheckBox", type = MVCViewCheckBox.class),
        @XmlElement(name = "Paginator", type = MVCViewPaginator.class),
        @XmlElement(name = "NumericBox", type = MVCViewNumericBox.class),
        @XmlElement(name = "Include", type = MVCViewInclude.class),
        @XmlElement(name = "H4", type = MVCViewH4 .class),
        @XmlElement(name = "DropDown", type = MVCViewDropDown.class),
        @XmlElement(name = "ElementReference", type = MVCViewElementReference.class),
        @XmlElement(name = "TreeView", type = MVCViewTreeView.class),
        @XmlElement(name = "Image", type = MVCViewImage.class)
    })
    protected List<MVCViewElement> elementList;

    /**
     * Gets the value of the elementList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elementList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getElementList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MVCViewTable }
     * {@link MVCViewFieldSet }
     * {@link MVCViewScript }
     * {@link MVCViewRepeater }
     * {@link MVCViewRadioButton }
     * {@link MVCViewDateBox }
     * {@link MVCViewHtmlEditor }
     * {@link MVCViewAside }
     * {@link MVCViewLabel }
     * {@link MVCViewLiteral }
     * {@link MVCViewHidden }
     * {@link MVCViewH2 }
     * {@link MVCViewLookup }
     * {@link MVCViewForm }
     * {@link MVCViewRadioButtonGroup }
     * {@link MVCViewButton }
     * {@link MVCViewTextArea }
     * {@link MVCViewContentPlaceHolder }
     * {@link MVCViewDiv }
     * {@link MVCViewCalendar }
     * {@link MVCViewUL }
     * {@link MVCViewHeader }
     * {@link MVCViewP }
     * {@link MVCViewBreakLine }
     * {@link MVCViewLI }
     * {@link MVCViewArticle }
     * {@link MVCViewTableRow }
     * {@link MVCViewTableCell }
     * {@link MVCViewTimeline }
     * {@link MVCViewH5 }
     * {@link MVCViewNav }
     * {@link MVCViewSpan }
     * {@link MVCViewHR }
     * {@link MVCViewObject }
     * {@link MVCViewH3 }
     * {@link MVCViewDateTimeBox }
     * {@link MVCViewH1 }
     * {@link MVCViewH6 }
     * {@link MVCViewTextBox }
     * {@link MVCViewLink }
     * {@link MVCViewCarousel }
     * {@link MVCViewFile }
     * {@link MVCViewCheckBox }
     * {@link MVCViewPaginator }
     * {@link MVCViewNumericBox }
     * {@link MVCViewInclude }
     * {@link MVCViewH4 }
     * {@link MVCViewDropDown }
     * {@link MVCViewElementReference }
     * {@link MVCViewTreeView }
     * {@link MVCViewImage }
     * 
     * 
     */
    public List<MVCViewElement> getElementList() {
        if (elementList == null) {
            elementList = new ArrayList<MVCViewElement>();
        }
        return this.elementList;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewElementCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final MVCViewElementCollection that = ((MVCViewElementCollection) object);
        {
            List<MVCViewElement> lhsElementList;
            lhsElementList = this.getElementList();
            List<MVCViewElement> rhsElementList;
            rhsElementList = that.getElementList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "elementList", lhsElementList), LocatorUtils.property(thatLocator, "elementList", rhsElementList), lhsElementList, rhsElementList)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof MVCViewElementCollection) {
            final MVCViewElementCollection copy = ((MVCViewElementCollection) draftCopy);
            if ((this.elementList!= null)&&(!this.elementList.isEmpty())) {
                List<MVCViewElement> sourceElementList;
                sourceElementList = this.getElementList();
                @SuppressWarnings("unchecked")
                List<MVCViewElement> copyElementList = ((List<MVCViewElement> ) strategy.copy(LocatorUtils.property(locator, "elementList", sourceElementList), sourceElementList));
                copy.elementList = null;
                List<MVCViewElement> uniqueElementListl = copy.getElementList();
                uniqueElementListl.addAll(copyElementList);
            } else {
                copy.elementList = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewElementCollection();
    }
    
//--simple--preserve
    
    public MVCViewElement getElementById(String id) {
    	for(MVCViewElement element : getElementList())
		{
    		if (element.getId() != null && element.getId().equals(id))
    		{
    			return element;
    		}
		}
    	return null;
	}
    
//--simple--preserve

}
