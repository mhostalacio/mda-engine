
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewElementReference complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewElementReference">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewElement">
 *       &lt;attribute name="ElementId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewElementReference")
public class MVCViewElementReference
    extends MVCViewElement
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "ElementId", required = true)
    protected String elementId;

    /**
     * Gets the value of the elementId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getElementId() {
        return elementId;
    }

    /**
     * Sets the value of the elementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setElementId(String value) {
        this.elementId = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewElementReference)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewElementReference that = ((MVCViewElementReference) object);
        {
            String lhsElementId;
            lhsElementId = this.getElementId();
            String rhsElementId;
            rhsElementId = that.getElementId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "elementId", lhsElementId), LocatorUtils.property(thatLocator, "elementId", rhsElementId), lhsElementId, rhsElementId)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCViewElementReference) {
            final MVCViewElementReference copy = ((MVCViewElementReference) draftCopy);
            if (this.elementId!= null) {
                String sourceElementId;
                sourceElementId = this.getElementId();
                String copyElementId = ((String) strategy.copy(LocatorUtils.property(locator, "elementId", sourceElementId), sourceElementId));
                copy.setElementId(copyElementId);
            } else {
                copy.elementId = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewElementReference();
    }
    
//--simple--preserve

    @Override
    public void writeHtml(com.mda.engine.utils.Stringcode c)
    {
    	c.writeLine("<% _{0}.Render(Html); %>", com.mda.engine.utils.StringUtils.getCamelCase(this.getElementId()));
    }
    
    @Override
    public boolean HasInitializeMethods() {
    	return false;
    }
    
    @Override
    public String getViewElementId() {
        return com.mda.engine.utils.StringUtils.getPascalCase(this.getElementId()) + "Element";
    }
    
//--simple--preserve

}
