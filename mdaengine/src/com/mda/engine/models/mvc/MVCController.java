
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCController complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCController">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Actions" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCControllerActionCollection"/>
 *         &lt;element name="ValidationGroups" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCControllerValidationGroupCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="ControllerBaseClassName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCController", propOrder = {
    "actions",
    "validationGroups"
})
public class MVCController
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Actions", required = true)
    protected MVCControllerActionCollection actions;
    @XmlElement(name = "ValidationGroups")
    protected MVCControllerValidationGroupCollection validationGroups;
    @XmlAttribute(name = "ControllerBaseClassName")
    protected String controllerBaseClassName;

    /**
     * Gets the value of the actions property.
     * 
     * @return
     *     possible object is
     *     {@link MVCControllerActionCollection }
     *     
     */
    public MVCControllerActionCollection getActions() {
        return actions;
    }

    /**
     * Sets the value of the actions property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCControllerActionCollection }
     *     
     */
    public void setActions(MVCControllerActionCollection value) {
        this.actions = value;
    }

    /**
     * Gets the value of the validationGroups property.
     * 
     * @return
     *     possible object is
     *     {@link MVCControllerValidationGroupCollection }
     *     
     */
    public MVCControllerValidationGroupCollection getValidationGroups() {
        return validationGroups;
    }

    /**
     * Sets the value of the validationGroups property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCControllerValidationGroupCollection }
     *     
     */
    public void setValidationGroups(MVCControllerValidationGroupCollection value) {
        this.validationGroups = value;
    }

    /**
     * Gets the value of the controllerBaseClassName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControllerBaseClassName() {
        return controllerBaseClassName;
    }

    /**
     * Sets the value of the controllerBaseClassName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControllerBaseClassName(String value) {
        this.controllerBaseClassName = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCController)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final MVCController that = ((MVCController) object);
        {
            MVCControllerActionCollection lhsActions;
            lhsActions = this.getActions();
            MVCControllerActionCollection rhsActions;
            rhsActions = that.getActions();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "actions", lhsActions), LocatorUtils.property(thatLocator, "actions", rhsActions), lhsActions, rhsActions)) {
                return false;
            }
        }
        {
            MVCControllerValidationGroupCollection lhsValidationGroups;
            lhsValidationGroups = this.getValidationGroups();
            MVCControllerValidationGroupCollection rhsValidationGroups;
            rhsValidationGroups = that.getValidationGroups();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "validationGroups", lhsValidationGroups), LocatorUtils.property(thatLocator, "validationGroups", rhsValidationGroups), lhsValidationGroups, rhsValidationGroups)) {
                return false;
            }
        }
        {
            String lhsControllerBaseClassName;
            lhsControllerBaseClassName = this.getControllerBaseClassName();
            String rhsControllerBaseClassName;
            rhsControllerBaseClassName = that.getControllerBaseClassName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "controllerBaseClassName", lhsControllerBaseClassName), LocatorUtils.property(thatLocator, "controllerBaseClassName", rhsControllerBaseClassName), lhsControllerBaseClassName, rhsControllerBaseClassName)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof MVCController) {
            final MVCController copy = ((MVCController) draftCopy);
            if (this.actions!= null) {
                MVCControllerActionCollection sourceActions;
                sourceActions = this.getActions();
                MVCControllerActionCollection copyActions = ((MVCControllerActionCollection) strategy.copy(LocatorUtils.property(locator, "actions", sourceActions), sourceActions));
                copy.setActions(copyActions);
            } else {
                copy.actions = null;
            }
            if (this.validationGroups!= null) {
                MVCControllerValidationGroupCollection sourceValidationGroups;
                sourceValidationGroups = this.getValidationGroups();
                MVCControllerValidationGroupCollection copyValidationGroups = ((MVCControllerValidationGroupCollection) strategy.copy(LocatorUtils.property(locator, "validationGroups", sourceValidationGroups), sourceValidationGroups));
                copy.setValidationGroups(copyValidationGroups);
            } else {
                copy.validationGroups = null;
            }
            if (this.controllerBaseClassName!= null) {
                String sourceControllerBaseClassName;
                sourceControllerBaseClassName = this.getControllerBaseClassName();
                String copyControllerBaseClassName = ((String) strategy.copy(LocatorUtils.property(locator, "controllerBaseClassName", sourceControllerBaseClassName), sourceControllerBaseClassName));
                copy.setControllerBaseClassName(copyControllerBaseClassName);
            } else {
                copy.controllerBaseClassName = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCController();
    }

}
