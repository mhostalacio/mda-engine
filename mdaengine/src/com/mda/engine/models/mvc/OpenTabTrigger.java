
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for OpenTabTrigger complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OpenTabTrigger">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCTrigger">
 *       &lt;attribute name="TabID" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpenTabTrigger")
public class OpenTabTrigger
    extends MVCTrigger
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "TabID", required = true)
    protected String tabID;

    /**
     * Gets the value of the tabID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTabID() {
        return tabID;
    }

    /**
     * Sets the value of the tabID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTabID(String value) {
        this.tabID = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof OpenTabTrigger)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final OpenTabTrigger that = ((OpenTabTrigger) object);
        {
            String lhsTabID;
            lhsTabID = this.getTabID();
            String rhsTabID;
            rhsTabID = that.getTabID();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "tabID", lhsTabID), LocatorUtils.property(thatLocator, "tabID", rhsTabID), lhsTabID, rhsTabID)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof OpenTabTrigger) {
            final OpenTabTrigger copy = ((OpenTabTrigger) draftCopy);
            if (this.tabID!= null) {
                String sourceTabID;
                sourceTabID = this.getTabID();
                String copyTabID = ((String) strategy.copy(LocatorUtils.property(locator, "tabID", sourceTabID), sourceTabID));
                copy.setTabID(copyTabID);
            } else {
                copy.tabID = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new OpenTabTrigger();
    }
    
//--simple--preserve

    @Override
    protected String getActionCodeName()
    {
    	return "MVCViewElementOpenTabViewTrigger";
    }
    
//--simple--preserve

}
