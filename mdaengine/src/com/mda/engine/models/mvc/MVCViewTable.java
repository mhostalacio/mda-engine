
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.IMVCViewComposedElement;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewTable">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewElement">
 *       &lt;sequence>
 *         &lt;element name="HeaderRows" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewTableRowChoice" minOccurs="0"/>
 *         &lt;element name="FooterRows" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewTableRowChoice" minOccurs="0"/>
 *         &lt;element name="Rows" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewTableRowChoice" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewTable", propOrder = {
    "headerRows",
    "footerRows",
    "rows"
})
@XmlRootElement
public class MVCViewTable
    extends MVCViewElement
    implements Serializable, Cloneable, IMVCViewComposedElement, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "HeaderRows")
    protected MVCViewTableRowChoice headerRows;
    @XmlElement(name = "FooterRows")
    protected MVCViewTableRowChoice footerRows;
    @XmlElement(name = "Rows")
    protected MVCViewTableRowChoice rows;

    /**
     * Gets the value of the headerRows property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewTableRowChoice }
     *     
     */
    public MVCViewTableRowChoice getHeaderRows() {
        return headerRows;
    }

    /**
     * Sets the value of the headerRows property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewTableRowChoice }
     *     
     */
    public void setHeaderRows(MVCViewTableRowChoice value) {
        this.headerRows = value;
    }

    /**
     * Gets the value of the footerRows property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewTableRowChoice }
     *     
     */
    public MVCViewTableRowChoice getFooterRows() {
        return footerRows;
    }

    /**
     * Sets the value of the footerRows property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewTableRowChoice }
     *     
     */
    public void setFooterRows(MVCViewTableRowChoice value) {
        this.footerRows = value;
    }

    /**
     * Gets the value of the rows property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewTableRowChoice }
     *     
     */
    public MVCViewTableRowChoice getRows() {
        return rows;
    }

    /**
     * Sets the value of the rows property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewTableRowChoice }
     *     
     */
    public void setRows(MVCViewTableRowChoice value) {
        this.rows = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewTable)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewTable that = ((MVCViewTable) object);
        {
            MVCViewTableRowChoice lhsHeaderRows;
            lhsHeaderRows = this.getHeaderRows();
            MVCViewTableRowChoice rhsHeaderRows;
            rhsHeaderRows = that.getHeaderRows();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "headerRows", lhsHeaderRows), LocatorUtils.property(thatLocator, "headerRows", rhsHeaderRows), lhsHeaderRows, rhsHeaderRows)) {
                return false;
            }
        }
        {
            MVCViewTableRowChoice lhsFooterRows;
            lhsFooterRows = this.getFooterRows();
            MVCViewTableRowChoice rhsFooterRows;
            rhsFooterRows = that.getFooterRows();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "footerRows", lhsFooterRows), LocatorUtils.property(thatLocator, "footerRows", rhsFooterRows), lhsFooterRows, rhsFooterRows)) {
                return false;
            }
        }
        {
            MVCViewTableRowChoice lhsRows;
            lhsRows = this.getRows();
            MVCViewTableRowChoice rhsRows;
            rhsRows = that.getRows();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "rows", lhsRows), LocatorUtils.property(thatLocator, "rows", rhsRows), lhsRows, rhsRows)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCViewTable) {
            final MVCViewTable copy = ((MVCViewTable) draftCopy);
            if (this.headerRows!= null) {
                MVCViewTableRowChoice sourceHeaderRows;
                sourceHeaderRows = this.getHeaderRows();
                MVCViewTableRowChoice copyHeaderRows = ((MVCViewTableRowChoice) strategy.copy(LocatorUtils.property(locator, "headerRows", sourceHeaderRows), sourceHeaderRows));
                copy.setHeaderRows(copyHeaderRows);
            } else {
                copy.headerRows = null;
            }
            if (this.footerRows!= null) {
                MVCViewTableRowChoice sourceFooterRows;
                sourceFooterRows = this.getFooterRows();
                MVCViewTableRowChoice copyFooterRows = ((MVCViewTableRowChoice) strategy.copy(LocatorUtils.property(locator, "footerRows", sourceFooterRows), sourceFooterRows));
                copy.setFooterRows(copyFooterRows);
            } else {
                copy.footerRows = null;
            }
            if (this.rows!= null) {
                MVCViewTableRowChoice sourceRows;
                sourceRows = this.getRows();
                MVCViewTableRowChoice copyRows = ((MVCViewTableRowChoice) strategy.copy(LocatorUtils.property(locator, "rows", sourceRows), sourceRows));
                copy.setRows(copyRows);
            } else {
                copy.rows = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewTable();
    }
    
//--simple--preserve
    private transient MVCViewElementCollection elements;
    @Override
    public MVCViewElementCollection getChildElements()
    {
    	if (elements == null)
    	{
    		elements = new MVCViewElementCollection();
    		
    		if (this.getHeaderRows() != null && this.getHeaderRows().getElementList() != null){
    			//elements.getElementList().addAll(this.getHeaderRows().getElementList());
    			for(MVCViewElement elem : this.getHeaderRows().getElementList())
    			{
    				elements.getElementList().add(elem);
    				
    				if (elem instanceof MVCViewTableRow){
    					((MVCViewTableRow)elem).setRowType(MVCViewTableRowType.HEADER);
    					
    				} else if (elem instanceof MVCViewRepeater) {
    					
    					MVCViewRepeater rep = (MVCViewRepeater)elem;
    					
    					if (rep.getTemplate().getElementList() != null){
    		    			for (MVCViewElement child : rep.getTemplate().getElementList()){
    		    				if (child instanceof MVCViewTableRow){
    		    					((MVCViewTableRow)child).setRowType(MVCViewTableRowType.HEADER);
    		    				}
    		    			}
    		    		}
    				}    				
    			}
    		}
    		
    		if (this.getFooterRows() != null && this.getFooterRows().getElementList() != null){
    			
    			for(MVCViewElement elem : this.getFooterRows().getElementList()){
    				elements.getElementList().add(elem);
    				
    				if (elem instanceof MVCViewTableRow){
    					((MVCViewTableRow)elem).setRowType(MVCViewTableRowType.FOOTER);
    					
    				} else if (elem instanceof MVCViewRepeater) {
    					
    					MVCViewRepeater rep = (MVCViewRepeater)elem;
    					
    					if (rep.getTemplate().getElementList() != null){
    		    			for (MVCViewElement child : rep.getTemplate().getElementList()){
    		    				if (child instanceof MVCViewTableRow){
    		    					((MVCViewTableRow)child).setRowType(MVCViewTableRowType.FOOTER);
    		    				}
    		    			}
    		    		}
    				}    				
    			}
    		}
    		
    		if (this.getRows() != null && this.getRows().getElementList() != null){
    			elements.getElementList().addAll(this.getRows().getElementList());
    		}
    	}
    	return elements;
    }
    

    @Override
    public void writeHtml(com.mda.engine.utils.Stringcode c)
    {

    	c.write("<table cellspacing=\"0\"");
    	if (getId() != null)
    	{
    		c.write(" id=\"{0}\"", getId());
    	}
    	if (getCss() != null)
    	{
    		c.write(" class=\"{0}\"", getCss());
    	}
    	c.write(">");
    	c.writeLine();
    	writeHtmlChildren(c);
    	c.write("</table>");
    	c.writeLine();
    }
    
    @Override
    public String getClassName() throws Exception
    {
    	return "MVCViewTable";
    }
    
    @Override
    public void writeInitializeMethodInternal(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product) throws Exception
    {
    	if (getHeaderRows()!= null)
    	{
    		for(MVCViewElement elem : getHeaderRows().getElementList())
    		{
    			MVCViewTableRow row = (MVCViewTableRow)elem;
    			row.setRowType(MVCViewTableRowType.HEADER);
    			if (row.getCells() != null)
    			{
    				for(MVCViewElement child : row.getCells().getElementList())
    				{
    					if (child instanceof MVCViewRepeater)
    					{
//    						MVCViewRepeater rep = (MVCViewRepeater)child;
//    			    		c.writeLine("{0}.ChildElements.AddRange({1}.GetViewElements<{2}>());", row.getViewElementId(), child.getViewElementId(), rep.getTemplate().getElementList().get(0).getClassName());
    					}
    					else
    					{
	    					MVCViewTableCell cell = (MVCViewTableCell)child;
	    					cell.setCellType(MVCViewTableRowType.HEADER);
    					}
    				}
    			}
    		}
    	}
    	
    	if (this.getFooterRows() != null){
    		
    		for(MVCViewElement elem : getFooterRows().getElementList()) {
    			
    			MVCViewTableRow row = (MVCViewTableRow)elem;
    			row.setRowType(MVCViewTableRowType.FOOTER);
    			
    			if (row.getCells() != null){
    				
    				for(MVCViewElement child : row.getCells().getElementList()){
    					if (child instanceof MVCViewRepeater) {
//    						MVCViewRepeater rep = (MVCViewRepeater)child;
//    			    		c.writeLine("{0}.ChildElements.AddRange({1}.GetViewElements<{2}>());", row.getViewElementId(), child.getViewElementId(), rep.getTemplate().getElementList().get(0).getClassName());
    					} else {
	    					MVCViewTableCell cell = (MVCViewTableCell)child;
	    					cell.setCellType(MVCViewTableRowType.FOOTER);
    					}
    				}
    			}
    		}
    	}
    	
    	if (getRows() != null)
    	{
    		
    	}
    }
//--simple--preserve

}
