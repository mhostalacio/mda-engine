
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for CallActionTrigger complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CallActionTrigger">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCTrigger">
 *       &lt;attribute name="InvokeMethod" type="{http://www.mdaengine.com/mdaengine/models/mvc}AcceptVerbsEnum" default="Get" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallActionTrigger")
public class CallActionTrigger
    extends MVCTrigger
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "InvokeMethod")
    protected AcceptVerbsEnum invokeMethod;

    /**
     * Gets the value of the invokeMethod property.
     * 
     * @return
     *     possible object is
     *     {@link AcceptVerbsEnum }
     *     
     */
    public AcceptVerbsEnum getInvokeMethod() {
        if (invokeMethod == null) {
            return AcceptVerbsEnum.GET;
        } else {
            return invokeMethod;
        }
    }

    /**
     * Sets the value of the invokeMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link AcceptVerbsEnum }
     *     
     */
    public void setInvokeMethod(AcceptVerbsEnum value) {
        this.invokeMethod = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof CallActionTrigger)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final CallActionTrigger that = ((CallActionTrigger) object);
        {
            AcceptVerbsEnum lhsInvokeMethod;
            lhsInvokeMethod = this.getInvokeMethod();
            AcceptVerbsEnum rhsInvokeMethod;
            rhsInvokeMethod = that.getInvokeMethod();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "invokeMethod", lhsInvokeMethod), LocatorUtils.property(thatLocator, "invokeMethod", rhsInvokeMethod), lhsInvokeMethod, rhsInvokeMethod)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof CallActionTrigger) {
            final CallActionTrigger copy = ((CallActionTrigger) draftCopy);
            if (this.invokeMethod!= null) {
                AcceptVerbsEnum sourceInvokeMethod;
                sourceInvokeMethod = this.getInvokeMethod();
                AcceptVerbsEnum copyInvokeMethod = ((AcceptVerbsEnum) strategy.copy(LocatorUtils.property(locator, "invokeMethod", sourceInvokeMethod), sourceInvokeMethod));
                copy.setInvokeMethod(copyInvokeMethod);
            } else {
                copy.invokeMethod = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new CallActionTrigger();
    }
    
//--simple--preserve

    @Override
    protected String getActionCodeName()
    {
    	return "MVCViewElementCallActionTrigger";
    }
    
//--simple--preserve

}
