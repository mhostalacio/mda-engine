
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCTrigger complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCTrigger">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Arguments" type="{http://www.mdaengine.com/mdaengine/models/mvc}TriggerArgumentsCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="ActionName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="UseAjax" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *       &lt;attribute name="BlockForm" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCTrigger", propOrder = {
    "arguments"
})
@XmlSeeAlso({
    OpenTabTrigger.class,
    JavaScriptActionTrigger.class,
    PostActionTrigger.class,
    OpenPopupTrigger.class,
    RedirectTrigger.class,
    OpenToolTipTrigger.class,
    CallActionTrigger.class
})
public abstract class MVCTrigger
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Arguments")
    protected TriggerArgumentsCollection arguments;
    @XmlAttribute(name = "ActionName")
    protected String actionName;
    @XmlAttribute(name = "UseAjax")
    protected Boolean useAjax;
    @XmlAttribute(name = "BlockForm")
    protected Boolean blockForm;

    /**
     * Gets the value of the arguments property.
     * 
     * @return
     *     possible object is
     *     {@link TriggerArgumentsCollection }
     *     
     */
    public TriggerArgumentsCollection getArguments() {
        return arguments;
    }

    /**
     * Sets the value of the arguments property.
     * 
     * @param value
     *     allowed object is
     *     {@link TriggerArgumentsCollection }
     *     
     */
    public void setArguments(TriggerArgumentsCollection value) {
        this.arguments = value;
    }

    /**
     * Gets the value of the actionName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionName() {
        return actionName;
    }

    /**
     * Sets the value of the actionName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionName(String value) {
        this.actionName = value;
    }

    /**
     * Gets the value of the useAjax property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isUseAjax() {
        if (useAjax == null) {
            return true;
        } else {
            return useAjax;
        }
    }

    /**
     * Sets the value of the useAjax property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseAjax(Boolean value) {
        this.useAjax = value;
    }

    /**
     * Gets the value of the blockForm property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isBlockForm() {
        if (blockForm == null) {
            return true;
        } else {
            return blockForm;
        }
    }

    /**
     * Sets the value of the blockForm property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBlockForm(Boolean value) {
        this.blockForm = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCTrigger)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final MVCTrigger that = ((MVCTrigger) object);
        {
            TriggerArgumentsCollection lhsArguments;
            lhsArguments = this.getArguments();
            TriggerArgumentsCollection rhsArguments;
            rhsArguments = that.getArguments();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "arguments", lhsArguments), LocatorUtils.property(thatLocator, "arguments", rhsArguments), lhsArguments, rhsArguments)) {
                return false;
            }
        }
        {
            String lhsActionName;
            lhsActionName = this.getActionName();
            String rhsActionName;
            rhsActionName = that.getActionName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "actionName", lhsActionName), LocatorUtils.property(thatLocator, "actionName", rhsActionName), lhsActionName, rhsActionName)) {
                return false;
            }
        }
        {
            boolean lhsUseAjax;
            lhsUseAjax = this.isUseAjax();
            boolean rhsUseAjax;
            rhsUseAjax = that.isUseAjax();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "useAjax", lhsUseAjax), LocatorUtils.property(thatLocator, "useAjax", rhsUseAjax), lhsUseAjax, rhsUseAjax)) {
                return false;
            }
        }
        {
            boolean lhsBlockForm;
            lhsBlockForm = this.isBlockForm();
            boolean rhsBlockForm;
            rhsBlockForm = that.isBlockForm();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "blockForm", lhsBlockForm), LocatorUtils.property(thatLocator, "blockForm", rhsBlockForm), lhsBlockForm, rhsBlockForm)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        if (null == target) {
            throw new IllegalArgumentException("Target argument must not be null for abstract copyable classes.");
        }
        if (target instanceof MVCTrigger) {
            final MVCTrigger copy = ((MVCTrigger) target);
            if (this.arguments!= null) {
                TriggerArgumentsCollection sourceArguments;
                sourceArguments = this.getArguments();
                TriggerArgumentsCollection copyArguments = ((TriggerArgumentsCollection) strategy.copy(LocatorUtils.property(locator, "arguments", sourceArguments), sourceArguments));
                copy.setArguments(copyArguments);
            } else {
                copy.arguments = null;
            }
            if (this.actionName!= null) {
                String sourceActionName;
                sourceActionName = this.getActionName();
                String copyActionName = ((String) strategy.copy(LocatorUtils.property(locator, "actionName", sourceActionName), sourceActionName));
                copy.setActionName(copyActionName);
            } else {
                copy.actionName = null;
            }
            if (this.useAjax!= null) {
                boolean sourceUseAjax;
                sourceUseAjax = this.isUseAjax();
                boolean copyUseAjax = strategy.copy(LocatorUtils.property(locator, "useAjax", sourceUseAjax), sourceUseAjax);
                copy.setUseAjax(copyUseAjax);
            } else {
                copy.useAjax = null;
            }
            if (this.blockForm!= null) {
                boolean sourceBlockForm;
                sourceBlockForm = this.isBlockForm();
                boolean copyBlockForm = strategy.copy(LocatorUtils.property(locator, "blockForm", sourceBlockForm), sourceBlockForm);
                copy.setBlockForm(copyBlockForm);
            } else {
                copy.blockForm = null;
            }
        }
        return target;
    }
    
//--simple--preserve
    
    protected String getActionCodeName()
    {
    	return null;
    }
    
    public void writeInitializeTrigger(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product, String elementId, String propertyName)
    {
    	c.line("{0}.{1}.Add(", elementId, propertyName);
    	c.tokenAdd2BeginOfLineIndent();
    	c.line("new {0}(){", getActionCodeName());
    	c.tokenAdd2BeginOfLineIndent();
    	
    	if (!(this instanceof OpenPopupTrigger) && !(this instanceof JavaScriptActionTrigger) && !(this instanceof RedirectTrigger) )
    	{
	    	c.line("AreaName = this.AreaName,");
	    	c.line("ControllerName = this.ControllerName,");
	    	c.line("UseAjax = {0},", this.isUseAjax());
    	}
    	c.line("ActionName = \"{0}\",", this.getActionName());
    	c.line("BlockForm = {0},", this.isBlockForm());
    	if (this instanceof CallActionTrigger)
    	{
    		CallActionTrigger getAction = (CallActionTrigger)this;
    		if (getAction.getInvokeMethod().equals(AcceptVerbsEnum.GET))
            {
    			c.line("Method = HttpVerbs.Get,");
            }
            else if (getAction.getInvokeMethod().equals(AcceptVerbsEnum.POST))
            {
            	c.line("Method = HttpVerbs.Post,");
            }
    	}
    	if (this instanceof OpenTabTrigger)
    	{
    		OpenTabTrigger openTabAction = (OpenTabTrigger)this;
    		c.line("TabID = \"{0}\",", openTabAction.getTabID());
    	}
    	if (this instanceof RedirectTrigger)
    	{
    		RedirectTrigger getAction = (RedirectTrigger)this;
    		c.line("AreaName = \"{0}\",", getAction.getAreaName());
	    	c.line("ControllerName = \"{0}\",", getAction.getControllerName());
	    	c.line("UseAjax = {0},", this.isUseAjax());
    	}
    	if (this instanceof OpenPopupTrigger)
    	{
    		OpenPopupTrigger getAction = (OpenPopupTrigger)this;
    		if (getAction.getPopupMvcName() != null)
            {
    			c.line("PopupWindowName = \"{0}\",", getAction.getPopupMvcName());
    			MVCInstance mvc = product.getMvcByName(getAction.getPopupMvcName());
	    		c.line("AreaName = \"{0}\",", mvc.getPackageName());
		    	c.line("ControllerName = \"{0}\",", mvc.getName());
            }
    		if (getAction.getPopupWidth() != null)
            {
    			c.line("PopupWidth = {0},", getAction.getPopupWidth());
            }
    		if (getAction.getPopupHeight() != null)
            {
    			c.line("PopupHeight = {0},", getAction.getPopupHeight());
            }
    	}
    	if (this instanceof OpenToolTipTrigger)
    	{
    		OpenToolTipTrigger getAction = (OpenToolTipTrigger)this;
    		c.line("HorizontalPosition = ToolTipHorizontalPosition.{0},", getAction.getHorizontalPosition().value());
    		c.line("VerticalPosition = ToolTipVerticalPosition.{0},", getAction.getVerticalPosition().value());
    	}
    	if (this.getArguments() != null && this.getArguments().getArgumentList() != null && this.getArguments().getArgumentList().size() > 0)
    	{
    		
    		c.line("Arguments = new MVCViewArgumentCollection(new[]");
    		c.line("{");
    		c.tokenAdd2BeginOfLineIndent();
    		for(TriggerArgument arg : this.getArguments().getArgumentList())
    		{
    			c.line("new MVCViewArgument { Name = \"{0}\", Value = {1} },", arg.getArgumentName(), arg.getValue().getTriggerArgumentValue());
    		}
    		c.tokenRemove2BeginOfLine();
    		c.line("}),");
    	}
    	c.tokenRemove2BeginOfLine();
        c.line("});");
        c.tokenRemove2BeginOfLine();
    }
    
//--simple--preserve

}
