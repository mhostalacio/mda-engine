
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewValueChoice complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewValueChoice">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice minOccurs="0">
 *         &lt;element name="ModelAttribute" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValueModelAttribute"/>
 *         &lt;element name="StaticStringValue" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValueStaticStringValue"/>
 *         &lt;element name="StaticBooleanValue" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValueStaticBooleanValue"/>
 *         &lt;element name="ViewElementValue" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewElementValue"/>
 *         &lt;element name="LocalizedStringValue" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValueLocalizedStringValue"/>
 *         &lt;element name="DataSourceIndex" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewDataSourceIndexValue"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewValueChoice", propOrder = {
    "modelAttribute",
    "staticStringValue",
    "staticBooleanValue",
    "viewElementValue",
    "localizedStringValue",
    "dataSourceIndex"
})
public class MVCViewValueChoice
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "ModelAttribute")
    protected MVCViewValueModelAttribute modelAttribute;
    @XmlElement(name = "StaticStringValue")
    protected MVCViewValueStaticStringValue staticStringValue;
    @XmlElement(name = "StaticBooleanValue")
    protected MVCViewValueStaticBooleanValue staticBooleanValue;
    @XmlElement(name = "ViewElementValue")
    protected MVCViewElementValue viewElementValue;
    @XmlElement(name = "LocalizedStringValue")
    protected MVCViewValueLocalizedStringValue localizedStringValue;
    @XmlElement(name = "DataSourceIndex")
    protected MVCViewDataSourceIndexValue dataSourceIndex;

    /**
     * Gets the value of the modelAttribute property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewValueModelAttribute }
     *     
     */
    public MVCViewValueModelAttribute getModelAttribute() {
        return modelAttribute;
    }

    /**
     * Sets the value of the modelAttribute property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewValueModelAttribute }
     *     
     */
    public void setModelAttribute(MVCViewValueModelAttribute value) {
        this.modelAttribute = value;
    }

    /**
     * Gets the value of the staticStringValue property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewValueStaticStringValue }
     *     
     */
    public MVCViewValueStaticStringValue getStaticStringValue() {
        return staticStringValue;
    }

    /**
     * Sets the value of the staticStringValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewValueStaticStringValue }
     *     
     */
    public void setStaticStringValue(MVCViewValueStaticStringValue value) {
        this.staticStringValue = value;
    }

    /**
     * Gets the value of the staticBooleanValue property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewValueStaticBooleanValue }
     *     
     */
    public MVCViewValueStaticBooleanValue getStaticBooleanValue() {
        return staticBooleanValue;
    }

    /**
     * Sets the value of the staticBooleanValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewValueStaticBooleanValue }
     *     
     */
    public void setStaticBooleanValue(MVCViewValueStaticBooleanValue value) {
        this.staticBooleanValue = value;
    }

    /**
     * Gets the value of the viewElementValue property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewElementValue }
     *     
     */
    public MVCViewElementValue getViewElementValue() {
        return viewElementValue;
    }

    /**
     * Sets the value of the viewElementValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewElementValue }
     *     
     */
    public void setViewElementValue(MVCViewElementValue value) {
        this.viewElementValue = value;
    }

    /**
     * Gets the value of the localizedStringValue property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewValueLocalizedStringValue }
     *     
     */
    public MVCViewValueLocalizedStringValue getLocalizedStringValue() {
        return localizedStringValue;
    }

    /**
     * Sets the value of the localizedStringValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewValueLocalizedStringValue }
     *     
     */
    public void setLocalizedStringValue(MVCViewValueLocalizedStringValue value) {
        this.localizedStringValue = value;
    }

    /**
     * Gets the value of the dataSourceIndex property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewDataSourceIndexValue }
     *     
     */
    public MVCViewDataSourceIndexValue getDataSourceIndex() {
        return dataSourceIndex;
    }

    /**
     * Sets the value of the dataSourceIndex property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewDataSourceIndexValue }
     *     
     */
    public void setDataSourceIndex(MVCViewDataSourceIndexValue value) {
        this.dataSourceIndex = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewValueChoice)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final MVCViewValueChoice that = ((MVCViewValueChoice) object);
        {
            MVCViewValueModelAttribute lhsModelAttribute;
            lhsModelAttribute = this.getModelAttribute();
            MVCViewValueModelAttribute rhsModelAttribute;
            rhsModelAttribute = that.getModelAttribute();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "modelAttribute", lhsModelAttribute), LocatorUtils.property(thatLocator, "modelAttribute", rhsModelAttribute), lhsModelAttribute, rhsModelAttribute)) {
                return false;
            }
        }
        {
            MVCViewValueStaticStringValue lhsStaticStringValue;
            lhsStaticStringValue = this.getStaticStringValue();
            MVCViewValueStaticStringValue rhsStaticStringValue;
            rhsStaticStringValue = that.getStaticStringValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "staticStringValue", lhsStaticStringValue), LocatorUtils.property(thatLocator, "staticStringValue", rhsStaticStringValue), lhsStaticStringValue, rhsStaticStringValue)) {
                return false;
            }
        }
        {
            MVCViewValueStaticBooleanValue lhsStaticBooleanValue;
            lhsStaticBooleanValue = this.getStaticBooleanValue();
            MVCViewValueStaticBooleanValue rhsStaticBooleanValue;
            rhsStaticBooleanValue = that.getStaticBooleanValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "staticBooleanValue", lhsStaticBooleanValue), LocatorUtils.property(thatLocator, "staticBooleanValue", rhsStaticBooleanValue), lhsStaticBooleanValue, rhsStaticBooleanValue)) {
                return false;
            }
        }
        {
            MVCViewElementValue lhsViewElementValue;
            lhsViewElementValue = this.getViewElementValue();
            MVCViewElementValue rhsViewElementValue;
            rhsViewElementValue = that.getViewElementValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "viewElementValue", lhsViewElementValue), LocatorUtils.property(thatLocator, "viewElementValue", rhsViewElementValue), lhsViewElementValue, rhsViewElementValue)) {
                return false;
            }
        }
        {
            MVCViewValueLocalizedStringValue lhsLocalizedStringValue;
            lhsLocalizedStringValue = this.getLocalizedStringValue();
            MVCViewValueLocalizedStringValue rhsLocalizedStringValue;
            rhsLocalizedStringValue = that.getLocalizedStringValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "localizedStringValue", lhsLocalizedStringValue), LocatorUtils.property(thatLocator, "localizedStringValue", rhsLocalizedStringValue), lhsLocalizedStringValue, rhsLocalizedStringValue)) {
                return false;
            }
        }
        {
            MVCViewDataSourceIndexValue lhsDataSourceIndex;
            lhsDataSourceIndex = this.getDataSourceIndex();
            MVCViewDataSourceIndexValue rhsDataSourceIndex;
            rhsDataSourceIndex = that.getDataSourceIndex();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dataSourceIndex", lhsDataSourceIndex), LocatorUtils.property(thatLocator, "dataSourceIndex", rhsDataSourceIndex), lhsDataSourceIndex, rhsDataSourceIndex)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof MVCViewValueChoice) {
            final MVCViewValueChoice copy = ((MVCViewValueChoice) draftCopy);
            if (this.modelAttribute!= null) {
                MVCViewValueModelAttribute sourceModelAttribute;
                sourceModelAttribute = this.getModelAttribute();
                MVCViewValueModelAttribute copyModelAttribute = ((MVCViewValueModelAttribute) strategy.copy(LocatorUtils.property(locator, "modelAttribute", sourceModelAttribute), sourceModelAttribute));
                copy.setModelAttribute(copyModelAttribute);
            } else {
                copy.modelAttribute = null;
            }
            if (this.staticStringValue!= null) {
                MVCViewValueStaticStringValue sourceStaticStringValue;
                sourceStaticStringValue = this.getStaticStringValue();
                MVCViewValueStaticStringValue copyStaticStringValue = ((MVCViewValueStaticStringValue) strategy.copy(LocatorUtils.property(locator, "staticStringValue", sourceStaticStringValue), sourceStaticStringValue));
                copy.setStaticStringValue(copyStaticStringValue);
            } else {
                copy.staticStringValue = null;
            }
            if (this.staticBooleanValue!= null) {
                MVCViewValueStaticBooleanValue sourceStaticBooleanValue;
                sourceStaticBooleanValue = this.getStaticBooleanValue();
                MVCViewValueStaticBooleanValue copyStaticBooleanValue = ((MVCViewValueStaticBooleanValue) strategy.copy(LocatorUtils.property(locator, "staticBooleanValue", sourceStaticBooleanValue), sourceStaticBooleanValue));
                copy.setStaticBooleanValue(copyStaticBooleanValue);
            } else {
                copy.staticBooleanValue = null;
            }
            if (this.viewElementValue!= null) {
                MVCViewElementValue sourceViewElementValue;
                sourceViewElementValue = this.getViewElementValue();
                MVCViewElementValue copyViewElementValue = ((MVCViewElementValue) strategy.copy(LocatorUtils.property(locator, "viewElementValue", sourceViewElementValue), sourceViewElementValue));
                copy.setViewElementValue(copyViewElementValue);
            } else {
                copy.viewElementValue = null;
            }
            if (this.localizedStringValue!= null) {
                MVCViewValueLocalizedStringValue sourceLocalizedStringValue;
                sourceLocalizedStringValue = this.getLocalizedStringValue();
                MVCViewValueLocalizedStringValue copyLocalizedStringValue = ((MVCViewValueLocalizedStringValue) strategy.copy(LocatorUtils.property(locator, "localizedStringValue", sourceLocalizedStringValue), sourceLocalizedStringValue));
                copy.setLocalizedStringValue(copyLocalizedStringValue);
            } else {
                copy.localizedStringValue = null;
            }
            if (this.dataSourceIndex!= null) {
                MVCViewDataSourceIndexValue sourceDataSourceIndex;
                sourceDataSourceIndex = this.getDataSourceIndex();
                MVCViewDataSourceIndexValue copyDataSourceIndex = ((MVCViewDataSourceIndexValue) strategy.copy(LocatorUtils.property(locator, "dataSourceIndex", sourceDataSourceIndex), sourceDataSourceIndex));
                copy.setDataSourceIndex(copyDataSourceIndex);
            } else {
                copy.dataSourceIndex = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewValueChoice();
    }
    
//--simple--preserve
    
    private transient com.mda.engine.models.businessObjectModel.ObjectType associatedBom;
    
    public String getStringBind()
    {
    	if (this.getModelAttribute() != null)
		{
    		if (this.getModelAttribute().isGetTranslation())
    		{
    			if (this.getModelAttribute().getTranslationStringFormat() != null)
    			{
    				return String.format("GetTranslation(\"%s\", String.Format(\"%s\", %s))", this.getModelAttribute().getTranslationKeyType(), this.getModelAttribute().getTranslationStringFormat(), this.getModelAttribute().getPath());
    			}
    			else
    			{
    				return String.format("GetTranslation(\"%s\", %s)", this.getModelAttribute().getTranslationKeyType(), this.getModelAttribute().getPath());
    			}
    		}
    		else
    		{
    			return this.getModelAttribute().getPath();
    		}
		}
		else if (this.getStaticStringValue() != null)
		{
			return this.getStaticStringValue().getValue();
		}
		else if (this.getStaticBooleanValue() != null)
		{
			return this.getStaticBooleanValue().value.toString();
		}
		else if (this.getLocalizedStringValue() != null) {
			return String.format("GetTranslation(\"%s\", \"%s\")" , this.getLocalizedStringValue().getKeyType(), this.getLocalizedStringValue().getKey());
			
		}
    	
    	return "NOT IMPLEMENTED";
    }
    
    public void writeBind(com.mda.engine.utils.Stringcode c, String elementPropertyName, String bindName, boolean convertToString)
    {
    	if (this.getModelAttribute() != null)
		{
    		if (convertToString)
			{
    			c.line("{0}.{2} = delegate(Boolean setValue, String newValue) { return Convert.ToString({1}); };",elementPropertyName , getStringBind(), bindName, this.getModelAttribute().getClassName());
				c.line("{0}.BindObjectCode = \"{1}\";",elementPropertyName , getStringBind());
			}
    		else if (this.getModelAttribute().isGetTranslation())
			{
    			c.line("{0}.{2} = delegate(Boolean setValue, String newValue) { return Convert.ToString({1}); };",elementPropertyName , getStringBind(), bindName, this.getModelAttribute().getClassName());
				c.line("{0}.BindObjectCode = {1};",elementPropertyName , getStringBind());
			}
			else
			{
				c.line("{0}.{2} = delegate(Boolean setValue, {3} newValue) { if (setValue) { {1}{4} = newValue; }  return {1}; };",elementPropertyName , getStringBind(), bindName, this.getModelAttribute().getClassName(),this.getModelAttribute().getPathSuffix());
				c.line("{0}.BindObjectCode = \"{1}\";",elementPropertyName , getStringBind());
			}
		}
		else if (this.getStaticStringValue() != null)
		{
			c.line("{0}.{2} = delegate(Boolean setValue, String newValue) { return \"{1}\"; };",elementPropertyName , getStringBind(), bindName);
		}
		else if (this.getStaticBooleanValue() != null)
		{
			
			if (convertToString)
			{
				c.line("{0}.{2} = delegate(Boolean setValue, String newValue) { return Convert.ToString({1}); };",elementPropertyName , getStringBind(), bindName);
			}
			else
			{
				c.line("{0}.{2} = delegate(Boolean setValue, Boolean newValue) { return {1}; };",elementPropertyName , getStringBind(), bindName);
			}
		}
		else if (this.getLocalizedStringValue() != null) {
			c.line("{0}.{2} = delegate(Boolean setValue, String newValue) { return {1}; };",elementPropertyName , getStringBind(), bindName);
			
		}
    }

    public void writeIsCheckedBind(com.mda.engine.utils.Stringcode c, String elementPropertyName, String bindName, MVCViewValueChoice optionBind)
    {
    	if (this.getModelAttribute() != null)
		{
    		if (this.getModelAttribute().isList())
    		{
    			c.line("{0}.{2} = delegate(Boolean setValue, Boolean isChecked) { if (setValue) { if (isChecked) { {1}.AddIfNew({4}); } else { {1}.Remove({4}); }  }  return {1}.Contains({4}); };",
    					elementPropertyName, getStringBind(), bindName, this.getModelAttribute().getSingleClassName(), optionBind.getStringBind());
    		}
		}
    }
    
    public void writeBindForDataSource(com.mda.engine.utils.Stringcode c, String elementPropertyName)
    {
    	if (this.getModelAttribute() != null)
		{
			c.line("{0}.DataSource = delegate() { return {1}; };",elementPropertyName,  this.getModelAttribute().getPath());
		}
    }
    
    public String getSingleClassName()
    {
    	if (this.getModelAttribute() != null)
		{
			return this.getModelAttribute().getSingleClassName();
		}
    	else
    		return getClassName();
    }
    
    public String getClassName()
    {
    	if (this.getModelAttribute() != null)
		{
			return this.getModelAttribute().getClassName();
		}
		else if (this.getStaticStringValue() != null)
		{
			return "String";
		}
		else if (this.getLocalizedStringValue() != null)
		{
			return "String";
		}
		else if (this.getStaticBooleanValue() != null)
		{
			return "Boolean";
		}
    	return "";
    }
    
    public void writeHtmlBind(com.mda.engine.utils.Stringcode c) {
    	if (this.getModelAttribute() != null)
		{
			c.write("<%= {0} %>", this.getModelAttribute().getPath());
		}
		else if (this.getStaticStringValue() != null)
		{
			c.write(this.getStaticStringValue().getValue());
		}
		else if (this.getLocalizedStringValue()!= null)
		{
			c.write("<%= Controller.GetTranslation(\"{0}\", \"{1}\") %>", this.getLocalizedStringValue().getKeyType(), this.getLocalizedStringValue().getKey());
		}
		else if (this.getStaticBooleanValue() != null)
		{
			c.write(this.getStaticBooleanValue().toString());
		}
	}
    
    public void setBinds(MVCModel model, com.mda.engine.core.IMVCViewElementWithDatasource contextElement, com.mda.engine.models.mvc.MVCViewElement currentElement) throws Exception
    {
    	if (this.getModelAttribute() != null)
		{    		
			this.getModelAttribute().setBinds(model, contextElement, currentElement);
			setAssociatedBom(this.getModelAttribute().getAssociatedBom());
		}
    }

	public void setAssociatedBom(com.mda.engine.models.businessObjectModel.ObjectType associatedBom) {
		this.associatedBom = associatedBom;
	}

	public com.mda.engine.models.businessObjectModel.ObjectType getAssociatedBom() {
		return associatedBom;
	}
	
	public String getTriggerArgumentValue()
	{
		if (this.getModelAttribute() != null)
		{
			return String.format("new MVCViewModelArgumentValue<%s> { Value = () => %s , AddEncodeUri = true }",this.getModelAttribute().getClassName(), this.getModelAttribute().getPath());
		}
		else if (this.getDataSourceIndex() != null)
		{
			return String.format("new MVCViewModelArgumentValue<int> { Value = () => %sIndex , AddEncodeUri = true }",this.getDataSourceIndex().getPath());
		}
		else if (this.getStaticStringValue() != null)
		{
			return String.format("new MVCViewLiteralArgumentValue(\"%s\")",this.getStaticStringValue().getValue());
		}
		else if (this.getLocalizedStringValue() != null)
		{
			return String.format("new MVCViewLiteralArgumentValue(GetTranslation(\"%s\", \"%s\"))",this.getLocalizedStringValue().getKeyType(), this.getLocalizedStringValue().getKey());
		}
		else if (this.getStaticBooleanValue() != null)
		{
			return String.format("new MVCViewLiteralArgumentValue(\"%s\")",this.getStaticBooleanValue().value);
		}
		else if (this.getViewElementValue() != null)
		{
			if (!this.getViewElementValue().getElementId().contains("."))
				return String.format("new MVCViewFormElementArgumentValue{ Element = () => %sElement , AddEncodeUri = true }",com.mda.engine.utils.StringUtils.getPascalCase(this.getViewElementValue().getElementId()));
			else
				return String.format("new MVCViewFormElementArgumentValue{ Element = () => %s , AddEncodeUri = true }",com.mda.engine.utils.StringUtils.getPascalCase(this.getViewElementValue().getElementId()));
		}
		return "";
	}
    
//--simple--preserve

}
