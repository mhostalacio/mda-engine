
package com.mda.engine.models.mvc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCControllerValidationGroupCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCControllerValidationGroupCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Group" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCControllerValidationGroup" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Initial" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCControllerValidationGroupCollection", propOrder = {
    "group"
})
public class MVCControllerValidationGroupCollection
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Group", required = true)
    protected List<MVCControllerValidationGroup> group;
    @XmlAttribute(name = "Initial", required = true)
    protected String initial;

    /**
     * Gets the value of the group property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the group property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGroup().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MVCControllerValidationGroup }
     * 
     * 
     */
    public List<MVCControllerValidationGroup> getGroup() {
        if (group == null) {
            group = new ArrayList<MVCControllerValidationGroup>();
        }
        return this.group;
    }

    /**
     * Gets the value of the initial property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInitial() {
        return initial;
    }

    /**
     * Sets the value of the initial property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInitial(String value) {
        this.initial = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCControllerValidationGroupCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final MVCControllerValidationGroupCollection that = ((MVCControllerValidationGroupCollection) object);
        {
            List<MVCControllerValidationGroup> lhsGroup;
            lhsGroup = this.getGroup();
            List<MVCControllerValidationGroup> rhsGroup;
            rhsGroup = that.getGroup();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "group", lhsGroup), LocatorUtils.property(thatLocator, "group", rhsGroup), lhsGroup, rhsGroup)) {
                return false;
            }
        }
        {
            String lhsInitial;
            lhsInitial = this.getInitial();
            String rhsInitial;
            rhsInitial = that.getInitial();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "initial", lhsInitial), LocatorUtils.property(thatLocator, "initial", rhsInitial), lhsInitial, rhsInitial)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof MVCControllerValidationGroupCollection) {
            final MVCControllerValidationGroupCollection copy = ((MVCControllerValidationGroupCollection) draftCopy);
            if ((this.group!= null)&&(!this.group.isEmpty())) {
                List<MVCControllerValidationGroup> sourceGroup;
                sourceGroup = this.getGroup();
                @SuppressWarnings("unchecked")
                List<MVCControllerValidationGroup> copyGroup = ((List<MVCControllerValidationGroup> ) strategy.copy(LocatorUtils.property(locator, "group", sourceGroup), sourceGroup));
                copy.group = null;
                List<MVCControllerValidationGroup> uniqueGroupl = copy.getGroup();
                uniqueGroupl.addAll(copyGroup);
            } else {
                copy.group = null;
            }
            if (this.initial!= null) {
                String sourceInitial;
                sourceInitial = this.getInitial();
                String copyInitial = ((String) strategy.copy(LocatorUtils.property(locator, "initial", sourceInitial), sourceInitial));
                copy.setInitial(copyInitial);
            } else {
                copy.initial = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCControllerValidationGroupCollection();
    }

}
