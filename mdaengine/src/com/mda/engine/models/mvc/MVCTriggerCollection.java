
package com.mda.engine.models.mvc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCTriggerCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCTriggerCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded" minOccurs="0">
 *         &lt;element name="GetAction" type="{http://www.mdaengine.com/mdaengine/models/mvc}CallActionTrigger" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="PostAction" type="{http://www.mdaengine.com/mdaengine/models/mvc}PostActionTrigger" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="JavaScriptAction" type="{http://www.mdaengine.com/mdaengine/models/mvc}JavaScriptActionTrigger" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="OpenPopup" type="{http://www.mdaengine.com/mdaengine/models/mvc}OpenPopupTrigger" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="OpenToolTip" type="{http://www.mdaengine.com/mdaengine/models/mvc}OpenToolTipTrigger" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="OpenTab" type="{http://www.mdaengine.com/mdaengine/models/mvc}OpenTabTrigger" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Redirect" type="{http://www.mdaengine.com/mdaengine/models/mvc}RedirectTrigger" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCTriggerCollection", propOrder = {
    "triggerList"
})
public class MVCTriggerCollection
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElements({
        @XmlElement(name = "Redirect", type = RedirectTrigger.class),
        @XmlElement(name = "GetAction", type = CallActionTrigger.class),
        @XmlElement(name = "PostAction", type = PostActionTrigger.class),
        @XmlElement(name = "OpenTab", type = OpenTabTrigger.class),
        @XmlElement(name = "OpenToolTip", type = OpenToolTipTrigger.class),
        @XmlElement(name = "OpenPopup", type = OpenPopupTrigger.class),
        @XmlElement(name = "JavaScriptAction", type = JavaScriptActionTrigger.class)
    })
    protected List<MVCTrigger> triggerList;

    /**
     * Gets the value of the triggerList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the triggerList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTriggerList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RedirectTrigger }
     * {@link CallActionTrigger }
     * {@link PostActionTrigger }
     * {@link OpenTabTrigger }
     * {@link OpenToolTipTrigger }
     * {@link OpenPopupTrigger }
     * {@link JavaScriptActionTrigger }
     * 
     * 
     */
    public List<MVCTrigger> getTriggerList() {
        if (triggerList == null) {
            triggerList = new ArrayList<MVCTrigger>();
        }
        return this.triggerList;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCTriggerCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final MVCTriggerCollection that = ((MVCTriggerCollection) object);
        {
            List<MVCTrigger> lhsTriggerList;
            lhsTriggerList = this.getTriggerList();
            List<MVCTrigger> rhsTriggerList;
            rhsTriggerList = that.getTriggerList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "triggerList", lhsTriggerList), LocatorUtils.property(thatLocator, "triggerList", rhsTriggerList), lhsTriggerList, rhsTriggerList)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof MVCTriggerCollection) {
            final MVCTriggerCollection copy = ((MVCTriggerCollection) draftCopy);
            if ((this.triggerList!= null)&&(!this.triggerList.isEmpty())) {
                List<MVCTrigger> sourceTriggerList;
                sourceTriggerList = this.getTriggerList();
                @SuppressWarnings("unchecked")
                List<MVCTrigger> copyTriggerList = ((List<MVCTrigger> ) strategy.copy(LocatorUtils.property(locator, "triggerList", sourceTriggerList), sourceTriggerList));
                copy.triggerList = null;
                List<MVCTrigger> uniqueTriggerListl = copy.getTriggerList();
                uniqueTriggerListl.addAll(copyTriggerList);
            } else {
                copy.triggerList = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCTriggerCollection();
    }

}
