
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewSpan complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewSpan">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewComposedElement">
 *       &lt;sequence>
 *         &lt;element name="Text" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValueChoice" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="ConcatMandatorySymbol" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewSpan", propOrder = {
    "text"
})
@XmlSeeAlso({
    MVCViewLink.class,
    MVCViewH5 .class,
    MVCViewH6 .class,
    MVCViewH1 .class,
    MVCViewH2 .class,
    MVCViewH3 .class,
    MVCViewH4 .class,
    MVCViewLabel.class
})
public class MVCViewSpan
    extends MVCViewComposedElement
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Text")
    protected MVCViewValueChoice text;
    @XmlAttribute(name = "ConcatMandatorySymbol")
    protected Boolean concatMandatorySymbol;

    /**
     * Gets the value of the text property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public MVCViewValueChoice getText() {
        return text;
    }

    /**
     * Sets the value of the text property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public void setText(MVCViewValueChoice value) {
        this.text = value;
    }

    /**
     * Gets the value of the concatMandatorySymbol property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isConcatMandatorySymbol() {
        return concatMandatorySymbol;
    }

    /**
     * Sets the value of the concatMandatorySymbol property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setConcatMandatorySymbol(Boolean value) {
        this.concatMandatorySymbol = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewSpan)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewSpan that = ((MVCViewSpan) object);
        {
            MVCViewValueChoice lhsText;
            lhsText = this.getText();
            MVCViewValueChoice rhsText;
            rhsText = that.getText();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "text", lhsText), LocatorUtils.property(thatLocator, "text", rhsText), lhsText, rhsText)) {
                return false;
            }
        }
        {
            Boolean lhsConcatMandatorySymbol;
            lhsConcatMandatorySymbol = this.isConcatMandatorySymbol();
            Boolean rhsConcatMandatorySymbol;
            rhsConcatMandatorySymbol = that.isConcatMandatorySymbol();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "concatMandatorySymbol", lhsConcatMandatorySymbol), LocatorUtils.property(thatLocator, "concatMandatorySymbol", rhsConcatMandatorySymbol), lhsConcatMandatorySymbol, rhsConcatMandatorySymbol)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCViewSpan) {
            final MVCViewSpan copy = ((MVCViewSpan) draftCopy);
            if (this.text!= null) {
                MVCViewValueChoice sourceText;
                sourceText = this.getText();
                MVCViewValueChoice copyText = ((MVCViewValueChoice) strategy.copy(LocatorUtils.property(locator, "text", sourceText), sourceText));
                copy.setText(copyText);
            } else {
                copy.text = null;
            }
            if (this.concatMandatorySymbol!= null) {
                Boolean sourceConcatMandatorySymbol;
                sourceConcatMandatorySymbol = this.isConcatMandatorySymbol();
                Boolean copyConcatMandatorySymbol = ((Boolean) strategy.copy(LocatorUtils.property(locator, "concatMandatorySymbol", sourceConcatMandatorySymbol), sourceConcatMandatorySymbol));
                copy.setConcatMandatorySymbol(copyConcatMandatorySymbol);
            } else {
                copy.concatMandatorySymbol = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewSpan();
    }
    
//--simple--preserve
    
    @Override
    public String getClassName()
    {
    	if (getText() != null)
    	{
    		return "MVCViewSpan<"+getText().getSingleClassName() + ">";
    	}
    	return "MVCViewSpan<String>";
    }
    
    @Override
    public void writeHtml(com.mda.engine.utils.Stringcode c)
    {
    	c.write("<span");
    	if (getId() != null)
    	{
    		c.write(" id=\"{0}\"", getId());
    	}
    	if (getCss() != null)
    	{
    		c.write(" class=\"{0}\"", getCss());
    	}
    	writeAttributes(c);
    	c.write(">");
    	writeHtmlTextValue(c);
    	writeHtmlIconValue(c);
    	if(getChildElements() != null && getChildElements().getElementList() != null)
    	{
    		for(MVCViewElement elem : getChildElements().getElementList())
    		{
    			elem.writeHtml(c);
    		}
    	}
    	c.write("</span>");
    	c.writeLine();
    }

    public void writeHtmlTextValue(com.mda.engine.utils.Stringcode c) {
		if (getText() != null)
    	{
			getText().writeHtmlBind(c);
    	}
		if (isConcatMandatorySymbol() != null && isConcatMandatorySymbol().booleanValue())
		{
			c.write(" (*)");
		}
		c.writeLine();
	}
    
    public void writeHtmlIconValue(com.mda.engine.utils.Stringcode c) {
		if (getIconCss() != null)
    	{
			c.write("<i class=\"{0}\"></i>", getIconCss());
    	}
		c.writeLine();
	}
	
    @Override
    public void writeInitializeMethodInternal(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product)
    {
    	if (getText() != null)
    	{
    		getText().writeBind(c, getViewElementId(), "Bind", false);
    	}
    	if (isConcatMandatorySymbol() != null && isConcatMandatorySymbol().booleanValue())
    	{
    		c.line("{0}.ConcatMandatorySymbol = true;",getViewElementId());
    	}
    }
    
    @Override
    public void setBinds(MVCModel model) throws Exception
    {
    	super.setBinds(model);
    	if (getText() != null)
    	{
    		getText().setBinds(model, getContextElement(), this);
    	}
    }
//--simple--preserve

}
