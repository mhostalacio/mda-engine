
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewValueLocalizedStringValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewValueLocalizedStringValue">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValue">
 *       &lt;attribute name="Key" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="KeyType" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewValueLocalizedStringValue")
public class MVCViewValueLocalizedStringValue
    extends MVCViewValue
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "Key", required = true)
    protected String key;
    @XmlAttribute(name = "KeyType", required = true)
    protected String keyType;

    /**
     * Gets the value of the key property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        return key;
    }

    /**
     * Sets the value of the key property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }

    /**
     * Gets the value of the keyType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKeyType() {
        return keyType;
    }

    /**
     * Sets the value of the keyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKeyType(String value) {
        this.keyType = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewValueLocalizedStringValue)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewValueLocalizedStringValue that = ((MVCViewValueLocalizedStringValue) object);
        {
            String lhsKey;
            lhsKey = this.getKey();
            String rhsKey;
            rhsKey = that.getKey();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "key", lhsKey), LocatorUtils.property(thatLocator, "key", rhsKey), lhsKey, rhsKey)) {
                return false;
            }
        }
        {
            String lhsKeyType;
            lhsKeyType = this.getKeyType();
            String rhsKeyType;
            rhsKeyType = that.getKeyType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "keyType", lhsKeyType), LocatorUtils.property(thatLocator, "keyType", rhsKeyType), lhsKeyType, rhsKeyType)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCViewValueLocalizedStringValue) {
            final MVCViewValueLocalizedStringValue copy = ((MVCViewValueLocalizedStringValue) draftCopy);
            if (this.key!= null) {
                String sourceKey;
                sourceKey = this.getKey();
                String copyKey = ((String) strategy.copy(LocatorUtils.property(locator, "key", sourceKey), sourceKey));
                copy.setKey(copyKey);
            } else {
                copy.key = null;
            }
            if (this.keyType!= null) {
                String sourceKeyType;
                sourceKeyType = this.getKeyType();
                String copyKeyType = ((String) strategy.copy(LocatorUtils.property(locator, "keyType", sourceKeyType), sourceKeyType));
                copy.setKeyType(copyKeyType);
            } else {
                copy.keyType = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewValueLocalizedStringValue();
    }

}
