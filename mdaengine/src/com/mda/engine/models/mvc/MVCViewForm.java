
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewForm complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewForm">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewComposedElement">
 *       &lt;attribute name="DefaultButtonId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewForm")
public class MVCViewForm
    extends MVCViewComposedElement
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "DefaultButtonId", required = true)
    protected String defaultButtonId;

    /**
     * Gets the value of the defaultButtonId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultButtonId() {
        return defaultButtonId;
    }

    /**
     * Sets the value of the defaultButtonId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultButtonId(String value) {
        this.defaultButtonId = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewForm)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewForm that = ((MVCViewForm) object);
        {
            String lhsDefaultButtonId;
            lhsDefaultButtonId = this.getDefaultButtonId();
            String rhsDefaultButtonId;
            rhsDefaultButtonId = that.getDefaultButtonId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "defaultButtonId", lhsDefaultButtonId), LocatorUtils.property(thatLocator, "defaultButtonId", rhsDefaultButtonId), lhsDefaultButtonId, rhsDefaultButtonId)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCViewForm) {
            final MVCViewForm copy = ((MVCViewForm) draftCopy);
            if (this.defaultButtonId!= null) {
                String sourceDefaultButtonId;
                sourceDefaultButtonId = this.getDefaultButtonId();
                String copyDefaultButtonId = ((String) strategy.copy(LocatorUtils.property(locator, "defaultButtonId", sourceDefaultButtonId), sourceDefaultButtonId));
                copy.setDefaultButtonId(copyDefaultButtonId);
            } else {
                copy.defaultButtonId = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewForm();
    }
    
//--simple--preserve
    
    @Override
    public void writeHtml(com.mda.engine.utils.Stringcode c)
    {
    	c.write("<form method=\"post\" enctype=\"multipart/form-data\" action=\"<%= FormPostAction() %>\"");
    	if (getId() != null)
    	{
    		c.write(" id=\"{0}\"", getId());
    	}
    	if (getCss() != null)
    	{
    		c.write(" class=\"{0}\"", getCss());
    	}
    	
    	if (getAttributes() != null && getAttributes().getAttribute() != null)
    	{
    		for(MVCViewElementAttribute att : getAttributes().getAttribute())
    		{
    			c.write(" {0}=\"{1}\"", att.getName(), att.getValue());
    		}
    	}
    	c.writeLine(">");
    	
    	if (getDefaultButtonId() != null)
		{
    		c.write("<input type=\"hidden\" id=\"defaultbuttonid\" value=\"{0}\"/>", getDefaultButtonId());
		}
    	
    	//add is on request field
    	c.writeLine("<input type=\"hidden\" name=\"<%=Library.Util.CommonSettings.IS_ON_REQUEST_HIDDEN_FIELD_NAME%>\" id=\"<%=Library.Util.CommonSettings.IS_ON_REQUEST_HIDDEN_FIELD_NAME%>\" value=\"\"/>");
    	//add Post_Back_Action_Name_Hidden
    	c.writeLine("<input type=\"hidden\" name=\"<%=Library.Util.CommonSettings.POST_ACTION_NAME_HIDDEN_FIELD_NAME%>\" id=\"<%=Library.Util.CommonSettings.POST_ACTION_NAME_HIDDEN_FIELD_NAME%>\" value=\"\"/>");
    	//add Post_Back_Arguments_Hidden
    	c.writeLine("<input type=\"hidden\" name=\"<%=Library.Util.CommonSettings.POST_ARGUMENTS_HIDDEN_FIELD_NAME%>\" id=\"<%=Library.Util.CommonSettings.POST_ARGUMENTS_HIDDEN_FIELD_NAME%>\" value=\"\"/>");
    	writeHtmlChildren(c);
    	c.write("</form>");
    	c.writeLine();
    }
    
    @Override
    public String getClassName() throws Exception {

    	return "MVCViewForm";
    }

    @Override
    public void writeInitializeMethodInternal(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product) throws Exception {
    	
    }
    
//--simple--preserve

}
