
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.IMVCViewClickableElement;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewLink complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewLink">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewSpan">
 *       &lt;sequence>
 *         &lt;element name="OnClick" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCTriggerCollection" minOccurs="0"/>
 *         &lt;element name="Href" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCHrefChoice" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewLink", propOrder = {
    "onClick",
    "href"
})
@XmlRootElement
public class MVCViewLink
    extends MVCViewSpan
    implements Serializable, Cloneable, IMVCViewClickableElement, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "OnClick")
    protected MVCTriggerCollection onClick;
    @XmlElement(name = "Href")
    protected MVCHrefChoice href;

    /**
     * Gets the value of the onClick property.
     * 
     * @return
     *     possible object is
     *     {@link MVCTriggerCollection }
     *     
     */
    public MVCTriggerCollection getOnClick() {
        return onClick;
    }

    /**
     * Sets the value of the onClick property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCTriggerCollection }
     *     
     */
    public void setOnClick(MVCTriggerCollection value) {
        this.onClick = value;
    }

    /**
     * Gets the value of the href property.
     * 
     * @return
     *     possible object is
     *     {@link MVCHrefChoice }
     *     
     */
    public MVCHrefChoice getHref() {
        return href;
    }

    /**
     * Sets the value of the href property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCHrefChoice }
     *     
     */
    public void setHref(MVCHrefChoice value) {
        this.href = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewLink)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewLink that = ((MVCViewLink) object);
        {
            MVCTriggerCollection lhsOnClick;
            lhsOnClick = this.getOnClick();
            MVCTriggerCollection rhsOnClick;
            rhsOnClick = that.getOnClick();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "onClick", lhsOnClick), LocatorUtils.property(thatLocator, "onClick", rhsOnClick), lhsOnClick, rhsOnClick)) {
                return false;
            }
        }
        {
            MVCHrefChoice lhsHref;
            lhsHref = this.getHref();
            MVCHrefChoice rhsHref;
            rhsHref = that.getHref();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "href", lhsHref), LocatorUtils.property(thatLocator, "href", rhsHref), lhsHref, rhsHref)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCViewLink) {
            final MVCViewLink copy = ((MVCViewLink) draftCopy);
            if (this.onClick!= null) {
                MVCTriggerCollection sourceOnClick;
                sourceOnClick = this.getOnClick();
                MVCTriggerCollection copyOnClick = ((MVCTriggerCollection) strategy.copy(LocatorUtils.property(locator, "onClick", sourceOnClick), sourceOnClick));
                copy.setOnClick(copyOnClick);
            } else {
                copy.onClick = null;
            }
            if (this.href!= null) {
                MVCHrefChoice sourceHref;
                sourceHref = this.getHref();
                MVCHrefChoice copyHref = ((MVCHrefChoice) strategy.copy(LocatorUtils.property(locator, "href", sourceHref), sourceHref));
                copy.setHref(copyHref);
            } else {
                copy.href = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewLink();
    }
    
//--simple--preserve
    
    @Override
    public String getClassName()
    {
    	return "MVCViewLink";
    }
    
    @Override
    public void writeInitializeMethodInternal(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product)
    {
    	super.writeInitializeMethodInternal(c, product);
    	
    	String elementId = getViewElementId();
    	
    	if(this.href != null){
    		
    		c.line("{0}.OpenInPopup = {1};", elementId, this.href.openInPopup);
    		
    		if(this.href.url != null){
    			c.line("{0}.Href = new MVCUrl();", elementId);
    			c.line("{0}.Href.AreaName = \"{1}\";", elementId, this.href.url.areaName);
    			c.line("{0}.Href.ControllerName = \"{1}\";", elementId, this.href.url.controllerName);
    			c.line("{0}.Href.ActionName = \"{1}\";", elementId, this.href.url.actionName);
    			
    			if (this.href.url.arguments != null && 
    					this.href.url.arguments.getArgumentList() != null && 
    					this.href.url.arguments.getArgumentList().size() > 0) {
    	    		
    	    		c.line("{0}.Arguments = new MVCViewArgumentCollection();", elementId);
    	    		
    	    		for(TriggerArgument arg : this.href.url.arguments.getArgumentList()) {
    	    			c.line("{0}.Arguments.Add(new MVCViewArgument { Name = \"{0}\", Value = {1} });", 
    	    					arg.getArgumentName(), 
    	    					arg.getValue().getTriggerArgumentValue());
    	    		}
    	    	}
    		}
    	}    	
    }
    
    @Override
    public void writeHtml(com.mda.engine.utils.Stringcode c)
    {
    	c.write("<a");
    	if (getId() != null)
    	{
    		c.write(" id='{0}'", getId());
    	}
    	if (getCss() != null)
    	{
    		c.write(" class='{0}'", getCss());
    	}
    	
    	writeAttributes(c);
    	
    	if(this.href != null){
    		
    		if(this.href.url != null){
    			c.write(" href='<%= Request.Url.Scheme%>://<%=Request.Url.Host%><%=Request.ApplicationPath%>{0}'", this.href.url.getTriggerURL());
    		}    		
    		
    	} else if (getOnClick() != null && getOnClick().getTriggerList() != null) {
    		
    		for (MVCTrigger trigger : getOnClick().getTriggerList())
    		{
    			if (trigger instanceof com.mda.engine.models.mvc.RedirectTrigger)
    			{
    				com.mda.engine.models.mvc.RedirectTrigger redirectTrigger = (com.mda.engine.models.mvc.RedirectTrigger)trigger;
    				c.write(" href='<%= Request.Url.Scheme%>://<%=Request.Url.Host%><%=Request.ApplicationPath%>{0}'", redirectTrigger.getTriggerURL());
    			}
    			if (trigger instanceof com.mda.engine.models.mvc.OpenTabTrigger)
    			{
    				com.mda.engine.models.mvc.OpenTabTrigger openTabTrigger = (com.mda.engine.models.mvc.OpenTabTrigger)trigger;
    				c.write(" href='#{0}' data-toggle='tab' ", openTabTrigger.getTabID());
    			}
    		}
    	}
    	c.write(">");
    	c.writeLine();
    	writeHtmlTextValue(c);
    	writeHtmlIconValue(c);
    	writeHtmlChildren(c);
    	c.write("</a>");
    	c.writeLine();
    }
    
    @Override
    public String getHtmlTagName() {
		return "a";
	}
//--simple--preserve

}
