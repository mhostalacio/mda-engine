
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewRadioButton complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewRadioButton">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewInput">
 *       &lt;sequence>
 *         &lt;element name="IsChecked" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValueChoice" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewRadioButton", propOrder = {
    "isChecked"
})
public class MVCViewRadioButton
    extends MVCViewInput
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "IsChecked")
    protected MVCViewValueChoice isChecked;

    /**
     * Gets the value of the isChecked property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public MVCViewValueChoice getIsChecked() {
        return isChecked;
    }

    /**
     * Sets the value of the isChecked property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public void setIsChecked(MVCViewValueChoice value) {
        this.isChecked = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewRadioButton)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewRadioButton that = ((MVCViewRadioButton) object);
        {
            MVCViewValueChoice lhsIsChecked;
            lhsIsChecked = this.getIsChecked();
            MVCViewValueChoice rhsIsChecked;
            rhsIsChecked = that.getIsChecked();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "isChecked", lhsIsChecked), LocatorUtils.property(thatLocator, "isChecked", rhsIsChecked), lhsIsChecked, rhsIsChecked)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCViewRadioButton) {
            final MVCViewRadioButton copy = ((MVCViewRadioButton) draftCopy);
            if (this.isChecked!= null) {
                MVCViewValueChoice sourceIsChecked;
                sourceIsChecked = this.getIsChecked();
                MVCViewValueChoice copyIsChecked = ((MVCViewValueChoice) strategy.copy(LocatorUtils.property(locator, "isChecked", sourceIsChecked), sourceIsChecked));
                copy.setIsChecked(copyIsChecked);
            } else {
                copy.isChecked = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewRadioButton();
    }
    
//--simple--preserve
    
    @Override
    public String getClassName()
    {
    	if (getValue() != null)
    	{
    		return "MVCViewRadio<"+getValue().getSingleClassName() + ">";
    	}
    	return "MVCViewRadio<String>";
    }
    
    @Override
    public void writeHtml(com.mda.engine.utils.Stringcode c)
    {
    	c.write("<input type=\"radio\"");
    	if (getId() != null)
    	{
    		c.write(" id=\"{0}\"", getId());
    	}
    	if (getCss() != null)
    	{
    		c.write(" class=\"{0}\"", getCss());
    	}
    	if (getValue() != null)
    	{
    		c.write(" value=\"");
    		writeHtmlInputValue(c);
    		c.write("\"");
    	}
    	c.write("/>");
    	c.writeLine();
    }
    
    @Override
    public void writeInitializeMethodInternal(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product)
    {
    	String elementId = getViewElementId();
     	super.writeInitializeMethodInternal(c, product);
    	if (getIsChecked() != null)
    	{
    		getIsChecked().writeBind(c, elementId, "IsCheckedValue", false);
    	}
    	if (getValue() != null)
    	{
    		getValue().writeBind(c, elementId, "Bind", false);
    	}
    }
    
    @Override
    public void setBinds(MVCModel model) throws Exception
    {
    	super.setBinds(model);
    	if (getIsChecked() != null)
    	{
    		getIsChecked().setBinds(model, getContextElement(), this);
    	}
    }
//--simple--preserve

}
