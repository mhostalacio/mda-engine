
package com.mda.engine.models.mvc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewSecurityCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewSecurityCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Token" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewSecurity" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewSecurityCollection", propOrder = {
    "token"
})
public class MVCViewSecurityCollection
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Token", required = true)
    protected List<MVCViewSecurity> token;

    /**
     * Gets the value of the token property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the token property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getToken().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MVCViewSecurity }
     * 
     * 
     */
    public List<MVCViewSecurity> getToken() {
        if (token == null) {
            token = new ArrayList<MVCViewSecurity>();
        }
        return this.token;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewSecurityCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final MVCViewSecurityCollection that = ((MVCViewSecurityCollection) object);
        {
            List<MVCViewSecurity> lhsToken;
            lhsToken = this.getToken();
            List<MVCViewSecurity> rhsToken;
            rhsToken = that.getToken();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "token", lhsToken), LocatorUtils.property(thatLocator, "token", rhsToken), lhsToken, rhsToken)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof MVCViewSecurityCollection) {
            final MVCViewSecurityCollection copy = ((MVCViewSecurityCollection) draftCopy);
            if ((this.token!= null)&&(!this.token.isEmpty())) {
                List<MVCViewSecurity> sourceToken;
                sourceToken = this.getToken();
                @SuppressWarnings("unchecked")
                List<MVCViewSecurity> copyToken = ((List<MVCViewSecurity> ) strategy.copy(LocatorUtils.property(locator, "token", sourceToken), sourceToken));
                copy.token = null;
                List<MVCViewSecurity> uniqueTokenl = copy.getToken();
                uniqueTokenl.addAll(copyToken);
            } else {
                copy.token = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewSecurityCollection();
    }

}
