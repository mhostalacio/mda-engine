
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCHrefChoice complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCHrefChoice">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice minOccurs="0">
 *         &lt;element name="Url" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCUrlDefinition" minOccurs="0"/>
 *       &lt;/choice>
 *       &lt;attribute name="OpenInPopup" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCHrefChoice", propOrder = {
    "url"
})
public class MVCHrefChoice
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Url")
    protected MVCUrlDefinition url;
    @XmlAttribute(name = "OpenInPopup")
    protected Boolean openInPopup;

    /**
     * Gets the value of the url property.
     * 
     * @return
     *     possible object is
     *     {@link MVCUrlDefinition }
     *     
     */
    public MVCUrlDefinition getUrl() {
        return url;
    }

    /**
     * Sets the value of the url property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCUrlDefinition }
     *     
     */
    public void setUrl(MVCUrlDefinition value) {
        this.url = value;
    }

    /**
     * Gets the value of the openInPopup property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isOpenInPopup() {
        if (openInPopup == null) {
            return true;
        } else {
            return openInPopup;
        }
    }

    /**
     * Sets the value of the openInPopup property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOpenInPopup(Boolean value) {
        this.openInPopup = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCHrefChoice)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final MVCHrefChoice that = ((MVCHrefChoice) object);
        {
            MVCUrlDefinition lhsUrl;
            lhsUrl = this.getUrl();
            MVCUrlDefinition rhsUrl;
            rhsUrl = that.getUrl();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "url", lhsUrl), LocatorUtils.property(thatLocator, "url", rhsUrl), lhsUrl, rhsUrl)) {
                return false;
            }
        }
        {
            boolean lhsOpenInPopup;
            lhsOpenInPopup = this.isOpenInPopup();
            boolean rhsOpenInPopup;
            rhsOpenInPopup = that.isOpenInPopup();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "openInPopup", lhsOpenInPopup), LocatorUtils.property(thatLocator, "openInPopup", rhsOpenInPopup), lhsOpenInPopup, rhsOpenInPopup)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof MVCHrefChoice) {
            final MVCHrefChoice copy = ((MVCHrefChoice) draftCopy);
            if (this.url!= null) {
                MVCUrlDefinition sourceUrl;
                sourceUrl = this.getUrl();
                MVCUrlDefinition copyUrl = ((MVCUrlDefinition) strategy.copy(LocatorUtils.property(locator, "url", sourceUrl), sourceUrl));
                copy.setUrl(copyUrl);
            } else {
                copy.url = null;
            }
            if (this.openInPopup!= null) {
                boolean sourceOpenInPopup;
                sourceOpenInPopup = this.isOpenInPopup();
                boolean copyOpenInPopup = strategy.copy(LocatorUtils.property(locator, "openInPopup", sourceOpenInPopup), sourceOpenInPopup);
                copy.setOpenInPopup(copyOpenInPopup);
            } else {
                copy.openInPopup = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCHrefChoice();
    }

}
