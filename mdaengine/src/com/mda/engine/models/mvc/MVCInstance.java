
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.models.common.BOMReferenceType;
import com.mda.engine.models.common.ModelInPackageBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCInstance complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCInstance">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/common}ModelInPackageBase">
 *       &lt;sequence>
 *         &lt;element name="OwnedBy" type="{http://www.mdaengine.com/mdaengine/models/common}BOMReferenceType"/>
 *         &lt;element name="ParentMVC" type="{http://www.mdaengine.com/mdaengine/models/mvc}ParentMVCType" minOccurs="0"/>
 *         &lt;element name="Model" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCModel"/>
 *         &lt;element name="View" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCView"/>
 *         &lt;element name="Controller" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCController"/>
 *       &lt;/sequence>
 *       &lt;attribute name="PageTitle" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="GenerateHTML" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCInstance", propOrder = {
    "ownedBy",
    "parentMVC",
    "model",
    "view",
    "controller"
})
@XmlRootElement
public class MVCInstance
    extends ModelInPackageBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "OwnedBy", required = true)
    protected BOMReferenceType ownedBy;
    @XmlElement(name = "ParentMVC")
    protected ParentMVCType parentMVC;
    @XmlElement(name = "Model", required = true)
    protected MVCModel model;
    @XmlElement(name = "View", required = true)
    protected MVCView view;
    @XmlElement(name = "Controller", required = true)
    protected MVCController controller;
    @XmlAttribute(name = "PageTitle", required = true)
    protected String pageTitle;
    @XmlAttribute(name = "GenerateHTML")
    protected Boolean generateHTML;

    /**
     * Gets the value of the ownedBy property.
     * 
     * @return
     *     possible object is
     *     {@link BOMReferenceType }
     *     
     */
    public BOMReferenceType getOwnedBy() {
        return ownedBy;
    }

    /**
     * Sets the value of the ownedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link BOMReferenceType }
     *     
     */
    public void setOwnedBy(BOMReferenceType value) {
        this.ownedBy = value;
    }

    /**
     * Gets the value of the parentMVC property.
     * 
     * @return
     *     possible object is
     *     {@link ParentMVCType }
     *     
     */
    public ParentMVCType getParentMVC() {
        return parentMVC;
    }

    /**
     * Sets the value of the parentMVC property.
     * 
     * @param value
     *     allowed object is
     *     {@link ParentMVCType }
     *     
     */
    public void setParentMVC(ParentMVCType value) {
        this.parentMVC = value;
    }

    /**
     * Gets the value of the model property.
     * 
     * @return
     *     possible object is
     *     {@link MVCModel }
     *     
     */
    public MVCModel getModel() {
        return model;
    }

    /**
     * Sets the value of the model property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCModel }
     *     
     */
    public void setModel(MVCModel value) {
        this.model = value;
    }

    /**
     * Gets the value of the view property.
     * 
     * @return
     *     possible object is
     *     {@link MVCView }
     *     
     */
    public MVCView getView() {
        return view;
    }

    /**
     * Sets the value of the view property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCView }
     *     
     */
    public void setView(MVCView value) {
        this.view = value;
    }

    /**
     * Gets the value of the controller property.
     * 
     * @return
     *     possible object is
     *     {@link MVCController }
     *     
     */
    public MVCController getController() {
        return controller;
    }

    /**
     * Sets the value of the controller property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCController }
     *     
     */
    public void setController(MVCController value) {
        this.controller = value;
    }

    /**
     * Gets the value of the pageTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPageTitle() {
        return pageTitle;
    }

    /**
     * Sets the value of the pageTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPageTitle(String value) {
        this.pageTitle = value;
    }

    /**
     * Gets the value of the generateHTML property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isGenerateHTML() {
        if (generateHTML == null) {
            return true;
        } else {
            return generateHTML;
        }
    }

    /**
     * Sets the value of the generateHTML property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGenerateHTML(Boolean value) {
        this.generateHTML = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCInstance)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCInstance that = ((MVCInstance) object);
        {
            BOMReferenceType lhsOwnedBy;
            lhsOwnedBy = this.getOwnedBy();
            BOMReferenceType rhsOwnedBy;
            rhsOwnedBy = that.getOwnedBy();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ownedBy", lhsOwnedBy), LocatorUtils.property(thatLocator, "ownedBy", rhsOwnedBy), lhsOwnedBy, rhsOwnedBy)) {
                return false;
            }
        }
        {
            ParentMVCType lhsParentMVC;
            lhsParentMVC = this.getParentMVC();
            ParentMVCType rhsParentMVC;
            rhsParentMVC = that.getParentMVC();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "parentMVC", lhsParentMVC), LocatorUtils.property(thatLocator, "parentMVC", rhsParentMVC), lhsParentMVC, rhsParentMVC)) {
                return false;
            }
        }
        {
            MVCModel lhsModel;
            lhsModel = this.getModel();
            MVCModel rhsModel;
            rhsModel = that.getModel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "model", lhsModel), LocatorUtils.property(thatLocator, "model", rhsModel), lhsModel, rhsModel)) {
                return false;
            }
        }
        {
            MVCView lhsView;
            lhsView = this.getView();
            MVCView rhsView;
            rhsView = that.getView();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "view", lhsView), LocatorUtils.property(thatLocator, "view", rhsView), lhsView, rhsView)) {
                return false;
            }
        }
        {
            MVCController lhsController;
            lhsController = this.getController();
            MVCController rhsController;
            rhsController = that.getController();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "controller", lhsController), LocatorUtils.property(thatLocator, "controller", rhsController), lhsController, rhsController)) {
                return false;
            }
        }
        {
            String lhsPageTitle;
            lhsPageTitle = this.getPageTitle();
            String rhsPageTitle;
            rhsPageTitle = that.getPageTitle();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "pageTitle", lhsPageTitle), LocatorUtils.property(thatLocator, "pageTitle", rhsPageTitle), lhsPageTitle, rhsPageTitle)) {
                return false;
            }
        }
        {
            boolean lhsGenerateHTML;
            lhsGenerateHTML = this.isGenerateHTML();
            boolean rhsGenerateHTML;
            rhsGenerateHTML = that.isGenerateHTML();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "generateHTML", lhsGenerateHTML), LocatorUtils.property(thatLocator, "generateHTML", rhsGenerateHTML), lhsGenerateHTML, rhsGenerateHTML)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCInstance) {
            final MVCInstance copy = ((MVCInstance) draftCopy);
            if (this.ownedBy!= null) {
                BOMReferenceType sourceOwnedBy;
                sourceOwnedBy = this.getOwnedBy();
                BOMReferenceType copyOwnedBy = ((BOMReferenceType) strategy.copy(LocatorUtils.property(locator, "ownedBy", sourceOwnedBy), sourceOwnedBy));
                copy.setOwnedBy(copyOwnedBy);
            } else {
                copy.ownedBy = null;
            }
            if (this.parentMVC!= null) {
                ParentMVCType sourceParentMVC;
                sourceParentMVC = this.getParentMVC();
                ParentMVCType copyParentMVC = ((ParentMVCType) strategy.copy(LocatorUtils.property(locator, "parentMVC", sourceParentMVC), sourceParentMVC));
                copy.setParentMVC(copyParentMVC);
            } else {
                copy.parentMVC = null;
            }
            if (this.model!= null) {
                MVCModel sourceModel;
                sourceModel = this.getModel();
                MVCModel copyModel = ((MVCModel) strategy.copy(LocatorUtils.property(locator, "model", sourceModel), sourceModel));
                copy.setModel(copyModel);
            } else {
                copy.model = null;
            }
            if (this.view!= null) {
                MVCView sourceView;
                sourceView = this.getView();
                MVCView copyView = ((MVCView) strategy.copy(LocatorUtils.property(locator, "view", sourceView), sourceView));
                copy.setView(copyView);
            } else {
                copy.view = null;
            }
            if (this.controller!= null) {
                MVCController sourceController;
                sourceController = this.getController();
                MVCController copyController = ((MVCController) strategy.copy(LocatorUtils.property(locator, "controller", sourceController), sourceController));
                copy.setController(copyController);
            } else {
                copy.controller = null;
            }
            if (this.pageTitle!= null) {
                String sourcePageTitle;
                sourcePageTitle = this.getPageTitle();
                String copyPageTitle = ((String) strategy.copy(LocatorUtils.property(locator, "pageTitle", sourcePageTitle), sourcePageTitle));
                copy.setPageTitle(copyPageTitle);
            } else {
                copy.pageTitle = null;
            }
            if (this.generateHTML!= null) {
                boolean sourceGenerateHTML;
                sourceGenerateHTML = this.isGenerateHTML();
                boolean copyGenerateHTML = strategy.copy(LocatorUtils.property(locator, "generateHTML", sourceGenerateHTML), sourceGenerateHTML);
                copy.setGenerateHTML(copyGenerateHTML);
            } else {
                copy.generateHTML = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCInstance();
    }
    
//--simple--preserve
    
    private transient String packageName;
    private transient String modelNamespace;
    private transient String viewControllerNamespace;
    private transient com.mda.engine.models.configurationManagementModel.Application application;
    private transient com.mda.engine.models.businessTransactionModel.InitMVCBusinessTransaction initTransaction;
    private transient com.mda.engine.models.businessObjectModel.LovDefinitionType initActionsLov;

	public void setPackageName(String packageName) {
		this.packageName = com.mda.engine.utils.StringUtils.getPascalCase(packageName);
	}

	public String getPackageName() {
		return packageName;
	}

	public void setApplication(com.mda.engine.models.configurationManagementModel.Application application) {
		this.application = application;
	}

	public com.mda.engine.models.configurationManagementModel.Application getApplication() {
		return application;
	}

	public void setModelNamespace(String modelNamespace) {
		this.modelNamespace = modelNamespace;
	}

	public String getModelNamespace() {
		return modelNamespace;
	}

	public void setViewControllerNamespace(String viewControllerNamespace) {
		this.viewControllerNamespace = viewControllerNamespace;
	}

	public String getViewControllerNamespace() {
		return viewControllerNamespace;
	}
	
    public com.mda.engine.models.businessTransactionModel.InitMVCBusinessTransaction getInitTransaction() {
		return initTransaction;
	}

	public void setInitTransaction(com.mda.engine.models.businessTransactionModel.InitMVCBusinessTransaction initTransaction) {
		this.initTransaction = initTransaction;
	}
	
	public com.mda.engine.models.businessObjectModel.LovDefinitionType getInitActionsLov() {
		return initActionsLov;
	}

	public void setInitActionsLov(
			com.mda.engine.models.businessObjectModel.LovDefinitionType initActionsLov) {
		this.initActionsLov = initActionsLov;
	}
	
//--simple--preserve

}
