
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.IMVCViewChangeableElement;
import com.mda.engine.core.IMVCViewClickableElement;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewInput complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewInput">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewElement">
 *       &lt;sequence>
 *         &lt;element name="Value" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValueChoice" minOccurs="0"/>
 *         &lt;element name="IsRequired" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValueChoice" minOccurs="0"/>
 *         &lt;element name="OnClick" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCTriggerCollection" minOccurs="0"/>
 *         &lt;element name="OnChange" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCTriggerCollection" minOccurs="0"/>
 *         &lt;element name="OnKeyPress" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCTriggerCollection" minOccurs="0"/>
 *         &lt;element name="OnKeyUp" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCTriggerCollection" minOccurs="0"/>
 *         &lt;element name="OnFocus" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCTriggerCollection" minOccurs="0"/>
 *         &lt;element name="OnOutFocus" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCTriggerCollection" minOccurs="0"/>
 *         &lt;element name="OnKeyDown" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCTriggerCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="PlaceHolder" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewInput", propOrder = {
    "value",
    "isRequired",
    "onClick",
    "onChange",
    "onKeyPress",
    "onKeyUp",
    "onFocus",
    "onOutFocus",
    "onKeyDown"
})
@XmlSeeAlso({
    MVCViewButton.class,
    MVCViewRadioButton.class,
    MVCViewHidden.class,
    MVCViewNumericBox.class,
    MVCViewHtmlEditor.class,
    MVCViewDateBox.class,
    MVCViewCheckBox.class,
    MVCViewDropDown.class,
    MVCViewFile.class,
    MVCViewTextBox.class
})
public abstract class MVCViewInput
    extends MVCViewElement
    implements Serializable, Cloneable, IMVCViewChangeableElement, IMVCViewClickableElement, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Value")
    protected MVCViewValueChoice value;
    @XmlElement(name = "IsRequired")
    protected MVCViewValueChoice isRequired;
    @XmlElement(name = "OnClick")
    protected MVCTriggerCollection onClick;
    @XmlElement(name = "OnChange")
    protected MVCTriggerCollection onChange;
    @XmlElement(name = "OnKeyPress")
    protected MVCTriggerCollection onKeyPress;
    @XmlElement(name = "OnKeyUp")
    protected MVCTriggerCollection onKeyUp;
    @XmlElement(name = "OnFocus")
    protected MVCTriggerCollection onFocus;
    @XmlElement(name = "OnOutFocus")
    protected MVCTriggerCollection onOutFocus;
    @XmlElement(name = "OnKeyDown")
    protected MVCTriggerCollection onKeyDown;
    @XmlAttribute(name = "PlaceHolder")
    protected String placeHolder;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public MVCViewValueChoice getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public void setValue(MVCViewValueChoice value) {
        this.value = value;
    }

    /**
     * Gets the value of the isRequired property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public MVCViewValueChoice getIsRequired() {
        return isRequired;
    }

    /**
     * Sets the value of the isRequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public void setIsRequired(MVCViewValueChoice value) {
        this.isRequired = value;
    }

    /**
     * Gets the value of the onClick property.
     * 
     * @return
     *     possible object is
     *     {@link MVCTriggerCollection }
     *     
     */
    public MVCTriggerCollection getOnClick() {
        return onClick;
    }

    /**
     * Sets the value of the onClick property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCTriggerCollection }
     *     
     */
    public void setOnClick(MVCTriggerCollection value) {
        this.onClick = value;
    }

    /**
     * Gets the value of the onChange property.
     * 
     * @return
     *     possible object is
     *     {@link MVCTriggerCollection }
     *     
     */
    public MVCTriggerCollection getOnChange() {
        return onChange;
    }

    /**
     * Sets the value of the onChange property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCTriggerCollection }
     *     
     */
    public void setOnChange(MVCTriggerCollection value) {
        this.onChange = value;
    }

    /**
     * Gets the value of the onKeyPress property.
     * 
     * @return
     *     possible object is
     *     {@link MVCTriggerCollection }
     *     
     */
    public MVCTriggerCollection getOnKeyPress() {
        return onKeyPress;
    }

    /**
     * Sets the value of the onKeyPress property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCTriggerCollection }
     *     
     */
    public void setOnKeyPress(MVCTriggerCollection value) {
        this.onKeyPress = value;
    }

    /**
     * Gets the value of the onKeyUp property.
     * 
     * @return
     *     possible object is
     *     {@link MVCTriggerCollection }
     *     
     */
    public MVCTriggerCollection getOnKeyUp() {
        return onKeyUp;
    }

    /**
     * Sets the value of the onKeyUp property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCTriggerCollection }
     *     
     */
    public void setOnKeyUp(MVCTriggerCollection value) {
        this.onKeyUp = value;
    }

    /**
     * Gets the value of the onFocus property.
     * 
     * @return
     *     possible object is
     *     {@link MVCTriggerCollection }
     *     
     */
    public MVCTriggerCollection getOnFocus() {
        return onFocus;
    }

    /**
     * Sets the value of the onFocus property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCTriggerCollection }
     *     
     */
    public void setOnFocus(MVCTriggerCollection value) {
        this.onFocus = value;
    }

    /**
     * Gets the value of the onOutFocus property.
     * 
     * @return
     *     possible object is
     *     {@link MVCTriggerCollection }
     *     
     */
    public MVCTriggerCollection getOnOutFocus() {
        return onOutFocus;
    }

    /**
     * Sets the value of the onOutFocus property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCTriggerCollection }
     *     
     */
    public void setOnOutFocus(MVCTriggerCollection value) {
        this.onOutFocus = value;
    }

    /**
     * Gets the value of the onKeyDown property.
     * 
     * @return
     *     possible object is
     *     {@link MVCTriggerCollection }
     *     
     */
    public MVCTriggerCollection getOnKeyDown() {
        return onKeyDown;
    }

    /**
     * Sets the value of the onKeyDown property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCTriggerCollection }
     *     
     */
    public void setOnKeyDown(MVCTriggerCollection value) {
        this.onKeyDown = value;
    }

    /**
     * Gets the value of the placeHolder property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlaceHolder() {
        return placeHolder;
    }

    /**
     * Sets the value of the placeHolder property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlaceHolder(String value) {
        this.placeHolder = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewInput)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewInput that = ((MVCViewInput) object);
        {
            MVCViewValueChoice lhsValue;
            lhsValue = this.getValue();
            MVCViewValueChoice rhsValue;
            rhsValue = that.getValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "value", lhsValue), LocatorUtils.property(thatLocator, "value", rhsValue), lhsValue, rhsValue)) {
                return false;
            }
        }
        {
            MVCViewValueChoice lhsIsRequired;
            lhsIsRequired = this.getIsRequired();
            MVCViewValueChoice rhsIsRequired;
            rhsIsRequired = that.getIsRequired();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "isRequired", lhsIsRequired), LocatorUtils.property(thatLocator, "isRequired", rhsIsRequired), lhsIsRequired, rhsIsRequired)) {
                return false;
            }
        }
        {
            MVCTriggerCollection lhsOnClick;
            lhsOnClick = this.getOnClick();
            MVCTriggerCollection rhsOnClick;
            rhsOnClick = that.getOnClick();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "onClick", lhsOnClick), LocatorUtils.property(thatLocator, "onClick", rhsOnClick), lhsOnClick, rhsOnClick)) {
                return false;
            }
        }
        {
            MVCTriggerCollection lhsOnChange;
            lhsOnChange = this.getOnChange();
            MVCTriggerCollection rhsOnChange;
            rhsOnChange = that.getOnChange();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "onChange", lhsOnChange), LocatorUtils.property(thatLocator, "onChange", rhsOnChange), lhsOnChange, rhsOnChange)) {
                return false;
            }
        }
        {
            MVCTriggerCollection lhsOnKeyPress;
            lhsOnKeyPress = this.getOnKeyPress();
            MVCTriggerCollection rhsOnKeyPress;
            rhsOnKeyPress = that.getOnKeyPress();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "onKeyPress", lhsOnKeyPress), LocatorUtils.property(thatLocator, "onKeyPress", rhsOnKeyPress), lhsOnKeyPress, rhsOnKeyPress)) {
                return false;
            }
        }
        {
            MVCTriggerCollection lhsOnKeyUp;
            lhsOnKeyUp = this.getOnKeyUp();
            MVCTriggerCollection rhsOnKeyUp;
            rhsOnKeyUp = that.getOnKeyUp();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "onKeyUp", lhsOnKeyUp), LocatorUtils.property(thatLocator, "onKeyUp", rhsOnKeyUp), lhsOnKeyUp, rhsOnKeyUp)) {
                return false;
            }
        }
        {
            MVCTriggerCollection lhsOnFocus;
            lhsOnFocus = this.getOnFocus();
            MVCTriggerCollection rhsOnFocus;
            rhsOnFocus = that.getOnFocus();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "onFocus", lhsOnFocus), LocatorUtils.property(thatLocator, "onFocus", rhsOnFocus), lhsOnFocus, rhsOnFocus)) {
                return false;
            }
        }
        {
            MVCTriggerCollection lhsOnOutFocus;
            lhsOnOutFocus = this.getOnOutFocus();
            MVCTriggerCollection rhsOnOutFocus;
            rhsOnOutFocus = that.getOnOutFocus();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "onOutFocus", lhsOnOutFocus), LocatorUtils.property(thatLocator, "onOutFocus", rhsOnOutFocus), lhsOnOutFocus, rhsOnOutFocus)) {
                return false;
            }
        }
        {
            MVCTriggerCollection lhsOnKeyDown;
            lhsOnKeyDown = this.getOnKeyDown();
            MVCTriggerCollection rhsOnKeyDown;
            rhsOnKeyDown = that.getOnKeyDown();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "onKeyDown", lhsOnKeyDown), LocatorUtils.property(thatLocator, "onKeyDown", rhsOnKeyDown), lhsOnKeyDown, rhsOnKeyDown)) {
                return false;
            }
        }
        {
            String lhsPlaceHolder;
            lhsPlaceHolder = this.getPlaceHolder();
            String rhsPlaceHolder;
            rhsPlaceHolder = that.getPlaceHolder();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "placeHolder", lhsPlaceHolder), LocatorUtils.property(thatLocator, "placeHolder", rhsPlaceHolder), lhsPlaceHolder, rhsPlaceHolder)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        if (null == target) {
            throw new IllegalArgumentException("Target argument must not be null for abstract copyable classes.");
        }
        super.copyTo(locator, target, strategy);
        if (target instanceof MVCViewInput) {
            final MVCViewInput copy = ((MVCViewInput) target);
            if (this.value!= null) {
                MVCViewValueChoice sourceValue;
                sourceValue = this.getValue();
                MVCViewValueChoice copyValue = ((MVCViewValueChoice) strategy.copy(LocatorUtils.property(locator, "value", sourceValue), sourceValue));
                copy.setValue(copyValue);
            } else {
                copy.value = null;
            }
            if (this.isRequired!= null) {
                MVCViewValueChoice sourceIsRequired;
                sourceIsRequired = this.getIsRequired();
                MVCViewValueChoice copyIsRequired = ((MVCViewValueChoice) strategy.copy(LocatorUtils.property(locator, "isRequired", sourceIsRequired), sourceIsRequired));
                copy.setIsRequired(copyIsRequired);
            } else {
                copy.isRequired = null;
            }
            if (this.onClick!= null) {
                MVCTriggerCollection sourceOnClick;
                sourceOnClick = this.getOnClick();
                MVCTriggerCollection copyOnClick = ((MVCTriggerCollection) strategy.copy(LocatorUtils.property(locator, "onClick", sourceOnClick), sourceOnClick));
                copy.setOnClick(copyOnClick);
            } else {
                copy.onClick = null;
            }
            if (this.onChange!= null) {
                MVCTriggerCollection sourceOnChange;
                sourceOnChange = this.getOnChange();
                MVCTriggerCollection copyOnChange = ((MVCTriggerCollection) strategy.copy(LocatorUtils.property(locator, "onChange", sourceOnChange), sourceOnChange));
                copy.setOnChange(copyOnChange);
            } else {
                copy.onChange = null;
            }
            if (this.onKeyPress!= null) {
                MVCTriggerCollection sourceOnKeyPress;
                sourceOnKeyPress = this.getOnKeyPress();
                MVCTriggerCollection copyOnKeyPress = ((MVCTriggerCollection) strategy.copy(LocatorUtils.property(locator, "onKeyPress", sourceOnKeyPress), sourceOnKeyPress));
                copy.setOnKeyPress(copyOnKeyPress);
            } else {
                copy.onKeyPress = null;
            }
            if (this.onKeyUp!= null) {
                MVCTriggerCollection sourceOnKeyUp;
                sourceOnKeyUp = this.getOnKeyUp();
                MVCTriggerCollection copyOnKeyUp = ((MVCTriggerCollection) strategy.copy(LocatorUtils.property(locator, "onKeyUp", sourceOnKeyUp), sourceOnKeyUp));
                copy.setOnKeyUp(copyOnKeyUp);
            } else {
                copy.onKeyUp = null;
            }
            if (this.onFocus!= null) {
                MVCTriggerCollection sourceOnFocus;
                sourceOnFocus = this.getOnFocus();
                MVCTriggerCollection copyOnFocus = ((MVCTriggerCollection) strategy.copy(LocatorUtils.property(locator, "onFocus", sourceOnFocus), sourceOnFocus));
                copy.setOnFocus(copyOnFocus);
            } else {
                copy.onFocus = null;
            }
            if (this.onOutFocus!= null) {
                MVCTriggerCollection sourceOnOutFocus;
                sourceOnOutFocus = this.getOnOutFocus();
                MVCTriggerCollection copyOnOutFocus = ((MVCTriggerCollection) strategy.copy(LocatorUtils.property(locator, "onOutFocus", sourceOnOutFocus), sourceOnOutFocus));
                copy.setOnOutFocus(copyOnOutFocus);
            } else {
                copy.onOutFocus = null;
            }
            if (this.onKeyDown!= null) {
                MVCTriggerCollection sourceOnKeyDown;
                sourceOnKeyDown = this.getOnKeyDown();
                MVCTriggerCollection copyOnKeyDown = ((MVCTriggerCollection) strategy.copy(LocatorUtils.property(locator, "onKeyDown", sourceOnKeyDown), sourceOnKeyDown));
                copy.setOnKeyDown(copyOnKeyDown);
            } else {
                copy.onKeyDown = null;
            }
            if (this.placeHolder!= null) {
                String sourcePlaceHolder;
                sourcePlaceHolder = this.getPlaceHolder();
                String copyPlaceHolder = ((String) strategy.copy(LocatorUtils.property(locator, "placeHolder", sourcePlaceHolder), sourcePlaceHolder));
                copy.setPlaceHolder(copyPlaceHolder);
            } else {
                copy.placeHolder = null;
            }
        }
        return target;
    }
    
//--simple--preserve
    

    @Override
    public void writeInitializeMethodInternal(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product)
    {
    	String elementId = getViewElementId();
    	if (getValue() != null)
    	{
    		getValue().writeBind(c, elementId, "Bind", false);
    	}
    	
    	if (getValue() != null &&  getValue().getModelAttribute() != null)
    	{
    		c.line("{0}.IsRequired = delegate (Boolean setValue, bool newValue) { return Model.IsRequiredfield(\"{1}\");};",elementId , getValue().getStringBind());
    		//getIsRequired().writeBind(c, elementId, "IsRequired", false);
    	}
    	if (getPlaceHolder() != null)
    	{
    		c.line("{1}.PlaceHolder = \"{0}\";", getPlaceHolder(), elementId);
    	}
    	c.line("{0}.IsEnabled = Model.Enabled;", elementId);
    }
    
    public void writeHtmlInputValue(com.mda.engine.utils.Stringcode c) {
    	if (getValue() != null)
    	{
    		getValue().writeHtmlBind(c);
    	}
		c.writeLine();
	}
    
    @Override
    public void setBinds(MVCModel model) throws Exception
    {
    	super.setBinds(model);
    	if (getValue() != null)
    	{
    		getValue().setBinds(model, getContextElement(), this);
    	}
    	if (getIsRequired() != null)
    	{
    		getIsRequired().setBinds(model, getContextElement(), this);
    	}
    }
    
//--simple--preserve

}
