
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCControllerValidationGroupModel complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCControllerValidationGroupModel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="ViewElementId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Path" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="ValidationType" use="required" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCControllerValidationType" />
 *       &lt;attribute name="RangeMin" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="RangeMax" type="{http://www.w3.org/2001/XMLSchema}int" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCControllerValidationGroupModel")
public class MVCControllerValidationGroupModel
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "ViewElementId", required = true)
    protected String viewElementId;
    @XmlAttribute(name = "Path", required = true)
    protected String path;
    @XmlAttribute(name = "ValidationType", required = true)
    protected MVCControllerValidationType validationType;
    @XmlAttribute(name = "RangeMin")
    protected Integer rangeMin;
    @XmlAttribute(name = "RangeMax")
    protected Integer rangeMax;

    /**
     * Gets the value of the viewElementId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getViewElementId() {
        return viewElementId;
    }

    /**
     * Sets the value of the viewElementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setViewElementId(String value) {
        this.viewElementId = value;
    }

    /**
     * Gets the value of the path property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPath() {
        return path;
    }

    /**
     * Sets the value of the path property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPath(String value) {
        this.path = value;
    }

    /**
     * Gets the value of the validationType property.
     * 
     * @return
     *     possible object is
     *     {@link MVCControllerValidationType }
     *     
     */
    public MVCControllerValidationType getValidationType() {
        return validationType;
    }

    /**
     * Sets the value of the validationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCControllerValidationType }
     *     
     */
    public void setValidationType(MVCControllerValidationType value) {
        this.validationType = value;
    }

    /**
     * Gets the value of the rangeMin property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRangeMin() {
        return rangeMin;
    }

    /**
     * Sets the value of the rangeMin property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRangeMin(Integer value) {
        this.rangeMin = value;
    }

    /**
     * Gets the value of the rangeMax property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRangeMax() {
        return rangeMax;
    }

    /**
     * Sets the value of the rangeMax property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRangeMax(Integer value) {
        this.rangeMax = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCControllerValidationGroupModel)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final MVCControllerValidationGroupModel that = ((MVCControllerValidationGroupModel) object);
        {
            String lhsViewElementId;
            lhsViewElementId = this.getViewElementId();
            String rhsViewElementId;
            rhsViewElementId = that.getViewElementId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "viewElementId", lhsViewElementId), LocatorUtils.property(thatLocator, "viewElementId", rhsViewElementId), lhsViewElementId, rhsViewElementId)) {
                return false;
            }
        }
        {
            String lhsPath;
            lhsPath = this.getPath();
            String rhsPath;
            rhsPath = that.getPath();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "path", lhsPath), LocatorUtils.property(thatLocator, "path", rhsPath), lhsPath, rhsPath)) {
                return false;
            }
        }
        {
            MVCControllerValidationType lhsValidationType;
            lhsValidationType = this.getValidationType();
            MVCControllerValidationType rhsValidationType;
            rhsValidationType = that.getValidationType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "validationType", lhsValidationType), LocatorUtils.property(thatLocator, "validationType", rhsValidationType), lhsValidationType, rhsValidationType)) {
                return false;
            }
        }
        {
            Integer lhsRangeMin;
            lhsRangeMin = this.getRangeMin();
            Integer rhsRangeMin;
            rhsRangeMin = that.getRangeMin();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "rangeMin", lhsRangeMin), LocatorUtils.property(thatLocator, "rangeMin", rhsRangeMin), lhsRangeMin, rhsRangeMin)) {
                return false;
            }
        }
        {
            Integer lhsRangeMax;
            lhsRangeMax = this.getRangeMax();
            Integer rhsRangeMax;
            rhsRangeMax = that.getRangeMax();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "rangeMax", lhsRangeMax), LocatorUtils.property(thatLocator, "rangeMax", rhsRangeMax), lhsRangeMax, rhsRangeMax)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof MVCControllerValidationGroupModel) {
            final MVCControllerValidationGroupModel copy = ((MVCControllerValidationGroupModel) draftCopy);
            if (this.viewElementId!= null) {
                String sourceViewElementId;
                sourceViewElementId = this.getViewElementId();
                String copyViewElementId = ((String) strategy.copy(LocatorUtils.property(locator, "viewElementId", sourceViewElementId), sourceViewElementId));
                copy.setViewElementId(copyViewElementId);
            } else {
                copy.viewElementId = null;
            }
            if (this.path!= null) {
                String sourcePath;
                sourcePath = this.getPath();
                String copyPath = ((String) strategy.copy(LocatorUtils.property(locator, "path", sourcePath), sourcePath));
                copy.setPath(copyPath);
            } else {
                copy.path = null;
            }
            if (this.validationType!= null) {
                MVCControllerValidationType sourceValidationType;
                sourceValidationType = this.getValidationType();
                MVCControllerValidationType copyValidationType = ((MVCControllerValidationType) strategy.copy(LocatorUtils.property(locator, "validationType", sourceValidationType), sourceValidationType));
                copy.setValidationType(copyValidationType);
            } else {
                copy.validationType = null;
            }
            if (this.rangeMin!= null) {
                Integer sourceRangeMin;
                sourceRangeMin = this.getRangeMin();
                Integer copyRangeMin = ((Integer) strategy.copy(LocatorUtils.property(locator, "rangeMin", sourceRangeMin), sourceRangeMin));
                copy.setRangeMin(copyRangeMin);
            } else {
                copy.rangeMin = null;
            }
            if (this.rangeMax!= null) {
                Integer sourceRangeMax;
                sourceRangeMax = this.getRangeMax();
                Integer copyRangeMax = ((Integer) strategy.copy(LocatorUtils.property(locator, "rangeMax", sourceRangeMax), sourceRangeMax));
                copy.setRangeMax(copyRangeMax);
            } else {
                copy.rangeMax = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCControllerValidationGroupModel();
    }

}
