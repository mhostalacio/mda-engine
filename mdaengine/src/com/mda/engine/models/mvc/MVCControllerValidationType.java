
package com.mda.engine.models.mvc;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MVCControllerValidationType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MVCControllerValidationType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Required"/>
 *     &lt;enumeration value="Email"/>
 *     &lt;enumeration value="Range"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MVCControllerValidationType")
@XmlEnum
public enum MVCControllerValidationType {

    @XmlEnumValue("Required")
    REQUIRED("Required"),
    @XmlEnumValue("Email")
    EMAIL("Email"),
    @XmlEnumValue("Range")
    RANGE("Range");
    private final String value;

    MVCControllerValidationType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MVCControllerValidationType fromValue(String v) {
        for (MVCControllerValidationType c: MVCControllerValidationType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
