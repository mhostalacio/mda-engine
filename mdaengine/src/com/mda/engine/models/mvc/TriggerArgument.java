
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for TriggerArgument complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TriggerArgument">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Value" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValueChoice"/>
 *       &lt;/sequence>
 *       &lt;attribute name="ArgumentName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TriggerArgument", propOrder = {
    "value"
})
public class TriggerArgument
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Value", required = true)
    protected MVCViewValueChoice value;
    @XmlAttribute(name = "ArgumentName")
    protected String argumentName;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public MVCViewValueChoice getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public void setValue(MVCViewValueChoice value) {
        this.value = value;
    }

    /**
     * Gets the value of the argumentName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArgumentName() {
        return argumentName;
    }

    /**
     * Sets the value of the argumentName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArgumentName(String value) {
        this.argumentName = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof TriggerArgument)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final TriggerArgument that = ((TriggerArgument) object);
        {
            MVCViewValueChoice lhsValue;
            lhsValue = this.getValue();
            MVCViewValueChoice rhsValue;
            rhsValue = that.getValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "value", lhsValue), LocatorUtils.property(thatLocator, "value", rhsValue), lhsValue, rhsValue)) {
                return false;
            }
        }
        {
            String lhsArgumentName;
            lhsArgumentName = this.getArgumentName();
            String rhsArgumentName;
            rhsArgumentName = that.getArgumentName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "argumentName", lhsArgumentName), LocatorUtils.property(thatLocator, "argumentName", rhsArgumentName), lhsArgumentName, rhsArgumentName)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof TriggerArgument) {
            final TriggerArgument copy = ((TriggerArgument) draftCopy);
            if (this.value!= null) {
                MVCViewValueChoice sourceValue;
                sourceValue = this.getValue();
                MVCViewValueChoice copyValue = ((MVCViewValueChoice) strategy.copy(LocatorUtils.property(locator, "value", sourceValue), sourceValue));
                copy.setValue(copyValue);
            } else {
                copy.value = null;
            }
            if (this.argumentName!= null) {
                String sourceArgumentName;
                sourceArgumentName = this.getArgumentName();
                String copyArgumentName = ((String) strategy.copy(LocatorUtils.property(locator, "argumentName", sourceArgumentName), sourceArgumentName));
                copy.setArgumentName(copyArgumentName);
            } else {
                copy.argumentName = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new TriggerArgument();
    }

}
