
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewTableCell complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewTableCell">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewComposedElement">
 *       &lt;attribute name="Colspan" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="Rowspan" type="{http://www.w3.org/2001/XMLSchema}int" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewTableCell")
public class MVCViewTableCell
    extends MVCViewComposedElement
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "Colspan")
    protected Integer colspan;
    @XmlAttribute(name = "Rowspan")
    protected Integer rowspan;

    /**
     * Gets the value of the colspan property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getColspan() {
        return colspan;
    }

    /**
     * Sets the value of the colspan property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setColspan(Integer value) {
        this.colspan = value;
    }

    /**
     * Gets the value of the rowspan property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRowspan() {
        return rowspan;
    }

    /**
     * Sets the value of the rowspan property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRowspan(Integer value) {
        this.rowspan = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewTableCell)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewTableCell that = ((MVCViewTableCell) object);
        {
            Integer lhsColspan;
            lhsColspan = this.getColspan();
            Integer rhsColspan;
            rhsColspan = that.getColspan();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "colspan", lhsColspan), LocatorUtils.property(thatLocator, "colspan", rhsColspan), lhsColspan, rhsColspan)) {
                return false;
            }
        }
        {
            Integer lhsRowspan;
            lhsRowspan = this.getRowspan();
            Integer rhsRowspan;
            rhsRowspan = that.getRowspan();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "rowspan", lhsRowspan), LocatorUtils.property(thatLocator, "rowspan", rhsRowspan), lhsRowspan, rhsRowspan)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCViewTableCell) {
            final MVCViewTableCell copy = ((MVCViewTableCell) draftCopy);
            if (this.colspan!= null) {
                Integer sourceColspan;
                sourceColspan = this.getColspan();
                Integer copyColspan = ((Integer) strategy.copy(LocatorUtils.property(locator, "colspan", sourceColspan), sourceColspan));
                copy.setColspan(copyColspan);
            } else {
                copy.colspan = null;
            }
            if (this.rowspan!= null) {
                Integer sourceRowspan;
                sourceRowspan = this.getRowspan();
                Integer copyRowspan = ((Integer) strategy.copy(LocatorUtils.property(locator, "rowspan", sourceRowspan), sourceRowspan));
                copy.setRowspan(copyRowspan);
            } else {
                copy.rowspan = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewTableCell();
    }
    
//--simple--preserve
    
    private transient MVCViewTableRowType cellType = MVCViewTableRowType.REGULAR;    
    public MVCViewTableRowType getCellType() {
		return this.cellType;
	}

	public void setCellType(MVCViewTableRowType type) {
		this.cellType = type;
	}
	
    @Override
    public void writeHtml(com.mda.engine.utils.Stringcode c)
    {

    	if (this.cellType == MVCViewTableRowType.HEADER) {
    		c.write("<th");	
    	} else {
    		c.write("<td");
    	}
    	if (getId() != null)
    	{
    		c.write(" id=\"{0}\"", getId());
    	}
    	if (getCss() != null)
    	{
    		c.write(" class=\"{0}\"", getCss());
    	}
    	if (getColspan() != null)
    	{
    		c.write(" colspan=\"{0}\"", getColspan());
    	}
    	if (getRowspan() != null)
    	{
    		c.write(" rowspan=\"{0}\"", getRowspan());
    	}
    	c.write(">");
    	c.writeLine();
    	writeHtmlChildren(c);
    	
    	if (this.cellType == MVCViewTableRowType.HEADER) {
    		c.write("</th>");
    	} else {
    		c.write("</td>");
    	}
    	c.writeLine();
    }
    
    @Override
    public String getClassName() throws Exception
    {
    	return "MVCViewTableCell";
    }
    
    @Override
    public void writeInitializeMethodInternal(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product)
    {
    	if (getColspan() != null)
    	{
    		c.line("{0}.Colspan = delegate() { return {1}; };", this.getViewElementId(), this.getColspan());
    	}
    	
    	if (getRowspan() != null){
    		c.line("{0}.Rowspan = delegate() { return {1}; };", this.getViewElementId(), this.getRowspan());
    	}
    	
    	if (this.cellType == MVCViewTableRowType.HEADER){
    		c.line("{0}.Type = TableRowType.Header;", this.getViewElementId());
    	} else if(this.cellType == MVCViewTableRowType.FOOTER){
    		c.line("{0}.Type = TableRowType.Footer;", this.getViewElementId());
    	} else if(this.cellType == MVCViewTableRowType.REGULAR){
    		c.line("{0}.Type = TableRowType.Regular;", this.getViewElementId());
    	}
    }
//--simple--preserve

}
