
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.IMVCViewComposedElement;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewComposedElement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewComposedElement">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewElement">
 *       &lt;sequence>
 *         &lt;element name="ChildElements" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewElementCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewComposedElement", propOrder = {
    "childElements"
})
@XmlSeeAlso({
    MVCViewLI.class,
    MVCViewArticle.class,
    MVCViewFieldSet.class,
    MVCViewContentPlaceHolder.class,
    MVCViewTableCell.class,
    MVCViewNav.class,
    MVCViewAside.class,
    MVCViewForm.class,
    MVCViewHeader.class,
    MVCViewDiv.class,
    MVCViewP.class,
    MVCViewSpan.class
})
@XmlRootElement
public abstract class MVCViewComposedElement
    extends MVCViewElement
    implements Serializable, Cloneable, IMVCViewComposedElement, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "ChildElements")
    protected MVCViewElementCollection childElements;

    /**
     * Gets the value of the childElements property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewElementCollection }
     *     
     */
    public MVCViewElementCollection getChildElements() {
        return childElements;
    }

    /**
     * Sets the value of the childElements property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewElementCollection }
     *     
     */
    public void setChildElements(MVCViewElementCollection value) {
        this.childElements = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewComposedElement)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewComposedElement that = ((MVCViewComposedElement) object);
        {
            MVCViewElementCollection lhsChildElements;
            lhsChildElements = this.getChildElements();
            MVCViewElementCollection rhsChildElements;
            rhsChildElements = that.getChildElements();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "childElements", lhsChildElements), LocatorUtils.property(thatLocator, "childElements", rhsChildElements), lhsChildElements, rhsChildElements)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        if (null == target) {
            throw new IllegalArgumentException("Target argument must not be null for abstract copyable classes.");
        }
        super.copyTo(locator, target, strategy);
        if (target instanceof MVCViewComposedElement) {
            final MVCViewComposedElement copy = ((MVCViewComposedElement) target);
            if (this.childElements!= null) {
                MVCViewElementCollection sourceChildElements;
                sourceChildElements = this.getChildElements();
                MVCViewElementCollection copyChildElements = ((MVCViewElementCollection) strategy.copy(LocatorUtils.property(locator, "childElements", sourceChildElements), sourceChildElements));
                copy.setChildElements(copyChildElements);
            } else {
                copy.childElements = null;
            }
        }
        return target;
    }

}
