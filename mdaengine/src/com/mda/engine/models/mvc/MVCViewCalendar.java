
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.IMVCViewElementWithDatasource;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewCalendar complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewCalendar">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewElement">
 *       &lt;sequence>
 *         &lt;element name="DataSource" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValueChoice"/>
 *         &lt;element name="EventTitle" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValueChoice"/>
 *         &lt;element name="EventStart" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValueChoice"/>
 *         &lt;element name="EventEnd" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValueChoice" minOccurs="0"/>
 *         &lt;element name="EventUrl" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValueChoice" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewCalendar", propOrder = {
    "dataSource",
    "eventTitle",
    "eventStart",
    "eventEnd",
    "eventUrl"
})
public class MVCViewCalendar
    extends MVCViewElement
    implements Serializable, Cloneable, IMVCViewElementWithDatasource, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "DataSource", required = true)
    protected MVCViewValueChoice dataSource;
    @XmlElement(name = "EventTitle", required = true)
    protected MVCViewValueChoice eventTitle;
    @XmlElement(name = "EventStart", required = true)
    protected MVCViewValueChoice eventStart;
    @XmlElement(name = "EventEnd")
    protected MVCViewValueChoice eventEnd;
    @XmlElement(name = "EventUrl")
    protected MVCViewValueChoice eventUrl;

    /**
     * Gets the value of the dataSource property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public MVCViewValueChoice getDataSource() {
        return dataSource;
    }

    /**
     * Sets the value of the dataSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public void setDataSource(MVCViewValueChoice value) {
        this.dataSource = value;
    }

    /**
     * Gets the value of the eventTitle property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public MVCViewValueChoice getEventTitle() {
        return eventTitle;
    }

    /**
     * Sets the value of the eventTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public void setEventTitle(MVCViewValueChoice value) {
        this.eventTitle = value;
    }

    /**
     * Gets the value of the eventStart property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public MVCViewValueChoice getEventStart() {
        return eventStart;
    }

    /**
     * Sets the value of the eventStart property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public void setEventStart(MVCViewValueChoice value) {
        this.eventStart = value;
    }

    /**
     * Gets the value of the eventEnd property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public MVCViewValueChoice getEventEnd() {
        return eventEnd;
    }

    /**
     * Sets the value of the eventEnd property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public void setEventEnd(MVCViewValueChoice value) {
        this.eventEnd = value;
    }

    /**
     * Gets the value of the eventUrl property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public MVCViewValueChoice getEventUrl() {
        return eventUrl;
    }

    /**
     * Sets the value of the eventUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public void setEventUrl(MVCViewValueChoice value) {
        this.eventUrl = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewCalendar)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewCalendar that = ((MVCViewCalendar) object);
        {
            MVCViewValueChoice lhsDataSource;
            lhsDataSource = this.getDataSource();
            MVCViewValueChoice rhsDataSource;
            rhsDataSource = that.getDataSource();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dataSource", lhsDataSource), LocatorUtils.property(thatLocator, "dataSource", rhsDataSource), lhsDataSource, rhsDataSource)) {
                return false;
            }
        }
        {
            MVCViewValueChoice lhsEventTitle;
            lhsEventTitle = this.getEventTitle();
            MVCViewValueChoice rhsEventTitle;
            rhsEventTitle = that.getEventTitle();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "eventTitle", lhsEventTitle), LocatorUtils.property(thatLocator, "eventTitle", rhsEventTitle), lhsEventTitle, rhsEventTitle)) {
                return false;
            }
        }
        {
            MVCViewValueChoice lhsEventStart;
            lhsEventStart = this.getEventStart();
            MVCViewValueChoice rhsEventStart;
            rhsEventStart = that.getEventStart();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "eventStart", lhsEventStart), LocatorUtils.property(thatLocator, "eventStart", rhsEventStart), lhsEventStart, rhsEventStart)) {
                return false;
            }
        }
        {
            MVCViewValueChoice lhsEventEnd;
            lhsEventEnd = this.getEventEnd();
            MVCViewValueChoice rhsEventEnd;
            rhsEventEnd = that.getEventEnd();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "eventEnd", lhsEventEnd), LocatorUtils.property(thatLocator, "eventEnd", rhsEventEnd), lhsEventEnd, rhsEventEnd)) {
                return false;
            }
        }
        {
            MVCViewValueChoice lhsEventUrl;
            lhsEventUrl = this.getEventUrl();
            MVCViewValueChoice rhsEventUrl;
            rhsEventUrl = that.getEventUrl();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "eventUrl", lhsEventUrl), LocatorUtils.property(thatLocator, "eventUrl", rhsEventUrl), lhsEventUrl, rhsEventUrl)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCViewCalendar) {
            final MVCViewCalendar copy = ((MVCViewCalendar) draftCopy);
            if (this.dataSource!= null) {
                MVCViewValueChoice sourceDataSource;
                sourceDataSource = this.getDataSource();
                MVCViewValueChoice copyDataSource = ((MVCViewValueChoice) strategy.copy(LocatorUtils.property(locator, "dataSource", sourceDataSource), sourceDataSource));
                copy.setDataSource(copyDataSource);
            } else {
                copy.dataSource = null;
            }
            if (this.eventTitle!= null) {
                MVCViewValueChoice sourceEventTitle;
                sourceEventTitle = this.getEventTitle();
                MVCViewValueChoice copyEventTitle = ((MVCViewValueChoice) strategy.copy(LocatorUtils.property(locator, "eventTitle", sourceEventTitle), sourceEventTitle));
                copy.setEventTitle(copyEventTitle);
            } else {
                copy.eventTitle = null;
            }
            if (this.eventStart!= null) {
                MVCViewValueChoice sourceEventStart;
                sourceEventStart = this.getEventStart();
                MVCViewValueChoice copyEventStart = ((MVCViewValueChoice) strategy.copy(LocatorUtils.property(locator, "eventStart", sourceEventStart), sourceEventStart));
                copy.setEventStart(copyEventStart);
            } else {
                copy.eventStart = null;
            }
            if (this.eventEnd!= null) {
                MVCViewValueChoice sourceEventEnd;
                sourceEventEnd = this.getEventEnd();
                MVCViewValueChoice copyEventEnd = ((MVCViewValueChoice) strategy.copy(LocatorUtils.property(locator, "eventEnd", sourceEventEnd), sourceEventEnd));
                copy.setEventEnd(copyEventEnd);
            } else {
                copy.eventEnd = null;
            }
            if (this.eventUrl!= null) {
                MVCViewValueChoice sourceEventUrl;
                sourceEventUrl = this.getEventUrl();
                MVCViewValueChoice copyEventUrl = ((MVCViewValueChoice) strategy.copy(LocatorUtils.property(locator, "eventUrl", sourceEventUrl), sourceEventUrl));
                copy.setEventUrl(copyEventUrl);
            } else {
                copy.eventUrl = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewCalendar();
    }
    
//--simple--preserve
    
    @Override
    public void setBinds(MVCModel model) throws Exception
    {
    	super.setBinds(model);
    	if (getDataSource() != null)
    	{
    		getDataSource().setBinds(model, this, this);
    	}
    	if (getEventTitle() != null)
    	{
    		getEventTitle().setBinds(model, this, this);
    	}
    	if (getEventStart() != null)
    	{
    		getEventStart().setBinds(model, this, this);
    	}
    	if (getEventEnd() != null)
    	{
    		getEventEnd().setBinds(model, this, this);
    	}
    }
    
    @Override
    public String getDataSourceSingleClassName()
	{
		return this.getDataSource().getSingleClassName();
	}
    
    @Override
    public String getClassName() throws Exception
    {
    	if (getDataSource() != null)
    	{
    		return "MVCViewCalendar<" + getDataSource().getSingleClassName() + ">";
    	}
    	throw new Exception("Datasource definition is missing");
    }
    
    @Override
    public void writeInitializeMethodInternal(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product) throws Exception
    {
    	String elementId = getViewElementId();
     	String datasourceItemName = com.mda.engine.utils.StringUtils.getCamelCase(getId());
    	//super.writeInitializeMethodInternal(c, product);
    	c.line("{0}.ElementCreator = delegate({1} item, IMVCViewElement parentElement, int index) { return {2}CreateElement(item, parentElement, index); };", elementId, getDataSource().getSingleClassName(), com.mda.engine.utils.StringUtils.getPascalCase(getId()));
    }
    
    @Override
    public void writeExtraMethodsToInitialize(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product) throws Exception
    {
    	c.line();
    	String name = com.mda.engine.utils.StringUtils.getCamelCase(getId());
   
    	c.line("/// <summary>");
        c.line("/// Method to initialize the items of '{0}'", this.getId());
        c.line("/// </summary>");
     
    	c.line("protected virtual MVCViewCalendarItem {0}CreateElement({1} {2}, IMVCViewElement parent, int {2}Index)", com.mda.engine.utils.StringUtils.getPascalCase(getId()), getDataSource().getSingleClassName(), name);
    	c.line("{");
    	c.tokenAdd2BeginOfLineIndent();
    	c.line("MVCViewCalendarItem item = new MVCViewCalendarItem();");
    	if (getEventTitle() != null)
    	{
    		c.line("item.Title = {0};", getEventTitle().getStringBind());
    	}
    	if (getEventStart() != null)
    	{
    		c.line("item.Start = {0};", getEventStart().getStringBind());
    	}
    	if (getEventEnd() != null)
    	{
    		c.line("item.End = {0};", getEventEnd().getStringBind());
    	}
    	if (getEventUrl() != null)
    	{
    		c.line("item.Url = {0};", getEventUrl().getStringBind());
    	}
    	c.line("return item;");
    	c.tokenRemove2BeginOfLine();
    	c.line("}");
    	
    	c.line();
		
    }
//--simple--preserve

}
