
package com.mda.engine.models.mvc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import com.mda.engine.models.common.ModelAttribute;
import com.mda.engine.models.common.ModelAttributeBoolean;
import com.mda.engine.models.common.ModelAttributeDecimal;
import com.mda.engine.models.common.ModelAttributeEnum;
import com.mda.engine.models.common.ModelAttributeInt;
import com.mda.engine.models.common.ModelAttributeLong;
import com.mda.engine.models.common.ModelAttributeObject;
import com.mda.engine.models.common.ModelAttributeText;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ActionArgumentsCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ActionArgumentsCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded" minOccurs="0">
 *         &lt;element name="Text" type="{http://www.mdaengine.com/mdaengine/models/common}ModelAttributeText"/>
 *         &lt;element name="Boolean" type="{http://www.mdaengine.com/mdaengine/models/common}ModelAttributeBoolean"/>
 *         &lt;element name="Decimal" type="{http://www.mdaengine.com/mdaengine/models/common}ModelAttributeDecimal"/>
 *         &lt;element name="Long" type="{http://www.mdaengine.com/mdaengine/models/common}ModelAttributeLong"/>
 *         &lt;element name="Int" type="{http://www.mdaengine.com/mdaengine/models/common}ModelAttributeInt"/>
 *         &lt;element name="Object" type="{http://www.mdaengine.com/mdaengine/models/common}ModelAttributeObject"/>
 *         &lt;element name="Enum" type="{http://www.mdaengine.com/mdaengine/models/common}ModelAttributeEnum"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ActionArgumentsCollection", propOrder = {
    "argumentList"
})
public class ActionArgumentsCollection
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElements({
        @XmlElement(name = "Long", type = ModelAttributeLong.class),
        @XmlElement(name = "Text", type = ModelAttributeText.class),
        @XmlElement(name = "Boolean", type = ModelAttributeBoolean.class),
        @XmlElement(name = "Object", type = ModelAttributeObject.class),
        @XmlElement(name = "Decimal", type = ModelAttributeDecimal.class),
        @XmlElement(name = "Int", type = ModelAttributeInt.class),
        @XmlElement(name = "Enum", type = ModelAttributeEnum.class)
    })
    protected List<ModelAttribute> argumentList;

    /**
     * Gets the value of the argumentList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the argumentList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getArgumentList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ModelAttributeLong }
     * {@link ModelAttributeText }
     * {@link ModelAttributeBoolean }
     * {@link ModelAttributeObject }
     * {@link ModelAttributeDecimal }
     * {@link ModelAttributeInt }
     * {@link ModelAttributeEnum }
     * 
     * 
     */
    public List<ModelAttribute> getArgumentList() {
        if (argumentList == null) {
            argumentList = new ArrayList<ModelAttribute>();
        }
        return this.argumentList;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ActionArgumentsCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ActionArgumentsCollection that = ((ActionArgumentsCollection) object);
        {
            List<ModelAttribute> lhsArgumentList;
            lhsArgumentList = this.getArgumentList();
            List<ModelAttribute> rhsArgumentList;
            rhsArgumentList = that.getArgumentList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "argumentList", lhsArgumentList), LocatorUtils.property(thatLocator, "argumentList", rhsArgumentList), lhsArgumentList, rhsArgumentList)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof ActionArgumentsCollection) {
            final ActionArgumentsCollection copy = ((ActionArgumentsCollection) draftCopy);
            if ((this.argumentList!= null)&&(!this.argumentList.isEmpty())) {
                List<ModelAttribute> sourceArgumentList;
                sourceArgumentList = this.getArgumentList();
                @SuppressWarnings("unchecked")
                List<ModelAttribute> copyArgumentList = ((List<ModelAttribute> ) strategy.copy(LocatorUtils.property(locator, "argumentList", sourceArgumentList), sourceArgumentList));
                copy.argumentList = null;
                List<ModelAttribute> uniqueArgumentListl = copy.getArgumentList();
                uniqueArgumentListl.addAll(copyArgumentList);
            } else {
                copy.argumentList = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ActionArgumentsCollection();
    }

}
