
package com.mda.engine.models.mvc;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MVCViewRadioButtonGroupStyle.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MVCViewRadioButtonGroupStyle">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Regular"/>
 *     &lt;enumeration value="ButtonGroup"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MVCViewRadioButtonGroupStyle")
@XmlEnum
public enum MVCViewRadioButtonGroupStyle {

    @XmlEnumValue("Regular")
    REGULAR("Regular"),
    @XmlEnumValue("ButtonGroup")
    BUTTON_GROUP("ButtonGroup");
    private final String value;

    MVCViewRadioButtonGroupStyle(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MVCViewRadioButtonGroupStyle fromValue(String v) {
        for (MVCViewRadioButtonGroupStyle c: MVCViewRadioButtonGroupStyle.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
