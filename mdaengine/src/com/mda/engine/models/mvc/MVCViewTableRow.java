
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.IMVCViewComposedElement;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewTableRow complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewTableRow">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewElement">
 *       &lt;sequence>
 *         &lt;element name="Cells" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewTableCellChoice" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewTableRow", propOrder = {
    "cells"
})
@XmlRootElement
public class MVCViewTableRow
    extends MVCViewElement
    implements Serializable, Cloneable, IMVCViewComposedElement, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Cells")
    protected MVCViewTableCellChoice cells;

    /**
     * Gets the value of the cells property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewTableCellChoice }
     *     
     */
    public MVCViewTableCellChoice getCells() {
        return cells;
    }

    /**
     * Sets the value of the cells property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewTableCellChoice }
     *     
     */
    public void setCells(MVCViewTableCellChoice value) {
        this.cells = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewTableRow)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewTableRow that = ((MVCViewTableRow) object);
        {
            MVCViewTableCellChoice lhsCells;
            lhsCells = this.getCells();
            MVCViewTableCellChoice rhsCells;
            rhsCells = that.getCells();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "cells", lhsCells), LocatorUtils.property(thatLocator, "cells", rhsCells), lhsCells, rhsCells)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCViewTableRow) {
            final MVCViewTableRow copy = ((MVCViewTableRow) draftCopy);
            if (this.cells!= null) {
                MVCViewTableCellChoice sourceCells;
                sourceCells = this.getCells();
                MVCViewTableCellChoice copyCells = ((MVCViewTableCellChoice) strategy.copy(LocatorUtils.property(locator, "cells", sourceCells), sourceCells));
                copy.setCells(copyCells);
            } else {
                copy.cells = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewTableRow();
    }
    
//--simple--preserve
    private transient MVCViewElementCollection elements;
    private transient MVCViewTableRowType type = MVCViewTableRowType.REGULAR;
    
    public MVCViewTableRowType getRowType() {
		return this.type;
	}

	public void setRowType(MVCViewTableRowType type) {
		this.type = type;
	}

    @Override
    public MVCViewElementCollection getChildElements()
    {
    	if (elements == null)
    	{
    		elements = new MVCViewElementCollection();
    		if (this.getCells() != null && this.getCells().getElementList() != null)
    		{
    			for(MVCViewElement elem : this.getCells().getElementList())
    			{
    				elements.getElementList().add(elem);
    				
    				if (elem instanceof MVCViewTableCell){
    					((MVCViewTableCell)elem).setCellType(this.type);
    					
    				} else if (elem instanceof MVCViewRepeater) {
    					MVCViewRepeater rep = (MVCViewRepeater)elem;
    					
    					if (rep.getTemplate().getElementList() != null) {
    						
    		    			for (MVCViewElement child : rep.getTemplate().getElementList()) {
    		    				if (child instanceof MVCViewTableCell) {
    		    					((MVCViewTableCell)child).setCellType(this.type);
    		    				}
    		    			}
    		    		}
    				}
    				
    			}
    			
    		}
    	}
    	return elements;
    }
    
    @Override
    public void writeHtml(com.mda.engine.utils.Stringcode c)
    {
    	
    	c.write("<tr");
    	if (getId() != null)
    	{
    		c.write(" id=\"{0}\"", getId());
    	}
    	if (getCss() != null)
    	{
    		c.write(" class=\"{0}\"", getCss());
    	}
    	c.write(">");
    	c.writeLine();
    	writeHtmlChildren(c);
    	
    	c.write("</tr>");
    	c.writeLine();
    }
    
    @Override
    public String getClassName() throws Exception
    {
    	return "MVCViewTableRow";
    }
    
//    @Override
//    public void writeAddElementToChildren(com.mda.engine.utils.Stringcode c, String elementId, MVCViewElement child)
//    {
//    	if (child instanceof MVCViewRepeater)
//		{
//			MVCViewRepeater rep = (MVCViewRepeater)child;
//			c.writeLine("{0}.ChildElements.AddRange({1}.GetViewElements<MVCViewTableCell>());", elementId, rep.getViewElementId());
//		}
//		else
//		{
//			c.writeLine("{1}.Cells.Add({0});", child.getViewElementId(), elementId);
//		}
//    }
    
    @Override
    public void writeInitializeMethodInternal(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product)
    {
    	if (this.type == MVCViewTableRowType.HEADER){
    		c.line("{0}.Type = TableRowType.Header;", this.getViewElementId());
    	} else if(this.type == MVCViewTableRowType.FOOTER){
    		c.line("{0}.Type = TableRowType.Footer;", this.getViewElementId());
    	} else if(this.type == MVCViewTableRowType.REGULAR){
    		c.line("{0}.Type = TableRowType.Regular;", this.getViewElementId());
    	}
    }
    @Override
    public String getHtmlTagName() {
		return "tr";
	}
//--simple--preserve

}
