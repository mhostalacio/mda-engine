
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ITransformerSource;
import com.mda.engine.core.ModelNodeBase;
import com.mda.engine.models.common.ModelAttributeCollection;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCModel complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCModel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="Attributes" type="{http://www.mdaengine.com/mdaengine/models/common}ModelAttributeCollection" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element name="InitTransactionInputs" type="{http://www.mdaengine.com/mdaengine/models/common}ModelAttributeCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Implements" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCModel", propOrder = {
    "attributes",
    "initTransactionInputs"
})
@XmlRootElement
public class MVCModel
    extends ModelNodeBase
    implements Serializable, Cloneable, ITransformerSource, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Attributes")
    protected ModelAttributeCollection attributes;
    @XmlElement(name = "InitTransactionInputs")
    protected ModelAttributeCollection initTransactionInputs;
    @XmlAttribute(name = "Implements")
    protected String _implements;

    /**
     * Gets the value of the attributes property.
     * 
     * @return
     *     possible object is
     *     {@link ModelAttributeCollection }
     *     
     */
    public ModelAttributeCollection getAttributes() {
        return attributes;
    }

    /**
     * Sets the value of the attributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link ModelAttributeCollection }
     *     
     */
    public void setAttributes(ModelAttributeCollection value) {
        this.attributes = value;
    }

    /**
     * Gets the value of the initTransactionInputs property.
     * 
     * @return
     *     possible object is
     *     {@link ModelAttributeCollection }
     *     
     */
    public ModelAttributeCollection getInitTransactionInputs() {
        return initTransactionInputs;
    }

    /**
     * Sets the value of the initTransactionInputs property.
     * 
     * @param value
     *     allowed object is
     *     {@link ModelAttributeCollection }
     *     
     */
    public void setInitTransactionInputs(ModelAttributeCollection value) {
        this.initTransactionInputs = value;
    }

    /**
     * Gets the value of the implements property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImplements() {
        return _implements;
    }

    /**
     * Sets the value of the implements property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImplements(String value) {
        this._implements = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCModel)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final MVCModel that = ((MVCModel) object);
        {
            ModelAttributeCollection lhsAttributes;
            lhsAttributes = this.getAttributes();
            ModelAttributeCollection rhsAttributes;
            rhsAttributes = that.getAttributes();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "attributes", lhsAttributes), LocatorUtils.property(thatLocator, "attributes", rhsAttributes), lhsAttributes, rhsAttributes)) {
                return false;
            }
        }
        {
            ModelAttributeCollection lhsInitTransactionInputs;
            lhsInitTransactionInputs = this.getInitTransactionInputs();
            ModelAttributeCollection rhsInitTransactionInputs;
            rhsInitTransactionInputs = that.getInitTransactionInputs();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "initTransactionInputs", lhsInitTransactionInputs), LocatorUtils.property(thatLocator, "initTransactionInputs", rhsInitTransactionInputs), lhsInitTransactionInputs, rhsInitTransactionInputs)) {
                return false;
            }
        }
        {
            String lhsImplements;
            lhsImplements = this.getImplements();
            String rhsImplements;
            rhsImplements = that.getImplements();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "_implements", lhsImplements), LocatorUtils.property(thatLocator, "_implements", rhsImplements), lhsImplements, rhsImplements)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof MVCModel) {
            final MVCModel copy = ((MVCModel) draftCopy);
            if (this.attributes!= null) {
                ModelAttributeCollection sourceAttributes;
                sourceAttributes = this.getAttributes();
                ModelAttributeCollection copyAttributes = ((ModelAttributeCollection) strategy.copy(LocatorUtils.property(locator, "attributes", sourceAttributes), sourceAttributes));
                copy.setAttributes(copyAttributes);
            } else {
                copy.attributes = null;
            }
            if (this.initTransactionInputs!= null) {
                ModelAttributeCollection sourceInitTransactionInputs;
                sourceInitTransactionInputs = this.getInitTransactionInputs();
                ModelAttributeCollection copyInitTransactionInputs = ((ModelAttributeCollection) strategy.copy(LocatorUtils.property(locator, "initTransactionInputs", sourceInitTransactionInputs), sourceInitTransactionInputs));
                copy.setInitTransactionInputs(copyInitTransactionInputs);
            } else {
                copy.initTransactionInputs = null;
            }
            if (this._implements!= null) {
                String sourceImplements;
                sourceImplements = this.getImplements();
                String copyImplements = ((String) strategy.copy(LocatorUtils.property(locator, "_implements", sourceImplements), sourceImplements));
                copy.setImplements(copyImplements);
            } else {
                copy._implements = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCModel();
    }
    
//--simple--preserve
    
    private transient MVCInstance mvc;
    
    public com.mda.engine.models.common.ModelAttribute getModelAttributeByName(String name)
    {
    	if (getAttributes() != null && getAttributes().getAttributeList() != null)
    	{
    		for(com.mda.engine.models.common.ModelAttribute atr : getAttributes().getAttributeList())
    		{
    			if (atr.getName().equals(name))
    				return atr;
    		}
    	}
    	return null;
    }

	public MVCInstance getMvc() {
		return mvc;
	}

	public void setMvc(MVCInstance mvc) {
		this.mvc = mvc;
	}
    
//--simple--preserve

}
