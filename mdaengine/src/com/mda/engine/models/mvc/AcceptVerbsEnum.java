
package com.mda.engine.models.mvc;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AcceptVerbsEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AcceptVerbsEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Get"/>
 *     &lt;enumeration value="Post"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AcceptVerbsEnum")
@XmlEnum
public enum AcceptVerbsEnum {

    @XmlEnumValue("Get")
    GET("Get"),
    @XmlEnumValue("Post")
    POST("Post");
    private final String value;

    AcceptVerbsEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AcceptVerbsEnum fromValue(String v) {
        for (AcceptVerbsEnum c: AcceptVerbsEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
