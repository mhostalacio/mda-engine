
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.IMVCViewElementWithDatasource;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewDropDown complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewDropDown">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewInput">
 *       &lt;sequence>
 *         &lt;element name="DataSource" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValueChoice"/>
 *         &lt;element name="DefaultOptionText" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValueChoice" minOccurs="0"/>
 *         &lt;element name="HiddenValue" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValueChoice"/>
 *         &lt;element name="DisplayValue" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValueChoice"/>
 *       &lt;/sequence>
 *       &lt;attribute name="AddDefaultOption" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="MultiSelection" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewDropDown", propOrder = {
    "dataSource",
    "defaultOptionText",
    "hiddenValue",
    "displayValue"
})
@XmlSeeAlso({
    MVCViewRadioButtonGroup.class
})
public class MVCViewDropDown
    extends MVCViewInput
    implements Serializable, Cloneable, IMVCViewElementWithDatasource, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "DataSource", required = true)
    protected MVCViewValueChoice dataSource;
    @XmlElement(name = "DefaultOptionText")
    protected MVCViewValueChoice defaultOptionText;
    @XmlElement(name = "HiddenValue", required = true)
    protected MVCViewValueChoice hiddenValue;
    @XmlElement(name = "DisplayValue", required = true)
    protected MVCViewValueChoice displayValue;
    @XmlAttribute(name = "AddDefaultOption")
    protected Boolean addDefaultOption;
    @XmlAttribute(name = "MultiSelection")
    protected Boolean multiSelection;

    /**
     * Gets the value of the dataSource property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public MVCViewValueChoice getDataSource() {
        return dataSource;
    }

    /**
     * Sets the value of the dataSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public void setDataSource(MVCViewValueChoice value) {
        this.dataSource = value;
    }

    /**
     * Gets the value of the defaultOptionText property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public MVCViewValueChoice getDefaultOptionText() {
        return defaultOptionText;
    }

    /**
     * Sets the value of the defaultOptionText property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public void setDefaultOptionText(MVCViewValueChoice value) {
        this.defaultOptionText = value;
    }

    /**
     * Gets the value of the hiddenValue property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public MVCViewValueChoice getHiddenValue() {
        return hiddenValue;
    }

    /**
     * Sets the value of the hiddenValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public void setHiddenValue(MVCViewValueChoice value) {
        this.hiddenValue = value;
    }

    /**
     * Gets the value of the displayValue property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public MVCViewValueChoice getDisplayValue() {
        return displayValue;
    }

    /**
     * Sets the value of the displayValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public void setDisplayValue(MVCViewValueChoice value) {
        this.displayValue = value;
    }

    /**
     * Gets the value of the addDefaultOption property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAddDefaultOption() {
        return addDefaultOption;
    }

    /**
     * Sets the value of the addDefaultOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAddDefaultOption(Boolean value) {
        this.addDefaultOption = value;
    }

    /**
     * Gets the value of the multiSelection property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isMultiSelection() {
        if (multiSelection == null) {
            return false;
        } else {
            return multiSelection;
        }
    }

    /**
     * Sets the value of the multiSelection property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMultiSelection(Boolean value) {
        this.multiSelection = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewDropDown)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewDropDown that = ((MVCViewDropDown) object);
        {
            MVCViewValueChoice lhsDataSource;
            lhsDataSource = this.getDataSource();
            MVCViewValueChoice rhsDataSource;
            rhsDataSource = that.getDataSource();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dataSource", lhsDataSource), LocatorUtils.property(thatLocator, "dataSource", rhsDataSource), lhsDataSource, rhsDataSource)) {
                return false;
            }
        }
        {
            MVCViewValueChoice lhsDefaultOptionText;
            lhsDefaultOptionText = this.getDefaultOptionText();
            MVCViewValueChoice rhsDefaultOptionText;
            rhsDefaultOptionText = that.getDefaultOptionText();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "defaultOptionText", lhsDefaultOptionText), LocatorUtils.property(thatLocator, "defaultOptionText", rhsDefaultOptionText), lhsDefaultOptionText, rhsDefaultOptionText)) {
                return false;
            }
        }
        {
            MVCViewValueChoice lhsHiddenValue;
            lhsHiddenValue = this.getHiddenValue();
            MVCViewValueChoice rhsHiddenValue;
            rhsHiddenValue = that.getHiddenValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "hiddenValue", lhsHiddenValue), LocatorUtils.property(thatLocator, "hiddenValue", rhsHiddenValue), lhsHiddenValue, rhsHiddenValue)) {
                return false;
            }
        }
        {
            MVCViewValueChoice lhsDisplayValue;
            lhsDisplayValue = this.getDisplayValue();
            MVCViewValueChoice rhsDisplayValue;
            rhsDisplayValue = that.getDisplayValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "displayValue", lhsDisplayValue), LocatorUtils.property(thatLocator, "displayValue", rhsDisplayValue), lhsDisplayValue, rhsDisplayValue)) {
                return false;
            }
        }
        {
            Boolean lhsAddDefaultOption;
            lhsAddDefaultOption = this.isAddDefaultOption();
            Boolean rhsAddDefaultOption;
            rhsAddDefaultOption = that.isAddDefaultOption();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "addDefaultOption", lhsAddDefaultOption), LocatorUtils.property(thatLocator, "addDefaultOption", rhsAddDefaultOption), lhsAddDefaultOption, rhsAddDefaultOption)) {
                return false;
            }
        }
        {
            boolean lhsMultiSelection;
            lhsMultiSelection = this.isMultiSelection();
            boolean rhsMultiSelection;
            rhsMultiSelection = that.isMultiSelection();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "multiSelection", lhsMultiSelection), LocatorUtils.property(thatLocator, "multiSelection", rhsMultiSelection), lhsMultiSelection, rhsMultiSelection)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCViewDropDown) {
            final MVCViewDropDown copy = ((MVCViewDropDown) draftCopy);
            if (this.dataSource!= null) {
                MVCViewValueChoice sourceDataSource;
                sourceDataSource = this.getDataSource();
                MVCViewValueChoice copyDataSource = ((MVCViewValueChoice) strategy.copy(LocatorUtils.property(locator, "dataSource", sourceDataSource), sourceDataSource));
                copy.setDataSource(copyDataSource);
            } else {
                copy.dataSource = null;
            }
            if (this.defaultOptionText!= null) {
                MVCViewValueChoice sourceDefaultOptionText;
                sourceDefaultOptionText = this.getDefaultOptionText();
                MVCViewValueChoice copyDefaultOptionText = ((MVCViewValueChoice) strategy.copy(LocatorUtils.property(locator, "defaultOptionText", sourceDefaultOptionText), sourceDefaultOptionText));
                copy.setDefaultOptionText(copyDefaultOptionText);
            } else {
                copy.defaultOptionText = null;
            }
            if (this.hiddenValue!= null) {
                MVCViewValueChoice sourceHiddenValue;
                sourceHiddenValue = this.getHiddenValue();
                MVCViewValueChoice copyHiddenValue = ((MVCViewValueChoice) strategy.copy(LocatorUtils.property(locator, "hiddenValue", sourceHiddenValue), sourceHiddenValue));
                copy.setHiddenValue(copyHiddenValue);
            } else {
                copy.hiddenValue = null;
            }
            if (this.displayValue!= null) {
                MVCViewValueChoice sourceDisplayValue;
                sourceDisplayValue = this.getDisplayValue();
                MVCViewValueChoice copyDisplayValue = ((MVCViewValueChoice) strategy.copy(LocatorUtils.property(locator, "displayValue", sourceDisplayValue), sourceDisplayValue));
                copy.setDisplayValue(copyDisplayValue);
            } else {
                copy.displayValue = null;
            }
            if (this.addDefaultOption!= null) {
                Boolean sourceAddDefaultOption;
                sourceAddDefaultOption = this.isAddDefaultOption();
                Boolean copyAddDefaultOption = ((Boolean) strategy.copy(LocatorUtils.property(locator, "addDefaultOption", sourceAddDefaultOption), sourceAddDefaultOption));
                copy.setAddDefaultOption(copyAddDefaultOption);
            } else {
                copy.addDefaultOption = null;
            }
            if (this.multiSelection!= null) {
                boolean sourceMultiSelection;
                sourceMultiSelection = this.isMultiSelection();
                boolean copyMultiSelection = strategy.copy(LocatorUtils.property(locator, "multiSelection", sourceMultiSelection), sourceMultiSelection);
                copy.setMultiSelection(copyMultiSelection);
            } else {
                copy.multiSelection = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewDropDown();
    }
    
//--simple--preserve
    @Override
    public void setBinds(MVCModel model) throws Exception
    {
    	super.setBinds(model);
    	if (getDataSource() != null)
    	{
    		getDataSource().setBinds(model, this, this);
    	}
    	if (getDefaultOptionText() != null)
    	{
    		getDefaultOptionText().setBinds(model, this, this);
    	}
    	if (getHiddenValue() != null)
    	{
    		getHiddenValue().setBinds(model, this, this);
    	}
    	if (getDisplayValue() != null)
    	{
    		getDisplayValue().setBinds(model, this, this);
    	}
    }
    
    @Override
    public String getDataSourceSingleClassName()
	{
		return this.getDataSource().getSingleClassName();
	}
    
    @Override
    public String getClassName() throws Exception
    {
    	if (getDataSource() != null)
    	{
    		return "MVCViewDropDown<"+getValue().getClassName()+","+ getDataSource().getSingleClassName()+", " +getValue().getSingleClassName()+">";
    	}
    	throw new Exception("Datasource definition is missing");
    }
    
    @Override
    public void writeInitializeMethodInternal(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product)
    {
    	String elementId = getViewElementId();
     	String datasourceItemName = com.mda.engine.utils.StringUtils.getCamelCase(getId());
    	super.writeInitializeMethodInternal(c, product);
    	c.line("{0}.ElementCreator = delegate({1} item, IMVCViewElement parentElement, int index) { return {2}CreateElement(item, parentElement, index); };", elementId, getDataSource().getSingleClassName(), com.mda.engine.utils.StringUtils.getPascalCase(getId()));
    	if (getDefaultOptionText() != null)
    	{
    		getDefaultOptionText().writeBind(c, elementId, "DefaultOptionTextValue", true);
    	}
    	if (isMultiSelection())
    	{
    		c.line("{0}.MultiSelection = true;", elementId);
    	}
    	if (getHiddenValue() != null)
    	{
    		c.line("{0}.OptionHiddenValueBind = delegate({1} {3}) { return {2}; };", elementId, getDataSource().getSingleClassName(), getHiddenValue().getStringBind(), datasourceItemName);
    	}
    	if (isAddDefaultOption() != null && !isAddDefaultOption())
    	{
    		c.line("{0}.AddDefaultOption = false;", elementId);
    	}
    	if (getDisplayValue() != null)
    	{
    		c.line("{0}.OptionDisplayValueBind = delegate({1} {3}) { return Convert.ToString({2}); };", elementId, getDataSource().getSingleClassName(), getDisplayValue().getStringBind(), datasourceItemName);
    	}
    }
    
    @Override
    public void writeExtraMethodsToInitialize(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product) throws Exception
    {
    	c.line();
    	String name = com.mda.engine.utils.StringUtils.getCamelCase(getId());
   
    	c.line("/// <summary>");
        c.line("/// Method to initialize the options of '{0}'", this.getId());
        c.line("/// </summary>");
     
    	c.line("protected virtual MVCViewOption {0}CreateElement({1} {2}, IMVCViewElement parent, int {2}Index)", com.mda.engine.utils.StringUtils.getPascalCase(getId()), getDataSource().getSingleClassName(), name);
    	c.line("{");
    	c.tokenAdd2BeginOfLineIndent();
    	c.line("MVCViewOption option = new MVCViewOption();", getValue().getSingleClassName());
    	if (getValue() != null && !(this instanceof MVCViewRadioButtonGroup) && isMultiSelection())
    	{
    		getValue().writeIsCheckedBind(c, "option", "IsSelectedValue", getHiddenValue());
    	}
    	c.line("return option;");
    	c.tokenRemove2BeginOfLine();
    	c.line("}");
    	
    	c.line();
		
    }
//--simple--preserve

}
