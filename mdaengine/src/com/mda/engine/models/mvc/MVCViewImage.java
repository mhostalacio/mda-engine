
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewImage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewImage">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewElement">
 *       &lt;sequence>
 *         &lt;element name="Source" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValueChoice"/>
 *       &lt;/sequence>
 *       &lt;attribute name="ConcatAppPathToSource" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewImage", propOrder = {
    "source"
})
public class MVCViewImage
    extends MVCViewElement
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Source", required = true)
    protected MVCViewValueChoice source;
    @XmlAttribute(name = "ConcatAppPathToSource")
    protected Boolean concatAppPathToSource;

    /**
     * Gets the value of the source property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public MVCViewValueChoice getSource() {
        return source;
    }

    /**
     * Sets the value of the source property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public void setSource(MVCViewValueChoice value) {
        this.source = value;
    }

    /**
     * Gets the value of the concatAppPathToSource property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isConcatAppPathToSource() {
        if (concatAppPathToSource == null) {
            return true;
        } else {
            return concatAppPathToSource;
        }
    }

    /**
     * Sets the value of the concatAppPathToSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setConcatAppPathToSource(Boolean value) {
        this.concatAppPathToSource = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewImage)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewImage that = ((MVCViewImage) object);
        {
            MVCViewValueChoice lhsSource;
            lhsSource = this.getSource();
            MVCViewValueChoice rhsSource;
            rhsSource = that.getSource();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "source", lhsSource), LocatorUtils.property(thatLocator, "source", rhsSource), lhsSource, rhsSource)) {
                return false;
            }
        }
        {
            boolean lhsConcatAppPathToSource;
            lhsConcatAppPathToSource = this.isConcatAppPathToSource();
            boolean rhsConcatAppPathToSource;
            rhsConcatAppPathToSource = that.isConcatAppPathToSource();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "concatAppPathToSource", lhsConcatAppPathToSource), LocatorUtils.property(thatLocator, "concatAppPathToSource", rhsConcatAppPathToSource), lhsConcatAppPathToSource, rhsConcatAppPathToSource)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCViewImage) {
            final MVCViewImage copy = ((MVCViewImage) draftCopy);
            if (this.source!= null) {
                MVCViewValueChoice sourceSource;
                sourceSource = this.getSource();
                MVCViewValueChoice copySource = ((MVCViewValueChoice) strategy.copy(LocatorUtils.property(locator, "source", sourceSource), sourceSource));
                copy.setSource(copySource);
            } else {
                copy.source = null;
            }
            if (this.concatAppPathToSource!= null) {
                boolean sourceConcatAppPathToSource;
                sourceConcatAppPathToSource = this.isConcatAppPathToSource();
                boolean copyConcatAppPathToSource = strategy.copy(LocatorUtils.property(locator, "concatAppPathToSource", sourceConcatAppPathToSource), sourceConcatAppPathToSource);
                copy.setConcatAppPathToSource(copyConcatAppPathToSource);
            } else {
                copy.concatAppPathToSource = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewImage();
    }
    
//--simple--preserve
    
    @Override
    public String getClassName()
    {
    	return "MVCViewImage";
    }
    
    @Override
    public void writeInitializeMethodInternal(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product)
    {
    	String elementId = getViewElementId();
    	if (getSource() != null)
    	{
    		getSource().writeBind(c, getViewElementId(), "Bind", false);
    	}
    	if (isConcatAppPathToSource())
    	{
    		c.line("{0}.ConcatAppPathToSource = {1};", elementId, isConcatAppPathToSource());
    	}
    }
    
    @Override
    public void writeHtml(com.mda.engine.utils.Stringcode c)
    {
    	c.write("<img");
    	if (getId() != null)
    	{
    		c.write(" id=\"{0}\"", getId());
    	}
    	if (getCss() != null)
    	{
    		c.write(" class=\"{0}\"", getCss());
    	}
    	writeAttributes(c);
    	c.write(" src=\"");
    	writeHtmlTextValue(c);
    	c.write("\"");
    	c.write("/>");
    	c.writeLine();
    }

    public void writeHtmlTextValue(com.mda.engine.utils.Stringcode c) {
    	if (isConcatAppPathToSource())
    	{
    		c.write("<%= Controller.GetApplicationPath() %>");
    	}
		if (getSource() != null)
    	{
			getSource().writeHtmlBind(c);
    	}
		c.writeLine();
	}
    
    @Override
    public void setBinds(MVCModel model) throws Exception
    {
    	super.setBinds(model);
    	if (getSource() != null)
    	{
    		getSource().setBinds(model, getContextElement(), this);
    	}
    }
    
//--simple--preserve

}
