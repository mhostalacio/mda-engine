
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewDateBox complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewDateBox">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewInput">
 *       &lt;attribute name="DatePickerButtonCss" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewDateBox")
@XmlSeeAlso({
    MVCViewDateTimeBox.class
})
public class MVCViewDateBox
    extends MVCViewInput
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "DatePickerButtonCss")
    protected String datePickerButtonCss;

    /**
     * Gets the value of the datePickerButtonCss property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDatePickerButtonCss() {
        return datePickerButtonCss;
    }

    /**
     * Sets the value of the datePickerButtonCss property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDatePickerButtonCss(String value) {
        this.datePickerButtonCss = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewDateBox)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewDateBox that = ((MVCViewDateBox) object);
        {
            String lhsDatePickerButtonCss;
            lhsDatePickerButtonCss = this.getDatePickerButtonCss();
            String rhsDatePickerButtonCss;
            rhsDatePickerButtonCss = that.getDatePickerButtonCss();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "datePickerButtonCss", lhsDatePickerButtonCss), LocatorUtils.property(thatLocator, "datePickerButtonCss", rhsDatePickerButtonCss), lhsDatePickerButtonCss, rhsDatePickerButtonCss)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCViewDateBox) {
            final MVCViewDateBox copy = ((MVCViewDateBox) draftCopy);
            if (this.datePickerButtonCss!= null) {
                String sourceDatePickerButtonCss;
                sourceDatePickerButtonCss = this.getDatePickerButtonCss();
                String copyDatePickerButtonCss = ((String) strategy.copy(LocatorUtils.property(locator, "datePickerButtonCss", sourceDatePickerButtonCss), sourceDatePickerButtonCss));
                copy.setDatePickerButtonCss(copyDatePickerButtonCss);
            } else {
                copy.datePickerButtonCss = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewDateBox();
    }
    
//--simple--preserve
    
    @Override
    public String getClassName()
    {
    	return "MVCViewDateBox";
    }
    
    @Override
    public void writeInitializeMethodInternal(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product) {
    	
    	super.writeInitializeMethodInternal(c, product);
    	
    	String elementId = getViewElementId();
    	
    	if(this.getDatePickerButtonCss() != null){
    		
    		c.writeLine("{0}.DatePickerButtonCssClass = \"{1}\";", elementId, this.getDatePickerButtonCss());
    	}
    }
    
    
//--simple--preserve

}
