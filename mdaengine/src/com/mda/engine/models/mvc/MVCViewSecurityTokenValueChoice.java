
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import com.mda.engine.models.expressionsModel.EnumValueExpression;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewSecurityTokenValueChoice complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewSecurityTokenValueChoice">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="Enum" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}EnumValueExpression"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewSecurityTokenValueChoice", propOrder = {
    "_enum"
})
public class MVCViewSecurityTokenValueChoice
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Enum")
    protected EnumValueExpression _enum;

    /**
     * Gets the value of the enum property.
     * 
     * @return
     *     possible object is
     *     {@link EnumValueExpression }
     *     
     */
    public EnumValueExpression getEnum() {
        return _enum;
    }

    /**
     * Sets the value of the enum property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnumValueExpression }
     *     
     */
    public void setEnum(EnumValueExpression value) {
        this._enum = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewSecurityTokenValueChoice)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final MVCViewSecurityTokenValueChoice that = ((MVCViewSecurityTokenValueChoice) object);
        {
            EnumValueExpression lhsEnum;
            lhsEnum = this.getEnum();
            EnumValueExpression rhsEnum;
            rhsEnum = that.getEnum();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "_enum", lhsEnum), LocatorUtils.property(thatLocator, "_enum", rhsEnum), lhsEnum, rhsEnum)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof MVCViewSecurityTokenValueChoice) {
            final MVCViewSecurityTokenValueChoice copy = ((MVCViewSecurityTokenValueChoice) draftCopy);
            if (this._enum!= null) {
                EnumValueExpression sourceEnum;
                sourceEnum = this.getEnum();
                EnumValueExpression copyEnum = ((EnumValueExpression) strategy.copy(LocatorUtils.property(locator, "_enum", sourceEnum), sourceEnum));
                copy.setEnum(copyEnum);
            } else {
                copy._enum = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewSecurityTokenValueChoice();
    }
    
//--simple--preserve

    public String getCSharpExpression(com.mda.engine.core.Product product){
  		String expression = "";
  		
  		if(this._enum != null){
  			expression = this._enum.getCSharpExpression(product);
  		}
  		
  		return expression;
  	}

//--simple--preserve

}
