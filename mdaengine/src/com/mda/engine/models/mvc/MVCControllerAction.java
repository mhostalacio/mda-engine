
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCControllerAction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCControllerAction">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Arguments" type="{http://www.mdaengine.com/mdaengine/models/mvc}ActionArgumentsCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="ActionName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="IsInitAction" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="ValidationGroup" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCControllerAction", propOrder = {
    "arguments"
})
@XmlSeeAlso({
    MVCControllerGetAction.class,
    MVCControllerPostAction.class
})
public abstract class MVCControllerAction
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Arguments")
    protected ActionArgumentsCollection arguments;
    @XmlAttribute(name = "ActionName")
    protected String actionName;
    @XmlAttribute(name = "IsInitAction")
    protected Boolean isInitAction;
    @XmlAttribute(name = "Description")
    protected String description;
    @XmlAttribute(name = "ValidationGroup")
    protected String validationGroup;

    /**
     * Gets the value of the arguments property.
     * 
     * @return
     *     possible object is
     *     {@link ActionArgumentsCollection }
     *     
     */
    public ActionArgumentsCollection getArguments() {
        return arguments;
    }

    /**
     * Sets the value of the arguments property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActionArgumentsCollection }
     *     
     */
    public void setArguments(ActionArgumentsCollection value) {
        this.arguments = value;
    }

    /**
     * Gets the value of the actionName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionName() {
        return actionName;
    }

    /**
     * Sets the value of the actionName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionName(String value) {
        this.actionName = value;
    }

    /**
     * Gets the value of the isInitAction property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsInitAction() {
        return isInitAction;
    }

    /**
     * Sets the value of the isInitAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsInitAction(Boolean value) {
        this.isInitAction = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the validationGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValidationGroup() {
        return validationGroup;
    }

    /**
     * Sets the value of the validationGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValidationGroup(String value) {
        this.validationGroup = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCControllerAction)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final MVCControllerAction that = ((MVCControllerAction) object);
        {
            ActionArgumentsCollection lhsArguments;
            lhsArguments = this.getArguments();
            ActionArgumentsCollection rhsArguments;
            rhsArguments = that.getArguments();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "arguments", lhsArguments), LocatorUtils.property(thatLocator, "arguments", rhsArguments), lhsArguments, rhsArguments)) {
                return false;
            }
        }
        {
            String lhsActionName;
            lhsActionName = this.getActionName();
            String rhsActionName;
            rhsActionName = that.getActionName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "actionName", lhsActionName), LocatorUtils.property(thatLocator, "actionName", rhsActionName), lhsActionName, rhsActionName)) {
                return false;
            }
        }
        {
            Boolean lhsIsInitAction;
            lhsIsInitAction = this.isIsInitAction();
            Boolean rhsIsInitAction;
            rhsIsInitAction = that.isIsInitAction();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "isInitAction", lhsIsInitAction), LocatorUtils.property(thatLocator, "isInitAction", rhsIsInitAction), lhsIsInitAction, rhsIsInitAction)) {
                return false;
            }
        }
        {
            String lhsDescription;
            lhsDescription = this.getDescription();
            String rhsDescription;
            rhsDescription = that.getDescription();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "description", lhsDescription), LocatorUtils.property(thatLocator, "description", rhsDescription), lhsDescription, rhsDescription)) {
                return false;
            }
        }
        {
            String lhsValidationGroup;
            lhsValidationGroup = this.getValidationGroup();
            String rhsValidationGroup;
            rhsValidationGroup = that.getValidationGroup();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "validationGroup", lhsValidationGroup), LocatorUtils.property(thatLocator, "validationGroup", rhsValidationGroup), lhsValidationGroup, rhsValidationGroup)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        if (null == target) {
            throw new IllegalArgumentException("Target argument must not be null for abstract copyable classes.");
        }
        if (target instanceof MVCControllerAction) {
            final MVCControllerAction copy = ((MVCControllerAction) target);
            if (this.arguments!= null) {
                ActionArgumentsCollection sourceArguments;
                sourceArguments = this.getArguments();
                ActionArgumentsCollection copyArguments = ((ActionArgumentsCollection) strategy.copy(LocatorUtils.property(locator, "arguments", sourceArguments), sourceArguments));
                copy.setArguments(copyArguments);
            } else {
                copy.arguments = null;
            }
            if (this.actionName!= null) {
                String sourceActionName;
                sourceActionName = this.getActionName();
                String copyActionName = ((String) strategy.copy(LocatorUtils.property(locator, "actionName", sourceActionName), sourceActionName));
                copy.setActionName(copyActionName);
            } else {
                copy.actionName = null;
            }
            if (this.isInitAction!= null) {
                Boolean sourceIsInitAction;
                sourceIsInitAction = this.isIsInitAction();
                Boolean copyIsInitAction = ((Boolean) strategy.copy(LocatorUtils.property(locator, "isInitAction", sourceIsInitAction), sourceIsInitAction));
                copy.setIsInitAction(copyIsInitAction);
            } else {
                copy.isInitAction = null;
            }
            if (this.description!= null) {
                String sourceDescription;
                sourceDescription = this.getDescription();
                String copyDescription = ((String) strategy.copy(LocatorUtils.property(locator, "description", sourceDescription), sourceDescription));
                copy.setDescription(copyDescription);
            } else {
                copy.description = null;
            }
            if (this.validationGroup!= null) {
                String sourceValidationGroup;
                sourceValidationGroup = this.getValidationGroup();
                String copyValidationGroup = ((String) strategy.copy(LocatorUtils.property(locator, "validationGroup", sourceValidationGroup), sourceValidationGroup));
                copy.setValidationGroup(copyValidationGroup);
            } else {
                copy.validationGroup = null;
            }
        }
        return target;
    }
    
//--simple--preserve
    
    private transient String actionId;
    
    public void setActionId(String actionId)
    {
    	this.actionId = actionId;
    }
    
    public String getActionId()
    {
    	return this.actionId;
    }
    
    public void writeAction(com.mda.engine.utils.Stringcode c, com.mda.engine.models.mvc.MVCInstance mvc)
    {
    	com.mda.engine.models.configurationManagementModel.Application app = mvc.getApplication();
    	
    	c.line("///<summary>");
    	c.line("/// {0}", this.getDescription());
    	c.line("///</summary>");
    	if (this instanceof MVCControllerGetAction)
    	{
    		MVCControllerGetAction getAction = (MVCControllerGetAction)this;
    		if (getAction.getAcceptMethod().equals(AcceptVerbsEnum.GET))
            {
            	c.line("[AcceptVerbs(HttpVerbs.Get)]");
            }
            else if (getAction.getAcceptMethod().equals(AcceptVerbsEnum.POST))
            {
            	c.line("[AcceptVerbs(HttpVerbs.Post)]");
            }
    	}
    	else if (this instanceof MVCControllerPostAction)
    	{
    		c.line("[AcceptVerbs(HttpVerbs.Post)]");
    	}
        String parsedArguments = "String mkey";
        if (this.isIsInitAction() != null && this.isIsInitAction().booleanValue())
        {
        	parsedArguments += ", String pmkey";
        }
        if (this.getArguments() != null && this.getArguments().getArgumentList() != null)
        {
        	for (int i = 0; i<this.getArguments().getArgumentList().size(); i++)
        	{
        		com.mda.engine.models.common.ModelAttribute arg = this.getArguments().getArgumentList().get(i);
        		parsedArguments += ", ";
        		parsedArguments += (arg.getClassName() + " " + com.mda.engine.utils.StringUtils.getCamelCase(arg.getName()));
        	}
        	
        }
        if (this instanceof MVCControllerPostAction)
        {
        	parsedArguments += ", FormCollection frmData";
        }
        c.line("public ActionResult {0}({1})", this.getActionName(), parsedArguments);
        c.line("{");
        c.tokenAdd2BeginOfLineIndent();
        c.line("Guid tranID = Guid.NewGuid();");
        
        if (app.getHasTransactionsMapping())
        {
        	c.line("Library.Util.Transactions.BusinessTransaction.CurrentContext.EnlistTransaction(tranID, {0}.{1}.{2}.ToString());", app.getTransactionsMappingClassName(), app.getUniqueName(), this.getActionId());
        	c.line("if (!HasAccessToAction({0}.{1}.{2}))", app.getTransactionsMappingClassName(), app.getUniqueName(), this.getActionId());
        }
        else
        {
        	c.line("Library.Util.Transactions.BusinessTransaction.CurrentContext.EnlistTransaction(tranID, \"{0}\");", this.getActionId());
        	c.line("if (!HasAccessToAction(\"{0}\"))", this.getActionId());
        }
        
        c.line("{");
        c.line("	Library.Util.Transactions.BusinessTransaction.CurrentContext.FinishEnlistedTransaction(tranID, false, \"Access Denied\");");
        c.line("	return RedirectToAccessDenied(ControllerContext.RequestContext);");
        c.line("}");
        
        c.line("ActionResult result = new EmptyResult();");

        c.line("try");
        c.line("{");
        c.tokenAdd2BeginOfLineIndent();
        
        if (this.getArguments() != null && this.getArguments().getArgumentList() != null)
        {
        	for (com.mda.engine.models.common.ModelAttribute arg : this.getArguments().getArgumentList())
        	{
        		c.line("{0}{1} = {2};",this.getActionName(), com.mda.engine.utils.StringUtils.getPascalCase(arg.getName()), com.mda.engine.utils.StringUtils.getCamelCase(arg.getName()));
        	}
        }
        if (this.isIsInitAction()  != null && this.isIsInitAction().booleanValue())
        {
        	c.line("ParentModelKey = pmkey;");
        	c.line("initAction = {0}.{1};", mvc.getName() + "InitActions", this.actionName);
        	c.line();
        	c.line("InitModel();");
        	c.line();
        	c.line("if(this.accessDeniedAtInit)");
            c.line("{");
            c.line("	Library.Util.Transactions.BusinessTransaction.CurrentContext.FinishEnlistedTransaction(tranID, false, \"Access Denied\");");
            c.line("	return RedirectToAccessDenied(ControllerContext.RequestContext);");
            c.line("}");
        }
        else
        {
        	c.line("ModelKey = mkey;");
        	c.line("RecoverModel();");
        }
        
        if (this instanceof MVCControllerPostAction)
        {
        	c.line("UpdateModelFromPostedForm(frmData);");
        	
        	MVCControllerPostAction postAction = (MVCControllerPostAction)this;
        	if(postAction.getValidationGroup()!=null){
        		c.line("ValidateModel(\"{0}\");", postAction.getValidationGroup());
        	}
        	else{
        		c.line("ValidateModel(string.Empty);", postAction.getValidationGroup());
        	}
        	
	    	  c.line("if (Model.IsValid)");
	          c.line("{");
	          c.line("	result = {0}Internal();", this.getActionName());
	          c.line("}");
        }
        else
        {
    	   c.line("result = {0}Internal();", this.getActionName());
        }
        
        c.line("if (!Request.IsAjaxRequest() && !(result is FileContentResult))");
        c.line("{");
        c.line("    InitializeElements();");
        
        if(mvc.isGenerateHTML()){
        	c.line("	result = View(ViewName, this.GetMasterPage(), Model);");
        }else{
        	c.line("	result = View(ViewName, Model);");
        }
        
        c.line("	Library.Util.Transactions.BusinessTransaction.CurrentContext.ClearMessages();");
        c.line("}");
        c.line("Library.Util.Transactions.BusinessTransaction.CurrentContext.FinishEnlistedTransaction(tranID);");
        c.tokenRemove2BeginOfLine();
        c.line("}");
        c.line("catch(Exception e)");
        c.line("{");
        c.line("	Library.Util.Transactions.BusinessTransaction.CurrentContext.FinishEnlistedTransaction(tranID, false, e.Message + e.StackTrace);");
        c.line("	AddErrorMessageToContext(e.Message + \": \" + e.StackTrace, null);");
        c.line("	throw;");
        c.line("}");
        c.line("return result;");
        c.tokenRemove2BeginOfLine();
        c.line("}");
        c.line();
        c.line("protected virtual ActionResult {0}Internal()", this.getActionName());
        c.line("{");
        c.tokenAdd2BeginOfLineIndent();
        
        c.line("return new EmptyResult();");
        c.tokenRemove2BeginOfLine();
        c.line("}");
        c.line();
    }

    
//--simple--preserve

}
