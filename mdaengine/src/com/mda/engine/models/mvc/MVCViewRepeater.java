
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.IMVCViewElementWithDatasource;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewRepeater complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewRepeater">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewElement">
 *       &lt;sequence>
 *         &lt;element name="DataSource" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValueChoice" minOccurs="0"/>
 *         &lt;element name="Template" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewRepeaterTemplate"/>
 *       &lt;/sequence>
 *       &lt;attribute name="RenderTagName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewRepeater", propOrder = {
    "dataSource",
    "template"
})
public class MVCViewRepeater
    extends MVCViewElement
    implements Serializable, Cloneable, IMVCViewElementWithDatasource, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "DataSource")
    protected MVCViewValueChoice dataSource;
    @XmlElement(name = "Template", required = true)
    protected MVCViewRepeaterTemplate template;
    @XmlAttribute(name = "RenderTagName", required = true)
    protected String renderTagName;

    /**
     * Gets the value of the dataSource property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public MVCViewValueChoice getDataSource() {
        return dataSource;
    }

    /**
     * Sets the value of the dataSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public void setDataSource(MVCViewValueChoice value) {
        this.dataSource = value;
    }

    /**
     * Gets the value of the template property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewRepeaterTemplate }
     *     
     */
    public MVCViewRepeaterTemplate getTemplate() {
        return template;
    }

    /**
     * Sets the value of the template property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewRepeaterTemplate }
     *     
     */
    public void setTemplate(MVCViewRepeaterTemplate value) {
        this.template = value;
    }

    /**
     * Gets the value of the renderTagName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRenderTagName() {
        return renderTagName;
    }

    /**
     * Sets the value of the renderTagName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRenderTagName(String value) {
        this.renderTagName = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewRepeater)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewRepeater that = ((MVCViewRepeater) object);
        {
            MVCViewValueChoice lhsDataSource;
            lhsDataSource = this.getDataSource();
            MVCViewValueChoice rhsDataSource;
            rhsDataSource = that.getDataSource();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dataSource", lhsDataSource), LocatorUtils.property(thatLocator, "dataSource", rhsDataSource), lhsDataSource, rhsDataSource)) {
                return false;
            }
        }
        {
            MVCViewRepeaterTemplate lhsTemplate;
            lhsTemplate = this.getTemplate();
            MVCViewRepeaterTemplate rhsTemplate;
            rhsTemplate = that.getTemplate();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "template", lhsTemplate), LocatorUtils.property(thatLocator, "template", rhsTemplate), lhsTemplate, rhsTemplate)) {
                return false;
            }
        }
        {
            String lhsRenderTagName;
            lhsRenderTagName = this.getRenderTagName();
            String rhsRenderTagName;
            rhsRenderTagName = that.getRenderTagName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "renderTagName", lhsRenderTagName), LocatorUtils.property(thatLocator, "renderTagName", rhsRenderTagName), lhsRenderTagName, rhsRenderTagName)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCViewRepeater) {
            final MVCViewRepeater copy = ((MVCViewRepeater) draftCopy);
            if (this.dataSource!= null) {
                MVCViewValueChoice sourceDataSource;
                sourceDataSource = this.getDataSource();
                MVCViewValueChoice copyDataSource = ((MVCViewValueChoice) strategy.copy(LocatorUtils.property(locator, "dataSource", sourceDataSource), sourceDataSource));
                copy.setDataSource(copyDataSource);
            } else {
                copy.dataSource = null;
            }
            if (this.template!= null) {
                MVCViewRepeaterTemplate sourceTemplate;
                sourceTemplate = this.getTemplate();
                MVCViewRepeaterTemplate copyTemplate = ((MVCViewRepeaterTemplate) strategy.copy(LocatorUtils.property(locator, "template", sourceTemplate), sourceTemplate));
                copy.setTemplate(copyTemplate);
            } else {
                copy.template = null;
            }
            if (this.renderTagName!= null) {
                String sourceRenderTagName;
                sourceRenderTagName = this.getRenderTagName();
                String copyRenderTagName = ((String) strategy.copy(LocatorUtils.property(locator, "renderTagName", sourceRenderTagName), sourceRenderTagName));
                copy.setRenderTagName(copyRenderTagName);
            } else {
                copy.renderTagName = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewRepeater();
    }
    
//--simple--preserve

    @Override
    public void writeInitializeMethodInternal(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product) throws Exception
    {
    	String elementId = getViewElementId();
    	String name = com.mda.engine.utils.StringUtils.getCamelCase(getId()) + "Parent";
    	if (getTemplate() != null)
    	{
    		c.line("{0}.RenderTagName = \"{1}\";", elementId, this.getRenderTagName());
    		java.util.HashMap<String, String> args = parseInitializeArgs("IMVCViewDatasourcedElement " + name, "Repeater");
        	String extraArguments = args.get("extraArguments");
        	args = parseInitializeArgsCall(name);
        	String extraArgumentsCall = args.get("extraArguments");
    		c.line("{0}.ElementCreator = delegate({1}) { return {2}CreateElement({3}); };", elementId, extraArguments, com.mda.engine.utils.StringUtils.getPascalCase(getId()), extraArgumentsCall);
    	}
    }
    
    private java.util.HashMap<String, String> parseInitializeArgs(String extraArguments, String suffix)
    {
    	//add current
    	String name = com.mda.engine.utils.StringUtils.getCamelCase(this.getId()) + suffix;
		extraArguments += String.format(", %s %s, int %sIndex", this.getDataSourceSingleClassName(), name, name);

		//add parents
//		com.mda.engine.core.IMVCViewElementWithDatasource ctx = this.getContextElement();
//		while (ctx != null)
//		{
//			name = com.mda.engine.utils.StringUtils.getCamelCase(ctx.getId()) + suffix;
//			extraArguments += String.format(", %s %s, int %sIndex", ctx.getDataSourceSingleClassName(), name, name);
//			ctx = ctx.getContextElement();
//		}
		
    	java.util.HashMap<String, String> args = new java.util.HashMap<String, String>();
    	args.put("extraArguments", extraArguments);
    	return args;
    }
    
    private java.util.HashMap<String, String> parseInitializeArgsCall(String extraArguments)
    {
    	java.util.HashMap<String, String> args = new java.util.HashMap<String, String>();
    	//add current
    	String name = com.mda.engine.utils.StringUtils.getCamelCase(this.getId()) + "Repeater";
		extraArguments += String.format(", %s, %sIndex", name, name);
    	
    	args.put("extraArguments", extraArguments);
    	return args;
    }
    

    
    @Override
    public String getClassName() throws Exception
    {
    	if (getDataSource() != null)
    	{
    		return "MVCViewRepeater<"+getDataSource().getSingleClassName()+">";
    	}
    	throw new Exception("Datasource definition is missing");
    }
    
    @Override
    public void setBinds(MVCModel model) throws Exception
    {
    	super.setBinds(model);
    	if (getDataSource() != null)
    	{
    		getDataSource().setBinds(model, this, this);
    	}
    	if (getTemplate() != null)
    	{
    		if (getTemplate().getElementList() != null)
    		{
    			for (MVCViewElement child : getTemplate().getElementList())
    			{
    				child.setContextElement(this);
    				child.setBinds(model);
    			}
    		}
    	}
    }
    
    @Override
    public void writeExtraMethodsToInitialize(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product) throws Exception
    {
    	c.line();
    	String name = com.mda.engine.utils.StringUtils.getCamelCase(getId()) + "Parent";
    	java.util.HashMap<String, String> args = parseInitializeArgs("IMVCViewDatasourcedElement " + name, "");
    	String extraArguments = args.get("extraArguments");

    	c.line("/// <summary>");
        c.line("/// Method to initialize the elements of '{0}'", this.getId());
        c.line("/// </summary>");
    	c.line("protected virtual IList<IMVCViewElement> {0}CreateElement({1})", com.mda.engine.utils.StringUtils.getPascalCase(getId()), extraArguments);
    	c.line("{");
    	c.tokenAdd2BeginOfLineIndent();
    	c.line("List<IMVCViewElement> elementList = new List<IMVCViewElement>();");
    	for (MVCViewElement elem : this.getTemplate().getElementList())
		{
    		c.writeLine();
        	c.write("{0} {1} = ",elem.getClassName(), elem.getViewElementId());
        	elem.writeInitializeMethodCall(c, name, true, product, true);
        	c.writeLine("");
        	c.writeLine("{0}.ContextElement = {1};", elem.getViewElementId(), name);
        	c.writeLine("elementList.Add({0});", elem.getViewElementId());
		}
    	c.writeLine("return elementList;");
    	c.tokenRemove2BeginOfLine();
    	c.line("}");
    	
    	c.line();
		for (MVCViewElement elem : this.getTemplate().getElementList())
		{
			c.line("/// <summary>");
	        c.line("/// Method to initialize the element '{0}'", elem.getId());
	        c.line("/// </summary>");
			elem.writeInitializeMethod(c, product);
			c.line();

		}
    }
    
    public String getDataSourceSingleClassName()
	{
    	return getDataSource().getSingleClassName();
	}
//--simple--preserve

}
