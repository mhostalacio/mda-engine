
package com.mda.engine.models.mvc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCControllerActionCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCControllerActionCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded" minOccurs="0">
 *         &lt;element name="Get" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCControllerGetAction" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Post" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCControllerPostAction" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCControllerActionCollection", propOrder = {
    "actionList"
})
public class MVCControllerActionCollection
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElements({
        @XmlElement(name = "Post", type = MVCControllerPostAction.class),
        @XmlElement(name = "Get", type = MVCControllerGetAction.class)
    })
    protected List<MVCControllerAction> actionList;

    /**
     * Gets the value of the actionList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the actionList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getActionList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MVCControllerPostAction }
     * {@link MVCControllerGetAction }
     * 
     * 
     */
    public List<MVCControllerAction> getActionList() {
        if (actionList == null) {
            actionList = new ArrayList<MVCControllerAction>();
        }
        return this.actionList;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCControllerActionCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final MVCControllerActionCollection that = ((MVCControllerActionCollection) object);
        {
            List<MVCControllerAction> lhsActionList;
            lhsActionList = this.getActionList();
            List<MVCControllerAction> rhsActionList;
            rhsActionList = that.getActionList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "actionList", lhsActionList), LocatorUtils.property(thatLocator, "actionList", rhsActionList), lhsActionList, rhsActionList)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof MVCControllerActionCollection) {
            final MVCControllerActionCollection copy = ((MVCControllerActionCollection) draftCopy);
            if ((this.actionList!= null)&&(!this.actionList.isEmpty())) {
                List<MVCControllerAction> sourceActionList;
                sourceActionList = this.getActionList();
                @SuppressWarnings("unchecked")
                List<MVCControllerAction> copyActionList = ((List<MVCControllerAction> ) strategy.copy(LocatorUtils.property(locator, "actionList", sourceActionList), sourceActionList));
                copy.actionList = null;
                List<MVCControllerAction> uniqueActionListl = copy.getActionList();
                uniqueActionListl.addAll(copyActionList);
            } else {
                copy.actionList = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCControllerActionCollection();
    }
    
//--simple--preserve
    
    public MVCControllerAction getActionByName(String name) {
    	for(MVCControllerAction element : getActionList())
		{
    		if (element.getActionName() != null && element.getActionName().equals(name))
    		{
    			return element;
    		}
		}
    	return null;
	}
    
//--simple--preserve

}
