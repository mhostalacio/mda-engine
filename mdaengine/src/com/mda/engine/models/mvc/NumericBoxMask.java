
package com.mda.engine.models.mvc;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NumericBoxMask.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="NumericBoxMask">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="INTEGER"/>
 *     &lt;enumeration value="MONEY"/>
 *     &lt;enumeration value="DECIMAL"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "NumericBoxMask")
@XmlEnum
public enum NumericBoxMask {

    INTEGER,
    MONEY,
    DECIMAL;

    public String value() {
        return name();
    }

    public static NumericBoxMask fromValue(String v) {
        return valueOf(v);
    }

}
