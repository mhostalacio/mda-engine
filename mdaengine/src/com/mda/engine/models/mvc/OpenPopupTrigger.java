
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for OpenPopupTrigger complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OpenPopupTrigger">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCTrigger">
 *       &lt;attribute name="PopupMvcName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="PopupWidth" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="PopupHeight" type="{http://www.w3.org/2001/XMLSchema}int" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpenPopupTrigger")
public class OpenPopupTrigger
    extends MVCTrigger
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "PopupMvcName", required = true)
    protected String popupMvcName;
    @XmlAttribute(name = "PopupWidth")
    protected Integer popupWidth;
    @XmlAttribute(name = "PopupHeight")
    protected Integer popupHeight;

    /**
     * Gets the value of the popupMvcName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPopupMvcName() {
        return popupMvcName;
    }

    /**
     * Sets the value of the popupMvcName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPopupMvcName(String value) {
        this.popupMvcName = value;
    }

    /**
     * Gets the value of the popupWidth property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPopupWidth() {
        return popupWidth;
    }

    /**
     * Sets the value of the popupWidth property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPopupWidth(Integer value) {
        this.popupWidth = value;
    }

    /**
     * Gets the value of the popupHeight property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPopupHeight() {
        return popupHeight;
    }

    /**
     * Sets the value of the popupHeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPopupHeight(Integer value) {
        this.popupHeight = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof OpenPopupTrigger)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final OpenPopupTrigger that = ((OpenPopupTrigger) object);
        {
            String lhsPopupMvcName;
            lhsPopupMvcName = this.getPopupMvcName();
            String rhsPopupMvcName;
            rhsPopupMvcName = that.getPopupMvcName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "popupMvcName", lhsPopupMvcName), LocatorUtils.property(thatLocator, "popupMvcName", rhsPopupMvcName), lhsPopupMvcName, rhsPopupMvcName)) {
                return false;
            }
        }
        {
            Integer lhsPopupWidth;
            lhsPopupWidth = this.getPopupWidth();
            Integer rhsPopupWidth;
            rhsPopupWidth = that.getPopupWidth();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "popupWidth", lhsPopupWidth), LocatorUtils.property(thatLocator, "popupWidth", rhsPopupWidth), lhsPopupWidth, rhsPopupWidth)) {
                return false;
            }
        }
        {
            Integer lhsPopupHeight;
            lhsPopupHeight = this.getPopupHeight();
            Integer rhsPopupHeight;
            rhsPopupHeight = that.getPopupHeight();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "popupHeight", lhsPopupHeight), LocatorUtils.property(thatLocator, "popupHeight", rhsPopupHeight), lhsPopupHeight, rhsPopupHeight)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof OpenPopupTrigger) {
            final OpenPopupTrigger copy = ((OpenPopupTrigger) draftCopy);
            if (this.popupMvcName!= null) {
                String sourcePopupMvcName;
                sourcePopupMvcName = this.getPopupMvcName();
                String copyPopupMvcName = ((String) strategy.copy(LocatorUtils.property(locator, "popupMvcName", sourcePopupMvcName), sourcePopupMvcName));
                copy.setPopupMvcName(copyPopupMvcName);
            } else {
                copy.popupMvcName = null;
            }
            if (this.popupWidth!= null) {
                Integer sourcePopupWidth;
                sourcePopupWidth = this.getPopupWidth();
                Integer copyPopupWidth = ((Integer) strategy.copy(LocatorUtils.property(locator, "popupWidth", sourcePopupWidth), sourcePopupWidth));
                copy.setPopupWidth(copyPopupWidth);
            } else {
                copy.popupWidth = null;
            }
            if (this.popupHeight!= null) {
                Integer sourcePopupHeight;
                sourcePopupHeight = this.getPopupHeight();
                Integer copyPopupHeight = ((Integer) strategy.copy(LocatorUtils.property(locator, "popupHeight", sourcePopupHeight), sourcePopupHeight));
                copy.setPopupHeight(copyPopupHeight);
            } else {
                copy.popupHeight = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new OpenPopupTrigger();
    }
    
//--simple--preserve

    @Override
    protected String getActionCodeName()
    {
    	return "MVCViewElementOpenPopupViewTrigger";
    }
    
//--simple--preserve

}
