
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewLabel complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewLabel">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewSpan">
 *       &lt;attribute name="For" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewLabel")
public class MVCViewLabel
    extends MVCViewSpan
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "For")
    protected String _for;

    /**
     * Gets the value of the for property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFor() {
        return _for;
    }

    /**
     * Sets the value of the for property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFor(String value) {
        this._for = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewLabel)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewLabel that = ((MVCViewLabel) object);
        {
            String lhsFor;
            lhsFor = this.getFor();
            String rhsFor;
            rhsFor = that.getFor();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "_for", lhsFor), LocatorUtils.property(thatLocator, "_for", rhsFor), lhsFor, rhsFor)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCViewLabel) {
            final MVCViewLabel copy = ((MVCViewLabel) draftCopy);
            if (this._for!= null) {
                String sourceFor;
                sourceFor = this.getFor();
                String copyFor = ((String) strategy.copy(LocatorUtils.property(locator, "_for", sourceFor), sourceFor));
                copy.setFor(copyFor);
            } else {
                copy._for = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewLabel();
    }
    
//--simple--preserve
    
    private transient MVCViewElement forElement;
    
    @Override
    public String getClassName()
    {
    	if (getText() != null)
    	{
    		return "MVCViewLabel<"+getText().getSingleClassName() + ">";
    	}
    	return "MVCViewLabel<String>";
    }
    
    @Override
    public void writeInitializeMethodInternal(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product)
    {
    	super.writeInitializeMethodInternal(c, product);
    	if (getForElement() != null)
    	{
    		c.line("{1}.For = delegate() { return {0}; };", getForElement().getViewElementId(), getViewElementId());
    	}
    }
    
    @Override
    public void writeHtml(com.mda.engine.utils.Stringcode c)
    {
    	c.write("<label");
    	if (getId() != null)
    	{
    		c.write(" id=\"{0}\"", getId());
    	}
    	if (getCss() != null)
    	{
    		c.write(" class=\"{0}\"", getCss());
    	}
    	if (getFor() != null)
    	{
    		c.write(" for=\"{0}\"", getFor());
    	}
    	c.write(">");
    	writeHtmlTextValue(c);
    	c.write("</label>");
    	c.writeLine();
    }

	public void setForElement(MVCViewElement forElement) {
		this.forElement = forElement;
	}

	public MVCViewElement getForElement() {
		return forElement;
	}
    
//--simple--preserve

}
