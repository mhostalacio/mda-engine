
package com.mda.engine.models.mvc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewTableRowChoice complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewTableRowChoice">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded" minOccurs="0">
 *         &lt;element name="Repeater" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewRepeater" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Row" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewTableRow" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ElementReference" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewElementReference" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewTableRowChoice", propOrder = {
    "elementList"
})
public class MVCViewTableRowChoice
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElements({
        @XmlElement(name = "ElementReference", type = MVCViewElementReference.class),
        @XmlElement(name = "Row", type = MVCViewTableRow.class),
        @XmlElement(name = "Repeater", type = MVCViewRepeater.class)
    })
    protected List<MVCViewElement> elementList;

    /**
     * Gets the value of the elementList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elementList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getElementList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MVCViewElementReference }
     * {@link MVCViewTableRow }
     * {@link MVCViewRepeater }
     * 
     * 
     */
    public List<MVCViewElement> getElementList() {
        if (elementList == null) {
            elementList = new ArrayList<MVCViewElement>();
        }
        return this.elementList;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewTableRowChoice)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final MVCViewTableRowChoice that = ((MVCViewTableRowChoice) object);
        {
            List<MVCViewElement> lhsElementList;
            lhsElementList = this.getElementList();
            List<MVCViewElement> rhsElementList;
            rhsElementList = that.getElementList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "elementList", lhsElementList), LocatorUtils.property(thatLocator, "elementList", rhsElementList), lhsElementList, rhsElementList)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof MVCViewTableRowChoice) {
            final MVCViewTableRowChoice copy = ((MVCViewTableRowChoice) draftCopy);
            if ((this.elementList!= null)&&(!this.elementList.isEmpty())) {
                List<MVCViewElement> sourceElementList;
                sourceElementList = this.getElementList();
                @SuppressWarnings("unchecked")
                List<MVCViewElement> copyElementList = ((List<MVCViewElement> ) strategy.copy(LocatorUtils.property(locator, "elementList", sourceElementList), sourceElementList));
                copy.elementList = null;
                List<MVCViewElement> uniqueElementListl = copy.getElementList();
                uniqueElementListl.addAll(copyElementList);
            } else {
                copy.elementList = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewTableRowChoice();
    }

}
