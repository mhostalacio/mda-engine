
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewP complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewP">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewComposedElement">
 *       &lt;sequence>
 *         &lt;element name="Text" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValueChoice" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewP", propOrder = {
    "text"
})
public class MVCViewP
    extends MVCViewComposedElement
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Text")
    protected MVCViewValueChoice text;

    /**
     * Gets the value of the text property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public MVCViewValueChoice getText() {
        return text;
    }

    /**
     * Sets the value of the text property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public void setText(MVCViewValueChoice value) {
        this.text = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewP)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewP that = ((MVCViewP) object);
        {
            MVCViewValueChoice lhsText;
            lhsText = this.getText();
            MVCViewValueChoice rhsText;
            rhsText = that.getText();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "text", lhsText), LocatorUtils.property(thatLocator, "text", rhsText), lhsText, rhsText)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCViewP) {
            final MVCViewP copy = ((MVCViewP) draftCopy);
            if (this.text!= null) {
                MVCViewValueChoice sourceText;
                sourceText = this.getText();
                MVCViewValueChoice copyText = ((MVCViewValueChoice) strategy.copy(LocatorUtils.property(locator, "text", sourceText), sourceText));
                copy.setText(copyText);
            } else {
                copy.text = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewP();
    }
    
//--simple--preserve
    
    @Override
    public String getClassName()
    {
    	if (getText() != null)
    	{
    		return "MVCViewP<"+getText().getSingleClassName() + ">";
    	}
    	return "MVCViewP<String>";
    }
    
    @Override
    public void writeHtml(com.mda.engine.utils.Stringcode c)
    {
    	c.write("<p");
    	if (getId() != null)
    	{
    		c.write(" id=\"{0}\"", getId());
    	}
    	if (getCss() != null)
    	{
    		c.write(" class=\"{0}\"", getCss());
    	}
    	writeAttributes(c);
    	c.write(">");
    	writeHtmlTextValue(c);
    	if(getChildElements() != null && getChildElements().getElementList() != null)
    	{
    		for(MVCViewElement elem : getChildElements().getElementList())
    		{
    			elem.writeHtml(c);
    		}
    	}
    	c.write("</p>");
    	c.writeLine();
    }

    public void writeHtmlTextValue(com.mda.engine.utils.Stringcode c) {
		if (getText() != null)
    	{
			getText().writeHtmlBind(c);
    	}
		c.writeLine();
	}

	
    @Override
    public void writeInitializeMethodInternal(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product)
    {
    	if (getText() != null)
    	{
    		getText().writeBind(c, getViewElementId(), "Bind", false);
    	}
    }
    
    @Override
    public void setBinds(MVCModel model) throws Exception
    {
    	super.setBinds(model);
    	if (getText() != null)
    	{
    		getText().setBinds(model, getContextElement(), this);
    	}
    }
//--simple--preserve

}
