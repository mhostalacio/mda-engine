
package com.mda.engine.models.mvc;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MVCViewRadioButtonGroupDisplay.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MVCViewRadioButtonGroupDisplay">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Horizontal"/>
 *     &lt;enumeration value="Vertical"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MVCViewRadioButtonGroupDisplay")
@XmlEnum
public enum MVCViewRadioButtonGroupDisplay {

    @XmlEnumValue("Horizontal")
    HORIZONTAL("Horizontal"),
    @XmlEnumValue("Vertical")
    VERTICAL("Vertical");
    private final String value;

    MVCViewRadioButtonGroupDisplay(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MVCViewRadioButtonGroupDisplay fromValue(String v) {
        for (MVCViewRadioButtonGroupDisplay c: MVCViewRadioButtonGroupDisplay.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
