
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewContentPlaceHolder complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewContentPlaceHolder">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewComposedElement">
 *       &lt;attribute name="ContentPlaceHolderID" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewContentPlaceHolder")
public class MVCViewContentPlaceHolder
    extends MVCViewComposedElement
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "ContentPlaceHolderID", required = true)
    protected String contentPlaceHolderID;

    /**
     * Gets the value of the contentPlaceHolderID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContentPlaceHolderID() {
        return contentPlaceHolderID;
    }

    /**
     * Sets the value of the contentPlaceHolderID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContentPlaceHolderID(String value) {
        this.contentPlaceHolderID = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewContentPlaceHolder)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewContentPlaceHolder that = ((MVCViewContentPlaceHolder) object);
        {
            String lhsContentPlaceHolderID;
            lhsContentPlaceHolderID = this.getContentPlaceHolderID();
            String rhsContentPlaceHolderID;
            rhsContentPlaceHolderID = that.getContentPlaceHolderID();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "contentPlaceHolderID", lhsContentPlaceHolderID), LocatorUtils.property(thatLocator, "contentPlaceHolderID", rhsContentPlaceHolderID), lhsContentPlaceHolderID, rhsContentPlaceHolderID)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCViewContentPlaceHolder) {
            final MVCViewContentPlaceHolder copy = ((MVCViewContentPlaceHolder) draftCopy);
            if (this.contentPlaceHolderID!= null) {
                String sourceContentPlaceHolderID;
                sourceContentPlaceHolderID = this.getContentPlaceHolderID();
                String copyContentPlaceHolderID = ((String) strategy.copy(LocatorUtils.property(locator, "contentPlaceHolderID", sourceContentPlaceHolderID), sourceContentPlaceHolderID));
                copy.setContentPlaceHolderID(copyContentPlaceHolderID);
            } else {
                copy.contentPlaceHolderID = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewContentPlaceHolder();
    }
    
//--simple--preserve
    
    @Override
    public void writeHtml(com.mda.engine.utils.Stringcode c)
    {
    	c.write("<asp:Content runat=\"server\"");
    	if (getId() != null)
    	{
    		c.write(" ID=\"{0}\"", getId());
    	}
    	if (getContentPlaceHolderID() != null)
    	{
    		c.write(" ContentPlaceHolderID=\"{0}\"", getContentPlaceHolderID());
    	}
    	if (getCss() != null)
    	{
    		c.write(" CssClass=\"{0}\"", getCss());
    	}
    	c.write(">");
    	c.writeLine();
    	writeHtmlChildren(c);
    	c.write("</asp:Content>");
    	c.writeLine();
    }
    
//--simple--preserve

}
