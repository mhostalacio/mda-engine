
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.IMVCViewElementWithDatasource;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewRadioButtonGroup complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewRadioButtonGroup">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewDropDown">
 *       &lt;sequence>
 *         &lt;element name="GroupName" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValueChoice" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Display" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewRadioButtonGroupDisplay" default="Vertical" />
 *       &lt;attribute name="Style" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewRadioButtonGroupStyle" default="Regular" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewRadioButtonGroup", propOrder = {
    "groupName"
})
public class MVCViewRadioButtonGroup
    extends MVCViewDropDown
    implements Serializable, Cloneable, IMVCViewElementWithDatasource, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "GroupName")
    protected MVCViewValueChoice groupName;
    @XmlAttribute(name = "Display")
    protected MVCViewRadioButtonGroupDisplay display;
    @XmlAttribute(name = "Style")
    protected MVCViewRadioButtonGroupStyle style;

    /**
     * Gets the value of the groupName property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public MVCViewValueChoice getGroupName() {
        return groupName;
    }

    /**
     * Sets the value of the groupName property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public void setGroupName(MVCViewValueChoice value) {
        this.groupName = value;
    }

    /**
     * Gets the value of the display property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewRadioButtonGroupDisplay }
     *     
     */
    public MVCViewRadioButtonGroupDisplay getDisplay() {
        if (display == null) {
            return MVCViewRadioButtonGroupDisplay.VERTICAL;
        } else {
            return display;
        }
    }

    /**
     * Sets the value of the display property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewRadioButtonGroupDisplay }
     *     
     */
    public void setDisplay(MVCViewRadioButtonGroupDisplay value) {
        this.display = value;
    }

    /**
     * Gets the value of the style property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewRadioButtonGroupStyle }
     *     
     */
    public MVCViewRadioButtonGroupStyle getStyle() {
        if (style == null) {
            return MVCViewRadioButtonGroupStyle.REGULAR;
        } else {
            return style;
        }
    }

    /**
     * Sets the value of the style property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewRadioButtonGroupStyle }
     *     
     */
    public void setStyle(MVCViewRadioButtonGroupStyle value) {
        this.style = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewRadioButtonGroup)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewRadioButtonGroup that = ((MVCViewRadioButtonGroup) object);
        {
            MVCViewValueChoice lhsGroupName;
            lhsGroupName = this.getGroupName();
            MVCViewValueChoice rhsGroupName;
            rhsGroupName = that.getGroupName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "groupName", lhsGroupName), LocatorUtils.property(thatLocator, "groupName", rhsGroupName), lhsGroupName, rhsGroupName)) {
                return false;
            }
        }
        {
            MVCViewRadioButtonGroupDisplay lhsDisplay;
            lhsDisplay = this.getDisplay();
            MVCViewRadioButtonGroupDisplay rhsDisplay;
            rhsDisplay = that.getDisplay();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "display", lhsDisplay), LocatorUtils.property(thatLocator, "display", rhsDisplay), lhsDisplay, rhsDisplay)) {
                return false;
            }
        }
        {
            MVCViewRadioButtonGroupStyle lhsStyle;
            lhsStyle = this.getStyle();
            MVCViewRadioButtonGroupStyle rhsStyle;
            rhsStyle = that.getStyle();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "style", lhsStyle), LocatorUtils.property(thatLocator, "style", rhsStyle), lhsStyle, rhsStyle)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCViewRadioButtonGroup) {
            final MVCViewRadioButtonGroup copy = ((MVCViewRadioButtonGroup) draftCopy);
            if (this.groupName!= null) {
                MVCViewValueChoice sourceGroupName;
                sourceGroupName = this.getGroupName();
                MVCViewValueChoice copyGroupName = ((MVCViewValueChoice) strategy.copy(LocatorUtils.property(locator, "groupName", sourceGroupName), sourceGroupName));
                copy.setGroupName(copyGroupName);
            } else {
                copy.groupName = null;
            }
            if (this.display!= null) {
                MVCViewRadioButtonGroupDisplay sourceDisplay;
                sourceDisplay = this.getDisplay();
                MVCViewRadioButtonGroupDisplay copyDisplay = ((MVCViewRadioButtonGroupDisplay) strategy.copy(LocatorUtils.property(locator, "display", sourceDisplay), sourceDisplay));
                copy.setDisplay(copyDisplay);
            } else {
                copy.display = null;
            }
            if (this.style!= null) {
                MVCViewRadioButtonGroupStyle sourceStyle;
                sourceStyle = this.getStyle();
                MVCViewRadioButtonGroupStyle copyStyle = ((MVCViewRadioButtonGroupStyle) strategy.copy(LocatorUtils.property(locator, "style", sourceStyle), sourceStyle));
                copy.setStyle(copyStyle);
            } else {
                copy.style = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewRadioButtonGroup();
    }
    
//--simple--preserve
    @Override
    public void setBinds(MVCModel model) throws Exception
    {
    	super.setBinds(model);
    	if (getDataSource() != null)
    	{
    		getDataSource().setBinds(model, this, this);
    	}
    	if (getValue() != null)
    	{
    		getValue().setBinds(model, getContextElement(), this);
    	}
    	if (getIsRequired() != null)
    	{
    		getIsRequired().setBinds(model, getContextElement(), this);
    	}
    	if (getDefaultOptionText() != null)
    	{
    		getDefaultOptionText().setBinds(model, this, this);
    	}
    	if (getHiddenValue() != null)
    	{
    		getHiddenValue().setBinds(model, this, this);
    	}
    	if (getDisplayValue() != null)
    	{
    		getDisplayValue().setBinds(model, this, this);
    	}
    }
    
    @Override
    public String getDataSourceSingleClassName()
	{
		return this.getDataSource().getSingleClassName();
	}
    
    @Override
    public String getClassName() throws Exception
    {
    	if (getDataSource() != null && getValue() != null)
    	{
    		return "MVCViewRadioButtonGroup<"+getValue().getClassName()+","+ getDataSource().getSingleClassName()+", " +getValue().getSingleClassName()+">";
    	}
    	else
    	{
    		return "MVCViewRadioButtonGroup<String, String, String>";
    	}
    }
    
    @Override
    public void writeInitializeMethodInternal(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product)
    {
    	super.writeInitializeMethodInternal(c, product);
    	
    	String elementId = this.getViewElementId();
    	
    	c.line("{1}.Display = MVCViewElementDisplay.{0};", this.getDisplay().value(), elementId);
    	c.line("{1}.RenderStyle = MVCViewRadioButtonRenderStyle.{0};", this.getStyle().value(), elementId);
    }
    
    @Override
    public void writeExtraMethodsToInitialize(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product) throws Exception
    {
    	c.line();
    	String name = com.mda.engine.utils.StringUtils.getCamelCase(getId());
    	c.line("/// <summary>");
        c.line("/// Method to initialize the options of '{0}'", this.getId());
        c.line("/// </summary>");
    	c.line("protected virtual IMVCViewElement {0}CreateElement({1} {2}, IMVCViewElement parent, int {2}Index)", com.mda.engine.utils.StringUtils.getPascalCase(getId()), getDataSource().getSingleClassName(), name);
    	c.line("{");
    	c.tokenAdd2BeginOfLineIndent();
    	if (this.isMultiSelection())
    	{
    		c.line("MVCViewCheckBox<{0}> option = new MVCViewCheckBox<{0}>();", getValue().getSingleClassName());
    	}
    	else
    	{
    		c.line("MVCViewRadio<{0}> option = new MVCViewRadio<{0}>();", getValue().getSingleClassName());
    	}
    	c.line("return option;");
    	c.tokenRemove2BeginOfLine();
    	c.line("}");
    	
    	c.line();
		
    }
//--simple--preserve

}
