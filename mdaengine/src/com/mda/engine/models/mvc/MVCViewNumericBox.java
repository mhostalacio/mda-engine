
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewNumericBox complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewNumericBox">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewInput">
 *       &lt;attribute name="Mask" use="required" type="{http://www.mdaengine.com/mdaengine/models/mvc}NumericBoxMask" />
 *       &lt;attribute name="NumberOfDecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="MaxLength" type="{http://www.w3.org/2001/XMLSchema}int" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewNumericBox")
public class MVCViewNumericBox
    extends MVCViewInput
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "Mask", required = true)
    protected NumericBoxMask mask;
    @XmlAttribute(name = "NumberOfDecimalPlaces")
    protected Integer numberOfDecimalPlaces;
    @XmlAttribute(name = "MaxLength")
    protected Integer maxLength;

    /**
     * Gets the value of the mask property.
     * 
     * @return
     *     possible object is
     *     {@link NumericBoxMask }
     *     
     */
    public NumericBoxMask getMask() {
        return mask;
    }

    /**
     * Sets the value of the mask property.
     * 
     * @param value
     *     allowed object is
     *     {@link NumericBoxMask }
     *     
     */
    public void setMask(NumericBoxMask value) {
        this.mask = value;
    }

    /**
     * Gets the value of the numberOfDecimalPlaces property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfDecimalPlaces() {
        return numberOfDecimalPlaces;
    }

    /**
     * Sets the value of the numberOfDecimalPlaces property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfDecimalPlaces(Integer value) {
        this.numberOfDecimalPlaces = value;
    }

    /**
     * Gets the value of the maxLength property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxLength() {
        return maxLength;
    }

    /**
     * Sets the value of the maxLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxLength(Integer value) {
        this.maxLength = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewNumericBox)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewNumericBox that = ((MVCViewNumericBox) object);
        {
            NumericBoxMask lhsMask;
            lhsMask = this.getMask();
            NumericBoxMask rhsMask;
            rhsMask = that.getMask();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "mask", lhsMask), LocatorUtils.property(thatLocator, "mask", rhsMask), lhsMask, rhsMask)) {
                return false;
            }
        }
        {
            Integer lhsNumberOfDecimalPlaces;
            lhsNumberOfDecimalPlaces = this.getNumberOfDecimalPlaces();
            Integer rhsNumberOfDecimalPlaces;
            rhsNumberOfDecimalPlaces = that.getNumberOfDecimalPlaces();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "numberOfDecimalPlaces", lhsNumberOfDecimalPlaces), LocatorUtils.property(thatLocator, "numberOfDecimalPlaces", rhsNumberOfDecimalPlaces), lhsNumberOfDecimalPlaces, rhsNumberOfDecimalPlaces)) {
                return false;
            }
        }
        {
            Integer lhsMaxLength;
            lhsMaxLength = this.getMaxLength();
            Integer rhsMaxLength;
            rhsMaxLength = that.getMaxLength();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "maxLength", lhsMaxLength), LocatorUtils.property(thatLocator, "maxLength", rhsMaxLength), lhsMaxLength, rhsMaxLength)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCViewNumericBox) {
            final MVCViewNumericBox copy = ((MVCViewNumericBox) draftCopy);
            if (this.mask!= null) {
                NumericBoxMask sourceMask;
                sourceMask = this.getMask();
                NumericBoxMask copyMask = ((NumericBoxMask) strategy.copy(LocatorUtils.property(locator, "mask", sourceMask), sourceMask));
                copy.setMask(copyMask);
            } else {
                copy.mask = null;
            }
            if (this.numberOfDecimalPlaces!= null) {
                Integer sourceNumberOfDecimalPlaces;
                sourceNumberOfDecimalPlaces = this.getNumberOfDecimalPlaces();
                Integer copyNumberOfDecimalPlaces = ((Integer) strategy.copy(LocatorUtils.property(locator, "numberOfDecimalPlaces", sourceNumberOfDecimalPlaces), sourceNumberOfDecimalPlaces));
                copy.setNumberOfDecimalPlaces(copyNumberOfDecimalPlaces);
            } else {
                copy.numberOfDecimalPlaces = null;
            }
            if (this.maxLength!= null) {
                Integer sourceMaxLength;
                sourceMaxLength = this.getMaxLength();
                Integer copyMaxLength = ((Integer) strategy.copy(LocatorUtils.property(locator, "maxLength", sourceMaxLength), sourceMaxLength));
                copy.setMaxLength(copyMaxLength);
            } else {
                copy.maxLength = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewNumericBox();
    }
    
//--simple--preserve
    
    @Override
    public String getClassName()
    {
    	return "MVCViewNumericBox<" + getValue().getClassName() + ">";
    }
    
    @Override
    public void writeHtml(com.mda.engine.utils.Stringcode c)
    {
    	c.write("<input type=\"text\"");
    	if (getId() != null)
    	{
    		c.write(" id=\"{0}\"", getId());
    	}
    	if (getCss() != null)
    	{
    		c.write(" class=\"{0}\"", getCss());
    	}
    	if (getValue() != null)
    	{
    		c.write(" value=\"");
    		writeHtmlInputValue(c);
    		c.write("\"");
    	}
    	c.write("/>");
    	c.writeLine();
    }
    
    @Override
    public void writeInitializeMethodInternal(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product)
    {
    	super.writeInitializeMethodInternal(c, product);
    	c.line("{0}.Mask = NumericBoxMask.{1};", getViewElementId(), getMask().value());
    	if (getMaxLength() != null)
    	{
    		c.line("{0}.MaxLength = {1};", getViewElementId(), getMaxLength());
    	}
    	if (getNumberOfDecimalPlaces() != null)
    	{
    		c.line("{0}.NumberOfDecimalPlaces = {1};", getViewElementId(), getNumberOfDecimalPlaces());
    	}
    }
    
//--simple--preserve

}
