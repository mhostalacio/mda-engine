
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for DropSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DropSettings">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Accepts" type="{http://www.mdaengine.com/mdaengine/models/mvc}AcceptDropSettings"/>
 *         &lt;element name="OnItemDropped" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCTriggerCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DropSettings", propOrder = {
    "accepts",
    "onItemDropped"
})
public class DropSettings
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Accepts", required = true)
    protected AcceptDropSettings accepts;
    @XmlElement(name = "OnItemDropped")
    protected MVCTriggerCollection onItemDropped;

    /**
     * Gets the value of the accepts property.
     * 
     * @return
     *     possible object is
     *     {@link AcceptDropSettings }
     *     
     */
    public AcceptDropSettings getAccepts() {
        return accepts;
    }

    /**
     * Sets the value of the accepts property.
     * 
     * @param value
     *     allowed object is
     *     {@link AcceptDropSettings }
     *     
     */
    public void setAccepts(AcceptDropSettings value) {
        this.accepts = value;
    }

    /**
     * Gets the value of the onItemDropped property.
     * 
     * @return
     *     possible object is
     *     {@link MVCTriggerCollection }
     *     
     */
    public MVCTriggerCollection getOnItemDropped() {
        return onItemDropped;
    }

    /**
     * Sets the value of the onItemDropped property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCTriggerCollection }
     *     
     */
    public void setOnItemDropped(MVCTriggerCollection value) {
        this.onItemDropped = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof DropSettings)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final DropSettings that = ((DropSettings) object);
        {
            AcceptDropSettings lhsAccepts;
            lhsAccepts = this.getAccepts();
            AcceptDropSettings rhsAccepts;
            rhsAccepts = that.getAccepts();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "accepts", lhsAccepts), LocatorUtils.property(thatLocator, "accepts", rhsAccepts), lhsAccepts, rhsAccepts)) {
                return false;
            }
        }
        {
            MVCTriggerCollection lhsOnItemDropped;
            lhsOnItemDropped = this.getOnItemDropped();
            MVCTriggerCollection rhsOnItemDropped;
            rhsOnItemDropped = that.getOnItemDropped();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "onItemDropped", lhsOnItemDropped), LocatorUtils.property(thatLocator, "onItemDropped", rhsOnItemDropped), lhsOnItemDropped, rhsOnItemDropped)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof DropSettings) {
            final DropSettings copy = ((DropSettings) draftCopy);
            if (this.accepts!= null) {
                AcceptDropSettings sourceAccepts;
                sourceAccepts = this.getAccepts();
                AcceptDropSettings copyAccepts = ((AcceptDropSettings) strategy.copy(LocatorUtils.property(locator, "accepts", sourceAccepts), sourceAccepts));
                copy.setAccepts(copyAccepts);
            } else {
                copy.accepts = null;
            }
            if (this.onItemDropped!= null) {
                MVCTriggerCollection sourceOnItemDropped;
                sourceOnItemDropped = this.getOnItemDropped();
                MVCTriggerCollection copyOnItemDropped = ((MVCTriggerCollection) strategy.copy(LocatorUtils.property(locator, "onItemDropped", sourceOnItemDropped), sourceOnItemDropped));
                copy.setOnItemDropped(copyOnItemDropped);
            } else {
                copy.onItemDropped = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new DropSettings();
    }

}
