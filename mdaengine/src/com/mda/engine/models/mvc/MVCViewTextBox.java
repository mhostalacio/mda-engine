
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewTextBox complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewTextBox">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewInput">
 *       &lt;attribute name="InputType" type="{http://www.mdaengine.com/mdaengine/models/mvc}TextBoxInputType" default="TEXT" />
 *       &lt;attribute name="MaxLength" type="{http://www.w3.org/2001/XMLSchema}int" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewTextBox")
@XmlSeeAlso({
    MVCViewTextArea.class
})
public class MVCViewTextBox
    extends MVCViewInput
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "InputType")
    protected TextBoxInputType inputType;
    @XmlAttribute(name = "MaxLength")
    protected Integer maxLength;

    /**
     * Gets the value of the inputType property.
     * 
     * @return
     *     possible object is
     *     {@link TextBoxInputType }
     *     
     */
    public TextBoxInputType getInputType() {
        if (inputType == null) {
            return TextBoxInputType.TEXT;
        } else {
            return inputType;
        }
    }

    /**
     * Sets the value of the inputType property.
     * 
     * @param value
     *     allowed object is
     *     {@link TextBoxInputType }
     *     
     */
    public void setInputType(TextBoxInputType value) {
        this.inputType = value;
    }

    /**
     * Gets the value of the maxLength property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxLength() {
        return maxLength;
    }

    /**
     * Sets the value of the maxLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxLength(Integer value) {
        this.maxLength = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewTextBox)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewTextBox that = ((MVCViewTextBox) object);
        {
            TextBoxInputType lhsInputType;
            lhsInputType = this.getInputType();
            TextBoxInputType rhsInputType;
            rhsInputType = that.getInputType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "inputType", lhsInputType), LocatorUtils.property(thatLocator, "inputType", rhsInputType), lhsInputType, rhsInputType)) {
                return false;
            }
        }
        {
            Integer lhsMaxLength;
            lhsMaxLength = this.getMaxLength();
            Integer rhsMaxLength;
            rhsMaxLength = that.getMaxLength();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "maxLength", lhsMaxLength), LocatorUtils.property(thatLocator, "maxLength", rhsMaxLength), lhsMaxLength, rhsMaxLength)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCViewTextBox) {
            final MVCViewTextBox copy = ((MVCViewTextBox) draftCopy);
            if (this.inputType!= null) {
                TextBoxInputType sourceInputType;
                sourceInputType = this.getInputType();
                TextBoxInputType copyInputType = ((TextBoxInputType) strategy.copy(LocatorUtils.property(locator, "inputType", sourceInputType), sourceInputType));
                copy.setInputType(copyInputType);
            } else {
                copy.inputType = null;
            }
            if (this.maxLength!= null) {
                Integer sourceMaxLength;
                sourceMaxLength = this.getMaxLength();
                Integer copyMaxLength = ((Integer) strategy.copy(LocatorUtils.property(locator, "maxLength", sourceMaxLength), sourceMaxLength));
                copy.setMaxLength(copyMaxLength);
            } else {
                copy.maxLength = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewTextBox();
    }
    
//--simple--preserve
    
    @Override
    public String getClassName()
    {
    	return "MVCViewTextBox";
    }
    
    @Override
    public void writeInitializeMethodInternal(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product)
    {
    	super.writeInitializeMethodInternal(c, product);
    	if (!(this instanceof MVCViewTextArea))
    	{
    		c.line("{0}.InputType = TextBoxInputType.{1};", getViewElementId(), getInputType().value());
    	}
    	if (getMaxLength() != null)
    	{
    		c.line("{0}.MaxLength = {1};", getViewElementId(), getMaxLength());
    	}
    }
    
    @Override
    public void writeHtml(com.mda.engine.utils.Stringcode c)
    {
    	c.write("<input type=\"text\"");
    	if (getId() != null)
    	{
    		c.write(" id=\"{0}\"", getId());
    	}
    	if (getCss() != null)
    	{
    		c.write(" class=\"{0}\"", getCss());
    	}
    	if (getValue() != null)
    	{
    		c.write(" value=\"");
    		writeHtmlInputValue(c);
    		c.write("\"");
    	}
    	c.write("/>");
    	c.writeLine();
    }
    
//--simple--preserve

}
