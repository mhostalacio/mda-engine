
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for OpenToolTipTrigger complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OpenToolTipTrigger">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCTrigger">
 *       &lt;sequence>
 *         &lt;element name="Content" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewRepeaterTemplate"/>
 *       &lt;/sequence>
 *       &lt;attribute name="HorizontalPosition" type="{http://www.mdaengine.com/mdaengine/models/mvc}ToolTipHorizontalPosition" default="Right" />
 *       &lt;attribute name="VerticalPosition" type="{http://www.mdaengine.com/mdaengine/models/mvc}ToolTipVerticalPosition" default="Bottom" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpenToolTipTrigger", propOrder = {
    "content"
})
public class OpenToolTipTrigger
    extends MVCTrigger
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Content", required = true)
    protected MVCViewRepeaterTemplate content;
    @XmlAttribute(name = "HorizontalPosition")
    protected ToolTipHorizontalPosition horizontalPosition;
    @XmlAttribute(name = "VerticalPosition")
    protected ToolTipVerticalPosition verticalPosition;

    /**
     * Gets the value of the content property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewRepeaterTemplate }
     *     
     */
    public MVCViewRepeaterTemplate getContent() {
        return content;
    }

    /**
     * Sets the value of the content property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewRepeaterTemplate }
     *     
     */
    public void setContent(MVCViewRepeaterTemplate value) {
        this.content = value;
    }

    /**
     * Gets the value of the horizontalPosition property.
     * 
     * @return
     *     possible object is
     *     {@link ToolTipHorizontalPosition }
     *     
     */
    public ToolTipHorizontalPosition getHorizontalPosition() {
        if (horizontalPosition == null) {
            return ToolTipHorizontalPosition.RIGHT;
        } else {
            return horizontalPosition;
        }
    }

    /**
     * Sets the value of the horizontalPosition property.
     * 
     * @param value
     *     allowed object is
     *     {@link ToolTipHorizontalPosition }
     *     
     */
    public void setHorizontalPosition(ToolTipHorizontalPosition value) {
        this.horizontalPosition = value;
    }

    /**
     * Gets the value of the verticalPosition property.
     * 
     * @return
     *     possible object is
     *     {@link ToolTipVerticalPosition }
     *     
     */
    public ToolTipVerticalPosition getVerticalPosition() {
        if (verticalPosition == null) {
            return ToolTipVerticalPosition.BOTTOM;
        } else {
            return verticalPosition;
        }
    }

    /**
     * Sets the value of the verticalPosition property.
     * 
     * @param value
     *     allowed object is
     *     {@link ToolTipVerticalPosition }
     *     
     */
    public void setVerticalPosition(ToolTipVerticalPosition value) {
        this.verticalPosition = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof OpenToolTipTrigger)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final OpenToolTipTrigger that = ((OpenToolTipTrigger) object);
        {
            MVCViewRepeaterTemplate lhsContent;
            lhsContent = this.getContent();
            MVCViewRepeaterTemplate rhsContent;
            rhsContent = that.getContent();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "content", lhsContent), LocatorUtils.property(thatLocator, "content", rhsContent), lhsContent, rhsContent)) {
                return false;
            }
        }
        {
            ToolTipHorizontalPosition lhsHorizontalPosition;
            lhsHorizontalPosition = this.getHorizontalPosition();
            ToolTipHorizontalPosition rhsHorizontalPosition;
            rhsHorizontalPosition = that.getHorizontalPosition();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "horizontalPosition", lhsHorizontalPosition), LocatorUtils.property(thatLocator, "horizontalPosition", rhsHorizontalPosition), lhsHorizontalPosition, rhsHorizontalPosition)) {
                return false;
            }
        }
        {
            ToolTipVerticalPosition lhsVerticalPosition;
            lhsVerticalPosition = this.getVerticalPosition();
            ToolTipVerticalPosition rhsVerticalPosition;
            rhsVerticalPosition = that.getVerticalPosition();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "verticalPosition", lhsVerticalPosition), LocatorUtils.property(thatLocator, "verticalPosition", rhsVerticalPosition), lhsVerticalPosition, rhsVerticalPosition)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof OpenToolTipTrigger) {
            final OpenToolTipTrigger copy = ((OpenToolTipTrigger) draftCopy);
            if (this.content!= null) {
                MVCViewRepeaterTemplate sourceContent;
                sourceContent = this.getContent();
                MVCViewRepeaterTemplate copyContent = ((MVCViewRepeaterTemplate) strategy.copy(LocatorUtils.property(locator, "content", sourceContent), sourceContent));
                copy.setContent(copyContent);
            } else {
                copy.content = null;
            }
            if (this.horizontalPosition!= null) {
                ToolTipHorizontalPosition sourceHorizontalPosition;
                sourceHorizontalPosition = this.getHorizontalPosition();
                ToolTipHorizontalPosition copyHorizontalPosition = ((ToolTipHorizontalPosition) strategy.copy(LocatorUtils.property(locator, "horizontalPosition", sourceHorizontalPosition), sourceHorizontalPosition));
                copy.setHorizontalPosition(copyHorizontalPosition);
            } else {
                copy.horizontalPosition = null;
            }
            if (this.verticalPosition!= null) {
                ToolTipVerticalPosition sourceVerticalPosition;
                sourceVerticalPosition = this.getVerticalPosition();
                ToolTipVerticalPosition copyVerticalPosition = ((ToolTipVerticalPosition) strategy.copy(LocatorUtils.property(locator, "verticalPosition", sourceVerticalPosition), sourceVerticalPosition));
                copy.setVerticalPosition(copyVerticalPosition);
            } else {
                copy.verticalPosition = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new OpenToolTipTrigger();
    }
    
//--simple--preserve

    @Override
    protected String getActionCodeName()
    {
    	return "MVCViewElementOpenToolTipViewTrigger";
    }
    
//--simple--preserve

}
