
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.IMVCViewElementWithDatasource;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewPaginator complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewPaginator">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewElement">
 *       &lt;sequence>
 *         &lt;element name="DataSource" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValueChoice"/>
 *       &lt;/sequence>
 *       &lt;attribute name="DataSourceChunkSize" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="PageSize" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="Type" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewPaginatorType" default="NUMBERED" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewPaginator", propOrder = {
    "dataSource"
})
public class MVCViewPaginator
    extends MVCViewElement
    implements Serializable, Cloneable, IMVCViewElementWithDatasource, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "DataSource", required = true)
    protected MVCViewValueChoice dataSource;
    @XmlAttribute(name = "DataSourceChunkSize", required = true)
    protected int dataSourceChunkSize;
    @XmlAttribute(name = "PageSize", required = true)
    protected int pageSize;
    @XmlAttribute(name = "Type")
    protected MVCViewPaginatorType type;

    /**
     * Gets the value of the dataSource property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public MVCViewValueChoice getDataSource() {
        return dataSource;
    }

    /**
     * Sets the value of the dataSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public void setDataSource(MVCViewValueChoice value) {
        this.dataSource = value;
    }

    /**
     * Gets the value of the dataSourceChunkSize property.
     * 
     */
    public int getDataSourceChunkSize() {
        return dataSourceChunkSize;
    }

    /**
     * Sets the value of the dataSourceChunkSize property.
     * 
     */
    public void setDataSourceChunkSize(int value) {
        this.dataSourceChunkSize = value;
    }

    /**
     * Gets the value of the pageSize property.
     * 
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * Sets the value of the pageSize property.
     * 
     */
    public void setPageSize(int value) {
        this.pageSize = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewPaginatorType }
     *     
     */
    public MVCViewPaginatorType getType() {
        if (type == null) {
            return MVCViewPaginatorType.NUMBERED;
        } else {
            return type;
        }
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewPaginatorType }
     *     
     */
    public void setType(MVCViewPaginatorType value) {
        this.type = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewPaginator)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewPaginator that = ((MVCViewPaginator) object);
        {
            MVCViewValueChoice lhsDataSource;
            lhsDataSource = this.getDataSource();
            MVCViewValueChoice rhsDataSource;
            rhsDataSource = that.getDataSource();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dataSource", lhsDataSource), LocatorUtils.property(thatLocator, "dataSource", rhsDataSource), lhsDataSource, rhsDataSource)) {
                return false;
            }
        }
        {
            int lhsDataSourceChunkSize;
            lhsDataSourceChunkSize = this.getDataSourceChunkSize();
            int rhsDataSourceChunkSize;
            rhsDataSourceChunkSize = that.getDataSourceChunkSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dataSourceChunkSize", lhsDataSourceChunkSize), LocatorUtils.property(thatLocator, "dataSourceChunkSize", rhsDataSourceChunkSize), lhsDataSourceChunkSize, rhsDataSourceChunkSize)) {
                return false;
            }
        }
        {
            int lhsPageSize;
            lhsPageSize = this.getPageSize();
            int rhsPageSize;
            rhsPageSize = that.getPageSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "pageSize", lhsPageSize), LocatorUtils.property(thatLocator, "pageSize", rhsPageSize), lhsPageSize, rhsPageSize)) {
                return false;
            }
        }
        {
            MVCViewPaginatorType lhsType;
            lhsType = this.getType();
            MVCViewPaginatorType rhsType;
            rhsType = that.getType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "type", lhsType), LocatorUtils.property(thatLocator, "type", rhsType), lhsType, rhsType)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCViewPaginator) {
            final MVCViewPaginator copy = ((MVCViewPaginator) draftCopy);
            if (this.dataSource!= null) {
                MVCViewValueChoice sourceDataSource;
                sourceDataSource = this.getDataSource();
                MVCViewValueChoice copyDataSource = ((MVCViewValueChoice) strategy.copy(LocatorUtils.property(locator, "dataSource", sourceDataSource), sourceDataSource));
                copy.setDataSource(copyDataSource);
            } else {
                copy.dataSource = null;
            }
            int sourceDataSourceChunkSize;
            sourceDataSourceChunkSize = this.getDataSourceChunkSize();
            int copyDataSourceChunkSize = strategy.copy(LocatorUtils.property(locator, "dataSourceChunkSize", sourceDataSourceChunkSize), sourceDataSourceChunkSize);
            copy.setDataSourceChunkSize(copyDataSourceChunkSize);
            int sourcePageSize;
            sourcePageSize = this.getPageSize();
            int copyPageSize = strategy.copy(LocatorUtils.property(locator, "pageSize", sourcePageSize), sourcePageSize);
            copy.setPageSize(copyPageSize);
            if (this.type!= null) {
                MVCViewPaginatorType sourceType;
                sourceType = this.getType();
                MVCViewPaginatorType copyType = ((MVCViewPaginatorType) strategy.copy(LocatorUtils.property(locator, "type", sourceType), sourceType));
                copy.setType(copyType);
            } else {
                copy.type = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewPaginator();
    }
    
//--simple--preserve
    @Override
    public void setBinds(MVCModel model) throws Exception
    {
    	super.setBinds(model);
    	if (getDataSource() != null)
    	{
    		getDataSource().setBinds(model, getContextElement(), this);
    	}
    }
    
    @Override
    public String getDataSourceSingleClassName()
	{
		return this.getDataSource().getSingleClassName();
	}
    
    @Override
    public String getClassName() throws Exception
    {
    	if (getDataSource() != null)
    	{
    		if (this.getType().equals(MVCViewPaginatorType.NUMBERED))
    			return "MVCViewPaginator<"+ getDataSource().getSingleClassName()+">";
    		else if (this.getType().equals(MVCViewPaginatorType.INCREMENTAL))
    			return "MVCViewIncrementalPaginator<"+ getDataSource().getSingleClassName()+">"; 
    	}
    	throw new Exception("Datasource definition is missing");
    }
    
    @Override
    public void writeInitializeMethodInternal(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product)
    {
    	String elementId = getViewElementId();
     	c.line("{0}.DataSourceChunkSize = {1};", elementId, this.getDataSourceChunkSize());
     	c.line("{0}.PageSize = {1};", elementId, this.getPageSize());
     	c.line("{0}.DataSource().PageSize = {0}.PageSize;", elementId);
    }
    
    @Override
    public void writeExtraMethodsToInitialize(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product) throws Exception
    {
    	c.line();
    	String elementId = getViewElementId();
    	c.line("/// <summary>");
        c.line("/// Method called when changing page of element '{0}'", this.getId());
        c.line("/// </summary>");
    	c.line("public virtual ActionResult {0}GoToPage(int pageNumber, String mkey)", this.getId());
    	c.line("{");
    	c.tokenAdd2BeginOfLineIndent();
    	c.line("ModelKey = mkey;");
    	c.line("RecoverModel();");
    	c.writeLine("");
    	writeInitializeMethodCall(c,null, true, product, false);
    	c.line("{0}.DataSource().CurrentPage = pageNumber;", elementId);
    	c.line("if ({0}.DataSource().NeedsMoreEntities())", elementId);
    	c.line("{");
        c.tokenAdd2BeginOfLineIndent();
        c.line("{0}GetMoreEntities({1}.DataSourceChunkSize);", this.getId(), elementId);
        c.tokenRemove2BeginOfLine();
        c.line("}");
        c.line("using (System.Web.UI.HtmlTextWriter writer = CreateHtmlTextWriter())");
        c.line("{");
        c.tokenAdd2BeginOfLineIndent();
        
        if (this.getDataSource().getModelAttribute() != null 
        		&& this.getDataSource().getModelAttribute().getAssociatedModelAttribute() != null
        		&& this.getDataSource().getModelAttribute().getAssociatedModelAttribute().GetElementsBinded2Me().size() > 1)
        {
        	java.util.ArrayList<MVCViewElement> list = this.getDataSource().getModelAttribute().getAssociatedModelAttribute().GetElementsBinded2Me();
        	c.line("//Initialize and render elements that are binded to datasource");
        	for(MVCViewElement elem : list)
        	{
        		if (elem != this && elem instanceof MVCViewRepeater)
        		{
        			c.writeLine("");
        			elem.writeInitializeMethodCall(c,null, true, product, false);
        			
        			if (this.getType().equals(MVCViewPaginatorType.NUMBERED))
        				c.line("{0}.Render(writer);", elem.getViewElementId());
            		else if (this.getType().equals(MVCViewPaginatorType.INCREMENTAL))
            		{
            			c.line("if (pageNumber == 0)");
            			c.line("	{0}.Render(writer);", elem.getViewElementId());
            			c.line("else");
            			c.line("	{0}.RenderIncremental(writer);", elem.getViewElementId());
            		}
        			
        		}
        	}
        }
        
    	
        c.line("{0}.Render(writer);", elementId);
        c.tokenRemove2BeginOfLine();
        c.line("}");
    	c.line("return new EmptyResult();");
    	c.tokenRemove2BeginOfLine();
    	c.line("}");
    	c.line();
    	
    	c.line();
    	c.line("/// <summary>");
        c.line("/// Method called to fill datasource when needed", this.getId());
        c.line("/// </summary>");
    	c.line("protected virtual void {0}GetMoreEntities(int chunkSize)", this.getId());
    	c.line("{");
    	c.line("}");

    }
//--simple--preserve

}
