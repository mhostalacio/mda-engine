
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for AcceptDropSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AcceptDropSettings">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="ElementsSelector" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AcceptDropSettings")
public class AcceptDropSettings
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "ElementsSelector")
    protected String elementsSelector;

    /**
     * Gets the value of the elementsSelector property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getElementsSelector() {
        return elementsSelector;
    }

    /**
     * Sets the value of the elementsSelector property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setElementsSelector(String value) {
        this.elementsSelector = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof AcceptDropSettings)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final AcceptDropSettings that = ((AcceptDropSettings) object);
        {
            String lhsElementsSelector;
            lhsElementsSelector = this.getElementsSelector();
            String rhsElementsSelector;
            rhsElementsSelector = that.getElementsSelector();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "elementsSelector", lhsElementsSelector), LocatorUtils.property(thatLocator, "elementsSelector", rhsElementsSelector), lhsElementsSelector, rhsElementsSelector)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof AcceptDropSettings) {
            final AcceptDropSettings copy = ((AcceptDropSettings) draftCopy);
            if (this.elementsSelector!= null) {
                String sourceElementsSelector;
                sourceElementsSelector = this.getElementsSelector();
                String copyElementsSelector = ((String) strategy.copy(LocatorUtils.property(locator, "elementsSelector", sourceElementsSelector), sourceElementsSelector));
                copy.setElementsSelector(copyElementsSelector);
            } else {
                copy.elementsSelector = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new AcceptDropSettings();
    }

}
