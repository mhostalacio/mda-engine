
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewValueModelAttribute complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewValueModelAttribute">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValue">
 *       &lt;attribute name="Path" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="GetTranslation" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="TranslationStringFormat" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="TranslationKeyType" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewValueModelAttribute")
public class MVCViewValueModelAttribute
    extends MVCViewValue
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "Path")
    protected String path;
    @XmlAttribute(name = "GetTranslation")
    protected Boolean getTranslation;
    @XmlAttribute(name = "TranslationStringFormat")
    protected String translationStringFormat;
    @XmlAttribute(name = "TranslationKeyType")
    protected String translationKeyType;

    /**
     * Gets the value of the path property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPath() {
        return path;
    }

    /**
     * Sets the value of the path property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPath(String value) {
        this.path = value;
    }

    /**
     * Gets the value of the getTranslation property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isGetTranslation() {
        if (getTranslation == null) {
            return false;
        } else {
            return getTranslation;
        }
    }

    /**
     * Sets the value of the getTranslation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGetTranslation(Boolean value) {
        this.getTranslation = value;
    }

    /**
     * Gets the value of the translationStringFormat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTranslationStringFormat() {
        return translationStringFormat;
    }

    /**
     * Sets the value of the translationStringFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTranslationStringFormat(String value) {
        this.translationStringFormat = value;
    }

    /**
     * Gets the value of the translationKeyType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTranslationKeyType() {
        return translationKeyType;
    }

    /**
     * Sets the value of the translationKeyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTranslationKeyType(String value) {
        this.translationKeyType = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewValueModelAttribute)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewValueModelAttribute that = ((MVCViewValueModelAttribute) object);
        {
            String lhsPath;
            lhsPath = this.getPath();
            String rhsPath;
            rhsPath = that.getPath();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "path", lhsPath), LocatorUtils.property(thatLocator, "path", rhsPath), lhsPath, rhsPath)) {
                return false;
            }
        }
        {
            boolean lhsGetTranslation;
            lhsGetTranslation = this.isGetTranslation();
            boolean rhsGetTranslation;
            rhsGetTranslation = that.isGetTranslation();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "getTranslation", lhsGetTranslation), LocatorUtils.property(thatLocator, "getTranslation", rhsGetTranslation), lhsGetTranslation, rhsGetTranslation)) {
                return false;
            }
        }
        {
            String lhsTranslationStringFormat;
            lhsTranslationStringFormat = this.getTranslationStringFormat();
            String rhsTranslationStringFormat;
            rhsTranslationStringFormat = that.getTranslationStringFormat();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "translationStringFormat", lhsTranslationStringFormat), LocatorUtils.property(thatLocator, "translationStringFormat", rhsTranslationStringFormat), lhsTranslationStringFormat, rhsTranslationStringFormat)) {
                return false;
            }
        }
        {
            String lhsTranslationKeyType;
            lhsTranslationKeyType = this.getTranslationKeyType();
            String rhsTranslationKeyType;
            rhsTranslationKeyType = that.getTranslationKeyType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "translationKeyType", lhsTranslationKeyType), LocatorUtils.property(thatLocator, "translationKeyType", rhsTranslationKeyType), lhsTranslationKeyType, rhsTranslationKeyType)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCViewValueModelAttribute) {
            final MVCViewValueModelAttribute copy = ((MVCViewValueModelAttribute) draftCopy);
            if (this.path!= null) {
                String sourcePath;
                sourcePath = this.getPath();
                String copyPath = ((String) strategy.copy(LocatorUtils.property(locator, "path", sourcePath), sourcePath));
                copy.setPath(copyPath);
            } else {
                copy.path = null;
            }
            if (this.getTranslation!= null) {
                boolean sourceGetTranslation;
                sourceGetTranslation = this.isGetTranslation();
                boolean copyGetTranslation = strategy.copy(LocatorUtils.property(locator, "getTranslation", sourceGetTranslation), sourceGetTranslation);
                copy.setGetTranslation(copyGetTranslation);
            } else {
                copy.getTranslation = null;
            }
            if (this.translationStringFormat!= null) {
                String sourceTranslationStringFormat;
                sourceTranslationStringFormat = this.getTranslationStringFormat();
                String copyTranslationStringFormat = ((String) strategy.copy(LocatorUtils.property(locator, "translationStringFormat", sourceTranslationStringFormat), sourceTranslationStringFormat));
                copy.setTranslationStringFormat(copyTranslationStringFormat);
            } else {
                copy.translationStringFormat = null;
            }
            if (this.translationKeyType!= null) {
                String sourceTranslationKeyType;
                sourceTranslationKeyType = this.getTranslationKeyType();
                String copyTranslationKeyType = ((String) strategy.copy(LocatorUtils.property(locator, "translationKeyType", sourceTranslationKeyType), sourceTranslationKeyType));
                copy.setTranslationKeyType(copyTranslationKeyType);
            } else {
                copy.translationKeyType = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewValueModelAttribute();
    }
    
//--simple--preserve
    
    private transient String className;
    private transient String singleClassName;
    private transient com.mda.engine.models.businessObjectModel.ObjectType associatedBom;
    private transient com.mda.engine.models.common.ModelAttribute associatedModelAttribute;
    private transient String pathSuffix = "";
    private transient boolean isList = false;
    private transient boolean nullable = false;
    
    public String getPathSuffix()
    {
    	return pathSuffix;
    }
    
    public void setBinds(MVCModel model, com.mda.engine.core.IMVCViewElementWithDatasource contextElement, com.mda.engine.models.mvc.MVCViewElement currentElement) throws Exception
    {
    	if (getPath() != null)
    	{
    		String[] parts = getPath().split("\\.");
    		if (parts.length > 1)
    		{
    			if (parts[0].equalsIgnoreCase("Model"))
    			{
    				com.mda.engine.models.common.ModelAttribute attribute = model.getModelAttributeByName(parts[1]);
	    			if (attribute != null)
	    			{
	    				if (!attribute.GetElementsBinded2Me().contains(currentElement))
	    					attribute.GetElementsBinded2Me().add(currentElement);
	    				setAssociatedModelAttribute(attribute);
	    				
	    				if (attribute instanceof com.mda.engine.models.common.ModelAttributeObject)
	    				{
	    					com.mda.engine.models.common.ModelAttributeObject objAtr = (com.mda.engine.models.common.ModelAttributeObject)attribute;
	    					if (parts.length == 2)
	    					{
	    						setAssociatedBom(objAtr.getBom());
	    					}
	    					setBindsInternal(parts, objAtr.getBom(), 2, objAtr.isIsList());
	    				}
	    				else if (attribute instanceof com.mda.engine.models.common.ModelAttributeEnum)
	    				{
	    					com.mda.engine.models.common.ModelAttributeEnum enumAtr = (com.mda.engine.models.common.ModelAttributeEnum)attribute;
	    					if (enumAtr.isIsList())
	    					{
	    						setIsList(true);
	    						setClassName("IList<" + enumAtr.getEnumType()+ ">");
	    					}
	    					else
	    					{
	    						setClassName(enumAtr.getEnumType() + (enumAtr.isNullable() ? "?" : ""));
	    					}
	    					setSingleClassName(enumAtr.getEnumType());
	    					setNullable(enumAtr.isNullable());
	    				}
	    				else if (attribute instanceof com.mda.engine.models.common.ModelAttributeText)
	    				{
	    					if (attribute.isIsList())
	    					{
	    						setIsList(true);
	    						setClassName("IList<String>");
	    					}
	    					else
	    					{
	    						setClassName("String");
	    					}
	    					setSingleClassName("String");
	    				}
	    				else if (attribute instanceof com.mda.engine.models.common.ModelAttributeBoolean)
	    				{
	    					if (attribute.isIsList())
	    					{
	    						setIsList(true);
	    						setClassName("IList<Boolean>");
	    					}
	    					else
	    					{
	    						setClassName("Boolean" + (attribute.isNullable() ? "?" : ""));
	    					}
	    					setSingleClassName("Boolean" + (attribute.isNullable() ? "?" : ""));
	    					setNullable(attribute.isNullable());
	    				}
	    				else if (attribute instanceof com.mda.engine.models.common.ModelAttributeDateTime)
	    				{
	    					if (attribute.isIsList())
	    					{
	    						setIsList(true);
	    						setClassName("IList<DateTime>");
	    					}
	    					else
	    					{
	    						setClassName("DateTime" + (attribute.isNullable() ? "?" : ""));
	    					}
	    					setSingleClassName("DateTime" + (attribute.isNullable() ? "?" : ""));
	    					setNullable(attribute.isNullable());
	    				}
	    				else if (attribute instanceof com.mda.engine.models.common.ModelAttributeLong)
	    				{
	    					if (attribute.isIsList())
	    					{
	    						setIsList(true);
	    						setClassName("IList<Int64>");
	    					}
	    					else
	    					{
	    						setClassName("Int64" + (attribute.isNullable() ? "?" : ""));
	    					}
	    					setSingleClassName("Int64" + (attribute.isNullable() ? "?" : ""));
	    					setNullable(attribute.isNullable());
	    				}
	    				else if (attribute instanceof com.mda.engine.models.common.ModelAttributeInt)
	    				{
	    					if (attribute.isIsList())
	    					{
	    						setIsList(true);
	    						setClassName("IList<int>");
	    					}
	    					else
	    					{
	    						setClassName("int" + (attribute.isNullable() ? "?" : ""));
	    					}
	    					setSingleClassName("int" + (attribute.isNullable() ? "?" : ""));
	    					setNullable(attribute.isNullable());
	    				}
	    				else if (attribute instanceof com.mda.engine.models.common.ModelAttributeDecimal)
	    				{
	    					if (attribute.isIsList())
	    					{
	    						setIsList(true);
	    						setClassName("IList<Decimal>");
	    					}
	    					else
	    					{
	    						setClassName("Decimal" + (attribute.isNullable() ? "?" : ""));
	    					}
	    					setSingleClassName("Decimal" + (attribute.isNullable() ? "?" : ""));
	    					setNullable(attribute.isNullable());
	    				}
	    				
	    			}
	    			else if (parts[1].equalsIgnoreCase("ModelKey"))
	    			{
	    				setClassName("String");
    					setSingleClassName("String");
	    			}
    			}
    			else if (contextElement != null)
    			{
    				com.mda.engine.core.IMVCViewElementWithDatasource currCtxElem = contextElement;
    				while (currCtxElem != null)
    				{
    					if (currCtxElem.getId().equalsIgnoreCase(parts[0]))
    					{
    						MVCViewValueChoice dataSource = currCtxElem.getDataSource();
    						if (dataSource.getAssociatedBom() != null)
    						{
    							setBindsInternal(parts, dataSource.getAssociatedBom(), 1, false);
    						}
    						else
    						{
    							//static value
    							setClassName(dataSource.getSingleClassName());
    						}
    						
    					}
    					if (currCtxElem == currCtxElem.getContextElement())
    					{
    						//lookup 
    						currCtxElem = null;
    					}
    					else
    					{
    						currCtxElem = currCtxElem.getContextElement();
    					}

    				}
    			}
    			else
    			{
    				throw new Exception("Model attribute not found: " + getPath());
    			}
    		}
    		else if (contextElement != null)
    		{
    			if (contextElement.getId().equalsIgnoreCase(parts[0]))
				{
					MVCViewValueChoice dataSource = contextElement.getDataSource();
					if (dataSource.getAssociatedBom() != null)
					{
						setBindsInternal(parts, dataSource.getAssociatedBom(), 1, false);
					}
					else
					{
						//static value
						setClassName(dataSource.getSingleClassName());
						setSingleClassName(dataSource.getSingleClassName());
					}
					
				}
    		}
    	}
    }

	private void setBindsInternal(String[] parts, com.mda.engine.models.businessObjectModel.ObjectType bom, int startLevel, Boolean isList)
			throws Exception {
		if (parts.length == startLevel)
		{
			
			if (isList)
			{
				setIsList(true);
				setClassName("PagedList<" + bom.getFullClassName() + ">");
			}else
			{
				setClassName(bom.getFullClassName());
			}
			setSingleClassName(bom.getFullClassName());
			
		}
		else if (parts.length == (startLevel + 1))
		{
			if (this.isGetTranslation())
			{
				setClassName("String");
				setSingleClassName("String");
			}
			else
			{
				//set to bom attribute
				com.mda.engine.models.businessObjectModel.BaseAttributeType baseAtr = bom.getAttributeByName(parts[startLevel]);
				if (baseAtr == null)
				{
					String val = "";
					for(int i = 0; i < parts.length; i++)
					{
						if (i > 0)
							val += ".";
						val += parts[i];
					}
					throw new Exception("Attribute not found: " + val + ", in ");
				}
				setClassName(baseAtr.getClassName());
				setNullable(baseAtr.isIsNullable());
				setIsList(baseAtr.isList());
				if (baseAtr instanceof com.mda.engine.models.businessObjectModel.BaseAttributeWithTargetType)
				{
					com.mda.engine.models.businessObjectModel.BaseAttributeWithTargetType attWithTarget = (com.mda.engine.models.businessObjectModel.BaseAttributeWithTargetType)baseAtr;
					setAssociatedBom(attWithTarget.getTargetObject());
					setNullable(attWithTarget.getMultiplicityNumber() <= 1);
				}
				if (baseAtr instanceof com.mda.engine.models.businessObjectModel.MultiLanguageBaseAttributeType)
				{
					com.mda.engine.models.businessObjectModel.MultiLanguageBaseAttributeType attMulti = (com.mda.engine.models.businessObjectModel.MultiLanguageBaseAttributeType)baseAtr;
					setClassName("String");
					if(attMulti.isIsMultiLanguage())
						pathSuffix = ".Value";
				}
				setSingleClassName(baseAtr.getSingleClassName());
			}
			
		}
		else if (parts.length > (startLevel + 1))
		{
			com.mda.engine.models.businessObjectModel.BaseAttributeType baseAtr = bom.getAttributeByName(parts[startLevel]);
			if (baseAtr instanceof com.mda.engine.models.businessObjectModel.BaseAttributeWithTargetType)
			{
				com.mda.engine.models.businessObjectModel.BaseAttributeWithTargetType attWithTarget = (com.mda.engine.models.businessObjectModel.BaseAttributeWithTargetType)baseAtr;
				com.mda.engine.models.businessObjectModel.ObjectType childBom = attWithTarget.getTargetObject();
				setBindsInternal(parts, childBom, startLevel+1, isList);
				setNullable(attWithTarget.getMultiplicityNumber() <= 1);
			}
			else
			{
				throw new Exception("Model attribute not found: " + getPath());
			}
		}
		else
		{
			throw new Exception("Model attribute not found: " + getPath());
		}
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getClassName() {
		return className;
	}

	public void setSingleClassName(String singleClassName) {
		this.singleClassName = singleClassName;
	}

	public String getSingleClassName() {
		return singleClassName;
	}

	public void setAssociatedBom(com.mda.engine.models.businessObjectModel.ObjectType associatedBom) {
		this.associatedBom = associatedBom;
	}

	public com.mda.engine.models.businessObjectModel.ObjectType getAssociatedBom() {
		return associatedBom;
	}

	public boolean isList() {
		return isList;
	}

	public void setIsList(boolean isList) {
		this.isList = isList;
	}

	public boolean isNullable() {
		return nullable;
	}

	public void setNullable(boolean nullable) {
		this.nullable = nullable;
	}

	public com.mda.engine.models.common.ModelAttribute getAssociatedModelAttribute() {
		return associatedModelAttribute;
	}

	public void setAssociatedModelAttribute(com.mda.engine.models.common.ModelAttribute associatedModelAttribute) {
		this.associatedModelAttribute = associatedModelAttribute;
	}
    
//--simple--preserve

}
