
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewElement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewElement">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="HelpText" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValueChoice" minOccurs="0"/>
 *         &lt;element name="OnMouseOver" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCTriggerCollection" minOccurs="0"/>
 *         &lt;element name="Draggable" type="{http://www.mdaengine.com/mdaengine/models/mvc}DragSettings" minOccurs="0"/>
 *         &lt;element name="Droppable" type="{http://www.mdaengine.com/mdaengine/models/mvc}DropSettings" minOccurs="0"/>
 *         &lt;element name="Attributes" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewElementAttributesCollection" minOccurs="0"/>
 *         &lt;element name="Security" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewSecurityCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Id" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Css" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="IconCss" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewElement", propOrder = {
    "helpText",
    "onMouseOver",
    "draggable",
    "droppable",
    "attributes",
    "security"
})
@XmlSeeAlso({
    MVCViewTable.class,
    MVCViewTableRow.class,
    MVCViewLookup.class,
    MVCViewUL.class,
    MVCViewObject.class,
    MVCViewTreeView.class,
    MVCViewElementReference.class,
    MVCViewCalendar.class,
    MVCViewTimeline.class,
    MVCViewRepeater.class,
    MVCViewPaginator.class,
    MVCViewComposedElement.class,
    MVCViewCarousel.class,
    MVCViewImage.class,
    MVCViewInclude.class,
    MVCViewInput.class,
    MVCViewBreakLine.class,
    MVCViewHR.class,
    MVCViewLiteral.class
})
public abstract class MVCViewElement
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "HelpText")
    protected MVCViewValueChoice helpText;
    @XmlElement(name = "OnMouseOver")
    protected MVCTriggerCollection onMouseOver;
    @XmlElement(name = "Draggable")
    protected DragSettings draggable;
    @XmlElement(name = "Droppable")
    protected DropSettings droppable;
    @XmlElement(name = "Attributes")
    protected MVCViewElementAttributesCollection attributes;
    @XmlElement(name = "Security")
    protected MVCViewSecurityCollection security;
    @XmlAttribute(name = "Id")
    protected String id;
    @XmlAttribute(name = "Css")
    protected String css;
    @XmlAttribute(name = "IconCss")
    protected String iconCss;

    /**
     * Gets the value of the helpText property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public MVCViewValueChoice getHelpText() {
        return helpText;
    }

    /**
     * Sets the value of the helpText property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public void setHelpText(MVCViewValueChoice value) {
        this.helpText = value;
    }

    /**
     * Gets the value of the onMouseOver property.
     * 
     * @return
     *     possible object is
     *     {@link MVCTriggerCollection }
     *     
     */
    public MVCTriggerCollection getOnMouseOver() {
        return onMouseOver;
    }

    /**
     * Sets the value of the onMouseOver property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCTriggerCollection }
     *     
     */
    public void setOnMouseOver(MVCTriggerCollection value) {
        this.onMouseOver = value;
    }

    /**
     * Gets the value of the draggable property.
     * 
     * @return
     *     possible object is
     *     {@link DragSettings }
     *     
     */
    public DragSettings getDraggable() {
        return draggable;
    }

    /**
     * Sets the value of the draggable property.
     * 
     * @param value
     *     allowed object is
     *     {@link DragSettings }
     *     
     */
    public void setDraggable(DragSettings value) {
        this.draggable = value;
    }

    /**
     * Gets the value of the droppable property.
     * 
     * @return
     *     possible object is
     *     {@link DropSettings }
     *     
     */
    public DropSettings getDroppable() {
        return droppable;
    }

    /**
     * Sets the value of the droppable property.
     * 
     * @param value
     *     allowed object is
     *     {@link DropSettings }
     *     
     */
    public void setDroppable(DropSettings value) {
        this.droppable = value;
    }

    /**
     * Gets the value of the attributes property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewElementAttributesCollection }
     *     
     */
    public MVCViewElementAttributesCollection getAttributes() {
        return attributes;
    }

    /**
     * Sets the value of the attributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewElementAttributesCollection }
     *     
     */
    public void setAttributes(MVCViewElementAttributesCollection value) {
        this.attributes = value;
    }

    /**
     * Gets the value of the security property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewSecurityCollection }
     *     
     */
    public MVCViewSecurityCollection getSecurity() {
        return security;
    }

    /**
     * Sets the value of the security property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewSecurityCollection }
     *     
     */
    public void setSecurity(MVCViewSecurityCollection value) {
        this.security = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the css property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCss() {
        return css;
    }

    /**
     * Sets the value of the css property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCss(String value) {
        this.css = value;
    }

    /**
     * Gets the value of the iconCss property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIconCss() {
        return iconCss;
    }

    /**
     * Sets the value of the iconCss property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIconCss(String value) {
        this.iconCss = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewElement)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final MVCViewElement that = ((MVCViewElement) object);
        {
            MVCViewValueChoice lhsHelpText;
            lhsHelpText = this.getHelpText();
            MVCViewValueChoice rhsHelpText;
            rhsHelpText = that.getHelpText();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "helpText", lhsHelpText), LocatorUtils.property(thatLocator, "helpText", rhsHelpText), lhsHelpText, rhsHelpText)) {
                return false;
            }
        }
        {
            MVCTriggerCollection lhsOnMouseOver;
            lhsOnMouseOver = this.getOnMouseOver();
            MVCTriggerCollection rhsOnMouseOver;
            rhsOnMouseOver = that.getOnMouseOver();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "onMouseOver", lhsOnMouseOver), LocatorUtils.property(thatLocator, "onMouseOver", rhsOnMouseOver), lhsOnMouseOver, rhsOnMouseOver)) {
                return false;
            }
        }
        {
            DragSettings lhsDraggable;
            lhsDraggable = this.getDraggable();
            DragSettings rhsDraggable;
            rhsDraggable = that.getDraggable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "draggable", lhsDraggable), LocatorUtils.property(thatLocator, "draggable", rhsDraggable), lhsDraggable, rhsDraggable)) {
                return false;
            }
        }
        {
            DropSettings lhsDroppable;
            lhsDroppable = this.getDroppable();
            DropSettings rhsDroppable;
            rhsDroppable = that.getDroppable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "droppable", lhsDroppable), LocatorUtils.property(thatLocator, "droppable", rhsDroppable), lhsDroppable, rhsDroppable)) {
                return false;
            }
        }
        {
            MVCViewElementAttributesCollection lhsAttributes;
            lhsAttributes = this.getAttributes();
            MVCViewElementAttributesCollection rhsAttributes;
            rhsAttributes = that.getAttributes();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "attributes", lhsAttributes), LocatorUtils.property(thatLocator, "attributes", rhsAttributes), lhsAttributes, rhsAttributes)) {
                return false;
            }
        }
        {
            MVCViewSecurityCollection lhsSecurity;
            lhsSecurity = this.getSecurity();
            MVCViewSecurityCollection rhsSecurity;
            rhsSecurity = that.getSecurity();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "security", lhsSecurity), LocatorUtils.property(thatLocator, "security", rhsSecurity), lhsSecurity, rhsSecurity)) {
                return false;
            }
        }
        {
            String lhsId;
            lhsId = this.getId();
            String rhsId;
            rhsId = that.getId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "id", lhsId), LocatorUtils.property(thatLocator, "id", rhsId), lhsId, rhsId)) {
                return false;
            }
        }
        {
            String lhsCss;
            lhsCss = this.getCss();
            String rhsCss;
            rhsCss = that.getCss();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "css", lhsCss), LocatorUtils.property(thatLocator, "css", rhsCss), lhsCss, rhsCss)) {
                return false;
            }
        }
        {
            String lhsIconCss;
            lhsIconCss = this.getIconCss();
            String rhsIconCss;
            rhsIconCss = that.getIconCss();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "iconCss", lhsIconCss), LocatorUtils.property(thatLocator, "iconCss", rhsIconCss), lhsIconCss, rhsIconCss)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        if (null == target) {
            throw new IllegalArgumentException("Target argument must not be null for abstract copyable classes.");
        }
        if (target instanceof MVCViewElement) {
            final MVCViewElement copy = ((MVCViewElement) target);
            if (this.helpText!= null) {
                MVCViewValueChoice sourceHelpText;
                sourceHelpText = this.getHelpText();
                MVCViewValueChoice copyHelpText = ((MVCViewValueChoice) strategy.copy(LocatorUtils.property(locator, "helpText", sourceHelpText), sourceHelpText));
                copy.setHelpText(copyHelpText);
            } else {
                copy.helpText = null;
            }
            if (this.onMouseOver!= null) {
                MVCTriggerCollection sourceOnMouseOver;
                sourceOnMouseOver = this.getOnMouseOver();
                MVCTriggerCollection copyOnMouseOver = ((MVCTriggerCollection) strategy.copy(LocatorUtils.property(locator, "onMouseOver", sourceOnMouseOver), sourceOnMouseOver));
                copy.setOnMouseOver(copyOnMouseOver);
            } else {
                copy.onMouseOver = null;
            }
            if (this.draggable!= null) {
                DragSettings sourceDraggable;
                sourceDraggable = this.getDraggable();
                DragSettings copyDraggable = ((DragSettings) strategy.copy(LocatorUtils.property(locator, "draggable", sourceDraggable), sourceDraggable));
                copy.setDraggable(copyDraggable);
            } else {
                copy.draggable = null;
            }
            if (this.droppable!= null) {
                DropSettings sourceDroppable;
                sourceDroppable = this.getDroppable();
                DropSettings copyDroppable = ((DropSettings) strategy.copy(LocatorUtils.property(locator, "droppable", sourceDroppable), sourceDroppable));
                copy.setDroppable(copyDroppable);
            } else {
                copy.droppable = null;
            }
            if (this.attributes!= null) {
                MVCViewElementAttributesCollection sourceAttributes;
                sourceAttributes = this.getAttributes();
                MVCViewElementAttributesCollection copyAttributes = ((MVCViewElementAttributesCollection) strategy.copy(LocatorUtils.property(locator, "attributes", sourceAttributes), sourceAttributes));
                copy.setAttributes(copyAttributes);
            } else {
                copy.attributes = null;
            }
            if (this.security!= null) {
                MVCViewSecurityCollection sourceSecurity;
                sourceSecurity = this.getSecurity();
                MVCViewSecurityCollection copySecurity = ((MVCViewSecurityCollection) strategy.copy(LocatorUtils.property(locator, "security", sourceSecurity), sourceSecurity));
                copy.setSecurity(copySecurity);
            } else {
                copy.security = null;
            }
            if (this.id!= null) {
                String sourceId;
                sourceId = this.getId();
                String copyId = ((String) strategy.copy(LocatorUtils.property(locator, "id", sourceId), sourceId));
                copy.setId(copyId);
            } else {
                copy.id = null;
            }
            if (this.css!= null) {
                String sourceCss;
                sourceCss = this.getCss();
                String copyCss = ((String) strategy.copy(LocatorUtils.property(locator, "css", sourceCss), sourceCss));
                copy.setCss(copyCss);
            } else {
                copy.css = null;
            }
            if (this.iconCss!= null) {
                String sourceIconCss;
                sourceIconCss = this.getIconCss();
                String copyIconCss = ((String) strategy.copy(LocatorUtils.property(locator, "iconCss", sourceIconCss), sourceIconCss));
                copy.setIconCss(copyIconCss);
            } else {
                copy.iconCss = null;
            }
        }
        return target;
    }
    
//--simple--preserve
    
    private transient com.mda.engine.core.IMVCViewElementWithDatasource contextElement;
    
    public String getViewElementId()
    {
    	return com.mda.engine.utils.StringUtils.getPascalCase(getId()) + "Element";
    }
    public String getClassName() throws Exception
    {
    	throw new org.apache.commons.lang.NotImplementedException("Missing getClassName() method implementation for: " + this.getClass().getName());
    }

    public void writeInitializeMethodCall(com.mda.engine.utils.Stringcode c, String parentElementId, boolean addComma, com.mda.engine.core.Product product, boolean firstMethodOfRepeater) throws Exception
    {
    	java.util.HashMap<String, String> args = parseInitializeArgumentsCall(parentElementId, firstMethodOfRepeater);
    	String extraArguments = args.get("extraArguments");
    	c.write("Initialize{0}({1}){2}", com.mda.engine.utils.StringUtils.getPascalCase(getId()), extraArguments, addComma ? ";" : "");
    }
    
    public java.util.HashMap<String, String> parseInitializeArgumentsCall(String parentElementId, boolean firstMethodOfRepeater)
    {
    	java.util.HashMap<String, String> args = new java.util.HashMap<String, String>();
    	String extraArguments = "null";
    	int repeaterLevel = 0;
    	java.util.ArrayList<String> classNames = new java.util.ArrayList<String>();
    	
    	if (getContextElement() != null)
    	{
    		extraArguments = parentElementId;
    		com.mda.engine.core.IMVCViewElementWithDatasource currCtxElem = getContextElement();
    		String previousId = "";
			while (currCtxElem != null)
			{
				if (currCtxElem instanceof MVCViewRepeater)
				{
					repeaterLevel++;
					classNames.add(((MVCViewRepeater)currCtxElem).getDataSourceSingleClassName());
				}
				
				if (!previousId.equals(currCtxElem.getId()))
				{
					String name = com.mda.engine.utils.StringUtils.getCamelCase(currCtxElem.getId());
					if(!args.containsKey("dataArgument"))
					{
						args.put("dataArgument", name);
					}
					previousId = currCtxElem.getId();
					
					if (firstMethodOfRepeater && currCtxElem instanceof MVCViewRepeater && repeaterLevel > 1)
					{
						String ctxElem = "";
						for (int i = 1; i < repeaterLevel; i++)
						{
							ctxElem += ".ContextElement";
						}
						extraArguments += String.format(", (%s)%s%s.CurrentBind, %s%s.CurrentIndex", classNames.get(repeaterLevel-1), parentElementId, ctxElem, parentElementId, ctxElem);
					}
					else
					{
						extraArguments += String.format(", %s, %sIndex", name, name);
					}
				}
				currCtxElem = currCtxElem.getContextElement();
			}
    	}
    	
    	
    	args.put("extraArguments", extraArguments);
    	return args;
    }
    
    public java.util.HashMap<String, String> parseInitializeArguments(String extraArguments, String idSuffix)
    {
    	if (getContextElement() != null)
    	{
    		com.mda.engine.core.IMVCViewElementWithDatasource currCtxElem = getContextElement();
    		String previousId = "";
			while (currCtxElem != null)
			{
				if (!previousId.equals(currCtxElem.getId()))
				{
					String name = com.mda.engine.utils.StringUtils.getCamelCase(currCtxElem.getId());
					previousId = currCtxElem.getId();
					extraArguments += String.format(", %s %s, int %sIndex", currCtxElem.getDataSourceSingleClassName(), name, name);
					idSuffix += String.format(" + \"_\" + %sIndex", name);
				}
				currCtxElem = currCtxElem.getContextElement();
			}
    	}
    	java.util.HashMap<String, String> args = new java.util.HashMap<String, String>();
    	args.put("extraArguments", extraArguments);
    	args.put("idSuffix", idSuffix);
    	return args;
    }
    
    public void writeInitializeMethod(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product) throws Exception
    {
    	String elementId = getViewElementId();
    	java.util.HashMap<String, String> args = parseInitializeArguments("", "");
    	String extraArguments = args.get("extraArguments");
    	String idSuffix = args.get("idSuffix");
    	c.line("protected virtual {0} Initialize{1}(IMVCViewElement parent{2})", getClassName(),  com.mda.engine.utils.StringUtils.getPascalCase(getId()), extraArguments);
    	c.line("{");
    	c.tokenAdd2BeginOfLineIndent();
    	if (getContextElement() == null)
    	{
	    	c.line("if ({0} == null)", elementId);
	    	c.line("{");
	    	c.tokenAdd2BeginOfLineIndent();
    	}
    	c.line("{2}{1} = new {0}();", getClassName(), elementId, (getContextElement() != null) ? (getClassName() + " ") : "");
    	c.line("{1}.Controller = this;", getClassName(), elementId);
    	c.line("{1}.Parent = parent;", getClassName(), elementId);
    	if (getContextElement() != null)
    	{
    		c.line("{1}.ContextElement = (parent != null) ? ((parent is IMVCViewDatasourcedElement) ? (IMVCViewDatasourcedElement)parent : parent.ContextElement) : null;", getClassName(), elementId);
    	}
    	c.line("{1}.Id = \"{0}\";", getId(), elementId);
    	if (getCss() != null)
    	{
    		c.line("{1}.CssClass = \"{0}\";", getCss(), elementId);
    	}
    	if (getIconCss() != null)
    	{
    		c.line("{1}.IconCssClass = \"{0}\";", getIconCss(), elementId);
    	}
    	if (getDraggable() != null)
    	{
    		c.line("{0}.Draggable = true;",  elementId);
    	}
    	if (getDroppable() != null)
    	{
    		c.line("{0}.Droppable = true;",  elementId);
    		c.line("{0}.DropSettings = new MVCViewDropSettings();",  elementId);
    		if (getDroppable().getAccepts() != null)
    		{
    			c.line("{0}.DropSettings.AcceptElementsSelector = \"{1}\";",  elementId, getDroppable().getAccepts().getElementsSelector());
    		}
    		if (getDroppable().getOnItemDropped() != null && getDroppable().getOnItemDropped().getTriggerList() != null && getDroppable().getOnItemDropped().getTriggerList().size() > 0)
    		{
    			for(MVCTrigger trigger : getDroppable().getOnItemDropped().getTriggerList())
    			{
    				trigger.writeInitializeTrigger(c, product, elementId, "DropSettings.OnItemDropped");
    			}
    		}
    	}
    	if (getHelpText() != null)
    	{
    		getHelpText().writeBind(c, elementId, "HelpTextValue", true);
    	}
    	if (getAttributes() != null && getAttributes().getAttribute() != null)
    	{
    		for (MVCViewElementAttribute att : getAttributes().getAttribute())
    		{
    			c.line("{0}.Attributes.Add(\"{1}\", \"{2}\");",  elementId, att.getName(), att.getValue());
    		}
    	}
    	c.line("{0}.ClientId = Model.ViewElementsPrefix + {0}.Id{1};", elementId, idSuffix);
    	
    	if (getOnMouseOver() != null)
    	{
    		for(MVCTrigger trigger : getOnMouseOver().getTriggerList())
			{
				trigger.writeInitializeTrigger(c, product, elementId, "OnMouseOver");
			}
    	}
    	if (this instanceof com.mda.engine.core.IMVCViewClickableElement)
    	{
    		com.mda.engine.core.IMVCViewClickableElement clickableElem = (com.mda.engine.core.IMVCViewClickableElement)this;
    		if (clickableElem.getOnClick() != null)
    		{
    			for(MVCTrigger trigger : clickableElem.getOnClick().getTriggerList())
    			{
    				trigger.writeInitializeTrigger(c, product, elementId, "OnClick");
    			}
    		}
    	}
    	if (this instanceof com.mda.engine.core.IMVCViewChangeableElement)
    	{
    		com.mda.engine.core.IMVCViewChangeableElement changeableElem = (com.mda.engine.core.IMVCViewChangeableElement)this;
    		if (changeableElem.getOnChange() != null)
    		{
    			for(MVCTrigger trigger : changeableElem.getOnChange().getTriggerList())
    			{
    				trigger.writeInitializeTrigger(c, product, elementId, "OnChange");
    			}
    		}
    		if (changeableElem.getOnKeyPress() != null)
    		{
    			for(MVCTrigger trigger : changeableElem.getOnKeyPress().getTriggerList())
    			{
    				trigger.writeInitializeTrigger(c, product, elementId, "OnKeyPress");
    			}
    		}
    		if (changeableElem.getOnKeyUp() != null)
    		{
    			for(MVCTrigger trigger : changeableElem.getOnKeyUp().getTriggerList())
    			{
    				trigger.writeInitializeTrigger(c, product, elementId, "OnKeyUp");
    			}
    		}
    		if (changeableElem.getOnFocus() != null)
    		{
    			for(MVCTrigger trigger : changeableElem.getOnFocus().getTriggerList())
    			{
    				trigger.writeInitializeTrigger(c, product, elementId, "OnFocus");
    			}
    		}
    		if (changeableElem.getOnOutFocus() != null)
    		{
    			for(MVCTrigger trigger : changeableElem.getOnOutFocus().getTriggerList())
    			{
    				trigger.writeInitializeTrigger(c, product, elementId, "OnOutFocus");
    			}
    		}
    		if (changeableElem.getOnKeyDown() != null)
    		{
    			for(MVCTrigger trigger : changeableElem.getOnKeyDown().getTriggerList())
    			{
    				trigger.writeInitializeTrigger(c, product, elementId, "OnKeyDown");
    			}
    		}
    	}
    	if (this instanceof com.mda.engine.core.IMVCViewElementWithDatasource)
    	{
    		com.mda.engine.core.IMVCViewElementWithDatasource dataSourceElem = (com.mda.engine.core.IMVCViewElementWithDatasource)this;
    		if (dataSourceElem.getDataSource() != null)
    		{
    			dataSourceElem.getDataSource().writeBindForDataSource(c, elementId);
    		}
			if (this instanceof MVCViewRepeater)
			{
				c.line("{0}.BindObjectCode = \"{1}\";",elementId,  dataSourceElem.getDataSource().getModelAttribute().getPath());
			}
			
    	}
    	if (this instanceof MVCViewLookup)
    	{
    		MVCViewLookup lkp = (MVCViewLookup)this;
    		if (lkp.getAutoCompleteAction() != null)
    		{
    			lkp.getAutoCompleteAction().writeInitializeTrigger(c, product, elementId, "LoadDataSourceTrigger");
    		}
    	}
    	if (this instanceof com.mda.engine.core.IMVCViewComposedElement)
    	{
    		com.mda.engine.core.IMVCViewComposedElement composedElement = (com.mda.engine.core.IMVCViewComposedElement)this;
    		
    		if (composedElement.getChildElements() != null && composedElement.getChildElements().getElementList() != null)
    		{
    			for (MVCViewElement elem : composedElement.getChildElements().getElementList())
    			{
    				if(elem.HasInitializeMethods()){    				
    					c.writeLine();
        	        	c.write("{0} {1} = ",elem.getClassName(), elem.getViewElementId());
        	        	elem.writeInitializeMethodCall(c, this.getViewElementId(), true, product, false);
        	        	c.writeLine();        	        	
    				}
    				
    				writeAddElementToChildren(c, elementId, elem);
    			}
    		}
    	}
    	writeInitializeMethodInternal(c, product);
    	if (getContextElement() == null)
    	{
	    	c.line("ViewData[\"{0}\"] = {1};", com.mda.engine.utils.StringUtils.getCamelCase(getId()), elementId);
	    	c.tokenRemove2BeginOfLine();
	    	c.line("}");
    	}
    	
    	this.writeSecurityTokens(elementId, product, c);
    	
    	c.line("if (!HasAccessToElement(\"{0}\"))", elementId);
        c.line("{");
        c.line("	{0}.IsVisible = false;", elementId);
        c.line("}");
        
    	c.line("return {0};", elementId);
    	c.tokenRemove2BeginOfLine();
    	c.line("}");
    	
    	writeExtraMethodsToInitialize(c, product);
    	
    	
    }
    
    public void writeAddElementToChildren(com.mda.engine.utils.Stringcode c, String elementId, MVCViewElement child) throws Exception
    {
    	if (child instanceof MVCViewRepeater)
    	{
    		MVCViewRepeater rep = (MVCViewRepeater)child;
    		
    		c.writeLine("{0}.ChildElements.AddRange({1}.GetViewElements<{2}>());", elementId, child.getViewElementId(), ((MVCViewRepeater) child).getTemplate().getElementList().get(0).getClassName());
    	}
    	else
    	{
    		c.writeLine("{1}.ChildElements.Add({0});", child.getViewElementId(), elementId);
    	}
    }
    
    public void writeExtraMethodsToInitialize(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product) throws Exception
    {
    	if (this instanceof com.mda.engine.core.IMVCViewComposedElement)
    	{
    		com.mda.engine.core.IMVCViewComposedElement composedElement = (com.mda.engine.core.IMVCViewComposedElement)this;
    		
    		if (composedElement.getChildElements() != null && composedElement.getChildElements().getElementList() != null)
    		{
    			for (MVCViewElement elem : composedElement.getChildElements().getElementList())
    			{
    				if(elem.HasInitializeMethods()){
        				c.line();
        				c.line("/// <summary>");
        		        c.line("/// Method to initialize the element '{0}'", elem.getId());
        		        c.line("/// </summary>");
        		        elem.writeInitializeMethod(c, product);    					
    				}
    			}
    		}
    	}
    	
    	if (this instanceof com.mda.engine.core.IMVCViewClickableElement)
    	{
    		com.mda.engine.core.IMVCViewClickableElement clickableElem = (com.mda.engine.core.IMVCViewClickableElement)this;
    		if (clickableElem.getOnClick() != null)
    		{
    			for(MVCTrigger trigger : clickableElem.getOnClick().getTriggerList())
    			{
    				if (trigger instanceof OpenToolTipTrigger)
    				{
    					OpenToolTipTrigger tooltipTrigger = (OpenToolTipTrigger)trigger;
						
    					c.line();
	    				c.line("/// <summary>");
	    		        c.line("/// Method to initialize content for the element '{0}'", this.getId());
	    		        c.line("/// </summary>");
	    		        java.util.HashMap<String, String> args = parseInitializeArguments("", "");
	    		    	String extraArguments = args.get("extraArguments");
	    		    	c.line("protected virtual List<IMVCViewElement> InitializeContentFor{0}(IMVCViewElement parent{1})", com.mda.engine.utils.StringUtils.getPascalCase(getId()), extraArguments);
	    		    	c.line("{");
	    		    	c.tokenAdd2BeginOfLineIndent();
	    		    	c.line("List<IMVCViewElement> elements = new List<IMVCViewElement>();");
	    		    	for(MVCViewElement child : tooltipTrigger.getContent().getElementList())
	    				{
	    		    		c.writeLine("");
	    		        	c.write("elements.Add(");
	    		        	child.writeInitializeMethodCall(c, null, false, product, false);
	    		        	c.write(");");
	    				}
	    		    	c.line("return elements;");
	    		    	c.tokenRemove2BeginOfLine();
	    		    	c.line("}");
	    		    	
	    		    	
						for(MVCViewElement child : tooltipTrigger.getContent().getElementList())
						{
							c.line();
		    				c.line("/// <summary>");
		    		        c.line("/// Method to initialize the element '{0}'", child.getId());
		    		        c.line("/// </summary>");
		    		        child.writeInitializeMethod(c, product);
						}
    					
    				}
    			}
    		}
    	}
    }
    
    public void writeInitializeMethodInternal(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product) throws Exception
    {
    	throw new org.apache.commons.lang.NotImplementedException("Missing writeInitializeMethodInternal() method implementation for: " + this.getClass().getName());
    }
    
    public void writeHtml(com.mda.engine.utils.Stringcode c)
    {
    	throw new org.apache.commons.lang.NotImplementedException("Missing writeInitializeMethodInternal() method implementation for: " + this.getClass().getName());
    }
    
    public void writeHtmlChildren(com.mda.engine.utils.Stringcode c)
    {
    	if (this instanceof com.mda.engine.core.IMVCViewComposedElement)
    	{
    		com.mda.engine.core.IMVCViewComposedElement parent = (com.mda.engine.core.IMVCViewComposedElement)this;
    		if (parent.getChildElements() != null)
    		{
    			c.tokenAdd2BeginOfLineIndent();
	    		for (MVCViewElement elem : parent.getChildElements().getElementList())
	    		{
	    			elem.writeHtml(c);
	    		}
	    		c.tokenRemove2BeginOfLine();
    		}
    	}
    }
    
    public void setBinds(MVCModel model) throws Exception
    {
    	if (getHelpText() != null)
    	{
    		getHelpText().setBinds(model, getContextElement(), this);
    	}
    	if (this instanceof com.mda.engine.core.IMVCViewComposedElement)
    	{
    		com.mda.engine.core.IMVCViewComposedElement comp = (com.mda.engine.core.IMVCViewComposedElement)this;
    		if (comp.getChildElements() != null && comp.getChildElements().getElementList() != null)
    		{
    			for (MVCViewElement child : comp.getChildElements().getElementList())
    			{
    				child.setContextElement(getContextElement());
    				child.setBinds(model);
    			}
    		}
    	}
    	if (this instanceof MVCViewLookup)
    	{
    		MVCViewLookup lkp = (MVCViewLookup)this;
    		if (lkp.getAutoCompleteAction() != null)
    		{
    			if (lkp.getAutoCompleteAction().getArguments() != null && lkp.getAutoCompleteAction().getArguments().getArgumentList() != null)
				{
					for (TriggerArgument arg : lkp.getAutoCompleteAction().getArguments().getArgumentList())
					{
						if (arg.getValue() != null)
						{
							arg.getValue().setBinds(model, getContextElement(), this);
						}
					}
				}
    		}
    	}
    	if (this instanceof com.mda.engine.core.IMVCViewClickableElement)
    	{
    		com.mda.engine.core.IMVCViewClickableElement clickableElem = (com.mda.engine.core.IMVCViewClickableElement)this;
    		if (clickableElem.getOnClick() != null)
    		{
    			for(MVCTrigger trigger : clickableElem.getOnClick().getTriggerList())
    			{
    				if (trigger.getArguments() != null && trigger.getArguments().getArgumentList() != null)
    				{
    					for (TriggerArgument arg : trigger.getArguments().getArgumentList())
    					{
    						if (arg.getValue() != null)
    						{
    							arg.getValue().setBinds(model, getContextElement(), this);
    						}
    					}
    				}
    				
    				if (trigger instanceof OpenToolTipTrigger)
    				{
    					OpenToolTipTrigger tooltipTrigger = (OpenToolTipTrigger)trigger;
						
						for(MVCViewElement child : tooltipTrigger.getContent().getElementList())
						{
							child.setContextElement(this.getContextElement());
							child.setBinds(model);
						}
    					
    				}
    			}
    		}
    	}
    	if (this instanceof com.mda.engine.core.IMVCViewChangeableElement)
    	{
    		com.mda.engine.core.IMVCViewChangeableElement changeableElem = (com.mda.engine.core.IMVCViewChangeableElement)this;
    		if (changeableElem.getOnChange() != null)
    		{
    			for(MVCTrigger trigger : changeableElem.getOnChange().getTriggerList())
    			{
    				if (trigger.getArguments() != null && trigger.getArguments().getArgumentList() != null)
    				{
    					for (TriggerArgument arg : trigger.getArguments().getArgumentList())
    					{
    						if (arg.getValue() != null)
    						{
    							arg.getValue().setBinds(model, getContextElement(), this);
    						}
    					}
    				}
    			}
    		}
    		if (changeableElem.getOnKeyPress() != null)
    		{
    			for(MVCTrigger trigger : changeableElem.getOnKeyPress().getTriggerList())
    			{
    				if (trigger.getArguments() != null && trigger.getArguments().getArgumentList() != null)
    				{
    					for (TriggerArgument arg : trigger.getArguments().getArgumentList())
    					{
    						if (arg.getValue() != null)
    						{
    							arg.getValue().setBinds(model, getContextElement(), this);
    						}
    					}
    				}
    			}
    		}
    		if (changeableElem.getOnKeyDown() != null)
    		{
    			for(MVCTrigger trigger : changeableElem.getOnKeyDown().getTriggerList())
    			{
    				if (trigger.getArguments() != null && trigger.getArguments().getArgumentList() != null)
    				{
    					for (TriggerArgument arg : trigger.getArguments().getArgumentList())
    					{
    						if (arg.getValue() != null)
    						{
    							arg.getValue().setBinds(model, getContextElement(), this);
    						}
    					}
    				}
    			}
    		}
    		if (changeableElem.getOnKeyUp() != null)
    		{
    			for(MVCTrigger trigger : changeableElem.getOnKeyUp().getTriggerList())
    			{
    				if (trigger.getArguments() != null && trigger.getArguments().getArgumentList() != null)
    				{
    					for (TriggerArgument arg : trigger.getArguments().getArgumentList())
    					{
    						if (arg.getValue() != null)
    						{
    							arg.getValue().setBinds(model, getContextElement(), this);
    						}
    					}
    				}
    			}
    		}
    		if (changeableElem.getOnFocus() != null)
    		{
    			for(MVCTrigger trigger : changeableElem.getOnFocus().getTriggerList())
    			{
    				if (trigger.getArguments() != null && trigger.getArguments().getArgumentList() != null)
    				{
    					for (TriggerArgument arg : trigger.getArguments().getArgumentList())
    					{
    						if (arg.getValue() != null)
    						{
    							arg.getValue().setBinds(model, getContextElement(), this);
    						}
    					}
    				}
    			}
    		}
    		if (changeableElem.getOnOutFocus() != null)
    		{
    			for(MVCTrigger trigger : changeableElem.getOnOutFocus().getTriggerList())
    			{
    				if (trigger.getArguments() != null && trigger.getArguments().getArgumentList() != null)
    				{
    					for (TriggerArgument arg : trigger.getArguments().getArgumentList())
    					{
    						if (arg.getValue() != null)
    						{
    							arg.getValue().setBinds(model, getContextElement(), this);
    						}
    					}
    				}
    			}
    		}
    	}
    }

	public void setContextElement(com.mda.engine.core.IMVCViewElementWithDatasource contextElement) {
		this.contextElement = contextElement;
	}

	public com.mda.engine.core.IMVCViewElementWithDatasource getContextElement() {
		return contextElement;
	}

	public String getHtmlTagName() {
		throw new org.apache.commons.lang.NotImplementedException("Missing getHtmlTagName() method implementation for: " + this.getClass().getName());
	}

	public void writeAttributes(com.mda.engine.utils.Stringcode c)
	{
		if (getAttributes() != null && getAttributes().getAttribute() != null)
    	{
    		for (MVCViewElementAttribute att : getAttributes().getAttribute())
    		{
    			c.write(" {0}=\"{1}\"", att.getName(), att.getValue());
    		}
    	}
	}
	
	private void writeSecurityTokens(String elementId, com.mda.engine.core.Product product, com.mda.engine.utils.Stringcode c){
		if(this.security != null && this.security.token != null){
			for(MVCViewSecurity token : this.security.token){
				String tokenKey = token.key.getCSharpExpression(product);
				String tokenValue = token.value.getCSharpExpression(product);
				
				c.write("{0}.AddSecurityToken({1}, {2});", elementId, tokenKey, tokenValue);
			}
		}
	}
	
	public boolean HasInitializeMethods(){
		return true;
	}
    
//--simple--preserve

}
