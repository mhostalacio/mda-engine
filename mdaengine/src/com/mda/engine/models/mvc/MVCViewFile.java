
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewFile complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewFile">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewInput">
 *       &lt;attribute name="AcceptOnlyImages" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="UploadActionName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="ImageWidth" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="ImageHeight" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="MaxFileSize" type="{http://www.w3.org/2001/XMLSchema}int" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewFile")
public class MVCViewFile
    extends MVCViewInput
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "AcceptOnlyImages")
    protected Boolean acceptOnlyImages;
    @XmlAttribute(name = "UploadActionName", required = true)
    protected String uploadActionName;
    @XmlAttribute(name = "ImageWidth")
    protected Integer imageWidth;
    @XmlAttribute(name = "ImageHeight")
    protected Integer imageHeight;
    @XmlAttribute(name = "MaxFileSize")
    protected Integer maxFileSize;

    /**
     * Gets the value of the acceptOnlyImages property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAcceptOnlyImages() {
        return acceptOnlyImages;
    }

    /**
     * Sets the value of the acceptOnlyImages property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAcceptOnlyImages(Boolean value) {
        this.acceptOnlyImages = value;
    }

    /**
     * Gets the value of the uploadActionName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUploadActionName() {
        return uploadActionName;
    }

    /**
     * Sets the value of the uploadActionName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUploadActionName(String value) {
        this.uploadActionName = value;
    }

    /**
     * Gets the value of the imageWidth property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getImageWidth() {
        return imageWidth;
    }

    /**
     * Sets the value of the imageWidth property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setImageWidth(Integer value) {
        this.imageWidth = value;
    }

    /**
     * Gets the value of the imageHeight property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getImageHeight() {
        return imageHeight;
    }

    /**
     * Sets the value of the imageHeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setImageHeight(Integer value) {
        this.imageHeight = value;
    }

    /**
     * Gets the value of the maxFileSize property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxFileSize() {
        return maxFileSize;
    }

    /**
     * Sets the value of the maxFileSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxFileSize(Integer value) {
        this.maxFileSize = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewFile)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewFile that = ((MVCViewFile) object);
        {
            Boolean lhsAcceptOnlyImages;
            lhsAcceptOnlyImages = this.isAcceptOnlyImages();
            Boolean rhsAcceptOnlyImages;
            rhsAcceptOnlyImages = that.isAcceptOnlyImages();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "acceptOnlyImages", lhsAcceptOnlyImages), LocatorUtils.property(thatLocator, "acceptOnlyImages", rhsAcceptOnlyImages), lhsAcceptOnlyImages, rhsAcceptOnlyImages)) {
                return false;
            }
        }
        {
            String lhsUploadActionName;
            lhsUploadActionName = this.getUploadActionName();
            String rhsUploadActionName;
            rhsUploadActionName = that.getUploadActionName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "uploadActionName", lhsUploadActionName), LocatorUtils.property(thatLocator, "uploadActionName", rhsUploadActionName), lhsUploadActionName, rhsUploadActionName)) {
                return false;
            }
        }
        {
            Integer lhsImageWidth;
            lhsImageWidth = this.getImageWidth();
            Integer rhsImageWidth;
            rhsImageWidth = that.getImageWidth();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "imageWidth", lhsImageWidth), LocatorUtils.property(thatLocator, "imageWidth", rhsImageWidth), lhsImageWidth, rhsImageWidth)) {
                return false;
            }
        }
        {
            Integer lhsImageHeight;
            lhsImageHeight = this.getImageHeight();
            Integer rhsImageHeight;
            rhsImageHeight = that.getImageHeight();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "imageHeight", lhsImageHeight), LocatorUtils.property(thatLocator, "imageHeight", rhsImageHeight), lhsImageHeight, rhsImageHeight)) {
                return false;
            }
        }
        {
            Integer lhsMaxFileSize;
            lhsMaxFileSize = this.getMaxFileSize();
            Integer rhsMaxFileSize;
            rhsMaxFileSize = that.getMaxFileSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "maxFileSize", lhsMaxFileSize), LocatorUtils.property(thatLocator, "maxFileSize", rhsMaxFileSize), lhsMaxFileSize, rhsMaxFileSize)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCViewFile) {
            final MVCViewFile copy = ((MVCViewFile) draftCopy);
            if (this.acceptOnlyImages!= null) {
                Boolean sourceAcceptOnlyImages;
                sourceAcceptOnlyImages = this.isAcceptOnlyImages();
                Boolean copyAcceptOnlyImages = ((Boolean) strategy.copy(LocatorUtils.property(locator, "acceptOnlyImages", sourceAcceptOnlyImages), sourceAcceptOnlyImages));
                copy.setAcceptOnlyImages(copyAcceptOnlyImages);
            } else {
                copy.acceptOnlyImages = null;
            }
            if (this.uploadActionName!= null) {
                String sourceUploadActionName;
                sourceUploadActionName = this.getUploadActionName();
                String copyUploadActionName = ((String) strategy.copy(LocatorUtils.property(locator, "uploadActionName", sourceUploadActionName), sourceUploadActionName));
                copy.setUploadActionName(copyUploadActionName);
            } else {
                copy.uploadActionName = null;
            }
            if (this.imageWidth!= null) {
                Integer sourceImageWidth;
                sourceImageWidth = this.getImageWidth();
                Integer copyImageWidth = ((Integer) strategy.copy(LocatorUtils.property(locator, "imageWidth", sourceImageWidth), sourceImageWidth));
                copy.setImageWidth(copyImageWidth);
            } else {
                copy.imageWidth = null;
            }
            if (this.imageHeight!= null) {
                Integer sourceImageHeight;
                sourceImageHeight = this.getImageHeight();
                Integer copyImageHeight = ((Integer) strategy.copy(LocatorUtils.property(locator, "imageHeight", sourceImageHeight), sourceImageHeight));
                copy.setImageHeight(copyImageHeight);
            } else {
                copy.imageHeight = null;
            }
            if (this.maxFileSize!= null) {
                Integer sourceMaxFileSize;
                sourceMaxFileSize = this.getMaxFileSize();
                Integer copyMaxFileSize = ((Integer) strategy.copy(LocatorUtils.property(locator, "maxFileSize", sourceMaxFileSize), sourceMaxFileSize));
                copy.setMaxFileSize(copyMaxFileSize);
            } else {
                copy.maxFileSize = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewFile();
    }
    
//--simple--preserve
    
    @Override
    public String getClassName() throws Exception
    {
    	return "MVCViewFile";
    }
    
    @Override
    public void writeHtml(com.mda.engine.utils.Stringcode c)
    {
    	c.write("<input type=\"file\"");
    	if (getId() != null)
    	{
    		c.write(" id=\"{0}\"", getId());
    	}
    	if (getCss() != null)
    	{
    		c.write(" class=\"{0}\"", getCss());
    	}
    	if (getValue() != null)
    	{
    		c.write(" value=\"");
    		writeHtmlInputValue(c);
    		c.write("\"");
    	}
    	c.write("/>");
    	c.writeLine();
    }
    
    @Override
    public void writeInitializeMethodInternal(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product)
    {
    	super.writeInitializeMethodInternal(c, product);
    	if (isAcceptOnlyImages() != null)
    	{
    		c.line("{0}.AcceptOnlyImages = {1};", getViewElementId(), isAcceptOnlyImages());
    	}
    	if (getUploadActionName() != null)
    	{
    		c.line("{0}.UploadActionName = \"{1}\";", getViewElementId(), getUploadActionName());
    	}
    	if (getImageWidth() != null)
    		c.line("{0}.ImageWidth = {1};", getViewElementId(), getImageWidth());
    	if (getImageHeight() != null)
    		c.line("{0}.ImageHeight = {1};", getViewElementId(), getImageHeight());
    	if (getMaxFileSize() != null)
    	{
    		c.line("{0}.MaxFileSize = {1};", getViewElementId(), getMaxFileSize());
    	}
    }
    
//--simple--preserve

}
