
package com.mda.engine.models.mvc;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MVCViewTableRowType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MVCViewTableRowType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Regular"/>
 *     &lt;enumeration value="Header"/>
 *     &lt;enumeration value="Footer"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MVCViewTableRowType")
@XmlEnum
public enum MVCViewTableRowType {

    @XmlEnumValue("Regular")
    REGULAR("Regular"),
    @XmlEnumValue("Header")
    HEADER("Header"),
    @XmlEnumValue("Footer")
    FOOTER("Footer");
    private final String value;

    MVCViewTableRowType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MVCViewTableRowType fromValue(String v) {
        for (MVCViewTableRowType c: MVCViewTableRowType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
