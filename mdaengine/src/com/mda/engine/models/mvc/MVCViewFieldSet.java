
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewFieldSet complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewFieldSet">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewComposedElement">
 *       &lt;sequence>
 *         &lt;element name="Legend" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewElementChoice" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewFieldSet", propOrder = {
    "legend"
})
public class MVCViewFieldSet
    extends MVCViewComposedElement
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Legend")
    protected MVCViewElementChoice legend;

    /**
     * Gets the value of the legend property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewElementChoice }
     *     
     */
    public MVCViewElementChoice getLegend() {
        return legend;
    }

    /**
     * Sets the value of the legend property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewElementChoice }
     *     
     */
    public void setLegend(MVCViewElementChoice value) {
        this.legend = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewFieldSet)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewFieldSet that = ((MVCViewFieldSet) object);
        {
            MVCViewElementChoice lhsLegend;
            lhsLegend = this.getLegend();
            MVCViewElementChoice rhsLegend;
            rhsLegend = that.getLegend();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "legend", lhsLegend), LocatorUtils.property(thatLocator, "legend", rhsLegend), lhsLegend, rhsLegend)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCViewFieldSet) {
            final MVCViewFieldSet copy = ((MVCViewFieldSet) draftCopy);
            if (this.legend!= null) {
                MVCViewElementChoice sourceLegend;
                sourceLegend = this.getLegend();
                MVCViewElementChoice copyLegend = ((MVCViewElementChoice) strategy.copy(LocatorUtils.property(locator, "legend", sourceLegend), sourceLegend));
                copy.setLegend(copyLegend);
            } else {
                copy.legend = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewFieldSet();
    }
    
//--simple--preserve
   

    @Override
    public void writeHtml(com.mda.engine.utils.Stringcode c)
    {
    	
    	c.write("<fieldset ");
    	if (getId() != null)
    	{
    		c.write(" id=\"{0}\"", getId());
    	}
    	if (getCss() != null)
    	{
    		c.write(" class=\"{0}\"", getCss());
    	}
    	c.write(">");
    	if (getLegend() != null)
    	{
    		c.write("<legend> ");
    		getLegend().writeHtml(c);
    		c.write("</legend> ");
    	}
    	c.writeLine();
    	writeHtmlChildren(c);

    	c.write("</fieldset>");
    	c.writeLine();
    }
    
    @Override
    public String getClassName() throws Exception
    {
    	return "MVCViewFieldset";
    }
    
    
//--simple--preserve

}
