
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for OnSelectTrigger complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OnSelectTrigger">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SetHiddenValue" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValueChoice"/>
 *         &lt;element name="SetDisplayValue" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewValueChoice"/>
 *         &lt;element name="Triggers" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCTriggerCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OnSelectTrigger", propOrder = {
    "setHiddenValue",
    "setDisplayValue",
    "triggers"
})
public class OnSelectTrigger
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "SetHiddenValue", required = true)
    protected MVCViewValueChoice setHiddenValue;
    @XmlElement(name = "SetDisplayValue", required = true)
    protected MVCViewValueChoice setDisplayValue;
    @XmlElement(name = "Triggers")
    protected MVCTriggerCollection triggers;

    /**
     * Gets the value of the setHiddenValue property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public MVCViewValueChoice getSetHiddenValue() {
        return setHiddenValue;
    }

    /**
     * Sets the value of the setHiddenValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public void setSetHiddenValue(MVCViewValueChoice value) {
        this.setHiddenValue = value;
    }

    /**
     * Gets the value of the setDisplayValue property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public MVCViewValueChoice getSetDisplayValue() {
        return setDisplayValue;
    }

    /**
     * Sets the value of the setDisplayValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewValueChoice }
     *     
     */
    public void setSetDisplayValue(MVCViewValueChoice value) {
        this.setDisplayValue = value;
    }

    /**
     * Gets the value of the triggers property.
     * 
     * @return
     *     possible object is
     *     {@link MVCTriggerCollection }
     *     
     */
    public MVCTriggerCollection getTriggers() {
        return triggers;
    }

    /**
     * Sets the value of the triggers property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCTriggerCollection }
     *     
     */
    public void setTriggers(MVCTriggerCollection value) {
        this.triggers = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof OnSelectTrigger)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final OnSelectTrigger that = ((OnSelectTrigger) object);
        {
            MVCViewValueChoice lhsSetHiddenValue;
            lhsSetHiddenValue = this.getSetHiddenValue();
            MVCViewValueChoice rhsSetHiddenValue;
            rhsSetHiddenValue = that.getSetHiddenValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "setHiddenValue", lhsSetHiddenValue), LocatorUtils.property(thatLocator, "setHiddenValue", rhsSetHiddenValue), lhsSetHiddenValue, rhsSetHiddenValue)) {
                return false;
            }
        }
        {
            MVCViewValueChoice lhsSetDisplayValue;
            lhsSetDisplayValue = this.getSetDisplayValue();
            MVCViewValueChoice rhsSetDisplayValue;
            rhsSetDisplayValue = that.getSetDisplayValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "setDisplayValue", lhsSetDisplayValue), LocatorUtils.property(thatLocator, "setDisplayValue", rhsSetDisplayValue), lhsSetDisplayValue, rhsSetDisplayValue)) {
                return false;
            }
        }
        {
            MVCTriggerCollection lhsTriggers;
            lhsTriggers = this.getTriggers();
            MVCTriggerCollection rhsTriggers;
            rhsTriggers = that.getTriggers();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "triggers", lhsTriggers), LocatorUtils.property(thatLocator, "triggers", rhsTriggers), lhsTriggers, rhsTriggers)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof OnSelectTrigger) {
            final OnSelectTrigger copy = ((OnSelectTrigger) draftCopy);
            if (this.setHiddenValue!= null) {
                MVCViewValueChoice sourceSetHiddenValue;
                sourceSetHiddenValue = this.getSetHiddenValue();
                MVCViewValueChoice copySetHiddenValue = ((MVCViewValueChoice) strategy.copy(LocatorUtils.property(locator, "setHiddenValue", sourceSetHiddenValue), sourceSetHiddenValue));
                copy.setSetHiddenValue(copySetHiddenValue);
            } else {
                copy.setHiddenValue = null;
            }
            if (this.setDisplayValue!= null) {
                MVCViewValueChoice sourceSetDisplayValue;
                sourceSetDisplayValue = this.getSetDisplayValue();
                MVCViewValueChoice copySetDisplayValue = ((MVCViewValueChoice) strategy.copy(LocatorUtils.property(locator, "setDisplayValue", sourceSetDisplayValue), sourceSetDisplayValue));
                copy.setSetDisplayValue(copySetDisplayValue);
            } else {
                copy.setDisplayValue = null;
            }
            if (this.triggers!= null) {
                MVCTriggerCollection sourceTriggers;
                sourceTriggers = this.getTriggers();
                MVCTriggerCollection copyTriggers = ((MVCTriggerCollection) strategy.copy(LocatorUtils.property(locator, "triggers", sourceTriggers), sourceTriggers));
                copy.setTriggers(copyTriggers);
            } else {
                copy.triggers = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new OnSelectTrigger();
    }

}
