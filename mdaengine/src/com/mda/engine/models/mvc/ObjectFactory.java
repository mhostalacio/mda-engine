
package com.mda.engine.models.mvc;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.mda.engine.models.mvc package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MVC_QNAME = new QName("http://www.mdaengine.com/mdaengine/models/mvc", "MVC");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.mda.engine.models.mvc
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MVCViewSecurity }
     * 
     */
    public MVCViewSecurity createMVCViewSecurity() {
        return new MVCViewSecurity();
    }

    /**
     * Create an instance of {@link MVCViewTimeline }
     * 
     */
    public MVCViewTimeline createMVCViewTimeline() {
        return new MVCViewTimeline();
    }

    /**
     * Create an instance of {@link MVCViewHR }
     * 
     */
    public MVCViewHR createMVCViewHR() {
        return new MVCViewHR();
    }

    /**
     * Create an instance of {@link MVCViewULChoice }
     * 
     */
    public MVCViewULChoice createMVCViewULChoice() {
        return new MVCViewULChoice();
    }

    /**
     * Create an instance of {@link MVCViewCheckBox }
     * 
     */
    public MVCViewCheckBox createMVCViewCheckBox() {
        return new MVCViewCheckBox();
    }

    /**
     * Create an instance of {@link MVCViewRepeaterTemplate }
     * 
     */
    public MVCViewRepeaterTemplate createMVCViewRepeaterTemplate() {
        return new MVCViewRepeaterTemplate();
    }

    /**
     * Create an instance of {@link MVCControllerValidationGroupCollection }
     * 
     */
    public MVCControllerValidationGroupCollection createMVCControllerValidationGroupCollection() {
        return new MVCControllerValidationGroupCollection();
    }

    /**
     * Create an instance of {@link MVCTriggerCollection }
     * 
     */
    public MVCTriggerCollection createMVCTriggerCollection() {
        return new MVCTriggerCollection();
    }

    /**
     * Create an instance of {@link MVCControllerValidationGroupModel }
     * 
     */
    public MVCControllerValidationGroupModel createMVCControllerValidationGroupModel() {
        return new MVCControllerValidationGroupModel();
    }

    /**
     * Create an instance of {@link ParentMVCType }
     * 
     */
    public ParentMVCType createParentMVCType() {
        return new ParentMVCType();
    }

    /**
     * Create an instance of {@link MVCViewRepeater }
     * 
     */
    public MVCViewRepeater createMVCViewRepeater() {
        return new MVCViewRepeater();
    }

    /**
     * Create an instance of {@link MVCViewElementCollection }
     * 
     */
    public MVCViewElementCollection createMVCViewElementCollection() {
        return new MVCViewElementCollection();
    }

    /**
     * Create an instance of {@link MVCView }
     * 
     */
    public MVCView createMVCView() {
        return new MVCView();
    }

    /**
     * Create an instance of {@link MVCControllerGetAction }
     * 
     */
    public MVCControllerGetAction createMVCControllerGetAction() {
        return new MVCControllerGetAction();
    }

    /**
     * Create an instance of {@link OpenToolTipTrigger }
     * 
     */
    public OpenToolTipTrigger createOpenToolTipTrigger() {
        return new OpenToolTipTrigger();
    }

    /**
     * Create an instance of {@link MVCViewNumericBox }
     * 
     */
    public MVCViewNumericBox createMVCViewNumericBox() {
        return new MVCViewNumericBox();
    }

    /**
     * Create an instance of {@link AcceptDropSettings }
     * 
     */
    public AcceptDropSettings createAcceptDropSettings() {
        return new AcceptDropSettings();
    }

    /**
     * Create an instance of {@link MVCViewDiv }
     * 
     */
    public MVCViewDiv createMVCViewDiv() {
        return new MVCViewDiv();
    }

    /**
     * Create an instance of {@link MVCUrlDefinition }
     * 
     */
    public MVCUrlDefinition createMVCUrlDefinition() {
        return new MVCUrlDefinition();
    }

    /**
     * Create an instance of {@link MVCViewValueStaticStringValue }
     * 
     */
    public MVCViewValueStaticStringValue createMVCViewValueStaticStringValue() {
        return new MVCViewValueStaticStringValue();
    }

    /**
     * Create an instance of {@link MVCViewLI }
     * 
     */
    public MVCViewLI createMVCViewLI() {
        return new MVCViewLI();
    }

    /**
     * Create an instance of {@link MVCHrefChoice }
     * 
     */
    public MVCHrefChoice createMVCHrefChoice() {
        return new MVCHrefChoice();
    }

    /**
     * Create an instance of {@link MVCModel }
     * 
     */
    public MVCModel createMVCModel() {
        return new MVCModel();
    }

    /**
     * Create an instance of {@link MVCViewNav }
     * 
     */
    public MVCViewNav createMVCViewNav() {
        return new MVCViewNav();
    }

    /**
     * Create an instance of {@link TriggerArgumentsCollection }
     * 
     */
    public TriggerArgumentsCollection createTriggerArgumentsCollection() {
        return new TriggerArgumentsCollection();
    }

    /**
     * Create an instance of {@link MVCController }
     * 
     */
    public MVCController createMVCController() {
        return new MVCController();
    }

    /**
     * Create an instance of {@link MVCViewScript }
     * 
     */
    public MVCViewScript createMVCViewScript() {
        return new MVCViewScript();
    }

    /**
     * Create an instance of {@link MVCViewBreakLine }
     * 
     */
    public MVCViewBreakLine createMVCViewBreakLine() {
        return new MVCViewBreakLine();
    }

    /**
     * Create an instance of {@link MVCViewElementChoice }
     * 
     */
    public MVCViewElementChoice createMVCViewElementChoice() {
        return new MVCViewElementChoice();
    }

    /**
     * Create an instance of {@link MVCViewUL }
     * 
     */
    public MVCViewUL createMVCViewUL() {
        return new MVCViewUL();
    }

    /**
     * Create an instance of {@link MVCViewHidden }
     * 
     */
    public MVCViewHidden createMVCViewHidden() {
        return new MVCViewHidden();
    }

    /**
     * Create an instance of {@link MVCViewRadioButtonGroup }
     * 
     */
    public MVCViewRadioButtonGroup createMVCViewRadioButtonGroup() {
        return new MVCViewRadioButtonGroup();
    }

    /**
     * Create an instance of {@link MVCViewFile }
     * 
     */
    public MVCViewFile createMVCViewFile() {
        return new MVCViewFile();
    }

    /**
     * Create an instance of {@link MVCViewLookup }
     * 
     */
    public MVCViewLookup createMVCViewLookup() {
        return new MVCViewLookup();
    }

    /**
     * Create an instance of {@link MVCViewTableCellChoice }
     * 
     */
    public MVCViewTableCellChoice createMVCViewTableCellChoice() {
        return new MVCViewTableCellChoice();
    }

    /**
     * Create an instance of {@link MVCViewDateBox }
     * 
     */
    public MVCViewDateBox createMVCViewDateBox() {
        return new MVCViewDateBox();
    }

    /**
     * Create an instance of {@link MVCViewLiteral }
     * 
     */
    public MVCViewLiteral createMVCViewLiteral() {
        return new MVCViewLiteral();
    }

    /**
     * Create an instance of {@link MVCViewTextBox }
     * 
     */
    public MVCViewTextBox createMVCViewTextBox() {
        return new MVCViewTextBox();
    }

    /**
     * Create an instance of {@link MVCViewSpan }
     * 
     */
    public MVCViewSpan createMVCViewSpan() {
        return new MVCViewSpan();
    }

    /**
     * Create an instance of {@link DropSettings }
     * 
     */
    public DropSettings createDropSettings() {
        return new DropSettings();
    }

    /**
     * Create an instance of {@link MVCViewTable }
     * 
     */
    public MVCViewTable createMVCViewTable() {
        return new MVCViewTable();
    }

    /**
     * Create an instance of {@link MVCViewSecurityCollection }
     * 
     */
    public MVCViewSecurityCollection createMVCViewSecurityCollection() {
        return new MVCViewSecurityCollection();
    }

    /**
     * Create an instance of {@link DragSettings }
     * 
     */
    public DragSettings createDragSettings() {
        return new DragSettings();
    }

    /**
     * Create an instance of {@link MVCViewValueLocalizedStringValue }
     * 
     */
    public MVCViewValueLocalizedStringValue createMVCViewValueLocalizedStringValue() {
        return new MVCViewValueLocalizedStringValue();
    }

    /**
     * Create an instance of {@link MVCViewElementAttributesCollection }
     * 
     */
    public MVCViewElementAttributesCollection createMVCViewElementAttributesCollection() {
        return new MVCViewElementAttributesCollection();
    }

    /**
     * Create an instance of {@link ActionArgumentsCollection }
     * 
     */
    public ActionArgumentsCollection createActionArgumentsCollection() {
        return new ActionArgumentsCollection();
    }

    /**
     * Create an instance of {@link OnSelectTrigger }
     * 
     */
    public OnSelectTrigger createOnSelectTrigger() {
        return new OnSelectTrigger();
    }

    /**
     * Create an instance of {@link MVCViewObject }
     * 
     */
    public MVCViewObject createMVCViewObject() {
        return new MVCViewObject();
    }

    /**
     * Create an instance of {@link MVCViewHeader }
     * 
     */
    public MVCViewHeader createMVCViewHeader() {
        return new MVCViewHeader();
    }

    /**
     * Create an instance of {@link PostActionTrigger }
     * 
     */
    public PostActionTrigger createPostActionTrigger() {
        return new PostActionTrigger();
    }

    /**
     * Create an instance of {@link MVCViewValueStaticBooleanValue }
     * 
     */
    public MVCViewValueStaticBooleanValue createMVCViewValueStaticBooleanValue() {
        return new MVCViewValueStaticBooleanValue();
    }

    /**
     * Create an instance of {@link MVCViewH5 }
     * 
     */
    public MVCViewH5 createMVCViewH5() {
        return new MVCViewH5();
    }

    /**
     * Create an instance of {@link MVCControllerValidationGroup }
     * 
     */
    public MVCControllerValidationGroup createMVCControllerValidationGroup() {
        return new MVCControllerValidationGroup();
    }

    /**
     * Create an instance of {@link MVCViewRadioButton }
     * 
     */
    public MVCViewRadioButton createMVCViewRadioButton() {
        return new MVCViewRadioButton();
    }

    /**
     * Create an instance of {@link TriggerArgument }
     * 
     */
    public TriggerArgument createTriggerArgument() {
        return new TriggerArgument();
    }

    /**
     * Create an instance of {@link MVCViewPaginator }
     * 
     */
    public MVCViewPaginator createMVCViewPaginator() {
        return new MVCViewPaginator();
    }

    /**
     * Create an instance of {@link MVCViewH2 }
     * 
     */
    public MVCViewH2 createMVCViewH2() {
        return new MVCViewH2();
    }

    /**
     * Create an instance of {@link OpenTabTrigger }
     * 
     */
    public OpenTabTrigger createOpenTabTrigger() {
        return new OpenTabTrigger();
    }

    /**
     * Create an instance of {@link MVCViewP }
     * 
     */
    public MVCViewP createMVCViewP() {
        return new MVCViewP();
    }

    /**
     * Create an instance of {@link MVCViewForm }
     * 
     */
    public MVCViewForm createMVCViewForm() {
        return new MVCViewForm();
    }

    /**
     * Create an instance of {@link MVCViewValueChoice }
     * 
     */
    public MVCViewValueChoice createMVCViewValueChoice() {
        return new MVCViewValueChoice();
    }

    /**
     * Create an instance of {@link MVCControllerActionCollection }
     * 
     */
    public MVCControllerActionCollection createMVCControllerActionCollection() {
        return new MVCControllerActionCollection();
    }

    /**
     * Create an instance of {@link MVCViewH6 }
     * 
     */
    public MVCViewH6 createMVCViewH6() {
        return new MVCViewH6();
    }

    /**
     * Create an instance of {@link MVCControllerPostAction }
     * 
     */
    public MVCControllerPostAction createMVCControllerPostAction() {
        return new MVCControllerPostAction();
    }

    /**
     * Create an instance of {@link MVCViewAside }
     * 
     */
    public MVCViewAside createMVCViewAside() {
        return new MVCViewAside();
    }

    /**
     * Create an instance of {@link RedirectTrigger }
     * 
     */
    public RedirectTrigger createRedirectTrigger() {
        return new RedirectTrigger();
    }

    /**
     * Create an instance of {@link MVCViewImage }
     * 
     */
    public MVCViewImage createMVCViewImage() {
        return new MVCViewImage();
    }

    /**
     * Create an instance of {@link MVCViewElementReference }
     * 
     */
    public MVCViewElementReference createMVCViewElementReference() {
        return new MVCViewElementReference();
    }

    /**
     * Create an instance of {@link MVCViewLabel }
     * 
     */
    public MVCViewLabel createMVCViewLabel() {
        return new MVCViewLabel();
    }

    /**
     * Create an instance of {@link MVCViewButton }
     * 
     */
    public MVCViewButton createMVCViewButton() {
        return new MVCViewButton();
    }

    /**
     * Create an instance of {@link MVCViewSecurityTokenValueChoice }
     * 
     */
    public MVCViewSecurityTokenValueChoice createMVCViewSecurityTokenValueChoice() {
        return new MVCViewSecurityTokenValueChoice();
    }

    /**
     * Create an instance of {@link MVCViewCalendar }
     * 
     */
    public MVCViewCalendar createMVCViewCalendar() {
        return new MVCViewCalendar();
    }

    /**
     * Create an instance of {@link OpenPopupTrigger }
     * 
     */
    public OpenPopupTrigger createOpenPopupTrigger() {
        return new OpenPopupTrigger();
    }

    /**
     * Create an instance of {@link MVCViewValueModelAttribute }
     * 
     */
    public MVCViewValueModelAttribute createMVCViewValueModelAttribute() {
        return new MVCViewValueModelAttribute();
    }

    /**
     * Create an instance of {@link MVCViewSecurityTokenKeyChoice }
     * 
     */
    public MVCViewSecurityTokenKeyChoice createMVCViewSecurityTokenKeyChoice() {
        return new MVCViewSecurityTokenKeyChoice();
    }

    /**
     * Create an instance of {@link MVCViewDateTimeBox }
     * 
     */
    public MVCViewDateTimeBox createMVCViewDateTimeBox() {
        return new MVCViewDateTimeBox();
    }

    /**
     * Create an instance of {@link MVCViewTableRowChoice }
     * 
     */
    public MVCViewTableRowChoice createMVCViewTableRowChoice() {
        return new MVCViewTableRowChoice();
    }

    /**
     * Create an instance of {@link MVCViewTreeView }
     * 
     */
    public MVCViewTreeView createMVCViewTreeView() {
        return new MVCViewTreeView();
    }

    /**
     * Create an instance of {@link CallActionTrigger }
     * 
     */
    public CallActionTrigger createCallActionTrigger() {
        return new CallActionTrigger();
    }

    /**
     * Create an instance of {@link MVCViewInclude }
     * 
     */
    public MVCViewInclude createMVCViewInclude() {
        return new MVCViewInclude();
    }

    /**
     * Create an instance of {@link MVCViewH4 }
     * 
     */
    public MVCViewH4 createMVCViewH4() {
        return new MVCViewH4();
    }

    /**
     * Create an instance of {@link MVCViewElementValue }
     * 
     */
    public MVCViewElementValue createMVCViewElementValue() {
        return new MVCViewElementValue();
    }

    /**
     * Create an instance of {@link MVCViewDataSourceIndexValue }
     * 
     */
    public MVCViewDataSourceIndexValue createMVCViewDataSourceIndexValue() {
        return new MVCViewDataSourceIndexValue();
    }

    /**
     * Create an instance of {@link MVCInstance }
     * 
     */
    public MVCInstance createMVCInstance() {
        return new MVCInstance();
    }

    /**
     * Create an instance of {@link MVCViewFieldSet }
     * 
     */
    public MVCViewFieldSet createMVCViewFieldSet() {
        return new MVCViewFieldSet();
    }

    /**
     * Create an instance of {@link MVCViewH1 }
     * 
     */
    public MVCViewH1 createMVCViewH1() {
        return new MVCViewH1();
    }

    /**
     * Create an instance of {@link MVCViewDropDown }
     * 
     */
    public MVCViewDropDown createMVCViewDropDown() {
        return new MVCViewDropDown();
    }

    /**
     * Create an instance of {@link MVCViewH3 }
     * 
     */
    public MVCViewH3 createMVCViewH3() {
        return new MVCViewH3();
    }

    /**
     * Create an instance of {@link JavaScriptActionTrigger }
     * 
     */
    public JavaScriptActionTrigger createJavaScriptActionTrigger() {
        return new JavaScriptActionTrigger();
    }

    /**
     * Create an instance of {@link MVCViewContentPlaceHolder }
     * 
     */
    public MVCViewContentPlaceHolder createMVCViewContentPlaceHolder() {
        return new MVCViewContentPlaceHolder();
    }

    /**
     * Create an instance of {@link MVCViewHtmlEditor }
     * 
     */
    public MVCViewHtmlEditor createMVCViewHtmlEditor() {
        return new MVCViewHtmlEditor();
    }

    /**
     * Create an instance of {@link MVCViewArticle }
     * 
     */
    public MVCViewArticle createMVCViewArticle() {
        return new MVCViewArticle();
    }

    /**
     * Create an instance of {@link MVCViewLink }
     * 
     */
    public MVCViewLink createMVCViewLink() {
        return new MVCViewLink();
    }

    /**
     * Create an instance of {@link MVCViewCarousel }
     * 
     */
    public MVCViewCarousel createMVCViewCarousel() {
        return new MVCViewCarousel();
    }

    /**
     * Create an instance of {@link MVCViewTextArea }
     * 
     */
    public MVCViewTextArea createMVCViewTextArea() {
        return new MVCViewTextArea();
    }

    /**
     * Create an instance of {@link MVCViewElementAttribute }
     * 
     */
    public MVCViewElementAttribute createMVCViewElementAttribute() {
        return new MVCViewElementAttribute();
    }

    /**
     * Create an instance of {@link MVCViewTableCell }
     * 
     */
    public MVCViewTableCell createMVCViewTableCell() {
        return new MVCViewTableCell();
    }

    /**
     * Create an instance of {@link MVCViewTableRow }
     * 
     */
    public MVCViewTableRow createMVCViewTableRow() {
        return new MVCViewTableRow();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MVCInstance }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.mdaengine.com/mdaengine/models/mvc", name = "MVC")
    public JAXBElement<MVCInstance> createMVC(MVCInstance value) {
        return new JAXBElement<MVCInstance>(_MVC_QNAME, MVCInstance.class, null, value);
    }

}
