
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCControllerGetAction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCControllerGetAction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCControllerAction">
 *       &lt;attribute name="AcceptMethod" type="{http://www.mdaengine.com/mdaengine/models/mvc}AcceptVerbsEnum" default="Get" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCControllerGetAction")
public class MVCControllerGetAction
    extends MVCControllerAction
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "AcceptMethod")
    protected AcceptVerbsEnum acceptMethod;

    /**
     * Gets the value of the acceptMethod property.
     * 
     * @return
     *     possible object is
     *     {@link AcceptVerbsEnum }
     *     
     */
    public AcceptVerbsEnum getAcceptMethod() {
        if (acceptMethod == null) {
            return AcceptVerbsEnum.GET;
        } else {
            return acceptMethod;
        }
    }

    /**
     * Sets the value of the acceptMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link AcceptVerbsEnum }
     *     
     */
    public void setAcceptMethod(AcceptVerbsEnum value) {
        this.acceptMethod = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCControllerGetAction)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCControllerGetAction that = ((MVCControllerGetAction) object);
        {
            AcceptVerbsEnum lhsAcceptMethod;
            lhsAcceptMethod = this.getAcceptMethod();
            AcceptVerbsEnum rhsAcceptMethod;
            rhsAcceptMethod = that.getAcceptMethod();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "acceptMethod", lhsAcceptMethod), LocatorUtils.property(thatLocator, "acceptMethod", rhsAcceptMethod), lhsAcceptMethod, rhsAcceptMethod)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCControllerGetAction) {
            final MVCControllerGetAction copy = ((MVCControllerGetAction) draftCopy);
            if (this.acceptMethod!= null) {
                AcceptVerbsEnum sourceAcceptMethod;
                sourceAcceptMethod = this.getAcceptMethod();
                AcceptVerbsEnum copyAcceptMethod = ((AcceptVerbsEnum) strategy.copy(LocatorUtils.property(locator, "acceptMethod", sourceAcceptMethod), sourceAcceptMethod));
                copy.setAcceptMethod(copyAcceptMethod);
            } else {
                copy.acceptMethod = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCControllerGetAction();
    }

}
