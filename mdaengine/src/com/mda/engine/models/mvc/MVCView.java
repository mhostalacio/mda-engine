
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCView complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCView">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Elements" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewElementCollection" minOccurs="0"/>
 *         &lt;element name="HTML" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewElementCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="MasterPageName" type="{http://www.w3.org/2001/XMLSchema}string" default="Default" />
 *       &lt;attribute name="PopupMasterPage" type="{http://www.w3.org/2001/XMLSchema}string" default="" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCView", propOrder = {
    "elements",
    "html"
})
public class MVCView
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Elements")
    protected MVCViewElementCollection elements;
    @XmlElement(name = "HTML")
    protected MVCViewElementCollection html;
    @XmlAttribute(name = "MasterPageName")
    protected String masterPageName;
    @XmlAttribute(name = "PopupMasterPage")
    protected String popupMasterPage;

    /**
     * Gets the value of the elements property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewElementCollection }
     *     
     */
    public MVCViewElementCollection getElements() {
        return elements;
    }

    /**
     * Sets the value of the elements property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewElementCollection }
     *     
     */
    public void setElements(MVCViewElementCollection value) {
        this.elements = value;
    }

    /**
     * Gets the value of the html property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewElementCollection }
     *     
     */
    public MVCViewElementCollection getHTML() {
        return html;
    }

    /**
     * Sets the value of the html property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewElementCollection }
     *     
     */
    public void setHTML(MVCViewElementCollection value) {
        this.html = value;
    }

    /**
     * Gets the value of the masterPageName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMasterPageName() {
        if (masterPageName == null) {
            return "Default";
        } else {
            return masterPageName;
        }
    }

    /**
     * Sets the value of the masterPageName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMasterPageName(String value) {
        this.masterPageName = value;
    }

    /**
     * Gets the value of the popupMasterPage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPopupMasterPage() {
        if (popupMasterPage == null) {
            return "";
        } else {
            return popupMasterPage;
        }
    }

    /**
     * Sets the value of the popupMasterPage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPopupMasterPage(String value) {
        this.popupMasterPage = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCView)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final MVCView that = ((MVCView) object);
        {
            MVCViewElementCollection lhsElements;
            lhsElements = this.getElements();
            MVCViewElementCollection rhsElements;
            rhsElements = that.getElements();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "elements", lhsElements), LocatorUtils.property(thatLocator, "elements", rhsElements), lhsElements, rhsElements)) {
                return false;
            }
        }
        {
            MVCViewElementCollection lhsHTML;
            lhsHTML = this.getHTML();
            MVCViewElementCollection rhsHTML;
            rhsHTML = that.getHTML();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "html", lhsHTML), LocatorUtils.property(thatLocator, "html", rhsHTML), lhsHTML, rhsHTML)) {
                return false;
            }
        }
        {
            String lhsMasterPageName;
            lhsMasterPageName = this.getMasterPageName();
            String rhsMasterPageName;
            rhsMasterPageName = that.getMasterPageName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "masterPageName", lhsMasterPageName), LocatorUtils.property(thatLocator, "masterPageName", rhsMasterPageName), lhsMasterPageName, rhsMasterPageName)) {
                return false;
            }
        }
        {
            String lhsPopupMasterPage;
            lhsPopupMasterPage = this.getPopupMasterPage();
            String rhsPopupMasterPage;
            rhsPopupMasterPage = that.getPopupMasterPage();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "popupMasterPage", lhsPopupMasterPage), LocatorUtils.property(thatLocator, "popupMasterPage", rhsPopupMasterPage), lhsPopupMasterPage, rhsPopupMasterPage)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof MVCView) {
            final MVCView copy = ((MVCView) draftCopy);
            if (this.elements!= null) {
                MVCViewElementCollection sourceElements;
                sourceElements = this.getElements();
                MVCViewElementCollection copyElements = ((MVCViewElementCollection) strategy.copy(LocatorUtils.property(locator, "elements", sourceElements), sourceElements));
                copy.setElements(copyElements);
            } else {
                copy.elements = null;
            }
            if (this.html!= null) {
                MVCViewElementCollection sourceHTML;
                sourceHTML = this.getHTML();
                MVCViewElementCollection copyHTML = ((MVCViewElementCollection) strategy.copy(LocatorUtils.property(locator, "html", sourceHTML), sourceHTML));
                copy.setHTML(copyHTML);
            } else {
                copy.html = null;
            }
            if (this.masterPageName!= null) {
                String sourceMasterPageName;
                sourceMasterPageName = this.getMasterPageName();
                String copyMasterPageName = ((String) strategy.copy(LocatorUtils.property(locator, "masterPageName", sourceMasterPageName), sourceMasterPageName));
                copy.setMasterPageName(copyMasterPageName);
            } else {
                copy.masterPageName = null;
            }
            if (this.popupMasterPage!= null) {
                String sourcePopupMasterPage;
                sourcePopupMasterPage = this.getPopupMasterPage();
                String copyPopupMasterPage = ((String) strategy.copy(LocatorUtils.property(locator, "popupMasterPage", sourcePopupMasterPage), sourcePopupMasterPage));
                copy.setPopupMasterPage(copyPopupMasterPage);
            } else {
                copy.popupMasterPage = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCView();
    }

}
