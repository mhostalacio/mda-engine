
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.IMVCViewComposedElement;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewUL complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewUL">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewElement">
 *       &lt;sequence>
 *         &lt;element name="Items" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewULChoice" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewUL", propOrder = {
    "items"
})
@XmlRootElement
public class MVCViewUL
    extends MVCViewElement
    implements Serializable, Cloneable, IMVCViewComposedElement, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Items")
    protected MVCViewULChoice items;

    /**
     * Gets the value of the items property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewULChoice }
     *     
     */
    public MVCViewULChoice getItems() {
        return items;
    }

    /**
     * Sets the value of the items property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewULChoice }
     *     
     */
    public void setItems(MVCViewULChoice value) {
        this.items = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewUL)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewUL that = ((MVCViewUL) object);
        {
            MVCViewULChoice lhsItems;
            lhsItems = this.getItems();
            MVCViewULChoice rhsItems;
            rhsItems = that.getItems();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "items", lhsItems), LocatorUtils.property(thatLocator, "items", rhsItems), lhsItems, rhsItems)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCViewUL) {
            final MVCViewUL copy = ((MVCViewUL) draftCopy);
            if (this.items!= null) {
                MVCViewULChoice sourceItems;
                sourceItems = this.getItems();
                MVCViewULChoice copyItems = ((MVCViewULChoice) strategy.copy(LocatorUtils.property(locator, "items", sourceItems), sourceItems));
                copy.setItems(copyItems);
            } else {
                copy.items = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewUL();
    }
    
//--simple--preserve
    private transient MVCViewElementCollection elements;
    @Override
    public MVCViewElementCollection getChildElements()
    {
    	if (elements == null)
    	{
    		elements = new MVCViewElementCollection();
    		if (this.getItems() != null && this.getItems().getElementList() != null)
    		{
    			elements.getElementList().addAll(this.getItems().getElementList());
    		}
    	}
    	return elements;
    }
    
    @Override
    public void writeHtml(com.mda.engine.utils.Stringcode c)
    {
    	c.write("<ul");
    	if (getId() != null)
    	{
    		c.write(" id=\"{0}\"", getId());
    	}
    	if (getCss() != null)
    	{
    		c.write(" class=\"{0}\"", getCss());
    	}
    	c.write(">");
    	c.writeLine();
    	writeHtmlChildren(c);
    	c.write("</ul>");
    	c.writeLine();
    }
    
    @Override
    public String getClassName() throws Exception
    {
    	return "MVCViewUL";
    }
    
    @Override
    public void writeInitializeMethodInternal(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product)
    {

    }
//--simple--preserve

}
