
package com.mda.engine.models.mvc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewSecurity complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewSecurity">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Key" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewSecurityTokenKeyChoice"/>
 *         &lt;element name="Value" type="{http://www.mdaengine.com/mdaengine/models/mvc}MVCViewSecurityTokenValueChoice"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewSecurity", propOrder = {
    "key",
    "value"
})
public class MVCViewSecurity
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Key", required = true)
    protected MVCViewSecurityTokenKeyChoice key;
    @XmlElement(name = "Value", required = true)
    protected MVCViewSecurityTokenValueChoice value;

    /**
     * Gets the value of the key property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewSecurityTokenKeyChoice }
     *     
     */
    public MVCViewSecurityTokenKeyChoice getKey() {
        return key;
    }

    /**
     * Sets the value of the key property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewSecurityTokenKeyChoice }
     *     
     */
    public void setKey(MVCViewSecurityTokenKeyChoice value) {
        this.key = value;
    }

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link MVCViewSecurityTokenValueChoice }
     *     
     */
    public MVCViewSecurityTokenValueChoice getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link MVCViewSecurityTokenValueChoice }
     *     
     */
    public void setValue(MVCViewSecurityTokenValueChoice value) {
        this.value = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewSecurity)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final MVCViewSecurity that = ((MVCViewSecurity) object);
        {
            MVCViewSecurityTokenKeyChoice lhsKey;
            lhsKey = this.getKey();
            MVCViewSecurityTokenKeyChoice rhsKey;
            rhsKey = that.getKey();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "key", lhsKey), LocatorUtils.property(thatLocator, "key", rhsKey), lhsKey, rhsKey)) {
                return false;
            }
        }
        {
            MVCViewSecurityTokenValueChoice lhsValue;
            lhsValue = this.getValue();
            MVCViewSecurityTokenValueChoice rhsValue;
            rhsValue = that.getValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "value", lhsValue), LocatorUtils.property(thatLocator, "value", rhsValue), lhsValue, rhsValue)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof MVCViewSecurity) {
            final MVCViewSecurity copy = ((MVCViewSecurity) draftCopy);
            if (this.key!= null) {
                MVCViewSecurityTokenKeyChoice sourceKey;
                sourceKey = this.getKey();
                MVCViewSecurityTokenKeyChoice copyKey = ((MVCViewSecurityTokenKeyChoice) strategy.copy(LocatorUtils.property(locator, "key", sourceKey), sourceKey));
                copy.setKey(copyKey);
            } else {
                copy.key = null;
            }
            if (this.value!= null) {
                MVCViewSecurityTokenValueChoice sourceValue;
                sourceValue = this.getValue();
                MVCViewSecurityTokenValueChoice copyValue = ((MVCViewSecurityTokenValueChoice) strategy.copy(LocatorUtils.property(locator, "value", sourceValue), sourceValue));
                copy.setValue(copyValue);
            } else {
                copy.value = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewSecurity();
    }

}
