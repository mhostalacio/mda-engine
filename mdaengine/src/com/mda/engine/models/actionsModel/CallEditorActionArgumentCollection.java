
package com.mda.engine.models.actionsModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for CallEditorActionArgumentCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CallEditorActionArgumentCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Argument" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}CallEditorActionArgument" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallEditorActionArgumentCollection", propOrder = {
    "argument"
})
public class CallEditorActionArgumentCollection
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Argument", required = true)
    protected List<CallEditorActionArgument> argument;

    /**
     * Gets the value of the argument property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the argument property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getArgument().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CallEditorActionArgument }
     * 
     * 
     */
    public List<CallEditorActionArgument> getArgument() {
        if (argument == null) {
            argument = new ArrayList<CallEditorActionArgument>();
        }
        return this.argument;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof CallEditorActionArgumentCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final CallEditorActionArgumentCollection that = ((CallEditorActionArgumentCollection) object);
        {
            List<CallEditorActionArgument> lhsArgument;
            lhsArgument = this.getArgument();
            List<CallEditorActionArgument> rhsArgument;
            rhsArgument = that.getArgument();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "argument", lhsArgument), LocatorUtils.property(thatLocator, "argument", rhsArgument), lhsArgument, rhsArgument)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof CallEditorActionArgumentCollection) {
            final CallEditorActionArgumentCollection copy = ((CallEditorActionArgumentCollection) draftCopy);
            if ((this.argument!= null)&&(!this.argument.isEmpty())) {
                List<CallEditorActionArgument> sourceArgument;
                sourceArgument = this.getArgument();
                @SuppressWarnings("unchecked")
                List<CallEditorActionArgument> copyArgument = ((List<CallEditorActionArgument> ) strategy.copy(LocatorUtils.property(locator, "argument", sourceArgument), sourceArgument));
                copy.argument = null;
                List<CallEditorActionArgument> uniqueArgumentl = copy.getArgument();
                uniqueArgumentl.addAll(copyArgument);
            } else {
                copy.argument = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new CallEditorActionArgumentCollection();
    }

}
