
package com.mda.engine.models.actionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.models.expressionsModel.BooleanExpressionChoice;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for IfAction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IfAction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/actionsModel}Action">
 *       &lt;sequence>
 *         &lt;element name="Expression" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}BooleanExpressionChoice"/>
 *         &lt;element name="Then" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}ActionChoice"/>
 *         &lt;element name="Else" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}ActionChoice" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IfAction", propOrder = {
    "expression",
    "then",
    "_else"
})
public class IfAction
    extends Action
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Expression", required = true)
    protected BooleanExpressionChoice expression;
    @XmlElement(name = "Then", required = true)
    protected ActionChoice then;
    @XmlElement(name = "Else")
    protected ActionChoice _else;

    /**
     * Gets the value of the expression property.
     * 
     * @return
     *     possible object is
     *     {@link BooleanExpressionChoice }
     *     
     */
    public BooleanExpressionChoice getExpression() {
        return expression;
    }

    /**
     * Sets the value of the expression property.
     * 
     * @param value
     *     allowed object is
     *     {@link BooleanExpressionChoice }
     *     
     */
    public void setExpression(BooleanExpressionChoice value) {
        this.expression = value;
    }

    /**
     * Gets the value of the then property.
     * 
     * @return
     *     possible object is
     *     {@link ActionChoice }
     *     
     */
    public ActionChoice getThen() {
        return then;
    }

    /**
     * Sets the value of the then property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActionChoice }
     *     
     */
    public void setThen(ActionChoice value) {
        this.then = value;
    }

    /**
     * Gets the value of the else property.
     * 
     * @return
     *     possible object is
     *     {@link ActionChoice }
     *     
     */
    public ActionChoice getElse() {
        return _else;
    }

    /**
     * Sets the value of the else property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActionChoice }
     *     
     */
    public void setElse(ActionChoice value) {
        this._else = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof IfAction)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final IfAction that = ((IfAction) object);
        {
            BooleanExpressionChoice lhsExpression;
            lhsExpression = this.getExpression();
            BooleanExpressionChoice rhsExpression;
            rhsExpression = that.getExpression();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "expression", lhsExpression), LocatorUtils.property(thatLocator, "expression", rhsExpression), lhsExpression, rhsExpression)) {
                return false;
            }
        }
        {
            ActionChoice lhsThen;
            lhsThen = this.getThen();
            ActionChoice rhsThen;
            rhsThen = that.getThen();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "then", lhsThen), LocatorUtils.property(thatLocator, "then", rhsThen), lhsThen, rhsThen)) {
                return false;
            }
        }
        {
            ActionChoice lhsElse;
            lhsElse = this.getElse();
            ActionChoice rhsElse;
            rhsElse = that.getElse();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "_else", lhsElse), LocatorUtils.property(thatLocator, "_else", rhsElse), lhsElse, rhsElse)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof IfAction) {
            final IfAction copy = ((IfAction) draftCopy);
            if (this.expression!= null) {
                BooleanExpressionChoice sourceExpression;
                sourceExpression = this.getExpression();
                BooleanExpressionChoice copyExpression = ((BooleanExpressionChoice) strategy.copy(LocatorUtils.property(locator, "expression", sourceExpression), sourceExpression));
                copy.setExpression(copyExpression);
            } else {
                copy.expression = null;
            }
            if (this.then!= null) {
                ActionChoice sourceThen;
                sourceThen = this.getThen();
                ActionChoice copyThen = ((ActionChoice) strategy.copy(LocatorUtils.property(locator, "then", sourceThen), sourceThen));
                copy.setThen(copyThen);
            } else {
                copy.then = null;
            }
            if (this._else!= null) {
                ActionChoice sourceElse;
                sourceElse = this.getElse();
                ActionChoice copyElse = ((ActionChoice) strategy.copy(LocatorUtils.property(locator, "_else", sourceElse), sourceElse));
                copy.setElse(copyElse);
            } else {
                copy._else = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new IfAction();
    }

}
