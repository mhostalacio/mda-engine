
package com.mda.engine.models.actionsModel;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.mda.engine.models.actionsModel package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.mda.engine.models.actionsModel
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateVariableAction }
     * 
     */
    public CreateVariableAction createCreateVariableAction() {
        return new CreateVariableAction();
    }

    /**
     * Create an instance of {@link IfAction }
     * 
     */
    public IfAction createIfAction() {
        return new IfAction();
    }

    /**
     * Create an instance of {@link CloseModalDialogAction }
     * 
     */
    public CloseModalDialogAction createCloseModalDialogAction() {
        return new CloseModalDialogAction();
    }

    /**
     * Create an instance of {@link RaiseMessageAction }
     * 
     */
    public RaiseMessageAction createRaiseMessageAction() {
        return new RaiseMessageAction();
    }

    /**
     * Create an instance of {@link CallEditorActionArgument }
     * 
     */
    public CallEditorActionArgument createCallEditorActionArgument() {
        return new CallEditorActionArgument();
    }

    /**
     * Create an instance of {@link CallEditorModelInitializationAction }
     * 
     */
    public CallEditorModelInitializationAction createCallEditorModelInitializationAction() {
        return new CallEditorModelInitializationAction();
    }

    /**
     * Create an instance of {@link InternalEditor }
     * 
     */
    public InternalEditor createInternalEditor() {
        return new InternalEditor();
    }

    /**
     * Create an instance of {@link SetProcessDataAction }
     * 
     */
    public SetProcessDataAction createSetProcessDataAction() {
        return new SetProcessDataAction();
    }

    /**
     * Create an instance of {@link InvokeBusinessTransactionAction }
     * 
     */
    public InvokeBusinessTransactionAction createInvokeBusinessTransactionAction() {
        return new InvokeBusinessTransactionAction();
    }

    /**
     * Create an instance of {@link RefreshElementsAction }
     * 
     */
    public RefreshElementsAction createRefreshElementsAction() {
        return new RefreshElementsAction();
    }

    /**
     * Create an instance of {@link MVCAction }
     * 
     */
    public MVCAction createMVCAction() {
        return new MVCAction();
    }

    /**
     * Create an instance of {@link PrivateEditor }
     * 
     */
    public PrivateEditor createPrivateEditor() {
        return new PrivateEditor();
    }

    /**
     * Create an instance of {@link FinishUserTaskAction }
     * 
     */
    public FinishUserTaskAction createFinishUserTaskAction() {
        return new FinishUserTaskAction();
    }

    /**
     * Create an instance of {@link SaveProcessAction }
     * 
     */
    public SaveProcessAction createSaveProcessAction() {
        return new SaveProcessAction();
    }

    /**
     * Create an instance of {@link InvokeEditorOutputProcessValueCollection }
     * 
     */
    public InvokeEditorOutputProcessValueCollection createInvokeEditorOutputProcessValueCollection() {
        return new InvokeEditorOutputProcessValueCollection();
    }

    /**
     * Create an instance of {@link InvokeEditorOutputProcessValue }
     * 
     */
    public InvokeEditorOutputProcessValue createInvokeEditorOutputProcessValue() {
        return new InvokeEditorOutputProcessValue();
    }

    /**
     * Create an instance of {@link ChangeCssClassAction }
     * 
     */
    public ChangeCssClassAction createChangeCssClassAction() {
        return new ChangeCssClassAction();
    }

    /**
     * Create an instance of {@link CloseAndCallOpenerAction }
     * 
     */
    public CloseAndCallOpenerAction createCloseAndCallOpenerAction() {
        return new CloseAndCallOpenerAction();
    }

    /**
     * Create an instance of {@link InvokeEditorOutputProcess }
     * 
     */
    public InvokeEditorOutputProcess createInvokeEditorOutputProcess() {
        return new InvokeEditorOutputProcess();
    }

    /**
     * Create an instance of {@link SetValueAction }
     * 
     */
    public SetValueAction createSetValueAction() {
        return new SetValueAction();
    }

    /**
     * Create an instance of {@link PersistAction }
     * 
     */
    public PersistAction createPersistAction() {
        return new PersistAction();
    }

    /**
     * Create an instance of {@link InitializeAction }
     * 
     */
    public InitializeAction createInitializeAction() {
        return new InitializeAction();
    }

    /**
     * Create an instance of {@link BackAction }
     * 
     */
    public BackAction createBackAction() {
        return new BackAction();
    }

    /**
     * Create an instance of {@link ReturnAction }
     * 
     */
    public ReturnAction createReturnAction() {
        return new ReturnAction();
    }

    /**
     * Create an instance of {@link ActionChoice }
     * 
     */
    public ActionChoice createActionChoice() {
        return new ActionChoice();
    }

    /**
     * Create an instance of {@link Action }
     * 
     */
    public Action createAction() {
        return new Action();
    }

    /**
     * Create an instance of {@link CallInitAction }
     * 
     */
    public CallInitAction createCallInitAction() {
        return new CallInitAction();
    }

    /**
     * Create an instance of {@link AlertAction }
     * 
     */
    public AlertAction createAlertAction() {
        return new AlertAction();
    }

    /**
     * Create an instance of {@link AddElementToListAction }
     * 
     */
    public AddElementToListAction createAddElementToListAction() {
        return new AddElementToListAction();
    }

    /**
     * Create an instance of {@link ExternalEditor }
     * 
     */
    public ExternalEditor createExternalEditor() {
        return new ExternalEditor();
    }

    /**
     * Create an instance of {@link ToggleVisibilityAction }
     * 
     */
    public ToggleVisibilityAction createToggleVisibilityAction() {
        return new ToggleVisibilityAction();
    }

    /**
     * Create an instance of {@link CallEditorAction }
     * 
     */
    public CallEditorAction createCallEditorAction() {
        return new CallEditorAction();
    }

    /**
     * Create an instance of {@link CallEditorActionArgumentCollection }
     * 
     */
    public CallEditorActionArgumentCollection createCallEditorActionArgumentCollection() {
        return new CallEditorActionArgumentCollection();
    }

    /**
     * Create an instance of {@link CallMethodAction }
     * 
     */
    public CallMethodAction createCallMethodAction() {
        return new CallMethodAction();
    }

    /**
     * Create an instance of {@link RedirectToActionTrigger }
     * 
     */
    public RedirectToActionTrigger createRedirectToActionTrigger() {
        return new RedirectToActionTrigger();
    }

    /**
     * Create an instance of {@link SetVisibilityAction }
     * 
     */
    public SetVisibilityAction createSetVisibilityAction() {
        return new SetVisibilityAction();
    }

    /**
     * Create an instance of {@link CloseSupportViewAction }
     * 
     */
    public CloseSupportViewAction createCloseSupportViewAction() {
        return new CloseSupportViewAction();
    }

    /**
     * Create an instance of {@link RefreshElementsActionElementExpression }
     * 
     */
    public RefreshElementsActionElementExpression createRefreshElementsActionElementExpression() {
        return new RefreshElementsActionElementExpression();
    }

    /**
     * Create an instance of {@link CloseModalSupportViewAction }
     * 
     */
    public CloseModalSupportViewAction createCloseModalSupportViewAction() {
        return new CloseModalSupportViewAction();
    }

    /**
     * Create an instance of {@link AddValueToTransactionContext }
     * 
     */
    public AddValueToTransactionContext createAddValueToTransactionContext() {
        return new AddValueToTransactionContext();
    }

}
