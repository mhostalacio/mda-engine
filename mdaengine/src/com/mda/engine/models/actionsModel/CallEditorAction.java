
package com.mda.engine.models.actionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for CallEditorAction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CallEditorAction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/actionsModel}Action">
 *       &lt;choice>
 *         &lt;element name="Private" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}PrivateEditor"/>
 *         &lt;element name="Internal" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}InternalEditor"/>
 *         &lt;element name="External" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}ExternalEditor"/>
 *       &lt;/choice>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallEditorAction", propOrder = {
    "editor"
})
public class CallEditorAction
    extends Action
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElements({
        @XmlElement(name = "Internal", type = InternalEditor.class),
        @XmlElement(name = "Private", type = PrivateEditor.class),
        @XmlElement(name = "External", type = ExternalEditor.class)
    })
    protected Editor editor;

    /**
     * Gets the value of the editor property.
     * 
     * @return
     *     possible object is
     *     {@link InternalEditor }
     *     {@link PrivateEditor }
     *     {@link ExternalEditor }
     *     
     */
    public Editor getEditor() {
        return editor;
    }

    /**
     * Sets the value of the editor property.
     * 
     * @param value
     *     allowed object is
     *     {@link InternalEditor }
     *     {@link PrivateEditor }
     *     {@link ExternalEditor }
     *     
     */
    public void setEditor(Editor value) {
        this.editor = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof CallEditorAction)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final CallEditorAction that = ((CallEditorAction) object);
        {
            Editor lhsEditor;
            lhsEditor = this.getEditor();
            Editor rhsEditor;
            rhsEditor = that.getEditor();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "editor", lhsEditor), LocatorUtils.property(thatLocator, "editor", rhsEditor), lhsEditor, rhsEditor)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof CallEditorAction) {
            final CallEditorAction copy = ((CallEditorAction) draftCopy);
            if (this.editor!= null) {
                Editor sourceEditor;
                sourceEditor = this.getEditor();
                Editor copyEditor = ((Editor) strategy.copy(LocatorUtils.property(locator, "editor", sourceEditor), sourceEditor));
                copy.setEditor(copyEditor);
            } else {
                copy.editor = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new CallEditorAction();
    }

}
