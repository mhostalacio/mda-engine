
package com.mda.engine.models.actionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.models.expressionsModel.ExpressionCollection;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for CallMethodAction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CallMethodAction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/actionsModel}Action">
 *       &lt;sequence>
 *         &lt;element name="Arguments" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ExpressionCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="MethodName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="InsertThisLiteralAsFirstArgument" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallMethodAction", propOrder = {
    "arguments"
})
public class CallMethodAction
    extends Action
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Arguments")
    protected ExpressionCollection arguments;
    @XmlAttribute(name = "MethodName", required = true)
    protected String methodName;
    @XmlAttribute(name = "InsertThisLiteralAsFirstArgument")
    protected Boolean insertThisLiteralAsFirstArgument;

    /**
     * Gets the value of the arguments property.
     * 
     * @return
     *     possible object is
     *     {@link ExpressionCollection }
     *     
     */
    public ExpressionCollection getArguments() {
        return arguments;
    }

    /**
     * Sets the value of the arguments property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpressionCollection }
     *     
     */
    public void setArguments(ExpressionCollection value) {
        this.arguments = value;
    }

    /**
     * Gets the value of the methodName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMethodName() {
        return methodName;
    }

    /**
     * Sets the value of the methodName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMethodName(String value) {
        this.methodName = value;
    }

    /**
     * Gets the value of the insertThisLiteralAsFirstArgument property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isInsertThisLiteralAsFirstArgument() {
        if (insertThisLiteralAsFirstArgument == null) {
            return false;
        } else {
            return insertThisLiteralAsFirstArgument;
        }
    }

    /**
     * Sets the value of the insertThisLiteralAsFirstArgument property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInsertThisLiteralAsFirstArgument(Boolean value) {
        this.insertThisLiteralAsFirstArgument = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof CallMethodAction)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final CallMethodAction that = ((CallMethodAction) object);
        {
            ExpressionCollection lhsArguments;
            lhsArguments = this.getArguments();
            ExpressionCollection rhsArguments;
            rhsArguments = that.getArguments();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "arguments", lhsArguments), LocatorUtils.property(thatLocator, "arguments", rhsArguments), lhsArguments, rhsArguments)) {
                return false;
            }
        }
        {
            String lhsMethodName;
            lhsMethodName = this.getMethodName();
            String rhsMethodName;
            rhsMethodName = that.getMethodName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "methodName", lhsMethodName), LocatorUtils.property(thatLocator, "methodName", rhsMethodName), lhsMethodName, rhsMethodName)) {
                return false;
            }
        }
        {
            boolean lhsInsertThisLiteralAsFirstArgument;
            lhsInsertThisLiteralAsFirstArgument = this.isInsertThisLiteralAsFirstArgument();
            boolean rhsInsertThisLiteralAsFirstArgument;
            rhsInsertThisLiteralAsFirstArgument = that.isInsertThisLiteralAsFirstArgument();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "insertThisLiteralAsFirstArgument", lhsInsertThisLiteralAsFirstArgument), LocatorUtils.property(thatLocator, "insertThisLiteralAsFirstArgument", rhsInsertThisLiteralAsFirstArgument), lhsInsertThisLiteralAsFirstArgument, rhsInsertThisLiteralAsFirstArgument)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof CallMethodAction) {
            final CallMethodAction copy = ((CallMethodAction) draftCopy);
            if (this.arguments!= null) {
                ExpressionCollection sourceArguments;
                sourceArguments = this.getArguments();
                ExpressionCollection copyArguments = ((ExpressionCollection) strategy.copy(LocatorUtils.property(locator, "arguments", sourceArguments), sourceArguments));
                copy.setArguments(copyArguments);
            } else {
                copy.arguments = null;
            }
            if (this.methodName!= null) {
                String sourceMethodName;
                sourceMethodName = this.getMethodName();
                String copyMethodName = ((String) strategy.copy(LocatorUtils.property(locator, "methodName", sourceMethodName), sourceMethodName));
                copy.setMethodName(copyMethodName);
            } else {
                copy.methodName = null;
            }
            if (this.insertThisLiteralAsFirstArgument!= null) {
                boolean sourceInsertThisLiteralAsFirstArgument;
                sourceInsertThisLiteralAsFirstArgument = this.isInsertThisLiteralAsFirstArgument();
                boolean copyInsertThisLiteralAsFirstArgument = strategy.copy(LocatorUtils.property(locator, "insertThisLiteralAsFirstArgument", sourceInsertThisLiteralAsFirstArgument), sourceInsertThisLiteralAsFirstArgument);
                copy.setInsertThisLiteralAsFirstArgument(copyInsertThisLiteralAsFirstArgument);
            } else {
                copy.insertThisLiteralAsFirstArgument = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new CallMethodAction();
    }

}
