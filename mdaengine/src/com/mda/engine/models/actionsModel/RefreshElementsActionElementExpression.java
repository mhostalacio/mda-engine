
package com.mda.engine.models.actionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.models.expressionsModel.MVCViewElementExpression;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for RefreshElementsActionElementExpression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RefreshElementsActionElementExpression">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}MVCViewElementExpression">
 *       &lt;attribute name="InitializeElement" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RefreshElementsActionElementExpression")
public class RefreshElementsActionElementExpression
    extends MVCViewElementExpression
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "InitializeElement")
    protected Boolean initializeElement;

    /**
     * Gets the value of the initializeElement property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isInitializeElement() {
        if (initializeElement == null) {
            return true;
        } else {
            return initializeElement;
        }
    }

    /**
     * Sets the value of the initializeElement property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInitializeElement(Boolean value) {
        this.initializeElement = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof RefreshElementsActionElementExpression)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final RefreshElementsActionElementExpression that = ((RefreshElementsActionElementExpression) object);
        {
            boolean lhsInitializeElement;
            lhsInitializeElement = this.isInitializeElement();
            boolean rhsInitializeElement;
            rhsInitializeElement = that.isInitializeElement();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "initializeElement", lhsInitializeElement), LocatorUtils.property(thatLocator, "initializeElement", rhsInitializeElement), lhsInitializeElement, rhsInitializeElement)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof RefreshElementsActionElementExpression) {
            final RefreshElementsActionElementExpression copy = ((RefreshElementsActionElementExpression) draftCopy);
            if (this.initializeElement!= null) {
                boolean sourceInitializeElement;
                sourceInitializeElement = this.isInitializeElement();
                boolean copyInitializeElement = strategy.copy(LocatorUtils.property(locator, "initializeElement", sourceInitializeElement), sourceInitializeElement);
                copy.setInitializeElement(copyInitializeElement);
            } else {
                copy.initializeElement = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new RefreshElementsActionElementExpression();
    }

}
