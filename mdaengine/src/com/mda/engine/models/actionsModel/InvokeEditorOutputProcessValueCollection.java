
package com.mda.engine.models.actionsModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for InvokeEditorOutputProcessValueCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InvokeEditorOutputProcessValueCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Value" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}InvokeEditorOutputProcessValue" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvokeEditorOutputProcessValueCollection", propOrder = {
    "value"
})
public class InvokeEditorOutputProcessValueCollection
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Value", required = true)
    protected List<InvokeEditorOutputProcessValue> value;

    /**
     * Gets the value of the value property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the value property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvokeEditorOutputProcessValue }
     * 
     * 
     */
    public List<InvokeEditorOutputProcessValue> getValue() {
        if (value == null) {
            value = new ArrayList<InvokeEditorOutputProcessValue>();
        }
        return this.value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof InvokeEditorOutputProcessValueCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final InvokeEditorOutputProcessValueCollection that = ((InvokeEditorOutputProcessValueCollection) object);
        {
            List<InvokeEditorOutputProcessValue> lhsValue;
            lhsValue = this.getValue();
            List<InvokeEditorOutputProcessValue> rhsValue;
            rhsValue = that.getValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "value", lhsValue), LocatorUtils.property(thatLocator, "value", rhsValue), lhsValue, rhsValue)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof InvokeEditorOutputProcessValueCollection) {
            final InvokeEditorOutputProcessValueCollection copy = ((InvokeEditorOutputProcessValueCollection) draftCopy);
            if ((this.value!= null)&&(!this.value.isEmpty())) {
                List<InvokeEditorOutputProcessValue> sourceValue;
                sourceValue = this.getValue();
                @SuppressWarnings("unchecked")
                List<InvokeEditorOutputProcessValue> copyValue = ((List<InvokeEditorOutputProcessValue> ) strategy.copy(LocatorUtils.property(locator, "value", sourceValue), sourceValue));
                copy.value = null;
                List<InvokeEditorOutputProcessValue> uniqueValuel = copy.getValue();
                uniqueValuel.addAll(copyValue);
            } else {
                copy.value = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new InvokeEditorOutputProcessValueCollection();
    }

}
