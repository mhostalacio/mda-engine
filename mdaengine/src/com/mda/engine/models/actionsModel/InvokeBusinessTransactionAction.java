
package com.mda.engine.models.actionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.models.common.BusinessTransactionReference;
import com.mda.engine.models.expressionsModel.InvokeBusinessTransactionExpressionInputCollection;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for InvokeBusinessTransactionAction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InvokeBusinessTransactionAction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/actionsModel}Action">
 *       &lt;sequence>
 *         &lt;element name="BusinessTransactionToInvoke" type="{http://www.mdaengine.com/mdaengine/models/common}BusinessTransactionReference"/>
 *         &lt;element name="Inputs" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}InvokeBusinessTransactionExpressionInputCollection" minOccurs="0"/>
 *         &lt;element name="Actions" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}ActionChoice" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="OutputVariableName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvokeBusinessTransactionAction", propOrder = {
    "businessTransactionToInvoke",
    "inputs",
    "actions"
})
public class InvokeBusinessTransactionAction
    extends Action
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "BusinessTransactionToInvoke", required = true)
    protected BusinessTransactionReference businessTransactionToInvoke;
    @XmlElement(name = "Inputs")
    protected InvokeBusinessTransactionExpressionInputCollection inputs;
    @XmlElement(name = "Actions")
    protected ActionChoice actions;
    @XmlAttribute(name = "OutputVariableName")
    protected String outputVariableName;

    /**
     * Gets the value of the businessTransactionToInvoke property.
     * 
     * @return
     *     possible object is
     *     {@link BusinessTransactionReference }
     *     
     */
    public BusinessTransactionReference getBusinessTransactionToInvoke() {
        return businessTransactionToInvoke;
    }

    /**
     * Sets the value of the businessTransactionToInvoke property.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessTransactionReference }
     *     
     */
    public void setBusinessTransactionToInvoke(BusinessTransactionReference value) {
        this.businessTransactionToInvoke = value;
    }

    /**
     * Gets the value of the inputs property.
     * 
     * @return
     *     possible object is
     *     {@link InvokeBusinessTransactionExpressionInputCollection }
     *     
     */
    public InvokeBusinessTransactionExpressionInputCollection getInputs() {
        return inputs;
    }

    /**
     * Sets the value of the inputs property.
     * 
     * @param value
     *     allowed object is
     *     {@link InvokeBusinessTransactionExpressionInputCollection }
     *     
     */
    public void setInputs(InvokeBusinessTransactionExpressionInputCollection value) {
        this.inputs = value;
    }

    /**
     * Gets the value of the actions property.
     * 
     * @return
     *     possible object is
     *     {@link ActionChoice }
     *     
     */
    public ActionChoice getActions() {
        return actions;
    }

    /**
     * Sets the value of the actions property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActionChoice }
     *     
     */
    public void setActions(ActionChoice value) {
        this.actions = value;
    }

    /**
     * Gets the value of the outputVariableName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutputVariableName() {
        return outputVariableName;
    }

    /**
     * Sets the value of the outputVariableName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutputVariableName(String value) {
        this.outputVariableName = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof InvokeBusinessTransactionAction)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final InvokeBusinessTransactionAction that = ((InvokeBusinessTransactionAction) object);
        {
            BusinessTransactionReference lhsBusinessTransactionToInvoke;
            lhsBusinessTransactionToInvoke = this.getBusinessTransactionToInvoke();
            BusinessTransactionReference rhsBusinessTransactionToInvoke;
            rhsBusinessTransactionToInvoke = that.getBusinessTransactionToInvoke();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "businessTransactionToInvoke", lhsBusinessTransactionToInvoke), LocatorUtils.property(thatLocator, "businessTransactionToInvoke", rhsBusinessTransactionToInvoke), lhsBusinessTransactionToInvoke, rhsBusinessTransactionToInvoke)) {
                return false;
            }
        }
        {
            InvokeBusinessTransactionExpressionInputCollection lhsInputs;
            lhsInputs = this.getInputs();
            InvokeBusinessTransactionExpressionInputCollection rhsInputs;
            rhsInputs = that.getInputs();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "inputs", lhsInputs), LocatorUtils.property(thatLocator, "inputs", rhsInputs), lhsInputs, rhsInputs)) {
                return false;
            }
        }
        {
            ActionChoice lhsActions;
            lhsActions = this.getActions();
            ActionChoice rhsActions;
            rhsActions = that.getActions();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "actions", lhsActions), LocatorUtils.property(thatLocator, "actions", rhsActions), lhsActions, rhsActions)) {
                return false;
            }
        }
        {
            String lhsOutputVariableName;
            lhsOutputVariableName = this.getOutputVariableName();
            String rhsOutputVariableName;
            rhsOutputVariableName = that.getOutputVariableName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "outputVariableName", lhsOutputVariableName), LocatorUtils.property(thatLocator, "outputVariableName", rhsOutputVariableName), lhsOutputVariableName, rhsOutputVariableName)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof InvokeBusinessTransactionAction) {
            final InvokeBusinessTransactionAction copy = ((InvokeBusinessTransactionAction) draftCopy);
            if (this.businessTransactionToInvoke!= null) {
                BusinessTransactionReference sourceBusinessTransactionToInvoke;
                sourceBusinessTransactionToInvoke = this.getBusinessTransactionToInvoke();
                BusinessTransactionReference copyBusinessTransactionToInvoke = ((BusinessTransactionReference) strategy.copy(LocatorUtils.property(locator, "businessTransactionToInvoke", sourceBusinessTransactionToInvoke), sourceBusinessTransactionToInvoke));
                copy.setBusinessTransactionToInvoke(copyBusinessTransactionToInvoke);
            } else {
                copy.businessTransactionToInvoke = null;
            }
            if (this.inputs!= null) {
                InvokeBusinessTransactionExpressionInputCollection sourceInputs;
                sourceInputs = this.getInputs();
                InvokeBusinessTransactionExpressionInputCollection copyInputs = ((InvokeBusinessTransactionExpressionInputCollection) strategy.copy(LocatorUtils.property(locator, "inputs", sourceInputs), sourceInputs));
                copy.setInputs(copyInputs);
            } else {
                copy.inputs = null;
            }
            if (this.actions!= null) {
                ActionChoice sourceActions;
                sourceActions = this.getActions();
                ActionChoice copyActions = ((ActionChoice) strategy.copy(LocatorUtils.property(locator, "actions", sourceActions), sourceActions));
                copy.setActions(copyActions);
            } else {
                copy.actions = null;
            }
            if (this.outputVariableName!= null) {
                String sourceOutputVariableName;
                sourceOutputVariableName = this.getOutputVariableName();
                String copyOutputVariableName = ((String) strategy.copy(LocatorUtils.property(locator, "outputVariableName", sourceOutputVariableName), sourceOutputVariableName));
                copy.setOutputVariableName(copyOutputVariableName);
            } else {
                copy.outputVariableName = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new InvokeBusinessTransactionAction();
    }

}
