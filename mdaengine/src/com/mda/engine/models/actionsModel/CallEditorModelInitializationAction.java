
package com.mda.engine.models.actionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for CallEditorModelInitializationAction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CallEditorModelInitializationAction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/actionsModel}Action">
 *       &lt;attribute name="EditorName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallEditorModelInitializationAction")
public class CallEditorModelInitializationAction
    extends Action
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "EditorName", required = true)
    protected String editorName;

    /**
     * Gets the value of the editorName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEditorName() {
        return editorName;
    }

    /**
     * Sets the value of the editorName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEditorName(String value) {
        this.editorName = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof CallEditorModelInitializationAction)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final CallEditorModelInitializationAction that = ((CallEditorModelInitializationAction) object);
        {
            String lhsEditorName;
            lhsEditorName = this.getEditorName();
            String rhsEditorName;
            rhsEditorName = that.getEditorName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "editorName", lhsEditorName), LocatorUtils.property(thatLocator, "editorName", rhsEditorName), lhsEditorName, rhsEditorName)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof CallEditorModelInitializationAction) {
            final CallEditorModelInitializationAction copy = ((CallEditorModelInitializationAction) draftCopy);
            if (this.editorName!= null) {
                String sourceEditorName;
                sourceEditorName = this.getEditorName();
                String copyEditorName = ((String) strategy.copy(LocatorUtils.property(locator, "editorName", sourceEditorName), sourceEditorName));
                copy.setEditorName(copyEditorName);
            } else {
                copy.editorName = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new CallEditorModelInitializationAction();
    }

}
