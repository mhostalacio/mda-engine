
package com.mda.engine.models.actionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.models.expressionsModel.ExpressionChoice;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for SetProcessDataAction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SetProcessDataAction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/actionsModel}Action">
 *       &lt;sequence>
 *         &lt;element name="Value" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ExpressionChoice"/>
 *       &lt;/sequence>
 *       &lt;attribute name="DataObjectName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SetProcessDataAction", propOrder = {
    "value"
})
public class SetProcessDataAction
    extends Action
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Value", required = true)
    protected ExpressionChoice value;
    @XmlAttribute(name = "DataObjectName", required = true)
    protected String dataObjectName;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link ExpressionChoice }
     *     
     */
    public ExpressionChoice getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpressionChoice }
     *     
     */
    public void setValue(ExpressionChoice value) {
        this.value = value;
    }

    /**
     * Gets the value of the dataObjectName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataObjectName() {
        return dataObjectName;
    }

    /**
     * Sets the value of the dataObjectName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataObjectName(String value) {
        this.dataObjectName = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof SetProcessDataAction)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final SetProcessDataAction that = ((SetProcessDataAction) object);
        {
            ExpressionChoice lhsValue;
            lhsValue = this.getValue();
            ExpressionChoice rhsValue;
            rhsValue = that.getValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "value", lhsValue), LocatorUtils.property(thatLocator, "value", rhsValue), lhsValue, rhsValue)) {
                return false;
            }
        }
        {
            String lhsDataObjectName;
            lhsDataObjectName = this.getDataObjectName();
            String rhsDataObjectName;
            rhsDataObjectName = that.getDataObjectName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dataObjectName", lhsDataObjectName), LocatorUtils.property(thatLocator, "dataObjectName", rhsDataObjectName), lhsDataObjectName, rhsDataObjectName)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof SetProcessDataAction) {
            final SetProcessDataAction copy = ((SetProcessDataAction) draftCopy);
            if (this.value!= null) {
                ExpressionChoice sourceValue;
                sourceValue = this.getValue();
                ExpressionChoice copyValue = ((ExpressionChoice) strategy.copy(LocatorUtils.property(locator, "value", sourceValue), sourceValue));
                copy.setValue(copyValue);
            } else {
                copy.value = null;
            }
            if (this.dataObjectName!= null) {
                String sourceDataObjectName;
                sourceDataObjectName = this.getDataObjectName();
                String copyDataObjectName = ((String) strategy.copy(LocatorUtils.property(locator, "dataObjectName", sourceDataObjectName), sourceDataObjectName));
                copy.setDataObjectName(copyDataObjectName);
            } else {
                copy.dataObjectName = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new SetProcessDataAction();
    }

}
