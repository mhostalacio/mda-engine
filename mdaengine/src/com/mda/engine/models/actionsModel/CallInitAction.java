
package com.mda.engine.models.actionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.models.expressionsModel.ExpressionCollection;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for CallInitAction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CallInitAction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/actionsModel}Action">
 *       &lt;sequence>
 *         &lt;element name="Arguments" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ExpressionCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="ActionName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallInitAction", propOrder = {
    "arguments"
})
public class CallInitAction
    extends Action
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Arguments")
    protected ExpressionCollection arguments;
    @XmlAttribute(name = "ActionName", required = true)
    protected String actionName;

    /**
     * Gets the value of the arguments property.
     * 
     * @return
     *     possible object is
     *     {@link ExpressionCollection }
     *     
     */
    public ExpressionCollection getArguments() {
        return arguments;
    }

    /**
     * Sets the value of the arguments property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpressionCollection }
     *     
     */
    public void setArguments(ExpressionCollection value) {
        this.arguments = value;
    }

    /**
     * Gets the value of the actionName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionName() {
        return actionName;
    }

    /**
     * Sets the value of the actionName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionName(String value) {
        this.actionName = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof CallInitAction)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final CallInitAction that = ((CallInitAction) object);
        {
            ExpressionCollection lhsArguments;
            lhsArguments = this.getArguments();
            ExpressionCollection rhsArguments;
            rhsArguments = that.getArguments();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "arguments", lhsArguments), LocatorUtils.property(thatLocator, "arguments", rhsArguments), lhsArguments, rhsArguments)) {
                return false;
            }
        }
        {
            String lhsActionName;
            lhsActionName = this.getActionName();
            String rhsActionName;
            rhsActionName = that.getActionName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "actionName", lhsActionName), LocatorUtils.property(thatLocator, "actionName", rhsActionName), lhsActionName, rhsActionName)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof CallInitAction) {
            final CallInitAction copy = ((CallInitAction) draftCopy);
            if (this.arguments!= null) {
                ExpressionCollection sourceArguments;
                sourceArguments = this.getArguments();
                ExpressionCollection copyArguments = ((ExpressionCollection) strategy.copy(LocatorUtils.property(locator, "arguments", sourceArguments), sourceArguments));
                copy.setArguments(copyArguments);
            } else {
                copy.arguments = null;
            }
            if (this.actionName!= null) {
                String sourceActionName;
                sourceActionName = this.getActionName();
                String copyActionName = ((String) strategy.copy(LocatorUtils.property(locator, "actionName", sourceActionName), sourceActionName));
                copy.setActionName(copyActionName);
            } else {
                copy.actionName = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new CallInitAction();
    }

}
