
package com.mda.engine.models.actionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for InvokeEditorOutputProcess complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InvokeEditorOutputProcess">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/actionsModel}Action">
 *       &lt;sequence>
 *         &lt;element name="Values" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}InvokeEditorOutputProcessValueCollection"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvokeEditorOutputProcess", propOrder = {
    "values"
})
public class InvokeEditorOutputProcess
    extends Action
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Values", required = true)
    protected InvokeEditorOutputProcessValueCollection values;

    /**
     * Gets the value of the values property.
     * 
     * @return
     *     possible object is
     *     {@link InvokeEditorOutputProcessValueCollection }
     *     
     */
    public InvokeEditorOutputProcessValueCollection getValues() {
        return values;
    }

    /**
     * Sets the value of the values property.
     * 
     * @param value
     *     allowed object is
     *     {@link InvokeEditorOutputProcessValueCollection }
     *     
     */
    public void setValues(InvokeEditorOutputProcessValueCollection value) {
        this.values = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof InvokeEditorOutputProcess)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final InvokeEditorOutputProcess that = ((InvokeEditorOutputProcess) object);
        {
            InvokeEditorOutputProcessValueCollection lhsValues;
            lhsValues = this.getValues();
            InvokeEditorOutputProcessValueCollection rhsValues;
            rhsValues = that.getValues();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "values", lhsValues), LocatorUtils.property(thatLocator, "values", rhsValues), lhsValues, rhsValues)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof InvokeEditorOutputProcess) {
            final InvokeEditorOutputProcess copy = ((InvokeEditorOutputProcess) draftCopy);
            if (this.values!= null) {
                InvokeEditorOutputProcessValueCollection sourceValues;
                sourceValues = this.getValues();
                InvokeEditorOutputProcessValueCollection copyValues = ((InvokeEditorOutputProcessValueCollection) strategy.copy(LocatorUtils.property(locator, "values", sourceValues), sourceValues));
                copy.setValues(copyValues);
            } else {
                copy.values = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new InvokeEditorOutputProcess();
    }

}
