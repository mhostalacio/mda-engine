
package com.mda.engine.models.actionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.models.expressionsModel.ExpressionChoice;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ToggleVisibilityAction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ToggleVisibilityAction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/actionsModel}Action">
 *       &lt;sequence>
 *         &lt;element name="On" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ExpressionChoice"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ToggleVisibilityAction", propOrder = {
    "on"
})
public class ToggleVisibilityAction
    extends Action
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "On", required = true)
    protected ExpressionChoice on;

    /**
     * Gets the value of the on property.
     * 
     * @return
     *     possible object is
     *     {@link ExpressionChoice }
     *     
     */
    public ExpressionChoice getOn() {
        return on;
    }

    /**
     * Sets the value of the on property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpressionChoice }
     *     
     */
    public void setOn(ExpressionChoice value) {
        this.on = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ToggleVisibilityAction)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final ToggleVisibilityAction that = ((ToggleVisibilityAction) object);
        {
            ExpressionChoice lhsOn;
            lhsOn = this.getOn();
            ExpressionChoice rhsOn;
            rhsOn = that.getOn();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "on", lhsOn), LocatorUtils.property(thatLocator, "on", rhsOn), lhsOn, rhsOn)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof ToggleVisibilityAction) {
            final ToggleVisibilityAction copy = ((ToggleVisibilityAction) draftCopy);
            if (this.on!= null) {
                ExpressionChoice sourceOn;
                sourceOn = this.getOn();
                ExpressionChoice copyOn = ((ExpressionChoice) strategy.copy(LocatorUtils.property(locator, "on", sourceOn), sourceOn));
                copy.setOn(copyOn);
            } else {
                copy.on = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ToggleVisibilityAction();
    }

}
