
package com.mda.engine.models.actionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.models.expressionsModel.ExpressionChoice;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ChangeCssClassAction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ChangeCssClassAction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/actionsModel}Action">
 *       &lt;sequence>
 *         &lt;element name="On" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ExpressionChoice"/>
 *         &lt;element name="NewCssClassName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OldCssClassName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeCssClassAction", propOrder = {
    "on",
    "newCssClassName",
    "oldCssClassName"
})
public class ChangeCssClassAction
    extends Action
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "On", required = true)
    protected ExpressionChoice on;
    @XmlElement(name = "NewCssClassName", required = true)
    protected String newCssClassName;
    @XmlElement(name = "OldCssClassName", required = true)
    protected String oldCssClassName;

    /**
     * Gets the value of the on property.
     * 
     * @return
     *     possible object is
     *     {@link ExpressionChoice }
     *     
     */
    public ExpressionChoice getOn() {
        return on;
    }

    /**
     * Sets the value of the on property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpressionChoice }
     *     
     */
    public void setOn(ExpressionChoice value) {
        this.on = value;
    }

    /**
     * Gets the value of the newCssClassName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewCssClassName() {
        return newCssClassName;
    }

    /**
     * Sets the value of the newCssClassName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewCssClassName(String value) {
        this.newCssClassName = value;
    }

    /**
     * Gets the value of the oldCssClassName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOldCssClassName() {
        return oldCssClassName;
    }

    /**
     * Sets the value of the oldCssClassName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOldCssClassName(String value) {
        this.oldCssClassName = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ChangeCssClassAction)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final ChangeCssClassAction that = ((ChangeCssClassAction) object);
        {
            ExpressionChoice lhsOn;
            lhsOn = this.getOn();
            ExpressionChoice rhsOn;
            rhsOn = that.getOn();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "on", lhsOn), LocatorUtils.property(thatLocator, "on", rhsOn), lhsOn, rhsOn)) {
                return false;
            }
        }
        {
            String lhsNewCssClassName;
            lhsNewCssClassName = this.getNewCssClassName();
            String rhsNewCssClassName;
            rhsNewCssClassName = that.getNewCssClassName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "newCssClassName", lhsNewCssClassName), LocatorUtils.property(thatLocator, "newCssClassName", rhsNewCssClassName), lhsNewCssClassName, rhsNewCssClassName)) {
                return false;
            }
        }
        {
            String lhsOldCssClassName;
            lhsOldCssClassName = this.getOldCssClassName();
            String rhsOldCssClassName;
            rhsOldCssClassName = that.getOldCssClassName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "oldCssClassName", lhsOldCssClassName), LocatorUtils.property(thatLocator, "oldCssClassName", rhsOldCssClassName), lhsOldCssClassName, rhsOldCssClassName)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof ChangeCssClassAction) {
            final ChangeCssClassAction copy = ((ChangeCssClassAction) draftCopy);
            if (this.on!= null) {
                ExpressionChoice sourceOn;
                sourceOn = this.getOn();
                ExpressionChoice copyOn = ((ExpressionChoice) strategy.copy(LocatorUtils.property(locator, "on", sourceOn), sourceOn));
                copy.setOn(copyOn);
            } else {
                copy.on = null;
            }
            if (this.newCssClassName!= null) {
                String sourceNewCssClassName;
                sourceNewCssClassName = this.getNewCssClassName();
                String copyNewCssClassName = ((String) strategy.copy(LocatorUtils.property(locator, "newCssClassName", sourceNewCssClassName), sourceNewCssClassName));
                copy.setNewCssClassName(copyNewCssClassName);
            } else {
                copy.newCssClassName = null;
            }
            if (this.oldCssClassName!= null) {
                String sourceOldCssClassName;
                sourceOldCssClassName = this.getOldCssClassName();
                String copyOldCssClassName = ((String) strategy.copy(LocatorUtils.property(locator, "oldCssClassName", sourceOldCssClassName), sourceOldCssClassName));
                copy.setOldCssClassName(copyOldCssClassName);
            } else {
                copy.oldCssClassName = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ChangeCssClassAction();
    }

}
