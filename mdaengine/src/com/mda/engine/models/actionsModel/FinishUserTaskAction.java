
package com.mda.engine.models.actionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for FinishUserTaskAction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FinishUserTaskAction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/actionsModel}Action">
 *       &lt;attribute name="BindToMVCModelAttribute" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinishUserTaskAction")
public class FinishUserTaskAction
    extends Action
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "BindToMVCModelAttribute")
    protected String bindToMVCModelAttribute;

    /**
     * Gets the value of the bindToMVCModelAttribute property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBindToMVCModelAttribute() {
        return bindToMVCModelAttribute;
    }

    /**
     * Sets the value of the bindToMVCModelAttribute property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBindToMVCModelAttribute(String value) {
        this.bindToMVCModelAttribute = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof FinishUserTaskAction)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final FinishUserTaskAction that = ((FinishUserTaskAction) object);
        {
            String lhsBindToMVCModelAttribute;
            lhsBindToMVCModelAttribute = this.getBindToMVCModelAttribute();
            String rhsBindToMVCModelAttribute;
            rhsBindToMVCModelAttribute = that.getBindToMVCModelAttribute();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "bindToMVCModelAttribute", lhsBindToMVCModelAttribute), LocatorUtils.property(thatLocator, "bindToMVCModelAttribute", rhsBindToMVCModelAttribute), lhsBindToMVCModelAttribute, rhsBindToMVCModelAttribute)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof FinishUserTaskAction) {
            final FinishUserTaskAction copy = ((FinishUserTaskAction) draftCopy);
            if (this.bindToMVCModelAttribute!= null) {
                String sourceBindToMVCModelAttribute;
                sourceBindToMVCModelAttribute = this.getBindToMVCModelAttribute();
                String copyBindToMVCModelAttribute = ((String) strategy.copy(LocatorUtils.property(locator, "bindToMVCModelAttribute", sourceBindToMVCModelAttribute), sourceBindToMVCModelAttribute));
                copy.setBindToMVCModelAttribute(copyBindToMVCModelAttribute);
            } else {
                copy.bindToMVCModelAttribute = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new FinishUserTaskAction();
    }

}
