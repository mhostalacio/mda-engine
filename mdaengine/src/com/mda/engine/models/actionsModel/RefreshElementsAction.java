
package com.mda.engine.models.actionsModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for RefreshElementsAction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RefreshElementsAction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/actionsModel}Action">
 *       &lt;sequence>
 *         &lt;element name="Element" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}RefreshElementsActionElementExpression" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RefreshElementsAction", propOrder = {
    "element"
})
public class RefreshElementsAction
    extends Action
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Element", required = true)
    protected List<RefreshElementsActionElementExpression> element;

    /**
     * Gets the value of the element property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the element property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getElement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefreshElementsActionElementExpression }
     * 
     * 
     */
    public List<RefreshElementsActionElementExpression> getElement() {
        if (element == null) {
            element = new ArrayList<RefreshElementsActionElementExpression>();
        }
        return this.element;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof RefreshElementsAction)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final RefreshElementsAction that = ((RefreshElementsAction) object);
        {
            List<RefreshElementsActionElementExpression> lhsElement;
            lhsElement = this.getElement();
            List<RefreshElementsActionElementExpression> rhsElement;
            rhsElement = that.getElement();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "element", lhsElement), LocatorUtils.property(thatLocator, "element", rhsElement), lhsElement, rhsElement)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof RefreshElementsAction) {
            final RefreshElementsAction copy = ((RefreshElementsAction) draftCopy);
            if ((this.element!= null)&&(!this.element.isEmpty())) {
                List<RefreshElementsActionElementExpression> sourceElement;
                sourceElement = this.getElement();
                @SuppressWarnings("unchecked")
                List<RefreshElementsActionElementExpression> copyElement = ((List<RefreshElementsActionElementExpression> ) strategy.copy(LocatorUtils.property(locator, "element", sourceElement), sourceElement));
                copy.element = null;
                List<RefreshElementsActionElementExpression> uniqueElementl = copy.getElement();
                uniqueElementl.addAll(copyElement);
            } else {
                copy.element = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new RefreshElementsAction();
    }

}
