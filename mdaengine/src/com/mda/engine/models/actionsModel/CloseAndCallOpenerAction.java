
package com.mda.engine.models.actionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for CloseAndCallOpenerAction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CloseAndCallOpenerAction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/actionsModel}MVCAction">
 *       &lt;attribute name="AreaName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="ControllerName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="ActionName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CloseAndCallOpenerAction")
public class CloseAndCallOpenerAction
    extends MVCAction
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "AreaName", required = true)
    protected String areaName;
    @XmlAttribute(name = "ControllerName", required = true)
    protected String controllerName;
    @XmlAttribute(name = "ActionName", required = true)
    protected String actionName;

    /**
     * Gets the value of the areaName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAreaName() {
        return areaName;
    }

    /**
     * Sets the value of the areaName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAreaName(String value) {
        this.areaName = value;
    }

    /**
     * Gets the value of the controllerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControllerName() {
        return controllerName;
    }

    /**
     * Sets the value of the controllerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControllerName(String value) {
        this.controllerName = value;
    }

    /**
     * Gets the value of the actionName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionName() {
        return actionName;
    }

    /**
     * Sets the value of the actionName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionName(String value) {
        this.actionName = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof CloseAndCallOpenerAction)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final CloseAndCallOpenerAction that = ((CloseAndCallOpenerAction) object);
        {
            String lhsAreaName;
            lhsAreaName = this.getAreaName();
            String rhsAreaName;
            rhsAreaName = that.getAreaName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "areaName", lhsAreaName), LocatorUtils.property(thatLocator, "areaName", rhsAreaName), lhsAreaName, rhsAreaName)) {
                return false;
            }
        }
        {
            String lhsControllerName;
            lhsControllerName = this.getControllerName();
            String rhsControllerName;
            rhsControllerName = that.getControllerName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "controllerName", lhsControllerName), LocatorUtils.property(thatLocator, "controllerName", rhsControllerName), lhsControllerName, rhsControllerName)) {
                return false;
            }
        }
        {
            String lhsActionName;
            lhsActionName = this.getActionName();
            String rhsActionName;
            rhsActionName = that.getActionName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "actionName", lhsActionName), LocatorUtils.property(thatLocator, "actionName", rhsActionName), lhsActionName, rhsActionName)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof CloseAndCallOpenerAction) {
            final CloseAndCallOpenerAction copy = ((CloseAndCallOpenerAction) draftCopy);
            if (this.areaName!= null) {
                String sourceAreaName;
                sourceAreaName = this.getAreaName();
                String copyAreaName = ((String) strategy.copy(LocatorUtils.property(locator, "areaName", sourceAreaName), sourceAreaName));
                copy.setAreaName(copyAreaName);
            } else {
                copy.areaName = null;
            }
            if (this.controllerName!= null) {
                String sourceControllerName;
                sourceControllerName = this.getControllerName();
                String copyControllerName = ((String) strategy.copy(LocatorUtils.property(locator, "controllerName", sourceControllerName), sourceControllerName));
                copy.setControllerName(copyControllerName);
            } else {
                copy.controllerName = null;
            }
            if (this.actionName!= null) {
                String sourceActionName;
                sourceActionName = this.getActionName();
                String copyActionName = ((String) strategy.copy(LocatorUtils.property(locator, "actionName", sourceActionName), sourceActionName));
                copy.setActionName(copyActionName);
            } else {
                copy.actionName = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new CloseAndCallOpenerAction();
    }

}
