
package com.mda.engine.models.actionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.models.expressionsModel.ExpressionChoice;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for RaiseMessageAction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RaiseMessageAction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/actionsModel}Action">
 *       &lt;sequence>
 *         &lt;element name="Message" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ExpressionChoice"/>
 *       &lt;/sequence>
 *       &lt;attribute name="MessageType" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}MessageType" default="Info" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RaiseMessageAction", propOrder = {
    "message"
})
public class RaiseMessageAction
    extends Action
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Message", required = true)
    protected ExpressionChoice message;
    @XmlAttribute(name = "MessageType")
    protected MessageType messageType;

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link ExpressionChoice }
     *     
     */
    public ExpressionChoice getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpressionChoice }
     *     
     */
    public void setMessage(ExpressionChoice value) {
        this.message = value;
    }

    /**
     * Gets the value of the messageType property.
     * 
     * @return
     *     possible object is
     *     {@link MessageType }
     *     
     */
    public MessageType getMessageType() {
        if (messageType == null) {
            return MessageType.INFO;
        } else {
            return messageType;
        }
    }

    /**
     * Sets the value of the messageType property.
     * 
     * @param value
     *     allowed object is
     *     {@link MessageType }
     *     
     */
    public void setMessageType(MessageType value) {
        this.messageType = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof RaiseMessageAction)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final RaiseMessageAction that = ((RaiseMessageAction) object);
        {
            ExpressionChoice lhsMessage;
            lhsMessage = this.getMessage();
            ExpressionChoice rhsMessage;
            rhsMessage = that.getMessage();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "message", lhsMessage), LocatorUtils.property(thatLocator, "message", rhsMessage), lhsMessage, rhsMessage)) {
                return false;
            }
        }
        {
            MessageType lhsMessageType;
            lhsMessageType = this.getMessageType();
            MessageType rhsMessageType;
            rhsMessageType = that.getMessageType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "messageType", lhsMessageType), LocatorUtils.property(thatLocator, "messageType", rhsMessageType), lhsMessageType, rhsMessageType)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof RaiseMessageAction) {
            final RaiseMessageAction copy = ((RaiseMessageAction) draftCopy);
            if (this.message!= null) {
                ExpressionChoice sourceMessage;
                sourceMessage = this.getMessage();
                ExpressionChoice copyMessage = ((ExpressionChoice) strategy.copy(LocatorUtils.property(locator, "message", sourceMessage), sourceMessage));
                copy.setMessage(copyMessage);
            } else {
                copy.message = null;
            }
            if (this.messageType!= null) {
                MessageType sourceMessageType;
                sourceMessageType = this.getMessageType();
                MessageType copyMessageType = ((MessageType) strategy.copy(LocatorUtils.property(locator, "messageType", sourceMessageType), sourceMessageType));
                copy.setMessageType(copyMessageType);
            } else {
                copy.messageType = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new RaiseMessageAction();
    }

}
