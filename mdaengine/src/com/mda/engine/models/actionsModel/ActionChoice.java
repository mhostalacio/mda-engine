
package com.mda.engine.models.actionsModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ActionChoice complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ActionChoice">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;element name="Back" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}BackAction"/>
 *         &lt;element name="ToggleVisibility" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}ToggleVisibilityAction"/>
 *         &lt;element name="SetVisibility" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}SetVisibilityAction"/>
 *         &lt;element name="ChangeCssClass" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}ChangeCssClassAction"/>
 *         &lt;element name="Alert" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}AlertAction"/>
 *         &lt;element name="CallMethod" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}CallMethodAction"/>
 *         &lt;element name="If" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}IfAction"/>
 *         &lt;element name="SetValue" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}SetValueAction"/>
 *         &lt;element name="RefreshElements" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}RefreshElementsAction"/>
 *         &lt;element name="Initialize" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}InitializeAction"/>
 *         &lt;element name="CreateVariable" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}CreateVariableAction"/>
 *         &lt;element name="Persist" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}PersistAction"/>
 *         &lt;element name="RedirectToAction" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}RedirectToActionTrigger"/>
 *         &lt;element name="RaiseMessage" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}RaiseMessageAction"/>
 *         &lt;element name="InvokeBusinessTransaction" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}InvokeBusinessTransactionAction"/>
 *         &lt;element name="CallEditor" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}CallEditorAction"/>
 *         &lt;element name="Return" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}ReturnAction"/>
 *         &lt;element name="CallEditorModelInitialization" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}CallEditorModelInitializationAction"/>
 *         &lt;element name="SetProcessData" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}SetProcessDataAction"/>
 *         &lt;element name="FinishUserTask" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}FinishUserTaskAction"/>
 *         &lt;element name="SaveProcess" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}SaveProcessAction"/>
 *         &lt;element name="CallInitAction" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}CallInitAction"/>
 *         &lt;element name="CloseAndCallOpenerAction" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}CloseAndCallOpenerAction"/>
 *         &lt;element name="CloseSupportView" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}CloseSupportViewAction"/>
 *         &lt;element name="CloseModalSupportView" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}CloseModalSupportViewAction"/>
 *         &lt;element name="CloseModalDialog" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}CloseModalDialogAction"/>
 *         &lt;element name="AddElementToList" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}AddElementToListAction"/>
 *         &lt;element name="ProcessEditorOutput" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}InvokeEditorOutputProcess"/>
 *         &lt;element name="AddContextValue" type="{http://www.mdaengine.com/mdaengine/models/actionsModel}AddValueToTransactionContext"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ActionChoice", propOrder = {
    "action"
})
public class ActionChoice
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElements({
        @XmlElement(name = "Back", type = BackAction.class),
        @XmlElement(name = "CallMethod", type = CallMethodAction.class),
        @XmlElement(name = "SaveProcess", type = SaveProcessAction.class),
        @XmlElement(name = "AddContextValue", type = AddValueToTransactionContext.class),
        @XmlElement(name = "Persist", type = PersistAction.class),
        @XmlElement(name = "CallEditor", type = CallEditorAction.class),
        @XmlElement(name = "ToggleVisibility", type = ToggleVisibilityAction.class),
        @XmlElement(name = "AddElementToList", type = AddElementToListAction.class),
        @XmlElement(name = "ProcessEditorOutput", type = InvokeEditorOutputProcess.class),
        @XmlElement(name = "ChangeCssClass", type = ChangeCssClassAction.class),
        @XmlElement(name = "SetProcessData", type = SetProcessDataAction.class),
        @XmlElement(name = "CallInitAction", type = CallInitAction.class),
        @XmlElement(name = "RefreshElements", type = RefreshElementsAction.class),
        @XmlElement(name = "Initialize", type = InitializeAction.class),
        @XmlElement(name = "CloseModalSupportView", type = CloseModalSupportViewAction.class),
        @XmlElement(name = "CloseModalDialog", type = CloseModalDialogAction.class),
        @XmlElement(name = "RaiseMessage", type = RaiseMessageAction.class),
        @XmlElement(name = "FinishUserTask", type = FinishUserTaskAction.class),
        @XmlElement(name = "InvokeBusinessTransaction", type = InvokeBusinessTransactionAction.class),
        @XmlElement(name = "CreateVariable", type = CreateVariableAction.class),
        @XmlElement(name = "Alert", type = AlertAction.class),
        @XmlElement(name = "CloseAndCallOpenerAction", type = CloseAndCallOpenerAction.class),
        @XmlElement(name = "If", type = IfAction.class),
        @XmlElement(name = "CallEditorModelInitialization", type = CallEditorModelInitializationAction.class),
        @XmlElement(name = "SetVisibility", type = SetVisibilityAction.class),
        @XmlElement(name = "Return", type = ReturnAction.class),
        @XmlElement(name = "RedirectToAction", type = RedirectToActionTrigger.class),
        @XmlElement(name = "CloseSupportView", type = CloseSupportViewAction.class),
        @XmlElement(name = "SetValue", type = SetValueAction.class)
    })
    protected List<Action> action;

    /**
     * Gets the value of the action property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the action property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAction().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BackAction }
     * {@link CallMethodAction }
     * {@link SaveProcessAction }
     * {@link AddValueToTransactionContext }
     * {@link PersistAction }
     * {@link CallEditorAction }
     * {@link ToggleVisibilityAction }
     * {@link AddElementToListAction }
     * {@link InvokeEditorOutputProcess }
     * {@link ChangeCssClassAction }
     * {@link SetProcessDataAction }
     * {@link CallInitAction }
     * {@link RefreshElementsAction }
     * {@link InitializeAction }
     * {@link CloseModalSupportViewAction }
     * {@link CloseModalDialogAction }
     * {@link RaiseMessageAction }
     * {@link FinishUserTaskAction }
     * {@link InvokeBusinessTransactionAction }
     * {@link CreateVariableAction }
     * {@link AlertAction }
     * {@link CloseAndCallOpenerAction }
     * {@link IfAction }
     * {@link CallEditorModelInitializationAction }
     * {@link SetVisibilityAction }
     * {@link ReturnAction }
     * {@link RedirectToActionTrigger }
     * {@link CloseSupportViewAction }
     * {@link SetValueAction }
     * 
     * 
     */
    public List<Action> getAction() {
        if (action == null) {
            action = new ArrayList<Action>();
        }
        return this.action;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ActionChoice)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ActionChoice that = ((ActionChoice) object);
        {
            List<Action> lhsAction;
            lhsAction = this.getAction();
            List<Action> rhsAction;
            rhsAction = that.getAction();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "action", lhsAction), LocatorUtils.property(thatLocator, "action", rhsAction), lhsAction, rhsAction)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof ActionChoice) {
            final ActionChoice copy = ((ActionChoice) draftCopy);
            if ((this.action!= null)&&(!this.action.isEmpty())) {
                List<Action> sourceAction;
                sourceAction = this.getAction();
                @SuppressWarnings("unchecked")
                List<Action> copyAction = ((List<Action> ) strategy.copy(LocatorUtils.property(locator, "action", sourceAction), sourceAction));
                copy.action = null;
                List<Action> uniqueActionl = copy.getAction();
                uniqueActionl.addAll(copyAction);
            } else {
                copy.action = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ActionChoice();
    }

}
