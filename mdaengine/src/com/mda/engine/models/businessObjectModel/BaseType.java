
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for BaseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BaseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="Name" use="required" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}RegularString" />
 *       &lt;attribute name="Serializable" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *       &lt;attribute name="Persistable" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *       &lt;attribute name="ConvertToMVCModel" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BaseType")
@XmlSeeAlso({
    ObjectType.class,
    TargetAttributeType.class,
    BaseAttributeType.class
})
@XmlRootElement
public class BaseType
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "Name", required = true)
    protected String name;
    @XmlAttribute(name = "Serializable")
    protected Boolean serializable;
    @XmlAttribute(name = "Persistable")
    protected Boolean persistable;
    @XmlAttribute(name = "ConvertToMVCModel")
    protected Boolean convertToMVCModel;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the serializable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isSerializable() {
        if (serializable == null) {
            return true;
        } else {
            return serializable;
        }
    }

    /**
     * Sets the value of the serializable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSerializable(Boolean value) {
        this.serializable = value;
    }

    /**
     * Gets the value of the persistable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isPersistable() {
        if (persistable == null) {
            return true;
        } else {
            return persistable;
        }
    }

    /**
     * Sets the value of the persistable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPersistable(Boolean value) {
        this.persistable = value;
    }

    /**
     * Gets the value of the convertToMVCModel property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isConvertToMVCModel() {
        return convertToMVCModel;
    }

    /**
     * Sets the value of the convertToMVCModel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setConvertToMVCModel(Boolean value) {
        this.convertToMVCModel = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof BaseType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final BaseType that = ((BaseType) object);
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            boolean lhsSerializable;
            lhsSerializable = this.isSerializable();
            boolean rhsSerializable;
            rhsSerializable = that.isSerializable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "serializable", lhsSerializable), LocatorUtils.property(thatLocator, "serializable", rhsSerializable), lhsSerializable, rhsSerializable)) {
                return false;
            }
        }
        {
            boolean lhsPersistable;
            lhsPersistable = this.isPersistable();
            boolean rhsPersistable;
            rhsPersistable = that.isPersistable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "persistable", lhsPersistable), LocatorUtils.property(thatLocator, "persistable", rhsPersistable), lhsPersistable, rhsPersistable)) {
                return false;
            }
        }
        {
            Boolean lhsConvertToMVCModel;
            lhsConvertToMVCModel = this.isConvertToMVCModel();
            Boolean rhsConvertToMVCModel;
            rhsConvertToMVCModel = that.isConvertToMVCModel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "convertToMVCModel", lhsConvertToMVCModel), LocatorUtils.property(thatLocator, "convertToMVCModel", rhsConvertToMVCModel), lhsConvertToMVCModel, rhsConvertToMVCModel)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof BaseType) {
            final BaseType copy = ((BaseType) draftCopy);
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.serializable!= null) {
                boolean sourceSerializable;
                sourceSerializable = this.isSerializable();
                boolean copySerializable = strategy.copy(LocatorUtils.property(locator, "serializable", sourceSerializable), sourceSerializable);
                copy.setSerializable(copySerializable);
            } else {
                copy.serializable = null;
            }
            if (this.persistable!= null) {
                boolean sourcePersistable;
                sourcePersistable = this.isPersistable();
                boolean copyPersistable = strategy.copy(LocatorUtils.property(locator, "persistable", sourcePersistable), sourcePersistable);
                copy.setPersistable(copyPersistable);
            } else {
                copy.persistable = null;
            }
            if (this.convertToMVCModel!= null) {
                Boolean sourceConvertToMVCModel;
                sourceConvertToMVCModel = this.isConvertToMVCModel();
                Boolean copyConvertToMVCModel = ((Boolean) strategy.copy(LocatorUtils.property(locator, "convertToMVCModel", sourceConvertToMVCModel), sourceConvertToMVCModel));
                copy.setConvertToMVCModel(copyConvertToMVCModel);
            } else {
                copy.convertToMVCModel = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new BaseType();
    }

}
