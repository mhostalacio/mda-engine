
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ActionRule complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ActionRule">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;element name="SetEnable" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}AttributeNameType" minOccurs="0"/>
 *         &lt;element name="SetDisable" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}AttributeNameType" minOccurs="0"/>
 *         &lt;element name="SetHidden" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}AttributeNameType" minOccurs="0"/>
 *         &lt;element name="SetVisible" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}AttributeNameType" minOccurs="0"/>
 *         &lt;element name="SetValue" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="AttributeName" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}NotEmptyString"/>
 *                   &lt;element name="Value" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}NotEmptyString"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="SetRestriction" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="AttributeName" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}RegularString"/>
 *                   &lt;element name="Restriction" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}RestrictionType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ActionRule", propOrder = {
    "setEnableOrSetDisableOrSetHidden"
})
public class ActionRule
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElementRefs({
        @XmlElementRef(name = "SetVisible", namespace = "http://www.mdaengine.com/mdaengine/models/businessObjectModel", type = JAXBElement.class),
        @XmlElementRef(name = "SetDisable", namespace = "http://www.mdaengine.com/mdaengine/models/businessObjectModel", type = JAXBElement.class),
        @XmlElementRef(name = "SetHidden", namespace = "http://www.mdaengine.com/mdaengine/models/businessObjectModel", type = JAXBElement.class),
        @XmlElementRef(name = "SetValue", namespace = "http://www.mdaengine.com/mdaengine/models/businessObjectModel", type = JAXBElement.class),
        @XmlElementRef(name = "SetRestriction", namespace = "http://www.mdaengine.com/mdaengine/models/businessObjectModel", type = JAXBElement.class),
        @XmlElementRef(name = "SetEnable", namespace = "http://www.mdaengine.com/mdaengine/models/businessObjectModel", type = JAXBElement.class)
    })
    protected List<JAXBElement<? extends ModelNodeBase>> setEnableOrSetDisableOrSetHidden;

    /**
     * Gets the value of the setEnableOrSetDisableOrSetHidden property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the setEnableOrSetDisableOrSetHidden property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSetEnableOrSetDisableOrSetHidden().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link AttributeNameType }{@code >}
     * {@link JAXBElement }{@code <}{@link AttributeNameType }{@code >}
     * {@link JAXBElement }{@code <}{@link AttributeNameType }{@code >}
     * {@link JAXBElement }{@code <}{@link ActionRule.SetValue }{@code >}
     * {@link JAXBElement }{@code <}{@link ActionRule.SetRestriction }{@code >}
     * {@link JAXBElement }{@code <}{@link AttributeNameType }{@code >}
     * 
     * 
     */
    public List<JAXBElement<? extends ModelNodeBase>> getSetEnableOrSetDisableOrSetHidden() {
        if (setEnableOrSetDisableOrSetHidden == null) {
            setEnableOrSetDisableOrSetHidden = new ArrayList<JAXBElement<? extends ModelNodeBase>>();
        }
        return this.setEnableOrSetDisableOrSetHidden;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ActionRule)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ActionRule that = ((ActionRule) object);
        {
            List<JAXBElement<? extends ModelNodeBase>> lhsSetEnableOrSetDisableOrSetHidden;
            lhsSetEnableOrSetDisableOrSetHidden = this.getSetEnableOrSetDisableOrSetHidden();
            List<JAXBElement<? extends ModelNodeBase>> rhsSetEnableOrSetDisableOrSetHidden;
            rhsSetEnableOrSetDisableOrSetHidden = that.getSetEnableOrSetDisableOrSetHidden();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "setEnableOrSetDisableOrSetHidden", lhsSetEnableOrSetDisableOrSetHidden), LocatorUtils.property(thatLocator, "setEnableOrSetDisableOrSetHidden", rhsSetEnableOrSetDisableOrSetHidden), lhsSetEnableOrSetDisableOrSetHidden, rhsSetEnableOrSetDisableOrSetHidden)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof ActionRule) {
            final ActionRule copy = ((ActionRule) draftCopy);
            if ((this.setEnableOrSetDisableOrSetHidden!= null)&&(!this.setEnableOrSetDisableOrSetHidden.isEmpty())) {
                List<JAXBElement<? extends ModelNodeBase>> sourceSetEnableOrSetDisableOrSetHidden;
                sourceSetEnableOrSetDisableOrSetHidden = this.getSetEnableOrSetDisableOrSetHidden();
                @SuppressWarnings("unchecked")
                List<JAXBElement<? extends ModelNodeBase>> copySetEnableOrSetDisableOrSetHidden = ((List<JAXBElement<? extends ModelNodeBase>> ) strategy.copy(LocatorUtils.property(locator, "setEnableOrSetDisableOrSetHidden", sourceSetEnableOrSetDisableOrSetHidden), sourceSetEnableOrSetDisableOrSetHidden));
                copy.setEnableOrSetDisableOrSetHidden = null;
                List<JAXBElement<? extends ModelNodeBase>> uniqueSetEnableOrSetDisableOrSetHiddenl = copy.getSetEnableOrSetDisableOrSetHidden();
                uniqueSetEnableOrSetDisableOrSetHiddenl.addAll(copySetEnableOrSetDisableOrSetHidden);
            } else {
                copy.setEnableOrSetDisableOrSetHidden = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ActionRule();
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="AttributeName" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}RegularString"/>
     *         &lt;element name="Restriction" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}RestrictionType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "attributeName",
        "restriction"
    })
    public static class SetRestriction
        extends ModelNodeBase
        implements Serializable, Cloneable, CopyTo, Equals
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "AttributeName", required = true)
        protected String attributeName;
        @XmlElement(name = "Restriction", required = true)
        protected RestrictionType restriction;

        /**
         * Gets the value of the attributeName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAttributeName() {
            return attributeName;
        }

        /**
         * Sets the value of the attributeName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAttributeName(String value) {
            this.attributeName = value;
        }

        /**
         * Gets the value of the restriction property.
         * 
         * @return
         *     possible object is
         *     {@link RestrictionType }
         *     
         */
        public RestrictionType getRestriction() {
            return restriction;
        }

        /**
         * Sets the value of the restriction property.
         * 
         * @param value
         *     allowed object is
         *     {@link RestrictionType }
         *     
         */
        public void setRestriction(RestrictionType value) {
            this.restriction = value;
        }

        public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
            if (!(object instanceof ActionRule.SetRestriction)) {
                return false;
            }
            if (this == object) {
                return true;
            }
            final ActionRule.SetRestriction that = ((ActionRule.SetRestriction) object);
            {
                String lhsAttributeName;
                lhsAttributeName = this.getAttributeName();
                String rhsAttributeName;
                rhsAttributeName = that.getAttributeName();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "attributeName", lhsAttributeName), LocatorUtils.property(thatLocator, "attributeName", rhsAttributeName), lhsAttributeName, rhsAttributeName)) {
                    return false;
                }
            }
            {
                RestrictionType lhsRestriction;
                lhsRestriction = this.getRestriction();
                RestrictionType rhsRestriction;
                rhsRestriction = that.getRestriction();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "restriction", lhsRestriction), LocatorUtils.property(thatLocator, "restriction", rhsRestriction), lhsRestriction, rhsRestriction)) {
                    return false;
                }
            }
            return true;
        }

        public boolean equals(Object object) {
            final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
            return equals(null, null, object, strategy);
        }

        public Object clone() {
            return copyTo(createNewInstance());
        }

        public Object copyTo(Object target) {
            final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
            return copyTo(null, target, strategy);
        }

        public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
            final Object draftCopy = ((target == null)?createNewInstance():target);
            if (draftCopy instanceof ActionRule.SetRestriction) {
                final ActionRule.SetRestriction copy = ((ActionRule.SetRestriction) draftCopy);
                if (this.attributeName!= null) {
                    String sourceAttributeName;
                    sourceAttributeName = this.getAttributeName();
                    String copyAttributeName = ((String) strategy.copy(LocatorUtils.property(locator, "attributeName", sourceAttributeName), sourceAttributeName));
                    copy.setAttributeName(copyAttributeName);
                } else {
                    copy.attributeName = null;
                }
                if (this.restriction!= null) {
                    RestrictionType sourceRestriction;
                    sourceRestriction = this.getRestriction();
                    RestrictionType copyRestriction = ((RestrictionType) strategy.copy(LocatorUtils.property(locator, "restriction", sourceRestriction), sourceRestriction));
                    copy.setRestriction(copyRestriction);
                } else {
                    copy.restriction = null;
                }
            }
            return draftCopy;
        }

        public Object createNewInstance() {
            return new ActionRule.SetRestriction();
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="AttributeName" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}NotEmptyString"/>
     *         &lt;element name="Value" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}NotEmptyString"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "attributeName",
        "value"
    })
    public static class SetValue
        extends ModelNodeBase
        implements Serializable, Cloneable, CopyTo, Equals
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "AttributeName", required = true)
        protected String attributeName;
        @XmlElement(name = "Value", required = true)
        protected String value;

        /**
         * Gets the value of the attributeName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAttributeName() {
            return attributeName;
        }

        /**
         * Sets the value of the attributeName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAttributeName(String value) {
            this.attributeName = value;
        }

        /**
         * Gets the value of the value property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

        public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
            if (!(object instanceof ActionRule.SetValue)) {
                return false;
            }
            if (this == object) {
                return true;
            }
            final ActionRule.SetValue that = ((ActionRule.SetValue) object);
            {
                String lhsAttributeName;
                lhsAttributeName = this.getAttributeName();
                String rhsAttributeName;
                rhsAttributeName = that.getAttributeName();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "attributeName", lhsAttributeName), LocatorUtils.property(thatLocator, "attributeName", rhsAttributeName), lhsAttributeName, rhsAttributeName)) {
                    return false;
                }
            }
            {
                String lhsValue;
                lhsValue = this.getValue();
                String rhsValue;
                rhsValue = that.getValue();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "value", lhsValue), LocatorUtils.property(thatLocator, "value", rhsValue), lhsValue, rhsValue)) {
                    return false;
                }
            }
            return true;
        }

        public boolean equals(Object object) {
            final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
            return equals(null, null, object, strategy);
        }

        public Object clone() {
            return copyTo(createNewInstance());
        }

        public Object copyTo(Object target) {
            final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
            return copyTo(null, target, strategy);
        }

        public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
            final Object draftCopy = ((target == null)?createNewInstance():target);
            if (draftCopy instanceof ActionRule.SetValue) {
                final ActionRule.SetValue copy = ((ActionRule.SetValue) draftCopy);
                if (this.attributeName!= null) {
                    String sourceAttributeName;
                    sourceAttributeName = this.getAttributeName();
                    String copyAttributeName = ((String) strategy.copy(LocatorUtils.property(locator, "attributeName", sourceAttributeName), sourceAttributeName));
                    copy.setAttributeName(copyAttributeName);
                } else {
                    copy.attributeName = null;
                }
                if (this.value!= null) {
                    String sourceValue;
                    sourceValue = this.getValue();
                    String copyValue = ((String) strategy.copy(LocatorUtils.property(locator, "value", sourceValue), sourceValue));
                    copy.setValue(copyValue);
                } else {
                    copy.value = null;
                }
            }
            return draftCopy;
        }

        public Object createNewInstance() {
            return new ActionRule.SetValue();
        }

    }

}
