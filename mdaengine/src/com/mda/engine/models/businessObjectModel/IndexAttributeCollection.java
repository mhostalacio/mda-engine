
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for IndexAttributeCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IndexAttributeCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="IndexAttribute" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}IndexAttribute" maxOccurs="unbounded"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IndexAttributeCollection", propOrder = {
    "indexAttribute"
})
public class IndexAttributeCollection
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "IndexAttribute")
    protected List<IndexAttribute> indexAttribute;

    /**
     * Gets the value of the indexAttribute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the indexAttribute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIndexAttribute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IndexAttribute }
     * 
     * 
     */
    public List<IndexAttribute> getIndexAttribute() {
        if (indexAttribute == null) {
            indexAttribute = new ArrayList<IndexAttribute>();
        }
        return this.indexAttribute;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof IndexAttributeCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final IndexAttributeCollection that = ((IndexAttributeCollection) object);
        {
            List<IndexAttribute> lhsIndexAttribute;
            lhsIndexAttribute = this.getIndexAttribute();
            List<IndexAttribute> rhsIndexAttribute;
            rhsIndexAttribute = that.getIndexAttribute();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "indexAttribute", lhsIndexAttribute), LocatorUtils.property(thatLocator, "indexAttribute", rhsIndexAttribute), lhsIndexAttribute, rhsIndexAttribute)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof IndexAttributeCollection) {
            final IndexAttributeCollection copy = ((IndexAttributeCollection) draftCopy);
            if ((this.indexAttribute!= null)&&(!this.indexAttribute.isEmpty())) {
                List<IndexAttribute> sourceIndexAttribute;
                sourceIndexAttribute = this.getIndexAttribute();
                @SuppressWarnings("unchecked")
                List<IndexAttribute> copyIndexAttribute = ((List<IndexAttribute> ) strategy.copy(LocatorUtils.property(locator, "indexAttribute", sourceIndexAttribute), sourceIndexAttribute));
                copy.indexAttribute = null;
                List<IndexAttribute> uniqueIndexAttributel = copy.getIndexAttribute();
                uniqueIndexAttributel.addAll(copyIndexAttribute);
            } else {
                copy.indexAttribute = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new IndexAttributeCollection();
    }

}
