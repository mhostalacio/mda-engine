
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for RegexRestrictionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RegexRestrictionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RegexValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RegexRestrictionType", propOrder = {
    "regexValue"
})
public class RegexRestrictionType
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "RegexValue", required = true)
    protected String regexValue;

    /**
     * Gets the value of the regexValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegexValue() {
        return regexValue;
    }

    /**
     * Sets the value of the regexValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegexValue(String value) {
        this.regexValue = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof RegexRestrictionType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final RegexRestrictionType that = ((RegexRestrictionType) object);
        {
            String lhsRegexValue;
            lhsRegexValue = this.getRegexValue();
            String rhsRegexValue;
            rhsRegexValue = that.getRegexValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "regexValue", lhsRegexValue), LocatorUtils.property(thatLocator, "regexValue", rhsRegexValue), lhsRegexValue, rhsRegexValue)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof RegexRestrictionType) {
            final RegexRestrictionType copy = ((RegexRestrictionType) draftCopy);
            if (this.regexValue!= null) {
                String sourceRegexValue;
                sourceRegexValue = this.getRegexValue();
                String copyRegexValue = ((String) strategy.copy(LocatorUtils.property(locator, "regexValue", sourceRegexValue), sourceRegexValue));
                copy.setRegexValue(copyRegexValue);
            } else {
                copy.regexValue = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new RegexRestrictionType();
    }

}
