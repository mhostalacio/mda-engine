
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for AggregationAttributeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AggregationAttributeType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}BaseAttributeWithTargetType">
 *       &lt;sequence>
 *         &lt;element name="TargetAttribute" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}TargetAttributeType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="IsNavigable" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *       &lt;attribute name="AvoidGenerateAggIndex" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="TargetMultiplicity" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}MultiplicityType" default="0..*" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AggregationAttributeType", propOrder = {
    "targetAttribute"
})
public class AggregationAttributeType
    extends BaseAttributeWithTargetType
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "TargetAttribute")
    protected TargetAttributeType targetAttribute;
    @XmlAttribute(name = "IsNavigable")
    protected Boolean isNavigable;
    @XmlAttribute(name = "AvoidGenerateAggIndex")
    protected Boolean avoidGenerateAggIndex;
    @XmlAttribute(name = "TargetMultiplicity")
    protected String targetMultiplicity;

    /**
     * Gets the value of the targetAttribute property.
     * 
     * @return
     *     possible object is
     *     {@link TargetAttributeType }
     *     
     */
    public TargetAttributeType getTargetAttribute() {
        return targetAttribute;
    }

    /**
     * Sets the value of the targetAttribute property.
     * 
     * @param value
     *     allowed object is
     *     {@link TargetAttributeType }
     *     
     */
    public void setTargetAttribute(TargetAttributeType value) {
        this.targetAttribute = value;
    }

    /**
     * Gets the value of the isNavigable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isIsNavigable() {
        if (isNavigable == null) {
            return true;
        } else {
            return isNavigable;
        }
    }

    /**
     * Sets the value of the isNavigable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsNavigable(Boolean value) {
        this.isNavigable = value;
    }

    /**
     * Gets the value of the avoidGenerateAggIndex property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isAvoidGenerateAggIndex() {
        if (avoidGenerateAggIndex == null) {
            return false;
        } else {
            return avoidGenerateAggIndex;
        }
    }

    /**
     * Sets the value of the avoidGenerateAggIndex property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAvoidGenerateAggIndex(Boolean value) {
        this.avoidGenerateAggIndex = value;
    }

    /**
     * Gets the value of the targetMultiplicity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetMultiplicity() {
        if (targetMultiplicity == null) {
            return "0..*";
        } else {
            return targetMultiplicity;
        }
    }

    /**
     * Sets the value of the targetMultiplicity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetMultiplicity(String value) {
        this.targetMultiplicity = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof AggregationAttributeType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final AggregationAttributeType that = ((AggregationAttributeType) object);
        {
            TargetAttributeType lhsTargetAttribute;
            lhsTargetAttribute = this.getTargetAttribute();
            TargetAttributeType rhsTargetAttribute;
            rhsTargetAttribute = that.getTargetAttribute();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "targetAttribute", lhsTargetAttribute), LocatorUtils.property(thatLocator, "targetAttribute", rhsTargetAttribute), lhsTargetAttribute, rhsTargetAttribute)) {
                return false;
            }
        }
        {
            boolean lhsIsNavigable;
            lhsIsNavigable = this.isIsNavigable();
            boolean rhsIsNavigable;
            rhsIsNavigable = that.isIsNavigable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "isNavigable", lhsIsNavigable), LocatorUtils.property(thatLocator, "isNavigable", rhsIsNavigable), lhsIsNavigable, rhsIsNavigable)) {
                return false;
            }
        }
        {
            boolean lhsAvoidGenerateAggIndex;
            lhsAvoidGenerateAggIndex = this.isAvoidGenerateAggIndex();
            boolean rhsAvoidGenerateAggIndex;
            rhsAvoidGenerateAggIndex = that.isAvoidGenerateAggIndex();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "avoidGenerateAggIndex", lhsAvoidGenerateAggIndex), LocatorUtils.property(thatLocator, "avoidGenerateAggIndex", rhsAvoidGenerateAggIndex), lhsAvoidGenerateAggIndex, rhsAvoidGenerateAggIndex)) {
                return false;
            }
        }
        {
            String lhsTargetMultiplicity;
            lhsTargetMultiplicity = this.getTargetMultiplicity();
            String rhsTargetMultiplicity;
            rhsTargetMultiplicity = that.getTargetMultiplicity();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "targetMultiplicity", lhsTargetMultiplicity), LocatorUtils.property(thatLocator, "targetMultiplicity", rhsTargetMultiplicity), lhsTargetMultiplicity, rhsTargetMultiplicity)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof AggregationAttributeType) {
            final AggregationAttributeType copy = ((AggregationAttributeType) draftCopy);
            if (this.targetAttribute!= null) {
                TargetAttributeType sourceTargetAttribute;
                sourceTargetAttribute = this.getTargetAttribute();
                TargetAttributeType copyTargetAttribute = ((TargetAttributeType) strategy.copy(LocatorUtils.property(locator, "targetAttribute", sourceTargetAttribute), sourceTargetAttribute));
                copy.setTargetAttribute(copyTargetAttribute);
            } else {
                copy.targetAttribute = null;
            }
            if (this.isNavigable!= null) {
                boolean sourceIsNavigable;
                sourceIsNavigable = this.isIsNavigable();
                boolean copyIsNavigable = strategy.copy(LocatorUtils.property(locator, "isNavigable", sourceIsNavigable), sourceIsNavigable);
                copy.setIsNavigable(copyIsNavigable);
            } else {
                copy.isNavigable = null;
            }
            if (this.avoidGenerateAggIndex!= null) {
                boolean sourceAvoidGenerateAggIndex;
                sourceAvoidGenerateAggIndex = this.isAvoidGenerateAggIndex();
                boolean copyAvoidGenerateAggIndex = strategy.copy(LocatorUtils.property(locator, "avoidGenerateAggIndex", sourceAvoidGenerateAggIndex), sourceAvoidGenerateAggIndex);
                copy.setAvoidGenerateAggIndex(copyAvoidGenerateAggIndex);
            } else {
                copy.avoidGenerateAggIndex = null;
            }
            if (this.targetMultiplicity!= null) {
                String sourceTargetMultiplicity;
                sourceTargetMultiplicity = this.getTargetMultiplicity();
                String copyTargetMultiplicity = ((String) strategy.copy(LocatorUtils.property(locator, "targetMultiplicity", sourceTargetMultiplicity), sourceTargetMultiplicity));
                copy.setTargetMultiplicity(copyTargetMultiplicity);
            } else {
                copy.targetMultiplicity = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new AggregationAttributeType();
    }
    
//--simple--preserve
    
    private transient Boolean autoCreated = false;
    private transient ObjectType aggregationObject;
    
    public void convertToNullable() {
		if (this.getMultiplicity().equals("1")) {

			this.setMultiplicity("0..1");
		} else if (this.getMultiplicity().equals("1..*")) {

			this.setMultiplicity("0..*");
		}
	}
 
    public Boolean isAutoCreated() {

		return this.autoCreated;
	}
    
    public void setAutoCreated(Boolean autoCreated)
    {
    	this.autoCreated = autoCreated;
    }

	public void setAggregationObject(ObjectType aggregationObject) {
		this.aggregationObject = aggregationObject;
	}

	public ObjectType getAggregationObject() {
		return aggregationObject;
	}
    
	
	
//--simple--preserve

}
