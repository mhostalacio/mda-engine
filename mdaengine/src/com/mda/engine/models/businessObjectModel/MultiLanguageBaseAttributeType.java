
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MultiLanguageBaseAttributeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MultiLanguageBaseAttributeType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}BaseAttributeType">
 *       &lt;attribute name="IsMultiLanguage" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="UseNVARCHAR" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="IsCaseSensitive" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="Unicode" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="UseSparseColumn" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MultiLanguageBaseAttributeType")
@XmlSeeAlso({
    LongtextAttributeType.class,
    TextAttributeType.class,
    CharAttributeType.class
})
public class MultiLanguageBaseAttributeType
    extends BaseAttributeType
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "IsMultiLanguage")
    protected Boolean isMultiLanguage;
    @XmlAttribute(name = "UseNVARCHAR")
    protected Boolean useNVARCHAR;
    @XmlAttribute(name = "IsCaseSensitive")
    protected Boolean isCaseSensitive;
    @XmlAttribute(name = "Unicode")
    protected Boolean unicode;
    @XmlAttribute(name = "UseSparseColumn")
    protected Boolean useSparseColumn;

    /**
     * Gets the value of the isMultiLanguage property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isIsMultiLanguage() {
        if (isMultiLanguage == null) {
            return false;
        } else {
            return isMultiLanguage;
        }
    }

    /**
     * Sets the value of the isMultiLanguage property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsMultiLanguage(Boolean value) {
        this.isMultiLanguage = value;
    }

    /**
     * Gets the value of the useNVARCHAR property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isUseNVARCHAR() {
        if (useNVARCHAR == null) {
            return false;
        } else {
            return useNVARCHAR;
        }
    }

    /**
     * Sets the value of the useNVARCHAR property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseNVARCHAR(Boolean value) {
        this.useNVARCHAR = value;
    }

    /**
     * Gets the value of the isCaseSensitive property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isIsCaseSensitive() {
        if (isCaseSensitive == null) {
            return false;
        } else {
            return isCaseSensitive;
        }
    }

    /**
     * Sets the value of the isCaseSensitive property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsCaseSensitive(Boolean value) {
        this.isCaseSensitive = value;
    }

    /**
     * Gets the value of the unicode property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isUnicode() {
        if (unicode == null) {
            return false;
        } else {
            return unicode;
        }
    }

    /**
     * Sets the value of the unicode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUnicode(Boolean value) {
        this.unicode = value;
    }

    /**
     * Gets the value of the useSparseColumn property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isUseSparseColumn() {
        if (useSparseColumn == null) {
            return false;
        } else {
            return useSparseColumn;
        }
    }

    /**
     * Sets the value of the useSparseColumn property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseSparseColumn(Boolean value) {
        this.useSparseColumn = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MultiLanguageBaseAttributeType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MultiLanguageBaseAttributeType that = ((MultiLanguageBaseAttributeType) object);
        {
            boolean lhsIsMultiLanguage;
            lhsIsMultiLanguage = this.isIsMultiLanguage();
            boolean rhsIsMultiLanguage;
            rhsIsMultiLanguage = that.isIsMultiLanguage();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "isMultiLanguage", lhsIsMultiLanguage), LocatorUtils.property(thatLocator, "isMultiLanguage", rhsIsMultiLanguage), lhsIsMultiLanguage, rhsIsMultiLanguage)) {
                return false;
            }
        }
        {
            boolean lhsUseNVARCHAR;
            lhsUseNVARCHAR = this.isUseNVARCHAR();
            boolean rhsUseNVARCHAR;
            rhsUseNVARCHAR = that.isUseNVARCHAR();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "useNVARCHAR", lhsUseNVARCHAR), LocatorUtils.property(thatLocator, "useNVARCHAR", rhsUseNVARCHAR), lhsUseNVARCHAR, rhsUseNVARCHAR)) {
                return false;
            }
        }
        {
            boolean lhsIsCaseSensitive;
            lhsIsCaseSensitive = this.isIsCaseSensitive();
            boolean rhsIsCaseSensitive;
            rhsIsCaseSensitive = that.isIsCaseSensitive();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "isCaseSensitive", lhsIsCaseSensitive), LocatorUtils.property(thatLocator, "isCaseSensitive", rhsIsCaseSensitive), lhsIsCaseSensitive, rhsIsCaseSensitive)) {
                return false;
            }
        }
        {
            boolean lhsUnicode;
            lhsUnicode = this.isUnicode();
            boolean rhsUnicode;
            rhsUnicode = that.isUnicode();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "unicode", lhsUnicode), LocatorUtils.property(thatLocator, "unicode", rhsUnicode), lhsUnicode, rhsUnicode)) {
                return false;
            }
        }
        {
            boolean lhsUseSparseColumn;
            lhsUseSparseColumn = this.isUseSparseColumn();
            boolean rhsUseSparseColumn;
            rhsUseSparseColumn = that.isUseSparseColumn();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "useSparseColumn", lhsUseSparseColumn), LocatorUtils.property(thatLocator, "useSparseColumn", rhsUseSparseColumn), lhsUseSparseColumn, rhsUseSparseColumn)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MultiLanguageBaseAttributeType) {
            final MultiLanguageBaseAttributeType copy = ((MultiLanguageBaseAttributeType) draftCopy);
            if (this.isMultiLanguage!= null) {
                boolean sourceIsMultiLanguage;
                sourceIsMultiLanguage = this.isIsMultiLanguage();
                boolean copyIsMultiLanguage = strategy.copy(LocatorUtils.property(locator, "isMultiLanguage", sourceIsMultiLanguage), sourceIsMultiLanguage);
                copy.setIsMultiLanguage(copyIsMultiLanguage);
            } else {
                copy.isMultiLanguage = null;
            }
            if (this.useNVARCHAR!= null) {
                boolean sourceUseNVARCHAR;
                sourceUseNVARCHAR = this.isUseNVARCHAR();
                boolean copyUseNVARCHAR = strategy.copy(LocatorUtils.property(locator, "useNVARCHAR", sourceUseNVARCHAR), sourceUseNVARCHAR);
                copy.setUseNVARCHAR(copyUseNVARCHAR);
            } else {
                copy.useNVARCHAR = null;
            }
            if (this.isCaseSensitive!= null) {
                boolean sourceIsCaseSensitive;
                sourceIsCaseSensitive = this.isIsCaseSensitive();
                boolean copyIsCaseSensitive = strategy.copy(LocatorUtils.property(locator, "isCaseSensitive", sourceIsCaseSensitive), sourceIsCaseSensitive);
                copy.setIsCaseSensitive(copyIsCaseSensitive);
            } else {
                copy.isCaseSensitive = null;
            }
            if (this.unicode!= null) {
                boolean sourceUnicode;
                sourceUnicode = this.isUnicode();
                boolean copyUnicode = strategy.copy(LocatorUtils.property(locator, "unicode", sourceUnicode), sourceUnicode);
                copy.setUnicode(copyUnicode);
            } else {
                copy.unicode = null;
            }
            if (this.useSparseColumn!= null) {
                boolean sourceUseSparseColumn;
                sourceUseSparseColumn = this.isUseSparseColumn();
                boolean copyUseSparseColumn = strategy.copy(LocatorUtils.property(locator, "useSparseColumn", sourceUseSparseColumn), sourceUseSparseColumn);
                copy.setUseSparseColumn(copyUseSparseColumn);
            } else {
                copy.useSparseColumn = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MultiLanguageBaseAttributeType();
    }
    
//--simple--preserve
    
   
//--simple--preserve

}
