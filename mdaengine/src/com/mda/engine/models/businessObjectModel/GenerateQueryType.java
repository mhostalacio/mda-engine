
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for GenerateQueryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GenerateQueryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="GenerateService" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="UseNoLockHint" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenerateQueryType")
public class GenerateQueryType
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "GenerateService", required = true)
    protected boolean generateService;
    @XmlAttribute(name = "UseNoLockHint")
    protected Boolean useNoLockHint;

    /**
     * Gets the value of the generateService property.
     * 
     */
    public boolean isGenerateService() {
        return generateService;
    }

    /**
     * Sets the value of the generateService property.
     * 
     */
    public void setGenerateService(boolean value) {
        this.generateService = value;
    }

    /**
     * Gets the value of the useNoLockHint property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isUseNoLockHint() {
        if (useNoLockHint == null) {
            return false;
        } else {
            return useNoLockHint;
        }
    }

    /**
     * Sets the value of the useNoLockHint property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseNoLockHint(Boolean value) {
        this.useNoLockHint = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof GenerateQueryType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final GenerateQueryType that = ((GenerateQueryType) object);
        {
            boolean lhsGenerateService;
            lhsGenerateService = this.isGenerateService();
            boolean rhsGenerateService;
            rhsGenerateService = that.isGenerateService();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "generateService", lhsGenerateService), LocatorUtils.property(thatLocator, "generateService", rhsGenerateService), lhsGenerateService, rhsGenerateService)) {
                return false;
            }
        }
        {
            boolean lhsUseNoLockHint;
            lhsUseNoLockHint = this.isUseNoLockHint();
            boolean rhsUseNoLockHint;
            rhsUseNoLockHint = that.isUseNoLockHint();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "useNoLockHint", lhsUseNoLockHint), LocatorUtils.property(thatLocator, "useNoLockHint", rhsUseNoLockHint), lhsUseNoLockHint, rhsUseNoLockHint)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof GenerateQueryType) {
            final GenerateQueryType copy = ((GenerateQueryType) draftCopy);
            boolean sourceGenerateService;
            sourceGenerateService = this.isGenerateService();
            boolean copyGenerateService = strategy.copy(LocatorUtils.property(locator, "generateService", sourceGenerateService), sourceGenerateService);
            copy.setGenerateService(copyGenerateService);
            if (this.useNoLockHint!= null) {
                boolean sourceUseNoLockHint;
                sourceUseNoLockHint = this.isUseNoLockHint();
                boolean copyUseNoLockHint = strategy.copy(LocatorUtils.property(locator, "useNoLockHint", sourceUseNoLockHint), sourceUseNoLockHint);
                copy.setUseNoLockHint(copyUseNoLockHint);
            } else {
                copy.useNoLockHint = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new GenerateQueryType();
    }
    
//--simple--preserve
    
    private transient com.mda.engine.models.queryModel.Query targetQuery;

    public com.mda.engine.models.queryModel.Query getTargetQuery() {

		return this.targetQuery;
	}
    
    public void setTargetQuery(com.mda.engine.models.queryModel.Query targetQuery)
    {
    	this.targetQuery = targetQuery;
    }
    
//--simple--preserve

}
