
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for IntervalRestrictionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IntervalRestrictionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded" minOccurs="0">
 *         &lt;element name="MinValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MaxValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/choice>
 *       &lt;attribute name="ExcludeMinValue" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="ExcludeMaxValue" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IntervalRestrictionType", propOrder = {
    "minValueOrMaxValue"
})
public class IntervalRestrictionType
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElementRefs({
        @XmlElementRef(name = "MaxValue", namespace = "http://www.mdaengine.com/mdaengine/models/businessObjectModel", type = JAXBElement.class),
        @XmlElementRef(name = "MinValue", namespace = "http://www.mdaengine.com/mdaengine/models/businessObjectModel", type = JAXBElement.class)
    })
    protected List<JAXBElement<String>> minValueOrMaxValue;
    @XmlAttribute(name = "ExcludeMinValue")
    protected Boolean excludeMinValue;
    @XmlAttribute(name = "ExcludeMaxValue")
    protected Boolean excludeMaxValue;

    /**
     * Gets the value of the minValueOrMaxValue property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the minValueOrMaxValue property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMinValueOrMaxValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * 
     */
    public List<JAXBElement<String>> getMinValueOrMaxValue() {
        if (minValueOrMaxValue == null) {
            minValueOrMaxValue = new ArrayList<JAXBElement<String>>();
        }
        return this.minValueOrMaxValue;
    }

    /**
     * Gets the value of the excludeMinValue property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExcludeMinValue() {
        return excludeMinValue;
    }

    /**
     * Sets the value of the excludeMinValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExcludeMinValue(Boolean value) {
        this.excludeMinValue = value;
    }

    /**
     * Gets the value of the excludeMaxValue property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExcludeMaxValue() {
        return excludeMaxValue;
    }

    /**
     * Sets the value of the excludeMaxValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExcludeMaxValue(Boolean value) {
        this.excludeMaxValue = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof IntervalRestrictionType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final IntervalRestrictionType that = ((IntervalRestrictionType) object);
        {
            List<JAXBElement<String>> lhsMinValueOrMaxValue;
            lhsMinValueOrMaxValue = this.getMinValueOrMaxValue();
            List<JAXBElement<String>> rhsMinValueOrMaxValue;
            rhsMinValueOrMaxValue = that.getMinValueOrMaxValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "minValueOrMaxValue", lhsMinValueOrMaxValue), LocatorUtils.property(thatLocator, "minValueOrMaxValue", rhsMinValueOrMaxValue), lhsMinValueOrMaxValue, rhsMinValueOrMaxValue)) {
                return false;
            }
        }
        {
            Boolean lhsExcludeMinValue;
            lhsExcludeMinValue = this.isExcludeMinValue();
            Boolean rhsExcludeMinValue;
            rhsExcludeMinValue = that.isExcludeMinValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "excludeMinValue", lhsExcludeMinValue), LocatorUtils.property(thatLocator, "excludeMinValue", rhsExcludeMinValue), lhsExcludeMinValue, rhsExcludeMinValue)) {
                return false;
            }
        }
        {
            Boolean lhsExcludeMaxValue;
            lhsExcludeMaxValue = this.isExcludeMaxValue();
            Boolean rhsExcludeMaxValue;
            rhsExcludeMaxValue = that.isExcludeMaxValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "excludeMaxValue", lhsExcludeMaxValue), LocatorUtils.property(thatLocator, "excludeMaxValue", rhsExcludeMaxValue), lhsExcludeMaxValue, rhsExcludeMaxValue)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof IntervalRestrictionType) {
            final IntervalRestrictionType copy = ((IntervalRestrictionType) draftCopy);
            if ((this.minValueOrMaxValue!= null)&&(!this.minValueOrMaxValue.isEmpty())) {
                List<JAXBElement<String>> sourceMinValueOrMaxValue;
                sourceMinValueOrMaxValue = this.getMinValueOrMaxValue();
                @SuppressWarnings("unchecked")
                List<JAXBElement<String>> copyMinValueOrMaxValue = ((List<JAXBElement<String>> ) strategy.copy(LocatorUtils.property(locator, "minValueOrMaxValue", sourceMinValueOrMaxValue), sourceMinValueOrMaxValue));
                copy.minValueOrMaxValue = null;
                List<JAXBElement<String>> uniqueMinValueOrMaxValuel = copy.getMinValueOrMaxValue();
                uniqueMinValueOrMaxValuel.addAll(copyMinValueOrMaxValue);
            } else {
                copy.minValueOrMaxValue = null;
            }
            if (this.excludeMinValue!= null) {
                Boolean sourceExcludeMinValue;
                sourceExcludeMinValue = this.isExcludeMinValue();
                Boolean copyExcludeMinValue = ((Boolean) strategy.copy(LocatorUtils.property(locator, "excludeMinValue", sourceExcludeMinValue), sourceExcludeMinValue));
                copy.setExcludeMinValue(copyExcludeMinValue);
            } else {
                copy.excludeMinValue = null;
            }
            if (this.excludeMaxValue!= null) {
                Boolean sourceExcludeMaxValue;
                sourceExcludeMaxValue = this.isExcludeMaxValue();
                Boolean copyExcludeMaxValue = ((Boolean) strategy.copy(LocatorUtils.property(locator, "excludeMaxValue", sourceExcludeMaxValue), sourceExcludeMaxValue));
                copy.setExcludeMaxValue(copyExcludeMaxValue);
            } else {
                copy.excludeMaxValue = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new IntervalRestrictionType();
    }

}
