
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for BaseAttributeWithTargetType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BaseAttributeWithTargetType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}BaseAttributeType">
 *       &lt;attribute name="TargetObjectName" use="required" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}RegularString" />
 *       &lt;attribute name="Multiplicity" use="required" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}MultiplicityType" />
 *       &lt;attribute name="Paged" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BaseAttributeWithTargetType")
@XmlSeeAlso({
    AggregationAttributeType.class,
    CompositionAttributeType.class,
    AssociationAttributeType.class
})
public class BaseAttributeWithTargetType
    extends BaseAttributeType
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "TargetObjectName", required = true)
    protected String targetObjectName;
    @XmlAttribute(name = "Multiplicity", required = true)
    protected String multiplicity;
    @XmlAttribute(name = "Paged")
    protected Boolean paged;

    /**
     * Gets the value of the targetObjectName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetObjectName() {
        return targetObjectName;
    }

    /**
     * Sets the value of the targetObjectName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetObjectName(String value) {
        this.targetObjectName = value;
    }

    /**
     * Gets the value of the multiplicity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMultiplicity() {
        return multiplicity;
    }

    /**
     * Sets the value of the multiplicity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMultiplicity(String value) {
        this.multiplicity = value;
    }

    /**
     * Gets the value of the paged property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isPaged() {
        if (paged == null) {
            return false;
        } else {
            return paged;
        }
    }

    /**
     * Sets the value of the paged property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPaged(Boolean value) {
        this.paged = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof BaseAttributeWithTargetType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final BaseAttributeWithTargetType that = ((BaseAttributeWithTargetType) object);
        {
            String lhsTargetObjectName;
            lhsTargetObjectName = this.getTargetObjectName();
            String rhsTargetObjectName;
            rhsTargetObjectName = that.getTargetObjectName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "targetObjectName", lhsTargetObjectName), LocatorUtils.property(thatLocator, "targetObjectName", rhsTargetObjectName), lhsTargetObjectName, rhsTargetObjectName)) {
                return false;
            }
        }
        {
            String lhsMultiplicity;
            lhsMultiplicity = this.getMultiplicity();
            String rhsMultiplicity;
            rhsMultiplicity = that.getMultiplicity();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "multiplicity", lhsMultiplicity), LocatorUtils.property(thatLocator, "multiplicity", rhsMultiplicity), lhsMultiplicity, rhsMultiplicity)) {
                return false;
            }
        }
        {
            boolean lhsPaged;
            lhsPaged = this.isPaged();
            boolean rhsPaged;
            rhsPaged = that.isPaged();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "paged", lhsPaged), LocatorUtils.property(thatLocator, "paged", rhsPaged), lhsPaged, rhsPaged)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof BaseAttributeWithTargetType) {
            final BaseAttributeWithTargetType copy = ((BaseAttributeWithTargetType) draftCopy);
            if (this.targetObjectName!= null) {
                String sourceTargetObjectName;
                sourceTargetObjectName = this.getTargetObjectName();
                String copyTargetObjectName = ((String) strategy.copy(LocatorUtils.property(locator, "targetObjectName", sourceTargetObjectName), sourceTargetObjectName));
                copy.setTargetObjectName(copyTargetObjectName);
            } else {
                copy.targetObjectName = null;
            }
            if (this.multiplicity!= null) {
                String sourceMultiplicity;
                sourceMultiplicity = this.getMultiplicity();
                String copyMultiplicity = ((String) strategy.copy(LocatorUtils.property(locator, "multiplicity", sourceMultiplicity), sourceMultiplicity));
                copy.setMultiplicity(copyMultiplicity);
            } else {
                copy.multiplicity = null;
            }
            if (this.paged!= null) {
                boolean sourcePaged;
                sourcePaged = this.isPaged();
                boolean copyPaged = strategy.copy(LocatorUtils.property(locator, "paged", sourcePaged), sourcePaged);
                copy.setPaged(copyPaged);
            } else {
                copy.paged = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new BaseAttributeWithTargetType();
    }
    
//--simple--preserve
    
    
    private transient ObjectType targetObject;
    private transient com.mda.engine.models.queryModel.Query populatedByQuery;
    
    public void setTargetObject(ObjectType targetObject) {
		this.targetObject = targetObject;
	}

	public ObjectType getTargetObject() {
		return targetObject;
	}
    
    public com.mda.engine.models.queryModel.Query getPopulatedByQuery() {

		return this.populatedByQuery;
	}
    
    public void setPopulatedByQuery(com.mda.engine.models.queryModel.Query query) {

    	this.populatedByQuery = query;
	}

    public int getMultiplicityNumber()
    {
    	if (multiplicity.equals("1"))
    		return 1;
    	else if (multiplicity.equals("0..1"))
    		return 0;
    	else if (multiplicity.equals("0..*"))
    		return 2;
    	else if (multiplicity.equals("1..*"))
    		return 2;
    	else
    		return 0;
    }
    
    public String getIdSuffix()
    {
    	if (this.getTargetObject() == null)
    		return "";
    	if (multiplicity.equals("1"))
    		return "Id";
    	else if (multiplicity.equals("0..1"))
    		return "Id";
    	else if (multiplicity.equals("0..*"))
    		return "Ids";
    	else if (multiplicity.equals("1..*"))
    		return "Ids";
    	else
    		return "";
    }
    
    private AggregationAttributeType targetObjectParentAggregation;

	public AggregationAttributeType getTargetObjectParentAggregation() {
		return targetObjectParentAggregation;
	}

	public void setTargetObjectParentAggregation(AggregationAttributeType targetObjectParentAggregation) {
		this.targetObjectParentAggregation = targetObjectParentAggregation;
	}
	
	public Boolean getIsQueryParameter() {
		return isQueryParameter;
	}

	public void setIsQueryParameter(Boolean isQueryParameter) {
		this.isQueryParameter = isQueryParameter;
	}

	private transient Boolean isQueryParameter;
//--simple--preserve

}
