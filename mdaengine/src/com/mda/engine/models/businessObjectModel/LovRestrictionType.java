
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for LovRestrictionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LovRestrictionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Lov" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}LovType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LovRestrictionType", propOrder = {
    "lov"
})
public class LovRestrictionType
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Lov", required = true)
    protected LovType lov;

    /**
     * Gets the value of the lov property.
     * 
     * @return
     *     possible object is
     *     {@link LovType }
     *     
     */
    public LovType getLov() {
        return lov;
    }

    /**
     * Sets the value of the lov property.
     * 
     * @param value
     *     allowed object is
     *     {@link LovType }
     *     
     */
    public void setLov(LovType value) {
        this.lov = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof LovRestrictionType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final LovRestrictionType that = ((LovRestrictionType) object);
        {
            LovType lhsLov;
            lhsLov = this.getLov();
            LovType rhsLov;
            rhsLov = that.getLov();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "lov", lhsLov), LocatorUtils.property(thatLocator, "lov", rhsLov), lhsLov, rhsLov)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof LovRestrictionType) {
            final LovRestrictionType copy = ((LovRestrictionType) draftCopy);
            if (this.lov!= null) {
                LovType sourceLov;
                sourceLov = this.getLov();
                LovType copyLov = ((LovType) strategy.copy(LocatorUtils.property(locator, "lov", sourceLov), sourceLov));
                copy.setLov(copyLov);
            } else {
                copy.lov = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new LovRestrictionType();
    }

}
