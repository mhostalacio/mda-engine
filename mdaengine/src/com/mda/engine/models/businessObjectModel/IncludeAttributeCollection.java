
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for IncludeAttributeCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IncludeAttributeCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="IncludeAttribute" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}IncludeAttribute" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IncludeAttributeCollection", propOrder = {
    "includeAttribute"
})
public class IncludeAttributeCollection
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "IncludeAttribute")
    protected List<IncludeAttribute> includeAttribute;

    /**
     * Gets the value of the includeAttribute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the includeAttribute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIncludeAttribute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IncludeAttribute }
     * 
     * 
     */
    public List<IncludeAttribute> getIncludeAttribute() {
        if (includeAttribute == null) {
            includeAttribute = new ArrayList<IncludeAttribute>();
        }
        return this.includeAttribute;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof IncludeAttributeCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final IncludeAttributeCollection that = ((IncludeAttributeCollection) object);
        {
            List<IncludeAttribute> lhsIncludeAttribute;
            lhsIncludeAttribute = this.getIncludeAttribute();
            List<IncludeAttribute> rhsIncludeAttribute;
            rhsIncludeAttribute = that.getIncludeAttribute();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "includeAttribute", lhsIncludeAttribute), LocatorUtils.property(thatLocator, "includeAttribute", rhsIncludeAttribute), lhsIncludeAttribute, rhsIncludeAttribute)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof IncludeAttributeCollection) {
            final IncludeAttributeCollection copy = ((IncludeAttributeCollection) draftCopy);
            if ((this.includeAttribute!= null)&&(!this.includeAttribute.isEmpty())) {
                List<IncludeAttribute> sourceIncludeAttribute;
                sourceIncludeAttribute = this.getIncludeAttribute();
                @SuppressWarnings("unchecked")
                List<IncludeAttribute> copyIncludeAttribute = ((List<IncludeAttribute> ) strategy.copy(LocatorUtils.property(locator, "includeAttribute", sourceIncludeAttribute), sourceIncludeAttribute));
                copy.includeAttribute = null;
                List<IncludeAttribute> uniqueIncludeAttributel = copy.getIncludeAttribute();
                uniqueIncludeAttributel.addAll(copyIncludeAttribute);
            } else {
                copy.includeAttribute = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new IncludeAttributeCollection();
    }

}
