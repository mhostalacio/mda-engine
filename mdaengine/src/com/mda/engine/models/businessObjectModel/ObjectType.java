
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ObjectType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ObjectType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}BaseType">
 *       &lt;sequence>
 *         &lt;element name="Header" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}HeaderType"/>
 *         &lt;element name="Queries" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}QueriesType"/>
 *         &lt;element name="Attributes" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}AttributeCollectionType"/>
 *         &lt;element name="Rules" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}RuleCollectionType" minOccurs="0"/>
 *         &lt;element name="Lovs" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}LovCollectionType" minOccurs="0"/>
 *         &lt;element name="AttributesGroups" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}AttributesGroupCollection" minOccurs="0"/>
 *         &lt;element name="Indexes" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}IndexCollection" minOccurs="0"/>
 *         &lt;element name="PrimaryKey" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}Index" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="ShouldConvertToMvcModel" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectType", propOrder = {
    "header",
    "queries",
    "attributes",
    "rules",
    "lovs",
    "attributesGroups",
    "indexes",
    "primaryKey"
})
@XmlSeeAlso({
    InterfaceType.class
})
@XmlRootElement(name = "Object")
public class ObjectType
    extends BaseType
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Header", required = true)
    protected HeaderType header;
    @XmlElement(name = "Queries", required = true)
    protected QueriesType queries;
    @XmlElement(name = "Attributes", required = true)
    protected AttributeCollectionType attributes;
    @XmlElement(name = "Rules")
    protected RuleCollectionType rules;
    @XmlElement(name = "Lovs")
    protected LovCollectionType lovs;
    @XmlElement(name = "AttributesGroups")
    protected AttributesGroupCollection attributesGroups;
    @XmlElement(name = "Indexes")
    protected IndexCollection indexes;
    @XmlElement(name = "PrimaryKey")
    protected Index primaryKey;
    @XmlAttribute(name = "ShouldConvertToMvcModel")
    protected Boolean shouldConvertToMvcModel;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link HeaderType }
     *     
     */
    public HeaderType getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link HeaderType }
     *     
     */
    public void setHeader(HeaderType value) {
        this.header = value;
    }

    /**
     * Gets the value of the queries property.
     * 
     * @return
     *     possible object is
     *     {@link QueriesType }
     *     
     */
    public QueriesType getQueries() {
        return queries;
    }

    /**
     * Sets the value of the queries property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueriesType }
     *     
     */
    public void setQueries(QueriesType value) {
        this.queries = value;
    }

    /**
     * Gets the value of the attributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeCollectionType }
     *     
     */
    public AttributeCollectionType getAttributes() {
        return attributes;
    }

    /**
     * Sets the value of the attributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeCollectionType }
     *     
     */
    public void setAttributes(AttributeCollectionType value) {
        this.attributes = value;
    }

    /**
     * Gets the value of the rules property.
     * 
     * @return
     *     possible object is
     *     {@link RuleCollectionType }
     *     
     */
    public RuleCollectionType getRules() {
        return rules;
    }

    /**
     * Sets the value of the rules property.
     * 
     * @param value
     *     allowed object is
     *     {@link RuleCollectionType }
     *     
     */
    public void setRules(RuleCollectionType value) {
        this.rules = value;
    }

    /**
     * Gets the value of the lovs property.
     * 
     * @return
     *     possible object is
     *     {@link LovCollectionType }
     *     
     */
    public LovCollectionType getLovs() {
        return lovs;
    }

    /**
     * Sets the value of the lovs property.
     * 
     * @param value
     *     allowed object is
     *     {@link LovCollectionType }
     *     
     */
    public void setLovs(LovCollectionType value) {
        this.lovs = value;
    }

    /**
     * Gets the value of the attributesGroups property.
     * 
     * @return
     *     possible object is
     *     {@link AttributesGroupCollection }
     *     
     */
    public AttributesGroupCollection getAttributesGroups() {
        return attributesGroups;
    }

    /**
     * Sets the value of the attributesGroups property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributesGroupCollection }
     *     
     */
    public void setAttributesGroups(AttributesGroupCollection value) {
        this.attributesGroups = value;
    }

    /**
     * Gets the value of the indexes property.
     * 
     * @return
     *     possible object is
     *     {@link IndexCollection }
     *     
     */
    public IndexCollection getIndexes() {
        return indexes;
    }

    /**
     * Sets the value of the indexes property.
     * 
     * @param value
     *     allowed object is
     *     {@link IndexCollection }
     *     
     */
    public void setIndexes(IndexCollection value) {
        this.indexes = value;
    }

    /**
     * Gets the value of the primaryKey property.
     * 
     * @return
     *     possible object is
     *     {@link Index }
     *     
     */
    public Index getPrimaryKey() {
        return primaryKey;
    }

    /**
     * Sets the value of the primaryKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link Index }
     *     
     */
    public void setPrimaryKey(Index value) {
        this.primaryKey = value;
    }

    /**
     * Gets the value of the shouldConvertToMvcModel property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isShouldConvertToMvcModel() {
        if (shouldConvertToMvcModel == null) {
            return true;
        } else {
            return shouldConvertToMvcModel;
        }
    }

    /**
     * Sets the value of the shouldConvertToMvcModel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShouldConvertToMvcModel(Boolean value) {
        this.shouldConvertToMvcModel = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ObjectType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final ObjectType that = ((ObjectType) object);
        {
            HeaderType lhsHeader;
            lhsHeader = this.getHeader();
            HeaderType rhsHeader;
            rhsHeader = that.getHeader();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "header", lhsHeader), LocatorUtils.property(thatLocator, "header", rhsHeader), lhsHeader, rhsHeader)) {
                return false;
            }
        }
        {
            QueriesType lhsQueries;
            lhsQueries = this.getQueries();
            QueriesType rhsQueries;
            rhsQueries = that.getQueries();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "queries", lhsQueries), LocatorUtils.property(thatLocator, "queries", rhsQueries), lhsQueries, rhsQueries)) {
                return false;
            }
        }
        {
            AttributeCollectionType lhsAttributes;
            lhsAttributes = this.getAttributes();
            AttributeCollectionType rhsAttributes;
            rhsAttributes = that.getAttributes();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "attributes", lhsAttributes), LocatorUtils.property(thatLocator, "attributes", rhsAttributes), lhsAttributes, rhsAttributes)) {
                return false;
            }
        }
        {
            RuleCollectionType lhsRules;
            lhsRules = this.getRules();
            RuleCollectionType rhsRules;
            rhsRules = that.getRules();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "rules", lhsRules), LocatorUtils.property(thatLocator, "rules", rhsRules), lhsRules, rhsRules)) {
                return false;
            }
        }
        {
            LovCollectionType lhsLovs;
            lhsLovs = this.getLovs();
            LovCollectionType rhsLovs;
            rhsLovs = that.getLovs();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "lovs", lhsLovs), LocatorUtils.property(thatLocator, "lovs", rhsLovs), lhsLovs, rhsLovs)) {
                return false;
            }
        }
        {
            AttributesGroupCollection lhsAttributesGroups;
            lhsAttributesGroups = this.getAttributesGroups();
            AttributesGroupCollection rhsAttributesGroups;
            rhsAttributesGroups = that.getAttributesGroups();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "attributesGroups", lhsAttributesGroups), LocatorUtils.property(thatLocator, "attributesGroups", rhsAttributesGroups), lhsAttributesGroups, rhsAttributesGroups)) {
                return false;
            }
        }
        {
            IndexCollection lhsIndexes;
            lhsIndexes = this.getIndexes();
            IndexCollection rhsIndexes;
            rhsIndexes = that.getIndexes();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "indexes", lhsIndexes), LocatorUtils.property(thatLocator, "indexes", rhsIndexes), lhsIndexes, rhsIndexes)) {
                return false;
            }
        }
        {
            Index lhsPrimaryKey;
            lhsPrimaryKey = this.getPrimaryKey();
            Index rhsPrimaryKey;
            rhsPrimaryKey = that.getPrimaryKey();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "primaryKey", lhsPrimaryKey), LocatorUtils.property(thatLocator, "primaryKey", rhsPrimaryKey), lhsPrimaryKey, rhsPrimaryKey)) {
                return false;
            }
        }
        {
            boolean lhsShouldConvertToMvcModel;
            lhsShouldConvertToMvcModel = this.isShouldConvertToMvcModel();
            boolean rhsShouldConvertToMvcModel;
            rhsShouldConvertToMvcModel = that.isShouldConvertToMvcModel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "shouldConvertToMvcModel", lhsShouldConvertToMvcModel), LocatorUtils.property(thatLocator, "shouldConvertToMvcModel", rhsShouldConvertToMvcModel), lhsShouldConvertToMvcModel, rhsShouldConvertToMvcModel)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof ObjectType) {
            final ObjectType copy = ((ObjectType) draftCopy);
            if (this.header!= null) {
                HeaderType sourceHeader;
                sourceHeader = this.getHeader();
                HeaderType copyHeader = ((HeaderType) strategy.copy(LocatorUtils.property(locator, "header", sourceHeader), sourceHeader));
                copy.setHeader(copyHeader);
            } else {
                copy.header = null;
            }
            if (this.queries!= null) {
                QueriesType sourceQueries;
                sourceQueries = this.getQueries();
                QueriesType copyQueries = ((QueriesType) strategy.copy(LocatorUtils.property(locator, "queries", sourceQueries), sourceQueries));
                copy.setQueries(copyQueries);
            } else {
                copy.queries = null;
            }
            if (this.attributes!= null) {
                AttributeCollectionType sourceAttributes;
                sourceAttributes = this.getAttributes();
                AttributeCollectionType copyAttributes = ((AttributeCollectionType) strategy.copy(LocatorUtils.property(locator, "attributes", sourceAttributes), sourceAttributes));
                copy.setAttributes(copyAttributes);
            } else {
                copy.attributes = null;
            }
            if (this.rules!= null) {
                RuleCollectionType sourceRules;
                sourceRules = this.getRules();
                RuleCollectionType copyRules = ((RuleCollectionType) strategy.copy(LocatorUtils.property(locator, "rules", sourceRules), sourceRules));
                copy.setRules(copyRules);
            } else {
                copy.rules = null;
            }
            if (this.lovs!= null) {
                LovCollectionType sourceLovs;
                sourceLovs = this.getLovs();
                LovCollectionType copyLovs = ((LovCollectionType) strategy.copy(LocatorUtils.property(locator, "lovs", sourceLovs), sourceLovs));
                copy.setLovs(copyLovs);
            } else {
                copy.lovs = null;
            }
            if (this.attributesGroups!= null) {
                AttributesGroupCollection sourceAttributesGroups;
                sourceAttributesGroups = this.getAttributesGroups();
                AttributesGroupCollection copyAttributesGroups = ((AttributesGroupCollection) strategy.copy(LocatorUtils.property(locator, "attributesGroups", sourceAttributesGroups), sourceAttributesGroups));
                copy.setAttributesGroups(copyAttributesGroups);
            } else {
                copy.attributesGroups = null;
            }
            if (this.indexes!= null) {
                IndexCollection sourceIndexes;
                sourceIndexes = this.getIndexes();
                IndexCollection copyIndexes = ((IndexCollection) strategy.copy(LocatorUtils.property(locator, "indexes", sourceIndexes), sourceIndexes));
                copy.setIndexes(copyIndexes);
            } else {
                copy.indexes = null;
            }
            if (this.primaryKey!= null) {
                Index sourcePrimaryKey;
                sourcePrimaryKey = this.getPrimaryKey();
                Index copyPrimaryKey = ((Index) strategy.copy(LocatorUtils.property(locator, "primaryKey", sourcePrimaryKey), sourcePrimaryKey));
                copy.setPrimaryKey(copyPrimaryKey);
            } else {
                copy.primaryKey = null;
            }
            if (this.shouldConvertToMvcModel!= null) {
                boolean sourceShouldConvertToMvcModel;
                sourceShouldConvertToMvcModel = this.isShouldConvertToMvcModel();
                boolean copyShouldConvertToMvcModel = strategy.copy(LocatorUtils.property(locator, "shouldConvertToMvcModel", sourceShouldConvertToMvcModel), sourceShouldConvertToMvcModel);
                copy.setShouldConvertToMvcModel(copyShouldConvertToMvcModel);
            } else {
                copy.shouldConvertToMvcModel = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ObjectType();
    }
    
//--simple--preserve
    
    
    private transient int compositionToMeCounter;
    private transient Boolean isAutoCreated;
	private transient com.mda.engine.models.businessObjectModel.IndentityAttributeType identity;
    private transient String packageName;

    private transient java.util.ArrayList<String> implementedInterfaces = new java.util.ArrayList<String>();
    private transient com.mda.engine.models.configurationManagementModel.Application application;
    private transient java.util.ArrayList<com.mda.engine.models.queryModel.Query> associatedQueries = new java.util.ArrayList<com.mda.engine.models.queryModel.Query>();
    private transient java.util.ArrayList<com.mda.engine.models.businessTransactionModel.BusinessTransaction> associatedBTMs = new java.util.ArrayList<com.mda.engine.models.businessTransactionModel.BusinessTransaction>();
    private transient Boolean isQueryObject = false;
    
    public void associateQuery(com.mda.engine.models.queryModel.Query query) {
    	if (!associatedQueries.contains(query))
    		associatedQueries.add(query);
	}
    
    public void associateTransaction(com.mda.engine.models.businessTransactionModel.BusinessTransaction btm) {
    	if (!associatedBTMs.contains(btm))
    		associatedBTMs.add(btm);
	}
    
    public java.util.ArrayList<com.mda.engine.models.businessTransactionModel.BusinessTransaction> getAssociatedTransactions()
    {
    	return associatedBTMs;
    }
    
    public java.util.ArrayList<com.mda.engine.models.queryModel.Query> getAssociatedQueries()
    {
    	return associatedQueries;
    }
    
    public void associateLov(com.mda.engine.models.businessObjectModel.LovDefinitionType lov){
    	if(this.lovs == null)
    		this.lovs = new LovCollectionType();
    	
    	this.lovs.getLovDefinition().add(lov);
    }
    
    public com.mda.engine.models.queryModel.Query getQueryById()
	{
    	for(com.mda.engine.models.queryModel.Query query : associatedQueries)
		 {
			 if (query.getOwnedBy().getName().equals(this.getName()) && query.getName().equals(com.mda.engine.utils.QueryHelper.GET_BY_ID_QUERY_NAME))
			 	return query;
		 }
		return null;
	}
    
    public Boolean getIsAutoCreated() {
    	if (isAutoCreated == null)
    		return false;
		return isAutoCreated;
	}

	public void setIsAutoCreated(Boolean isAutoCreated) {
		this.isAutoCreated = isAutoCreated;
	}
	
    public int getCompositionToMeCounter() {

		return this.compositionToMeCounter;
	}
    
    public void incrementCompositionToMeCounter() {

		this.compositionToMeCounter ++;
	}

	public void setIdentity(com.mda.engine.models.businessObjectModel.IndentityAttributeType identity) {
		this.identity = identity;
	}

	public com.mda.engine.models.businessObjectModel.IndentityAttributeType getIdentity() {
		if (this.identity == null)
		{
			for (BaseAttributeType att : this.getAttributes().getAttributeList())
			{
				if (att instanceof IndentityAttributeType)
				{
					identity = (IndentityAttributeType)att;
					break;
				}
			}
		}
		return identity;
	}

	private BaseAttributeType getDummyAggregationListProperty(String name)
	{
		NumberAttributeType attId = new NumberAttributeType();
		attId.setList(true);
		attId.setName(name);
		attId.setDataType(AttributeNumberDataType.LONG);
		attId.setIsNullable(true);
		attId.setAttributeForTargetIdName("Id");
		attId.setList(true);
		return attId;
	}
	
    public com.mda.engine.models.businessObjectModel.BaseAttributeType getAttributeByName(String name)
    {
    	for (BaseAttributeType att : this.getAttributes().getAttributeList())
		{
    		if (att instanceof AggregationAttributeType) 
			{
				AggregationAttributeType agg = (AggregationAttributeType) att;
				if (agg.getMultiplicityNumber() > 1)
				{
					if ((att.getName() + agg.getIdSuffix()).equals(name))
						return getDummyAggregationListProperty(att.getName() + agg.getIdSuffix());
				}
			}
			if (att.getName().equals(name))
			{
				return att;
			}
		}
    	return null;
    }

	public void setPackageName(String packageName) {
		this.packageName = com.mda.engine.utils.StringUtils.getPascalCase(packageName);
	}

	public String getPackageName() {
		return packageName;
	}
	

	public String getClassName() {
		return com.mda.engine.utils.StringUtils.getPascalCase(name);
	}
	
	public String getFullClassName() {
		return application.getEntitiesProjectNamespace() + "." + packageName + "." + getClassName();
	}

	public String getDataContractClassName() {
		return com.mda.engine.utils.StringUtils.getPascalCase(name) + "DataContract";
	}
	
	public void addImplementedInterface(String interfacename) {
		if (!this.implementedInterfaces.contains(interfacename))
			this.implementedInterfaces.add(interfacename);
	}

	public String getImplementedInterfaceList() {
		if (this.implementedInterfaces.size() > 0)
			return "," + com.mda.engine.utils.StringUtils.toDelimitedString(this.implementedInterfaces, ",");
		return "";
	}
	
	public Boolean hasMultiLanguageAttribute()
	{
		for (BaseAttributeType att : this.getAttributes().getAttributeList())
		{
			if (att instanceof MultiLanguageBaseAttributeType)
			{
				if (((MultiLanguageBaseAttributeType)att).isIsMultiLanguage())
					return true;
			}
		}
		return false;
	}

	public void setApplication(com.mda.engine.models.configurationManagementModel.Application application) {
		this.application = application;
	}

	public com.mda.engine.models.configurationManagementModel.Application getApplication() {
		return application;
	}

	public void setIsQueryObject(Boolean isQueryObject) {
		this.isQueryObject = isQueryObject;
	}

	public Boolean getIsQueryObject() {
		return isQueryObject;
	}


	
    
//--simple--preserve

}
