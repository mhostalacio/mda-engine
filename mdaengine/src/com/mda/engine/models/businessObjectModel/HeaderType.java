
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * Header of BOM
 * 
 * <p>Java class for HeaderType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HeaderType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="IsAbstract" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Extends" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Label" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HeaderType", propOrder = {

})
public class HeaderType
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "IsAbstract")
    protected boolean isAbstract;
    @XmlElement(name = "Extends", required = true)
    protected String _extends;
    @XmlElement(name = "Label", required = true)
    protected String label;

    /**
     * Gets the value of the isAbstract property.
     * 
     */
    public boolean isIsAbstract() {
        return isAbstract;
    }

    /**
     * Sets the value of the isAbstract property.
     * 
     */
    public void setIsAbstract(boolean value) {
        this.isAbstract = value;
    }

    /**
     * Gets the value of the extends property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtends() {
        return _extends;
    }

    /**
     * Sets the value of the extends property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtends(String value) {
        this._extends = value;
    }

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof HeaderType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final HeaderType that = ((HeaderType) object);
        {
            boolean lhsIsAbstract;
            lhsIsAbstract = this.isIsAbstract();
            boolean rhsIsAbstract;
            rhsIsAbstract = that.isIsAbstract();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "isAbstract", lhsIsAbstract), LocatorUtils.property(thatLocator, "isAbstract", rhsIsAbstract), lhsIsAbstract, rhsIsAbstract)) {
                return false;
            }
        }
        {
            String lhsExtends;
            lhsExtends = this.getExtends();
            String rhsExtends;
            rhsExtends = that.getExtends();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "_extends", lhsExtends), LocatorUtils.property(thatLocator, "_extends", rhsExtends), lhsExtends, rhsExtends)) {
                return false;
            }
        }
        {
            String lhsLabel;
            lhsLabel = this.getLabel();
            String rhsLabel;
            rhsLabel = that.getLabel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "label", lhsLabel), LocatorUtils.property(thatLocator, "label", rhsLabel), lhsLabel, rhsLabel)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof HeaderType) {
            final HeaderType copy = ((HeaderType) draftCopy);
            boolean sourceIsAbstract;
            sourceIsAbstract = this.isIsAbstract();
            boolean copyIsAbstract = strategy.copy(LocatorUtils.property(locator, "isAbstract", sourceIsAbstract), sourceIsAbstract);
            copy.setIsAbstract(copyIsAbstract);
            if (this._extends!= null) {
                String sourceExtends;
                sourceExtends = this.getExtends();
                String copyExtends = ((String) strategy.copy(LocatorUtils.property(locator, "_extends", sourceExtends), sourceExtends));
                copy.setExtends(copyExtends);
            } else {
                copy._extends = null;
            }
            if (this.label!= null) {
                String sourceLabel;
                sourceLabel = this.getLabel();
                String copyLabel = ((String) strategy.copy(LocatorUtils.property(locator, "label", sourceLabel), sourceLabel));
                copy.setLabel(copyLabel);
            } else {
                copy.label = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new HeaderType();
    }

}
