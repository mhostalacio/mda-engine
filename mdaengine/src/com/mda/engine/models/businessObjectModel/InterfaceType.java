
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.models.common.BOMReferenceTypeCollection;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for InterfaceType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InterfaceType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}ObjectType">
 *       &lt;sequence>
 *         &lt;element name="ImplementedBy" type="{http://www.mdaengine.com/mdaengine/models/common}BOMReferenceTypeCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InterfaceType", propOrder = {
    "implementedBy"
})
@XmlRootElement(name = "Interface")
public class InterfaceType
    extends ObjectType
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "ImplementedBy")
    protected BOMReferenceTypeCollection implementedBy;

    /**
     * Gets the value of the implementedBy property.
     * 
     * @return
     *     possible object is
     *     {@link BOMReferenceTypeCollection }
     *     
     */
    public BOMReferenceTypeCollection getImplementedBy() {
        return implementedBy;
    }

    /**
     * Sets the value of the implementedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link BOMReferenceTypeCollection }
     *     
     */
    public void setImplementedBy(BOMReferenceTypeCollection value) {
        this.implementedBy = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof InterfaceType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final InterfaceType that = ((InterfaceType) object);
        {
            BOMReferenceTypeCollection lhsImplementedBy;
            lhsImplementedBy = this.getImplementedBy();
            BOMReferenceTypeCollection rhsImplementedBy;
            rhsImplementedBy = that.getImplementedBy();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "implementedBy", lhsImplementedBy), LocatorUtils.property(thatLocator, "implementedBy", rhsImplementedBy), lhsImplementedBy, rhsImplementedBy)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof InterfaceType) {
            final InterfaceType copy = ((InterfaceType) draftCopy);
            if (this.implementedBy!= null) {
                BOMReferenceTypeCollection sourceImplementedBy;
                sourceImplementedBy = this.getImplementedBy();
                BOMReferenceTypeCollection copyImplementedBy = ((BOMReferenceTypeCollection) strategy.copy(LocatorUtils.property(locator, "implementedBy", sourceImplementedBy), sourceImplementedBy));
                copy.setImplementedBy(copyImplementedBy);
            } else {
                copy.implementedBy = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new InterfaceType();
    }

}
