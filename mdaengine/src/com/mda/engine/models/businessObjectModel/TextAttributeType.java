
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.models.common.BOMAttributeReferenceType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for TextAttributeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TextAttributeType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}MultiLanguageBaseAttributeType">
 *       &lt;sequence>
 *         &lt;element name="MaxLength" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger"/>
 *         &lt;element name="UniqueIdentifierReference" type="{http://www.mdaengine.com/mdaengine/models/common}BOMAttributeReferenceType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TextAttributeType", propOrder = {
    "maxLength",
    "uniqueIdentifierReference"
})
public class TextAttributeType
    extends MultiLanguageBaseAttributeType
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "MaxLength", required = true)
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger maxLength;
    @XmlElementRef(name = "UniqueIdentifierReference", namespace = "http://www.mdaengine.com/mdaengine/models/businessObjectModel", type = JAXBElement.class)
    protected JAXBElement<BOMAttributeReferenceType> uniqueIdentifierReference;

    /**
     * Gets the value of the maxLength property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaxLength() {
        return maxLength;
    }

    /**
     * Sets the value of the maxLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaxLength(BigInteger value) {
        this.maxLength = value;
    }

    /**
     * Gets the value of the uniqueIdentifierReference property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BOMAttributeReferenceType }{@code >}
     *     
     */
    public JAXBElement<BOMAttributeReferenceType> getUniqueIdentifierReference() {
        return uniqueIdentifierReference;
    }

    /**
     * Sets the value of the uniqueIdentifierReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BOMAttributeReferenceType }{@code >}
     *     
     */
    public void setUniqueIdentifierReference(JAXBElement<BOMAttributeReferenceType> value) {
        this.uniqueIdentifierReference = ((JAXBElement<BOMAttributeReferenceType> ) value);
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof TextAttributeType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final TextAttributeType that = ((TextAttributeType) object);
        {
            BigInteger lhsMaxLength;
            lhsMaxLength = this.getMaxLength();
            BigInteger rhsMaxLength;
            rhsMaxLength = that.getMaxLength();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "maxLength", lhsMaxLength), LocatorUtils.property(thatLocator, "maxLength", rhsMaxLength), lhsMaxLength, rhsMaxLength)) {
                return false;
            }
        }
        {
            JAXBElement<BOMAttributeReferenceType> lhsUniqueIdentifierReference;
            lhsUniqueIdentifierReference = this.getUniqueIdentifierReference();
            JAXBElement<BOMAttributeReferenceType> rhsUniqueIdentifierReference;
            rhsUniqueIdentifierReference = that.getUniqueIdentifierReference();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "uniqueIdentifierReference", lhsUniqueIdentifierReference), LocatorUtils.property(thatLocator, "uniqueIdentifierReference", rhsUniqueIdentifierReference), lhsUniqueIdentifierReference, rhsUniqueIdentifierReference)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof TextAttributeType) {
            final TextAttributeType copy = ((TextAttributeType) draftCopy);
            if (this.maxLength!= null) {
                BigInteger sourceMaxLength;
                sourceMaxLength = this.getMaxLength();
                BigInteger copyMaxLength = ((BigInteger) strategy.copy(LocatorUtils.property(locator, "maxLength", sourceMaxLength), sourceMaxLength));
                copy.setMaxLength(copyMaxLength);
            } else {
                copy.maxLength = null;
            }
            if (this.uniqueIdentifierReference!= null) {
                JAXBElement<BOMAttributeReferenceType> sourceUniqueIdentifierReference;
                sourceUniqueIdentifierReference = this.getUniqueIdentifierReference();
                @SuppressWarnings("unchecked")
                JAXBElement<BOMAttributeReferenceType> copyUniqueIdentifierReference = ((JAXBElement<BOMAttributeReferenceType> ) strategy.copy(LocatorUtils.property(locator, "uniqueIdentifierReference", sourceUniqueIdentifierReference), sourceUniqueIdentifierReference));
                copy.setUniqueIdentifierReference(copyUniqueIdentifierReference);
            } else {
                copy.uniqueIdentifierReference = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new TextAttributeType();
    }
    
//--simple--preserve
    @Override
    public String getSingleClassName() {
		return "String";
	}
//--simple--preserve

}
