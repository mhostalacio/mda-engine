
package com.mda.engine.models.businessObjectModel;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import com.mda.engine.models.common.BOMAttributeReferenceType;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.mda.engine.models.businessObjectModel package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Interface_QNAME = new QName("http://www.mdaengine.com/mdaengine/models/businessObjectModel", "Interface");
    private final static QName _Object_QNAME = new QName("http://www.mdaengine.com/mdaengine/models/businessObjectModel", "Object");
    private final static QName _IntervalRestrictionTypeMinValue_QNAME = new QName("http://www.mdaengine.com/mdaengine/models/businessObjectModel", "MinValue");
    private final static QName _IntervalRestrictionTypeMaxValue_QNAME = new QName("http://www.mdaengine.com/mdaengine/models/businessObjectModel", "MaxValue");
    private final static QName _BinaryAttributeRawTypeMaxLength_QNAME = new QName("http://www.mdaengine.com/mdaengine/models/businessObjectModel", "MaxLength");
    private final static QName _TextAttributeTypeUniqueIdentifierReference_QNAME = new QName("http://www.mdaengine.com/mdaengine/models/businessObjectModel", "UniqueIdentifierReference");
    private final static QName _ActionRuleSetEnable_QNAME = new QName("http://www.mdaengine.com/mdaengine/models/businessObjectModel", "SetEnable");
    private final static QName _ActionRuleSetHidden_QNAME = new QName("http://www.mdaengine.com/mdaengine/models/businessObjectModel", "SetHidden");
    private final static QName _ActionRuleSetVisible_QNAME = new QName("http://www.mdaengine.com/mdaengine/models/businessObjectModel", "SetVisible");
    private final static QName _ActionRuleSetValue_QNAME = new QName("http://www.mdaengine.com/mdaengine/models/businessObjectModel", "SetValue");
    private final static QName _ActionRuleSetRestriction_QNAME = new QName("http://www.mdaengine.com/mdaengine/models/businessObjectModel", "SetRestriction");
    private final static QName _ActionRuleSetDisable_QNAME = new QName("http://www.mdaengine.com/mdaengine/models/businessObjectModel", "SetDisable");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.mda.engine.models.businessObjectModel
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TimeAttributeRawType }
     * 
     */
    public TimeAttributeRawType createTimeAttributeRawType() {
        return new TimeAttributeRawType();
    }

    /**
     * Create an instance of {@link LongtextAttributeType }
     * 
     */
    public LongtextAttributeType createLongtextAttributeType() {
        return new LongtextAttributeType();
    }

    /**
     * Create an instance of {@link IncludeAttributeCollection }
     * 
     */
    public IncludeAttributeCollection createIncludeAttributeCollection() {
        return new IncludeAttributeCollection();
    }

    /**
     * Create an instance of {@link BaseType }
     * 
     */
    public BaseType createBaseType() {
        return new BaseType();
    }

    /**
     * Create an instance of {@link XmlAttributeType }
     * 
     */
    public XmlAttributeType createXmlAttributeType() {
        return new XmlAttributeType();
    }

    /**
     * Create an instance of {@link ActionRule.SetRestriction }
     * 
     */
    public ActionRule.SetRestriction createActionRuleSetRestriction() {
        return new ActionRule.SetRestriction();
    }

    /**
     * Create an instance of {@link IndexAttributeCollection }
     * 
     */
    public IndexAttributeCollection createIndexAttributeCollection() {
        return new IndexAttributeCollection();
    }

    /**
     * Create an instance of {@link IndentityAttributeType }
     * 
     */
    public IndentityAttributeType createIndentityAttributeType() {
        return new IndentityAttributeType();
    }

    /**
     * Create an instance of {@link IndexCollection }
     * 
     */
    public IndexCollection createIndexCollection() {
        return new IndexCollection();
    }

    /**
     * Create an instance of {@link NumberAttributeRawType }
     * 
     */
    public NumberAttributeRawType createNumberAttributeRawType() {
        return new NumberAttributeRawType();
    }

    /**
     * Create an instance of {@link QueriesType }
     * 
     */
    public QueriesType createQueriesType() {
        return new QueriesType();
    }

    /**
     * Create an instance of {@link IntervalRestrictionType }
     * 
     */
    public IntervalRestrictionType createIntervalRestrictionType() {
        return new IntervalRestrictionType();
    }

    /**
     * Create an instance of {@link ObjectNameCollectionType }
     * 
     */
    public ObjectNameCollectionType createObjectNameCollectionType() {
        return new ObjectNameCollectionType();
    }

    /**
     * Create an instance of {@link CharAttributeType }
     * 
     */
    public CharAttributeType createCharAttributeType() {
        return new CharAttributeType();
    }

    /**
     * Create an instance of {@link EnumAttributeType }
     * 
     */
    public EnumAttributeType createEnumAttributeType() {
        return new EnumAttributeType();
    }

    /**
     * Create an instance of {@link BooleanAttributeRawType }
     * 
     */
    public BooleanAttributeRawType createBooleanAttributeRawType() {
        return new BooleanAttributeRawType();
    }

    /**
     * Create an instance of {@link AttributesGroupAttribute }
     * 
     */
    public AttributesGroupAttribute createAttributesGroupAttribute() {
        return new AttributesGroupAttribute();
    }

    /**
     * Create an instance of {@link DatetimeAttributeType }
     * 
     */
    public DatetimeAttributeType createDatetimeAttributeType() {
        return new DatetimeAttributeType();
    }

    /**
     * Create an instance of {@link SimplAttributeCollectionType }
     * 
     */
    public SimplAttributeCollectionType createSimplAttributeCollectionType() {
        return new SimplAttributeCollectionType();
    }

    /**
     * Create an instance of {@link RestrictionType }
     * 
     */
    public RestrictionType createRestrictionType() {
        return new RestrictionType();
    }

    /**
     * Create an instance of {@link MultiLanguageBaseAttributeType }
     * 
     */
    public MultiLanguageBaseAttributeType createMultiLanguageBaseAttributeType() {
        return new MultiLanguageBaseAttributeType();
    }

    /**
     * Create an instance of {@link InterfaceNameCollectionType }
     * 
     */
    public InterfaceNameCollectionType createInterfaceNameCollectionType() {
        return new InterfaceNameCollectionType();
    }

    /**
     * Create an instance of {@link RuleCollectionType }
     * 
     */
    public RuleCollectionType createRuleCollectionType() {
        return new RuleCollectionType();
    }

    /**
     * Create an instance of {@link TextAttributeType }
     * 
     */
    public TextAttributeType createTextAttributeType() {
        return new TextAttributeType();
    }

    /**
     * Create an instance of {@link AttributesGroupCollection }
     * 
     */
    public AttributesGroupCollection createAttributesGroupCollection() {
        return new AttributesGroupCollection();
    }

    /**
     * Create an instance of {@link TimedurationAttributeType }
     * 
     */
    public TimedurationAttributeType createTimedurationAttributeType() {
        return new TimedurationAttributeType();
    }

    /**
     * Create an instance of {@link DateAttributeRawType }
     * 
     */
    public DateAttributeRawType createDateAttributeRawType() {
        return new DateAttributeRawType();
    }

    /**
     * Create an instance of {@link RuleCollectionType.Rule }
     * 
     */
    public RuleCollectionType.Rule createRuleCollectionTypeRule() {
        return new RuleCollectionType.Rule();
    }

    /**
     * Create an instance of {@link GuidAttributeType }
     * 
     */
    public GuidAttributeType createGuidAttributeType() {
        return new GuidAttributeType();
    }

    /**
     * Create an instance of {@link LovItem }
     * 
     */
    public LovItem createLovItem() {
        return new LovItem();
    }

    /**
     * Create an instance of {@link UniqueRestrictionType }
     * 
     */
    public UniqueRestrictionType createUniqueRestrictionType() {
        return new UniqueRestrictionType();
    }

    /**
     * Create an instance of {@link IncludeAttribute }
     * 
     */
    public IncludeAttribute createIncludeAttribute() {
        return new IncludeAttribute();
    }

    /**
     * Create an instance of {@link TimestampAttributeType }
     * 
     */
    public TimestampAttributeType createTimestampAttributeType() {
        return new TimestampAttributeType();
    }

    /**
     * Create an instance of {@link LovType }
     * 
     */
    public LovType createLovType() {
        return new LovType();
    }

    /**
     * Create an instance of {@link IndexAttribute }
     * 
     */
    public IndexAttribute createIndexAttribute() {
        return new IndexAttribute();
    }

    /**
     * Create an instance of {@link EnumAttributeRawType }
     * 
     */
    public EnumAttributeRawType createEnumAttributeRawType() {
        return new EnumAttributeRawType();
    }

    /**
     * Create an instance of {@link TargetAttributeType }
     * 
     */
    public TargetAttributeType createTargetAttributeType() {
        return new TargetAttributeType();
    }

    /**
     * Create an instance of {@link BaseAttributeType }
     * 
     */
    public BaseAttributeType createBaseAttributeType() {
        return new BaseAttributeType();
    }

    /**
     * Create an instance of {@link BinaryAttributeType }
     * 
     */
    public BinaryAttributeType createBinaryAttributeType() {
        return new BinaryAttributeType();
    }

    /**
     * Create an instance of {@link TextAttributeRawType }
     * 
     */
    public TextAttributeRawType createTextAttributeRawType() {
        return new TextAttributeRawType();
    }

    /**
     * Create an instance of {@link DatetimeAttributeRawType }
     * 
     */
    public DatetimeAttributeRawType createDatetimeAttributeRawType() {
        return new DatetimeAttributeRawType();
    }

    /**
     * Create an instance of {@link Index }
     * 
     */
    public Index createIndex() {
        return new Index();
    }

    /**
     * Create an instance of {@link AttributesGroupAttributeCollection }
     * 
     */
    public AttributesGroupAttributeCollection createAttributesGroupAttributeCollection() {
        return new AttributesGroupAttributeCollection();
    }

    /**
     * Create an instance of {@link BooleanAttributeType }
     * 
     */
    public BooleanAttributeType createBooleanAttributeType() {
        return new BooleanAttributeType();
    }

    /**
     * Create an instance of {@link XmlAttributeRawType }
     * 
     */
    public XmlAttributeRawType createXmlAttributeRawType() {
        return new XmlAttributeRawType();
    }

    /**
     * Create an instance of {@link CompositionAttributeType }
     * 
     */
    public CompositionAttributeType createCompositionAttributeType() {
        return new CompositionAttributeType();
    }

    /**
     * Create an instance of {@link ActionRule }
     * 
     */
    public ActionRule createActionRule() {
        return new ActionRule();
    }

    /**
     * Create an instance of {@link CharAttributeRawType }
     * 
     */
    public CharAttributeRawType createCharAttributeRawType() {
        return new CharAttributeRawType();
    }

    /**
     * Create an instance of {@link BaseAttributeWithTargetType }
     * 
     */
    public BaseAttributeWithTargetType createBaseAttributeWithTargetType() {
        return new BaseAttributeWithTargetType();
    }

    /**
     * Create an instance of {@link LongtextAttributeRawType }
     * 
     */
    public LongtextAttributeRawType createLongtextAttributeRawType() {
        return new LongtextAttributeRawType();
    }

    /**
     * Create an instance of {@link RegexRestrictionType }
     * 
     */
    public RegexRestrictionType createRegexRestrictionType() {
        return new RegexRestrictionType();
    }

    /**
     * Create an instance of {@link HeaderType }
     * 
     */
    public HeaderType createHeaderType() {
        return new HeaderType();
    }

    /**
     * Create an instance of {@link LovDefinitionType }
     * 
     */
    public LovDefinitionType createLovDefinitionType() {
        return new LovDefinitionType();
    }

    /**
     * Create an instance of {@link AssociationAttributeType }
     * 
     */
    public AssociationAttributeType createAssociationAttributeType() {
        return new AssociationAttributeType();
    }

    /**
     * Create an instance of {@link NumberAttributeType }
     * 
     */
    public NumberAttributeType createNumberAttributeType() {
        return new NumberAttributeType();
    }

    /**
     * Create an instance of {@link TimeAttributeType }
     * 
     */
    public TimeAttributeType createTimeAttributeType() {
        return new TimeAttributeType();
    }

    /**
     * Create an instance of {@link GenerateQueryType }
     * 
     */
    public GenerateQueryType createGenerateQueryType() {
        return new GenerateQueryType();
    }

    /**
     * Create an instance of {@link AttributesGroup }
     * 
     */
    public AttributesGroup createAttributesGroup() {
        return new AttributesGroup();
    }

    /**
     * Create an instance of {@link TimestampAttributeRawType }
     * 
     */
    public TimestampAttributeRawType createTimestampAttributeRawType() {
        return new TimestampAttributeRawType();
    }

    /**
     * Create an instance of {@link LovItemCollection }
     * 
     */
    public LovItemCollection createLovItemCollection() {
        return new LovItemCollection();
    }

    /**
     * Create an instance of {@link AggregationAttributeType }
     * 
     */
    public AggregationAttributeType createAggregationAttributeType() {
        return new AggregationAttributeType();
    }

    /**
     * Create an instance of {@link AttributeNameType }
     * 
     */
    public AttributeNameType createAttributeNameType() {
        return new AttributeNameType();
    }

    /**
     * Create an instance of {@link AttributeCollectionType }
     * 
     */
    public AttributeCollectionType createAttributeCollectionType() {
        return new AttributeCollectionType();
    }

    /**
     * Create an instance of {@link BinaryAttributeRawType }
     * 
     */
    public BinaryAttributeRawType createBinaryAttributeRawType() {
        return new BinaryAttributeRawType();
    }

    /**
     * Create an instance of {@link LovRestrictionType }
     * 
     */
    public LovRestrictionType createLovRestrictionType() {
        return new LovRestrictionType();
    }

    /**
     * Create an instance of {@link InterfaceType }
     * 
     */
    public InterfaceType createInterfaceType() {
        return new InterfaceType();
    }

    /**
     * Create an instance of {@link LovCollectionType }
     * 
     */
    public LovCollectionType createLovCollectionType() {
        return new LovCollectionType();
    }

    /**
     * Create an instance of {@link ObjectType }
     * 
     */
    public ObjectType createObjectType() {
        return new ObjectType();
    }

    /**
     * Create an instance of {@link ActionRule.SetValue }
     * 
     */
    public ActionRule.SetValue createActionRuleSetValue() {
        return new ActionRule.SetValue();
    }

    /**
     * Create an instance of {@link GuidAttributeRawType }
     * 
     */
    public GuidAttributeRawType createGuidAttributeRawType() {
        return new GuidAttributeRawType();
    }

    /**
     * Create an instance of {@link TimedurationAttributeRawType }
     * 
     */
    public TimedurationAttributeRawType createTimedurationAttributeRawType() {
        return new TimedurationAttributeRawType();
    }

    /**
     * Create an instance of {@link DateAttributeType }
     * 
     */
    public DateAttributeType createDateAttributeType() {
        return new DateAttributeType();
    }

    /**
     * Create an instance of {@link ModelObjectBase }
     * 
     */
    public ModelObjectBase createModelObjectBase() {
        return new ModelObjectBase();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InterfaceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.mdaengine.com/mdaengine/models/businessObjectModel", name = "Interface")
    public JAXBElement<InterfaceType> createInterface(InterfaceType value) {
        return new JAXBElement<InterfaceType>(_Interface_QNAME, InterfaceType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.mdaengine.com/mdaengine/models/businessObjectModel", name = "Object")
    public JAXBElement<ObjectType> createObject(ObjectType value) {
        return new JAXBElement<ObjectType>(_Object_QNAME, ObjectType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.mdaengine.com/mdaengine/models/businessObjectModel", name = "MinValue", scope = IntervalRestrictionType.class)
    public JAXBElement<String> createIntervalRestrictionTypeMinValue(String value) {
        return new JAXBElement<String>(_IntervalRestrictionTypeMinValue_QNAME, String.class, IntervalRestrictionType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.mdaengine.com/mdaengine/models/businessObjectModel", name = "MaxValue", scope = IntervalRestrictionType.class)
    public JAXBElement<String> createIntervalRestrictionTypeMaxValue(String value) {
        return new JAXBElement<String>(_IntervalRestrictionTypeMaxValue_QNAME, String.class, IntervalRestrictionType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.mdaengine.com/mdaengine/models/businessObjectModel", name = "MaxLength", scope = BinaryAttributeRawType.class)
    public JAXBElement<BigInteger> createBinaryAttributeRawTypeMaxLength(BigInteger value) {
        return new JAXBElement<BigInteger>(_BinaryAttributeRawTypeMaxLength_QNAME, BigInteger.class, BinaryAttributeRawType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BOMAttributeReferenceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.mdaengine.com/mdaengine/models/businessObjectModel", name = "UniqueIdentifierReference", scope = TextAttributeType.class)
    public JAXBElement<BOMAttributeReferenceType> createTextAttributeTypeUniqueIdentifierReference(BOMAttributeReferenceType value) {
        return new JAXBElement<BOMAttributeReferenceType>(_TextAttributeTypeUniqueIdentifierReference_QNAME, BOMAttributeReferenceType.class, TextAttributeType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.mdaengine.com/mdaengine/models/businessObjectModel", name = "MaxLength", scope = BinaryAttributeType.class)
    public JAXBElement<BigInteger> createBinaryAttributeTypeMaxLength(BigInteger value) {
        return new JAXBElement<BigInteger>(_BinaryAttributeRawTypeMaxLength_QNAME, BigInteger.class, BinaryAttributeType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AttributeNameType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.mdaengine.com/mdaengine/models/businessObjectModel", name = "SetEnable", scope = ActionRule.class)
    public JAXBElement<AttributeNameType> createActionRuleSetEnable(AttributeNameType value) {
        return new JAXBElement<AttributeNameType>(_ActionRuleSetEnable_QNAME, AttributeNameType.class, ActionRule.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AttributeNameType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.mdaengine.com/mdaengine/models/businessObjectModel", name = "SetHidden", scope = ActionRule.class)
    public JAXBElement<AttributeNameType> createActionRuleSetHidden(AttributeNameType value) {
        return new JAXBElement<AttributeNameType>(_ActionRuleSetHidden_QNAME, AttributeNameType.class, ActionRule.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AttributeNameType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.mdaengine.com/mdaengine/models/businessObjectModel", name = "SetVisible", scope = ActionRule.class)
    public JAXBElement<AttributeNameType> createActionRuleSetVisible(AttributeNameType value) {
        return new JAXBElement<AttributeNameType>(_ActionRuleSetVisible_QNAME, AttributeNameType.class, ActionRule.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActionRule.SetValue }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.mdaengine.com/mdaengine/models/businessObjectModel", name = "SetValue", scope = ActionRule.class)
    public JAXBElement<ActionRule.SetValue> createActionRuleSetValue(ActionRule.SetValue value) {
        return new JAXBElement<ActionRule.SetValue>(_ActionRuleSetValue_QNAME, ActionRule.SetValue.class, ActionRule.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActionRule.SetRestriction }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.mdaengine.com/mdaengine/models/businessObjectModel", name = "SetRestriction", scope = ActionRule.class)
    public JAXBElement<ActionRule.SetRestriction> createActionRuleSetRestriction(ActionRule.SetRestriction value) {
        return new JAXBElement<ActionRule.SetRestriction>(_ActionRuleSetRestriction_QNAME, ActionRule.SetRestriction.class, ActionRule.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AttributeNameType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.mdaengine.com/mdaengine/models/businessObjectModel", name = "SetDisable", scope = ActionRule.class)
    public JAXBElement<AttributeNameType> createActionRuleSetDisable(AttributeNameType value) {
        return new JAXBElement<AttributeNameType>(_ActionRuleSetDisable_QNAME, AttributeNameType.class, ActionRule.class, value);
    }

}
