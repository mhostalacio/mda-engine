
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for AttributesGroupAttribute complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AttributesGroupAttribute">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="Name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="TargetAttributeGroup" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AttributesGroupAttribute")
public class AttributesGroupAttribute
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "Name", required = true)
    protected String name;
    @XmlAttribute(name = "TargetAttributeGroup")
    protected String targetAttributeGroup;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the targetAttributeGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetAttributeGroup() {
        return targetAttributeGroup;
    }

    /**
     * Sets the value of the targetAttributeGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetAttributeGroup(String value) {
        this.targetAttributeGroup = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof AttributesGroupAttribute)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final AttributesGroupAttribute that = ((AttributesGroupAttribute) object);
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsTargetAttributeGroup;
            lhsTargetAttributeGroup = this.getTargetAttributeGroup();
            String rhsTargetAttributeGroup;
            rhsTargetAttributeGroup = that.getTargetAttributeGroup();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "targetAttributeGroup", lhsTargetAttributeGroup), LocatorUtils.property(thatLocator, "targetAttributeGroup", rhsTargetAttributeGroup), lhsTargetAttributeGroup, rhsTargetAttributeGroup)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof AttributesGroupAttribute) {
            final AttributesGroupAttribute copy = ((AttributesGroupAttribute) draftCopy);
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.targetAttributeGroup!= null) {
                String sourceTargetAttributeGroup;
                sourceTargetAttributeGroup = this.getTargetAttributeGroup();
                String copyTargetAttributeGroup = ((String) strategy.copy(LocatorUtils.property(locator, "targetAttributeGroup", sourceTargetAttributeGroup), sourceTargetAttributeGroup));
                copy.setTargetAttributeGroup(copyTargetAttributeGroup);
            } else {
                copy.targetAttributeGroup = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new AttributesGroupAttribute();
    }

}
