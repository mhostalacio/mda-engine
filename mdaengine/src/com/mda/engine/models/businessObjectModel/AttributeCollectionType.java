
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for AttributeCollectionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AttributeCollectionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded" minOccurs="0">
 *         &lt;element name="Enum" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}EnumAttributeType" minOccurs="0"/>
 *         &lt;element name="Text" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}TextAttributeType" minOccurs="0"/>
 *         &lt;element name="Longtext" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}LongtextAttributeType" minOccurs="0"/>
 *         &lt;element name="Char" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}CharAttributeType" minOccurs="0"/>
 *         &lt;element name="Number" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}NumberAttributeType" minOccurs="0"/>
 *         &lt;element name="Date" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}DateAttributeType" minOccurs="0"/>
 *         &lt;element name="Time" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}TimeAttributeType" minOccurs="0"/>
 *         &lt;element name="Datetime" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}DatetimeAttributeType" minOccurs="0"/>
 *         &lt;element name="Timeduration" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}TimedurationAttributeType" minOccurs="0"/>
 *         &lt;element name="Timestamp" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}TimestampAttributeType" minOccurs="0"/>
 *         &lt;element name="Boolean" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}BooleanAttributeType" minOccurs="0"/>
 *         &lt;element name="Guid" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}GuidAttributeType" minOccurs="0"/>
 *         &lt;element name="Xml" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}XmlAttributeType" minOccurs="0"/>
 *         &lt;element name="Binary" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}BinaryAttributeType" minOccurs="0"/>
 *         &lt;element name="Aggregation" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}AggregationAttributeType" minOccurs="0"/>
 *         &lt;element name="Composition" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}CompositionAttributeType" minOccurs="0"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AttributeCollectionType", propOrder = {
    "attributeList"
})
public class AttributeCollectionType
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElements({
        @XmlElement(name = "Aggregation", type = AggregationAttributeType.class),
        @XmlElement(name = "Xml", type = XmlAttributeType.class),
        @XmlElement(name = "Longtext", type = LongtextAttributeType.class),
        @XmlElement(name = "Enum", type = EnumAttributeType.class),
        @XmlElement(name = "Char", type = CharAttributeType.class),
        @XmlElement(name = "Date", type = DateAttributeType.class),
        @XmlElement(name = "Number", type = NumberAttributeType.class),
        @XmlElement(name = "Time", type = TimeAttributeType.class),
        @XmlElement(name = "Text", type = TextAttributeType.class),
        @XmlElement(name = "Binary", type = BinaryAttributeType.class),
        @XmlElement(name = "Boolean", type = BooleanAttributeType.class),
        @XmlElement(name = "Timeduration", type = TimedurationAttributeType.class),
        @XmlElement(name = "Composition", type = CompositionAttributeType.class),
        @XmlElement(name = "Guid", type = GuidAttributeType.class),
        @XmlElement(name = "Datetime", type = DatetimeAttributeType.class),
        @XmlElement(name = "Timestamp", type = TimestampAttributeType.class)
    })
    protected List<BaseAttributeType> attributeList;

    /**
     * Gets the value of the attributeList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the attributeList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttributeList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AggregationAttributeType }
     * {@link XmlAttributeType }
     * {@link LongtextAttributeType }
     * {@link EnumAttributeType }
     * {@link CharAttributeType }
     * {@link DateAttributeType }
     * {@link NumberAttributeType }
     * {@link TimeAttributeType }
     * {@link TextAttributeType }
     * {@link BinaryAttributeType }
     * {@link BooleanAttributeType }
     * {@link TimedurationAttributeType }
     * {@link CompositionAttributeType }
     * {@link GuidAttributeType }
     * {@link DatetimeAttributeType }
     * {@link TimestampAttributeType }
     * 
     * 
     */
    public List<BaseAttributeType> getAttributeList() {
        if (attributeList == null) {
            attributeList = new ArrayList<BaseAttributeType>();
        }
        return this.attributeList;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof AttributeCollectionType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final AttributeCollectionType that = ((AttributeCollectionType) object);
        {
            List<BaseAttributeType> lhsAttributeList;
            lhsAttributeList = this.getAttributeList();
            List<BaseAttributeType> rhsAttributeList;
            rhsAttributeList = that.getAttributeList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "attributeList", lhsAttributeList), LocatorUtils.property(thatLocator, "attributeList", rhsAttributeList), lhsAttributeList, rhsAttributeList)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof AttributeCollectionType) {
            final AttributeCollectionType copy = ((AttributeCollectionType) draftCopy);
            if ((this.attributeList!= null)&&(!this.attributeList.isEmpty())) {
                List<BaseAttributeType> sourceAttributeList;
                sourceAttributeList = this.getAttributeList();
                @SuppressWarnings("unchecked")
                List<BaseAttributeType> copyAttributeList = ((List<BaseAttributeType> ) strategy.copy(LocatorUtils.property(locator, "attributeList", sourceAttributeList), sourceAttributeList));
                copy.attributeList = null;
                List<BaseAttributeType> uniqueAttributeListl = copy.getAttributeList();
                uniqueAttributeListl.addAll(copyAttributeList);
            } else {
                copy.attributeList = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new AttributeCollectionType();
    }
    
//--simple--preserve
    public List<BaseAttributeType> getOrderedAttributeList() {
    	List<BaseAttributeType> list = getAttributeList();
    	java.util.Collections.sort(list, new com.mda.engine.utils.AttributeComparator());
        return list;
    }
//--simple--preserve

}
