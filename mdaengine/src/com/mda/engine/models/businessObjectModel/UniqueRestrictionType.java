
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for UniqueRestrictionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UniqueRestrictionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DenyDuplicatedValues" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UniqueRestrictionType", propOrder = {
    "denyDuplicatedValues"
})
public class UniqueRestrictionType
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "DenyDuplicatedValues")
    protected boolean denyDuplicatedValues;

    /**
     * Gets the value of the denyDuplicatedValues property.
     * 
     */
    public boolean isDenyDuplicatedValues() {
        return denyDuplicatedValues;
    }

    /**
     * Sets the value of the denyDuplicatedValues property.
     * 
     */
    public void setDenyDuplicatedValues(boolean value) {
        this.denyDuplicatedValues = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof UniqueRestrictionType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final UniqueRestrictionType that = ((UniqueRestrictionType) object);
        {
            boolean lhsDenyDuplicatedValues;
            lhsDenyDuplicatedValues = this.isDenyDuplicatedValues();
            boolean rhsDenyDuplicatedValues;
            rhsDenyDuplicatedValues = that.isDenyDuplicatedValues();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "denyDuplicatedValues", lhsDenyDuplicatedValues), LocatorUtils.property(thatLocator, "denyDuplicatedValues", rhsDenyDuplicatedValues), lhsDenyDuplicatedValues, rhsDenyDuplicatedValues)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof UniqueRestrictionType) {
            final UniqueRestrictionType copy = ((UniqueRestrictionType) draftCopy);
            boolean sourceDenyDuplicatedValues;
            sourceDenyDuplicatedValues = this.isDenyDuplicatedValues();
            boolean copyDenyDuplicatedValues = strategy.copy(LocatorUtils.property(locator, "denyDuplicatedValues", sourceDenyDuplicatedValues), sourceDenyDuplicatedValues);
            copy.setDenyDuplicatedValues(copyDenyDuplicatedValues);
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new UniqueRestrictionType();
    }

}
