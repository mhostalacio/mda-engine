
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for AttributesGroupAttributeCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AttributesGroupAttributeCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Attribute" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}AttributesGroupAttribute" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AttributesGroupAttributeCollection", propOrder = {
    "attribute"
})
public class AttributesGroupAttributeCollection
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Attribute", required = true)
    protected List<AttributesGroupAttribute> attribute;

    /**
     * Gets the value of the attribute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the attribute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttribute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AttributesGroupAttribute }
     * 
     * 
     */
    public List<AttributesGroupAttribute> getAttribute() {
        if (attribute == null) {
            attribute = new ArrayList<AttributesGroupAttribute>();
        }
        return this.attribute;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof AttributesGroupAttributeCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final AttributesGroupAttributeCollection that = ((AttributesGroupAttributeCollection) object);
        {
            List<AttributesGroupAttribute> lhsAttribute;
            lhsAttribute = this.getAttribute();
            List<AttributesGroupAttribute> rhsAttribute;
            rhsAttribute = that.getAttribute();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "attribute", lhsAttribute), LocatorUtils.property(thatLocator, "attribute", rhsAttribute), lhsAttribute, rhsAttribute)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof AttributesGroupAttributeCollection) {
            final AttributesGroupAttributeCollection copy = ((AttributesGroupAttributeCollection) draftCopy);
            if ((this.attribute!= null)&&(!this.attribute.isEmpty())) {
                List<AttributesGroupAttribute> sourceAttribute;
                sourceAttribute = this.getAttribute();
                @SuppressWarnings("unchecked")
                List<AttributesGroupAttribute> copyAttribute = ((List<AttributesGroupAttribute> ) strategy.copy(LocatorUtils.property(locator, "attribute", sourceAttribute), sourceAttribute));
                copy.attribute = null;
                List<AttributesGroupAttribute> uniqueAttributel = copy.getAttribute();
                uniqueAttributel.addAll(copyAttribute);
            } else {
                copy.attribute = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new AttributesGroupAttributeCollection();
    }

}
