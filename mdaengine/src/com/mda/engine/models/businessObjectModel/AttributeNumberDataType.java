
package com.mda.engine.models.businessObjectModel;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AttributeNumberDataType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AttributeNumberDataType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Decimal"/>
 *     &lt;enumeration value="Double"/>
 *     &lt;enumeration value="Float"/>
 *     &lt;enumeration value="Integer"/>
 *     &lt;enumeration value="Long"/>
 *     &lt;enumeration value="Short"/>
 *     &lt;enumeration value="Byte"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AttributeNumberDataType")
@XmlEnum
public enum AttributeNumberDataType {

    @XmlEnumValue("Decimal")
    DECIMAL("Decimal"),
    @XmlEnumValue("Double")
    DOUBLE("Double"),
    @XmlEnumValue("Float")
    FLOAT("Float"),
    @XmlEnumValue("Integer")
    INTEGER("Integer"),
    @XmlEnumValue("Long")
    LONG("Long"),
    @XmlEnumValue("Short")
    SHORT("Short"),
    @XmlEnumValue("Byte")
    BYTE("Byte");
    private final String value;

    AttributeNumberDataType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AttributeNumberDataType fromValue(String v) {
        for (AttributeNumberDataType c: AttributeNumberDataType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
