
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for CompositionAttributeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CompositionAttributeType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}BaseAttributeWithTargetType">
 *       &lt;sequence>
 *         &lt;element name="TargetAttribute" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}TargetAttributeType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="AvoidGenerateAggIndex" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CompositionAttributeType", propOrder = {
    "targetAttribute"
})
public class CompositionAttributeType
    extends BaseAttributeWithTargetType
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "TargetAttribute")
    protected TargetAttributeType targetAttribute;
    @XmlAttribute(name = "AvoidGenerateAggIndex")
    protected Boolean avoidGenerateAggIndex;

    /**
     * Gets the value of the targetAttribute property.
     * 
     * @return
     *     possible object is
     *     {@link TargetAttributeType }
     *     
     */
    public TargetAttributeType getTargetAttribute() {
        return targetAttribute;
    }

    /**
     * Sets the value of the targetAttribute property.
     * 
     * @param value
     *     allowed object is
     *     {@link TargetAttributeType }
     *     
     */
    public void setTargetAttribute(TargetAttributeType value) {
        this.targetAttribute = value;
    }

    /**
     * Gets the value of the avoidGenerateAggIndex property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isAvoidGenerateAggIndex() {
        if (avoidGenerateAggIndex == null) {
            return false;
        } else {
            return avoidGenerateAggIndex;
        }
    }

    /**
     * Sets the value of the avoidGenerateAggIndex property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAvoidGenerateAggIndex(Boolean value) {
        this.avoidGenerateAggIndex = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof CompositionAttributeType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final CompositionAttributeType that = ((CompositionAttributeType) object);
        {
            TargetAttributeType lhsTargetAttribute;
            lhsTargetAttribute = this.getTargetAttribute();
            TargetAttributeType rhsTargetAttribute;
            rhsTargetAttribute = that.getTargetAttribute();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "targetAttribute", lhsTargetAttribute), LocatorUtils.property(thatLocator, "targetAttribute", rhsTargetAttribute), lhsTargetAttribute, rhsTargetAttribute)) {
                return false;
            }
        }
        {
            boolean lhsAvoidGenerateAggIndex;
            lhsAvoidGenerateAggIndex = this.isAvoidGenerateAggIndex();
            boolean rhsAvoidGenerateAggIndex;
            rhsAvoidGenerateAggIndex = that.isAvoidGenerateAggIndex();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "avoidGenerateAggIndex", lhsAvoidGenerateAggIndex), LocatorUtils.property(thatLocator, "avoidGenerateAggIndex", rhsAvoidGenerateAggIndex), lhsAvoidGenerateAggIndex, rhsAvoidGenerateAggIndex)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof CompositionAttributeType) {
            final CompositionAttributeType copy = ((CompositionAttributeType) draftCopy);
            if (this.targetAttribute!= null) {
                TargetAttributeType sourceTargetAttribute;
                sourceTargetAttribute = this.getTargetAttribute();
                TargetAttributeType copyTargetAttribute = ((TargetAttributeType) strategy.copy(LocatorUtils.property(locator, "targetAttribute", sourceTargetAttribute), sourceTargetAttribute));
                copy.setTargetAttribute(copyTargetAttribute);
            } else {
                copy.targetAttribute = null;
            }
            if (this.avoidGenerateAggIndex!= null) {
                boolean sourceAvoidGenerateAggIndex;
                sourceAvoidGenerateAggIndex = this.isAvoidGenerateAggIndex();
                boolean copyAvoidGenerateAggIndex = strategy.copy(LocatorUtils.property(locator, "avoidGenerateAggIndex", sourceAvoidGenerateAggIndex), sourceAvoidGenerateAggIndex);
                copy.setAvoidGenerateAggIndex(copyAvoidGenerateAggIndex);
            } else {
                copy.avoidGenerateAggIndex = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new CompositionAttributeType();
    }

}
