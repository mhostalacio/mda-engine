
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for QueriesType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueriesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetById" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}GenerateQueryType" minOccurs="0"/>
 *         &lt;element name="GetAll" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}GenerateQueryType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueriesType", propOrder = {
    "getById",
    "getAll"
})
public class QueriesType
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "GetById")
    protected GenerateQueryType getById;
    @XmlElement(name = "GetAll")
    protected GenerateQueryType getAll;

    /**
     * Gets the value of the getById property.
     * 
     * @return
     *     possible object is
     *     {@link GenerateQueryType }
     *     
     */
    public GenerateQueryType getGetById() {
        return getById;
    }

    /**
     * Sets the value of the getById property.
     * 
     * @param value
     *     allowed object is
     *     {@link GenerateQueryType }
     *     
     */
    public void setGetById(GenerateQueryType value) {
        this.getById = value;
    }

    /**
     * Gets the value of the getAll property.
     * 
     * @return
     *     possible object is
     *     {@link GenerateQueryType }
     *     
     */
    public GenerateQueryType getGetAll() {
        return getAll;
    }

    /**
     * Sets the value of the getAll property.
     * 
     * @param value
     *     allowed object is
     *     {@link GenerateQueryType }
     *     
     */
    public void setGetAll(GenerateQueryType value) {
        this.getAll = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof QueriesType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final QueriesType that = ((QueriesType) object);
        {
            GenerateQueryType lhsGetById;
            lhsGetById = this.getGetById();
            GenerateQueryType rhsGetById;
            rhsGetById = that.getGetById();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "getById", lhsGetById), LocatorUtils.property(thatLocator, "getById", rhsGetById), lhsGetById, rhsGetById)) {
                return false;
            }
        }
        {
            GenerateQueryType lhsGetAll;
            lhsGetAll = this.getGetAll();
            GenerateQueryType rhsGetAll;
            rhsGetAll = that.getGetAll();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "getAll", lhsGetAll), LocatorUtils.property(thatLocator, "getAll", rhsGetAll), lhsGetAll, rhsGetAll)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof QueriesType) {
            final QueriesType copy = ((QueriesType) draftCopy);
            if (this.getById!= null) {
                GenerateQueryType sourceGetById;
                sourceGetById = this.getGetById();
                GenerateQueryType copyGetById = ((GenerateQueryType) strategy.copy(LocatorUtils.property(locator, "getById", sourceGetById), sourceGetById));
                copy.setGetById(copyGetById);
            } else {
                copy.getById = null;
            }
            if (this.getAll!= null) {
                GenerateQueryType sourceGetAll;
                sourceGetAll = this.getGetAll();
                GenerateQueryType copyGetAll = ((GenerateQueryType) strategy.copy(LocatorUtils.property(locator, "getAll", sourceGetAll), sourceGetAll));
                copy.setGetAll(copyGetAll);
            } else {
                copy.getAll = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new QueriesType();
    }

}
