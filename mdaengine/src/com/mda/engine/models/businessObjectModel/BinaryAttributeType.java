
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for BinaryAttributeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BinaryAttributeType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}BaseAttributeType">
 *       &lt;sequence>
 *         &lt;element name="MaxLength" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BinaryAttributeType", propOrder = {
    "maxLength"
})
public class BinaryAttributeType
    extends BaseAttributeType
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "MaxLength", namespace = "http://www.mdaengine.com/mdaengine/models/businessObjectModel", type = JAXBElement.class)
    protected JAXBElement<BigInteger> maxLength;

    /**
     * Gets the value of the maxLength property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *     
     */
    public JAXBElement<BigInteger> getMaxLength() {
        return maxLength;
    }

    /**
     * Sets the value of the maxLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *     
     */
    public void setMaxLength(JAXBElement<BigInteger> value) {
        this.maxLength = ((JAXBElement<BigInteger> ) value);
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof BinaryAttributeType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final BinaryAttributeType that = ((BinaryAttributeType) object);
        {
            JAXBElement<BigInteger> lhsMaxLength;
            lhsMaxLength = this.getMaxLength();
            JAXBElement<BigInteger> rhsMaxLength;
            rhsMaxLength = that.getMaxLength();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "maxLength", lhsMaxLength), LocatorUtils.property(thatLocator, "maxLength", rhsMaxLength), lhsMaxLength, rhsMaxLength)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof BinaryAttributeType) {
            final BinaryAttributeType copy = ((BinaryAttributeType) draftCopy);
            if (this.maxLength!= null) {
                JAXBElement<BigInteger> sourceMaxLength;
                sourceMaxLength = this.getMaxLength();
                @SuppressWarnings("unchecked")
                JAXBElement<BigInteger> copyMaxLength = ((JAXBElement<BigInteger> ) strategy.copy(LocatorUtils.property(locator, "maxLength", sourceMaxLength), sourceMaxLength));
                copy.setMaxLength(copyMaxLength);
            } else {
                copy.maxLength = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new BinaryAttributeType();
    }

}
