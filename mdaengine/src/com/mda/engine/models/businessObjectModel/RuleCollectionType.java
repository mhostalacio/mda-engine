
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for RuleCollectionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RuleCollectionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded" minOccurs="0">
 *         &lt;element name="Rule">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Event">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;enumeration value="OnInit"/>
 *                         &lt;enumeration value="OnLoad"/>
 *                         &lt;enumeration value="OnChange"/>
 *                         &lt;enumeration value="OnSave"/>
 *                         &lt;enumeration value="OnSaveNew"/>
 *                         &lt;enumeration value="OnRemove"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="WhenCondition" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="True" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}ActionRule"/>
 *                   &lt;element name="False" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}ActionRule"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="Name" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}RegularString" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RuleCollectionType", propOrder = {
    "rule"
})
public class RuleCollectionType
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Rule")
    protected List<RuleCollectionType.Rule> rule;

    /**
     * Gets the value of the rule property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rule property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRule().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RuleCollectionType.Rule }
     * 
     * 
     */
    public List<RuleCollectionType.Rule> getRule() {
        if (rule == null) {
            rule = new ArrayList<RuleCollectionType.Rule>();
        }
        return this.rule;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof RuleCollectionType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final RuleCollectionType that = ((RuleCollectionType) object);
        {
            List<RuleCollectionType.Rule> lhsRule;
            lhsRule = this.getRule();
            List<RuleCollectionType.Rule> rhsRule;
            rhsRule = that.getRule();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "rule", lhsRule), LocatorUtils.property(thatLocator, "rule", rhsRule), lhsRule, rhsRule)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof RuleCollectionType) {
            final RuleCollectionType copy = ((RuleCollectionType) draftCopy);
            if ((this.rule!= null)&&(!this.rule.isEmpty())) {
                List<RuleCollectionType.Rule> sourceRule;
                sourceRule = this.getRule();
                @SuppressWarnings("unchecked")
                List<RuleCollectionType.Rule> copyRule = ((List<RuleCollectionType.Rule> ) strategy.copy(LocatorUtils.property(locator, "rule", sourceRule), sourceRule));
                copy.rule = null;
                List<RuleCollectionType.Rule> uniqueRulel = copy.getRule();
                uniqueRulel.addAll(copyRule);
            } else {
                copy.rule = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new RuleCollectionType();
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Event">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;enumeration value="OnInit"/>
     *               &lt;enumeration value="OnLoad"/>
     *               &lt;enumeration value="OnChange"/>
     *               &lt;enumeration value="OnSave"/>
     *               &lt;enumeration value="OnSaveNew"/>
     *               &lt;enumeration value="OnRemove"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="WhenCondition" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="True" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}ActionRule"/>
     *         &lt;element name="False" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}ActionRule"/>
     *       &lt;/sequence>
     *       &lt;attribute name="Name" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}RegularString" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "event",
        "whenCondition",
        "_true",
        "_false"
    })
    public static class Rule
        extends ModelNodeBase
        implements Serializable, Cloneable, CopyTo, Equals
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "Event", required = true)
        protected String event;
        @XmlElement(name = "WhenCondition", required = true)
        protected String whenCondition;
        @XmlElement(name = "True", required = true)
        protected ActionRule _true;
        @XmlElement(name = "False", required = true)
        protected ActionRule _false;
        @XmlAttribute(name = "Name")
        protected String name;

        /**
         * Gets the value of the event property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEvent() {
            return event;
        }

        /**
         * Sets the value of the event property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEvent(String value) {
            this.event = value;
        }

        /**
         * Gets the value of the whenCondition property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getWhenCondition() {
            return whenCondition;
        }

        /**
         * Sets the value of the whenCondition property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setWhenCondition(String value) {
            this.whenCondition = value;
        }

        /**
         * Gets the value of the true property.
         * 
         * @return
         *     possible object is
         *     {@link ActionRule }
         *     
         */
        public ActionRule getTrue() {
            return _true;
        }

        /**
         * Sets the value of the true property.
         * 
         * @param value
         *     allowed object is
         *     {@link ActionRule }
         *     
         */
        public void setTrue(ActionRule value) {
            this._true = value;
        }

        /**
         * Gets the value of the false property.
         * 
         * @return
         *     possible object is
         *     {@link ActionRule }
         *     
         */
        public ActionRule getFalse() {
            return _false;
        }

        /**
         * Sets the value of the false property.
         * 
         * @param value
         *     allowed object is
         *     {@link ActionRule }
         *     
         */
        public void setFalse(ActionRule value) {
            this._false = value;
        }

        /**
         * Gets the value of the name property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getName() {
            return name;
        }

        /**
         * Sets the value of the name property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setName(String value) {
            this.name = value;
        }

        public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
            if (!(object instanceof RuleCollectionType.Rule)) {
                return false;
            }
            if (this == object) {
                return true;
            }
            final RuleCollectionType.Rule that = ((RuleCollectionType.Rule) object);
            {
                String lhsEvent;
                lhsEvent = this.getEvent();
                String rhsEvent;
                rhsEvent = that.getEvent();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "event", lhsEvent), LocatorUtils.property(thatLocator, "event", rhsEvent), lhsEvent, rhsEvent)) {
                    return false;
                }
            }
            {
                String lhsWhenCondition;
                lhsWhenCondition = this.getWhenCondition();
                String rhsWhenCondition;
                rhsWhenCondition = that.getWhenCondition();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "whenCondition", lhsWhenCondition), LocatorUtils.property(thatLocator, "whenCondition", rhsWhenCondition), lhsWhenCondition, rhsWhenCondition)) {
                    return false;
                }
            }
            {
                ActionRule lhsTrue;
                lhsTrue = this.getTrue();
                ActionRule rhsTrue;
                rhsTrue = that.getTrue();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "_true", lhsTrue), LocatorUtils.property(thatLocator, "_true", rhsTrue), lhsTrue, rhsTrue)) {
                    return false;
                }
            }
            {
                ActionRule lhsFalse;
                lhsFalse = this.getFalse();
                ActionRule rhsFalse;
                rhsFalse = that.getFalse();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "_false", lhsFalse), LocatorUtils.property(thatLocator, "_false", rhsFalse), lhsFalse, rhsFalse)) {
                    return false;
                }
            }
            {
                String lhsName;
                lhsName = this.getName();
                String rhsName;
                rhsName = that.getName();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                    return false;
                }
            }
            return true;
        }

        public boolean equals(Object object) {
            final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
            return equals(null, null, object, strategy);
        }

        public Object clone() {
            return copyTo(createNewInstance());
        }

        public Object copyTo(Object target) {
            final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
            return copyTo(null, target, strategy);
        }

        public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
            final Object draftCopy = ((target == null)?createNewInstance():target);
            if (draftCopy instanceof RuleCollectionType.Rule) {
                final RuleCollectionType.Rule copy = ((RuleCollectionType.Rule) draftCopy);
                if (this.event!= null) {
                    String sourceEvent;
                    sourceEvent = this.getEvent();
                    String copyEvent = ((String) strategy.copy(LocatorUtils.property(locator, "event", sourceEvent), sourceEvent));
                    copy.setEvent(copyEvent);
                } else {
                    copy.event = null;
                }
                if (this.whenCondition!= null) {
                    String sourceWhenCondition;
                    sourceWhenCondition = this.getWhenCondition();
                    String copyWhenCondition = ((String) strategy.copy(LocatorUtils.property(locator, "whenCondition", sourceWhenCondition), sourceWhenCondition));
                    copy.setWhenCondition(copyWhenCondition);
                } else {
                    copy.whenCondition = null;
                }
                if (this._true!= null) {
                    ActionRule sourceTrue;
                    sourceTrue = this.getTrue();
                    ActionRule copyTrue = ((ActionRule) strategy.copy(LocatorUtils.property(locator, "_true", sourceTrue), sourceTrue));
                    copy.setTrue(copyTrue);
                } else {
                    copy._true = null;
                }
                if (this._false!= null) {
                    ActionRule sourceFalse;
                    sourceFalse = this.getFalse();
                    ActionRule copyFalse = ((ActionRule) strategy.copy(LocatorUtils.property(locator, "_false", sourceFalse), sourceFalse));
                    copy.setFalse(copyFalse);
                } else {
                    copy._false = null;
                }
                if (this.name!= null) {
                    String sourceName;
                    sourceName = this.getName();
                    String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                    copy.setName(copyName);
                } else {
                    copy.name = null;
                }
            }
            return draftCopy;
        }

        public Object createNewInstance() {
            return new RuleCollectionType.Rule();
        }

    }

}
