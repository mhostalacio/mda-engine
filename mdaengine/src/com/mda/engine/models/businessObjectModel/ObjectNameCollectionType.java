
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ObjectNameCollectionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ObjectNameCollectionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ObjectName" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}RegularString" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectNameCollectionType", propOrder = {
    "objectName"
})
public class ObjectNameCollectionType
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "ObjectName")
    protected List<String> objectName;

    /**
     * Gets the value of the objectName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the objectName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getObjectName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getObjectName() {
        if (objectName == null) {
            objectName = new ArrayList<String>();
        }
        return this.objectName;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ObjectNameCollectionType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ObjectNameCollectionType that = ((ObjectNameCollectionType) object);
        {
            List<String> lhsObjectName;
            lhsObjectName = this.getObjectName();
            List<String> rhsObjectName;
            rhsObjectName = that.getObjectName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "objectName", lhsObjectName), LocatorUtils.property(thatLocator, "objectName", rhsObjectName), lhsObjectName, rhsObjectName)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof ObjectNameCollectionType) {
            final ObjectNameCollectionType copy = ((ObjectNameCollectionType) draftCopy);
            if ((this.objectName!= null)&&(!this.objectName.isEmpty())) {
                List<String> sourceObjectName;
                sourceObjectName = this.getObjectName();
                @SuppressWarnings("unchecked")
                List<String> copyObjectName = ((List<String> ) strategy.copy(LocatorUtils.property(locator, "objectName", sourceObjectName), sourceObjectName));
                copy.objectName = null;
                List<String> uniqueObjectNamel = copy.getObjectName();
                uniqueObjectNamel.addAll(copyObjectName);
            } else {
                copy.objectName = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ObjectNameCollectionType();
    }

}
