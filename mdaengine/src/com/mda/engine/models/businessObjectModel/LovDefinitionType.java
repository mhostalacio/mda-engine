
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for LovDefinitionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LovDefinitionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Description" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}NotEmptyString"/>
 *         &lt;element name="Label" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}NotEmptyString"/>
 *         &lt;element name="AcceptExtraValues" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Translatable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Items" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}LovItemCollection"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Name" use="required" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}RegularString" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LovDefinitionType", propOrder = {
    "description",
    "label",
    "acceptExtraValues",
    "translatable",
    "items"
})
public class LovDefinitionType
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Description", required = true)
    protected String description;
    @XmlElement(name = "Label", required = true)
    protected String label;
    @XmlElement(name = "AcceptExtraValues")
    protected boolean acceptExtraValues;
    @XmlElement(name = "Translatable")
    protected boolean translatable;
    @XmlElement(name = "Items", required = true)
    protected LovItemCollection items;
    @XmlAttribute(name = "Name", required = true)
    protected String name;

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Gets the value of the acceptExtraValues property.
     * 
     */
    public boolean isAcceptExtraValues() {
        return acceptExtraValues;
    }

    /**
     * Sets the value of the acceptExtraValues property.
     * 
     */
    public void setAcceptExtraValues(boolean value) {
        this.acceptExtraValues = value;
    }

    /**
     * Gets the value of the translatable property.
     * 
     */
    public boolean isTranslatable() {
        return translatable;
    }

    /**
     * Sets the value of the translatable property.
     * 
     */
    public void setTranslatable(boolean value) {
        this.translatable = value;
    }

    /**
     * Gets the value of the items property.
     * 
     * @return
     *     possible object is
     *     {@link LovItemCollection }
     *     
     */
    public LovItemCollection getItems() {
        return items;
    }

    /**
     * Sets the value of the items property.
     * 
     * @param value
     *     allowed object is
     *     {@link LovItemCollection }
     *     
     */
    public void setItems(LovItemCollection value) {
        this.items = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof LovDefinitionType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final LovDefinitionType that = ((LovDefinitionType) object);
        {
            String lhsDescription;
            lhsDescription = this.getDescription();
            String rhsDescription;
            rhsDescription = that.getDescription();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "description", lhsDescription), LocatorUtils.property(thatLocator, "description", rhsDescription), lhsDescription, rhsDescription)) {
                return false;
            }
        }
        {
            String lhsLabel;
            lhsLabel = this.getLabel();
            String rhsLabel;
            rhsLabel = that.getLabel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "label", lhsLabel), LocatorUtils.property(thatLocator, "label", rhsLabel), lhsLabel, rhsLabel)) {
                return false;
            }
        }
        {
            boolean lhsAcceptExtraValues;
            lhsAcceptExtraValues = this.isAcceptExtraValues();
            boolean rhsAcceptExtraValues;
            rhsAcceptExtraValues = that.isAcceptExtraValues();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "acceptExtraValues", lhsAcceptExtraValues), LocatorUtils.property(thatLocator, "acceptExtraValues", rhsAcceptExtraValues), lhsAcceptExtraValues, rhsAcceptExtraValues)) {
                return false;
            }
        }
        {
            boolean lhsTranslatable;
            lhsTranslatable = this.isTranslatable();
            boolean rhsTranslatable;
            rhsTranslatable = that.isTranslatable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "translatable", lhsTranslatable), LocatorUtils.property(thatLocator, "translatable", rhsTranslatable), lhsTranslatable, rhsTranslatable)) {
                return false;
            }
        }
        {
            LovItemCollection lhsItems;
            lhsItems = this.getItems();
            LovItemCollection rhsItems;
            rhsItems = that.getItems();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "items", lhsItems), LocatorUtils.property(thatLocator, "items", rhsItems), lhsItems, rhsItems)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof LovDefinitionType) {
            final LovDefinitionType copy = ((LovDefinitionType) draftCopy);
            if (this.description!= null) {
                String sourceDescription;
                sourceDescription = this.getDescription();
                String copyDescription = ((String) strategy.copy(LocatorUtils.property(locator, "description", sourceDescription), sourceDescription));
                copy.setDescription(copyDescription);
            } else {
                copy.description = null;
            }
            if (this.label!= null) {
                String sourceLabel;
                sourceLabel = this.getLabel();
                String copyLabel = ((String) strategy.copy(LocatorUtils.property(locator, "label", sourceLabel), sourceLabel));
                copy.setLabel(copyLabel);
            } else {
                copy.label = null;
            }
            boolean sourceAcceptExtraValues;
            sourceAcceptExtraValues = this.isAcceptExtraValues();
            boolean copyAcceptExtraValues = strategy.copy(LocatorUtils.property(locator, "acceptExtraValues", sourceAcceptExtraValues), sourceAcceptExtraValues);
            copy.setAcceptExtraValues(copyAcceptExtraValues);
            boolean sourceTranslatable;
            sourceTranslatable = this.isTranslatable();
            boolean copyTranslatable = strategy.copy(LocatorUtils.property(locator, "translatable", sourceTranslatable), sourceTranslatable);
            copy.setTranslatable(copyTranslatable);
            if (this.items!= null) {
                LovItemCollection sourceItems;
                sourceItems = this.getItems();
                LovItemCollection copyItems = ((LovItemCollection) strategy.copy(LocatorUtils.property(locator, "items", sourceItems), sourceItems));
                copy.setItems(copyItems);
            } else {
                copy.items = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new LovDefinitionType();
    }
    
//--simple--preserve
    
    public LovItem getItemByCode(String code)
    {
    	for(LovItem item : this.getItems().getItem())
    	{
    		if (item.getCode().equals(code))
    			return item;
    	}
    	return null;
    }
//--simple--preserve

}
