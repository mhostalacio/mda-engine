
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for NumberAttributeRawType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NumberAttributeRawType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Len" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="Dec" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="DataType" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}AttributeNumberDataType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NumberAttributeRawType", propOrder = {
    "len",
    "dec",
    "dataType"
})
public class NumberAttributeRawType
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Len")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger len;
    @XmlElement(name = "Dec")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger dec;
    @XmlElement(name = "DataType")
    protected AttributeNumberDataType dataType;

    /**
     * Gets the value of the len property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLen() {
        return len;
    }

    /**
     * Sets the value of the len property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLen(BigInteger value) {
        this.len = value;
    }

    /**
     * Gets the value of the dec property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDec() {
        return dec;
    }

    /**
     * Sets the value of the dec property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDec(BigInteger value) {
        this.dec = value;
    }

    /**
     * Gets the value of the dataType property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeNumberDataType }
     *     
     */
    public AttributeNumberDataType getDataType() {
        return dataType;
    }

    /**
     * Sets the value of the dataType property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeNumberDataType }
     *     
     */
    public void setDataType(AttributeNumberDataType value) {
        this.dataType = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof NumberAttributeRawType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final NumberAttributeRawType that = ((NumberAttributeRawType) object);
        {
            BigInteger lhsLen;
            lhsLen = this.getLen();
            BigInteger rhsLen;
            rhsLen = that.getLen();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "len", lhsLen), LocatorUtils.property(thatLocator, "len", rhsLen), lhsLen, rhsLen)) {
                return false;
            }
        }
        {
            BigInteger lhsDec;
            lhsDec = this.getDec();
            BigInteger rhsDec;
            rhsDec = that.getDec();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dec", lhsDec), LocatorUtils.property(thatLocator, "dec", rhsDec), lhsDec, rhsDec)) {
                return false;
            }
        }
        {
            AttributeNumberDataType lhsDataType;
            lhsDataType = this.getDataType();
            AttributeNumberDataType rhsDataType;
            rhsDataType = that.getDataType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dataType", lhsDataType), LocatorUtils.property(thatLocator, "dataType", rhsDataType), lhsDataType, rhsDataType)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof NumberAttributeRawType) {
            final NumberAttributeRawType copy = ((NumberAttributeRawType) draftCopy);
            if (this.len!= null) {
                BigInteger sourceLen;
                sourceLen = this.getLen();
                BigInteger copyLen = ((BigInteger) strategy.copy(LocatorUtils.property(locator, "len", sourceLen), sourceLen));
                copy.setLen(copyLen);
            } else {
                copy.len = null;
            }
            if (this.dec!= null) {
                BigInteger sourceDec;
                sourceDec = this.getDec();
                BigInteger copyDec = ((BigInteger) strategy.copy(LocatorUtils.property(locator, "dec", sourceDec), sourceDec));
                copy.setDec(copyDec);
            } else {
                copy.dec = null;
            }
            if (this.dataType!= null) {
                AttributeNumberDataType sourceDataType;
                sourceDataType = this.getDataType();
                AttributeNumberDataType copyDataType = ((AttributeNumberDataType) strategy.copy(LocatorUtils.property(locator, "dataType", sourceDataType), sourceDataType));
                copy.setDataType(copyDataType);
            } else {
                copy.dataType = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new NumberAttributeRawType();
    }

}
