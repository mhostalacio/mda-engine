
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for LovCollectionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LovCollectionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence maxOccurs="unbounded" minOccurs="0">
 *         &lt;element name="LovDefinition" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}LovDefinitionType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LovCollectionType", propOrder = {
    "lovDefinition"
})
public class LovCollectionType
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "LovDefinition")
    protected List<LovDefinitionType> lovDefinition;

    /**
     * Gets the value of the lovDefinition property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lovDefinition property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLovDefinition().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LovDefinitionType }
     * 
     * 
     */
    public List<LovDefinitionType> getLovDefinition() {
        if (lovDefinition == null) {
            lovDefinition = new ArrayList<LovDefinitionType>();
        }
        return this.lovDefinition;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof LovCollectionType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final LovCollectionType that = ((LovCollectionType) object);
        {
            List<LovDefinitionType> lhsLovDefinition;
            lhsLovDefinition = this.getLovDefinition();
            List<LovDefinitionType> rhsLovDefinition;
            rhsLovDefinition = that.getLovDefinition();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "lovDefinition", lhsLovDefinition), LocatorUtils.property(thatLocator, "lovDefinition", rhsLovDefinition), lhsLovDefinition, rhsLovDefinition)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof LovCollectionType) {
            final LovCollectionType copy = ((LovCollectionType) draftCopy);
            if ((this.lovDefinition!= null)&&(!this.lovDefinition.isEmpty())) {
                List<LovDefinitionType> sourceLovDefinition;
                sourceLovDefinition = this.getLovDefinition();
                @SuppressWarnings("unchecked")
                List<LovDefinitionType> copyLovDefinition = ((List<LovDefinitionType> ) strategy.copy(LocatorUtils.property(locator, "lovDefinition", sourceLovDefinition), sourceLovDefinition));
                copy.lovDefinition = null;
                List<LovDefinitionType> uniqueLovDefinitionl = copy.getLovDefinition();
                uniqueLovDefinitionl.addAll(copyLovDefinition);
            } else {
                copy.lovDefinition = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new LovCollectionType();
    }

}
