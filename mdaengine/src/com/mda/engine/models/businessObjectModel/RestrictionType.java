
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for RestrictionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RestrictionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded" minOccurs="0">
 *         &lt;element name="LovRestriction" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}LovRestrictionType"/>
 *         &lt;element name="IntervalRestriction" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}IntervalRestrictionType"/>
 *         &lt;element name="RegexRestriction" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}RegexRestrictionType"/>
 *         &lt;element name="UniqueRestriction" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}UniqueRestrictionType"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RestrictionType", propOrder = {
    "lovRestrictionOrIntervalRestrictionOrRegexRestriction"
})
public class RestrictionType
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElements({
        @XmlElement(name = "UniqueRestriction", type = UniqueRestrictionType.class),
        @XmlElement(name = "LovRestriction", type = LovRestrictionType.class),
        @XmlElement(name = "RegexRestriction", type = RegexRestrictionType.class),
        @XmlElement(name = "IntervalRestriction", type = IntervalRestrictionType.class)
    })
    protected List<ModelNodeBase> lovRestrictionOrIntervalRestrictionOrRegexRestriction;

    /**
     * Gets the value of the lovRestrictionOrIntervalRestrictionOrRegexRestriction property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lovRestrictionOrIntervalRestrictionOrRegexRestriction property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLovRestrictionOrIntervalRestrictionOrRegexRestriction().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UniqueRestrictionType }
     * {@link LovRestrictionType }
     * {@link RegexRestrictionType }
     * {@link IntervalRestrictionType }
     * 
     * 
     */
    public List<ModelNodeBase> getLovRestrictionOrIntervalRestrictionOrRegexRestriction() {
        if (lovRestrictionOrIntervalRestrictionOrRegexRestriction == null) {
            lovRestrictionOrIntervalRestrictionOrRegexRestriction = new ArrayList<ModelNodeBase>();
        }
        return this.lovRestrictionOrIntervalRestrictionOrRegexRestriction;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof RestrictionType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final RestrictionType that = ((RestrictionType) object);
        {
            List<ModelNodeBase> lhsLovRestrictionOrIntervalRestrictionOrRegexRestriction;
            lhsLovRestrictionOrIntervalRestrictionOrRegexRestriction = this.getLovRestrictionOrIntervalRestrictionOrRegexRestriction();
            List<ModelNodeBase> rhsLovRestrictionOrIntervalRestrictionOrRegexRestriction;
            rhsLovRestrictionOrIntervalRestrictionOrRegexRestriction = that.getLovRestrictionOrIntervalRestrictionOrRegexRestriction();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "lovRestrictionOrIntervalRestrictionOrRegexRestriction", lhsLovRestrictionOrIntervalRestrictionOrRegexRestriction), LocatorUtils.property(thatLocator, "lovRestrictionOrIntervalRestrictionOrRegexRestriction", rhsLovRestrictionOrIntervalRestrictionOrRegexRestriction), lhsLovRestrictionOrIntervalRestrictionOrRegexRestriction, rhsLovRestrictionOrIntervalRestrictionOrRegexRestriction)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof RestrictionType) {
            final RestrictionType copy = ((RestrictionType) draftCopy);
            if ((this.lovRestrictionOrIntervalRestrictionOrRegexRestriction!= null)&&(!this.lovRestrictionOrIntervalRestrictionOrRegexRestriction.isEmpty())) {
                List<ModelNodeBase> sourceLovRestrictionOrIntervalRestrictionOrRegexRestriction;
                sourceLovRestrictionOrIntervalRestrictionOrRegexRestriction = this.getLovRestrictionOrIntervalRestrictionOrRegexRestriction();
                @SuppressWarnings("unchecked")
                List<ModelNodeBase> copyLovRestrictionOrIntervalRestrictionOrRegexRestriction = ((List<ModelNodeBase> ) strategy.copy(LocatorUtils.property(locator, "lovRestrictionOrIntervalRestrictionOrRegexRestriction", sourceLovRestrictionOrIntervalRestrictionOrRegexRestriction), sourceLovRestrictionOrIntervalRestrictionOrRegexRestriction));
                copy.lovRestrictionOrIntervalRestrictionOrRegexRestriction = null;
                List<ModelNodeBase> uniqueLovRestrictionOrIntervalRestrictionOrRegexRestrictionl = copy.getLovRestrictionOrIntervalRestrictionOrRegexRestriction();
                uniqueLovRestrictionOrIntervalRestrictionOrRegexRestrictionl.addAll(copyLovRestrictionOrIntervalRestrictionOrRegexRestriction);
            } else {
                copy.lovRestrictionOrIntervalRestrictionOrRegexRestriction = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new RestrictionType();
    }

}
