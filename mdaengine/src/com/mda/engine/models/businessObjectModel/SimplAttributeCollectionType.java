
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for SimplAttributeCollectionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SimplAttributeCollectionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;element name="Text" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}TextAttributeType" minOccurs="0"/>
 *         &lt;element name="Longtext" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}LongtextAttributeType" minOccurs="0"/>
 *         &lt;element name="Char" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}CharAttributeType" minOccurs="0"/>
 *         &lt;element name="Number" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}NumberAttributeType" minOccurs="0"/>
 *         &lt;element name="Date" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}DateAttributeType" minOccurs="0"/>
 *         &lt;element name="Time" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}TimeAttributeType" minOccurs="0"/>
 *         &lt;element name="Datetime" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}DatetimeAttributeType" minOccurs="0"/>
 *         &lt;element name="Timeduration" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}TimedurationAttributeType" minOccurs="0"/>
 *         &lt;element name="Timestamp" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}TimestampAttributeType" minOccurs="0"/>
 *         &lt;element name="Check" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}BooleanAttributeType" minOccurs="0"/>
 *         &lt;element name="Identity" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}IndentityAttributeType" minOccurs="0"/>
 *         &lt;element name="Guid" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}GuidAttributeType" minOccurs="0"/>
 *         &lt;element name="Xml" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}XmlAttributeType" minOccurs="0"/>
 *         &lt;element name="Binary" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}BinaryAttributeType" minOccurs="0"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SimplAttributeCollectionType", propOrder = {
    "textOrLongtextOrChar"
})
public class SimplAttributeCollectionType
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElements({
        @XmlElement(name = "Text", type = TextAttributeType.class),
        @XmlElement(name = "Number", type = NumberAttributeType.class),
        @XmlElement(name = "Char", type = CharAttributeType.class),
        @XmlElement(name = "Guid", type = GuidAttributeType.class),
        @XmlElement(name = "Timeduration", type = TimedurationAttributeType.class),
        @XmlElement(name = "Timestamp", type = TimestampAttributeType.class),
        @XmlElement(name = "Check", type = BooleanAttributeType.class),
        @XmlElement(name = "Binary", type = BinaryAttributeType.class),
        @XmlElement(name = "Longtext", type = LongtextAttributeType.class),
        @XmlElement(name = "Date", type = DateAttributeType.class),
        @XmlElement(name = "Xml", type = XmlAttributeType.class),
        @XmlElement(name = "Identity", type = IndentityAttributeType.class),
        @XmlElement(name = "Time", type = TimeAttributeType.class),
        @XmlElement(name = "Datetime", type = DatetimeAttributeType.class)
    })
    protected List<BaseAttributeType> textOrLongtextOrChar;

    /**
     * Gets the value of the textOrLongtextOrChar property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the textOrLongtextOrChar property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTextOrLongtextOrChar().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TextAttributeType }
     * {@link NumberAttributeType }
     * {@link CharAttributeType }
     * {@link GuidAttributeType }
     * {@link TimedurationAttributeType }
     * {@link TimestampAttributeType }
     * {@link BooleanAttributeType }
     * {@link BinaryAttributeType }
     * {@link LongtextAttributeType }
     * {@link DateAttributeType }
     * {@link XmlAttributeType }
     * {@link IndentityAttributeType }
     * {@link TimeAttributeType }
     * {@link DatetimeAttributeType }
     * 
     * 
     */
    public List<BaseAttributeType> getTextOrLongtextOrChar() {
        if (textOrLongtextOrChar == null) {
            textOrLongtextOrChar = new ArrayList<BaseAttributeType>();
        }
        return this.textOrLongtextOrChar;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof SimplAttributeCollectionType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final SimplAttributeCollectionType that = ((SimplAttributeCollectionType) object);
        {
            List<BaseAttributeType> lhsTextOrLongtextOrChar;
            lhsTextOrLongtextOrChar = this.getTextOrLongtextOrChar();
            List<BaseAttributeType> rhsTextOrLongtextOrChar;
            rhsTextOrLongtextOrChar = that.getTextOrLongtextOrChar();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "textOrLongtextOrChar", lhsTextOrLongtextOrChar), LocatorUtils.property(thatLocator, "textOrLongtextOrChar", rhsTextOrLongtextOrChar), lhsTextOrLongtextOrChar, rhsTextOrLongtextOrChar)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof SimplAttributeCollectionType) {
            final SimplAttributeCollectionType copy = ((SimplAttributeCollectionType) draftCopy);
            if ((this.textOrLongtextOrChar!= null)&&(!this.textOrLongtextOrChar.isEmpty())) {
                List<BaseAttributeType> sourceTextOrLongtextOrChar;
                sourceTextOrLongtextOrChar = this.getTextOrLongtextOrChar();
                @SuppressWarnings("unchecked")
                List<BaseAttributeType> copyTextOrLongtextOrChar = ((List<BaseAttributeType> ) strategy.copy(LocatorUtils.property(locator, "textOrLongtextOrChar", sourceTextOrLongtextOrChar), sourceTextOrLongtextOrChar));
                copy.textOrLongtextOrChar = null;
                List<BaseAttributeType> uniqueTextOrLongtextOrCharl = copy.getTextOrLongtextOrChar();
                uniqueTextOrLongtextOrCharl.addAll(copyTextOrLongtextOrChar);
            } else {
                copy.textOrLongtextOrChar = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new SimplAttributeCollectionType();
    }

}
