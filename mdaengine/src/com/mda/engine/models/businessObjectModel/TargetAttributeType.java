
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for TargetAttributeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TargetAttributeType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}BaseType">
 *       &lt;all>
 *         &lt;element name="Label" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BridgeAttributes" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}AttributeCollectionType" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TargetAttributeType", propOrder = {
    "label",
    "description",
    "bridgeAttributes"
})
public class TargetAttributeType
    extends BaseType
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Label", required = true)
    protected String label;
    @XmlElement(name = "Description", required = true)
    protected String description;
    @XmlElement(name = "BridgeAttributes")
    protected AttributeCollectionType bridgeAttributes;

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the bridgeAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeCollectionType }
     *     
     */
    public AttributeCollectionType getBridgeAttributes() {
        return bridgeAttributes;
    }

    /**
     * Sets the value of the bridgeAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeCollectionType }
     *     
     */
    public void setBridgeAttributes(AttributeCollectionType value) {
        this.bridgeAttributes = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof TargetAttributeType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final TargetAttributeType that = ((TargetAttributeType) object);
        {
            String lhsLabel;
            lhsLabel = this.getLabel();
            String rhsLabel;
            rhsLabel = that.getLabel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "label", lhsLabel), LocatorUtils.property(thatLocator, "label", rhsLabel), lhsLabel, rhsLabel)) {
                return false;
            }
        }
        {
            String lhsDescription;
            lhsDescription = this.getDescription();
            String rhsDescription;
            rhsDescription = that.getDescription();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "description", lhsDescription), LocatorUtils.property(thatLocator, "description", rhsDescription), lhsDescription, rhsDescription)) {
                return false;
            }
        }
        {
            AttributeCollectionType lhsBridgeAttributes;
            lhsBridgeAttributes = this.getBridgeAttributes();
            AttributeCollectionType rhsBridgeAttributes;
            rhsBridgeAttributes = that.getBridgeAttributes();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "bridgeAttributes", lhsBridgeAttributes), LocatorUtils.property(thatLocator, "bridgeAttributes", rhsBridgeAttributes), lhsBridgeAttributes, rhsBridgeAttributes)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof TargetAttributeType) {
            final TargetAttributeType copy = ((TargetAttributeType) draftCopy);
            if (this.label!= null) {
                String sourceLabel;
                sourceLabel = this.getLabel();
                String copyLabel = ((String) strategy.copy(LocatorUtils.property(locator, "label", sourceLabel), sourceLabel));
                copy.setLabel(copyLabel);
            } else {
                copy.label = null;
            }
            if (this.description!= null) {
                String sourceDescription;
                sourceDescription = this.getDescription();
                String copyDescription = ((String) strategy.copy(LocatorUtils.property(locator, "description", sourceDescription), sourceDescription));
                copy.setDescription(copyDescription);
            } else {
                copy.description = null;
            }
            if (this.bridgeAttributes!= null) {
                AttributeCollectionType sourceBridgeAttributes;
                sourceBridgeAttributes = this.getBridgeAttributes();
                AttributeCollectionType copyBridgeAttributes = ((AttributeCollectionType) strategy.copy(LocatorUtils.property(locator, "bridgeAttributes", sourceBridgeAttributes), sourceBridgeAttributes));
                copy.setBridgeAttributes(copyBridgeAttributes);
            } else {
                copy.bridgeAttributes = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new TargetAttributeType();
    }

}
