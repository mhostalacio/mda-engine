
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for BaseAttributeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BaseAttributeType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}BaseType">
 *       &lt;sequence>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IsNullable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Restriction" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}RestrictionType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="UseLazyInitialization" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="List" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BaseAttributeType", propOrder = {
    "description",
    "isNullable",
    "restriction"
})
@XmlSeeAlso({
    BooleanAttributeType.class,
    TimestampAttributeType.class,
    BinaryAttributeType.class,
    DatetimeAttributeType.class,
    TimeAttributeType.class,
    EnumAttributeType.class,
    TimedurationAttributeType.class,
    DateAttributeType.class,
    NumberAttributeType.class,
    BaseAttributeWithTargetType.class,
    XmlAttributeType.class,
    GuidAttributeType.class,
    IndentityAttributeType.class,
    MultiLanguageBaseAttributeType.class
})
public class BaseAttributeType
    extends BaseType
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Description", required = true)
    protected String description;
    @XmlElement(name = "IsNullable")
    protected boolean isNullable;
    @XmlElement(name = "Restriction")
    protected RestrictionType restriction;
    @XmlAttribute(name = "UseLazyInitialization")
    protected Boolean useLazyInitialization;
    @XmlAttribute(name = "List")
    protected Boolean list;

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the isNullable property.
     * 
     */
    public boolean isIsNullable() {
        return isNullable;
    }

    /**
     * Sets the value of the isNullable property.
     * 
     */
    public void setIsNullable(boolean value) {
        this.isNullable = value;
    }

    /**
     * Gets the value of the restriction property.
     * 
     * @return
     *     possible object is
     *     {@link RestrictionType }
     *     
     */
    public RestrictionType getRestriction() {
        return restriction;
    }

    /**
     * Sets the value of the restriction property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestrictionType }
     *     
     */
    public void setRestriction(RestrictionType value) {
        this.restriction = value;
    }

    /**
     * Gets the value of the useLazyInitialization property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isUseLazyInitialization() {
        if (useLazyInitialization == null) {
            return false;
        } else {
            return useLazyInitialization;
        }
    }

    /**
     * Sets the value of the useLazyInitialization property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseLazyInitialization(Boolean value) {
        this.useLazyInitialization = value;
    }

    /**
     * Gets the value of the list property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isList() {
        if (list == null) {
            return false;
        } else {
            return list;
        }
    }

    /**
     * Sets the value of the list property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setList(Boolean value) {
        this.list = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof BaseAttributeType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final BaseAttributeType that = ((BaseAttributeType) object);
        {
            String lhsDescription;
            lhsDescription = this.getDescription();
            String rhsDescription;
            rhsDescription = that.getDescription();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "description", lhsDescription), LocatorUtils.property(thatLocator, "description", rhsDescription), lhsDescription, rhsDescription)) {
                return false;
            }
        }
        {
            boolean lhsIsNullable;
            lhsIsNullable = this.isIsNullable();
            boolean rhsIsNullable;
            rhsIsNullable = that.isIsNullable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "isNullable", lhsIsNullable), LocatorUtils.property(thatLocator, "isNullable", rhsIsNullable), lhsIsNullable, rhsIsNullable)) {
                return false;
            }
        }
        {
            RestrictionType lhsRestriction;
            lhsRestriction = this.getRestriction();
            RestrictionType rhsRestriction;
            rhsRestriction = that.getRestriction();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "restriction", lhsRestriction), LocatorUtils.property(thatLocator, "restriction", rhsRestriction), lhsRestriction, rhsRestriction)) {
                return false;
            }
        }
        {
            boolean lhsUseLazyInitialization;
            lhsUseLazyInitialization = this.isUseLazyInitialization();
            boolean rhsUseLazyInitialization;
            rhsUseLazyInitialization = that.isUseLazyInitialization();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "useLazyInitialization", lhsUseLazyInitialization), LocatorUtils.property(thatLocator, "useLazyInitialization", rhsUseLazyInitialization), lhsUseLazyInitialization, rhsUseLazyInitialization)) {
                return false;
            }
        }
        {
            boolean lhsList;
            lhsList = this.isList();
            boolean rhsList;
            rhsList = that.isList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "list", lhsList), LocatorUtils.property(thatLocator, "list", rhsList), lhsList, rhsList)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof BaseAttributeType) {
            final BaseAttributeType copy = ((BaseAttributeType) draftCopy);
            if (this.description!= null) {
                String sourceDescription;
                sourceDescription = this.getDescription();
                String copyDescription = ((String) strategy.copy(LocatorUtils.property(locator, "description", sourceDescription), sourceDescription));
                copy.setDescription(copyDescription);
            } else {
                copy.description = null;
            }
            boolean sourceIsNullable;
            sourceIsNullable = this.isIsNullable();
            boolean copyIsNullable = strategy.copy(LocatorUtils.property(locator, "isNullable", sourceIsNullable), sourceIsNullable);
            copy.setIsNullable(copyIsNullable);
            if (this.restriction!= null) {
                RestrictionType sourceRestriction;
                sourceRestriction = this.getRestriction();
                RestrictionType copyRestriction = ((RestrictionType) strategy.copy(LocatorUtils.property(locator, "restriction", sourceRestriction), sourceRestriction));
                copy.setRestriction(copyRestriction);
            } else {
                copy.restriction = null;
            }
            if (this.useLazyInitialization!= null) {
                boolean sourceUseLazyInitialization;
                sourceUseLazyInitialization = this.isUseLazyInitialization();
                boolean copyUseLazyInitialization = strategy.copy(LocatorUtils.property(locator, "useLazyInitialization", sourceUseLazyInitialization), sourceUseLazyInitialization);
                copy.setUseLazyInitialization(copyUseLazyInitialization);
            } else {
                copy.useLazyInitialization = null;
            }
            if (this.list!= null) {
                boolean sourceList;
                sourceList = this.isList();
                boolean copyList = strategy.copy(LocatorUtils.property(locator, "list", sourceList), sourceList);
                copy.setList(copyList);
            } else {
                copy.list = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new BaseAttributeType();
    }
    
//--simple--preserve
    
    
   private transient Boolean auditAttribute = false;
   private transient com.mda.engine.models.dataModel.TableColumn matchingTableColumn;
   private transient String attributeForTargetIdName = null;
   private transient String attributeForTargetOriginalName = null;
   private transient ObjectType targetIdObject;
    
	public void setAuditAttribute(Boolean auditAttribute) {
		this.auditAttribute = auditAttribute;
	}

	public Boolean isAuditAttribute() {
		return auditAttribute;
	}
	
	public String getClassName() {
		return com.mda.engine.utils.CodeHelper.getClassName(this);
	}

	public String getDataContractClassName() {
		return com.mda.engine.utils.CodeHelper.getClassName(this, true, false, com.mda.engine.core.ApplicationScope.ServicesLayer);
	}
	
	public String getSingleClassName() {
		return com.mda.engine.utils.CodeHelper.getClassName(this, true, true, com.mda.engine.core.ApplicationScope.EntitiesLayer);
	}
	
	public void setMatchingTableColumn(com.mda.engine.models.dataModel.TableColumn matchingTableColumn) {
		this.matchingTableColumn = matchingTableColumn;
	}

	public com.mda.engine.models.dataModel.TableColumn getMatchingTableColumn() {
		return matchingTableColumn;
	}

	public String getAttributeForTargetIdName() {
		return attributeForTargetIdName;
	}

	public void setAttributeForTargetIdName(String attributeForTargetIdName) {
		this.attributeForTargetIdName = attributeForTargetIdName;
	}

	public ObjectType getTargetIdObject() {
		return targetIdObject;
	}

	public void setTargetIdObject(ObjectType targetIdObject) {
		this.targetIdObject = targetIdObject;
	}

	public String getAttributeForTargetOriginalName() {
		return attributeForTargetOriginalName;
	}

	public void setAttributeForTargetOriginalName(String attributeForTargetOriginalName) {
		this.attributeForTargetOriginalName = attributeForTargetOriginalName;
	}
    
//--simple--preserve

}
