
package com.mda.engine.models.businessObjectModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for Index complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Index">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Attributes" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}IndexAttributeCollection" minOccurs="0"/>
 *         &lt;element name="Includes" type="{http://www.mdaengine.com/mdaengine/models/businessObjectModel}IncludeAttributeCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Unique" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="FillFactor" type="{http://www.w3.org/2001/XMLSchema}int" default="100" />
 *       &lt;attribute name="FilterClause" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="AllowPageLocks" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *       &lt;attribute name="AllowRowLocks" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Index", propOrder = {
    "attributes",
    "includes"
})
public class Index
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Attributes")
    protected IndexAttributeCollection attributes;
    @XmlElement(name = "Includes")
    protected IncludeAttributeCollection includes;
    @XmlAttribute(name = "Name", required = true)
    protected String name;
    @XmlAttribute(name = "Unique")
    protected Boolean unique;
    @XmlAttribute(name = "FillFactor")
    protected Integer fillFactor;
    @XmlAttribute(name = "FilterClause")
    protected String filterClause;
    @XmlAttribute(name = "AllowPageLocks")
    protected Boolean allowPageLocks;
    @XmlAttribute(name = "AllowRowLocks")
    protected Boolean allowRowLocks;

    /**
     * Gets the value of the attributes property.
     * 
     * @return
     *     possible object is
     *     {@link IndexAttributeCollection }
     *     
     */
    public IndexAttributeCollection getAttributes() {
        return attributes;
    }

    /**
     * Sets the value of the attributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link IndexAttributeCollection }
     *     
     */
    public void setAttributes(IndexAttributeCollection value) {
        this.attributes = value;
    }

    /**
     * Gets the value of the includes property.
     * 
     * @return
     *     possible object is
     *     {@link IncludeAttributeCollection }
     *     
     */
    public IncludeAttributeCollection getIncludes() {
        return includes;
    }

    /**
     * Sets the value of the includes property.
     * 
     * @param value
     *     allowed object is
     *     {@link IncludeAttributeCollection }
     *     
     */
    public void setIncludes(IncludeAttributeCollection value) {
        this.includes = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the unique property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isUnique() {
        if (unique == null) {
            return false;
        } else {
            return unique;
        }
    }

    /**
     * Sets the value of the unique property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUnique(Boolean value) {
        this.unique = value;
    }

    /**
     * Gets the value of the fillFactor property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public int getFillFactor() {
        if (fillFactor == null) {
            return  100;
        } else {
            return fillFactor;
        }
    }

    /**
     * Sets the value of the fillFactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFillFactor(Integer value) {
        this.fillFactor = value;
    }

    /**
     * Gets the value of the filterClause property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFilterClause() {
        return filterClause;
    }

    /**
     * Sets the value of the filterClause property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFilterClause(String value) {
        this.filterClause = value;
    }

    /**
     * Gets the value of the allowPageLocks property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isAllowPageLocks() {
        if (allowPageLocks == null) {
            return true;
        } else {
            return allowPageLocks;
        }
    }

    /**
     * Sets the value of the allowPageLocks property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowPageLocks(Boolean value) {
        this.allowPageLocks = value;
    }

    /**
     * Gets the value of the allowRowLocks property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isAllowRowLocks() {
        if (allowRowLocks == null) {
            return true;
        } else {
            return allowRowLocks;
        }
    }

    /**
     * Sets the value of the allowRowLocks property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowRowLocks(Boolean value) {
        this.allowRowLocks = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Index)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Index that = ((Index) object);
        {
            IndexAttributeCollection lhsAttributes;
            lhsAttributes = this.getAttributes();
            IndexAttributeCollection rhsAttributes;
            rhsAttributes = that.getAttributes();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "attributes", lhsAttributes), LocatorUtils.property(thatLocator, "attributes", rhsAttributes), lhsAttributes, rhsAttributes)) {
                return false;
            }
        }
        {
            IncludeAttributeCollection lhsIncludes;
            lhsIncludes = this.getIncludes();
            IncludeAttributeCollection rhsIncludes;
            rhsIncludes = that.getIncludes();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "includes", lhsIncludes), LocatorUtils.property(thatLocator, "includes", rhsIncludes), lhsIncludes, rhsIncludes)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            boolean lhsUnique;
            lhsUnique = this.isUnique();
            boolean rhsUnique;
            rhsUnique = that.isUnique();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "unique", lhsUnique), LocatorUtils.property(thatLocator, "unique", rhsUnique), lhsUnique, rhsUnique)) {
                return false;
            }
        }
        {
            int lhsFillFactor;
            lhsFillFactor = this.getFillFactor();
            int rhsFillFactor;
            rhsFillFactor = that.getFillFactor();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "fillFactor", lhsFillFactor), LocatorUtils.property(thatLocator, "fillFactor", rhsFillFactor), lhsFillFactor, rhsFillFactor)) {
                return false;
            }
        }
        {
            String lhsFilterClause;
            lhsFilterClause = this.getFilterClause();
            String rhsFilterClause;
            rhsFilterClause = that.getFilterClause();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "filterClause", lhsFilterClause), LocatorUtils.property(thatLocator, "filterClause", rhsFilterClause), lhsFilterClause, rhsFilterClause)) {
                return false;
            }
        }
        {
            boolean lhsAllowPageLocks;
            lhsAllowPageLocks = this.isAllowPageLocks();
            boolean rhsAllowPageLocks;
            rhsAllowPageLocks = that.isAllowPageLocks();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "allowPageLocks", lhsAllowPageLocks), LocatorUtils.property(thatLocator, "allowPageLocks", rhsAllowPageLocks), lhsAllowPageLocks, rhsAllowPageLocks)) {
                return false;
            }
        }
        {
            boolean lhsAllowRowLocks;
            lhsAllowRowLocks = this.isAllowRowLocks();
            boolean rhsAllowRowLocks;
            rhsAllowRowLocks = that.isAllowRowLocks();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "allowRowLocks", lhsAllowRowLocks), LocatorUtils.property(thatLocator, "allowRowLocks", rhsAllowRowLocks), lhsAllowRowLocks, rhsAllowRowLocks)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Index) {
            final Index copy = ((Index) draftCopy);
            if (this.attributes!= null) {
                IndexAttributeCollection sourceAttributes;
                sourceAttributes = this.getAttributes();
                IndexAttributeCollection copyAttributes = ((IndexAttributeCollection) strategy.copy(LocatorUtils.property(locator, "attributes", sourceAttributes), sourceAttributes));
                copy.setAttributes(copyAttributes);
            } else {
                copy.attributes = null;
            }
            if (this.includes!= null) {
                IncludeAttributeCollection sourceIncludes;
                sourceIncludes = this.getIncludes();
                IncludeAttributeCollection copyIncludes = ((IncludeAttributeCollection) strategy.copy(LocatorUtils.property(locator, "includes", sourceIncludes), sourceIncludes));
                copy.setIncludes(copyIncludes);
            } else {
                copy.includes = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.unique!= null) {
                boolean sourceUnique;
                sourceUnique = this.isUnique();
                boolean copyUnique = strategy.copy(LocatorUtils.property(locator, "unique", sourceUnique), sourceUnique);
                copy.setUnique(copyUnique);
            } else {
                copy.unique = null;
            }
            if (this.fillFactor!= null) {
                int sourceFillFactor;
                sourceFillFactor = this.getFillFactor();
                int copyFillFactor = strategy.copy(LocatorUtils.property(locator, "fillFactor", sourceFillFactor), sourceFillFactor);
                copy.setFillFactor(copyFillFactor);
            } else {
                copy.fillFactor = null;
            }
            if (this.filterClause!= null) {
                String sourceFilterClause;
                sourceFilterClause = this.getFilterClause();
                String copyFilterClause = ((String) strategy.copy(LocatorUtils.property(locator, "filterClause", sourceFilterClause), sourceFilterClause));
                copy.setFilterClause(copyFilterClause);
            } else {
                copy.filterClause = null;
            }
            if (this.allowPageLocks!= null) {
                boolean sourceAllowPageLocks;
                sourceAllowPageLocks = this.isAllowPageLocks();
                boolean copyAllowPageLocks = strategy.copy(LocatorUtils.property(locator, "allowPageLocks", sourceAllowPageLocks), sourceAllowPageLocks);
                copy.setAllowPageLocks(copyAllowPageLocks);
            } else {
                copy.allowPageLocks = null;
            }
            if (this.allowRowLocks!= null) {
                boolean sourceAllowRowLocks;
                sourceAllowRowLocks = this.isAllowRowLocks();
                boolean copyAllowRowLocks = strategy.copy(LocatorUtils.property(locator, "allowRowLocks", sourceAllowRowLocks), sourceAllowRowLocks);
                copy.setAllowRowLocks(copyAllowRowLocks);
            } else {
                copy.allowRowLocks = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Index();
    }

}
