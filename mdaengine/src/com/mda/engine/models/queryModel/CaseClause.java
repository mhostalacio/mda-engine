
package com.mda.engine.models.queryModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for CaseClause complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CaseClause">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/queryModel}SelectAttributesBase">
 *       &lt;sequence>
 *         &lt;element name="When" type="{http://www.mdaengine.com/mdaengine/models/queryModel}CaseWhendClause" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Else" type="{http://www.mdaengine.com/mdaengine/models/queryModel}SelectColumnReference" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="ResultAlias" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CaseClause", propOrder = {
    "when",
    "_else"
})
public class CaseClause
    extends SelectAttributesBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "When")
    protected List<CaseWhendClause> when;
    @XmlElement(name = "Else")
    protected SelectColumnReference _else;
    @XmlAttribute(name = "ResultAlias")
    protected String resultAlias;

    /**
     * Gets the value of the when property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the when property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWhen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CaseWhendClause }
     * 
     * 
     */
    public List<CaseWhendClause> getWhen() {
        if (when == null) {
            when = new ArrayList<CaseWhendClause>();
        }
        return this.when;
    }

    /**
     * Gets the value of the else property.
     * 
     * @return
     *     possible object is
     *     {@link SelectColumnReference }
     *     
     */
    public SelectColumnReference getElse() {
        return _else;
    }

    /**
     * Sets the value of the else property.
     * 
     * @param value
     *     allowed object is
     *     {@link SelectColumnReference }
     *     
     */
    public void setElse(SelectColumnReference value) {
        this._else = value;
    }

    /**
     * Gets the value of the resultAlias property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResultAlias() {
        return resultAlias;
    }

    /**
     * Sets the value of the resultAlias property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResultAlias(String value) {
        this.resultAlias = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof CaseClause)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final CaseClause that = ((CaseClause) object);
        {
            List<CaseWhendClause> lhsWhen;
            lhsWhen = this.getWhen();
            List<CaseWhendClause> rhsWhen;
            rhsWhen = that.getWhen();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "when", lhsWhen), LocatorUtils.property(thatLocator, "when", rhsWhen), lhsWhen, rhsWhen)) {
                return false;
            }
        }
        {
            SelectColumnReference lhsElse;
            lhsElse = this.getElse();
            SelectColumnReference rhsElse;
            rhsElse = that.getElse();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "_else", lhsElse), LocatorUtils.property(thatLocator, "_else", rhsElse), lhsElse, rhsElse)) {
                return false;
            }
        }
        {
            String lhsResultAlias;
            lhsResultAlias = this.getResultAlias();
            String rhsResultAlias;
            rhsResultAlias = that.getResultAlias();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "resultAlias", lhsResultAlias), LocatorUtils.property(thatLocator, "resultAlias", rhsResultAlias), lhsResultAlias, rhsResultAlias)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof CaseClause) {
            final CaseClause copy = ((CaseClause) draftCopy);
            if ((this.when!= null)&&(!this.when.isEmpty())) {
                List<CaseWhendClause> sourceWhen;
                sourceWhen = this.getWhen();
                @SuppressWarnings("unchecked")
                List<CaseWhendClause> copyWhen = ((List<CaseWhendClause> ) strategy.copy(LocatorUtils.property(locator, "when", sourceWhen), sourceWhen));
                copy.when = null;
                List<CaseWhendClause> uniqueWhenl = copy.getWhen();
                uniqueWhenl.addAll(copyWhen);
            } else {
                copy.when = null;
            }
            if (this._else!= null) {
                SelectColumnReference sourceElse;
                sourceElse = this.getElse();
                SelectColumnReference copyElse = ((SelectColumnReference) strategy.copy(LocatorUtils.property(locator, "_else", sourceElse), sourceElse));
                copy.setElse(copyElse);
            } else {
                copy._else = null;
            }
            if (this.resultAlias!= null) {
                String sourceResultAlias;
                sourceResultAlias = this.getResultAlias();
                String copyResultAlias = ((String) strategy.copy(LocatorUtils.property(locator, "resultAlias", sourceResultAlias), sourceResultAlias));
                copy.setResultAlias(copyResultAlias);
            } else {
                copy.resultAlias = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new CaseClause();
    }
    
//--simple--preserve
    
  	@Override
  	public void writeSelection(com.mda.engine.utils.Stringcode c) {
		
		c.line("CASE ");
		c.tokenAdd2BeginOfLine("	");
		if (this.getWhen() != null && this.getWhen().size() > 0)
		{
			for(CaseWhendClause when : this.getWhen())
			{
				when.writeSelection(c);
			}
		}
		if (this.getElse() != null)
		{
			c.line("ELSE ");
			c.tokenAdd2BeginOfLine("	");
			this.getElse().setAddComa(false);
			this.getElse().writeSelection(c);
			c.tokenRemove2BeginOfLine();
		}
		c.tokenRemove2BeginOfLine();
		c.line("END {0},", getResultAlias() != null  ? ("AS " + getResultAlias()) : "");
  	}   
    	
//--simple--preserve

}
