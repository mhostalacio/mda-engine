
package com.mda.engine.models.queryModel;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderByDirectionEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OrderByDirectionEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ASC"/>
 *     &lt;enumeration value="DESC"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "OrderByDirectionEnum")
@XmlEnum
public enum OrderByDirectionEnum {

    ASC,
    DESC;

    public String value() {
        return name();
    }

    public static OrderByDirectionEnum fromValue(String v) {
        return valueOf(v);
    }

}
