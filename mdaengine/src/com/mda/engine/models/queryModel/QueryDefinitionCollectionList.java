
package com.mda.engine.models.queryModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for QueryDefinitionCollectionList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueryDefinitionCollectionList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;element name="Select" type="{http://www.mdaengine.com/mdaengine/models/queryModel}QueryDefinitionSelect"/>
 *         &lt;element name="Union" type="{http://www.mdaengine.com/mdaengine/models/queryModel}QueryDefinitionCollectionList"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryDefinitionCollectionList", propOrder = {
    "selectOrUnion"
})
public class QueryDefinitionCollectionList
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElements({
        @XmlElement(name = "Select", type = QueryDefinitionSelect.class),
        @XmlElement(name = "Union", type = QueryDefinitionCollectionList.class)
    })
    protected List<ModelNodeBase> selectOrUnion;

    /**
     * Gets the value of the selectOrUnion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the selectOrUnion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSelectOrUnion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QueryDefinitionSelect }
     * {@link QueryDefinitionCollectionList }
     * 
     * 
     */
    public List<ModelNodeBase> getSelectOrUnion() {
        if (selectOrUnion == null) {
            selectOrUnion = new ArrayList<ModelNodeBase>();
        }
        return this.selectOrUnion;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof QueryDefinitionCollectionList)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final QueryDefinitionCollectionList that = ((QueryDefinitionCollectionList) object);
        {
            List<ModelNodeBase> lhsSelectOrUnion;
            lhsSelectOrUnion = this.getSelectOrUnion();
            List<ModelNodeBase> rhsSelectOrUnion;
            rhsSelectOrUnion = that.getSelectOrUnion();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "selectOrUnion", lhsSelectOrUnion), LocatorUtils.property(thatLocator, "selectOrUnion", rhsSelectOrUnion), lhsSelectOrUnion, rhsSelectOrUnion)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof QueryDefinitionCollectionList) {
            final QueryDefinitionCollectionList copy = ((QueryDefinitionCollectionList) draftCopy);
            if ((this.selectOrUnion!= null)&&(!this.selectOrUnion.isEmpty())) {
                List<ModelNodeBase> sourceSelectOrUnion;
                sourceSelectOrUnion = this.getSelectOrUnion();
                @SuppressWarnings("unchecked")
                List<ModelNodeBase> copySelectOrUnion = ((List<ModelNodeBase> ) strategy.copy(LocatorUtils.property(locator, "selectOrUnion", sourceSelectOrUnion), sourceSelectOrUnion));
                copy.selectOrUnion = null;
                List<ModelNodeBase> uniqueSelectOrUnionl = copy.getSelectOrUnion();
                uniqueSelectOrUnionl.addAll(copySelectOrUnion);
            } else {
                copy.selectOrUnion = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new QueryDefinitionCollectionList();
    }

}
