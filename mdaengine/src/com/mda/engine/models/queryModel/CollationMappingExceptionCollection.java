
package com.mda.engine.models.queryModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for CollationMappingExceptionCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CollationMappingExceptionCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="Exception" type="{http://www.mdaengine.com/mdaengine/models/queryModel}CollationMappingException" maxOccurs="unbounded"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CollationMappingExceptionCollection", propOrder = {
    "exception"
})
public class CollationMappingExceptionCollection
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Exception")
    protected List<CollationMappingException> exception;

    /**
     * Gets the value of the exception property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the exception property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getException().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CollationMappingException }
     * 
     * 
     */
    public List<CollationMappingException> getException() {
        if (exception == null) {
            exception = new ArrayList<CollationMappingException>();
        }
        return this.exception;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof CollationMappingExceptionCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final CollationMappingExceptionCollection that = ((CollationMappingExceptionCollection) object);
        {
            List<CollationMappingException> lhsException;
            lhsException = this.getException();
            List<CollationMappingException> rhsException;
            rhsException = that.getException();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "exception", lhsException), LocatorUtils.property(thatLocator, "exception", rhsException), lhsException, rhsException)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof CollationMappingExceptionCollection) {
            final CollationMappingExceptionCollection copy = ((CollationMappingExceptionCollection) draftCopy);
            if ((this.exception!= null)&&(!this.exception.isEmpty())) {
                List<CollationMappingException> sourceException;
                sourceException = this.getException();
                @SuppressWarnings("unchecked")
                List<CollationMappingException> copyException = ((List<CollationMappingException> ) strategy.copy(LocatorUtils.property(locator, "exception", sourceException), sourceException));
                copy.exception = null;
                List<CollationMappingException> uniqueExceptionl = copy.getException();
                uniqueExceptionl.addAll(copyException);
            } else {
                copy.exception = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new CollationMappingExceptionCollection();
    }

}
