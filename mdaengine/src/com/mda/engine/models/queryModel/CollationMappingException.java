
package com.mda.engine.models.queryModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for CollationMappingException complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CollationMappingException">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Langs" type="{http://www.mdaengine.com/mdaengine/models/queryModel}CollationLangsCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="UniqueName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Collation" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CollationMappingException", propOrder = {
    "langs"
})
public class CollationMappingException
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Langs")
    protected CollationLangsCollection langs;
    @XmlAttribute(name = "UniqueName", required = true)
    protected String uniqueName;
    @XmlAttribute(name = "Collation", required = true)
    protected String collation;

    /**
     * Gets the value of the langs property.
     * 
     * @return
     *     possible object is
     *     {@link CollationLangsCollection }
     *     
     */
    public CollationLangsCollection getLangs() {
        return langs;
    }

    /**
     * Sets the value of the langs property.
     * 
     * @param value
     *     allowed object is
     *     {@link CollationLangsCollection }
     *     
     */
    public void setLangs(CollationLangsCollection value) {
        this.langs = value;
    }

    /**
     * Gets the value of the uniqueName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUniqueName() {
        return uniqueName;
    }

    /**
     * Sets the value of the uniqueName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUniqueName(String value) {
        this.uniqueName = value;
    }

    /**
     * Gets the value of the collation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCollation() {
        return collation;
    }

    /**
     * Sets the value of the collation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCollation(String value) {
        this.collation = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof CollationMappingException)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final CollationMappingException that = ((CollationMappingException) object);
        {
            CollationLangsCollection lhsLangs;
            lhsLangs = this.getLangs();
            CollationLangsCollection rhsLangs;
            rhsLangs = that.getLangs();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "langs", lhsLangs), LocatorUtils.property(thatLocator, "langs", rhsLangs), lhsLangs, rhsLangs)) {
                return false;
            }
        }
        {
            String lhsUniqueName;
            lhsUniqueName = this.getUniqueName();
            String rhsUniqueName;
            rhsUniqueName = that.getUniqueName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "uniqueName", lhsUniqueName), LocatorUtils.property(thatLocator, "uniqueName", rhsUniqueName), lhsUniqueName, rhsUniqueName)) {
                return false;
            }
        }
        {
            String lhsCollation;
            lhsCollation = this.getCollation();
            String rhsCollation;
            rhsCollation = that.getCollation();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "collation", lhsCollation), LocatorUtils.property(thatLocator, "collation", rhsCollation), lhsCollation, rhsCollation)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof CollationMappingException) {
            final CollationMappingException copy = ((CollationMappingException) draftCopy);
            if (this.langs!= null) {
                CollationLangsCollection sourceLangs;
                sourceLangs = this.getLangs();
                CollationLangsCollection copyLangs = ((CollationLangsCollection) strategy.copy(LocatorUtils.property(locator, "langs", sourceLangs), sourceLangs));
                copy.setLangs(copyLangs);
            } else {
                copy.langs = null;
            }
            if (this.uniqueName!= null) {
                String sourceUniqueName;
                sourceUniqueName = this.getUniqueName();
                String copyUniqueName = ((String) strategy.copy(LocatorUtils.property(locator, "uniqueName", sourceUniqueName), sourceUniqueName));
                copy.setUniqueName(copyUniqueName);
            } else {
                copy.uniqueName = null;
            }
            if (this.collation!= null) {
                String sourceCollation;
                sourceCollation = this.getCollation();
                String copyCollation = ((String) strategy.copy(LocatorUtils.property(locator, "collation", sourceCollation), sourceCollation));
                copy.setCollation(copyCollation);
            } else {
                copy.collation = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new CollationMappingException();
    }

}
