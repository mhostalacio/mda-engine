
package com.mda.engine.models.queryModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for BaseJoinClause complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BaseJoinClause">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="With" type="{http://www.mdaengine.com/mdaengine/models/queryModel}WithClause"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BaseJoinClause", propOrder = {
    "with"
})
@XmlSeeAlso({
    LeftJoinClause.class,
    RightJoinClause.class,
    InnerJoinClause.class
})
public class BaseJoinClause
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "With", required = true)
    protected WithClause with;

    /**
     * Gets the value of the with property.
     * 
     * @return
     *     possible object is
     *     {@link WithClause }
     *     
     */
    public WithClause getWith() {
        return with;
    }

    /**
     * Sets the value of the with property.
     * 
     * @param value
     *     allowed object is
     *     {@link WithClause }
     *     
     */
    public void setWith(WithClause value) {
        this.with = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof BaseJoinClause)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final BaseJoinClause that = ((BaseJoinClause) object);
        {
            WithClause lhsWith;
            lhsWith = this.getWith();
            WithClause rhsWith;
            rhsWith = that.getWith();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "with", lhsWith), LocatorUtils.property(thatLocator, "with", rhsWith), lhsWith, rhsWith)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof BaseJoinClause) {
            final BaseJoinClause copy = ((BaseJoinClause) draftCopy);
            if (this.with!= null) {
                WithClause sourceWith;
                sourceWith = this.getWith();
                WithClause copyWith = ((WithClause) strategy.copy(LocatorUtils.property(locator, "with", sourceWith), sourceWith));
                copy.setWith(copyWith);
            } else {
                copy.with = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new BaseJoinClause();
    }

}
