
package com.mda.engine.models.queryModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for SelectAttributesBase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SelectAttributesBase">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="TableAlias" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SelectAttributesBase")
@XmlSeeAlso({
    CaseClause.class,
    SelectAllReference.class,
    SelectCount.class,
    SelectColumnReference.class
})
public abstract class SelectAttributesBase
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "TableAlias")
    protected String tableAlias;

    /**
     * Gets the value of the tableAlias property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTableAlias() {
        return tableAlias;
    }

    /**
     * Sets the value of the tableAlias property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTableAlias(String value) {
        this.tableAlias = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof SelectAttributesBase)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final SelectAttributesBase that = ((SelectAttributesBase) object);
        {
            String lhsTableAlias;
            lhsTableAlias = this.getTableAlias();
            String rhsTableAlias;
            rhsTableAlias = that.getTableAlias();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "tableAlias", lhsTableAlias), LocatorUtils.property(thatLocator, "tableAlias", rhsTableAlias), lhsTableAlias, rhsTableAlias)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        if (null == target) {
            throw new IllegalArgumentException("Target argument must not be null for abstract copyable classes.");
        }
        if (target instanceof SelectAttributesBase) {
            final SelectAttributesBase copy = ((SelectAttributesBase) target);
            if (this.tableAlias!= null) {
                String sourceTableAlias;
                sourceTableAlias = this.getTableAlias();
                String copyTableAlias = ((String) strategy.copy(LocatorUtils.property(locator, "tableAlias", sourceTableAlias), sourceTableAlias));
                copy.setTableAlias(copyTableAlias);
            } else {
                copy.tableAlias = null;
            }
        }
        return target;
    }
    
//--simple--preserve
    
	public abstract void writeSelection(com.mda.engine.utils.Stringcode c);   
	
//--simple--preserve

}
