
package com.mda.engine.models.queryModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for QueryParametersReference complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueryParametersReference">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="QueryName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryParametersReference")
public class QueryParametersReference
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "QueryName")
    protected String queryName;

    /**
     * Gets the value of the queryName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQueryName() {
        return queryName;
    }

    /**
     * Sets the value of the queryName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQueryName(String value) {
        this.queryName = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof QueryParametersReference)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final QueryParametersReference that = ((QueryParametersReference) object);
        {
            String lhsQueryName;
            lhsQueryName = this.getQueryName();
            String rhsQueryName;
            rhsQueryName = that.getQueryName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "queryName", lhsQueryName), LocatorUtils.property(thatLocator, "queryName", rhsQueryName), lhsQueryName, rhsQueryName)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof QueryParametersReference) {
            final QueryParametersReference copy = ((QueryParametersReference) draftCopy);
            if (this.queryName!= null) {
                String sourceQueryName;
                sourceQueryName = this.getQueryName();
                String copyQueryName = ((String) strategy.copy(LocatorUtils.property(locator, "queryName", sourceQueryName), sourceQueryName));
                copy.setQueryName(copyQueryName);
            } else {
                copy.queryName = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new QueryParametersReference();
    }
    
//--simple--preserve
    
    
    private transient com.mda.engine.models.queryModel.Query referencedQuery;

	public com.mda.engine.models.queryModel.Query getReferencedQuery() {
		return referencedQuery;
	}

	public void setReferencedQuery(
			com.mda.engine.models.queryModel.Query referencedQuery) {
		this.referencedQuery = referencedQuery;
	}
   
	
//--simple--preserve

}
