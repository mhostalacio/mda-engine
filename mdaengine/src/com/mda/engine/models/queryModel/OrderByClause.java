
package com.mda.engine.models.queryModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for OrderByClause complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderByClause">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;element name="Column" type="{http://www.mdaengine.com/mdaengine/models/queryModel}SelectColumnOrderByReference" maxOccurs="unbounded"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderByClause", propOrder = {
    "columns"
})
public class OrderByClause
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Column")
    protected List<SelectColumnOrderByReference> columns;

    /**
     * Gets the value of the columns property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the columns property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getColumns().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SelectColumnOrderByReference }
     * 
     * 
     */
    public List<SelectColumnOrderByReference> getColumns() {
        if (columns == null) {
            columns = new ArrayList<SelectColumnOrderByReference>();
        }
        return this.columns;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof OrderByClause)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final OrderByClause that = ((OrderByClause) object);
        {
            List<SelectColumnOrderByReference> lhsColumns;
            lhsColumns = this.getColumns();
            List<SelectColumnOrderByReference> rhsColumns;
            rhsColumns = that.getColumns();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "columns", lhsColumns), LocatorUtils.property(thatLocator, "columns", rhsColumns), lhsColumns, rhsColumns)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof OrderByClause) {
            final OrderByClause copy = ((OrderByClause) draftCopy);
            if ((this.columns!= null)&&(!this.columns.isEmpty())) {
                List<SelectColumnOrderByReference> sourceColumns;
                sourceColumns = this.getColumns();
                @SuppressWarnings("unchecked")
                List<SelectColumnOrderByReference> copyColumns = ((List<SelectColumnOrderByReference> ) strategy.copy(LocatorUtils.property(locator, "columns", sourceColumns), sourceColumns));
                copy.columns = null;
                List<SelectColumnOrderByReference> uniqueColumnsl = copy.getColumns();
                uniqueColumnsl.addAll(copyColumns);
            } else {
                copy.columns = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new OrderByClause();
    }

}
