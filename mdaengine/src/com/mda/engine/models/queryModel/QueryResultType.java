
package com.mda.engine.models.queryModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import com.mda.engine.models.common.BOMReferenceType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for QueryResultType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueryResultType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="Object" type="{http://www.mdaengine.com/mdaengine/models/common}BOMReferenceType"/>
 *         &lt;element name="Custom" type="{http://www.mdaengine.com/mdaengine/models/queryModel}ParameterCollection"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryResultType", propOrder = {
    "object",
    "custom"
})
public class QueryResultType
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Object")
    protected BOMReferenceType object;
    @XmlElement(name = "Custom")
    protected ParameterCollection custom;

    /**
     * Gets the value of the object property.
     * 
     * @return
     *     possible object is
     *     {@link BOMReferenceType }
     *     
     */
    public BOMReferenceType getObject() {
        return object;
    }

    /**
     * Sets the value of the object property.
     * 
     * @param value
     *     allowed object is
     *     {@link BOMReferenceType }
     *     
     */
    public void setObject(BOMReferenceType value) {
        this.object = value;
    }

    /**
     * Gets the value of the custom property.
     * 
     * @return
     *     possible object is
     *     {@link ParameterCollection }
     *     
     */
    public ParameterCollection getCustom() {
        return custom;
    }

    /**
     * Sets the value of the custom property.
     * 
     * @param value
     *     allowed object is
     *     {@link ParameterCollection }
     *     
     */
    public void setCustom(ParameterCollection value) {
        this.custom = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof QueryResultType)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final QueryResultType that = ((QueryResultType) object);
        {
            BOMReferenceType lhsObject;
            lhsObject = this.getObject();
            BOMReferenceType rhsObject;
            rhsObject = that.getObject();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "object", lhsObject), LocatorUtils.property(thatLocator, "object", rhsObject), lhsObject, rhsObject)) {
                return false;
            }
        }
        {
            ParameterCollection lhsCustom;
            lhsCustom = this.getCustom();
            ParameterCollection rhsCustom;
            rhsCustom = that.getCustom();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "custom", lhsCustom), LocatorUtils.property(thatLocator, "custom", rhsCustom), lhsCustom, rhsCustom)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof QueryResultType) {
            final QueryResultType copy = ((QueryResultType) draftCopy);
            if (this.object!= null) {
                BOMReferenceType sourceObject;
                sourceObject = this.getObject();
                BOMReferenceType copyObject = ((BOMReferenceType) strategy.copy(LocatorUtils.property(locator, "object", sourceObject), sourceObject));
                copy.setObject(copyObject);
            } else {
                copy.object = null;
            }
            if (this.custom!= null) {
                ParameterCollection sourceCustom;
                sourceCustom = this.getCustom();
                ParameterCollection copyCustom = ((ParameterCollection) strategy.copy(LocatorUtils.property(locator, "custom", sourceCustom), sourceCustom));
                copy.setCustom(copyCustom);
            } else {
                copy.custom = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new QueryResultType();
    }

}
