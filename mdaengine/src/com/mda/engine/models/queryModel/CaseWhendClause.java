
package com.mda.engine.models.queryModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for CaseWhendClause complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CaseWhendClause">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/queryModel}SelectColumnReference">
 *       &lt;sequence>
 *         &lt;element name="Then" type="{http://www.mdaengine.com/mdaengine/models/queryModel}SelectColumnReference"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CaseWhendClause", propOrder = {
    "then"
})
public class CaseWhendClause
    extends SelectColumnReference
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Then", required = true)
    protected SelectColumnReference then;

    /**
     * Gets the value of the then property.
     * 
     * @return
     *     possible object is
     *     {@link SelectColumnReference }
     *     
     */
    public SelectColumnReference getThen() {
        return then;
    }

    /**
     * Sets the value of the then property.
     * 
     * @param value
     *     allowed object is
     *     {@link SelectColumnReference }
     *     
     */
    public void setThen(SelectColumnReference value) {
        this.then = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof CaseWhendClause)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final CaseWhendClause that = ((CaseWhendClause) object);
        {
            SelectColumnReference lhsThen;
            lhsThen = this.getThen();
            SelectColumnReference rhsThen;
            rhsThen = that.getThen();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "then", lhsThen), LocatorUtils.property(thatLocator, "then", rhsThen), lhsThen, rhsThen)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof CaseWhendClause) {
            final CaseWhendClause copy = ((CaseWhendClause) draftCopy);
            if (this.then!= null) {
                SelectColumnReference sourceThen;
                sourceThen = this.getThen();
                SelectColumnReference copyThen = ((SelectColumnReference) strategy.copy(LocatorUtils.property(locator, "then", sourceThen), sourceThen));
                copy.setThen(copyThen);
            } else {
                copy.then = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new CaseWhendClause();
    }
    
//--simple--preserve
    
  	@Override
  	public void writeSelection(com.mda.engine.utils.Stringcode c) {
		
		c.line("WHEN");
		c.tokenAdd2BeginOfLine("	");
		this.setAddComa(false);
		super.writeSelection(c);
		c.tokenRemove2BeginOfLine();
		c.line("THEN");
		c.tokenAdd2BeginOfLine("	");
		this.getThen().setAddComa(false);
		this.getThen().writeSelection(c);
		c.tokenRemove2BeginOfLine();
  	}   
    	
//--simple--preserve

}
