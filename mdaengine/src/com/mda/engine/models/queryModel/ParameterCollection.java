
package com.mda.engine.models.queryModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import com.mda.engine.models.dataModel.BigIntTableColumn;
import com.mda.engine.models.dataModel.BitTableColumn;
import com.mda.engine.models.dataModel.DataBaseItem;
import com.mda.engine.models.dataModel.DateTimeTableColumn;
import com.mda.engine.models.dataModel.DecimalTableColumn;
import com.mda.engine.models.dataModel.EnumTableColumn;
import com.mda.engine.models.dataModel.FloatTableColumn;
import com.mda.engine.models.dataModel.IntTableColumn;
import com.mda.engine.models.dataModel.NvarcharTableColumn;
import com.mda.engine.models.dataModel.SmallIntTableColumn;
import com.mda.engine.models.dataModel.TinyIntTableColumn;
import com.mda.engine.models.dataModel.UniqueIdentifierTableColumn;
import com.mda.engine.models.dataModel.VarcharTableColumn;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ParameterCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ParameterCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded" minOccurs="0">
 *         &lt;element name="Varchar" type="{http://www.mdaengine.com/mdaengine/models/dataModel}VarcharTableColumn" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Nvarchar" type="{http://www.mdaengine.com/mdaengine/models/dataModel}NvarcharTableColumn" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Datetime" type="{http://www.mdaengine.com/mdaengine/models/dataModel}DateTimeTableColumn" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Int" type="{http://www.mdaengine.com/mdaengine/models/dataModel}IntTableColumn" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TinyInt" type="{http://www.mdaengine.com/mdaengine/models/dataModel}TinyIntTableColumn" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SmallInt" type="{http://www.mdaengine.com/mdaengine/models/dataModel}SmallIntTableColumn" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Float" type="{http://www.mdaengine.com/mdaengine/models/dataModel}FloatTableColumn" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="BigInt" type="{http://www.mdaengine.com/mdaengine/models/dataModel}BigIntTableColumn" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Decimal" type="{http://www.mdaengine.com/mdaengine/models/dataModel}DecimalTableColumn" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Bit" type="{http://www.mdaengine.com/mdaengine/models/dataModel}BitTableColumn" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="UniqueIdentifier" type="{http://www.mdaengine.com/mdaengine/models/dataModel}UniqueIdentifierTableColumn" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="BigIntList" type="{http://www.mdaengine.com/mdaengine/models/queryModel}BigIntList"/>
 *         &lt;element name="BigIntPairList" type="{http://www.mdaengine.com/mdaengine/models/queryModel}BigIntPairList"/>
 *         &lt;element name="StringList" type="{http://www.mdaengine.com/mdaengine/models/queryModel}StringList"/>
 *         &lt;element name="Enum" type="{http://www.mdaengine.com/mdaengine/models/dataModel}EnumTableColumn" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ParameterCollection", propOrder = {
    "columnList"
})
public class ParameterCollection
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElements({
        @XmlElement(name = "UniqueIdentifier", type = UniqueIdentifierTableColumn.class),
        @XmlElement(name = "Enum", type = EnumTableColumn.class),
        @XmlElement(name = "TinyInt", type = TinyIntTableColumn.class),
        @XmlElement(name = "Decimal", type = DecimalTableColumn.class),
        @XmlElement(name = "Bit", type = BitTableColumn.class),
        @XmlElement(name = "StringList", type = StringList.class),
        @XmlElement(name = "SmallInt", type = SmallIntTableColumn.class),
        @XmlElement(name = "Varchar", type = VarcharTableColumn.class),
        @XmlElement(name = "BigIntList", type = BigIntList.class),
        @XmlElement(name = "BigIntPairList", type = BigIntPairList.class),
        @XmlElement(name = "Nvarchar", type = NvarcharTableColumn.class),
        @XmlElement(name = "BigInt", type = BigIntTableColumn.class),
        @XmlElement(name = "Int", type = IntTableColumn.class),
        @XmlElement(name = "Datetime", type = DateTimeTableColumn.class),
        @XmlElement(name = "Float", type = FloatTableColumn.class)
    })
    protected List<DataBaseItem> columnList;

    /**
     * Gets the value of the columnList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the columnList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getColumnList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UniqueIdentifierTableColumn }
     * {@link EnumTableColumn }
     * {@link TinyIntTableColumn }
     * {@link DecimalTableColumn }
     * {@link BitTableColumn }
     * {@link StringList }
     * {@link SmallIntTableColumn }
     * {@link VarcharTableColumn }
     * {@link BigIntList }
     * {@link BigIntPairList }
     * {@link NvarcharTableColumn }
     * {@link BigIntTableColumn }
     * {@link IntTableColumn }
     * {@link DateTimeTableColumn }
     * {@link FloatTableColumn }
     * 
     * 
     */
    public List<DataBaseItem> getColumnList() {
        if (columnList == null) {
            columnList = new ArrayList<DataBaseItem>();
        }
        return this.columnList;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ParameterCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ParameterCollection that = ((ParameterCollection) object);
        {
            List<DataBaseItem> lhsColumnList;
            lhsColumnList = this.getColumnList();
            List<DataBaseItem> rhsColumnList;
            rhsColumnList = that.getColumnList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "columnList", lhsColumnList), LocatorUtils.property(thatLocator, "columnList", rhsColumnList), lhsColumnList, rhsColumnList)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof ParameterCollection) {
            final ParameterCollection copy = ((ParameterCollection) draftCopy);
            if ((this.columnList!= null)&&(!this.columnList.isEmpty())) {
                List<DataBaseItem> sourceColumnList;
                sourceColumnList = this.getColumnList();
                @SuppressWarnings("unchecked")
                List<DataBaseItem> copyColumnList = ((List<DataBaseItem> ) strategy.copy(LocatorUtils.property(locator, "columnList", sourceColumnList), sourceColumnList));
                copy.columnList = null;
                List<DataBaseItem> uniqueColumnListl = copy.getColumnList();
                uniqueColumnListl.addAll(copyColumnList);
            } else {
                copy.columnList = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ParameterCollection();
    }

}
