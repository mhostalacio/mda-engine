
package com.mda.engine.models.queryModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for CollationMappings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CollationMappings">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Exceptions" type="{http://www.mdaengine.com/mdaengine/models/queryModel}CollationMappingExceptionCollection"/>
 *       &lt;/sequence>
 *       &lt;attribute name="DefaultCollation" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CollationMappings", propOrder = {
    "exceptions"
})
public class CollationMappings
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Exceptions", required = true)
    protected CollationMappingExceptionCollection exceptions;
    @XmlAttribute(name = "DefaultCollation", required = true)
    protected String defaultCollation;

    /**
     * Gets the value of the exceptions property.
     * 
     * @return
     *     possible object is
     *     {@link CollationMappingExceptionCollection }
     *     
     */
    public CollationMappingExceptionCollection getExceptions() {
        return exceptions;
    }

    /**
     * Sets the value of the exceptions property.
     * 
     * @param value
     *     allowed object is
     *     {@link CollationMappingExceptionCollection }
     *     
     */
    public void setExceptions(CollationMappingExceptionCollection value) {
        this.exceptions = value;
    }

    /**
     * Gets the value of the defaultCollation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultCollation() {
        return defaultCollation;
    }

    /**
     * Sets the value of the defaultCollation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultCollation(String value) {
        this.defaultCollation = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof CollationMappings)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final CollationMappings that = ((CollationMappings) object);
        {
            CollationMappingExceptionCollection lhsExceptions;
            lhsExceptions = this.getExceptions();
            CollationMappingExceptionCollection rhsExceptions;
            rhsExceptions = that.getExceptions();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "exceptions", lhsExceptions), LocatorUtils.property(thatLocator, "exceptions", rhsExceptions), lhsExceptions, rhsExceptions)) {
                return false;
            }
        }
        {
            String lhsDefaultCollation;
            lhsDefaultCollation = this.getDefaultCollation();
            String rhsDefaultCollation;
            rhsDefaultCollation = that.getDefaultCollation();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "defaultCollation", lhsDefaultCollation), LocatorUtils.property(thatLocator, "defaultCollation", rhsDefaultCollation), lhsDefaultCollation, rhsDefaultCollation)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof CollationMappings) {
            final CollationMappings copy = ((CollationMappings) draftCopy);
            if (this.exceptions!= null) {
                CollationMappingExceptionCollection sourceExceptions;
                sourceExceptions = this.getExceptions();
                CollationMappingExceptionCollection copyExceptions = ((CollationMappingExceptionCollection) strategy.copy(LocatorUtils.property(locator, "exceptions", sourceExceptions), sourceExceptions));
                copy.setExceptions(copyExceptions);
            } else {
                copy.exceptions = null;
            }
            if (this.defaultCollation!= null) {
                String sourceDefaultCollation;
                sourceDefaultCollation = this.getDefaultCollation();
                String copyDefaultCollation = ((String) strategy.copy(LocatorUtils.property(locator, "defaultCollation", sourceDefaultCollation), sourceDefaultCollation));
                copy.setDefaultCollation(copyDefaultCollation);
            } else {
                copy.defaultCollation = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new CollationMappings();
    }

}
