
package com.mda.engine.models.queryModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import com.mda.engine.models.common.BOMReferenceType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for FromClauseChoice complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FromClauseChoice">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="Table" type="{http://www.mdaengine.com/mdaengine/models/common}BOMReferenceType" minOccurs="0"/>
 *           &lt;element name="Query" type="{http://www.mdaengine.com/mdaengine/models/queryModel}QueryDefinitionCollection" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;choice maxOccurs="unbounded">
 *           &lt;element name="InnerJoin" type="{http://www.mdaengine.com/mdaengine/models/queryModel}InnerJoinClause" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element name="LeftJoin" type="{http://www.mdaengine.com/mdaengine/models/queryModel}LeftJoinClause" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element name="RightJoin" type="{http://www.mdaengine.com/mdaengine/models/queryModel}RightJoinClause" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *       &lt;attribute name="Alias" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FromClauseChoice", propOrder = {
    "table",
    "query",
    "clauseList"
})
public class FromClauseChoice
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Table")
    protected BOMReferenceType table;
    @XmlElement(name = "Query")
    protected QueryDefinitionCollection query;
    @XmlElements({
        @XmlElement(name = "RightJoin", type = RightJoinClause.class),
        @XmlElement(name = "LeftJoin", type = LeftJoinClause.class),
        @XmlElement(name = "InnerJoin", type = InnerJoinClause.class)
    })
    protected List<BaseJoinClause> clauseList;
    @XmlAttribute(name = "Alias")
    protected String alias;

    /**
     * Gets the value of the table property.
     * 
     * @return
     *     possible object is
     *     {@link BOMReferenceType }
     *     
     */
    public BOMReferenceType getTable() {
        return table;
    }

    /**
     * Sets the value of the table property.
     * 
     * @param value
     *     allowed object is
     *     {@link BOMReferenceType }
     *     
     */
    public void setTable(BOMReferenceType value) {
        this.table = value;
    }

    /**
     * Gets the value of the query property.
     * 
     * @return
     *     possible object is
     *     {@link QueryDefinitionCollection }
     *     
     */
    public QueryDefinitionCollection getQuery() {
        return query;
    }

    /**
     * Sets the value of the query property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryDefinitionCollection }
     *     
     */
    public void setQuery(QueryDefinitionCollection value) {
        this.query = value;
    }

    /**
     * Gets the value of the clauseList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the clauseList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClauseList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RightJoinClause }
     * {@link LeftJoinClause }
     * {@link InnerJoinClause }
     * 
     * 
     */
    public List<BaseJoinClause> getClauseList() {
        if (clauseList == null) {
            clauseList = new ArrayList<BaseJoinClause>();
        }
        return this.clauseList;
    }

    /**
     * Gets the value of the alias property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlias() {
        return alias;
    }

    /**
     * Sets the value of the alias property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlias(String value) {
        this.alias = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof FromClauseChoice)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final FromClauseChoice that = ((FromClauseChoice) object);
        {
            BOMReferenceType lhsTable;
            lhsTable = this.getTable();
            BOMReferenceType rhsTable;
            rhsTable = that.getTable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "table", lhsTable), LocatorUtils.property(thatLocator, "table", rhsTable), lhsTable, rhsTable)) {
                return false;
            }
        }
        {
            QueryDefinitionCollection lhsQuery;
            lhsQuery = this.getQuery();
            QueryDefinitionCollection rhsQuery;
            rhsQuery = that.getQuery();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "query", lhsQuery), LocatorUtils.property(thatLocator, "query", rhsQuery), lhsQuery, rhsQuery)) {
                return false;
            }
        }
        {
            List<BaseJoinClause> lhsClauseList;
            lhsClauseList = this.getClauseList();
            List<BaseJoinClause> rhsClauseList;
            rhsClauseList = that.getClauseList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "clauseList", lhsClauseList), LocatorUtils.property(thatLocator, "clauseList", rhsClauseList), lhsClauseList, rhsClauseList)) {
                return false;
            }
        }
        {
            String lhsAlias;
            lhsAlias = this.getAlias();
            String rhsAlias;
            rhsAlias = that.getAlias();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "alias", lhsAlias), LocatorUtils.property(thatLocator, "alias", rhsAlias), lhsAlias, rhsAlias)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof FromClauseChoice) {
            final FromClauseChoice copy = ((FromClauseChoice) draftCopy);
            if (this.table!= null) {
                BOMReferenceType sourceTable;
                sourceTable = this.getTable();
                BOMReferenceType copyTable = ((BOMReferenceType) strategy.copy(LocatorUtils.property(locator, "table", sourceTable), sourceTable));
                copy.setTable(copyTable);
            } else {
                copy.table = null;
            }
            if (this.query!= null) {
                QueryDefinitionCollection sourceQuery;
                sourceQuery = this.getQuery();
                QueryDefinitionCollection copyQuery = ((QueryDefinitionCollection) strategy.copy(LocatorUtils.property(locator, "query", sourceQuery), sourceQuery));
                copy.setQuery(copyQuery);
            } else {
                copy.query = null;
            }
            if ((this.clauseList!= null)&&(!this.clauseList.isEmpty())) {
                List<BaseJoinClause> sourceClauseList;
                sourceClauseList = this.getClauseList();
                @SuppressWarnings("unchecked")
                List<BaseJoinClause> copyClauseList = ((List<BaseJoinClause> ) strategy.copy(LocatorUtils.property(locator, "clauseList", sourceClauseList), sourceClauseList));
                copy.clauseList = null;
                List<BaseJoinClause> uniqueClauseListl = copy.getClauseList();
                uniqueClauseListl.addAll(copyClauseList);
            } else {
                copy.clauseList = null;
            }
            if (this.alias!= null) {
                String sourceAlias;
                sourceAlias = this.getAlias();
                String copyAlias = ((String) strategy.copy(LocatorUtils.property(locator, "alias", sourceAlias), sourceAlias));
                copy.setAlias(copyAlias);
            } else {
                copy.alias = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new FromClauseChoice();
    }

}
