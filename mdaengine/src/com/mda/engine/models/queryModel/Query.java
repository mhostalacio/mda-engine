
package com.mda.engine.models.queryModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.models.common.BOMReferenceType;
import com.mda.engine.models.common.ModelInPackageBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for Query complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Query">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/common}ModelInPackageBase">
 *       &lt;sequence>
 *         &lt;element name="OwnedBy" type="{http://www.mdaengine.com/mdaengine/models/common}BOMReferenceType"/>
 *         &lt;element name="Parameters" type="{http://www.mdaengine.com/mdaengine/models/queryModel}QueryParameters"/>
 *         &lt;element name="ResultType" type="{http://www.mdaengine.com/mdaengine/models/queryModel}QueryResultType" minOccurs="0"/>
 *         &lt;element name="Definition" type="{http://www.mdaengine.com/mdaengine/models/queryModel}QueryDefinitionCollection"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Description" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="ResultSet" use="required" type="{http://www.mdaengine.com/mdaengine/models/queryModel}QueryResultSet" />
 *       &lt;attribute name="UseNolockHint" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="UsePagination" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Query", propOrder = {
    "ownedBy",
    "parameters",
    "resultType",
    "definition"
})
@XmlRootElement
public class Query
    extends ModelInPackageBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "OwnedBy", required = true)
    protected BOMReferenceType ownedBy;
    @XmlElement(name = "Parameters", required = true)
    protected QueryParameters parameters;
    @XmlElement(name = "ResultType")
    protected QueryResultType resultType;
    @XmlElement(name = "Definition", required = true)
    protected QueryDefinitionCollection definition;
    @XmlAttribute(name = "Description", required = true)
    protected String description;
    @XmlAttribute(name = "ResultSet", required = true)
    protected QueryResultSet resultSet;
    @XmlAttribute(name = "UseNolockHint")
    protected Boolean useNolockHint;
    @XmlAttribute(name = "UsePagination")
    protected Boolean usePagination;

    /**
     * Gets the value of the ownedBy property.
     * 
     * @return
     *     possible object is
     *     {@link BOMReferenceType }
     *     
     */
    public BOMReferenceType getOwnedBy() {
        return ownedBy;
    }

    /**
     * Sets the value of the ownedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link BOMReferenceType }
     *     
     */
    public void setOwnedBy(BOMReferenceType value) {
        this.ownedBy = value;
    }

    /**
     * Gets the value of the parameters property.
     * 
     * @return
     *     possible object is
     *     {@link QueryParameters }
     *     
     */
    public QueryParameters getParameters() {
        return parameters;
    }

    /**
     * Sets the value of the parameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryParameters }
     *     
     */
    public void setParameters(QueryParameters value) {
        this.parameters = value;
    }

    /**
     * Gets the value of the resultType property.
     * 
     * @return
     *     possible object is
     *     {@link QueryResultType }
     *     
     */
    public QueryResultType getResultType() {
        return resultType;
    }

    /**
     * Sets the value of the resultType property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryResultType }
     *     
     */
    public void setResultType(QueryResultType value) {
        this.resultType = value;
    }

    /**
     * Gets the value of the definition property.
     * 
     * @return
     *     possible object is
     *     {@link QueryDefinitionCollection }
     *     
     */
    public QueryDefinitionCollection getDefinition() {
        return definition;
    }

    /**
     * Sets the value of the definition property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryDefinitionCollection }
     *     
     */
    public void setDefinition(QueryDefinitionCollection value) {
        this.definition = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the resultSet property.
     * 
     * @return
     *     possible object is
     *     {@link QueryResultSet }
     *     
     */
    public QueryResultSet getResultSet() {
        return resultSet;
    }

    /**
     * Sets the value of the resultSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryResultSet }
     *     
     */
    public void setResultSet(QueryResultSet value) {
        this.resultSet = value;
    }

    /**
     * Gets the value of the useNolockHint property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isUseNolockHint() {
        if (useNolockHint == null) {
            return false;
        } else {
            return useNolockHint;
        }
    }

    /**
     * Sets the value of the useNolockHint property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseNolockHint(Boolean value) {
        this.useNolockHint = value;
    }

    /**
     * Gets the value of the usePagination property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isUsePagination() {
        if (usePagination == null) {
            return false;
        } else {
            return usePagination;
        }
    }

    /**
     * Sets the value of the usePagination property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUsePagination(Boolean value) {
        this.usePagination = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Query)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final Query that = ((Query) object);
        {
            BOMReferenceType lhsOwnedBy;
            lhsOwnedBy = this.getOwnedBy();
            BOMReferenceType rhsOwnedBy;
            rhsOwnedBy = that.getOwnedBy();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ownedBy", lhsOwnedBy), LocatorUtils.property(thatLocator, "ownedBy", rhsOwnedBy), lhsOwnedBy, rhsOwnedBy)) {
                return false;
            }
        }
        {
            QueryParameters lhsParameters;
            lhsParameters = this.getParameters();
            QueryParameters rhsParameters;
            rhsParameters = that.getParameters();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "parameters", lhsParameters), LocatorUtils.property(thatLocator, "parameters", rhsParameters), lhsParameters, rhsParameters)) {
                return false;
            }
        }
        {
            QueryResultType lhsResultType;
            lhsResultType = this.getResultType();
            QueryResultType rhsResultType;
            rhsResultType = that.getResultType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "resultType", lhsResultType), LocatorUtils.property(thatLocator, "resultType", rhsResultType), lhsResultType, rhsResultType)) {
                return false;
            }
        }
        {
            QueryDefinitionCollection lhsDefinition;
            lhsDefinition = this.getDefinition();
            QueryDefinitionCollection rhsDefinition;
            rhsDefinition = that.getDefinition();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "definition", lhsDefinition), LocatorUtils.property(thatLocator, "definition", rhsDefinition), lhsDefinition, rhsDefinition)) {
                return false;
            }
        }
        {
            String lhsDescription;
            lhsDescription = this.getDescription();
            String rhsDescription;
            rhsDescription = that.getDescription();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "description", lhsDescription), LocatorUtils.property(thatLocator, "description", rhsDescription), lhsDescription, rhsDescription)) {
                return false;
            }
        }
        {
            QueryResultSet lhsResultSet;
            lhsResultSet = this.getResultSet();
            QueryResultSet rhsResultSet;
            rhsResultSet = that.getResultSet();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "resultSet", lhsResultSet), LocatorUtils.property(thatLocator, "resultSet", rhsResultSet), lhsResultSet, rhsResultSet)) {
                return false;
            }
        }
        {
            boolean lhsUseNolockHint;
            lhsUseNolockHint = this.isUseNolockHint();
            boolean rhsUseNolockHint;
            rhsUseNolockHint = that.isUseNolockHint();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "useNolockHint", lhsUseNolockHint), LocatorUtils.property(thatLocator, "useNolockHint", rhsUseNolockHint), lhsUseNolockHint, rhsUseNolockHint)) {
                return false;
            }
        }
        {
            boolean lhsUsePagination;
            lhsUsePagination = this.isUsePagination();
            boolean rhsUsePagination;
            rhsUsePagination = that.isUsePagination();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "usePagination", lhsUsePagination), LocatorUtils.property(thatLocator, "usePagination", rhsUsePagination), lhsUsePagination, rhsUsePagination)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof Query) {
            final Query copy = ((Query) draftCopy);
            if (this.ownedBy!= null) {
                BOMReferenceType sourceOwnedBy;
                sourceOwnedBy = this.getOwnedBy();
                BOMReferenceType copyOwnedBy = ((BOMReferenceType) strategy.copy(LocatorUtils.property(locator, "ownedBy", sourceOwnedBy), sourceOwnedBy));
                copy.setOwnedBy(copyOwnedBy);
            } else {
                copy.ownedBy = null;
            }
            if (this.parameters!= null) {
                QueryParameters sourceParameters;
                sourceParameters = this.getParameters();
                QueryParameters copyParameters = ((QueryParameters) strategy.copy(LocatorUtils.property(locator, "parameters", sourceParameters), sourceParameters));
                copy.setParameters(copyParameters);
            } else {
                copy.parameters = null;
            }
            if (this.resultType!= null) {
                QueryResultType sourceResultType;
                sourceResultType = this.getResultType();
                QueryResultType copyResultType = ((QueryResultType) strategy.copy(LocatorUtils.property(locator, "resultType", sourceResultType), sourceResultType));
                copy.setResultType(copyResultType);
            } else {
                copy.resultType = null;
            }
            if (this.definition!= null) {
                QueryDefinitionCollection sourceDefinition;
                sourceDefinition = this.getDefinition();
                QueryDefinitionCollection copyDefinition = ((QueryDefinitionCollection) strategy.copy(LocatorUtils.property(locator, "definition", sourceDefinition), sourceDefinition));
                copy.setDefinition(copyDefinition);
            } else {
                copy.definition = null;
            }
            if (this.description!= null) {
                String sourceDescription;
                sourceDescription = this.getDescription();
                String copyDescription = ((String) strategy.copy(LocatorUtils.property(locator, "description", sourceDescription), sourceDescription));
                copy.setDescription(copyDescription);
            } else {
                copy.description = null;
            }
            if (this.resultSet!= null) {
                QueryResultSet sourceResultSet;
                sourceResultSet = this.getResultSet();
                QueryResultSet copyResultSet = ((QueryResultSet) strategy.copy(LocatorUtils.property(locator, "resultSet", sourceResultSet), sourceResultSet));
                copy.setResultSet(copyResultSet);
            } else {
                copy.resultSet = null;
            }
            if (this.useNolockHint!= null) {
                boolean sourceUseNolockHint;
                sourceUseNolockHint = this.isUseNolockHint();
                boolean copyUseNolockHint = strategy.copy(LocatorUtils.property(locator, "useNolockHint", sourceUseNolockHint), sourceUseNolockHint);
                copy.setUseNolockHint(copyUseNolockHint);
            } else {
                copy.useNolockHint = null;
            }
            if (this.usePagination!= null) {
                boolean sourceUsePagination;
                sourceUsePagination = this.isUsePagination();
                boolean copyUsePagination = strategy.copy(LocatorUtils.property(locator, "usePagination", sourceUsePagination), sourceUsePagination);
                copy.setUsePagination(copyUsePagination);
            } else {
                copy.usePagination = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Query();
    }
    
//--simple--preserve
    
    
    private transient com.mda.engine.models.businessObjectModel.ObjectType owner;
    private transient String extendedName;
    
    public com.mda.engine.models.businessObjectModel.ObjectType getOwner() {

		return this.owner;
	}
    
    public void setOwner(com.mda.engine.models.businessObjectModel.ObjectType owner) {

		this.owner = owner;
	}
    
    public String getResultSetClassName()
    {
    	if (this.getResultSet().equals(QueryResultSet.SINGLE))
			return getResultSetSingleClassName();
		else
			return String.format("EntityList<%s>", getResultSetSingleClassName());
    }
    
    public String getResultSetSingleClassName(){
    	
    	if (this.getResultType() != null && this.getResultType().getCustom() != null && this.getResultType().getCustom().getColumnList() != null) {
    		return String.format("%s%sQueryResult", getOwner().getName(), getName());
    		
    	} else if(this.getResultType() != null && this.getResultType().getObject() != null){
    		return this.getResultType().getObject().getName();
    		
    	} else if (this.getParameters().getReference() != null && this.getParameters().getReference().getReferencedQuery() != null) {
    		return this.getParameters().getReference().getReferencedQuery().getResultSetSingleClassName();
    		
    	} else {
    		return getOwner().getApplication().getEntitiesProjectNamespace() + "." + getOwner().getPackageName() + "." + getOwner().getName();
    	}
    }
    
    public String getArgumentsClassName()
    {
    	if (this.getParameters() != null && this.getParameters().getDefinition() != null)
    	{
    		return String.format("%s%sQueryArgument", getOwner().getName(), getName());
    	}
    	else if (this.getParameters().getReference().getReferencedQuery() != null)
    	{
    		return this.getParameters().getReference().getReferencedQuery().getResultSetClassName();
    	}
    	
    	throw new org.apache.commons.lang.NotImplementedException();
    }
    
    public com.mda.engine.models.dataModel.DataBaseItem getFirstArgument()
    {
    	return this.getParameters().getDefinition().getColumnList().get(0);
    }
    
    public String getQueryMethodName()
    {
    	return getName();
    }

	public void setExtendedName(String extendedName) {
		this.extendedName = extendedName;
	}

	public String getExtendedName() {
		return extendedName;
	}

	
//--simple--preserve

}
