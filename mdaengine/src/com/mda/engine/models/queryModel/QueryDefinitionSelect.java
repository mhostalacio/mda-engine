
package com.mda.engine.models.queryModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for QueryDefinitionSelect complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueryDefinitionSelect">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Columns" type="{http://www.mdaengine.com/mdaengine/models/queryModel}SelectColumnCollection" minOccurs="0"/>
 *         &lt;element name="From" type="{http://www.mdaengine.com/mdaengine/models/queryModel}FromClauseChoice"/>
 *         &lt;element name="Where" type="{http://www.mdaengine.com/mdaengine/models/queryModel}WhereClause" minOccurs="0"/>
 *         &lt;element name="GroupBy" type="{http://www.mdaengine.com/mdaengine/models/queryModel}GroupByClause" minOccurs="0"/>
 *         &lt;element name="OrderBy" type="{http://www.mdaengine.com/mdaengine/models/queryModel}OrderByClause" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Distinct" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="Top" type="{http://www.w3.org/2001/XMLSchema}int" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryDefinitionSelect", propOrder = {
    "columns",
    "from",
    "where",
    "groupBy",
    "orderBy"
})
public class QueryDefinitionSelect
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Columns")
    protected SelectColumnCollection columns;
    @XmlElement(name = "From", required = true)
    protected FromClauseChoice from;
    @XmlElement(name = "Where")
    protected WhereClause where;
    @XmlElement(name = "GroupBy")
    protected GroupByClause groupBy;
    @XmlElement(name = "OrderBy")
    protected OrderByClause orderBy;
    @XmlAttribute(name = "Distinct")
    protected Boolean distinct;
    @XmlAttribute(name = "Top")
    protected Integer top;

    /**
     * Gets the value of the columns property.
     * 
     * @return
     *     possible object is
     *     {@link SelectColumnCollection }
     *     
     */
    public SelectColumnCollection getColumns() {
        return columns;
    }

    /**
     * Sets the value of the columns property.
     * 
     * @param value
     *     allowed object is
     *     {@link SelectColumnCollection }
     *     
     */
    public void setColumns(SelectColumnCollection value) {
        this.columns = value;
    }

    /**
     * Gets the value of the from property.
     * 
     * @return
     *     possible object is
     *     {@link FromClauseChoice }
     *     
     */
    public FromClauseChoice getFrom() {
        return from;
    }

    /**
     * Sets the value of the from property.
     * 
     * @param value
     *     allowed object is
     *     {@link FromClauseChoice }
     *     
     */
    public void setFrom(FromClauseChoice value) {
        this.from = value;
    }

    /**
     * Gets the value of the where property.
     * 
     * @return
     *     possible object is
     *     {@link WhereClause }
     *     
     */
    public WhereClause getWhere() {
        return where;
    }

    /**
     * Sets the value of the where property.
     * 
     * @param value
     *     allowed object is
     *     {@link WhereClause }
     *     
     */
    public void setWhere(WhereClause value) {
        this.where = value;
    }

    /**
     * Gets the value of the groupBy property.
     * 
     * @return
     *     possible object is
     *     {@link GroupByClause }
     *     
     */
    public GroupByClause getGroupBy() {
        return groupBy;
    }

    /**
     * Sets the value of the groupBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupByClause }
     *     
     */
    public void setGroupBy(GroupByClause value) {
        this.groupBy = value;
    }

    /**
     * Gets the value of the orderBy property.
     * 
     * @return
     *     possible object is
     *     {@link OrderByClause }
     *     
     */
    public OrderByClause getOrderBy() {
        return orderBy;
    }

    /**
     * Sets the value of the orderBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderByClause }
     *     
     */
    public void setOrderBy(OrderByClause value) {
        this.orderBy = value;
    }

    /**
     * Gets the value of the distinct property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isDistinct() {
        if (distinct == null) {
            return false;
        } else {
            return distinct;
        }
    }

    /**
     * Sets the value of the distinct property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDistinct(Boolean value) {
        this.distinct = value;
    }

    /**
     * Gets the value of the top property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTop() {
        return top;
    }

    /**
     * Sets the value of the top property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTop(Integer value) {
        this.top = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof QueryDefinitionSelect)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final QueryDefinitionSelect that = ((QueryDefinitionSelect) object);
        {
            SelectColumnCollection lhsColumns;
            lhsColumns = this.getColumns();
            SelectColumnCollection rhsColumns;
            rhsColumns = that.getColumns();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "columns", lhsColumns), LocatorUtils.property(thatLocator, "columns", rhsColumns), lhsColumns, rhsColumns)) {
                return false;
            }
        }
        {
            FromClauseChoice lhsFrom;
            lhsFrom = this.getFrom();
            FromClauseChoice rhsFrom;
            rhsFrom = that.getFrom();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "from", lhsFrom), LocatorUtils.property(thatLocator, "from", rhsFrom), lhsFrom, rhsFrom)) {
                return false;
            }
        }
        {
            WhereClause lhsWhere;
            lhsWhere = this.getWhere();
            WhereClause rhsWhere;
            rhsWhere = that.getWhere();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "where", lhsWhere), LocatorUtils.property(thatLocator, "where", rhsWhere), lhsWhere, rhsWhere)) {
                return false;
            }
        }
        {
            GroupByClause lhsGroupBy;
            lhsGroupBy = this.getGroupBy();
            GroupByClause rhsGroupBy;
            rhsGroupBy = that.getGroupBy();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "groupBy", lhsGroupBy), LocatorUtils.property(thatLocator, "groupBy", rhsGroupBy), lhsGroupBy, rhsGroupBy)) {
                return false;
            }
        }
        {
            OrderByClause lhsOrderBy;
            lhsOrderBy = this.getOrderBy();
            OrderByClause rhsOrderBy;
            rhsOrderBy = that.getOrderBy();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "orderBy", lhsOrderBy), LocatorUtils.property(thatLocator, "orderBy", rhsOrderBy), lhsOrderBy, rhsOrderBy)) {
                return false;
            }
        }
        {
            boolean lhsDistinct;
            lhsDistinct = this.isDistinct();
            boolean rhsDistinct;
            rhsDistinct = that.isDistinct();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "distinct", lhsDistinct), LocatorUtils.property(thatLocator, "distinct", rhsDistinct), lhsDistinct, rhsDistinct)) {
                return false;
            }
        }
        {
            Integer lhsTop;
            lhsTop = this.getTop();
            Integer rhsTop;
            rhsTop = that.getTop();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "top", lhsTop), LocatorUtils.property(thatLocator, "top", rhsTop), lhsTop, rhsTop)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof QueryDefinitionSelect) {
            final QueryDefinitionSelect copy = ((QueryDefinitionSelect) draftCopy);
            if (this.columns!= null) {
                SelectColumnCollection sourceColumns;
                sourceColumns = this.getColumns();
                SelectColumnCollection copyColumns = ((SelectColumnCollection) strategy.copy(LocatorUtils.property(locator, "columns", sourceColumns), sourceColumns));
                copy.setColumns(copyColumns);
            } else {
                copy.columns = null;
            }
            if (this.from!= null) {
                FromClauseChoice sourceFrom;
                sourceFrom = this.getFrom();
                FromClauseChoice copyFrom = ((FromClauseChoice) strategy.copy(LocatorUtils.property(locator, "from", sourceFrom), sourceFrom));
                copy.setFrom(copyFrom);
            } else {
                copy.from = null;
            }
            if (this.where!= null) {
                WhereClause sourceWhere;
                sourceWhere = this.getWhere();
                WhereClause copyWhere = ((WhereClause) strategy.copy(LocatorUtils.property(locator, "where", sourceWhere), sourceWhere));
                copy.setWhere(copyWhere);
            } else {
                copy.where = null;
            }
            if (this.groupBy!= null) {
                GroupByClause sourceGroupBy;
                sourceGroupBy = this.getGroupBy();
                GroupByClause copyGroupBy = ((GroupByClause) strategy.copy(LocatorUtils.property(locator, "groupBy", sourceGroupBy), sourceGroupBy));
                copy.setGroupBy(copyGroupBy);
            } else {
                copy.groupBy = null;
            }
            if (this.orderBy!= null) {
                OrderByClause sourceOrderBy;
                sourceOrderBy = this.getOrderBy();
                OrderByClause copyOrderBy = ((OrderByClause) strategy.copy(LocatorUtils.property(locator, "orderBy", sourceOrderBy), sourceOrderBy));
                copy.setOrderBy(copyOrderBy);
            } else {
                copy.orderBy = null;
            }
            if (this.distinct!= null) {
                boolean sourceDistinct;
                sourceDistinct = this.isDistinct();
                boolean copyDistinct = strategy.copy(LocatorUtils.property(locator, "distinct", sourceDistinct), sourceDistinct);
                copy.setDistinct(copyDistinct);
            } else {
                copy.distinct = null;
            }
            if (this.top!= null) {
                Integer sourceTop;
                sourceTop = this.getTop();
                Integer copyTop = ((Integer) strategy.copy(LocatorUtils.property(locator, "top", sourceTop), sourceTop));
                copy.setTop(copyTop);
            } else {
                copy.top = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new QueryDefinitionSelect();
    }

}
