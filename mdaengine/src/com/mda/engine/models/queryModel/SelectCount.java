
package com.mda.engine.models.queryModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for SelectCount complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SelectCount">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/queryModel}SelectAttributesBase">
 *       &lt;attribute name="ResultAlias" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SelectCount")
public class SelectCount
    extends SelectAttributesBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "ResultAlias", required = true)
    protected String resultAlias;

    /**
     * Gets the value of the resultAlias property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResultAlias() {
        return resultAlias;
    }

    /**
     * Sets the value of the resultAlias property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResultAlias(String value) {
        this.resultAlias = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof SelectCount)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final SelectCount that = ((SelectCount) object);
        {
            String lhsResultAlias;
            lhsResultAlias = this.getResultAlias();
            String rhsResultAlias;
            rhsResultAlias = that.getResultAlias();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "resultAlias", lhsResultAlias), LocatorUtils.property(thatLocator, "resultAlias", rhsResultAlias), lhsResultAlias, rhsResultAlias)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof SelectCount) {
            final SelectCount copy = ((SelectCount) draftCopy);
            if (this.resultAlias!= null) {
                String sourceResultAlias;
                sourceResultAlias = this.getResultAlias();
                String copyResultAlias = ((String) strategy.copy(LocatorUtils.property(locator, "resultAlias", sourceResultAlias), sourceResultAlias));
                copy.setResultAlias(copyResultAlias);
            } else {
                copy.resultAlias = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new SelectCount();
    }
    
//--simple--preserve
    
  	@Override
  	public void writeSelection(com.mda.engine.utils.Stringcode c) {
  		c.line("COUNT(1) AS {0},", this.resultAlias);   
  	}   
    	
//--simple--preserve

}
