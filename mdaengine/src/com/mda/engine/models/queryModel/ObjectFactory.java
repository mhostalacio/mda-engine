
package com.mda.engine.models.queryModel;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.mda.engine.models.queryModel package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Query_QNAME = new QName("http://www.mdaengine.com/mdaengine/models/queryModel", "Query");
    private final static QName _CollationMappings_QNAME = new QName("http://www.mdaengine.com/mdaengine/models/queryModel", "CollationMappings");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.mda.engine.models.queryModel
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CollationLangsCollection }
     * 
     */
    public CollationLangsCollection createCollationLangsCollection() {
        return new CollationLangsCollection();
    }

    /**
     * Create an instance of {@link QueryParametersReference }
     * 
     */
    public QueryParametersReference createQueryParametersReference() {
        return new QueryParametersReference();
    }

    /**
     * Create an instance of {@link SelectAllReference }
     * 
     */
    public SelectAllReference createSelectAllReference() {
        return new SelectAllReference();
    }

    /**
     * Create an instance of {@link RightJoinClause }
     * 
     */
    public RightJoinClause createRightJoinClause() {
        return new RightJoinClause();
    }

    /**
     * Create an instance of {@link SelectColumnCollection }
     * 
     */
    public SelectColumnCollection createSelectColumnCollection() {
        return new SelectColumnCollection();
    }

    /**
     * Create an instance of {@link SelectColumnOrderByReference }
     * 
     */
    public SelectColumnOrderByReference createSelectColumnOrderByReference() {
        return new SelectColumnOrderByReference();
    }

    /**
     * Create an instance of {@link QueryDefinitionCollectionList }
     * 
     */
    public QueryDefinitionCollectionList createQueryDefinitionCollectionList() {
        return new QueryDefinitionCollectionList();
    }

    /**
     * Create an instance of {@link OrderByClause }
     * 
     */
    public OrderByClause createOrderByClause() {
        return new OrderByClause();
    }

    /**
     * Create an instance of {@link OnClause }
     * 
     */
    public OnClause createOnClause() {
        return new OnClause();
    }

    /**
     * Create an instance of {@link QueryDefinitionSelect }
     * 
     */
    public QueryDefinitionSelect createQueryDefinitionSelect() {
        return new QueryDefinitionSelect();
    }

    /**
     * Create an instance of {@link SelectColumnReference }
     * 
     */
    public SelectColumnReference createSelectColumnReference() {
        return new SelectColumnReference();
    }

    /**
     * Create an instance of {@link QueryResultType }
     * 
     */
    public QueryResultType createQueryResultType() {
        return new QueryResultType();
    }

    /**
     * Create an instance of {@link InnerJoinClause }
     * 
     */
    public InnerJoinClause createInnerJoinClause() {
        return new InnerJoinClause();
    }

    /**
     * Create an instance of {@link GroupByClause }
     * 
     */
    public GroupByClause createGroupByClause() {
        return new GroupByClause();
    }

    /**
     * Create an instance of {@link BigIntList }
     * 
     */
    public BigIntList createBigIntList() {
        return new BigIntList();
    }

    /**
     * Create an instance of {@link BaseJoinClause }
     * 
     */
    public BaseJoinClause createBaseJoinClause() {
        return new BaseJoinClause();
    }

    /**
     * Create an instance of {@link CollationMappingException }
     * 
     */
    public CollationMappingException createCollationMappingException() {
        return new CollationMappingException();
    }

    /**
     * Create an instance of {@link QueryParameters }
     * 
     */
    public QueryParameters createQueryParameters() {
        return new QueryParameters();
    }

    /**
     * Create an instance of {@link QueryDefinitionCollection }
     * 
     */
    public QueryDefinitionCollection createQueryDefinitionCollection() {
        return new QueryDefinitionCollection();
    }

    /**
     * Create an instance of {@link CollationLang }
     * 
     */
    public CollationLang createCollationLang() {
        return new CollationLang();
    }

    /**
     * Create an instance of {@link CaseClause }
     * 
     */
    public CaseClause createCaseClause() {
        return new CaseClause();
    }

    /**
     * Create an instance of {@link SelectCount }
     * 
     */
    public SelectCount createSelectCount() {
        return new SelectCount();
    }

    /**
     * Create an instance of {@link CaseWhendClause }
     * 
     */
    public CaseWhendClause createCaseWhendClause() {
        return new CaseWhendClause();
    }

    /**
     * Create an instance of {@link WithClause }
     * 
     */
    public WithClause createWithClause() {
        return new WithClause();
    }

    /**
     * Create an instance of {@link Query }
     * 
     */
    public Query createQuery() {
        return new Query();
    }

    /**
     * Create an instance of {@link WhereClause }
     * 
     */
    public WhereClause createWhereClause() {
        return new WhereClause();
    }

    /**
     * Create an instance of {@link LeftJoinClause }
     * 
     */
    public LeftJoinClause createLeftJoinClause() {
        return new LeftJoinClause();
    }

    /**
     * Create an instance of {@link BigIntPairList }
     * 
     */
    public BigIntPairList createBigIntPairList() {
        return new BigIntPairList();
    }

    /**
     * Create an instance of {@link FromClauseChoice }
     * 
     */
    public FromClauseChoice createFromClauseChoice() {
        return new FromClauseChoice();
    }

    /**
     * Create an instance of {@link CollationMappings }
     * 
     */
    public CollationMappings createCollationMappings() {
        return new CollationMappings();
    }

    /**
     * Create an instance of {@link CollationMappingExceptionCollection }
     * 
     */
    public CollationMappingExceptionCollection createCollationMappingExceptionCollection() {
        return new CollationMappingExceptionCollection();
    }

    /**
     * Create an instance of {@link ParameterCollection }
     * 
     */
    public ParameterCollection createParameterCollection() {
        return new ParameterCollection();
    }

    /**
     * Create an instance of {@link StringList }
     * 
     */
    public StringList createStringList() {
        return new StringList();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Query }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.mdaengine.com/mdaengine/models/queryModel", name = "Query")
    public JAXBElement<Query> createQuery(Query value) {
        return new JAXBElement<Query>(_Query_QNAME, Query.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CollationMappings }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.mdaengine.com/mdaengine/models/queryModel", name = "CollationMappings")
    public JAXBElement<CollationMappings> createCollationMappings(CollationMappings value) {
        return new JAXBElement<CollationMappings>(_CollationMappings_QNAME, CollationMappings.class, null, value);
    }

}
