
package com.mda.engine.models.queryModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import com.mda.engine.models.common.BOMReferenceType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for WithClause complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WithClause">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="Table" type="{http://www.mdaengine.com/mdaengine/models/common}BOMReferenceType" minOccurs="0"/>
 *           &lt;element name="Query" type="{http://www.mdaengine.com/mdaengine/models/queryModel}Query" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element name="On" type="{http://www.mdaengine.com/mdaengine/models/queryModel}OnClause"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Alias" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WithClause", propOrder = {
    "table",
    "query",
    "on"
})
public class WithClause
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Table")
    protected BOMReferenceType table;
    @XmlElement(name = "Query")
    protected Query query;
    @XmlElement(name = "On", required = true)
    protected OnClause on;
    @XmlAttribute(name = "Alias")
    protected String alias;

    /**
     * Gets the value of the table property.
     * 
     * @return
     *     possible object is
     *     {@link BOMReferenceType }
     *     
     */
    public BOMReferenceType getTable() {
        return table;
    }

    /**
     * Sets the value of the table property.
     * 
     * @param value
     *     allowed object is
     *     {@link BOMReferenceType }
     *     
     */
    public void setTable(BOMReferenceType value) {
        this.table = value;
    }

    /**
     * Gets the value of the query property.
     * 
     * @return
     *     possible object is
     *     {@link Query }
     *     
     */
    public Query getQuery() {
        return query;
    }

    /**
     * Sets the value of the query property.
     * 
     * @param value
     *     allowed object is
     *     {@link Query }
     *     
     */
    public void setQuery(Query value) {
        this.query = value;
    }

    /**
     * Gets the value of the on property.
     * 
     * @return
     *     possible object is
     *     {@link OnClause }
     *     
     */
    public OnClause getOn() {
        return on;
    }

    /**
     * Sets the value of the on property.
     * 
     * @param value
     *     allowed object is
     *     {@link OnClause }
     *     
     */
    public void setOn(OnClause value) {
        this.on = value;
    }

    /**
     * Gets the value of the alias property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlias() {
        return alias;
    }

    /**
     * Sets the value of the alias property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlias(String value) {
        this.alias = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof WithClause)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final WithClause that = ((WithClause) object);
        {
            BOMReferenceType lhsTable;
            lhsTable = this.getTable();
            BOMReferenceType rhsTable;
            rhsTable = that.getTable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "table", lhsTable), LocatorUtils.property(thatLocator, "table", rhsTable), lhsTable, rhsTable)) {
                return false;
            }
        }
        {
            Query lhsQuery;
            lhsQuery = this.getQuery();
            Query rhsQuery;
            rhsQuery = that.getQuery();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "query", lhsQuery), LocatorUtils.property(thatLocator, "query", rhsQuery), lhsQuery, rhsQuery)) {
                return false;
            }
        }
        {
            OnClause lhsOn;
            lhsOn = this.getOn();
            OnClause rhsOn;
            rhsOn = that.getOn();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "on", lhsOn), LocatorUtils.property(thatLocator, "on", rhsOn), lhsOn, rhsOn)) {
                return false;
            }
        }
        {
            String lhsAlias;
            lhsAlias = this.getAlias();
            String rhsAlias;
            rhsAlias = that.getAlias();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "alias", lhsAlias), LocatorUtils.property(thatLocator, "alias", rhsAlias), lhsAlias, rhsAlias)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof WithClause) {
            final WithClause copy = ((WithClause) draftCopy);
            if (this.table!= null) {
                BOMReferenceType sourceTable;
                sourceTable = this.getTable();
                BOMReferenceType copyTable = ((BOMReferenceType) strategy.copy(LocatorUtils.property(locator, "table", sourceTable), sourceTable));
                copy.setTable(copyTable);
            } else {
                copy.table = null;
            }
            if (this.query!= null) {
                Query sourceQuery;
                sourceQuery = this.getQuery();
                Query copyQuery = ((Query) strategy.copy(LocatorUtils.property(locator, "query", sourceQuery), sourceQuery));
                copy.setQuery(copyQuery);
            } else {
                copy.query = null;
            }
            if (this.on!= null) {
                OnClause sourceOn;
                sourceOn = this.getOn();
                OnClause copyOn = ((OnClause) strategy.copy(LocatorUtils.property(locator, "on", sourceOn), sourceOn));
                copy.setOn(copyOn);
            } else {
                copy.on = null;
            }
            if (this.alias!= null) {
                String sourceAlias;
                sourceAlias = this.getAlias();
                String copyAlias = ((String) strategy.copy(LocatorUtils.property(locator, "alias", sourceAlias), sourceAlias));
                copy.setAlias(copyAlias);
            } else {
                copy.alias = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new WithClause();
    }

}
