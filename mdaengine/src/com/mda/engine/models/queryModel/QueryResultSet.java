
package com.mda.engine.models.queryModel;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QueryResultSet.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="QueryResultSet">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="List"/>
 *     &lt;enumeration value="Single"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "QueryResultSet")
@XmlEnum
public enum QueryResultSet {

    @XmlEnumValue("List")
    LIST("List"),
    @XmlEnumValue("Single")
    SINGLE("Single");
    private final String value;

    QueryResultSet(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static QueryResultSet fromValue(String v) {
        for (QueryResultSet c: QueryResultSet.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
