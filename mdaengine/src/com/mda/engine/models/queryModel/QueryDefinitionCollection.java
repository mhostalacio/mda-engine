
package com.mda.engine.models.queryModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for QueryDefinitionCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueryDefinitionCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="Select" type="{http://www.mdaengine.com/mdaengine/models/queryModel}QueryDefinitionSelect"/>
 *         &lt;element name="Union" type="{http://www.mdaengine.com/mdaengine/models/queryModel}QueryDefinitionCollectionList"/>
 *       &lt;/choice>
 *       &lt;attribute name="Alias" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryDefinitionCollection", propOrder = {
    "select",
    "union"
})
public class QueryDefinitionCollection
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Select")
    protected QueryDefinitionSelect select;
    @XmlElement(name = "Union")
    protected QueryDefinitionCollectionList union;
    @XmlAttribute(name = "Alias")
    protected String alias;

    /**
     * Gets the value of the select property.
     * 
     * @return
     *     possible object is
     *     {@link QueryDefinitionSelect }
     *     
     */
    public QueryDefinitionSelect getSelect() {
        return select;
    }

    /**
     * Sets the value of the select property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryDefinitionSelect }
     *     
     */
    public void setSelect(QueryDefinitionSelect value) {
        this.select = value;
    }

    /**
     * Gets the value of the union property.
     * 
     * @return
     *     possible object is
     *     {@link QueryDefinitionCollectionList }
     *     
     */
    public QueryDefinitionCollectionList getUnion() {
        return union;
    }

    /**
     * Sets the value of the union property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryDefinitionCollectionList }
     *     
     */
    public void setUnion(QueryDefinitionCollectionList value) {
        this.union = value;
    }

    /**
     * Gets the value of the alias property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlias() {
        return alias;
    }

    /**
     * Sets the value of the alias property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlias(String value) {
        this.alias = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof QueryDefinitionCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final QueryDefinitionCollection that = ((QueryDefinitionCollection) object);
        {
            QueryDefinitionSelect lhsSelect;
            lhsSelect = this.getSelect();
            QueryDefinitionSelect rhsSelect;
            rhsSelect = that.getSelect();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "select", lhsSelect), LocatorUtils.property(thatLocator, "select", rhsSelect), lhsSelect, rhsSelect)) {
                return false;
            }
        }
        {
            QueryDefinitionCollectionList lhsUnion;
            lhsUnion = this.getUnion();
            QueryDefinitionCollectionList rhsUnion;
            rhsUnion = that.getUnion();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "union", lhsUnion), LocatorUtils.property(thatLocator, "union", rhsUnion), lhsUnion, rhsUnion)) {
                return false;
            }
        }
        {
            String lhsAlias;
            lhsAlias = this.getAlias();
            String rhsAlias;
            rhsAlias = that.getAlias();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "alias", lhsAlias), LocatorUtils.property(thatLocator, "alias", rhsAlias), lhsAlias, rhsAlias)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof QueryDefinitionCollection) {
            final QueryDefinitionCollection copy = ((QueryDefinitionCollection) draftCopy);
            if (this.select!= null) {
                QueryDefinitionSelect sourceSelect;
                sourceSelect = this.getSelect();
                QueryDefinitionSelect copySelect = ((QueryDefinitionSelect) strategy.copy(LocatorUtils.property(locator, "select", sourceSelect), sourceSelect));
                copy.setSelect(copySelect);
            } else {
                copy.select = null;
            }
            if (this.union!= null) {
                QueryDefinitionCollectionList sourceUnion;
                sourceUnion = this.getUnion();
                QueryDefinitionCollectionList copyUnion = ((QueryDefinitionCollectionList) strategy.copy(LocatorUtils.property(locator, "union", sourceUnion), sourceUnion));
                copy.setUnion(copyUnion);
            } else {
                copy.union = null;
            }
            if (this.alias!= null) {
                String sourceAlias;
                sourceAlias = this.getAlias();
                String copyAlias = ((String) strategy.copy(LocatorUtils.property(locator, "alias", sourceAlias), sourceAlias));
                copy.setAlias(copyAlias);
            } else {
                copy.alias = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new QueryDefinitionCollection();
    }

}
