
package com.mda.engine.models.queryModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for QueryParameters complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueryParameters">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="Reference" type="{http://www.mdaengine.com/mdaengine/models/queryModel}QueryParametersReference"/>
 *         &lt;element name="Definition" type="{http://www.mdaengine.com/mdaengine/models/queryModel}ParameterCollection"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryParameters", propOrder = {
    "reference",
    "definition"
})
public class QueryParameters
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Reference")
    protected QueryParametersReference reference;
    @XmlElement(name = "Definition")
    protected ParameterCollection definition;

    /**
     * Gets the value of the reference property.
     * 
     * @return
     *     possible object is
     *     {@link QueryParametersReference }
     *     
     */
    public QueryParametersReference getReference() {
        return reference;
    }

    /**
     * Sets the value of the reference property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryParametersReference }
     *     
     */
    public void setReference(QueryParametersReference value) {
        this.reference = value;
    }

    /**
     * Gets the value of the definition property.
     * 
     * @return
     *     possible object is
     *     {@link ParameterCollection }
     *     
     */
    public ParameterCollection getDefinition() {
        return definition;
    }

    /**
     * Sets the value of the definition property.
     * 
     * @param value
     *     allowed object is
     *     {@link ParameterCollection }
     *     
     */
    public void setDefinition(ParameterCollection value) {
        this.definition = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof QueryParameters)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final QueryParameters that = ((QueryParameters) object);
        {
            QueryParametersReference lhsReference;
            lhsReference = this.getReference();
            QueryParametersReference rhsReference;
            rhsReference = that.getReference();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "reference", lhsReference), LocatorUtils.property(thatLocator, "reference", rhsReference), lhsReference, rhsReference)) {
                return false;
            }
        }
        {
            ParameterCollection lhsDefinition;
            lhsDefinition = this.getDefinition();
            ParameterCollection rhsDefinition;
            rhsDefinition = that.getDefinition();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "definition", lhsDefinition), LocatorUtils.property(thatLocator, "definition", rhsDefinition), lhsDefinition, rhsDefinition)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof QueryParameters) {
            final QueryParameters copy = ((QueryParameters) draftCopy);
            if (this.reference!= null) {
                QueryParametersReference sourceReference;
                sourceReference = this.getReference();
                QueryParametersReference copyReference = ((QueryParametersReference) strategy.copy(LocatorUtils.property(locator, "reference", sourceReference), sourceReference));
                copy.setReference(copyReference);
            } else {
                copy.reference = null;
            }
            if (this.definition!= null) {
                ParameterCollection sourceDefinition;
                sourceDefinition = this.getDefinition();
                ParameterCollection copyDefinition = ((ParameterCollection) strategy.copy(LocatorUtils.property(locator, "definition", sourceDefinition), sourceDefinition));
                copy.setDefinition(copyDefinition);
            } else {
                copy.definition = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new QueryParameters();
    }

}
