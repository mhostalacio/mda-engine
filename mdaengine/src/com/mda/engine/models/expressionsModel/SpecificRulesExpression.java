
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for SpecificRulesExpression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SpecificRulesExpression">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="RuleName" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}RuleNameExpression" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpecificRulesExpression", propOrder = {
    "ruleName"
})
public class SpecificRulesExpression
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "RuleName")
    protected List<RuleNameExpression> ruleName;

    /**
     * Gets the value of the ruleName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ruleName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRuleName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RuleNameExpression }
     * 
     * 
     */
    public List<RuleNameExpression> getRuleName() {
        if (ruleName == null) {
            ruleName = new ArrayList<RuleNameExpression>();
        }
        return this.ruleName;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof SpecificRulesExpression)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final SpecificRulesExpression that = ((SpecificRulesExpression) object);
        {
            List<RuleNameExpression> lhsRuleName;
            lhsRuleName = this.getRuleName();
            List<RuleNameExpression> rhsRuleName;
            rhsRuleName = that.getRuleName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ruleName", lhsRuleName), LocatorUtils.property(thatLocator, "ruleName", rhsRuleName), lhsRuleName, rhsRuleName)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof SpecificRulesExpression) {
            final SpecificRulesExpression copy = ((SpecificRulesExpression) draftCopy);
            if ((this.ruleName!= null)&&(!this.ruleName.isEmpty())) {
                List<RuleNameExpression> sourceRuleName;
                sourceRuleName = this.getRuleName();
                @SuppressWarnings("unchecked")
                List<RuleNameExpression> copyRuleName = ((List<RuleNameExpression> ) strategy.copy(LocatorUtils.property(locator, "ruleName", sourceRuleName), sourceRuleName));
                copy.ruleName = null;
                List<RuleNameExpression> uniqueRuleNamel = copy.getRuleName();
                uniqueRuleNamel.addAll(copyRuleName);
            } else {
                copy.ruleName = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new SpecificRulesExpression();
    }

}
