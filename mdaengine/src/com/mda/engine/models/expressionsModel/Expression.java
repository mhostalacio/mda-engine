
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;


/**
 * <p>Java class for Expression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Expression">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Expression")
@XmlSeeAlso({
    CommonSettingsPropertyExpression.class,
    TranslatorExpression.class,
    IsSupportControllerExpression.class,
    IfExpression.class,
    NullExpression.class,
    ElseExpression.class,
    CurrentContextUrlExpression.class,
    MethodArgumentsExpression.class,
    UserContextPropertyExpression.class,
    TernaryOperatorExpression.class,
    BOMtoArrayExpression.class,
    PIValueExpression.class,
    StringBuilderExpression.class,
    ScreenCodeExpression.class,
    DelegateValuePropertyExpression.class,
    CurrentPerspectiveExpression.class,
    CurrentUrlExpression.class,
    ExecuteMethodExpression.class,
    ApplicationPathExpression.class,
    RegexExpression.class,
    PreviousContextLabelExpression.class,
    BooleanExpressionCollection.class,
    DataMaskPropertyExpression.class,
    InvokeBusinessTransactionExpression.class,
    TableColumnExpression.class,
    BooleanExpressionChoice.class,
    QueryParameterExpression.class,
    LiteralExpression.class,
    StringFormatExpression.class,
    InvokeQueryAction.class,
    PreviousContextUrlExpression.class,
    BTContextPropertyExpression.class,
    ResourceExpression.class,
    AddItemsToListAction.class,
    TaskOutputValueExpression.class,
    ModelKeyExpression.class,
    ServerAddressExpression.class,
    ProcessDataItemExpression.class,
    BTContextExpression.class,
    DateTimeExpression.class,
    EventArgumentExpression.class,
    IsInStateExpression.class,
    DoubleArgumentsExpression.class,
    BOMExpression.class,
    ValueExpression.class,
    UserRuntimeSettingExpression.class,
    CollectionExpression.class,
    ExpressionChoice.class,
    BPMNProcessInstanceData.class,
    ExpressionCollection.class,
    MVCExpression.class
})
public class Expression
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Expression)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Expression();
    }
    
//--simple--preserve

  	public void writeSQLExpression(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product) {
  		throw new org.apache.commons.lang.NotImplementedException("Expression :" + this.getClass().getSimpleName());
  	}
  	
  	public String getCSharpExpression(com.mda.engine.core.Product product){
  		throw new org.apache.commons.lang.NotImplementedException("Expression :" + this.getClass().getSimpleName());
  	}   
  	
//--simple--preserve

}
