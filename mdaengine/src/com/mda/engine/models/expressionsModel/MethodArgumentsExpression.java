
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MethodArgumentsExpression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MethodArgumentsExpression">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}Expression">
 *       &lt;choice>
 *         &lt;element name="BooleanValue" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}BooleanValueExpression"/>
 *         &lt;element name="StringValue" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}StringValueExpression"/>
 *         &lt;element name="ShortValue" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ShortValueExpression"/>
 *         &lt;element name="IntValue" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IntValueExpression"/>
 *         &lt;element name="Literal" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}LiteralExpression"/>
 *         &lt;element name="Model" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}BOMExpression"/>
 *         &lt;element name="EnumValue" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}EnumValueExpression"/>
 *       &lt;/choice>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MethodArgumentsExpression", propOrder = {
    "booleanValue",
    "stringValue",
    "shortValue",
    "intValue",
    "literal",
    "model",
    "enumValue"
})
public class MethodArgumentsExpression
    extends Expression
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "BooleanValue")
    protected BooleanValueExpression booleanValue;
    @XmlElement(name = "StringValue")
    protected StringValueExpression stringValue;
    @XmlElement(name = "ShortValue")
    protected ShortValueExpression shortValue;
    @XmlElement(name = "IntValue")
    protected IntValueExpression intValue;
    @XmlElement(name = "Literal")
    protected LiteralExpression literal;
    @XmlElement(name = "Model")
    protected BOMExpression model;
    @XmlElement(name = "EnumValue")
    protected EnumValueExpression enumValue;

    /**
     * Gets the value of the booleanValue property.
     * 
     * @return
     *     possible object is
     *     {@link BooleanValueExpression }
     *     
     */
    public BooleanValueExpression getBooleanValue() {
        return booleanValue;
    }

    /**
     * Sets the value of the booleanValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BooleanValueExpression }
     *     
     */
    public void setBooleanValue(BooleanValueExpression value) {
        this.booleanValue = value;
    }

    /**
     * Gets the value of the stringValue property.
     * 
     * @return
     *     possible object is
     *     {@link StringValueExpression }
     *     
     */
    public StringValueExpression getStringValue() {
        return stringValue;
    }

    /**
     * Sets the value of the stringValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link StringValueExpression }
     *     
     */
    public void setStringValue(StringValueExpression value) {
        this.stringValue = value;
    }

    /**
     * Gets the value of the shortValue property.
     * 
     * @return
     *     possible object is
     *     {@link ShortValueExpression }
     *     
     */
    public ShortValueExpression getShortValue() {
        return shortValue;
    }

    /**
     * Sets the value of the shortValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShortValueExpression }
     *     
     */
    public void setShortValue(ShortValueExpression value) {
        this.shortValue = value;
    }

    /**
     * Gets the value of the intValue property.
     * 
     * @return
     *     possible object is
     *     {@link IntValueExpression }
     *     
     */
    public IntValueExpression getIntValue() {
        return intValue;
    }

    /**
     * Sets the value of the intValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntValueExpression }
     *     
     */
    public void setIntValue(IntValueExpression value) {
        this.intValue = value;
    }

    /**
     * Gets the value of the literal property.
     * 
     * @return
     *     possible object is
     *     {@link LiteralExpression }
     *     
     */
    public LiteralExpression getLiteral() {
        return literal;
    }

    /**
     * Sets the value of the literal property.
     * 
     * @param value
     *     allowed object is
     *     {@link LiteralExpression }
     *     
     */
    public void setLiteral(LiteralExpression value) {
        this.literal = value;
    }

    /**
     * Gets the value of the model property.
     * 
     * @return
     *     possible object is
     *     {@link BOMExpression }
     *     
     */
    public BOMExpression getModel() {
        return model;
    }

    /**
     * Sets the value of the model property.
     * 
     * @param value
     *     allowed object is
     *     {@link BOMExpression }
     *     
     */
    public void setModel(BOMExpression value) {
        this.model = value;
    }

    /**
     * Gets the value of the enumValue property.
     * 
     * @return
     *     possible object is
     *     {@link EnumValueExpression }
     *     
     */
    public EnumValueExpression getEnumValue() {
        return enumValue;
    }

    /**
     * Sets the value of the enumValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnumValueExpression }
     *     
     */
    public void setEnumValue(EnumValueExpression value) {
        this.enumValue = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MethodArgumentsExpression)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MethodArgumentsExpression that = ((MethodArgumentsExpression) object);
        {
            BooleanValueExpression lhsBooleanValue;
            lhsBooleanValue = this.getBooleanValue();
            BooleanValueExpression rhsBooleanValue;
            rhsBooleanValue = that.getBooleanValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "booleanValue", lhsBooleanValue), LocatorUtils.property(thatLocator, "booleanValue", rhsBooleanValue), lhsBooleanValue, rhsBooleanValue)) {
                return false;
            }
        }
        {
            StringValueExpression lhsStringValue;
            lhsStringValue = this.getStringValue();
            StringValueExpression rhsStringValue;
            rhsStringValue = that.getStringValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "stringValue", lhsStringValue), LocatorUtils.property(thatLocator, "stringValue", rhsStringValue), lhsStringValue, rhsStringValue)) {
                return false;
            }
        }
        {
            ShortValueExpression lhsShortValue;
            lhsShortValue = this.getShortValue();
            ShortValueExpression rhsShortValue;
            rhsShortValue = that.getShortValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "shortValue", lhsShortValue), LocatorUtils.property(thatLocator, "shortValue", rhsShortValue), lhsShortValue, rhsShortValue)) {
                return false;
            }
        }
        {
            IntValueExpression lhsIntValue;
            lhsIntValue = this.getIntValue();
            IntValueExpression rhsIntValue;
            rhsIntValue = that.getIntValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "intValue", lhsIntValue), LocatorUtils.property(thatLocator, "intValue", rhsIntValue), lhsIntValue, rhsIntValue)) {
                return false;
            }
        }
        {
            LiteralExpression lhsLiteral;
            lhsLiteral = this.getLiteral();
            LiteralExpression rhsLiteral;
            rhsLiteral = that.getLiteral();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "literal", lhsLiteral), LocatorUtils.property(thatLocator, "literal", rhsLiteral), lhsLiteral, rhsLiteral)) {
                return false;
            }
        }
        {
            BOMExpression lhsModel;
            lhsModel = this.getModel();
            BOMExpression rhsModel;
            rhsModel = that.getModel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "model", lhsModel), LocatorUtils.property(thatLocator, "model", rhsModel), lhsModel, rhsModel)) {
                return false;
            }
        }
        {
            EnumValueExpression lhsEnumValue;
            lhsEnumValue = this.getEnumValue();
            EnumValueExpression rhsEnumValue;
            rhsEnumValue = that.getEnumValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "enumValue", lhsEnumValue), LocatorUtils.property(thatLocator, "enumValue", rhsEnumValue), lhsEnumValue, rhsEnumValue)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MethodArgumentsExpression) {
            final MethodArgumentsExpression copy = ((MethodArgumentsExpression) draftCopy);
            if (this.booleanValue!= null) {
                BooleanValueExpression sourceBooleanValue;
                sourceBooleanValue = this.getBooleanValue();
                BooleanValueExpression copyBooleanValue = ((BooleanValueExpression) strategy.copy(LocatorUtils.property(locator, "booleanValue", sourceBooleanValue), sourceBooleanValue));
                copy.setBooleanValue(copyBooleanValue);
            } else {
                copy.booleanValue = null;
            }
            if (this.stringValue!= null) {
                StringValueExpression sourceStringValue;
                sourceStringValue = this.getStringValue();
                StringValueExpression copyStringValue = ((StringValueExpression) strategy.copy(LocatorUtils.property(locator, "stringValue", sourceStringValue), sourceStringValue));
                copy.setStringValue(copyStringValue);
            } else {
                copy.stringValue = null;
            }
            if (this.shortValue!= null) {
                ShortValueExpression sourceShortValue;
                sourceShortValue = this.getShortValue();
                ShortValueExpression copyShortValue = ((ShortValueExpression) strategy.copy(LocatorUtils.property(locator, "shortValue", sourceShortValue), sourceShortValue));
                copy.setShortValue(copyShortValue);
            } else {
                copy.shortValue = null;
            }
            if (this.intValue!= null) {
                IntValueExpression sourceIntValue;
                sourceIntValue = this.getIntValue();
                IntValueExpression copyIntValue = ((IntValueExpression) strategy.copy(LocatorUtils.property(locator, "intValue", sourceIntValue), sourceIntValue));
                copy.setIntValue(copyIntValue);
            } else {
                copy.intValue = null;
            }
            if (this.literal!= null) {
                LiteralExpression sourceLiteral;
                sourceLiteral = this.getLiteral();
                LiteralExpression copyLiteral = ((LiteralExpression) strategy.copy(LocatorUtils.property(locator, "literal", sourceLiteral), sourceLiteral));
                copy.setLiteral(copyLiteral);
            } else {
                copy.literal = null;
            }
            if (this.model!= null) {
                BOMExpression sourceModel;
                sourceModel = this.getModel();
                BOMExpression copyModel = ((BOMExpression) strategy.copy(LocatorUtils.property(locator, "model", sourceModel), sourceModel));
                copy.setModel(copyModel);
            } else {
                copy.model = null;
            }
            if (this.enumValue!= null) {
                EnumValueExpression sourceEnumValue;
                sourceEnumValue = this.getEnumValue();
                EnumValueExpression copyEnumValue = ((EnumValueExpression) strategy.copy(LocatorUtils.property(locator, "enumValue", sourceEnumValue), sourceEnumValue));
                copy.setEnumValue(copyEnumValue);
            } else {
                copy.enumValue = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MethodArgumentsExpression();
    }

}
