
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ElseExpression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ElseExpression">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}Expression">
 *       &lt;sequence>
 *         &lt;element name="Expression" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ExpressionChoice" minOccurs="0"/>
 *         &lt;element name="Then" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ExpressionChoice" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ElseExpression", propOrder = {
    "expression",
    "then"
})
public class ElseExpression
    extends Expression
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Expression")
    protected ExpressionChoice expression;
    @XmlElement(name = "Then")
    protected ExpressionChoice then;

    /**
     * Gets the value of the expression property.
     * 
     * @return
     *     possible object is
     *     {@link ExpressionChoice }
     *     
     */
    public ExpressionChoice getExpression() {
        return expression;
    }

    /**
     * Sets the value of the expression property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpressionChoice }
     *     
     */
    public void setExpression(ExpressionChoice value) {
        this.expression = value;
    }

    /**
     * Gets the value of the then property.
     * 
     * @return
     *     possible object is
     *     {@link ExpressionChoice }
     *     
     */
    public ExpressionChoice getThen() {
        return then;
    }

    /**
     * Sets the value of the then property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpressionChoice }
     *     
     */
    public void setThen(ExpressionChoice value) {
        this.then = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ElseExpression)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final ElseExpression that = ((ElseExpression) object);
        {
            ExpressionChoice lhsExpression;
            lhsExpression = this.getExpression();
            ExpressionChoice rhsExpression;
            rhsExpression = that.getExpression();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "expression", lhsExpression), LocatorUtils.property(thatLocator, "expression", rhsExpression), lhsExpression, rhsExpression)) {
                return false;
            }
        }
        {
            ExpressionChoice lhsThen;
            lhsThen = this.getThen();
            ExpressionChoice rhsThen;
            rhsThen = that.getThen();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "then", lhsThen), LocatorUtils.property(thatLocator, "then", rhsThen), lhsThen, rhsThen)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof ElseExpression) {
            final ElseExpression copy = ((ElseExpression) draftCopy);
            if (this.expression!= null) {
                ExpressionChoice sourceExpression;
                sourceExpression = this.getExpression();
                ExpressionChoice copyExpression = ((ExpressionChoice) strategy.copy(LocatorUtils.property(locator, "expression", sourceExpression), sourceExpression));
                copy.setExpression(copyExpression);
            } else {
                copy.expression = null;
            }
            if (this.then!= null) {
                ExpressionChoice sourceThen;
                sourceThen = this.getThen();
                ExpressionChoice copyThen = ((ExpressionChoice) strategy.copy(LocatorUtils.property(locator, "then", sourceThen), sourceThen));
                copy.setThen(copyThen);
            } else {
                copy.then = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ElseExpression();
    }

}
