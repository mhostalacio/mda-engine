
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.models.common.QueryReference;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for InvokeQueryAction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InvokeQueryAction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}Expression">
 *       &lt;sequence>
 *         &lt;element name="QueryToInvoke" type="{http://www.mdaengine.com/mdaengine/models/common}QueryReference"/>
 *         &lt;element name="Parameters" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}InvokeQueryActionParameterCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="InstantiateParameterClass" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvokeQueryAction", propOrder = {
    "queryToInvoke",
    "parameters"
})
public class InvokeQueryAction
    extends Expression
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "QueryToInvoke", required = true)
    protected QueryReference queryToInvoke;
    @XmlElement(name = "Parameters")
    protected InvokeQueryActionParameterCollection parameters;
    @XmlAttribute(name = "InstantiateParameterClass")
    protected Boolean instantiateParameterClass;

    /**
     * Gets the value of the queryToInvoke property.
     * 
     * @return
     *     possible object is
     *     {@link QueryReference }
     *     
     */
    public QueryReference getQueryToInvoke() {
        return queryToInvoke;
    }

    /**
     * Sets the value of the queryToInvoke property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryReference }
     *     
     */
    public void setQueryToInvoke(QueryReference value) {
        this.queryToInvoke = value;
    }

    /**
     * Gets the value of the parameters property.
     * 
     * @return
     *     possible object is
     *     {@link InvokeQueryActionParameterCollection }
     *     
     */
    public InvokeQueryActionParameterCollection getParameters() {
        return parameters;
    }

    /**
     * Sets the value of the parameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link InvokeQueryActionParameterCollection }
     *     
     */
    public void setParameters(InvokeQueryActionParameterCollection value) {
        this.parameters = value;
    }

    /**
     * Gets the value of the instantiateParameterClass property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isInstantiateParameterClass() {
        return instantiateParameterClass;
    }

    /**
     * Sets the value of the instantiateParameterClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInstantiateParameterClass(Boolean value) {
        this.instantiateParameterClass = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof InvokeQueryAction)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final InvokeQueryAction that = ((InvokeQueryAction) object);
        {
            QueryReference lhsQueryToInvoke;
            lhsQueryToInvoke = this.getQueryToInvoke();
            QueryReference rhsQueryToInvoke;
            rhsQueryToInvoke = that.getQueryToInvoke();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "queryToInvoke", lhsQueryToInvoke), LocatorUtils.property(thatLocator, "queryToInvoke", rhsQueryToInvoke), lhsQueryToInvoke, rhsQueryToInvoke)) {
                return false;
            }
        }
        {
            InvokeQueryActionParameterCollection lhsParameters;
            lhsParameters = this.getParameters();
            InvokeQueryActionParameterCollection rhsParameters;
            rhsParameters = that.getParameters();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "parameters", lhsParameters), LocatorUtils.property(thatLocator, "parameters", rhsParameters), lhsParameters, rhsParameters)) {
                return false;
            }
        }
        {
            Boolean lhsInstantiateParameterClass;
            lhsInstantiateParameterClass = this.isInstantiateParameterClass();
            Boolean rhsInstantiateParameterClass;
            rhsInstantiateParameterClass = that.isInstantiateParameterClass();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "instantiateParameterClass", lhsInstantiateParameterClass), LocatorUtils.property(thatLocator, "instantiateParameterClass", rhsInstantiateParameterClass), lhsInstantiateParameterClass, rhsInstantiateParameterClass)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof InvokeQueryAction) {
            final InvokeQueryAction copy = ((InvokeQueryAction) draftCopy);
            if (this.queryToInvoke!= null) {
                QueryReference sourceQueryToInvoke;
                sourceQueryToInvoke = this.getQueryToInvoke();
                QueryReference copyQueryToInvoke = ((QueryReference) strategy.copy(LocatorUtils.property(locator, "queryToInvoke", sourceQueryToInvoke), sourceQueryToInvoke));
                copy.setQueryToInvoke(copyQueryToInvoke);
            } else {
                copy.queryToInvoke = null;
            }
            if (this.parameters!= null) {
                InvokeQueryActionParameterCollection sourceParameters;
                sourceParameters = this.getParameters();
                InvokeQueryActionParameterCollection copyParameters = ((InvokeQueryActionParameterCollection) strategy.copy(LocatorUtils.property(locator, "parameters", sourceParameters), sourceParameters));
                copy.setParameters(copyParameters);
            } else {
                copy.parameters = null;
            }
            if (this.instantiateParameterClass!= null) {
                Boolean sourceInstantiateParameterClass;
                sourceInstantiateParameterClass = this.isInstantiateParameterClass();
                Boolean copyInstantiateParameterClass = ((Boolean) strategy.copy(LocatorUtils.property(locator, "instantiateParameterClass", sourceInstantiateParameterClass), sourceInstantiateParameterClass));
                copy.setInstantiateParameterClass(copyInstantiateParameterClass);
            } else {
                copy.instantiateParameterClass = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new InvokeQueryAction();
    }

}
