
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for InvokeBusinessTransactionExpressionInputCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InvokeBusinessTransactionExpressionInputCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Input" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}InvokeBusinessTransactionExpressionInput" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvokeBusinessTransactionExpressionInputCollection", propOrder = {
    "input"
})
public class InvokeBusinessTransactionExpressionInputCollection
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Input", required = true)
    protected List<InvokeBusinessTransactionExpressionInput> input;

    /**
     * Gets the value of the input property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the input property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInput().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvokeBusinessTransactionExpressionInput }
     * 
     * 
     */
    public List<InvokeBusinessTransactionExpressionInput> getInput() {
        if (input == null) {
            input = new ArrayList<InvokeBusinessTransactionExpressionInput>();
        }
        return this.input;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof InvokeBusinessTransactionExpressionInputCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final InvokeBusinessTransactionExpressionInputCollection that = ((InvokeBusinessTransactionExpressionInputCollection) object);
        {
            List<InvokeBusinessTransactionExpressionInput> lhsInput;
            lhsInput = this.getInput();
            List<InvokeBusinessTransactionExpressionInput> rhsInput;
            rhsInput = that.getInput();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "input", lhsInput), LocatorUtils.property(thatLocator, "input", rhsInput), lhsInput, rhsInput)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof InvokeBusinessTransactionExpressionInputCollection) {
            final InvokeBusinessTransactionExpressionInputCollection copy = ((InvokeBusinessTransactionExpressionInputCollection) draftCopy);
            if ((this.input!= null)&&(!this.input.isEmpty())) {
                List<InvokeBusinessTransactionExpressionInput> sourceInput;
                sourceInput = this.getInput();
                @SuppressWarnings("unchecked")
                List<InvokeBusinessTransactionExpressionInput> copyInput = ((List<InvokeBusinessTransactionExpressionInput> ) strategy.copy(LocatorUtils.property(locator, "input", sourceInput), sourceInput));
                copy.input = null;
                List<InvokeBusinessTransactionExpressionInput> uniqueInputl = copy.getInput();
                uniqueInputl.addAll(copyInput);
            } else {
                copy.input = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new InvokeBusinessTransactionExpressionInputCollection();
    }

}
