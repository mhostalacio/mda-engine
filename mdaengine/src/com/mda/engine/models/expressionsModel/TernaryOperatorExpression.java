
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for TernaryOperatorExpression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TernaryOperatorExpression">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}Expression">
 *       &lt;sequence>
 *         &lt;element name="If" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}BooleanExpressionChoice"/>
 *         &lt;element name="Then" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ExpressionChoice"/>
 *         &lt;element name="Else" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ExpressionChoice"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TernaryOperatorExpression", propOrder = {
    "_if",
    "then",
    "_else"
})
public class TernaryOperatorExpression
    extends Expression
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "If", required = true)
    protected BooleanExpressionChoice _if;
    @XmlElement(name = "Then", required = true)
    protected ExpressionChoice then;
    @XmlElement(name = "Else", required = true)
    protected ExpressionChoice _else;

    /**
     * Gets the value of the if property.
     * 
     * @return
     *     possible object is
     *     {@link BooleanExpressionChoice }
     *     
     */
    public BooleanExpressionChoice getIf() {
        return _if;
    }

    /**
     * Sets the value of the if property.
     * 
     * @param value
     *     allowed object is
     *     {@link BooleanExpressionChoice }
     *     
     */
    public void setIf(BooleanExpressionChoice value) {
        this._if = value;
    }

    /**
     * Gets the value of the then property.
     * 
     * @return
     *     possible object is
     *     {@link ExpressionChoice }
     *     
     */
    public ExpressionChoice getThen() {
        return then;
    }

    /**
     * Sets the value of the then property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpressionChoice }
     *     
     */
    public void setThen(ExpressionChoice value) {
        this.then = value;
    }

    /**
     * Gets the value of the else property.
     * 
     * @return
     *     possible object is
     *     {@link ExpressionChoice }
     *     
     */
    public ExpressionChoice getElse() {
        return _else;
    }

    /**
     * Sets the value of the else property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpressionChoice }
     *     
     */
    public void setElse(ExpressionChoice value) {
        this._else = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof TernaryOperatorExpression)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final TernaryOperatorExpression that = ((TernaryOperatorExpression) object);
        {
            BooleanExpressionChoice lhsIf;
            lhsIf = this.getIf();
            BooleanExpressionChoice rhsIf;
            rhsIf = that.getIf();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "_if", lhsIf), LocatorUtils.property(thatLocator, "_if", rhsIf), lhsIf, rhsIf)) {
                return false;
            }
        }
        {
            ExpressionChoice lhsThen;
            lhsThen = this.getThen();
            ExpressionChoice rhsThen;
            rhsThen = that.getThen();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "then", lhsThen), LocatorUtils.property(thatLocator, "then", rhsThen), lhsThen, rhsThen)) {
                return false;
            }
        }
        {
            ExpressionChoice lhsElse;
            lhsElse = this.getElse();
            ExpressionChoice rhsElse;
            rhsElse = that.getElse();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "_else", lhsElse), LocatorUtils.property(thatLocator, "_else", rhsElse), lhsElse, rhsElse)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof TernaryOperatorExpression) {
            final TernaryOperatorExpression copy = ((TernaryOperatorExpression) draftCopy);
            if (this._if!= null) {
                BooleanExpressionChoice sourceIf;
                sourceIf = this.getIf();
                BooleanExpressionChoice copyIf = ((BooleanExpressionChoice) strategy.copy(LocatorUtils.property(locator, "_if", sourceIf), sourceIf));
                copy.setIf(copyIf);
            } else {
                copy._if = null;
            }
            if (this.then!= null) {
                ExpressionChoice sourceThen;
                sourceThen = this.getThen();
                ExpressionChoice copyThen = ((ExpressionChoice) strategy.copy(LocatorUtils.property(locator, "then", sourceThen), sourceThen));
                copy.setThen(copyThen);
            } else {
                copy.then = null;
            }
            if (this._else!= null) {
                ExpressionChoice sourceElse;
                sourceElse = this.getElse();
                ExpressionChoice copyElse = ((ExpressionChoice) strategy.copy(LocatorUtils.property(locator, "_else", sourceElse), sourceElse));
                copy.setElse(copyElse);
            } else {
                copy._else = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new TernaryOperatorExpression();
    }

}
