
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for BooleanExpressionChoice complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BooleanExpressionChoice">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}Expression">
 *       &lt;choice>
 *         &lt;element name="AND" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}AndExpression"/>
 *         &lt;element name="OR" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}OrExpression"/>
 *         &lt;element name="Not" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}NotExpression"/>
 *         &lt;element name="IsNull" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IsNullExpression"/>
 *         &lt;element name="HasValue" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}HasValueExpression"/>
 *         &lt;element name="Equals" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}EqualsExpression"/>
 *         &lt;element name="NotEquals" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}NotEqualsExpression"/>
 *         &lt;element name="GreaterThan" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}GreaterThanExpression"/>
 *         &lt;element name="GreaterOrEqualThan" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}GreaterOrEqualThanExpression"/>
 *         &lt;element name="LowerThan" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}LowerThanExpression"/>
 *         &lt;element name="LowerOrEqualThan" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}LowerOrEqualThanExpression"/>
 *         &lt;element name="Like" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}LikeExpression"/>
 *         &lt;element name="IsNew" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IsNewExpression"/>
 *         &lt;element name="IsDirty" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IsDirtyExpression"/>
 *         &lt;element name="IsRequired" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IsRequiredExpression"/>
 *         &lt;element name="IsEnable" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IsEnableExpression"/>
 *         &lt;element name="IsVisible" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IsVisibleExpression"/>
 *         &lt;element name="IsValid" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IsValidExpression"/>
 *         &lt;element name="IsTrue" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IsTrueExpression"/>
 *         &lt;element name="Regex" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}RegexExpression"/>
 *         &lt;element name="GetValue" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}GetValueExpression"/>
 *         &lt;element name="Contains" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ContainsValueExpression"/>
 *         &lt;element name="IsInState" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IsInStateExpression"/>
 *       &lt;/choice>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BooleanExpressionChoice", propOrder = {
    "expression"
})
public class BooleanExpressionChoice
    extends Expression
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElements({
        @XmlElement(name = "IsInState", type = IsInStateExpression.class),
        @XmlElement(name = "HasValue", type = HasValueExpression.class),
        @XmlElement(name = "IsRequired", type = IsRequiredExpression.class),
        @XmlElement(name = "IsValid", type = IsValidExpression.class),
        @XmlElement(name = "OR", type = OrExpression.class),
        @XmlElement(name = "NotEquals", type = NotEqualsExpression.class),
        @XmlElement(name = "IsTrue", type = IsTrueExpression.class),
        @XmlElement(name = "AND", type = AndExpression.class),
        @XmlElement(name = "Equals", type = EqualsExpression.class),
        @XmlElement(name = "Like", type = LikeExpression.class),
        @XmlElement(name = "GreaterThan", type = GreaterThanExpression.class),
        @XmlElement(name = "GetValue", type = GetValueExpression.class),
        @XmlElement(name = "IsDirty", type = IsDirtyExpression.class),
        @XmlElement(name = "IsNew", type = IsNewExpression.class),
        @XmlElement(name = "IsVisible", type = IsVisibleExpression.class),
        @XmlElement(name = "Contains", type = ContainsValueExpression.class),
        @XmlElement(name = "LowerThan", type = LowerThanExpression.class),
        @XmlElement(name = "Regex", type = RegexExpression.class),
        @XmlElement(name = "Not", type = NotExpression.class),
        @XmlElement(name = "LowerOrEqualThan", type = LowerOrEqualThanExpression.class),
        @XmlElement(name = "IsNull", type = IsNullExpression.class),
        @XmlElement(name = "GreaterOrEqualThan", type = GreaterOrEqualThanExpression.class),
        @XmlElement(name = "IsEnable", type = IsEnableExpression.class)
    })
    protected Expression expression;

    /**
     * Gets the value of the expression property.
     * 
     * @return
     *     possible object is
     *     {@link IsInStateExpression }
     *     {@link HasValueExpression }
     *     {@link IsRequiredExpression }
     *     {@link IsValidExpression }
     *     {@link OrExpression }
     *     {@link NotEqualsExpression }
     *     {@link IsTrueExpression }
     *     {@link AndExpression }
     *     {@link EqualsExpression }
     *     {@link LikeExpression }
     *     {@link GreaterThanExpression }
     *     {@link GetValueExpression }
     *     {@link IsDirtyExpression }
     *     {@link IsNewExpression }
     *     {@link IsVisibleExpression }
     *     {@link ContainsValueExpression }
     *     {@link LowerThanExpression }
     *     {@link RegexExpression }
     *     {@link NotExpression }
     *     {@link LowerOrEqualThanExpression }
     *     {@link IsNullExpression }
     *     {@link GreaterOrEqualThanExpression }
     *     {@link IsEnableExpression }
     *     
     */
    public Expression getExpression() {
        return expression;
    }

    /**
     * Sets the value of the expression property.
     * 
     * @param value
     *     allowed object is
     *     {@link IsInStateExpression }
     *     {@link HasValueExpression }
     *     {@link IsRequiredExpression }
     *     {@link IsValidExpression }
     *     {@link OrExpression }
     *     {@link NotEqualsExpression }
     *     {@link IsTrueExpression }
     *     {@link AndExpression }
     *     {@link EqualsExpression }
     *     {@link LikeExpression }
     *     {@link GreaterThanExpression }
     *     {@link GetValueExpression }
     *     {@link IsDirtyExpression }
     *     {@link IsNewExpression }
     *     {@link IsVisibleExpression }
     *     {@link ContainsValueExpression }
     *     {@link LowerThanExpression }
     *     {@link RegexExpression }
     *     {@link NotExpression }
     *     {@link LowerOrEqualThanExpression }
     *     {@link IsNullExpression }
     *     {@link GreaterOrEqualThanExpression }
     *     {@link IsEnableExpression }
     *     
     */
    public void setExpression(Expression value) {
        this.expression = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof BooleanExpressionChoice)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final BooleanExpressionChoice that = ((BooleanExpressionChoice) object);
        {
            Expression lhsExpression;
            lhsExpression = this.getExpression();
            Expression rhsExpression;
            rhsExpression = that.getExpression();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "expression", lhsExpression), LocatorUtils.property(thatLocator, "expression", rhsExpression), lhsExpression, rhsExpression)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof BooleanExpressionChoice) {
            final BooleanExpressionChoice copy = ((BooleanExpressionChoice) draftCopy);
            if (this.expression!= null) {
                Expression sourceExpression;
                sourceExpression = this.getExpression();
                Expression copyExpression = ((Expression) strategy.copy(LocatorUtils.property(locator, "expression", sourceExpression), sourceExpression));
                copy.setExpression(copyExpression);
            } else {
                copy.expression = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new BooleanExpressionChoice();
    }

}
