
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewModelJSONExpression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewModelJSONExpression">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}MVCExpression">
 *       &lt;sequence>
 *         &lt;element name="ViewModelName" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ExpressionChoice"/>
 *         &lt;element name="ViewModelKey" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ExpressionChoice" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewModelJSONExpression", propOrder = {
    "viewModelName",
    "viewModelKey"
})
public class MVCViewModelJSONExpression
    extends MVCExpression
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "ViewModelName", required = true)
    protected ExpressionChoice viewModelName;
    @XmlElement(name = "ViewModelKey")
    protected ExpressionChoice viewModelKey;

    /**
     * Gets the value of the viewModelName property.
     * 
     * @return
     *     possible object is
     *     {@link ExpressionChoice }
     *     
     */
    public ExpressionChoice getViewModelName() {
        return viewModelName;
    }

    /**
     * Sets the value of the viewModelName property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpressionChoice }
     *     
     */
    public void setViewModelName(ExpressionChoice value) {
        this.viewModelName = value;
    }

    /**
     * Gets the value of the viewModelKey property.
     * 
     * @return
     *     possible object is
     *     {@link ExpressionChoice }
     *     
     */
    public ExpressionChoice getViewModelKey() {
        return viewModelKey;
    }

    /**
     * Sets the value of the viewModelKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpressionChoice }
     *     
     */
    public void setViewModelKey(ExpressionChoice value) {
        this.viewModelKey = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewModelJSONExpression)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewModelJSONExpression that = ((MVCViewModelJSONExpression) object);
        {
            ExpressionChoice lhsViewModelName;
            lhsViewModelName = this.getViewModelName();
            ExpressionChoice rhsViewModelName;
            rhsViewModelName = that.getViewModelName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "viewModelName", lhsViewModelName), LocatorUtils.property(thatLocator, "viewModelName", rhsViewModelName), lhsViewModelName, rhsViewModelName)) {
                return false;
            }
        }
        {
            ExpressionChoice lhsViewModelKey;
            lhsViewModelKey = this.getViewModelKey();
            ExpressionChoice rhsViewModelKey;
            rhsViewModelKey = that.getViewModelKey();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "viewModelKey", lhsViewModelKey), LocatorUtils.property(thatLocator, "viewModelKey", rhsViewModelKey), lhsViewModelKey, rhsViewModelKey)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCViewModelJSONExpression) {
            final MVCViewModelJSONExpression copy = ((MVCViewModelJSONExpression) draftCopy);
            if (this.viewModelName!= null) {
                ExpressionChoice sourceViewModelName;
                sourceViewModelName = this.getViewModelName();
                ExpressionChoice copyViewModelName = ((ExpressionChoice) strategy.copy(LocatorUtils.property(locator, "viewModelName", sourceViewModelName), sourceViewModelName));
                copy.setViewModelName(copyViewModelName);
            } else {
                copy.viewModelName = null;
            }
            if (this.viewModelKey!= null) {
                ExpressionChoice sourceViewModelKey;
                sourceViewModelKey = this.getViewModelKey();
                ExpressionChoice copyViewModelKey = ((ExpressionChoice) strategy.copy(LocatorUtils.property(locator, "viewModelKey", sourceViewModelKey), sourceViewModelKey));
                copy.setViewModelKey(copyViewModelKey);
            } else {
                copy.viewModelKey = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewModelJSONExpression();
    }

}
