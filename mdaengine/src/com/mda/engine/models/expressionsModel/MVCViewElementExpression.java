
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.models.actionsModel.RefreshElementsActionElementExpression;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewElementExpression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewElementExpression">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}MVCViewExpression">
 *       &lt;attribute name="ElementId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewElementExpression")
@XmlSeeAlso({
    MVCViewElementSelectedIndexExpression.class,
    MVCViewCheckBoxListExpression.class,
    MVCViewElementValueExpression.class,
    RefreshElementsActionElementExpression.class
})
public class MVCViewElementExpression
    extends MVCViewExpression
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "ElementId", required = true)
    protected String elementId;

    /**
     * Gets the value of the elementId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getElementId() {
        return elementId;
    }

    /**
     * Sets the value of the elementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setElementId(String value) {
        this.elementId = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewElementExpression)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewElementExpression that = ((MVCViewElementExpression) object);
        {
            String lhsElementId;
            lhsElementId = this.getElementId();
            String rhsElementId;
            rhsElementId = that.getElementId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "elementId", lhsElementId), LocatorUtils.property(thatLocator, "elementId", rhsElementId), lhsElementId, rhsElementId)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCViewElementExpression) {
            final MVCViewElementExpression copy = ((MVCViewElementExpression) draftCopy);
            if (this.elementId!= null) {
                String sourceElementId;
                sourceElementId = this.getElementId();
                String copyElementId = ((String) strategy.copy(LocatorUtils.property(locator, "elementId", sourceElementId), sourceElementId));
                copy.setElementId(copyElementId);
            } else {
                copy.elementId = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewElementExpression();
    }

}
