
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.models.queryModel.OnClause;
import com.mda.engine.models.queryModel.WhereClause;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ExpressionCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ExpressionCollection">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}Expression">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;element name="AND" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}AndExpression"/>
 *         &lt;element name="OR" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}OrExpression"/>
 *         &lt;element name="Not" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}NotExpression"/>
 *         &lt;element name="IsNull" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IsNullExpression"/>
 *         &lt;element name="HasValue" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}HasValueExpression"/>
 *         &lt;element name="Equals" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}EqualsExpression"/>
 *         &lt;element name="NotEquals" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}NotEqualsExpression"/>
 *         &lt;element name="GreaterThan" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}GreaterThanExpression"/>
 *         &lt;element name="GreaterOrEqualThan" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}GreaterOrEqualThanExpression"/>
 *         &lt;element name="LowerThan" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}LowerThanExpression"/>
 *         &lt;element name="LowerOrEqualThan" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}LowerOrEqualThanExpression"/>
 *         &lt;element name="Like" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}LikeExpression"/>
 *         &lt;element name="IsNew" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IsNewExpression"/>
 *         &lt;element name="IsDirty" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IsDirtyExpression"/>
 *         &lt;element name="IsRequired" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IsRequiredExpression"/>
 *         &lt;element name="IsEnable" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IsEnableExpression"/>
 *         &lt;element name="IsVisible" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IsVisibleExpression"/>
 *         &lt;element name="IsTrue" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IsTrueExpression"/>
 *         &lt;element name="IsValid" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IsValidExpression"/>
 *         &lt;element name="Count" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}CountCollectionExpression"/>
 *         &lt;element name="First" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}FirstCollectionExpression"/>
 *         &lt;element name="FirstOrDefault" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}FirstOrDefaultCollectionExpression"/>
 *         &lt;element name="Last" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}LastCollectionExpression"/>
 *         &lt;element name="LastOrDefault" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}LastOrDefaultCollectionExpression"/>
 *         &lt;element name="GetItem" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}GetCollectionItemExpression"/>
 *         &lt;element name="GetItemOrDefault" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}GetCollectionItemOrDefaultExpression"/>
 *         &lt;element name="DateTimeNow" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}DateTimeNowExpression"/>
 *         &lt;element name="DateTimeAddDays" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}DateTimeAddDaysExpression"/>
 *         &lt;element name="DateTimeAddMonths" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}DateTimeAddMonthsExpression"/>
 *         &lt;element name="DateTimeAddYears" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}DateTimeAddYearsExpression"/>
 *         &lt;element name="Regex" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}RegexExpression"/>
 *         &lt;element name="BOM" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}BOMExpression"/>
 *         &lt;element name="ViewElement" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}MVCViewElementExpression"/>
 *         &lt;element name="HtmlElement" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}MVCHtmlElementExpression"/>
 *         &lt;element name="ElementValue" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}MVCViewElementValueExpression"/>
 *         &lt;element name="ElementSelectedIndex" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}MVCViewElementSelectedIndexExpression"/>
 *         &lt;element name="PerspectivePlaceholder" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}MVCViewPerspectivePlaceholderExpression"/>
 *         &lt;element name="CheckBoxList" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}MVCViewCheckBoxListExpression"/>
 *         &lt;element name="BooleanValue" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}BooleanValueExpression"/>
 *         &lt;element name="StringValue" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}StringValueExpression"/>
 *         &lt;element name="RowIndex" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}RowIndexExpression"/>
 *         &lt;element name="EnumValue" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}EnumValueExpression"/>
 *         &lt;element name="ShortValue" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ShortValueExpression"/>
 *         &lt;element name="IntValue" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IntValueExpression"/>
 *         &lt;element name="Literal" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}LiteralExpression"/>
 *         &lt;element name="StringBuilder" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}StringBuilderExpression"/>
 *         &lt;element name="StringFormat" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}StringFormatExpression"/>
 *         &lt;element name="PreviousContextLabel" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}PreviousContextLabelExpression"/>
 *         &lt;element name="PreviousContextUrl" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}PreviousContextUrlExpression"/>
 *         &lt;element name="ApplicationPath" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ApplicationPathExpression"/>
 *         &lt;element name="ServerAddress" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ServerAddressExpression"/>
 *         &lt;element name="ModelKey" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ModelKeyExpression"/>
 *         &lt;element name="InvokeQuery" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}InvokeQueryAction"/>
 *         &lt;element name="AddItemsToList" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}AddItemsToListAction"/>
 *         &lt;element name="InvokeBusinessTransaction" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}InvokeBusinessTransactionExpression"/>
 *         &lt;element name="Transform" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}TransformExpression"/>
 *         &lt;element name="ProcessData" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}BPMNProcessInstanceData"/>
 *         &lt;element name="If" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IfExpression"/>
 *         &lt;element name="TaskOutputValue" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}TaskOutputValueExpression"/>
 *         &lt;element name="CurrentPerspective" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}CurrentPerspectiveExpression"/>
 *         &lt;element name="PerspectiveValue" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}PerspectiveValueExpression"/>
 *         &lt;element name="Contains" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ContainsValueExpression"/>
 *         &lt;element name="EventArgument" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}EventArgumentExpression"/>
 *         &lt;element name="RequestProcessDataItem" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}RequestProcessDataItemExpression"/>
 *         &lt;element name="ReplyProcessDataItem" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ReplyProcessDataItemExpression"/>
 *         &lt;element name="IsInState" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IsInStateExpression"/>
 *         &lt;element name="Null" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}NullExpression"/>
 *         &lt;element name="ConfigurationValue" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ConfigurationValueExpression"/>
 *         &lt;element name="TernaryOperator" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}TernaryOperatorExpression"/>
 *         &lt;element name="BTContext" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}BTContextExpression"/>
 *         &lt;element name="BTContextProperty" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}BTContextPropertyExpression"/>
 *         &lt;element name="UserContext" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}UserContextPropertyExpression"/>
 *         &lt;element name="CommonSettings" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}CommonSettingsPropertyExpression"/>
 *         &lt;element name="Add" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}AddExpression"/>
 *         &lt;element name="Multiply" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}MulExpression"/>
 *         &lt;element name="Divide" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}DivExpression"/>
 *         &lt;element name="Minus" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}MinusExpression"/>
 *         &lt;element name="Min" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}MinExpression"/>
 *         &lt;element name="Max" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}MaxExpression"/>
 *         &lt;element name="Parentesis" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ParentesisExpression"/>
 *         &lt;element name="DoubleValue" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}DoubleValueExpression"/>
 *         &lt;element name="BOMtoArray" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}BOMtoArrayExpression"/>
 *         &lt;element name="DataMask" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}DataMaskPropertyExpression"/>
 *         &lt;element name="UserRuntimeSetting" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}UserRuntimeSettingExpression"/>
 *         &lt;element name="Return" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ReturnExpression"/>
 *         &lt;element name="TableColumn" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}TableColumnExpression"/>
 *         &lt;element name="QueryParameter" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}QueryParameterExpression"/>
 *       &lt;/choice>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExpressionCollection", propOrder = {
    "expressions"
})
@XmlSeeAlso({
    WhereClause.class,
    OnClause.class
})
public class ExpressionCollection
    extends Expression
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElements({
        @XmlElement(name = "If", type = IfExpression.class),
        @XmlElement(name = "NotEquals", type = NotEqualsExpression.class),
        @XmlElement(name = "GreaterOrEqualThan", type = GreaterOrEqualThanExpression.class),
        @XmlElement(name = "EventArgument", type = EventArgumentExpression.class),
        @XmlElement(name = "ShortValue", type = ShortValueExpression.class),
        @XmlElement(name = "GetItemOrDefault", type = GetCollectionItemOrDefaultExpression.class),
        @XmlElement(name = "Min", type = MinExpression.class),
        @XmlElement(name = "BooleanValue", type = BooleanValueExpression.class),
        @XmlElement(name = "ProcessData", type = BPMNProcessInstanceData.class),
        @XmlElement(name = "IsRequired", type = IsRequiredExpression.class),
        @XmlElement(name = "GreaterThan", type = GreaterThanExpression.class),
        @XmlElement(name = "ModelKey", type = ModelKeyExpression.class),
        @XmlElement(name = "ConfigurationValue", type = ConfigurationValueExpression.class),
        @XmlElement(name = "CheckBoxList", type = MVCViewCheckBoxListExpression.class),
        @XmlElement(name = "Null", type = NullExpression.class),
        @XmlElement(name = "Like", type = LikeExpression.class),
        @XmlElement(name = "IsValid", type = IsValidExpression.class),
        @XmlElement(name = "Not", type = NotExpression.class),
        @XmlElement(name = "RowIndex", type = RowIndexExpression.class),
        @XmlElement(name = "TaskOutputValue", type = TaskOutputValueExpression.class),
        @XmlElement(name = "InvokeBusinessTransaction", type = InvokeBusinessTransactionExpression.class),
        @XmlElement(name = "LastOrDefault", type = LastOrDefaultCollectionExpression.class),
        @XmlElement(name = "DateTimeNow", type = DateTimeNowExpression.class),
        @XmlElement(name = "Multiply", type = MulExpression.class),
        @XmlElement(name = "DataMask", type = DataMaskPropertyExpression.class),
        @XmlElement(name = "TernaryOperator", type = TernaryOperatorExpression.class),
        @XmlElement(name = "First", type = FirstCollectionExpression.class),
        @XmlElement(name = "Parentesis", type = ParentesisExpression.class),
        @XmlElement(name = "EnumValue", type = EnumValueExpression.class),
        @XmlElement(name = "CommonSettings", type = CommonSettingsPropertyExpression.class),
        @XmlElement(name = "Count", type = CountCollectionExpression.class),
        @XmlElement(name = "Last", type = LastCollectionExpression.class),
        @XmlElement(name = "Literal", type = LiteralExpression.class),
        @XmlElement(name = "IsTrue", type = IsTrueExpression.class),
        @XmlElement(name = "DateTimeAddDays", type = DateTimeAddDaysExpression.class),
        @XmlElement(name = "IntValue", type = IntValueExpression.class),
        @XmlElement(name = "Transform", type = TransformExpression.class),
        @XmlElement(name = "UserRuntimeSetting", type = UserRuntimeSettingExpression.class),
        @XmlElement(name = "ElementSelectedIndex", type = MVCViewElementSelectedIndexExpression.class),
        @XmlElement(name = "PerspectiveValue", type = PerspectiveValueExpression.class),
        @XmlElement(name = "Return", type = ReturnExpression.class),
        @XmlElement(name = "CurrentPerspective", type = CurrentPerspectiveExpression.class),
        @XmlElement(name = "Minus", type = MinusExpression.class),
        @XmlElement(name = "QueryParameter", type = QueryParameterExpression.class),
        @XmlElement(name = "AND", type = AndExpression.class),
        @XmlElement(name = "PerspectivePlaceholder", type = MVCViewPerspectivePlaceholderExpression.class),
        @XmlElement(name = "AddItemsToList", type = AddItemsToListAction.class),
        @XmlElement(name = "StringBuilder", type = StringBuilderExpression.class),
        @XmlElement(name = "Max", type = MaxExpression.class),
        @XmlElement(name = "Divide", type = DivExpression.class),
        @XmlElement(name = "StringValue", type = StringValueExpression.class),
        @XmlElement(name = "Contains", type = ContainsValueExpression.class),
        @XmlElement(name = "IsDirty", type = IsDirtyExpression.class),
        @XmlElement(name = "Regex", type = RegexExpression.class),
        @XmlElement(name = "ApplicationPath", type = ApplicationPathExpression.class),
        @XmlElement(name = "RequestProcessDataItem", type = RequestProcessDataItemExpression.class),
        @XmlElement(name = "OR", type = OrExpression.class),
        @XmlElement(name = "StringFormat", type = StringFormatExpression.class),
        @XmlElement(name = "FirstOrDefault", type = FirstOrDefaultCollectionExpression.class),
        @XmlElement(name = "ServerAddress", type = ServerAddressExpression.class),
        @XmlElement(name = "PreviousContextUrl", type = PreviousContextUrlExpression.class),
        @XmlElement(name = "Add", type = AddExpression.class),
        @XmlElement(name = "LowerThan", type = LowerThanExpression.class),
        @XmlElement(name = "InvokeQuery", type = InvokeQueryAction.class),
        @XmlElement(name = "PreviousContextLabel", type = PreviousContextLabelExpression.class),
        @XmlElement(name = "BOMtoArray", type = BOMtoArrayExpression.class),
        @XmlElement(name = "ViewElement", type = MVCViewElementExpression.class),
        @XmlElement(name = "ReplyProcessDataItem", type = ReplyProcessDataItemExpression.class),
        @XmlElement(name = "UserContext", type = UserContextPropertyExpression.class),
        @XmlElement(name = "IsNull", type = IsNullExpression.class),
        @XmlElement(name = "HasValue", type = HasValueExpression.class),
        @XmlElement(name = "DateTimeAddMonths", type = DateTimeAddMonthsExpression.class),
        @XmlElement(name = "LowerOrEqualThan", type = LowerOrEqualThanExpression.class),
        @XmlElement(name = "GetItem", type = GetCollectionItemExpression.class),
        @XmlElement(name = "BTContext", type = BTContextExpression.class),
        @XmlElement(name = "Equals", type = EqualsExpression.class),
        @XmlElement(name = "HtmlElement", type = MVCHtmlElementExpression.class),
        @XmlElement(name = "BOM", type = BOMExpression.class),
        @XmlElement(name = "TableColumn", type = TableColumnExpression.class),
        @XmlElement(name = "IsEnable", type = IsEnableExpression.class),
        @XmlElement(name = "IsInState", type = IsInStateExpression.class),
        @XmlElement(name = "IsNew", type = IsNewExpression.class),
        @XmlElement(name = "DoubleValue", type = DoubleValueExpression.class),
        @XmlElement(name = "ElementValue", type = MVCViewElementValueExpression.class),
        @XmlElement(name = "DateTimeAddYears", type = DateTimeAddYearsExpression.class),
        @XmlElement(name = "IsVisible", type = IsVisibleExpression.class),
        @XmlElement(name = "BTContextProperty", type = BTContextPropertyExpression.class)
    })
    protected List<Expression> expressions;

    /**
     * Gets the value of the expressions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the expressions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExpressions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IfExpression }
     * {@link NotEqualsExpression }
     * {@link GreaterOrEqualThanExpression }
     * {@link EventArgumentExpression }
     * {@link ShortValueExpression }
     * {@link GetCollectionItemOrDefaultExpression }
     * {@link MinExpression }
     * {@link BooleanValueExpression }
     * {@link BPMNProcessInstanceData }
     * {@link IsRequiredExpression }
     * {@link GreaterThanExpression }
     * {@link ModelKeyExpression }
     * {@link ConfigurationValueExpression }
     * {@link MVCViewCheckBoxListExpression }
     * {@link NullExpression }
     * {@link LikeExpression }
     * {@link IsValidExpression }
     * {@link NotExpression }
     * {@link RowIndexExpression }
     * {@link TaskOutputValueExpression }
     * {@link InvokeBusinessTransactionExpression }
     * {@link LastOrDefaultCollectionExpression }
     * {@link DateTimeNowExpression }
     * {@link MulExpression }
     * {@link DataMaskPropertyExpression }
     * {@link TernaryOperatorExpression }
     * {@link FirstCollectionExpression }
     * {@link ParentesisExpression }
     * {@link EnumValueExpression }
     * {@link CommonSettingsPropertyExpression }
     * {@link CountCollectionExpression }
     * {@link LastCollectionExpression }
     * {@link LiteralExpression }
     * {@link IsTrueExpression }
     * {@link DateTimeAddDaysExpression }
     * {@link IntValueExpression }
     * {@link TransformExpression }
     * {@link UserRuntimeSettingExpression }
     * {@link MVCViewElementSelectedIndexExpression }
     * {@link PerspectiveValueExpression }
     * {@link ReturnExpression }
     * {@link CurrentPerspectiveExpression }
     * {@link MinusExpression }
     * {@link QueryParameterExpression }
     * {@link AndExpression }
     * {@link MVCViewPerspectivePlaceholderExpression }
     * {@link AddItemsToListAction }
     * {@link StringBuilderExpression }
     * {@link MaxExpression }
     * {@link DivExpression }
     * {@link StringValueExpression }
     * {@link ContainsValueExpression }
     * {@link IsDirtyExpression }
     * {@link RegexExpression }
     * {@link ApplicationPathExpression }
     * {@link RequestProcessDataItemExpression }
     * {@link OrExpression }
     * {@link StringFormatExpression }
     * {@link FirstOrDefaultCollectionExpression }
     * {@link ServerAddressExpression }
     * {@link PreviousContextUrlExpression }
     * {@link AddExpression }
     * {@link LowerThanExpression }
     * {@link InvokeQueryAction }
     * {@link PreviousContextLabelExpression }
     * {@link BOMtoArrayExpression }
     * {@link MVCViewElementExpression }
     * {@link ReplyProcessDataItemExpression }
     * {@link UserContextPropertyExpression }
     * {@link IsNullExpression }
     * {@link HasValueExpression }
     * {@link DateTimeAddMonthsExpression }
     * {@link LowerOrEqualThanExpression }
     * {@link GetCollectionItemExpression }
     * {@link BTContextExpression }
     * {@link EqualsExpression }
     * {@link MVCHtmlElementExpression }
     * {@link BOMExpression }
     * {@link TableColumnExpression }
     * {@link IsEnableExpression }
     * {@link IsInStateExpression }
     * {@link IsNewExpression }
     * {@link DoubleValueExpression }
     * {@link MVCViewElementValueExpression }
     * {@link DateTimeAddYearsExpression }
     * {@link IsVisibleExpression }
     * {@link BTContextPropertyExpression }
     * 
     * 
     */
    public List<Expression> getExpressions() {
        if (expressions == null) {
            expressions = new ArrayList<Expression>();
        }
        return this.expressions;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ExpressionCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final ExpressionCollection that = ((ExpressionCollection) object);
        {
            List<Expression> lhsExpressions;
            lhsExpressions = this.getExpressions();
            List<Expression> rhsExpressions;
            rhsExpressions = that.getExpressions();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "expressions", lhsExpressions), LocatorUtils.property(thatLocator, "expressions", rhsExpressions), lhsExpressions, rhsExpressions)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof ExpressionCollection) {
            final ExpressionCollection copy = ((ExpressionCollection) draftCopy);
            if ((this.expressions!= null)&&(!this.expressions.isEmpty())) {
                List<Expression> sourceExpressions;
                sourceExpressions = this.getExpressions();
                @SuppressWarnings("unchecked")
                List<Expression> copyExpressions = ((List<Expression> ) strategy.copy(LocatorUtils.property(locator, "expressions", sourceExpressions), sourceExpressions));
                copy.expressions = null;
                List<Expression> uniqueExpressionsl = copy.getExpressions();
                uniqueExpressionsl.addAll(copyExpressions);
            } else {
                copy.expressions = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ExpressionCollection();
    }

}
