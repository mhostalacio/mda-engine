
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ModelKeyExpression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ModelKeyExpression">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}Expression">
 *       &lt;attribute name="GetValue" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ModelKeyExpression")
public class ModelKeyExpression
    extends Expression
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "GetValue")
    protected Boolean getValue;

    /**
     * Gets the value of the getValue property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isGetValue() {
        if (getValue == null) {
            return false;
        } else {
            return getValue;
        }
    }

    /**
     * Sets the value of the getValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGetValue(Boolean value) {
        this.getValue = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ModelKeyExpression)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final ModelKeyExpression that = ((ModelKeyExpression) object);
        {
            boolean lhsGetValue;
            lhsGetValue = this.isGetValue();
            boolean rhsGetValue;
            rhsGetValue = that.isGetValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "getValue", lhsGetValue), LocatorUtils.property(thatLocator, "getValue", rhsGetValue), lhsGetValue, rhsGetValue)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof ModelKeyExpression) {
            final ModelKeyExpression copy = ((ModelKeyExpression) draftCopy);
            if (this.getValue!= null) {
                boolean sourceGetValue;
                sourceGetValue = this.isGetValue();
                boolean copyGetValue = strategy.copy(LocatorUtils.property(locator, "getValue", sourceGetValue), sourceGetValue);
                copy.setGetValue(copyGetValue);
            } else {
                copy.getValue = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ModelKeyExpression();
    }

}
