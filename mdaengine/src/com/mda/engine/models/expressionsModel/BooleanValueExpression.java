
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for BooleanValueExpression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BooleanValueExpression">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ValueExpression">
 *       &lt;attribute name="Value" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="GetValue" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BooleanValueExpression")
public class BooleanValueExpression
    extends ValueExpression
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "Value", required = true)
    protected boolean value;
    @XmlAttribute(name = "GetValue")
    protected Boolean getValue;

    /**
     * Gets the value of the value property.
     * 
     */
    public boolean isValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     */
    public void setValue(boolean value) {
        this.value = value;
    }

    /**
     * Gets the value of the getValue property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isGetValue() {
        if (getValue == null) {
            return false;
        } else {
            return getValue;
        }
    }

    /**
     * Sets the value of the getValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGetValue(Boolean value) {
        this.getValue = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof BooleanValueExpression)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final BooleanValueExpression that = ((BooleanValueExpression) object);
        {
            boolean lhsValue;
            lhsValue = this.isValue();
            boolean rhsValue;
            rhsValue = that.isValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "value", lhsValue), LocatorUtils.property(thatLocator, "value", rhsValue), lhsValue, rhsValue)) {
                return false;
            }
        }
        {
            boolean lhsGetValue;
            lhsGetValue = this.isGetValue();
            boolean rhsGetValue;
            rhsGetValue = that.isGetValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "getValue", lhsGetValue), LocatorUtils.property(thatLocator, "getValue", rhsGetValue), lhsGetValue, rhsGetValue)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof BooleanValueExpression) {
            final BooleanValueExpression copy = ((BooleanValueExpression) draftCopy);
            boolean sourceValue;
            sourceValue = this.isValue();
            boolean copyValue = strategy.copy(LocatorUtils.property(locator, "value", sourceValue), sourceValue);
            copy.setValue(copyValue);
            if (this.getValue!= null) {
                boolean sourceGetValue;
                sourceGetValue = this.isGetValue();
                boolean copyGetValue = strategy.copy(LocatorUtils.property(locator, "getValue", sourceGetValue), sourceGetValue);
                copy.setGetValue(copyGetValue);
            } else {
                copy.getValue = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new BooleanValueExpression();
    }
    
//--simple--preserve
    
    @Override
	public void writeSQLExpression(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product)
	{
    	if (this.isValue())
    	{
    		c.write("CAST(1 AS BIT)");
    	}
    	else
    	{
    		c.write("CAST(0 AS BIT)");
    	}
    		
    	
	}
   
	
//--simple--preserve

}
