
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for DelegateValuePropertyExpression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DelegateValuePropertyExpression">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}Expression">
 *       &lt;sequence>
 *         &lt;element name="MethodArguments" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}MethodArgumentsExpression" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="MethodName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DelegateValuePropertyExpression", propOrder = {
    "methodArguments"
})
public class DelegateValuePropertyExpression
    extends Expression
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "MethodArguments")
    protected List<MethodArgumentsExpression> methodArguments;
    @XmlAttribute(name = "MethodName", required = true)
    protected String methodName;

    /**
     * Gets the value of the methodArguments property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the methodArguments property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMethodArguments().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MethodArgumentsExpression }
     * 
     * 
     */
    public List<MethodArgumentsExpression> getMethodArguments() {
        if (methodArguments == null) {
            methodArguments = new ArrayList<MethodArgumentsExpression>();
        }
        return this.methodArguments;
    }

    /**
     * Gets the value of the methodName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMethodName() {
        return methodName;
    }

    /**
     * Sets the value of the methodName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMethodName(String value) {
        this.methodName = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof DelegateValuePropertyExpression)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final DelegateValuePropertyExpression that = ((DelegateValuePropertyExpression) object);
        {
            List<MethodArgumentsExpression> lhsMethodArguments;
            lhsMethodArguments = this.getMethodArguments();
            List<MethodArgumentsExpression> rhsMethodArguments;
            rhsMethodArguments = that.getMethodArguments();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "methodArguments", lhsMethodArguments), LocatorUtils.property(thatLocator, "methodArguments", rhsMethodArguments), lhsMethodArguments, rhsMethodArguments)) {
                return false;
            }
        }
        {
            String lhsMethodName;
            lhsMethodName = this.getMethodName();
            String rhsMethodName;
            rhsMethodName = that.getMethodName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "methodName", lhsMethodName), LocatorUtils.property(thatLocator, "methodName", rhsMethodName), lhsMethodName, rhsMethodName)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof DelegateValuePropertyExpression) {
            final DelegateValuePropertyExpression copy = ((DelegateValuePropertyExpression) draftCopy);
            if ((this.methodArguments!= null)&&(!this.methodArguments.isEmpty())) {
                List<MethodArgumentsExpression> sourceMethodArguments;
                sourceMethodArguments = this.getMethodArguments();
                @SuppressWarnings("unchecked")
                List<MethodArgumentsExpression> copyMethodArguments = ((List<MethodArgumentsExpression> ) strategy.copy(LocatorUtils.property(locator, "methodArguments", sourceMethodArguments), sourceMethodArguments));
                copy.methodArguments = null;
                List<MethodArgumentsExpression> uniqueMethodArgumentsl = copy.getMethodArguments();
                uniqueMethodArgumentsl.addAll(copyMethodArguments);
            } else {
                copy.methodArguments = null;
            }
            if (this.methodName!= null) {
                String sourceMethodName;
                sourceMethodName = this.getMethodName();
                String copyMethodName = ((String) strategy.copy(LocatorUtils.property(locator, "methodName", sourceMethodName), sourceMethodName));
                copy.setMethodName(copyMethodName);
            } else {
                copy.methodName = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new DelegateValuePropertyExpression();
    }

}
