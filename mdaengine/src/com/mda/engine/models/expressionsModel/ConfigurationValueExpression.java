
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.models.common.ModelAttributeCollection;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ConfigurationValueExpression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConfigurationValueExpression">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ValueExpression">
 *       &lt;sequence>
 *         &lt;element name="ConvertTo" type="{http://www.mdaengine.com/mdaengine/models/common}ModelAttributeCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="AppSettingsKey" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="IsStringValueNode" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConfigurationValueExpression", propOrder = {
    "convertTo"
})
public class ConfigurationValueExpression
    extends ValueExpression
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "ConvertTo")
    protected ModelAttributeCollection convertTo;
    @XmlAttribute(name = "AppSettingsKey", required = true)
    protected String appSettingsKey;
    @XmlAttribute(name = "IsStringValueNode")
    protected Boolean isStringValueNode;

    /**
     * Gets the value of the convertTo property.
     * 
     * @return
     *     possible object is
     *     {@link ModelAttributeCollection }
     *     
     */
    public ModelAttributeCollection getConvertTo() {
        return convertTo;
    }

    /**
     * Sets the value of the convertTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ModelAttributeCollection }
     *     
     */
    public void setConvertTo(ModelAttributeCollection value) {
        this.convertTo = value;
    }

    /**
     * Gets the value of the appSettingsKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppSettingsKey() {
        return appSettingsKey;
    }

    /**
     * Sets the value of the appSettingsKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppSettingsKey(String value) {
        this.appSettingsKey = value;
    }

    /**
     * Gets the value of the isStringValueNode property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isIsStringValueNode() {
        if (isStringValueNode == null) {
            return false;
        } else {
            return isStringValueNode;
        }
    }

    /**
     * Sets the value of the isStringValueNode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsStringValueNode(Boolean value) {
        this.isStringValueNode = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ConfigurationValueExpression)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final ConfigurationValueExpression that = ((ConfigurationValueExpression) object);
        {
            ModelAttributeCollection lhsConvertTo;
            lhsConvertTo = this.getConvertTo();
            ModelAttributeCollection rhsConvertTo;
            rhsConvertTo = that.getConvertTo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "convertTo", lhsConvertTo), LocatorUtils.property(thatLocator, "convertTo", rhsConvertTo), lhsConvertTo, rhsConvertTo)) {
                return false;
            }
        }
        {
            String lhsAppSettingsKey;
            lhsAppSettingsKey = this.getAppSettingsKey();
            String rhsAppSettingsKey;
            rhsAppSettingsKey = that.getAppSettingsKey();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "appSettingsKey", lhsAppSettingsKey), LocatorUtils.property(thatLocator, "appSettingsKey", rhsAppSettingsKey), lhsAppSettingsKey, rhsAppSettingsKey)) {
                return false;
            }
        }
        {
            boolean lhsIsStringValueNode;
            lhsIsStringValueNode = this.isIsStringValueNode();
            boolean rhsIsStringValueNode;
            rhsIsStringValueNode = that.isIsStringValueNode();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "isStringValueNode", lhsIsStringValueNode), LocatorUtils.property(thatLocator, "isStringValueNode", rhsIsStringValueNode), lhsIsStringValueNode, rhsIsStringValueNode)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof ConfigurationValueExpression) {
            final ConfigurationValueExpression copy = ((ConfigurationValueExpression) draftCopy);
            if (this.convertTo!= null) {
                ModelAttributeCollection sourceConvertTo;
                sourceConvertTo = this.getConvertTo();
                ModelAttributeCollection copyConvertTo = ((ModelAttributeCollection) strategy.copy(LocatorUtils.property(locator, "convertTo", sourceConvertTo), sourceConvertTo));
                copy.setConvertTo(copyConvertTo);
            } else {
                copy.convertTo = null;
            }
            if (this.appSettingsKey!= null) {
                String sourceAppSettingsKey;
                sourceAppSettingsKey = this.getAppSettingsKey();
                String copyAppSettingsKey = ((String) strategy.copy(LocatorUtils.property(locator, "appSettingsKey", sourceAppSettingsKey), sourceAppSettingsKey));
                copy.setAppSettingsKey(copyAppSettingsKey);
            } else {
                copy.appSettingsKey = null;
            }
            if (this.isStringValueNode!= null) {
                boolean sourceIsStringValueNode;
                sourceIsStringValueNode = this.isIsStringValueNode();
                boolean copyIsStringValueNode = strategy.copy(LocatorUtils.property(locator, "isStringValueNode", sourceIsStringValueNode), sourceIsStringValueNode);
                copy.setIsStringValueNode(copyIsStringValueNode);
            } else {
                copy.isStringValueNode = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ConfigurationValueExpression();
    }

}
