
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for DateTimeComputedValueExpression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DateTimeComputedValueExpression">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}DateTimeExpression">
 *       &lt;sequence>
 *         &lt;element name="BaseValue" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ExpressionChoice"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DateTimeComputedValueExpression", propOrder = {
    "baseValue"
})
@XmlSeeAlso({
    DateTimeAddYearsExpression.class,
    DateTimeAddMonthsExpression.class,
    DateTimeAddDaysExpression.class
})
public class DateTimeComputedValueExpression
    extends DateTimeExpression
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "BaseValue", required = true)
    protected ExpressionChoice baseValue;

    /**
     * Gets the value of the baseValue property.
     * 
     * @return
     *     possible object is
     *     {@link ExpressionChoice }
     *     
     */
    public ExpressionChoice getBaseValue() {
        return baseValue;
    }

    /**
     * Sets the value of the baseValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpressionChoice }
     *     
     */
    public void setBaseValue(ExpressionChoice value) {
        this.baseValue = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof DateTimeComputedValueExpression)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final DateTimeComputedValueExpression that = ((DateTimeComputedValueExpression) object);
        {
            ExpressionChoice lhsBaseValue;
            lhsBaseValue = this.getBaseValue();
            ExpressionChoice rhsBaseValue;
            rhsBaseValue = that.getBaseValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "baseValue", lhsBaseValue), LocatorUtils.property(thatLocator, "baseValue", rhsBaseValue), lhsBaseValue, rhsBaseValue)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof DateTimeComputedValueExpression) {
            final DateTimeComputedValueExpression copy = ((DateTimeComputedValueExpression) draftCopy);
            if (this.baseValue!= null) {
                ExpressionChoice sourceBaseValue;
                sourceBaseValue = this.getBaseValue();
                ExpressionChoice copyBaseValue = ((ExpressionChoice) strategy.copy(LocatorUtils.property(locator, "baseValue", sourceBaseValue), sourceBaseValue));
                copy.setBaseValue(copyBaseValue);
            } else {
                copy.baseValue = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new DateTimeComputedValueExpression();
    }

}
