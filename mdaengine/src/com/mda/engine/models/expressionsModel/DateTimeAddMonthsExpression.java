
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for DateTimeAddMonthsExpression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DateTimeAddMonthsExpression">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}DateTimeComputedValueExpression">
 *       &lt;attribute name="MonthsToAdd" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DateTimeAddMonthsExpression")
public class DateTimeAddMonthsExpression
    extends DateTimeComputedValueExpression
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "MonthsToAdd", required = true)
    protected int monthsToAdd;

    /**
     * Gets the value of the monthsToAdd property.
     * 
     */
    public int getMonthsToAdd() {
        return monthsToAdd;
    }

    /**
     * Sets the value of the monthsToAdd property.
     * 
     */
    public void setMonthsToAdd(int value) {
        this.monthsToAdd = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof DateTimeAddMonthsExpression)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final DateTimeAddMonthsExpression that = ((DateTimeAddMonthsExpression) object);
        {
            int lhsMonthsToAdd;
            lhsMonthsToAdd = this.getMonthsToAdd();
            int rhsMonthsToAdd;
            rhsMonthsToAdd = that.getMonthsToAdd();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "monthsToAdd", lhsMonthsToAdd), LocatorUtils.property(thatLocator, "monthsToAdd", rhsMonthsToAdd), lhsMonthsToAdd, rhsMonthsToAdd)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof DateTimeAddMonthsExpression) {
            final DateTimeAddMonthsExpression copy = ((DateTimeAddMonthsExpression) draftCopy);
            int sourceMonthsToAdd;
            sourceMonthsToAdd = this.getMonthsToAdd();
            int copyMonthsToAdd = strategy.copy(LocatorUtils.property(locator, "monthsToAdd", sourceMonthsToAdd), sourceMonthsToAdd);
            copy.setMonthsToAdd(copyMonthsToAdd);
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new DateTimeAddMonthsExpression();
    }

}
