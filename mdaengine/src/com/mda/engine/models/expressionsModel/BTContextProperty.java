
package com.mda.engine.models.expressionsModel;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BTContextProperty.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BTContextProperty">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ClientApplicationName"/>
 *     &lt;enumeration value="ClientApplicationDateTime"/>
 *     &lt;enumeration value="LoginUserRole"/>
 *     &lt;enumeration value="ResponsibleUser"/>
 *     &lt;enumeration value="RepresentativeUser"/>
 *     &lt;enumeration value="UniqueId"/>
 *     &lt;enumeration value="CompanyCode"/>
 *     &lt;enumeration value="ProcessCode"/>
 *     &lt;enumeration value="RemoteHostAddress"/>
 *     &lt;enumeration value="FrontEndAddress"/>
 *     &lt;enumeration value="SessionId"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BTContextProperty")
@XmlEnum
public enum BTContextProperty {

    @XmlEnumValue("ClientApplicationName")
    CLIENT_APPLICATION_NAME("ClientApplicationName"),
    @XmlEnumValue("ClientApplicationDateTime")
    CLIENT_APPLICATION_DATE_TIME("ClientApplicationDateTime"),
    @XmlEnumValue("LoginUserRole")
    LOGIN_USER_ROLE("LoginUserRole"),
    @XmlEnumValue("ResponsibleUser")
    RESPONSIBLE_USER("ResponsibleUser"),
    @XmlEnumValue("RepresentativeUser")
    REPRESENTATIVE_USER("RepresentativeUser"),
    @XmlEnumValue("UniqueId")
    UNIQUE_ID("UniqueId"),
    @XmlEnumValue("CompanyCode")
    COMPANY_CODE("CompanyCode"),
    @XmlEnumValue("ProcessCode")
    PROCESS_CODE("ProcessCode"),
    @XmlEnumValue("RemoteHostAddress")
    REMOTE_HOST_ADDRESS("RemoteHostAddress"),
    @XmlEnumValue("FrontEndAddress")
    FRONT_END_ADDRESS("FrontEndAddress"),
    @XmlEnumValue("SessionId")
    SESSION_ID("SessionId");
    private final String value;

    BTContextProperty(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BTContextProperty fromValue(String v) {
        for (BTContextProperty c: BTContextProperty.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
