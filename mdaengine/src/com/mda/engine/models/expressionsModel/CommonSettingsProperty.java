
package com.mda.engine.models.expressionsModel;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CommonSettingsProperty.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CommonSettingsProperty">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ApplicationName"/>
 *     &lt;enumeration value="CurrentReleaseName"/>
 *     &lt;enumeration value="InPublicSite"/>
 *     &lt;enumeration value="MarketPlaceDefault"/>
 *     &lt;enumeration value="MarketPlaceName"/>
 *     &lt;enumeration value="PortalCode"/>
 *     &lt;enumeration value="SystemCompanyCode"/>
 *     &lt;enumeration value="SystemUserCode"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CommonSettingsProperty")
@XmlEnum
public enum CommonSettingsProperty {

    @XmlEnumValue("ApplicationName")
    APPLICATION_NAME("ApplicationName"),
    @XmlEnumValue("CurrentReleaseName")
    CURRENT_RELEASE_NAME("CurrentReleaseName"),
    @XmlEnumValue("InPublicSite")
    IN_PUBLIC_SITE("InPublicSite"),
    @XmlEnumValue("MarketPlaceDefault")
    MARKET_PLACE_DEFAULT("MarketPlaceDefault"),
    @XmlEnumValue("MarketPlaceName")
    MARKET_PLACE_NAME("MarketPlaceName"),
    @XmlEnumValue("PortalCode")
    PORTAL_CODE("PortalCode"),
    @XmlEnumValue("SystemCompanyCode")
    SYSTEM_COMPANY_CODE("SystemCompanyCode"),
    @XmlEnumValue("SystemUserCode")
    SYSTEM_USER_CODE("SystemUserCode");
    private final String value;

    CommonSettingsProperty(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CommonSettingsProperty fromValue(String v) {
        for (CommonSettingsProperty c: CommonSettingsProperty.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
