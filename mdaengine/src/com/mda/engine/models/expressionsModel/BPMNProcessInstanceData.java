
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for BPMNProcessInstanceData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BPMNProcessInstanceData">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}Expression">
 *       &lt;choice>
 *         &lt;sequence>
 *           &lt;element name="DataObject" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}BOMExpression"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="ProcessInstanceId" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;/sequence>
 *       &lt;/choice>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BPMNProcessInstanceData", propOrder = {
    "dataObject",
    "processInstanceId"
})
public class BPMNProcessInstanceData
    extends Expression
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "DataObject")
    protected BOMExpression dataObject;
    @XmlElement(name = "ProcessInstanceId")
    protected Object processInstanceId;

    /**
     * Gets the value of the dataObject property.
     * 
     * @return
     *     possible object is
     *     {@link BOMExpression }
     *     
     */
    public BOMExpression getDataObject() {
        return dataObject;
    }

    /**
     * Sets the value of the dataObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link BOMExpression }
     *     
     */
    public void setDataObject(BOMExpression value) {
        this.dataObject = value;
    }

    /**
     * Gets the value of the processInstanceId property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getProcessInstanceId() {
        return processInstanceId;
    }

    /**
     * Sets the value of the processInstanceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setProcessInstanceId(Object value) {
        this.processInstanceId = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof BPMNProcessInstanceData)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final BPMNProcessInstanceData that = ((BPMNProcessInstanceData) object);
        {
            BOMExpression lhsDataObject;
            lhsDataObject = this.getDataObject();
            BOMExpression rhsDataObject;
            rhsDataObject = that.getDataObject();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dataObject", lhsDataObject), LocatorUtils.property(thatLocator, "dataObject", rhsDataObject), lhsDataObject, rhsDataObject)) {
                return false;
            }
        }
        {
            Object lhsProcessInstanceId;
            lhsProcessInstanceId = this.getProcessInstanceId();
            Object rhsProcessInstanceId;
            rhsProcessInstanceId = that.getProcessInstanceId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "processInstanceId", lhsProcessInstanceId), LocatorUtils.property(thatLocator, "processInstanceId", rhsProcessInstanceId), lhsProcessInstanceId, rhsProcessInstanceId)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof BPMNProcessInstanceData) {
            final BPMNProcessInstanceData copy = ((BPMNProcessInstanceData) draftCopy);
            if (this.dataObject!= null) {
                BOMExpression sourceDataObject;
                sourceDataObject = this.getDataObject();
                BOMExpression copyDataObject = ((BOMExpression) strategy.copy(LocatorUtils.property(locator, "dataObject", sourceDataObject), sourceDataObject));
                copy.setDataObject(copyDataObject);
            } else {
                copy.dataObject = null;
            }
            if (this.processInstanceId!= null) {
                Object sourceProcessInstanceId;
                sourceProcessInstanceId = this.getProcessInstanceId();
                Object copyProcessInstanceId = ((Object) strategy.copy(LocatorUtils.property(locator, "processInstanceId", sourceProcessInstanceId), sourceProcessInstanceId));
                copy.setProcessInstanceId(copyProcessInstanceId);
            } else {
                copy.processInstanceId = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new BPMNProcessInstanceData();
    }

}
