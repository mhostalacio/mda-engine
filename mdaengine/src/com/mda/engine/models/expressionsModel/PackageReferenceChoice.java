
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for PackageReferenceChoice complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PackageReferenceChoice">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="Current" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}CurrentPackageReference"/>
 *         &lt;element name="Other" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}CustomPackageReference"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PackageReferenceChoice", propOrder = {
    "reference"
})
public class PackageReferenceChoice
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElements({
        @XmlElement(name = "Other", type = CustomPackageReference.class),
        @XmlElement(name = "Current", type = CurrentPackageReference.class)
    })
    protected PackageReference reference;

    /**
     * Gets the value of the reference property.
     * 
     * @return
     *     possible object is
     *     {@link CustomPackageReference }
     *     {@link CurrentPackageReference }
     *     
     */
    public PackageReference getReference() {
        return reference;
    }

    /**
     * Sets the value of the reference property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomPackageReference }
     *     {@link CurrentPackageReference }
     *     
     */
    public void setReference(PackageReference value) {
        this.reference = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof PackageReferenceChoice)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final PackageReferenceChoice that = ((PackageReferenceChoice) object);
        {
            PackageReference lhsReference;
            lhsReference = this.getReference();
            PackageReference rhsReference;
            rhsReference = that.getReference();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "reference", lhsReference), LocatorUtils.property(thatLocator, "reference", rhsReference), lhsReference, rhsReference)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof PackageReferenceChoice) {
            final PackageReferenceChoice copy = ((PackageReferenceChoice) draftCopy);
            if (this.reference!= null) {
                PackageReference sourceReference;
                sourceReference = this.getReference();
                PackageReference copyReference = ((PackageReference) strategy.copy(LocatorUtils.property(locator, "reference", sourceReference), sourceReference));
                copy.setReference(copyReference);
            } else {
                copy.reference = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new PackageReferenceChoice();
    }

}
