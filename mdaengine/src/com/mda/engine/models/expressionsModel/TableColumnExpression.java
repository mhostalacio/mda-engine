
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for TableColumnExpression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TableColumnExpression">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}Expression">
 *       &lt;attribute name="TableAlias" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="ColumnName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TableColumnExpression")
public class TableColumnExpression
    extends Expression
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "TableAlias", required = true)
    protected String tableAlias;
    @XmlAttribute(name = "ColumnName", required = true)
    protected String columnName;

    /**
     * Gets the value of the tableAlias property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTableAlias() {
        return tableAlias;
    }

    /**
     * Sets the value of the tableAlias property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTableAlias(String value) {
        this.tableAlias = value;
    }

    /**
     * Gets the value of the columnName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColumnName() {
        return columnName;
    }

    /**
     * Sets the value of the columnName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColumnName(String value) {
        this.columnName = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof TableColumnExpression)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final TableColumnExpression that = ((TableColumnExpression) object);
        {
            String lhsTableAlias;
            lhsTableAlias = this.getTableAlias();
            String rhsTableAlias;
            rhsTableAlias = that.getTableAlias();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "tableAlias", lhsTableAlias), LocatorUtils.property(thatLocator, "tableAlias", rhsTableAlias), lhsTableAlias, rhsTableAlias)) {
                return false;
            }
        }
        {
            String lhsColumnName;
            lhsColumnName = this.getColumnName();
            String rhsColumnName;
            rhsColumnName = that.getColumnName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "columnName", lhsColumnName), LocatorUtils.property(thatLocator, "columnName", rhsColumnName), lhsColumnName, rhsColumnName)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof TableColumnExpression) {
            final TableColumnExpression copy = ((TableColumnExpression) draftCopy);
            if (this.tableAlias!= null) {
                String sourceTableAlias;
                sourceTableAlias = this.getTableAlias();
                String copyTableAlias = ((String) strategy.copy(LocatorUtils.property(locator, "tableAlias", sourceTableAlias), sourceTableAlias));
                copy.setTableAlias(copyTableAlias);
            } else {
                copy.tableAlias = null;
            }
            if (this.columnName!= null) {
                String sourceColumnName;
                sourceColumnName = this.getColumnName();
                String copyColumnName = ((String) strategy.copy(LocatorUtils.property(locator, "columnName", sourceColumnName), sourceColumnName));
                copy.setColumnName(copyColumnName);
            } else {
                copy.columnName = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new TableColumnExpression();
    }
    
//--simple--preserve
    
    @Override
	public void writeSQLExpression(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product)
	{
		c.write(this.getTableAlias());
		c.write(".[");
		c.write(this.getColumnName());
		c.write("]");
	}
   
	
//--simple--preserve

}
