
package com.mda.engine.models.expressionsModel;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.mda.engine.models.expressionsModel package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Formula_QNAME = new QName("http://www.mdaengine.com/mdaengine/models/expressionsModel", "Formula");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.mda.engine.models.expressionsModel
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link IsInStateExpressionState }
     * 
     */
    public IsInStateExpressionState createIsInStateExpressionState() {
        return new IsInStateExpressionState();
    }

    /**
     * Create an instance of {@link CurrentUrlExpression }
     * 
     */
    public CurrentUrlExpression createCurrentUrlExpression() {
        return new CurrentUrlExpression();
    }

    /**
     * Create an instance of {@link IsDirtyExpression }
     * 
     */
    public IsDirtyExpression createIsDirtyExpression() {
        return new IsDirtyExpression();
    }

    /**
     * Create an instance of {@link PackageReferenceChoice }
     * 
     */
    public PackageReferenceChoice createPackageReferenceChoice() {
        return new PackageReferenceChoice();
    }

    /**
     * Create an instance of {@link CustomPackageReference }
     * 
     */
    public CustomPackageReference createCustomPackageReference() {
        return new CustomPackageReference();
    }

    /**
     * Create an instance of {@link PackageReference }
     * 
     */
    public PackageReference createPackageReference() {
        return new PackageReference();
    }

    /**
     * Create an instance of {@link DelegateValuePropertyExpression }
     * 
     */
    public DelegateValuePropertyExpression createDelegateValuePropertyExpression() {
        return new DelegateValuePropertyExpression();
    }

    /**
     * Create an instance of {@link ServerAddressExpression }
     * 
     */
    public ServerAddressExpression createServerAddressExpression() {
        return new ServerAddressExpression();
    }

    /**
     * Create an instance of {@link PreviousContextLabelExpression }
     * 
     */
    public PreviousContextLabelExpression createPreviousContextLabelExpression() {
        return new PreviousContextLabelExpression();
    }

    /**
     * Create an instance of {@link BooleanValueExpression }
     * 
     */
    public BooleanValueExpression createBooleanValueExpression() {
        return new BooleanValueExpression();
    }

    /**
     * Create an instance of {@link ConfigurationValueExpression }
     * 
     */
    public ConfigurationValueExpression createConfigurationValueExpression() {
        return new ConfigurationValueExpression();
    }

    /**
     * Create an instance of {@link EnumValueExpression }
     * 
     */
    public EnumValueExpression createEnumValueExpression() {
        return new EnumValueExpression();
    }

    /**
     * Create an instance of {@link DateTimeExpression }
     * 
     */
    public DateTimeExpression createDateTimeExpression() {
        return new DateTimeExpression();
    }

    /**
     * Create an instance of {@link LastCollectionExpression }
     * 
     */
    public LastCollectionExpression createLastCollectionExpression() {
        return new LastCollectionExpression();
    }

    /**
     * Create an instance of {@link CurrentBOMExpression }
     * 
     */
    public CurrentBOMExpression createCurrentBOMExpression() {
        return new CurrentBOMExpression();
    }

    /**
     * Create an instance of {@link UserRuntimeSettingExpression }
     * 
     */
    public UserRuntimeSettingExpression createUserRuntimeSettingExpression() {
        return new UserRuntimeSettingExpression();
    }

    /**
     * Create an instance of {@link ResourceExpression }
     * 
     */
    public ResourceExpression createResourceExpression() {
        return new ResourceExpression();
    }

    /**
     * Create an instance of {@link AddExpression }
     * 
     */
    public AddExpression createAddExpression() {
        return new AddExpression();
    }

    /**
     * Create an instance of {@link IsEnableExpression }
     * 
     */
    public IsEnableExpression createIsEnableExpression() {
        return new IsEnableExpression();
    }

    /**
     * Create an instance of {@link StringFormatExpression }
     * 
     */
    public StringFormatExpression createStringFormatExpression() {
        return new StringFormatExpression();
    }

    /**
     * Create an instance of {@link NotEqualsExpression }
     * 
     */
    public NotEqualsExpression createNotEqualsExpression() {
        return new NotEqualsExpression();
    }

    /**
     * Create an instance of {@link IsValidExpression }
     * 
     */
    public IsValidExpression createIsValidExpression() {
        return new IsValidExpression();
    }

    /**
     * Create an instance of {@link MVCViewElementValueExpression }
     * 
     */
    public MVCViewElementValueExpression createMVCViewElementValueExpression() {
        return new MVCViewElementValueExpression();
    }

    /**
     * Create an instance of {@link IsInStateExpression }
     * 
     */
    public IsInStateExpression createIsInStateExpression() {
        return new IsInStateExpression();
    }

    /**
     * Create an instance of {@link MinExpression }
     * 
     */
    public MinExpression createMinExpression() {
        return new MinExpression();
    }

    /**
     * Create an instance of {@link MVCViewPerspectivePlaceholderExpression }
     * 
     */
    public MVCViewPerspectivePlaceholderExpression createMVCViewPerspectivePlaceholderExpression() {
        return new MVCViewPerspectivePlaceholderExpression();
    }

    /**
     * Create an instance of {@link BooleanExpressionCollection }
     * 
     */
    public BooleanExpressionCollection createBooleanExpressionCollection() {
        return new BooleanExpressionCollection();
    }

    /**
     * Create an instance of {@link CurrentContextUrlExpression }
     * 
     */
    public CurrentContextUrlExpression createCurrentContextUrlExpression() {
        return new CurrentContextUrlExpression();
    }

    /**
     * Create an instance of {@link GreaterOrEqualThanExpression }
     * 
     */
    public GreaterOrEqualThanExpression createGreaterOrEqualThanExpression() {
        return new GreaterOrEqualThanExpression();
    }

    /**
     * Create an instance of {@link CountCollectionExpression }
     * 
     */
    public CountCollectionExpression createCountCollectionExpression() {
        return new CountCollectionExpression();
    }

    /**
     * Create an instance of {@link ParentesisExpression }
     * 
     */
    public ParentesisExpression createParentesisExpression() {
        return new ParentesisExpression();
    }

    /**
     * Create an instance of {@link GreaterThanExpression }
     * 
     */
    public GreaterThanExpression createGreaterThanExpression() {
        return new GreaterThanExpression();
    }

    /**
     * Create an instance of {@link DivExpression }
     * 
     */
    public DivExpression createDivExpression() {
        return new DivExpression();
    }

    /**
     * Create an instance of {@link CurrentItemExpression }
     * 
     */
    public CurrentItemExpression createCurrentItemExpression() {
        return new CurrentItemExpression();
    }

    /**
     * Create an instance of {@link FactExpression }
     * 
     */
    public FactExpression createFactExpression() {
        return new FactExpression();
    }

    /**
     * Create an instance of {@link CurrentPackageReference }
     * 
     */
    public CurrentPackageReference createCurrentPackageReference() {
        return new CurrentPackageReference();
    }

    /**
     * Create an instance of {@link AbsExpression }
     * 
     */
    public AbsExpression createAbsExpression() {
        return new AbsExpression();
    }

    /**
     * Create an instance of {@link MVCViewElementExpression }
     * 
     */
    public MVCViewElementExpression createMVCViewElementExpression() {
        return new MVCViewElementExpression();
    }

    /**
     * Create an instance of {@link PreviousContextUrlExpression }
     * 
     */
    public PreviousContextUrlExpression createPreviousContextUrlExpression() {
        return new PreviousContextUrlExpression();
    }

    /**
     * Create an instance of {@link MinusExpression }
     * 
     */
    public MinusExpression createMinusExpression() {
        return new MinusExpression();
    }

    /**
     * Create an instance of {@link SumExpression }
     * 
     */
    public SumExpression createSumExpression() {
        return new SumExpression();
    }

    /**
     * Create an instance of {@link ElseExpression }
     * 
     */
    public ElseExpression createElseExpression() {
        return new ElseExpression();
    }

    /**
     * Create an instance of {@link UserContextPropertyExpression }
     * 
     */
    public UserContextPropertyExpression createUserContextPropertyExpression() {
        return new UserContextPropertyExpression();
    }

    /**
     * Create an instance of {@link MVCViewModelJSONExpression }
     * 
     */
    public MVCViewModelJSONExpression createMVCViewModelJSONExpression() {
        return new MVCViewModelJSONExpression();
    }

    /**
     * Create an instance of {@link EqualsExpression }
     * 
     */
    public EqualsExpression createEqualsExpression() {
        return new EqualsExpression();
    }

    /**
     * Create an instance of {@link GetValueExpression }
     * 
     */
    public GetValueExpression createGetValueExpression() {
        return new GetValueExpression();
    }

    /**
     * Create an instance of {@link PIValueExpression }
     * 
     */
    public PIValueExpression createPIValueExpression() {
        return new PIValueExpression();
    }

    /**
     * Create an instance of {@link ContainsValueExpression }
     * 
     */
    public ContainsValueExpression createContainsValueExpression() {
        return new ContainsValueExpression();
    }

    /**
     * Create an instance of {@link QueryParameterExpression }
     * 
     */
    public QueryParameterExpression createQueryParameterExpression() {
        return new QueryParameterExpression();
    }

    /**
     * Create an instance of {@link DoubleArgumentsExpression }
     * 
     */
    public DoubleArgumentsExpression createDoubleArgumentsExpression() {
        return new DoubleArgumentsExpression();
    }

    /**
     * Create an instance of {@link InvokeBusinessTransactionExpressionInputCollection }
     * 
     */
    public InvokeBusinessTransactionExpressionInputCollection createInvokeBusinessTransactionExpressionInputCollection() {
        return new InvokeBusinessTransactionExpressionInputCollection();
    }

    /**
     * Create an instance of {@link BPMNProcessInstanceData }
     * 
     */
    public BPMNProcessInstanceData createBPMNProcessInstanceData() {
        return new BPMNProcessInstanceData();
    }

    /**
     * Create an instance of {@link TableColumnExpression }
     * 
     */
    public TableColumnExpression createTableColumnExpression() {
        return new TableColumnExpression();
    }

    /**
     * Create an instance of {@link BooleanExpressionChoice }
     * 
     */
    public BooleanExpressionChoice createBooleanExpressionChoice() {
        return new BooleanExpressionChoice();
    }

    /**
     * Create an instance of {@link BOMExpression }
     * 
     */
    public BOMExpression createBOMExpression() {
        return new BOMExpression();
    }

    /**
     * Create an instance of {@link DoubleValueExpression }
     * 
     */
    public DoubleValueExpression createDoubleValueExpression() {
        return new DoubleValueExpression();
    }

    /**
     * Create an instance of {@link StringBuilderAppendItem }
     * 
     */
    public StringBuilderAppendItem createStringBuilderAppendItem() {
        return new StringBuilderAppendItem();
    }

    /**
     * Create an instance of {@link InvokeBusinessTransactionExpressionInput }
     * 
     */
    public InvokeBusinessTransactionExpressionInput createInvokeBusinessTransactionExpressionInput() {
        return new InvokeBusinessTransactionExpressionInput();
    }

    /**
     * Create an instance of {@link LikeExpression }
     * 
     */
    public LikeExpression createLikeExpression() {
        return new LikeExpression();
    }

    /**
     * Create an instance of {@link TaskOutputValueExpression }
     * 
     */
    public TaskOutputValueExpression createTaskOutputValueExpression() {
        return new TaskOutputValueExpression();
    }

    /**
     * Create an instance of {@link NotExpression }
     * 
     */
    public NotExpression createNotExpression() {
        return new NotExpression();
    }

    /**
     * Create an instance of {@link DateTimeNowExpression }
     * 
     */
    public DateTimeNowExpression createDateTimeNowExpression() {
        return new DateTimeNowExpression();
    }

    /**
     * Create an instance of {@link IsRequiredExpression }
     * 
     */
    public IsRequiredExpression createIsRequiredExpression() {
        return new IsRequiredExpression();
    }

    /**
     * Create an instance of {@link TernaryOperatorExpression }
     * 
     */
    public TernaryOperatorExpression createTernaryOperatorExpression() {
        return new TernaryOperatorExpression();
    }

    /**
     * Create an instance of {@link DateTimeAddDaysExpression }
     * 
     */
    public DateTimeAddDaysExpression createDateTimeAddDaysExpression() {
        return new DateTimeAddDaysExpression();
    }

    /**
     * Create an instance of {@link ReturnExpression }
     * 
     */
    public ReturnExpression createReturnExpression() {
        return new ReturnExpression();
    }

    /**
     * Create an instance of {@link LastOrDefaultCollectionExpression }
     * 
     */
    public LastOrDefaultCollectionExpression createLastOrDefaultCollectionExpression() {
        return new LastOrDefaultCollectionExpression();
    }

    /**
     * Create an instance of {@link InvokeQueryActionParameter }
     * 
     */
    public InvokeQueryActionParameter createInvokeQueryActionParameter() {
        return new InvokeQueryActionParameter();
    }

    /**
     * Create an instance of {@link AddItemsToListAction }
     * 
     */
    public AddItemsToListAction createAddItemsToListAction() {
        return new AddItemsToListAction();
    }

    /**
     * Create an instance of {@link RowIndexExpression }
     * 
     */
    public RowIndexExpression createRowIndexExpression() {
        return new RowIndexExpression();
    }

    /**
     * Create an instance of {@link IsNullExpression }
     * 
     */
    public IsNullExpression createIsNullExpression() {
        return new IsNullExpression();
    }

    /**
     * Create an instance of {@link BOMtoArrayExpression }
     * 
     */
    public BOMtoArrayExpression createBOMtoArrayExpression() {
        return new BOMtoArrayExpression();
    }

    /**
     * Create an instance of {@link ReplyProcessDataItemExpression }
     * 
     */
    public ReplyProcessDataItemExpression createReplyProcessDataItemExpression() {
        return new ReplyProcessDataItemExpression();
    }

    /**
     * Create an instance of {@link ExecuteMethodExpression }
     * 
     */
    public ExecuteMethodExpression createExecuteMethodExpression() {
        return new ExecuteMethodExpression();
    }

    /**
     * Create an instance of {@link GetCollectionItemExpression }
     * 
     */
    public GetCollectionItemExpression createGetCollectionItemExpression() {
        return new GetCollectionItemExpression();
    }

    /**
     * Create an instance of {@link ModExpression }
     * 
     */
    public ModExpression createModExpression() {
        return new ModExpression();
    }

    /**
     * Create an instance of {@link OrExpression }
     * 
     */
    public OrExpression createOrExpression() {
        return new OrExpression();
    }

    /**
     * Create an instance of {@link InvokeQueryActionParameterCollection }
     * 
     */
    public InvokeQueryActionParameterCollection createInvokeQueryActionParameterCollection() {
        return new InvokeQueryActionParameterCollection();
    }

    /**
     * Create an instance of {@link MaxOfAllExpression }
     * 
     */
    public MaxOfAllExpression createMaxOfAllExpression() {
        return new MaxOfAllExpression();
    }

    /**
     * Create an instance of {@link RegexExpression }
     * 
     */
    public RegexExpression createRegexExpression() {
        return new RegexExpression();
    }

    /**
     * Create an instance of {@link IsEmptyCollectionExpression }
     * 
     */
    public IsEmptyCollectionExpression createIsEmptyCollectionExpression() {
        return new IsEmptyCollectionExpression();
    }

    /**
     * Create an instance of {@link MVCViewElementSelectedIndexExpression }
     * 
     */
    public MVCViewElementSelectedIndexExpression createMVCViewElementSelectedIndexExpression() {
        return new MVCViewElementSelectedIndexExpression();
    }

    /**
     * Create an instance of {@link IdentifierValueExpression }
     * 
     */
    public IdentifierValueExpression createIdentifierValueExpression() {
        return new IdentifierValueExpression();
    }

    /**
     * Create an instance of {@link FloatValueExpression }
     * 
     */
    public FloatValueExpression createFloatValueExpression() {
        return new FloatValueExpression();
    }

    /**
     * Create an instance of {@link DateTimeComputedValueExpression }
     * 
     */
    public DateTimeComputedValueExpression createDateTimeComputedValueExpression() {
        return new DateTimeComputedValueExpression();
    }

    /**
     * Create an instance of {@link IsTrueExpression }
     * 
     */
    public IsTrueExpression createIsTrueExpression() {
        return new IsTrueExpression();
    }

    /**
     * Create an instance of {@link MethodArgumentsExpression }
     * 
     */
    public MethodArgumentsExpression createMethodArgumentsExpression() {
        return new MethodArgumentsExpression();
    }

    /**
     * Create an instance of {@link MaxExpression }
     * 
     */
    public MaxExpression createMaxExpression() {
        return new MaxExpression();
    }

    /**
     * Create an instance of {@link Expression }
     * 
     */
    public Expression createExpression() {
        return new Expression();
    }

    /**
     * Create an instance of {@link FirstCollectionExpression }
     * 
     */
    public FirstCollectionExpression createFirstCollectionExpression() {
        return new FirstCollectionExpression();
    }

    /**
     * Create an instance of {@link MVCExpression }
     * 
     */
    public MVCExpression createMVCExpression() {
        return new MVCExpression();
    }

    /**
     * Create an instance of {@link GetCollectionItemOrDefaultExpression }
     * 
     */
    public GetCollectionItemOrDefaultExpression createGetCollectionItemOrDefaultExpression() {
        return new GetCollectionItemOrDefaultExpression();
    }

    /**
     * Create an instance of {@link DataMaskPropertyExpression }
     * 
     */
    public DataMaskPropertyExpression createDataMaskPropertyExpression() {
        return new DataMaskPropertyExpression();
    }

    /**
     * Create an instance of {@link MVCViewModelsJSONExpression }
     * 
     */
    public MVCViewModelsJSONExpression createMVCViewModelsJSONExpression() {
        return new MVCViewModelsJSONExpression();
    }

    /**
     * Create an instance of {@link MulExpression }
     * 
     */
    public MulExpression createMulExpression() {
        return new MulExpression();
    }

    /**
     * Create an instance of {@link ApplicationPathExpression }
     * 
     */
    public ApplicationPathExpression createApplicationPathExpression() {
        return new ApplicationPathExpression();
    }

    /**
     * Create an instance of {@link TransformExpression }
     * 
     */
    public TransformExpression createTransformExpression() {
        return new TransformExpression();
    }

    /**
     * Create an instance of {@link MVCHtmlElementExpression }
     * 
     */
    public MVCHtmlElementExpression createMVCHtmlElementExpression() {
        return new MVCHtmlElementExpression();
    }

    /**
     * Create an instance of {@link DateTimeAddMonthsExpression }
     * 
     */
    public DateTimeAddMonthsExpression createDateTimeAddMonthsExpression() {
        return new DateTimeAddMonthsExpression();
    }

    /**
     * Create an instance of {@link HasValueExpression }
     * 
     */
    public HasValueExpression createHasValueExpression() {
        return new HasValueExpression();
    }

    /**
     * Create an instance of {@link IsNewExpression }
     * 
     */
    public IsNewExpression createIsNewExpression() {
        return new IsNewExpression();
    }

    /**
     * Create an instance of {@link BTContextPropertyExpression }
     * 
     */
    public BTContextPropertyExpression createBTContextPropertyExpression() {
        return new BTContextPropertyExpression();
    }

    /**
     * Create an instance of {@link TranslatorExpression }
     * 
     */
    public TranslatorExpression createTranslatorExpression() {
        return new TranslatorExpression();
    }

    /**
     * Create an instance of {@link IsNotNullExpression }
     * 
     */
    public IsNotNullExpression createIsNotNullExpression() {
        return new IsNotNullExpression();
    }

    /**
     * Create an instance of {@link CastExpression }
     * 
     */
    public CastExpression createCastExpression() {
        return new CastExpression();
    }

    /**
     * Create an instance of {@link ShortValueExpression }
     * 
     */
    public ShortValueExpression createShortValueExpression() {
        return new ShortValueExpression();
    }

    /**
     * Create an instance of {@link EventArgumentExpression }
     * 
     */
    public EventArgumentExpression createEventArgumentExpression() {
        return new EventArgumentExpression();
    }

    /**
     * Create an instance of {@link IsSupportControllerExpression }
     * 
     */
    public IsSupportControllerExpression createIsSupportControllerExpression() {
        return new IsSupportControllerExpression();
    }

    /**
     * Create an instance of {@link FirstOrDefaultCollectionExpression }
     * 
     */
    public FirstOrDefaultCollectionExpression createFirstOrDefaultCollectionExpression() {
        return new FirstOrDefaultCollectionExpression();
    }

    /**
     * Create an instance of {@link PerspectiveValueExpression }
     * 
     */
    public PerspectiveValueExpression createPerspectiveValueExpression() {
        return new PerspectiveValueExpression();
    }

    /**
     * Create an instance of {@link IsVisibleExpression }
     * 
     */
    public IsVisibleExpression createIsVisibleExpression() {
        return new IsVisibleExpression();
    }

    /**
     * Create an instance of {@link NegExpression }
     * 
     */
    public NegExpression createNegExpression() {
        return new NegExpression();
    }

    /**
     * Create an instance of {@link BTContextExpression }
     * 
     */
    public BTContextExpression createBTContextExpression() {
        return new BTContextExpression();
    }

    /**
     * Create an instance of {@link StaticContentExpression }
     * 
     */
    public StaticContentExpression createStaticContentExpression() {
        return new StaticContentExpression();
    }

    /**
     * Create an instance of {@link StringValueExpression }
     * 
     */
    public StringValueExpression createStringValueExpression() {
        return new StringValueExpression();
    }

    /**
     * Create an instance of {@link RequestProcessDataItemExpression }
     * 
     */
    public RequestProcessDataItemExpression createRequestProcessDataItemExpression() {
        return new RequestProcessDataItemExpression();
    }

    /**
     * Create an instance of {@link InvokeBusinessTransactionExpression }
     * 
     */
    public InvokeBusinessTransactionExpression createInvokeBusinessTransactionExpression() {
        return new InvokeBusinessTransactionExpression();
    }

    /**
     * Create an instance of {@link StringBuilderAppendItemCollection }
     * 
     */
    public StringBuilderAppendItemCollection createStringBuilderAppendItemCollection() {
        return new StringBuilderAppendItemCollection();
    }

    /**
     * Create an instance of {@link IntValueExpression }
     * 
     */
    public IntValueExpression createIntValueExpression() {
        return new IntValueExpression();
    }

    /**
     * Create an instance of {@link LowerOrEqualThanExpression }
     * 
     */
    public LowerOrEqualThanExpression createLowerOrEqualThanExpression() {
        return new LowerOrEqualThanExpression();
    }

    /**
     * Create an instance of {@link DecimalValueExpression }
     * 
     */
    public DecimalValueExpression createDecimalValueExpression() {
        return new DecimalValueExpression();
    }

    /**
     * Create an instance of {@link CurrentPerspectiveExpression }
     * 
     */
    public CurrentPerspectiveExpression createCurrentPerspectiveExpression() {
        return new CurrentPerspectiveExpression();
    }

    /**
     * Create an instance of {@link AndExpression }
     * 
     */
    public AndExpression createAndExpression() {
        return new AndExpression();
    }

    /**
     * Create an instance of {@link CommonSettingsPropertyExpression }
     * 
     */
    public CommonSettingsPropertyExpression createCommonSettingsPropertyExpression() {
        return new CommonSettingsPropertyExpression();
    }

    /**
     * Create an instance of {@link NullExpression }
     * 
     */
    public NullExpression createNullExpression() {
        return new NullExpression();
    }

    /**
     * Create an instance of {@link InvokeQueryAction }
     * 
     */
    public InvokeQueryAction createInvokeQueryAction() {
        return new InvokeQueryAction();
    }

    /**
     * Create an instance of {@link DateTimeAddYearsExpression }
     * 
     */
    public DateTimeAddYearsExpression createDateTimeAddYearsExpression() {
        return new DateTimeAddYearsExpression();
    }

    /**
     * Create an instance of {@link SpecificRulesExpression }
     * 
     */
    public SpecificRulesExpression createSpecificRulesExpression() {
        return new SpecificRulesExpression();
    }

    /**
     * Create an instance of {@link LongValueExpression }
     * 
     */
    public LongValueExpression createLongValueExpression() {
        return new LongValueExpression();
    }

    /**
     * Create an instance of {@link LiteralExpression }
     * 
     */
    public LiteralExpression createLiteralExpression() {
        return new LiteralExpression();
    }

    /**
     * Create an instance of {@link ScreenCodeExpression }
     * 
     */
    public ScreenCodeExpression createScreenCodeExpression() {
        return new ScreenCodeExpression();
    }

    /**
     * Create an instance of {@link PowerExpression }
     * 
     */
    public PowerExpression createPowerExpression() {
        return new PowerExpression();
    }

    /**
     * Create an instance of {@link StringBuilderExpression }
     * 
     */
    public StringBuilderExpression createStringBuilderExpression() {
        return new StringBuilderExpression();
    }

    /**
     * Create an instance of {@link AddItemsToListFromLov }
     * 
     */
    public AddItemsToListFromLov createAddItemsToListFromLov() {
        return new AddItemsToListFromLov();
    }

    /**
     * Create an instance of {@link RuleNameExpression }
     * 
     */
    public RuleNameExpression createRuleNameExpression() {
        return new RuleNameExpression();
    }

    /**
     * Create an instance of {@link ExpressionCollection }
     * 
     */
    public ExpressionCollection createExpressionCollection() {
        return new ExpressionCollection();
    }

    /**
     * Create an instance of {@link LowerThanExpression }
     * 
     */
    public LowerThanExpression createLowerThanExpression() {
        return new LowerThanExpression();
    }

    /**
     * Create an instance of {@link IfExpression }
     * 
     */
    public IfExpression createIfExpression() {
        return new IfExpression();
    }

    /**
     * Create an instance of {@link IsInStateExpressionStateCollection }
     * 
     */
    public IsInStateExpressionStateCollection createIsInStateExpressionStateCollection() {
        return new IsInStateExpressionStateCollection();
    }

    /**
     * Create an instance of {@link MinOfAllExpression }
     * 
     */
    public MinOfAllExpression createMinOfAllExpression() {
        return new MinOfAllExpression();
    }

    /**
     * Create an instance of {@link MVCViewCheckBoxListExpression }
     * 
     */
    public MVCViewCheckBoxListExpression createMVCViewCheckBoxListExpression() {
        return new MVCViewCheckBoxListExpression();
    }

    /**
     * Create an instance of {@link ExpressionChoice }
     * 
     */
    public ExpressionChoice createExpressionChoice() {
        return new ExpressionChoice();
    }

    /**
     * Create an instance of {@link SqrtExpression }
     * 
     */
    public SqrtExpression createSqrtExpression() {
        return new SqrtExpression();
    }

    /**
     * Create an instance of {@link ModelKeyExpression }
     * 
     */
    public ModelKeyExpression createModelKeyExpression() {
        return new ModelKeyExpression();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExpressionChoice }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.mdaengine.com/mdaengine/models/expressionsModel", name = "Formula")
    public JAXBElement<ExpressionChoice> createFormula(ExpressionChoice value) {
        return new JAXBElement<ExpressionChoice>(_Formula_QNAME, ExpressionChoice.class, null, value);
    }

}
