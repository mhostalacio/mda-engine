
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for StringFormatExpression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StringFormatExpression">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}Expression">
 *       &lt;sequence>
 *         &lt;element name="FormatString" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ExpressionChoice" minOccurs="0"/>
 *         &lt;element name="Values" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ExpressionCollection" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StringFormatExpression", propOrder = {
    "formatString",
    "values"
})
public class StringFormatExpression
    extends Expression
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "FormatString")
    protected ExpressionChoice formatString;
    @XmlElement(name = "Values")
    protected ExpressionCollection values;

    /**
     * Gets the value of the formatString property.
     * 
     * @return
     *     possible object is
     *     {@link ExpressionChoice }
     *     
     */
    public ExpressionChoice getFormatString() {
        return formatString;
    }

    /**
     * Sets the value of the formatString property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpressionChoice }
     *     
     */
    public void setFormatString(ExpressionChoice value) {
        this.formatString = value;
    }

    /**
     * Gets the value of the values property.
     * 
     * @return
     *     possible object is
     *     {@link ExpressionCollection }
     *     
     */
    public ExpressionCollection getValues() {
        return values;
    }

    /**
     * Sets the value of the values property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpressionCollection }
     *     
     */
    public void setValues(ExpressionCollection value) {
        this.values = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof StringFormatExpression)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final StringFormatExpression that = ((StringFormatExpression) object);
        {
            ExpressionChoice lhsFormatString;
            lhsFormatString = this.getFormatString();
            ExpressionChoice rhsFormatString;
            rhsFormatString = that.getFormatString();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "formatString", lhsFormatString), LocatorUtils.property(thatLocator, "formatString", rhsFormatString), lhsFormatString, rhsFormatString)) {
                return false;
            }
        }
        {
            ExpressionCollection lhsValues;
            lhsValues = this.getValues();
            ExpressionCollection rhsValues;
            rhsValues = that.getValues();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "values", lhsValues), LocatorUtils.property(thatLocator, "values", rhsValues), lhsValues, rhsValues)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof StringFormatExpression) {
            final StringFormatExpression copy = ((StringFormatExpression) draftCopy);
            if (this.formatString!= null) {
                ExpressionChoice sourceFormatString;
                sourceFormatString = this.getFormatString();
                ExpressionChoice copyFormatString = ((ExpressionChoice) strategy.copy(LocatorUtils.property(locator, "formatString", sourceFormatString), sourceFormatString));
                copy.setFormatString(copyFormatString);
            } else {
                copy.formatString = null;
            }
            if (this.values!= null) {
                ExpressionCollection sourceValues;
                sourceValues = this.getValues();
                ExpressionCollection copyValues = ((ExpressionCollection) strategy.copy(LocatorUtils.property(locator, "values", sourceValues), sourceValues));
                copy.setValues(copyValues);
            } else {
                copy.values = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new StringFormatExpression();
    }

}
