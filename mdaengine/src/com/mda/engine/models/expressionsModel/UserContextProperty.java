
package com.mda.engine.models.expressionsModel;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UserContextProperty.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="UserContextProperty">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CompanyList"/>
 *     &lt;enumeration value="CurrentCompanyCode"/>
 *     &lt;enumeration value="CurrentCompanyName"/>
 *     &lt;enumeration value="CurrentSkinCode"/>
 *     &lt;enumeration value="CurrentSkinFolder"/>
 *     &lt;enumeration value="LoginSkinCode"/>
 *     &lt;enumeration value="UserCode"/>
 *     &lt;enumeration value="CurrentAppUserId"/>
 *     &lt;enumeration value="UserName"/>
 *     &lt;enumeration value="UserAccessId"/>
 *     &lt;enumeration value="UserRuntimeSettingList"/>
 *     &lt;enumeration value="CurrentLanguage"/>
 *     &lt;enumeration value="CurrentCulture"/>
 *     &lt;enumeration value="InAnAdministratorMode"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "UserContextProperty")
@XmlEnum
public enum UserContextProperty {

    @XmlEnumValue("CompanyList")
    COMPANY_LIST("CompanyList"),
    @XmlEnumValue("CurrentCompanyCode")
    CURRENT_COMPANY_CODE("CurrentCompanyCode"),
    @XmlEnumValue("CurrentCompanyName")
    CURRENT_COMPANY_NAME("CurrentCompanyName"),
    @XmlEnumValue("CurrentSkinCode")
    CURRENT_SKIN_CODE("CurrentSkinCode"),
    @XmlEnumValue("CurrentSkinFolder")
    CURRENT_SKIN_FOLDER("CurrentSkinFolder"),
    @XmlEnumValue("LoginSkinCode")
    LOGIN_SKIN_CODE("LoginSkinCode"),
    @XmlEnumValue("UserCode")
    USER_CODE("UserCode"),
    @XmlEnumValue("CurrentAppUserId")
    CURRENT_APP_USER_ID("CurrentAppUserId"),
    @XmlEnumValue("UserName")
    USER_NAME("UserName"),
    @XmlEnumValue("UserAccessId")
    USER_ACCESS_ID("UserAccessId"),
    @XmlEnumValue("UserRuntimeSettingList")
    USER_RUNTIME_SETTING_LIST("UserRuntimeSettingList"),
    @XmlEnumValue("CurrentLanguage")
    CURRENT_LANGUAGE("CurrentLanguage"),
    @XmlEnumValue("CurrentCulture")
    CURRENT_CULTURE("CurrentCulture"),
    @XmlEnumValue("InAnAdministratorMode")
    IN_AN_ADMINISTRATOR_MODE("InAnAdministratorMode");
    private final String value;

    UserContextProperty(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static UserContextProperty fromValue(String v) {
        for (UserContextProperty c: UserContextProperty.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
