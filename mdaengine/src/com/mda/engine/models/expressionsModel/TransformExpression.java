
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for TransformExpression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransformExpression">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ExpressionChoice">
 *       &lt;sequence>
 *         &lt;element name="TransformerPackage" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}PackageReferenceChoice" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransformExpression", propOrder = {
    "transformerPackage"
})
public class TransformExpression
    extends ExpressionChoice
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "TransformerPackage")
    protected PackageReferenceChoice transformerPackage;

    /**
     * Gets the value of the transformerPackage property.
     * 
     * @return
     *     possible object is
     *     {@link PackageReferenceChoice }
     *     
     */
    public PackageReferenceChoice getTransformerPackage() {
        return transformerPackage;
    }

    /**
     * Sets the value of the transformerPackage property.
     * 
     * @param value
     *     allowed object is
     *     {@link PackageReferenceChoice }
     *     
     */
    public void setTransformerPackage(PackageReferenceChoice value) {
        this.transformerPackage = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof TransformExpression)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final TransformExpression that = ((TransformExpression) object);
        {
            PackageReferenceChoice lhsTransformerPackage;
            lhsTransformerPackage = this.getTransformerPackage();
            PackageReferenceChoice rhsTransformerPackage;
            rhsTransformerPackage = that.getTransformerPackage();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "transformerPackage", lhsTransformerPackage), LocatorUtils.property(thatLocator, "transformerPackage", rhsTransformerPackage), lhsTransformerPackage, rhsTransformerPackage)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof TransformExpression) {
            final TransformExpression copy = ((TransformExpression) draftCopy);
            if (this.transformerPackage!= null) {
                PackageReferenceChoice sourceTransformerPackage;
                sourceTransformerPackage = this.getTransformerPackage();
                PackageReferenceChoice copyTransformerPackage = ((PackageReferenceChoice) strategy.copy(LocatorUtils.property(locator, "transformerPackage", sourceTransformerPackage), sourceTransformerPackage));
                copy.setTransformerPackage(copyTransformerPackage);
            } else {
                copy.transformerPackage = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new TransformExpression();
    }

}
