
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for EnumValueExpression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EnumValueExpression">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ValueExpression">
 *       &lt;attribute name="EnumTypeFullName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="EnumValue" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="ConvertToInt" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="CreateValueNode" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="CreateValueWrapper" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="UseTranslatorValueWrapper" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnumValueExpression")
public class EnumValueExpression
    extends ValueExpression
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "EnumTypeFullName", required = true)
    protected String enumTypeFullName;
    @XmlAttribute(name = "EnumValue", required = true)
    protected String enumValue;
    @XmlAttribute(name = "ConvertToInt")
    protected Boolean convertToInt;
    @XmlAttribute(name = "CreateValueNode")
    protected Boolean createValueNode;
    @XmlAttribute(name = "CreateValueWrapper")
    protected Boolean createValueWrapper;
    @XmlAttribute(name = "UseTranslatorValueWrapper")
    protected Boolean useTranslatorValueWrapper;

    /**
     * Gets the value of the enumTypeFullName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnumTypeFullName() {
        return enumTypeFullName;
    }

    /**
     * Sets the value of the enumTypeFullName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnumTypeFullName(String value) {
        this.enumTypeFullName = value;
    }

    /**
     * Gets the value of the enumValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnumValue() {
        return enumValue;
    }

    /**
     * Sets the value of the enumValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnumValue(String value) {
        this.enumValue = value;
    }

    /**
     * Gets the value of the convertToInt property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isConvertToInt() {
        return convertToInt;
    }

    /**
     * Sets the value of the convertToInt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setConvertToInt(Boolean value) {
        this.convertToInt = value;
    }

    /**
     * Gets the value of the createValueNode property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isCreateValueNode() {
        if (createValueNode == null) {
            return false;
        } else {
            return createValueNode;
        }
    }

    /**
     * Sets the value of the createValueNode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCreateValueNode(Boolean value) {
        this.createValueNode = value;
    }

    /**
     * Gets the value of the createValueWrapper property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isCreateValueWrapper() {
        if (createValueWrapper == null) {
            return false;
        } else {
            return createValueWrapper;
        }
    }

    /**
     * Sets the value of the createValueWrapper property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCreateValueWrapper(Boolean value) {
        this.createValueWrapper = value;
    }

    /**
     * Gets the value of the useTranslatorValueWrapper property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isUseTranslatorValueWrapper() {
        if (useTranslatorValueWrapper == null) {
            return false;
        } else {
            return useTranslatorValueWrapper;
        }
    }

    /**
     * Sets the value of the useTranslatorValueWrapper property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseTranslatorValueWrapper(Boolean value) {
        this.useTranslatorValueWrapper = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof EnumValueExpression)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final EnumValueExpression that = ((EnumValueExpression) object);
        {
            String lhsEnumTypeFullName;
            lhsEnumTypeFullName = this.getEnumTypeFullName();
            String rhsEnumTypeFullName;
            rhsEnumTypeFullName = that.getEnumTypeFullName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "enumTypeFullName", lhsEnumTypeFullName), LocatorUtils.property(thatLocator, "enumTypeFullName", rhsEnumTypeFullName), lhsEnumTypeFullName, rhsEnumTypeFullName)) {
                return false;
            }
        }
        {
            String lhsEnumValue;
            lhsEnumValue = this.getEnumValue();
            String rhsEnumValue;
            rhsEnumValue = that.getEnumValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "enumValue", lhsEnumValue), LocatorUtils.property(thatLocator, "enumValue", rhsEnumValue), lhsEnumValue, rhsEnumValue)) {
                return false;
            }
        }
        {
            Boolean lhsConvertToInt;
            lhsConvertToInt = this.isConvertToInt();
            Boolean rhsConvertToInt;
            rhsConvertToInt = that.isConvertToInt();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "convertToInt", lhsConvertToInt), LocatorUtils.property(thatLocator, "convertToInt", rhsConvertToInt), lhsConvertToInt, rhsConvertToInt)) {
                return false;
            }
        }
        {
            boolean lhsCreateValueNode;
            lhsCreateValueNode = this.isCreateValueNode();
            boolean rhsCreateValueNode;
            rhsCreateValueNode = that.isCreateValueNode();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "createValueNode", lhsCreateValueNode), LocatorUtils.property(thatLocator, "createValueNode", rhsCreateValueNode), lhsCreateValueNode, rhsCreateValueNode)) {
                return false;
            }
        }
        {
            boolean lhsCreateValueWrapper;
            lhsCreateValueWrapper = this.isCreateValueWrapper();
            boolean rhsCreateValueWrapper;
            rhsCreateValueWrapper = that.isCreateValueWrapper();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "createValueWrapper", lhsCreateValueWrapper), LocatorUtils.property(thatLocator, "createValueWrapper", rhsCreateValueWrapper), lhsCreateValueWrapper, rhsCreateValueWrapper)) {
                return false;
            }
        }
        {
            boolean lhsUseTranslatorValueWrapper;
            lhsUseTranslatorValueWrapper = this.isUseTranslatorValueWrapper();
            boolean rhsUseTranslatorValueWrapper;
            rhsUseTranslatorValueWrapper = that.isUseTranslatorValueWrapper();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "useTranslatorValueWrapper", lhsUseTranslatorValueWrapper), LocatorUtils.property(thatLocator, "useTranslatorValueWrapper", rhsUseTranslatorValueWrapper), lhsUseTranslatorValueWrapper, rhsUseTranslatorValueWrapper)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof EnumValueExpression) {
            final EnumValueExpression copy = ((EnumValueExpression) draftCopy);
            if (this.enumTypeFullName!= null) {
                String sourceEnumTypeFullName;
                sourceEnumTypeFullName = this.getEnumTypeFullName();
                String copyEnumTypeFullName = ((String) strategy.copy(LocatorUtils.property(locator, "enumTypeFullName", sourceEnumTypeFullName), sourceEnumTypeFullName));
                copy.setEnumTypeFullName(copyEnumTypeFullName);
            } else {
                copy.enumTypeFullName = null;
            }
            if (this.enumValue!= null) {
                String sourceEnumValue;
                sourceEnumValue = this.getEnumValue();
                String copyEnumValue = ((String) strategy.copy(LocatorUtils.property(locator, "enumValue", sourceEnumValue), sourceEnumValue));
                copy.setEnumValue(copyEnumValue);
            } else {
                copy.enumValue = null;
            }
            if (this.convertToInt!= null) {
                Boolean sourceConvertToInt;
                sourceConvertToInt = this.isConvertToInt();
                Boolean copyConvertToInt = ((Boolean) strategy.copy(LocatorUtils.property(locator, "convertToInt", sourceConvertToInt), sourceConvertToInt));
                copy.setConvertToInt(copyConvertToInt);
            } else {
                copy.convertToInt = null;
            }
            if (this.createValueNode!= null) {
                boolean sourceCreateValueNode;
                sourceCreateValueNode = this.isCreateValueNode();
                boolean copyCreateValueNode = strategy.copy(LocatorUtils.property(locator, "createValueNode", sourceCreateValueNode), sourceCreateValueNode);
                copy.setCreateValueNode(copyCreateValueNode);
            } else {
                copy.createValueNode = null;
            }
            if (this.createValueWrapper!= null) {
                boolean sourceCreateValueWrapper;
                sourceCreateValueWrapper = this.isCreateValueWrapper();
                boolean copyCreateValueWrapper = strategy.copy(LocatorUtils.property(locator, "createValueWrapper", sourceCreateValueWrapper), sourceCreateValueWrapper);
                copy.setCreateValueWrapper(copyCreateValueWrapper);
            } else {
                copy.createValueWrapper = null;
            }
            if (this.useTranslatorValueWrapper!= null) {
                boolean sourceUseTranslatorValueWrapper;
                sourceUseTranslatorValueWrapper = this.isUseTranslatorValueWrapper();
                boolean copyUseTranslatorValueWrapper = strategy.copy(LocatorUtils.property(locator, "useTranslatorValueWrapper", sourceUseTranslatorValueWrapper), sourceUseTranslatorValueWrapper);
                copy.setUseTranslatorValueWrapper(copyUseTranslatorValueWrapper);
            } else {
                copy.useTranslatorValueWrapper = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new EnumValueExpression();
    }
    
//--simple--preserve
    
    @Override
	public void writeSQLExpression(com.mda.engine.utils.Stringcode c, com.mda.engine.core.Product product) {
    	com.mda.engine.models.businessObjectModel.LovDefinitionType lov = product.getLovByName(this.getEnumTypeFullName());
    	com.mda.engine.models.businessObjectModel.LovItem item = lov.getItemByCode(this.getEnumValue());
    	c.write(String.valueOf(item.getKey()));
	}
    
    @Override
    public String getCSharpExpression(com.mda.engine.core.Product product) {    	
    	com.mda.engine.models.businessObjectModel.LovDefinitionType lov = product.getLovByName(this.getEnumTypeFullName());
    	com.mda.engine.models.businessObjectModel.LovItem item = lov.getItemByCode(this.getEnumValue());
    	
    	return com.mda.engine.utils.StringUtils.format("{0}.{1}", lov.getName(), item.getCode());
    }   
	
//--simple--preserve

}
