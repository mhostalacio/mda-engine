
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for StringBuilderAppendItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StringBuilderAppendItem">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ExpressionChoice">
 *       &lt;sequence>
 *         &lt;element name="OnlyIf" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}BooleanExpressionChoice" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="NewLine" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StringBuilderAppendItem", propOrder = {
    "onlyIf"
})
public class StringBuilderAppendItem
    extends ExpressionChoice
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "OnlyIf")
    protected BooleanExpressionChoice onlyIf;
    @XmlAttribute(name = "NewLine")
    protected Boolean newLine;

    /**
     * Gets the value of the onlyIf property.
     * 
     * @return
     *     possible object is
     *     {@link BooleanExpressionChoice }
     *     
     */
    public BooleanExpressionChoice getOnlyIf() {
        return onlyIf;
    }

    /**
     * Sets the value of the onlyIf property.
     * 
     * @param value
     *     allowed object is
     *     {@link BooleanExpressionChoice }
     *     
     */
    public void setOnlyIf(BooleanExpressionChoice value) {
        this.onlyIf = value;
    }

    /**
     * Gets the value of the newLine property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isNewLine() {
        if (newLine == null) {
            return false;
        } else {
            return newLine;
        }
    }

    /**
     * Sets the value of the newLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNewLine(Boolean value) {
        this.newLine = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof StringBuilderAppendItem)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final StringBuilderAppendItem that = ((StringBuilderAppendItem) object);
        {
            BooleanExpressionChoice lhsOnlyIf;
            lhsOnlyIf = this.getOnlyIf();
            BooleanExpressionChoice rhsOnlyIf;
            rhsOnlyIf = that.getOnlyIf();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "onlyIf", lhsOnlyIf), LocatorUtils.property(thatLocator, "onlyIf", rhsOnlyIf), lhsOnlyIf, rhsOnlyIf)) {
                return false;
            }
        }
        {
            boolean lhsNewLine;
            lhsNewLine = this.isNewLine();
            boolean rhsNewLine;
            rhsNewLine = that.isNewLine();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "newLine", lhsNewLine), LocatorUtils.property(thatLocator, "newLine", rhsNewLine), lhsNewLine, rhsNewLine)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof StringBuilderAppendItem) {
            final StringBuilderAppendItem copy = ((StringBuilderAppendItem) draftCopy);
            if (this.onlyIf!= null) {
                BooleanExpressionChoice sourceOnlyIf;
                sourceOnlyIf = this.getOnlyIf();
                BooleanExpressionChoice copyOnlyIf = ((BooleanExpressionChoice) strategy.copy(LocatorUtils.property(locator, "onlyIf", sourceOnlyIf), sourceOnlyIf));
                copy.setOnlyIf(copyOnlyIf);
            } else {
                copy.onlyIf = null;
            }
            if (this.newLine!= null) {
                boolean sourceNewLine;
                sourceNewLine = this.isNewLine();
                boolean copyNewLine = strategy.copy(LocatorUtils.property(locator, "newLine", sourceNewLine), sourceNewLine);
                copy.setNewLine(copyNewLine);
            } else {
                copy.newLine = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new StringBuilderAppendItem();
    }

}
