
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for CastExpression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CastExpression">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="TypeName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="PackageName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="ApplicationName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CastExpression")
public class CastExpression
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "TypeName", required = true)
    protected String typeName;
    @XmlAttribute(name = "PackageName")
    protected String packageName;
    @XmlAttribute(name = "ApplicationName")
    protected String applicationName;

    /**
     * Gets the value of the typeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeName() {
        return typeName;
    }

    /**
     * Sets the value of the typeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeName(String value) {
        this.typeName = value;
    }

    /**
     * Gets the value of the packageName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackageName() {
        return packageName;
    }

    /**
     * Sets the value of the packageName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackageName(String value) {
        this.packageName = value;
    }

    /**
     * Gets the value of the applicationName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicationName() {
        return applicationName;
    }

    /**
     * Sets the value of the applicationName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicationName(String value) {
        this.applicationName = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof CastExpression)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final CastExpression that = ((CastExpression) object);
        {
            String lhsTypeName;
            lhsTypeName = this.getTypeName();
            String rhsTypeName;
            rhsTypeName = that.getTypeName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "typeName", lhsTypeName), LocatorUtils.property(thatLocator, "typeName", rhsTypeName), lhsTypeName, rhsTypeName)) {
                return false;
            }
        }
        {
            String lhsPackageName;
            lhsPackageName = this.getPackageName();
            String rhsPackageName;
            rhsPackageName = that.getPackageName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "packageName", lhsPackageName), LocatorUtils.property(thatLocator, "packageName", rhsPackageName), lhsPackageName, rhsPackageName)) {
                return false;
            }
        }
        {
            String lhsApplicationName;
            lhsApplicationName = this.getApplicationName();
            String rhsApplicationName;
            rhsApplicationName = that.getApplicationName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "applicationName", lhsApplicationName), LocatorUtils.property(thatLocator, "applicationName", rhsApplicationName), lhsApplicationName, rhsApplicationName)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof CastExpression) {
            final CastExpression copy = ((CastExpression) draftCopy);
            if (this.typeName!= null) {
                String sourceTypeName;
                sourceTypeName = this.getTypeName();
                String copyTypeName = ((String) strategy.copy(LocatorUtils.property(locator, "typeName", sourceTypeName), sourceTypeName));
                copy.setTypeName(copyTypeName);
            } else {
                copy.typeName = null;
            }
            if (this.packageName!= null) {
                String sourcePackageName;
                sourcePackageName = this.getPackageName();
                String copyPackageName = ((String) strategy.copy(LocatorUtils.property(locator, "packageName", sourcePackageName), sourcePackageName));
                copy.setPackageName(copyPackageName);
            } else {
                copy.packageName = null;
            }
            if (this.applicationName!= null) {
                String sourceApplicationName;
                sourceApplicationName = this.getApplicationName();
                String copyApplicationName = ((String) strategy.copy(LocatorUtils.property(locator, "applicationName", sourceApplicationName), sourceApplicationName));
                copy.setApplicationName(copyApplicationName);
            } else {
                copy.applicationName = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new CastExpression();
    }

}
