
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for BooleanExpressionCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BooleanExpressionCollection">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}Expression">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;element name="AND" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}AndExpression"/>
 *         &lt;element name="OR" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}OrExpression"/>
 *         &lt;element name="Not" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}NotExpression"/>
 *         &lt;element name="IsNull" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IsNullExpression"/>
 *         &lt;element name="IsNotNull" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IsNotNullExpression"/>
 *         &lt;element name="HasValue" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}HasValueExpression"/>
 *         &lt;element name="Equals" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}EqualsExpression"/>
 *         &lt;element name="NotEquals" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}NotEqualsExpression"/>
 *         &lt;element name="GreaterThan" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}GreaterThanExpression"/>
 *         &lt;element name="GreaterOrEqualThan" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}GreaterOrEqualThanExpression"/>
 *         &lt;element name="LowerThan" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}LowerThanExpression"/>
 *         &lt;element name="LowerOrEqualThan" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}LowerOrEqualThanExpression"/>
 *         &lt;element name="Like" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}LikeExpression"/>
 *         &lt;element name="IsNew" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IsNewExpression"/>
 *         &lt;element name="IsDirty" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IsDirtyExpression"/>
 *         &lt;element name="IsEnable" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IsEnableExpression"/>
 *         &lt;element name="IsVisible" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IsVisibleExpression"/>
 *         &lt;element name="IsRequired" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IsRequiredExpression"/>
 *         &lt;element name="IsValid" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IsValidExpression"/>
 *         &lt;element name="IsTrue" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IsTrueExpression"/>
 *         &lt;element name="GetValue" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}GetValueExpression"/>
 *         &lt;element name="Contains" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ContainsValueExpression"/>
 *         &lt;element name="IsInState" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IsInStateExpression"/>
 *         &lt;element name="Literal" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}LiteralExpression"/>
 *         &lt;element name="ExecuteMethod" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ExecuteMethodExpression"/>
 *       &lt;/choice>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BooleanExpressionCollection", propOrder = {
    "expressions"
})
@XmlSeeAlso({
    AndExpression.class,
    OrExpression.class
})
public class BooleanExpressionCollection
    extends Expression
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElements({
        @XmlElement(name = "Literal", type = LiteralExpression.class),
        @XmlElement(name = "IsNotNull", type = IsNotNullExpression.class),
        @XmlElement(name = "HasValue", type = HasValueExpression.class),
        @XmlElement(name = "IsTrue", type = IsTrueExpression.class),
        @XmlElement(name = "OR", type = OrExpression.class),
        @XmlElement(name = "IsEnable", type = IsEnableExpression.class),
        @XmlElement(name = "Equals", type = EqualsExpression.class),
        @XmlElement(name = "ExecuteMethod", type = ExecuteMethodExpression.class),
        @XmlElement(name = "IsInState", type = IsInStateExpression.class),
        @XmlElement(name = "IsNew", type = IsNewExpression.class),
        @XmlElement(name = "LowerThan", type = LowerThanExpression.class),
        @XmlElement(name = "GreaterOrEqualThan", type = GreaterOrEqualThanExpression.class),
        @XmlElement(name = "IsVisible", type = IsVisibleExpression.class),
        @XmlElement(name = "Contains", type = ContainsValueExpression.class),
        @XmlElement(name = "Like", type = LikeExpression.class),
        @XmlElement(name = "GreaterThan", type = GreaterThanExpression.class),
        @XmlElement(name = "IsNull", type = IsNullExpression.class),
        @XmlElement(name = "NotEquals", type = NotEqualsExpression.class),
        @XmlElement(name = "LowerOrEqualThan", type = LowerOrEqualThanExpression.class),
        @XmlElement(name = "IsValid", type = IsValidExpression.class),
        @XmlElement(name = "AND", type = AndExpression.class),
        @XmlElement(name = "Not", type = NotExpression.class),
        @XmlElement(name = "GetValue", type = GetValueExpression.class),
        @XmlElement(name = "IsRequired", type = IsRequiredExpression.class),
        @XmlElement(name = "IsDirty", type = IsDirtyExpression.class)
    })
    protected List<Expression> expressions;

    /**
     * Gets the value of the expressions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the expressions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExpressions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LiteralExpression }
     * {@link IsNotNullExpression }
     * {@link HasValueExpression }
     * {@link IsTrueExpression }
     * {@link OrExpression }
     * {@link IsEnableExpression }
     * {@link EqualsExpression }
     * {@link ExecuteMethodExpression }
     * {@link IsInStateExpression }
     * {@link IsNewExpression }
     * {@link LowerThanExpression }
     * {@link GreaterOrEqualThanExpression }
     * {@link IsVisibleExpression }
     * {@link ContainsValueExpression }
     * {@link LikeExpression }
     * {@link GreaterThanExpression }
     * {@link IsNullExpression }
     * {@link NotEqualsExpression }
     * {@link LowerOrEqualThanExpression }
     * {@link IsValidExpression }
     * {@link AndExpression }
     * {@link NotExpression }
     * {@link GetValueExpression }
     * {@link IsRequiredExpression }
     * {@link IsDirtyExpression }
     * 
     * 
     */
    public List<Expression> getExpressions() {
        if (expressions == null) {
            expressions = new ArrayList<Expression>();
        }
        return this.expressions;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof BooleanExpressionCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final BooleanExpressionCollection that = ((BooleanExpressionCollection) object);
        {
            List<Expression> lhsExpressions;
            lhsExpressions = this.getExpressions();
            List<Expression> rhsExpressions;
            rhsExpressions = that.getExpressions();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "expressions", lhsExpressions), LocatorUtils.property(thatLocator, "expressions", rhsExpressions), lhsExpressions, rhsExpressions)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof BooleanExpressionCollection) {
            final BooleanExpressionCollection copy = ((BooleanExpressionCollection) draftCopy);
            if ((this.expressions!= null)&&(!this.expressions.isEmpty())) {
                List<Expression> sourceExpressions;
                sourceExpressions = this.getExpressions();
                @SuppressWarnings("unchecked")
                List<Expression> copyExpressions = ((List<Expression> ) strategy.copy(LocatorUtils.property(locator, "expressions", sourceExpressions), sourceExpressions));
                copy.expressions = null;
                List<Expression> uniqueExpressionsl = copy.getExpressions();
                uniqueExpressionsl.addAll(copyExpressions);
            } else {
                copy.expressions = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new BooleanExpressionCollection();
    }

}
