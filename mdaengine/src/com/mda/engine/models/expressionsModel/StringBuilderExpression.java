
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for StringBuilderExpression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StringBuilderExpression">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}Expression">
 *       &lt;sequence>
 *         &lt;element name="Append" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}StringBuilderAppendItemCollection"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StringBuilderExpression", propOrder = {
    "append"
})
public class StringBuilderExpression
    extends Expression
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Append", required = true)
    protected StringBuilderAppendItemCollection append;

    /**
     * Gets the value of the append property.
     * 
     * @return
     *     possible object is
     *     {@link StringBuilderAppendItemCollection }
     *     
     */
    public StringBuilderAppendItemCollection getAppend() {
        return append;
    }

    /**
     * Sets the value of the append property.
     * 
     * @param value
     *     allowed object is
     *     {@link StringBuilderAppendItemCollection }
     *     
     */
    public void setAppend(StringBuilderAppendItemCollection value) {
        this.append = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof StringBuilderExpression)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final StringBuilderExpression that = ((StringBuilderExpression) object);
        {
            StringBuilderAppendItemCollection lhsAppend;
            lhsAppend = this.getAppend();
            StringBuilderAppendItemCollection rhsAppend;
            rhsAppend = that.getAppend();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "append", lhsAppend), LocatorUtils.property(thatLocator, "append", rhsAppend), lhsAppend, rhsAppend)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof StringBuilderExpression) {
            final StringBuilderExpression copy = ((StringBuilderExpression) draftCopy);
            if (this.append!= null) {
                StringBuilderAppendItemCollection sourceAppend;
                sourceAppend = this.getAppend();
                StringBuilderAppendItemCollection copyAppend = ((StringBuilderAppendItemCollection) strategy.copy(LocatorUtils.property(locator, "append", sourceAppend), sourceAppend));
                copy.setAppend(copyAppend);
            } else {
                copy.append = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new StringBuilderExpression();
    }

}
