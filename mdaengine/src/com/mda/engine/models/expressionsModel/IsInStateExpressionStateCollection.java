
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for IsInStateExpressionStateCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IsInStateExpressionStateCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="State" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IsInStateExpressionState" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IsInStateExpressionStateCollection", propOrder = {
    "state"
})
public class IsInStateExpressionStateCollection
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "State", required = true)
    protected List<IsInStateExpressionState> state;

    /**
     * Gets the value of the state property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the state property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getState().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IsInStateExpressionState }
     * 
     * 
     */
    public List<IsInStateExpressionState> getState() {
        if (state == null) {
            state = new ArrayList<IsInStateExpressionState>();
        }
        return this.state;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof IsInStateExpressionStateCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final IsInStateExpressionStateCollection that = ((IsInStateExpressionStateCollection) object);
        {
            List<IsInStateExpressionState> lhsState;
            lhsState = this.getState();
            List<IsInStateExpressionState> rhsState;
            rhsState = that.getState();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "state", lhsState), LocatorUtils.property(thatLocator, "state", rhsState), lhsState, rhsState)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof IsInStateExpressionStateCollection) {
            final IsInStateExpressionStateCollection copy = ((IsInStateExpressionStateCollection) draftCopy);
            if ((this.state!= null)&&(!this.state.isEmpty())) {
                List<IsInStateExpressionState> sourceState;
                sourceState = this.getState();
                @SuppressWarnings("unchecked")
                List<IsInStateExpressionState> copyState = ((List<IsInStateExpressionState> ) strategy.copy(LocatorUtils.property(locator, "state", sourceState), sourceState));
                copy.state = null;
                List<IsInStateExpressionState> uniqueStatel = copy.getState();
                uniqueStatel.addAll(copyState);
            } else {
                copy.state = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new IsInStateExpressionStateCollection();
    }

}
