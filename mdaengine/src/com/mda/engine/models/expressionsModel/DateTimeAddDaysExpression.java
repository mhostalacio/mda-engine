
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for DateTimeAddDaysExpression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DateTimeAddDaysExpression">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}DateTimeComputedValueExpression">
 *       &lt;attribute name="DaysToAdd" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DateTimeAddDaysExpression")
public class DateTimeAddDaysExpression
    extends DateTimeComputedValueExpression
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "DaysToAdd", required = true)
    protected int daysToAdd;

    /**
     * Gets the value of the daysToAdd property.
     * 
     */
    public int getDaysToAdd() {
        return daysToAdd;
    }

    /**
     * Sets the value of the daysToAdd property.
     * 
     */
    public void setDaysToAdd(int value) {
        this.daysToAdd = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof DateTimeAddDaysExpression)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final DateTimeAddDaysExpression that = ((DateTimeAddDaysExpression) object);
        {
            int lhsDaysToAdd;
            lhsDaysToAdd = this.getDaysToAdd();
            int rhsDaysToAdd;
            rhsDaysToAdd = that.getDaysToAdd();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "daysToAdd", lhsDaysToAdd), LocatorUtils.property(thatLocator, "daysToAdd", rhsDaysToAdd), lhsDaysToAdd, rhsDaysToAdd)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof DateTimeAddDaysExpression) {
            final DateTimeAddDaysExpression copy = ((DateTimeAddDaysExpression) draftCopy);
            int sourceDaysToAdd;
            sourceDaysToAdd = this.getDaysToAdd();
            int copyDaysToAdd = strategy.copy(LocatorUtils.property(locator, "daysToAdd", sourceDaysToAdd), sourceDaysToAdd);
            copy.setDaysToAdd(copyDaysToAdd);
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new DateTimeAddDaysExpression();
    }

}
