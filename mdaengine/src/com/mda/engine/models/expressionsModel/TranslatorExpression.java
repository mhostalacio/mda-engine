
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for TranslatorExpression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TranslatorExpression">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}Expression">
 *       &lt;attribute name="SentenceCode" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="SentenceGroup" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="SentenceType" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TranslatorExpression")
public class TranslatorExpression
    extends Expression
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "SentenceCode", required = true)
    protected String sentenceCode;
    @XmlAttribute(name = "SentenceGroup", required = true)
    protected String sentenceGroup;
    @XmlAttribute(name = "SentenceType", required = true)
    protected String sentenceType;

    /**
     * Gets the value of the sentenceCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSentenceCode() {
        return sentenceCode;
    }

    /**
     * Sets the value of the sentenceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSentenceCode(String value) {
        this.sentenceCode = value;
    }

    /**
     * Gets the value of the sentenceGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSentenceGroup() {
        return sentenceGroup;
    }

    /**
     * Sets the value of the sentenceGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSentenceGroup(String value) {
        this.sentenceGroup = value;
    }

    /**
     * Gets the value of the sentenceType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSentenceType() {
        return sentenceType;
    }

    /**
     * Sets the value of the sentenceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSentenceType(String value) {
        this.sentenceType = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof TranslatorExpression)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final TranslatorExpression that = ((TranslatorExpression) object);
        {
            String lhsSentenceCode;
            lhsSentenceCode = this.getSentenceCode();
            String rhsSentenceCode;
            rhsSentenceCode = that.getSentenceCode();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "sentenceCode", lhsSentenceCode), LocatorUtils.property(thatLocator, "sentenceCode", rhsSentenceCode), lhsSentenceCode, rhsSentenceCode)) {
                return false;
            }
        }
        {
            String lhsSentenceGroup;
            lhsSentenceGroup = this.getSentenceGroup();
            String rhsSentenceGroup;
            rhsSentenceGroup = that.getSentenceGroup();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "sentenceGroup", lhsSentenceGroup), LocatorUtils.property(thatLocator, "sentenceGroup", rhsSentenceGroup), lhsSentenceGroup, rhsSentenceGroup)) {
                return false;
            }
        }
        {
            String lhsSentenceType;
            lhsSentenceType = this.getSentenceType();
            String rhsSentenceType;
            rhsSentenceType = that.getSentenceType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "sentenceType", lhsSentenceType), LocatorUtils.property(thatLocator, "sentenceType", rhsSentenceType), lhsSentenceType, rhsSentenceType)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof TranslatorExpression) {
            final TranslatorExpression copy = ((TranslatorExpression) draftCopy);
            if (this.sentenceCode!= null) {
                String sourceSentenceCode;
                sourceSentenceCode = this.getSentenceCode();
                String copySentenceCode = ((String) strategy.copy(LocatorUtils.property(locator, "sentenceCode", sourceSentenceCode), sourceSentenceCode));
                copy.setSentenceCode(copySentenceCode);
            } else {
                copy.sentenceCode = null;
            }
            if (this.sentenceGroup!= null) {
                String sourceSentenceGroup;
                sourceSentenceGroup = this.getSentenceGroup();
                String copySentenceGroup = ((String) strategy.copy(LocatorUtils.property(locator, "sentenceGroup", sourceSentenceGroup), sourceSentenceGroup));
                copy.setSentenceGroup(copySentenceGroup);
            } else {
                copy.sentenceGroup = null;
            }
            if (this.sentenceType!= null) {
                String sourceSentenceType;
                sourceSentenceType = this.getSentenceType();
                String copySentenceType = ((String) strategy.copy(LocatorUtils.property(locator, "sentenceType", sourceSentenceType), sourceSentenceType));
                copy.setSentenceType(copySentenceType);
            } else {
                copy.sentenceType = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new TranslatorExpression();
    }

}
