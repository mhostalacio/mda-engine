
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for BTContextExpression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BTContextExpression">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}Expression">
 *       &lt;attribute name="ValueKey" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BTContextExpression")
public class BTContextExpression
    extends Expression
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "ValueKey", required = true)
    protected String valueKey;

    /**
     * Gets the value of the valueKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValueKey() {
        return valueKey;
    }

    /**
     * Sets the value of the valueKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValueKey(String value) {
        this.valueKey = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof BTContextExpression)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final BTContextExpression that = ((BTContextExpression) object);
        {
            String lhsValueKey;
            lhsValueKey = this.getValueKey();
            String rhsValueKey;
            rhsValueKey = that.getValueKey();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "valueKey", lhsValueKey), LocatorUtils.property(thatLocator, "valueKey", rhsValueKey), lhsValueKey, rhsValueKey)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof BTContextExpression) {
            final BTContextExpression copy = ((BTContextExpression) draftCopy);
            if (this.valueKey!= null) {
                String sourceValueKey;
                sourceValueKey = this.getValueKey();
                String copyValueKey = ((String) strategy.copy(LocatorUtils.property(locator, "valueKey", sourceValueKey), sourceValueKey));
                copy.setValueKey(copyValueKey);
            } else {
                copy.valueKey = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new BTContextExpression();
    }

}
