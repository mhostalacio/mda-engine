
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for MVCViewCheckBoxListExpression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVCViewCheckBoxListExpression">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}MVCViewElementExpression">
 *       &lt;attribute name="ContainerElementId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Checked" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVCViewCheckBoxListExpression")
public class MVCViewCheckBoxListExpression
    extends MVCViewElementExpression
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "ContainerElementId", required = true)
    protected String containerElementId;
    @XmlAttribute(name = "Checked")
    protected Boolean checked;

    /**
     * Gets the value of the containerElementId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContainerElementId() {
        return containerElementId;
    }

    /**
     * Sets the value of the containerElementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContainerElementId(String value) {
        this.containerElementId = value;
    }

    /**
     * Gets the value of the checked property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isChecked() {
        if (checked == null) {
            return true;
        } else {
            return checked;
        }
    }

    /**
     * Sets the value of the checked property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setChecked(Boolean value) {
        this.checked = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MVCViewCheckBoxListExpression)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final MVCViewCheckBoxListExpression that = ((MVCViewCheckBoxListExpression) object);
        {
            String lhsContainerElementId;
            lhsContainerElementId = this.getContainerElementId();
            String rhsContainerElementId;
            rhsContainerElementId = that.getContainerElementId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "containerElementId", lhsContainerElementId), LocatorUtils.property(thatLocator, "containerElementId", rhsContainerElementId), lhsContainerElementId, rhsContainerElementId)) {
                return false;
            }
        }
        {
            boolean lhsChecked;
            lhsChecked = this.isChecked();
            boolean rhsChecked;
            rhsChecked = that.isChecked();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "checked", lhsChecked), LocatorUtils.property(thatLocator, "checked", rhsChecked), lhsChecked, rhsChecked)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof MVCViewCheckBoxListExpression) {
            final MVCViewCheckBoxListExpression copy = ((MVCViewCheckBoxListExpression) draftCopy);
            if (this.containerElementId!= null) {
                String sourceContainerElementId;
                sourceContainerElementId = this.getContainerElementId();
                String copyContainerElementId = ((String) strategy.copy(LocatorUtils.property(locator, "containerElementId", sourceContainerElementId), sourceContainerElementId));
                copy.setContainerElementId(copyContainerElementId);
            } else {
                copy.containerElementId = null;
            }
            if (this.checked!= null) {
                boolean sourceChecked;
                sourceChecked = this.isChecked();
                boolean copyChecked = strategy.copy(LocatorUtils.property(locator, "checked", sourceChecked), sourceChecked);
                copy.setChecked(copyChecked);
            } else {
                copy.checked = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MVCViewCheckBoxListExpression();
    }

}
