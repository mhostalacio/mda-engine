
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.mda.engine.core.ModelNodeBase;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for InvokeQueryActionParameterCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InvokeQueryActionParameterCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Parameter" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}InvokeQueryActionParameter" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvokeQueryActionParameterCollection", propOrder = {
    "parameter"
})
public class InvokeQueryActionParameterCollection
    extends ModelNodeBase
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Parameter", required = true)
    protected List<InvokeQueryActionParameter> parameter;

    /**
     * Gets the value of the parameter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the parameter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParameter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvokeQueryActionParameter }
     * 
     * 
     */
    public List<InvokeQueryActionParameter> getParameter() {
        if (parameter == null) {
            parameter = new ArrayList<InvokeQueryActionParameter>();
        }
        return this.parameter;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof InvokeQueryActionParameterCollection)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final InvokeQueryActionParameterCollection that = ((InvokeQueryActionParameterCollection) object);
        {
            List<InvokeQueryActionParameter> lhsParameter;
            lhsParameter = this.getParameter();
            List<InvokeQueryActionParameter> rhsParameter;
            rhsParameter = that.getParameter();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "parameter", lhsParameter), LocatorUtils.property(thatLocator, "parameter", rhsParameter), lhsParameter, rhsParameter)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof InvokeQueryActionParameterCollection) {
            final InvokeQueryActionParameterCollection copy = ((InvokeQueryActionParameterCollection) draftCopy);
            if ((this.parameter!= null)&&(!this.parameter.isEmpty())) {
                List<InvokeQueryActionParameter> sourceParameter;
                sourceParameter = this.getParameter();
                @SuppressWarnings("unchecked")
                List<InvokeQueryActionParameter> copyParameter = ((List<InvokeQueryActionParameter> ) strategy.copy(LocatorUtils.property(locator, "parameter", sourceParameter), sourceParameter));
                copy.parameter = null;
                List<InvokeQueryActionParameter> uniqueParameterl = copy.getParameter();
                uniqueParameterl.addAll(copyParameter);
            } else {
                copy.parameter = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new InvokeQueryActionParameterCollection();
    }

}
