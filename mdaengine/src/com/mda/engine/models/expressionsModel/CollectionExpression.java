
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for CollectionExpression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CollectionExpression">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}Expression">
 *       &lt;sequence>
 *         &lt;element name="Argument" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}SingleArgumentExpression"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CollectionExpression", propOrder = {
    "argument"
})
@XmlSeeAlso({
    LastOrDefaultCollectionExpression.class,
    GetCollectionItemOrDefaultExpression.class,
    GetCollectionItemExpression.class,
    LastCollectionExpression.class,
    IsEmptyCollectionExpression.class,
    FirstOrDefaultCollectionExpression.class,
    FirstCollectionExpression.class
})
public abstract class CollectionExpression
    extends Expression
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Argument", required = true)
    protected SingleArgumentExpression argument;

    /**
     * Gets the value of the argument property.
     * 
     * @return
     *     possible object is
     *     {@link SingleArgumentExpression }
     *     
     */
    public SingleArgumentExpression getArgument() {
        return argument;
    }

    /**
     * Sets the value of the argument property.
     * 
     * @param value
     *     allowed object is
     *     {@link SingleArgumentExpression }
     *     
     */
    public void setArgument(SingleArgumentExpression value) {
        this.argument = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof CollectionExpression)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final CollectionExpression that = ((CollectionExpression) object);
        {
            SingleArgumentExpression lhsArgument;
            lhsArgument = this.getArgument();
            SingleArgumentExpression rhsArgument;
            rhsArgument = that.getArgument();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "argument", lhsArgument), LocatorUtils.property(thatLocator, "argument", rhsArgument), lhsArgument, rhsArgument)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        if (null == target) {
            throw new IllegalArgumentException("Target argument must not be null for abstract copyable classes.");
        }
        super.copyTo(locator, target, strategy);
        if (target instanceof CollectionExpression) {
            final CollectionExpression copy = ((CollectionExpression) target);
            if (this.argument!= null) {
                SingleArgumentExpression sourceArgument;
                sourceArgument = this.getArgument();
                SingleArgumentExpression copyArgument = ((SingleArgumentExpression) strategy.copy(LocatorUtils.property(locator, "argument", sourceArgument), sourceArgument));
                copy.setArgument(copyArgument);
            } else {
                copy.argument = null;
            }
        }
        return target;
    }

}
