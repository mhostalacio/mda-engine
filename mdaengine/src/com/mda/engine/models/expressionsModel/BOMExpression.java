
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for BOMExpression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BOMExpression">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}Expression">
 *       &lt;sequence>
 *         &lt;element name="CasTo" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}CastExpression" minOccurs="0"/>
 *         &lt;element name="SpecificRules" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}SpecificRulesExpression" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Path" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="GetValue" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="IsNullable" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BOMExpression", propOrder = {
    "casTo",
    "specificRules"
})
@XmlSeeAlso({
    GetValueExpression.class,
    IsRequiredExpression.class,
    IsVisibleExpression.class,
    IsEnableExpression.class,
    IsNewExpression.class,
    IsDirtyExpression.class,
    CurrentBOMExpression.class,
    IsValidExpression.class
})
public class BOMExpression
    extends Expression
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "CasTo")
    protected CastExpression casTo;
    @XmlElement(name = "SpecificRules")
    protected SpecificRulesExpression specificRules;
    @XmlAttribute(name = "Path", required = true)
    protected String path;
    @XmlAttribute(name = "GetValue")
    protected Boolean getValue;
    @XmlAttribute(name = "IsNullable")
    protected Boolean isNullable;

    /**
     * Gets the value of the casTo property.
     * 
     * @return
     *     possible object is
     *     {@link CastExpression }
     *     
     */
    public CastExpression getCasTo() {
        return casTo;
    }

    /**
     * Sets the value of the casTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CastExpression }
     *     
     */
    public void setCasTo(CastExpression value) {
        this.casTo = value;
    }

    /**
     * Gets the value of the specificRules property.
     * 
     * @return
     *     possible object is
     *     {@link SpecificRulesExpression }
     *     
     */
    public SpecificRulesExpression getSpecificRules() {
        return specificRules;
    }

    /**
     * Sets the value of the specificRules property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpecificRulesExpression }
     *     
     */
    public void setSpecificRules(SpecificRulesExpression value) {
        this.specificRules = value;
    }

    /**
     * Gets the value of the path property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPath() {
        return path;
    }

    /**
     * Sets the value of the path property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPath(String value) {
        this.path = value;
    }

    /**
     * Gets the value of the getValue property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isGetValue() {
        if (getValue == null) {
            return false;
        } else {
            return getValue;
        }
    }

    /**
     * Sets the value of the getValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGetValue(Boolean value) {
        this.getValue = value;
    }

    /**
     * Gets the value of the isNullable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isIsNullable() {
        if (isNullable == null) {
            return false;
        } else {
            return isNullable;
        }
    }

    /**
     * Sets the value of the isNullable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsNullable(Boolean value) {
        this.isNullable = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof BOMExpression)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final BOMExpression that = ((BOMExpression) object);
        {
            CastExpression lhsCasTo;
            lhsCasTo = this.getCasTo();
            CastExpression rhsCasTo;
            rhsCasTo = that.getCasTo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "casTo", lhsCasTo), LocatorUtils.property(thatLocator, "casTo", rhsCasTo), lhsCasTo, rhsCasTo)) {
                return false;
            }
        }
        {
            SpecificRulesExpression lhsSpecificRules;
            lhsSpecificRules = this.getSpecificRules();
            SpecificRulesExpression rhsSpecificRules;
            rhsSpecificRules = that.getSpecificRules();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "specificRules", lhsSpecificRules), LocatorUtils.property(thatLocator, "specificRules", rhsSpecificRules), lhsSpecificRules, rhsSpecificRules)) {
                return false;
            }
        }
        {
            String lhsPath;
            lhsPath = this.getPath();
            String rhsPath;
            rhsPath = that.getPath();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "path", lhsPath), LocatorUtils.property(thatLocator, "path", rhsPath), lhsPath, rhsPath)) {
                return false;
            }
        }
        {
            boolean lhsGetValue;
            lhsGetValue = this.isGetValue();
            boolean rhsGetValue;
            rhsGetValue = that.isGetValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "getValue", lhsGetValue), LocatorUtils.property(thatLocator, "getValue", rhsGetValue), lhsGetValue, rhsGetValue)) {
                return false;
            }
        }
        {
            boolean lhsIsNullable;
            lhsIsNullable = this.isIsNullable();
            boolean rhsIsNullable;
            rhsIsNullable = that.isIsNullable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "isNullable", lhsIsNullable), LocatorUtils.property(thatLocator, "isNullable", rhsIsNullable), lhsIsNullable, rhsIsNullable)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof BOMExpression) {
            final BOMExpression copy = ((BOMExpression) draftCopy);
            if (this.casTo!= null) {
                CastExpression sourceCasTo;
                sourceCasTo = this.getCasTo();
                CastExpression copyCasTo = ((CastExpression) strategy.copy(LocatorUtils.property(locator, "casTo", sourceCasTo), sourceCasTo));
                copy.setCasTo(copyCasTo);
            } else {
                copy.casTo = null;
            }
            if (this.specificRules!= null) {
                SpecificRulesExpression sourceSpecificRules;
                sourceSpecificRules = this.getSpecificRules();
                SpecificRulesExpression copySpecificRules = ((SpecificRulesExpression) strategy.copy(LocatorUtils.property(locator, "specificRules", sourceSpecificRules), sourceSpecificRules));
                copy.setSpecificRules(copySpecificRules);
            } else {
                copy.specificRules = null;
            }
            if (this.path!= null) {
                String sourcePath;
                sourcePath = this.getPath();
                String copyPath = ((String) strategy.copy(LocatorUtils.property(locator, "path", sourcePath), sourcePath));
                copy.setPath(copyPath);
            } else {
                copy.path = null;
            }
            if (this.getValue!= null) {
                boolean sourceGetValue;
                sourceGetValue = this.isGetValue();
                boolean copyGetValue = strategy.copy(LocatorUtils.property(locator, "getValue", sourceGetValue), sourceGetValue);
                copy.setGetValue(copyGetValue);
            } else {
                copy.getValue = null;
            }
            if (this.isNullable!= null) {
                boolean sourceIsNullable;
                sourceIsNullable = this.isIsNullable();
                boolean copyIsNullable = strategy.copy(LocatorUtils.property(locator, "isNullable", sourceIsNullable), sourceIsNullable);
                copy.setIsNullable(copyIsNullable);
            } else {
                copy.isNullable = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new BOMExpression();
    }

}
