
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for IsInStateExpression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IsInStateExpression">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}Expression">
 *       &lt;sequence>
 *         &lt;element name="Value" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ExpressionChoice"/>
 *         &lt;element name="States" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}IsInStateExpressionStateCollection"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IsInStateExpression", propOrder = {
    "value",
    "states"
})
public class IsInStateExpression
    extends Expression
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Value", required = true)
    protected ExpressionChoice value;
    @XmlElement(name = "States", required = true)
    protected IsInStateExpressionStateCollection states;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link ExpressionChoice }
     *     
     */
    public ExpressionChoice getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpressionChoice }
     *     
     */
    public void setValue(ExpressionChoice value) {
        this.value = value;
    }

    /**
     * Gets the value of the states property.
     * 
     * @return
     *     possible object is
     *     {@link IsInStateExpressionStateCollection }
     *     
     */
    public IsInStateExpressionStateCollection getStates() {
        return states;
    }

    /**
     * Sets the value of the states property.
     * 
     * @param value
     *     allowed object is
     *     {@link IsInStateExpressionStateCollection }
     *     
     */
    public void setStates(IsInStateExpressionStateCollection value) {
        this.states = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof IsInStateExpression)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final IsInStateExpression that = ((IsInStateExpression) object);
        {
            ExpressionChoice lhsValue;
            lhsValue = this.getValue();
            ExpressionChoice rhsValue;
            rhsValue = that.getValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "value", lhsValue), LocatorUtils.property(thatLocator, "value", rhsValue), lhsValue, rhsValue)) {
                return false;
            }
        }
        {
            IsInStateExpressionStateCollection lhsStates;
            lhsStates = this.getStates();
            IsInStateExpressionStateCollection rhsStates;
            rhsStates = that.getStates();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "states", lhsStates), LocatorUtils.property(thatLocator, "states", rhsStates), lhsStates, rhsStates)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof IsInStateExpression) {
            final IsInStateExpression copy = ((IsInStateExpression) draftCopy);
            if (this.value!= null) {
                ExpressionChoice sourceValue;
                sourceValue = this.getValue();
                ExpressionChoice copyValue = ((ExpressionChoice) strategy.copy(LocatorUtils.property(locator, "value", sourceValue), sourceValue));
                copy.setValue(copyValue);
            } else {
                copy.value = null;
            }
            if (this.states!= null) {
                IsInStateExpressionStateCollection sourceStates;
                sourceStates = this.getStates();
                IsInStateExpressionStateCollection copyStates = ((IsInStateExpressionStateCollection) strategy.copy(LocatorUtils.property(locator, "states", sourceStates), sourceStates));
                copy.setStates(copyStates);
            } else {
                copy.states = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new IsInStateExpression();
    }

}
