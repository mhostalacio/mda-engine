
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for AddItemsToListAction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddItemsToListAction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}Expression">
 *       &lt;choice>
 *         &lt;element name="GetLovList" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}AddItemsToListFromLov" minOccurs="0"/>
 *       &lt;/choice>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddItemsToListAction", propOrder = {
    "getLovList"
})
public class AddItemsToListAction
    extends Expression
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "GetLovList")
    protected AddItemsToListFromLov getLovList;

    /**
     * Gets the value of the getLovList property.
     * 
     * @return
     *     possible object is
     *     {@link AddItemsToListFromLov }
     *     
     */
    public AddItemsToListFromLov getGetLovList() {
        return getLovList;
    }

    /**
     * Sets the value of the getLovList property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddItemsToListFromLov }
     *     
     */
    public void setGetLovList(AddItemsToListFromLov value) {
        this.getLovList = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof AddItemsToListAction)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final AddItemsToListAction that = ((AddItemsToListAction) object);
        {
            AddItemsToListFromLov lhsGetLovList;
            lhsGetLovList = this.getGetLovList();
            AddItemsToListFromLov rhsGetLovList;
            rhsGetLovList = that.getGetLovList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "getLovList", lhsGetLovList), LocatorUtils.property(thatLocator, "getLovList", rhsGetLovList), lhsGetLovList, rhsGetLovList)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof AddItemsToListAction) {
            final AddItemsToListAction copy = ((AddItemsToListAction) draftCopy);
            if (this.getLovList!= null) {
                AddItemsToListFromLov sourceGetLovList;
                sourceGetLovList = this.getGetLovList();
                AddItemsToListFromLov copyGetLovList = ((AddItemsToListFromLov) strategy.copy(LocatorUtils.property(locator, "getLovList", sourceGetLovList), sourceGetLovList));
                copy.setGetLovList(copyGetLovList);
            } else {
                copy.getLovList = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new AddItemsToListAction();
    }

}
