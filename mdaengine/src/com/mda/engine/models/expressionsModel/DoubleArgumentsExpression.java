
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for DoubleArgumentsExpression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DoubleArgumentsExpression">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}Expression">
 *       &lt;sequence>
 *         &lt;element name="FirstArg" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ExpressionChoice"/>
 *         &lt;element name="SecondArg" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}ExpressionChoice"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DoubleArgumentsExpression", propOrder = {
    "firstArg",
    "secondArg"
})
@XmlSeeAlso({
    PowerExpression.class,
    GreaterThanExpression.class,
    DivExpression.class,
    LowerThanExpression.class,
    MinExpression.class,
    AddExpression.class,
    MaxExpression.class,
    NotEqualsExpression.class,
    LikeExpression.class,
    EqualsExpression.class,
    ContainsValueExpression.class,
    ModExpression.class,
    LowerOrEqualThanExpression.class,
    GreaterOrEqualThanExpression.class,
    MulExpression.class,
    MinusExpression.class
})
public class DoubleArgumentsExpression
    extends Expression
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "FirstArg", required = true)
    protected ExpressionChoice firstArg;
    @XmlElement(name = "SecondArg", required = true)
    protected ExpressionChoice secondArg;

    /**
     * Gets the value of the firstArg property.
     * 
     * @return
     *     possible object is
     *     {@link ExpressionChoice }
     *     
     */
    public ExpressionChoice getFirstArg() {
        return firstArg;
    }

    /**
     * Sets the value of the firstArg property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpressionChoice }
     *     
     */
    public void setFirstArg(ExpressionChoice value) {
        this.firstArg = value;
    }

    /**
     * Gets the value of the secondArg property.
     * 
     * @return
     *     possible object is
     *     {@link ExpressionChoice }
     *     
     */
    public ExpressionChoice getSecondArg() {
        return secondArg;
    }

    /**
     * Sets the value of the secondArg property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpressionChoice }
     *     
     */
    public void setSecondArg(ExpressionChoice value) {
        this.secondArg = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof DoubleArgumentsExpression)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final DoubleArgumentsExpression that = ((DoubleArgumentsExpression) object);
        {
            ExpressionChoice lhsFirstArg;
            lhsFirstArg = this.getFirstArg();
            ExpressionChoice rhsFirstArg;
            rhsFirstArg = that.getFirstArg();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "firstArg", lhsFirstArg), LocatorUtils.property(thatLocator, "firstArg", rhsFirstArg), lhsFirstArg, rhsFirstArg)) {
                return false;
            }
        }
        {
            ExpressionChoice lhsSecondArg;
            lhsSecondArg = this.getSecondArg();
            ExpressionChoice rhsSecondArg;
            rhsSecondArg = that.getSecondArg();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "secondArg", lhsSecondArg), LocatorUtils.property(thatLocator, "secondArg", rhsSecondArg), lhsSecondArg, rhsSecondArg)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof DoubleArgumentsExpression) {
            final DoubleArgumentsExpression copy = ((DoubleArgumentsExpression) draftCopy);
            if (this.firstArg!= null) {
                ExpressionChoice sourceFirstArg;
                sourceFirstArg = this.getFirstArg();
                ExpressionChoice copyFirstArg = ((ExpressionChoice) strategy.copy(LocatorUtils.property(locator, "firstArg", sourceFirstArg), sourceFirstArg));
                copy.setFirstArg(copyFirstArg);
            } else {
                copy.firstArg = null;
            }
            if (this.secondArg!= null) {
                ExpressionChoice sourceSecondArg;
                sourceSecondArg = this.getSecondArg();
                ExpressionChoice copySecondArg = ((ExpressionChoice) strategy.copy(LocatorUtils.property(locator, "secondArg", sourceSecondArg), sourceSecondArg));
                copy.setSecondArg(copySecondArg);
            } else {
                copy.secondArg = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new DoubleArgumentsExpression();
    }

}
