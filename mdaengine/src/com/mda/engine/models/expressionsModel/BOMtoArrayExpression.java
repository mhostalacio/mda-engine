
package com.mda.engine.models.expressionsModel;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for BOMtoArrayExpression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BOMtoArrayExpression">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.mdaengine.com/mdaengine/models/expressionsModel}Expression">
 *       &lt;sequence>
 *         &lt;element name="BOM" type="{http://www.mdaengine.com/mdaengine/models/expressionsModel}BOMExpression"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BOMtoArrayExpression", propOrder = {
    "bom"
})
public class BOMtoArrayExpression
    extends Expression
    implements Serializable, Cloneable, CopyTo, Equals
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "BOM", required = true)
    protected BOMExpression bom;

    /**
     * Gets the value of the bom property.
     * 
     * @return
     *     possible object is
     *     {@link BOMExpression }
     *     
     */
    public BOMExpression getBOM() {
        return bom;
    }

    /**
     * Sets the value of the bom property.
     * 
     * @param value
     *     allowed object is
     *     {@link BOMExpression }
     *     
     */
    public void setBOM(BOMExpression value) {
        this.bom = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof BOMtoArrayExpression)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final BOMtoArrayExpression that = ((BOMtoArrayExpression) object);
        {
            BOMExpression lhsBOM;
            lhsBOM = this.getBOM();
            BOMExpression rhsBOM;
            rhsBOM = that.getBOM();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "bom", lhsBOM), LocatorUtils.property(thatLocator, "bom", rhsBOM), lhsBOM, rhsBOM)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof BOMtoArrayExpression) {
            final BOMtoArrayExpression copy = ((BOMtoArrayExpression) draftCopy);
            if (this.bom!= null) {
                BOMExpression sourceBOM;
                sourceBOM = this.getBOM();
                BOMExpression copyBOM = ((BOMExpression) strategy.copy(LocatorUtils.property(locator, "bom", sourceBOM), sourceBOM));
                copy.setBOM(copyBOM);
            } else {
                copy.bom = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new BOMtoArrayExpression();
    }

}
