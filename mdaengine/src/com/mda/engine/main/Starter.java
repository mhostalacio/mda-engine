package com.mda.engine.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.jvnet.jaxb2_commons.lang.StringUtils;
import org.xml.sax.SAXException;

import com.mda.engine.core.ApplicationScope;
import com.mda.engine.core.ConfigurationKey;
import com.mda.engine.core.MdaLogger;
import com.mda.engine.core.Product;
import com.mda.engine.model2model.BusinessModelAdapter;
import com.mda.engine.model2model.BusinessTransactionAdapter;
import com.mda.engine.model2model.DataModelAdapter;
import com.mda.engine.model2model.MVCModelAdapter;
import com.mda.engine.model2model.QueryModelAdapter;
import com.mda.engine.model2text.CUDQueriesTransformer;
import com.mda.engine.model2text.CommonSQLTransformer;
import com.mda.engine.model2text.DataContractTransformer;
import com.mda.engine.model2text.DataModuleTransformer;
import com.mda.engine.model2text.DataTableTransformer;
import com.mda.engine.model2text.DatabaseConstraintsTransformer;
import com.mda.engine.model2text.EntitiesConverterTransformer;
import com.mda.engine.model2text.EntityTransformer;
import com.mda.engine.model2text.EnumDataContractTransformer;
import com.mda.engine.model2text.EnumTransformer;
import com.mda.engine.model2text.InterfaceTransformer;
import com.mda.engine.model2text.MVCControllerTransformer;
import com.mda.engine.model2text.MVCModelTransformer;
import com.mda.engine.model2text.MVCViewTransformer;
import com.mda.engine.model2text.ProjectFileTransformer;
import com.mda.engine.model2text.QueryTransformer;
import com.mda.engine.model2text.RepositoryInterfaceTransformer;
import com.mda.engine.model2text.RepositoryTransformer;
import com.mda.engine.model2text.RequestMessageTransformer;
import com.mda.engine.model2text.ResponseMessageTransformer;
import com.mda.engine.model2text.ServiceClientTransformer;
import com.mda.engine.model2text.ServiceHostTransformer;
import com.mda.engine.model2text.ServiceImplementationTransformer;
import com.mda.engine.model2text.ServiceInterfaceTransformer;
import com.mda.engine.model2text.SpringConfigTransformer;
import com.mda.engine.model2text.TransactionsConverterTransformer;
import com.mda.engine.model2text.TransactionsMappingTransformer;
import com.mda.engine.model2text.TransactionsTransformer;
import com.mda.engine.model2text.WCFImplementationTransformer;
import com.mda.engine.model2text.WebTransformer;
import com.mda.engine.models.businessObjectModel.InterfaceType;
import com.mda.engine.models.businessObjectModel.LovDefinitionType;
import com.mda.engine.models.businessObjectModel.ObjectType;
import com.mda.engine.models.businessTransactionModel.BusinessTransaction;
import com.mda.engine.models.configurationManagementModel.Application;
import com.mda.engine.models.configurationManagementModel.ClassLibraryProject;
import com.mda.engine.models.configurationManagementModel.ConsoleApplicationProject;
import com.mda.engine.models.configurationManagementModel.EntitiesConverterProject;
import com.mda.engine.models.configurationManagementModel.EntitiesProject;
import com.mda.engine.models.configurationManagementModel.ModelClassLibraryProject;
import com.mda.engine.models.configurationManagementModel.Project;
import com.mda.engine.models.configurationManagementModel.RepositoryProject;
import com.mda.engine.models.configurationManagementModel.ServiceContractsClassLibraryProject;
import com.mda.engine.models.configurationManagementModel.ServiceImplementationProject;
import com.mda.engine.models.configurationManagementModel.ServiceInterfaceProject;
import com.mda.engine.models.configurationManagementModel.TransactionsConverterProject;
import com.mda.engine.models.configurationManagementModel.TransactionsProject;
import com.mda.engine.models.configurationManagementModel.ViewControllerClassLibraryProject;
import com.mda.engine.models.configurationManagementModel.WCFImplementationProject;
import com.mda.engine.models.configurationManagementModel.WebApplicationProject;
import com.mda.engine.models.configurationManagementModel.WebServiceHostProject;
import com.mda.engine.models.configurationManagementModel.WindowsServiceProject;
import com.mda.engine.models.dataModel.DataTable;
import com.mda.engine.models.mvc.MVCInstance;
import com.mda.engine.models.queryModel.Query;
import com.mda.engine.models.rulesModel.ObjectValidationScope;
import com.mda.engine.uml.GraphBuilderOptions;
import com.mda.engine.uml.UMLGraphBuilder;
import com.mda.engine.utils.IOHelper;
import com.mda.engine.utils.SerializationHelper;


public class Starter extends MdaLogger {
	
	
	private static Product product = new Product("MDAStudio");
	
	public static void main(String[] args) throws Exception {
		
		//BasicConfigurator.configure();
		
		parseArguments(args);
		loadProperties();
		loadApplications();
		loadBusinessModel();
		adaptBusinessModel();
		generateCode();
		//generateUML();
		System.out.println("Finished!");
	}

	public static void ResetProduct()
	{
		product = new Product("MDAStudio");
	}

	public static Product getProduct()
	{
		return product;
	}
	
	/**
	 * Parses all arguments passed through command line
	 * 
	 * @param args
	 *            Arguments
	 */
	public static void parseArguments(String[] args) {
		for (int i = 0; i < args.length; i++) {

			String arg = args[i].toLowerCase();
			
			if (arg.startsWith("appfolder=")) {
				product.addProperty(ConfigurationKey.ApplicationsFolder, arg.substring("appfolder=".length() ));
			}
			else if (arg.startsWith("propfolder=")) {
				product.addProperty(ConfigurationKey.PropertiesFolder, arg.substring("propfolder=".length()));
			}
		}
		
	}
	
	/**
	 * Loads all properties defined in .properties file. Each one must match the enum ConfigurationKey
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 */
	public static void loadProperties() throws ParserConfigurationException, SAXException, IOException {
		
		System.out.println("loading properties...");
		org.w3c.dom.Document doc = SerializationHelper.disserializeXML(product.getProperty(ConfigurationKey.PropertiesFolder) + "\\default.properties");
		org.w3c.dom.Element rootElement=doc.getDocumentElement();
		org.w3c.dom.NodeList nodeList = rootElement.getElementsByTagName("property");
		for(int i = 0; i < nodeList.getLength(); i++)
		{
			org.w3c.dom.Node node = nodeList.item(i);
			try
			{
				ConfigurationKey key = ConfigurationKey.valueOf(node.getAttributes().getNamedItem("name").getTextContent());
				product.addProperty(key, node.getAttributes().getNamedItem("value").getTextContent());
			}catch (Exception ex)
			{
				log(Starter.class, Level.WARN, "Configuration key not mapped to ConfigurationKey enum", ex);
			}
		}
	}
	
	/**
	 * Loads all applications (.cfg files)  
	 * @throws Exception 
	 */
	public static void loadApplications() throws Exception {
		
		System.out.println("loading applications...");
		File[] cfgs = IOHelper.listFiles(new File(product.getProperty(ConfigurationKey.ApplicationsFolder)), new  java.io.FilenameFilter() {
			public boolean accept(File dir, String n) {
				return n.endsWith(".cfg");
			}
		}, true);
		
		for(File f : cfgs)
		{
			Application app = SerializationHelper.deserializeApplication(f.getPath());
			Project project = app.getEntitiesProject();
			if (project != null)
			{
				app.setEntitiesProjectNamespace(project.getNamespace());
			}
			project = app.getRepositoryProject();
			if (project != null)
			{
				app.setRepositoryProjectNamespace(project.getNamespace());
			}
			project = app.getTransactionsProject();
			if (project != null)
			{
				app.setTransactionsProjectNamespace(project.getNamespace());
			}
			
			
			for(Project proj : app.getProjects().getProject())
			{
				if (proj.getArtifacts() != null && proj.getArtifacts().getTransactionsMapping() != null)
				{
					if (!StringUtils.isEmpty(proj.getArtifacts().getTransactionsMapping().getGenerateClassName()))
					{
						app.setHasTransactionsMapping(true);
						app.setTransactionsMappingClassName(proj.getNamespace() + "." + proj.getArtifacts().getTransactionsMapping().getGenerateClassName());
					}
				}
			}
			product.addApplication(app);
		}
	}
	
	/**
	 * Loads all BOM's, Rules, Queries and BTM's 
	 * @throws JAXBException 
	 * @throws FileNotFoundException 
	 */
	public static void loadBusinessModel() throws FileNotFoundException, JAXBException {

		System.out.println("loading business model...");
		String pathSeparator = File.separator;
		
		for (Application app : product.getApplications())
		{
			if (app.isActive())
			{
				//Parse BOM's
				File[] cfgs = IOHelper.listFiles(new File(product.getProperty(ConfigurationKey.ApplicationsFolder) + "\\" + app.getRelativeFolderPath()), new  java.io.FilenameFilter() {
					public boolean accept(File dir, String n) {
						return n.endsWith(".bom");
					}
				}, true);
				
				for(File f : cfgs)
				{
					String[] paths = f.getPath().split(Pattern.quote(pathSeparator), 0);
					String packageName = paths[paths.length-2];
					ObjectType bom = SerializationHelper.deserializeBusinessObject(f.getPath());
					bom.setPackageName(packageName);
					product.addObject(app, bom);
				}
				
				//Parse Queries
				cfgs = IOHelper.listFiles(new File(product.getProperty(ConfigurationKey.ApplicationsFolder) + "\\" + app.getRelativeFolderPath()), new  java.io.FilenameFilter() {
					public boolean accept(File dir, String n) {
						return n.endsWith(".qry");
					}
				}, true);
				
				for(File f : cfgs)
				{
					Query query = SerializationHelper.deserializeQuery(f.getPath());
					product.addQuery(app, query);
				}
				
				//Parse Rules
				cfgs = IOHelper.listFiles(new File(product.getProperty(ConfigurationKey.ApplicationsFolder) + "\\" + app.getRelativeFolderPath()), new  java.io.FilenameFilter() {
					public boolean accept(File dir, String n) {
						return n.endsWith(".rules");
					}
				}, true);
				
				for(File f : cfgs)
				{
					ObjectValidationScope rules = SerializationHelper.deserializeRules(f.getPath());
					product.addRules(app, rules);
				}
				
				//Parse BTM's
				cfgs = IOHelper.listFiles(new File(product.getProperty(ConfigurationKey.ApplicationsFolder) + "\\" + app.getRelativeFolderPath()), new  java.io.FilenameFilter() {
					public boolean accept(File dir, String n) {
						return n.endsWith(".btm");
					}
				}, true);
				
				for(File f : cfgs)
				{
					String[] paths = f.getPath().split(Pattern.quote(pathSeparator), 0);
					BusinessTransaction btm = SerializationHelper.deserializeBusinessTransaction(f.getPath());
					String packageName = paths[paths.length-2];
					btm.setPackageName(packageName);
					product.addBTM(app, btm);
				}
				
				if (product.generateScope(ApplicationScope.MVCLayer))
				{
					//Parse MVC's
					cfgs = IOHelper.listFiles(new File(product.getProperty(ConfigurationKey.ApplicationsFolder) + "\\" + app.getRelativeFolderPath()), new  java.io.FilenameFilter() {
						public boolean accept(File dir, String n) {
							return n.endsWith(".mvc");
						}
					}, true);
					
					for(File f : cfgs)
					{
						String[] paths = f.getPath().split(Pattern.quote(pathSeparator), 0);
						String packageName = paths[paths.length-2];
						MVCInstance mvc = SerializationHelper.deserializeMVC(f.getPath());
						mvc.setPackageName(packageName);
						product.addMVC(app, mvc);
					}
				}
			}
		}
	}
	
	/**
	 * Adapts business model 
	 * @throws Exception 
	 */
	public static void adaptBusinessModel() throws Exception {
		
		System.out.println("adapting business model...");
		BusinessModelAdapter m2m = new BusinessModelAdapter(product);
		m2m.adaptBusinessModel();
		
		DataModelAdapter m2d = new DataModelAdapter(product);
		m2d.adaptDataModel();
		
		QueryModelAdapter m2q = new QueryModelAdapter(product);
		m2q.adaptDataModel();
		
		if (product.generateScope(ApplicationScope.MVCLayer))
		{
			MVCModelAdapter m2mvc = new MVCModelAdapter(product);
			m2mvc.adaptMvcModel();
		}
		
		BusinessTransactionAdapter m2bt = new BusinessTransactionAdapter(product);
		m2bt.adaptBTMModel();
	}
	
	/**
	 * Generate code according to parameters 
	 * @throws Exception 
	 */
	public static void generateCode() throws Exception {

		System.out.println("generating code...");
		if (product.generateScope(ApplicationScope.DataLayer))
		{
			generateDataLayer();
		}
		if (product.generateScope(ApplicationScope.EntitiesLayer))
		{
			generateEntitiesLayer();
		}
		if (product.generateScope(ApplicationScope.ServicesLayer))
		{
			generateServicesLayer();
		}
		if (product.generateScope(ApplicationScope.MVCLayer))
		{
			generateMVCLayer();
		}
		generateOtherProjects();
	}

	private static void generateMVCLayer() throws Exception {
		
		for(Application app : product.getApplications())
		{
			ViewControllerClassLibraryProject viewCtrlProj = app.getViewControllerProject();
			ModelClassLibraryProject modelProj = app.getModelsProject();
			WebApplicationProject webProj = app.getWebProject();
			
			if (viewCtrlProj != null && modelProj != null)
			{
				String directoryPatternController = product.getProperty(ConfigurationKey.ApplicationsFolder) + "\\" + app.getRelativeFolderPath() + "\\" + viewCtrlProj.getRelativePath() + "\\";
				String directoryPatternModel = product.getProperty(ConfigurationKey.ApplicationsFolder) + "\\" + app.getRelativeFolderPath() + "\\" + modelProj.getRelativePath() + "\\";
				String directoryPatternWeb = null;
				if (webProj != null)
				{
					directoryPatternWeb = product.getProperty(ConfigurationKey.ApplicationsFolder) + "\\" + app.getRelativeFolderPath() + "\\" + webProj.getRelativePath() + "\\";
				}
				for(MVCInstance mvc : product.getApplicationMvcs(app))
				{
					File dir = new File(directoryPatternController + mvc.getPackageName() + "\\(gen)\\");
					if (!dir.exists())
						dir.mkdirs();
					
					mvc.setModelNamespace(modelProj.getNamespace() +  "." + mvc.getPackageName());
					mvc.setViewControllerNamespace(viewCtrlProj.getNamespace() +  "." + mvc.getPackageName());
					
					//Controller
					File f = new File(dir.getPath() + "\\" + mvc.getName() + "Controller" + product.getProperty(ConfigurationKey.GeneratedFilesExtension));
					MVCControllerTransformer m2t = new MVCControllerTransformer();
					m2t.setMVC(mvc);
					m2t.setNamespace(mvc.getViewControllerNamespace());
					m2t.generateCode(f);
					
					//View
					f = new File(dir.getPath() + "\\" + mvc.getName() + "View" + product.getProperty(ConfigurationKey.GeneratedFilesExtension));
					MVCViewTransformer m2view = new MVCViewTransformer();
					m2view.setMVC(mvc);
					m2view.setNamespace(mvc.getViewControllerNamespace());
					m2view.generateCode(f);
					
					dir = new File(directoryPatternModel + mvc.getPackageName() + "\\(gen)\\");
					if (!dir.exists())
						dir.mkdirs();
					
					//Model
					f = new File(dir.getPath() + "\\" + mvc.getName() + "Model" + product.getProperty(ConfigurationKey.GeneratedFilesExtension));
					MVCModelTransformer model2t = new MVCModelTransformer();
					model2t.setMVC(mvc);
					model2t.setNamespace(mvc.getModelNamespace());
					model2t.generateCode(f);
					
					//Web
					if (directoryPatternWeb != null)
					{
						dir = new File(directoryPatternWeb + "Areas\\" + mvc.getPackageName() + "\\Views\\" + mvc.getName() + "\\");
						if (!dir.exists())
							dir.mkdirs();
						
						f = new File(dir.getPath() + "\\" + mvc.getName() + "View" + product.getProperty(ConfigurationKey.WebFilesExtension));
						if (!f.exists() || mvc.isGenerateHTML())
						{
							WebTransformer model2Web = new WebTransformer();
							model2Web.setMVC(mvc);
							model2Web.setNamespace(webProj.getNamespace());
							model2Web.generateCode(f);
						}
					}
				}
				
				//Spring configuration
				if (webProj != null)
				{
					File dir = new File(directoryPatternWeb + "Environment\\(gen)\\");
					if (!dir.exists())
						dir.mkdirs();
					
					File f = new File(dir.getPath() + "\\spring.config");
					SpringConfigTransformer m2t = new SpringConfigTransformer();
					m2t.setProduct(product);
					m2t.setApplicationScope(ApplicationScope.MVCLayer);
					m2t.generateCode(f);
				}
				
				//generate project files
				File csProjFile = new File(directoryPatternController + viewCtrlProj.getAssemblyName() + ".gen.csproj");
				if (!csProjFile.exists())
				{
					ProjectFileTransformer csProj2Text = new ProjectFileTransformer();
					csProj2Text.setProject(viewCtrlProj);
					csProj2Text.generateCode(csProjFile);
				}
				
				
				csProjFile = new File(directoryPatternModel + modelProj.getAssemblyName() + ".gen.csproj");
				if (!csProjFile.exists())
				{
					ProjectFileTransformer csProj2Text = new ProjectFileTransformer();
					csProj2Text.setProject(modelProj);
					csProj2Text.generateCode(csProjFile);
				}
				
				if (webProj != null)
				{
					csProjFile = new File(directoryPatternWeb + webProj.getAssemblyName() + ".gen.csproj");
					if (!csProjFile.exists())
					{
						ProjectFileTransformer csProj2Text = new ProjectFileTransformer();
						csProj2Text.setProject(webProj);
						csProj2Text.generateCode(csProjFile);
					}
				}
			}
			

		}
		
	}

	private static void generateServicesLayer() throws Exception {
		for(Application app : product.getApplications())
		{
			ServiceContractsClassLibraryProject contractsProj = app.getServiceContractsProject();
			ServiceInterfaceProject serviceInterfaceProj = app.getServiceInterfaceProject();
			EntitiesConverterProject entitiesConverter = app.getEntitiesConverterProject();
			TransactionsConverterProject transactionsConverter = app.getTransactionsConverterProject();
			ServiceImplementationProject serviceImplProj = app.getServiceImplementationProject();
			WebServiceHostProject serviceHostProj = app.getServiceHostProject();
			WCFImplementationProject wcfProj = app.getWCFImplementationProject();
			
			if (contractsProj != null)
			{
				app.setServiceContractsProjectNamespace(contractsProj.getNamespace());
				String directoryPattern = product.getProperty(ConfigurationKey.ApplicationsFolder) + "\\" + app.getRelativeFolderPath() + "\\" + contractsProj.getRelativePath() + "\\";
				
				//Data Contracts classes
				for(ObjectType bom : product.getApplicationBOMs(app))
				{
					if (!bom.getIsAutoCreated())
					{
						File dir = new File(directoryPattern + bom.getPackageName() + "\\(gen)\\DataContract\\");
						if (!dir.exists())
							dir.mkdirs();
						File f = new File(dir.getPath() + "\\" + bom.getName() + "DataContract" + product.getProperty(ConfigurationKey.GeneratedFilesExtension));
						
						//DataContract class
						DataContractTransformer m2t = new DataContractTransformer();
						m2t.setBom(bom);
						m2t.setNamespace(contractsProj.getNamespace() +  "." + bom.getPackageName());
						m2t.generateCode(f);
					

						if (bom.getLovs() != null && bom.getLovs().getLovDefinition() != null)
						{
							for(LovDefinitionType lov : bom.getLovs().getLovDefinition())
							{
								f = new File(dir.getPath() + "\\" + lov.getName() + "DataContract"  + product.getProperty(ConfigurationKey.GeneratedFilesExtension));
								EnumDataContractTransformer enumM2t = new EnumDataContractTransformer();
								enumM2t.setLov(lov);
								enumM2t.setNamespace(contractsProj.getNamespace() +  "." + bom.getPackageName());
								enumM2t.generateCode(f);
							}
						}
					}
				}
				
				//Transactions classes
				for(BusinessTransaction btm : product.getApplicationBTMs(app))
				{
					File dir = new File(directoryPattern + btm.getPackageName() + "\\(gen)\\RequestMessage\\");
					if (!dir.exists())
						dir.mkdirs();
					File f = new File(dir.getPath() + "\\" + btm.getName() + "RequestMessage" + product.getProperty(ConfigurationKey.GeneratedFilesExtension));
					
					//RequestMessage class
					RequestMessageTransformer m2treq = new RequestMessageTransformer();
					m2treq.setBtm(btm);
					m2treq.setNamespace(contractsProj.getNamespace() +  "." + btm.getPackageName());
					m2treq.generateCode(f);
					
					dir = new File(directoryPattern + btm.getPackageName() + "\\(gen)\\ResponseMessage\\");
					if (!dir.exists())
						dir.mkdirs();
					f = new File(dir.getPath() + "\\" + btm.getName() + "ResponseMessage" + product.getProperty(ConfigurationKey.GeneratedFilesExtension));
					
					//ResponseMessage class
					ResponseMessageTransformer m2tresp = new ResponseMessageTransformer();
					m2tresp.setBtm(btm);
					m2tresp.setNamespace(contractsProj.getNamespace() +  "." + btm.getPackageName());
					m2tresp.generateCode(f);
				}

				
				//generate project file
				File csProjFile = new File(directoryPattern + contractsProj.getAssemblyName() + ".gen.csproj");
				if (!csProjFile.exists())
				{
					ProjectFileTransformer csProj2Text = new ProjectFileTransformer();
					csProj2Text.setProject(contractsProj);
					csProj2Text.generateCode(csProjFile);
				}
			}
			
			if (serviceInterfaceProj != null)
			{
				app.setServiceInterfaceProjectNamespace(serviceInterfaceProj.getNamespace());
				String directoryPattern = product.getProperty(ConfigurationKey.ApplicationsFolder) + "\\" + app.getRelativeFolderPath() + "\\" + serviceInterfaceProj.getRelativePath() + "\\";
				
				for(ObjectType bom : product.getApplicationBOMs(app))
				{
					if (!bom.getIsAutoCreated() && bom.isPersistable())
					{
						File dir = new File(directoryPattern + bom.getPackageName() + "\\(gen)\\");
						if (!dir.exists())
							dir.mkdirs();
						File f = new File(dir.getPath() + "\\I" + bom.getName() + "Service" + product.getProperty(ConfigurationKey.GeneratedFilesExtension));
						
						//Service Interface
						ServiceInterfaceTransformer m2t = new ServiceInterfaceTransformer();
						m2t.setBom(bom);
						m2t.setNamespace(serviceInterfaceProj.getNamespace() +  "." + bom.getPackageName());
						m2t.generateCode(f);
					
					}
				}
				
				//generate project file
				File csProjFile = new File(directoryPattern + serviceInterfaceProj.getAssemblyName() + ".gen.csproj");
				if (!csProjFile.exists())
				{
					ProjectFileTransformer csProj2Text = new ProjectFileTransformer();
					csProj2Text.setProject(serviceInterfaceProj);
					csProj2Text.generateCode(csProjFile);
				}
			}
			
			if (entitiesConverter != null)
			{
				app.setEntitiesConverterProjectNamespace(entitiesConverter.getNamespace());
				String directoryPattern = product.getProperty(ConfigurationKey.ApplicationsFolder) + "\\" + app.getRelativeFolderPath() + "\\" + entitiesConverter.getRelativePath() + "\\";
				
				for(ObjectType bom : product.getApplicationBOMs(app))
				{
					if (bom.getPackageName() != null && !bom.getIsAutoCreated())
					{
						File dir = new File(directoryPattern + bom.getPackageName() + "\\(gen)\\");
						if (!dir.exists())
							dir.mkdirs();
						File f = new File(dir.getPath() + "\\" + bom.getName() + "Converter" + product.getProperty(ConfigurationKey.GeneratedFilesExtension));
						
						//Entities Converter
						EntitiesConverterTransformer m2t = new EntitiesConverterTransformer();
						m2t.setBom(bom);
						m2t.setNamespace(entitiesConverter.getNamespace() +  "." + bom.getPackageName());
						m2t.generateCode(f);
					
					}
				}
				
				//generate project file
				File csProjFile = new File(directoryPattern + entitiesConverter.getAssemblyName() + ".gen.csproj");
				if (!csProjFile.exists())
				{
					ProjectFileTransformer csProj2Text = new ProjectFileTransformer();
					csProj2Text.setProject(contractsProj);
					csProj2Text.generateCode(csProjFile);
				}
			}
			
			if (transactionsConverter != null)
			{
				app.setTransactionsConverterProjectNamespace(transactionsConverter.getNamespace());
				String directoryPattern = product.getProperty(ConfigurationKey.ApplicationsFolder) + "\\" + app.getRelativeFolderPath() + "\\" + transactionsConverter.getRelativePath() + "\\";
				
				for(BusinessTransaction btm : product.getApplicationBTMs(app))
				{
					File dir = new File(directoryPattern + btm.getPackageName() + "\\(gen)\\");
					if (!dir.exists())
						dir.mkdirs();
					File f = new File(dir.getPath() + "\\" + btm.getName() + "Converter" + product.getProperty(ConfigurationKey.GeneratedFilesExtension));
					
					//Transactions Converter
					TransactionsConverterTransformer m2t = new TransactionsConverterTransformer();
					m2t.setBtm(btm);
					m2t.setNamespace(transactionsConverter.getNamespace() +  "." + btm.getPackageName());
					m2t.generateCode(f);
				}
				
				//generate project file
				File csProjFile = new File(directoryPattern + transactionsConverter.getAssemblyName() + ".gen.csproj");
				if (!csProjFile.exists())
				{
					ProjectFileTransformer csProj2Text = new ProjectFileTransformer();
					csProj2Text.setProject(contractsProj);
					csProj2Text.generateCode(csProjFile);
				}
			}
			
			if (serviceImplProj != null)
			{
				app.setServiceImplementationProjectNamespace(serviceImplProj.getNamespace());
				String directoryPattern = product.getProperty(ConfigurationKey.ApplicationsFolder) + "\\" + app.getRelativeFolderPath() + "\\" + serviceImplProj.getRelativePath() + "\\";
				
				for(ObjectType bom : product.getApplicationBOMs(app))
				{
					if (!bom.getIsAutoCreated() && bom.isPersistable())
					{
						File dir = new File(directoryPattern + bom.getPackageName() + "\\(gen)\\");
						if (!dir.exists())
							dir.mkdirs();
						File f = new File(dir.getPath() + "\\" + bom.getName() + "Service" + product.getProperty(ConfigurationKey.GeneratedFilesExtension));
						
						//Service Interface
						ServiceImplementationTransformer m2t = new ServiceImplementationTransformer();
						m2t.setBom(bom);
						m2t.setNamespace(serviceImplProj.getNamespace() +  "." + bom.getPackageName());
						m2t.generateCode(f);
					
					}
				}
				
				//generate project file
				File csProjFile = new File(directoryPattern + serviceImplProj.getAssemblyName() + ".gen.csproj");
				if (!csProjFile.exists())
				{
					ProjectFileTransformer csProj2Text = new ProjectFileTransformer();
					csProj2Text.setProject(serviceImplProj);
					csProj2Text.generateCode(csProjFile);
				}
			}
			
			if (serviceHostProj != null)
			{
				app.setServiceHostProjectNamespace(serviceHostProj.getNamespace());
				String directoryPattern = product.getProperty(ConfigurationKey.ApplicationsFolder) + "\\" + app.getRelativeFolderPath() + "\\" + serviceHostProj.getRelativePath() + "\\";
				
				for(String pack : product.getApplicationPackages(app))
				{
					File dir = new File(directoryPattern + pack + "\\");
					if (!dir.exists())
						dir.mkdirs();
					File f = new File(dir.getPath() + "\\" + pack + "Service.svc");
					
					//Service Host class
					ServiceHostTransformer m2tHost = new ServiceHostTransformer();
					m2tHost.setPackageName(pack);
					m2tHost.setNamespace(serviceHostProj.getNamespace() +  "." + pack);
					m2tHost.setApplication(app);
					m2tHost.setProduct(product);
					m2tHost.generateCode(f);
					
					dir = new File(directoryPattern + pack + "\\(gen)\\");
					if (!dir.exists())
						dir.mkdirs();
					f = new File(dir.getPath() + "\\" + pack + "ServiceClient" + product.getProperty(ConfigurationKey.GeneratedFilesExtension));
					
					//Service Client class
					ServiceClientTransformer m2t = new ServiceClientTransformer();
					m2t.setPackageName(pack);
					m2t.setNamespace(serviceHostProj.getNamespace() +  "." + pack);
					m2t.setApplication(app);
					m2t.setProduct(product);
					m2t.generateCode(f);
					
				}

				//Spring configuration
				File dir = new File(directoryPattern + "Environment\\(gen)\\");
				if (!dir.exists())
					dir.mkdirs();
				
				File f = new File(dir.getPath() + "\\spring.config");
				SpringConfigTransformer m2t = new SpringConfigTransformer();
				m2t.setProduct(product);
				m2t.setApplicationScope(ApplicationScope.ServicesLayer);
				m2t.generateCode(f);
				
				//generate project file
				File csProjFile = new File(directoryPattern + serviceHostProj.getAssemblyName() + ".gen.csproj");
				if (!csProjFile.exists())
				{
					ProjectFileTransformer csProj2Text = new ProjectFileTransformer();
					csProj2Text.setProject(serviceHostProj);
					csProj2Text.generateCode(csProjFile);
				}
			}
			
			if (wcfProj != null)
			{
				app.setWcfImplementationProjectNamespace(wcfProj.getNamespace());
				String directoryPattern = product.getProperty(ConfigurationKey.ApplicationsFolder) + "\\" + app.getRelativeFolderPath() + "\\" + wcfProj.getRelativePath() + "\\";
				
				for(ObjectType bom : product.getApplicationBOMs(app))
				{
					if (!bom.getIsAutoCreated() && bom.isPersistable())
					{
						File dir = new File(directoryPattern + bom.getPackageName() + "\\(gen)\\");
						if (!dir.exists())
							dir.mkdirs();
						File f = new File(dir.getPath() + "\\" + bom.getName() + "WCFService" + product.getProperty(ConfigurationKey.GeneratedFilesExtension));
						
						//Service Interface
						WCFImplementationTransformer m2t = new WCFImplementationTransformer();
						m2t.setBom(bom);
						m2t.setNamespace(wcfProj.getNamespace() +  "." + bom.getPackageName());
						m2t.generateCode(f);
					
					}
				}
				
				//generate project file
				File csProjFile = new File(directoryPattern + wcfProj.getAssemblyName() + ".gen.csproj");
				if (!csProjFile.exists())
				{
					ProjectFileTransformer csProj2Text = new ProjectFileTransformer();
					csProj2Text.setProject(wcfProj);
					csProj2Text.generateCode(csProjFile);
				}
			}
		}
		
	}

	private static void generateEntitiesLayer() throws Exception {
		
		for(Application app : product.getApplications())
		{
			EntitiesProject entitiesProj = app.getEntitiesProject();
			RepositoryProject repositoryProj = app.getRepositoryProject();
			TransactionsProject transactionsProj = app.getTransactionsProject();
			
			if (entitiesProj != null)
			{
				app.setEntitiesProjectNamespace(entitiesProj.getNamespace());
				String directoryPattern = product.getProperty(ConfigurationKey.ApplicationsFolder) + "\\" + app.getRelativeFolderPath() + "\\" + entitiesProj.getRelativePath() + "\\";
				for(InterfaceType interf : product.getApplicationInterfaces(app))
				{
					File dir = new File(directoryPattern + interf.getPackageName() + "\\(gen)\\");
					if (!dir.exists())
						dir.mkdirs();
					File f = new File(dir.getPath() + "\\" + interf.getName() + product.getProperty(ConfigurationKey.GeneratedFilesExtension));
					InterfaceTransformer m2t = new InterfaceTransformer();
					m2t.setBom(interf);
					m2t.setNamespace(entitiesProj.getNamespace() +  "." + interf.getPackageName());
					m2t.generateCode(f);
					
					if (interf.getLovs() != null && interf.getLovs().getLovDefinition() != null)
					{
						for(LovDefinitionType lov : interf.getLovs().getLovDefinition())
						{
							f = new File(dir.getPath() + "\\" + lov.getName() + product.getProperty(ConfigurationKey.GeneratedFilesExtension));
							EnumTransformer enumM2t = new EnumTransformer();
							enumM2t.setLov(lov);
							enumM2t.setNamespace(entitiesProj.getNamespace() +  "." + interf.getPackageName());
							enumM2t.generateCode(f);
						}
					}
				}
				
				for(ObjectType bom : product.getApplicationBOMs(app))
				{
					if (!bom.getIsAutoCreated())
					{
						File dir = new File(directoryPattern + bom.getPackageName() + "\\(gen)\\");
						if (!dir.exists())
							dir.mkdirs();
						File f = new File(dir.getPath() + "\\" + bom.getName() + product.getProperty(ConfigurationKey.GeneratedFilesExtension));
						
						//Entity class
						EntityTransformer m2t = new EntityTransformer();
						m2t.setBom(bom);
						m2t.setNamespace(entitiesProj.getNamespace() +  "." + bom.getPackageName());
						m2t.generateCode(f);
					
						//Repository Interface
						if (bom.isPersistable())
						{
							f = new File(dir.getPath() + "\\I" + bom.getName() + "Repository" + product.getProperty(ConfigurationKey.GeneratedFilesExtension));
							RepositoryInterfaceTransformer m2tRep = new RepositoryInterfaceTransformer();
							m2tRep.setBom(bom);
							m2tRep.setNamespace(entitiesProj.getNamespace() +  "." + bom.getPackageName());
							m2tRep.generateCode(f);	
						}
						
						if (bom.getLovs() != null && bom.getLovs().getLovDefinition() != null)
						{
							for(LovDefinitionType lov : bom.getLovs().getLovDefinition())
							{
								f = new File(dir.getPath() + "\\" + lov.getName() + product.getProperty(ConfigurationKey.GeneratedFilesExtension));
								EnumTransformer enumM2t = new EnumTransformer();
								enumM2t.setLov(lov);
								enumM2t.setNamespace(entitiesProj.getNamespace() +  "." + bom.getPackageName());
								enumM2t.generateCode(f);
							}
						}
					}
				}
				
				//generate project file
				File csProjFile = new File(directoryPattern + entitiesProj.getAssemblyName() + ".gen.csproj");
				if (!csProjFile.exists())
				{
					ProjectFileTransformer csProj2Text = new ProjectFileTransformer();
					csProj2Text.setProject(entitiesProj);
					csProj2Text.generateCode(csProjFile);
				}
			}
			
			if (repositoryProj != null)
			{
				app.setRepositoryProjectNamespace(repositoryProj.getNamespace());
				String directoryPattern = product.getProperty(ConfigurationKey.ApplicationsFolder) + "\\" + app.getRelativeFolderPath() + "\\" + repositoryProj.getRelativePath() + "\\";
				
				
				for(ObjectType bom : product.getApplicationBOMs(app))
				{
					if (!bom.getIsAutoCreated())
					{
						File dir = new File(directoryPattern + bom.getPackageName() + "\\(gen)\\");
						if (!dir.exists())
							dir.mkdirs();
						File f = new File(dir.getPath() + "\\" + bom.getName() + product.getProperty(ConfigurationKey.GeneratedFilesExtension));
						
						//Repository class
						if (bom.isPersistable())
						{
							f = new File(dir.getPath() + "\\" + bom.getName() + "Repository" + product.getProperty(ConfigurationKey.GeneratedFilesExtension));
							RepositoryTransformer m2tRep = new RepositoryTransformer();
							m2tRep.setBom(bom);
							m2tRep.setNamespace(repositoryProj.getNamespace() +  "." + bom.getPackageName());
							m2tRep.generateCode(f);	
						}

					}
				}
				
				//generate project file
				File csProjFile = new File(directoryPattern + repositoryProj.getAssemblyName() + ".gen.csproj");
				if (!csProjFile.exists())
				{
					ProjectFileTransformer csProj2Text = new ProjectFileTransformer();
					csProj2Text.setProject(repositoryProj);
					csProj2Text.generateCode(csProjFile);
				}
			}
			
			if (transactionsProj != null)
			{
				app.setTransactionsProjectNamespace(transactionsProj.getNamespace());
				String directoryPattern = product.getProperty(ConfigurationKey.ApplicationsFolder) + "\\" + app.getRelativeFolderPath() + "\\" + transactionsProj.getRelativePath() + "\\";
				
				
				for(BusinessTransaction btm : product.getApplicationBTMs(app))
				{
					File dir = new File(directoryPattern + btm.getPackageName() + "\\(gen)\\");
					if (!dir.exists())
						dir.mkdirs();
					File f = new File(dir.getPath() + "\\" + btm.getName() + product.getProperty(ConfigurationKey.GeneratedFilesExtension));
					
					//Transaction class
					f = new File(dir.getPath() + "\\" + btm.getName() + "BusinessTransaction" + product.getProperty(ConfigurationKey.GeneratedFilesExtension));
					TransactionsTransformer m2tRep = new TransactionsTransformer();
					m2tRep.setBtm(btm);
					m2tRep.setNamespace(transactionsProj.getNamespace() +  "." + btm.getPackageName());
					m2tRep.generateCode(f);
				}
				
				
				
				//generate project file
				File csProjFile = new File(directoryPattern + transactionsProj.getAssemblyName() + ".gen.csproj");
				if (!csProjFile.exists())
				{
					ProjectFileTransformer csProj2Text = new ProjectFileTransformer();
					csProj2Text.setProject(transactionsProj);
					csProj2Text.generateCode(csProjFile);
				}
			}
		}
		
	}

	private static void generateDataLayer() throws Exception {
		
		for(Application app : product.getApplications())
		{
			String directoryPattern = product.getProperty(ConfigurationKey.ApplicationsFolder) + "\\" + app.getRelativeFolderPath() + "\\DataBase\\Schema\\";
			File dir = new File(directoryPattern);
			if (!dir.exists())
				dir.mkdirs();
			File fileTables = new File(dir.getPath() + "\\CreateTables_" +  app.getUniqueName() + ".sql");
			File fileCUDs = new File(dir.getPath() + "\\CUDs_" +  app.getUniqueName() + ".sql");
			File fileQueries = new File(dir.getPath() + "\\Queries_" +  app.getUniqueName() + ".sql");
			File fileConstraints = new File(dir.getPath() + "\\Constraints_" +  app.getUniqueName() + ".sql");
			File fileDataModules = new File(dir.getPath() + "\\DataModule_" +  app.getUniqueName() + ".xml");
			
			String genCodeTables = "";
			String genCodeCUDs = "";
			String genCodeQueries = "";
			String genCodeDataModules = "";
			String genCodeConstraints = "";
			
			//Common table types needed 
			CommonSQLTransformer m2tCommon = new CommonSQLTransformer();
			genCodeTables += m2tCommon.generateCodeInternal();
			
			for(DataTable tb : product.getApplicationTables(app))
			{
				DataTableTransformer m2t = new DataTableTransformer();
				m2t.setDataTable(tb);
				genCodeTables += m2t.generateCodeInternal();
				
				CUDQueriesTransformer m2tcud = new CUDQueriesTransformer();
				m2tcud.setDataTable(tb);
				genCodeCUDs += m2tcud.generateCodeInternal();
				
				DatabaseConstraintsTransformer m2tconst = new DatabaseConstraintsTransformer();
				m2tconst.setDataTable(tb);
				genCodeConstraints += m2tconst.generateCodeInternal();
			}
			for(Query query : product.getApplicationQueries(app))
			{
				QueryTransformer m2t = new QueryTransformer();
				m2t.setQuery(query);
				genCodeQueries += m2t.generateCodeInternal();
			}
			
			DataModuleTransformer m2t = new DataModuleTransformer();
			m2t.setApplication(app);
			genCodeDataModules = m2t.generateCodeInternal();
			
			
			IOHelper.writeToFile(genCodeTables, fileTables);
			IOHelper.writeToFile(genCodeCUDs, fileCUDs);
			IOHelper.writeToFile(genCodeQueries, fileQueries);
			IOHelper.writeToFile(genCodeDataModules, fileDataModules);
			IOHelper.writeToFile(genCodeConstraints, fileConstraints);
		}
		
	}
	
	private static void generateOtherProjects() throws Exception {
		
		for(Application app : product.getApplications())
		{
			ArrayList<ClassLibraryProject> projs = app.getClassLibraryProjects();			
			
			for(ClassLibraryProject proj : projs)
			{
				if (app.getHasTransactionsMapping() && proj.getArtifacts() != null && proj.getArtifacts().getTransactionsMapping() != null)
				{
					if (!StringUtils.isEmpty(proj.getArtifacts().getTransactionsMapping().getGenerateClassName()))
					{
						String directoryPattern = product.getProperty(ConfigurationKey.ApplicationsFolder) + "\\" + app.getRelativeFolderPath() + "\\" + proj.getRelativePath() + "\\";
						File dir = new File(directoryPattern);
						if (!dir.exists())
							dir.mkdirs();
						
						File f = new File(dir.getPath() + "\\" + proj.getArtifacts().getTransactionsMapping().getGenerateClassName() + product.getProperty(ConfigurationKey.GeneratedFilesExtension));
						
						//Transaction Mapping class
						TransactionsMappingTransformer m2tRep = new TransactionsMappingTransformer();
						m2tRep.setProduct(product);
						m2tRep.setClassname(proj.getArtifacts().getTransactionsMapping().getGenerateClassName());
						m2tRep.setNamespace(proj.getNamespace());
						m2tRep.generateCode(f);
					}
				}
				String directoryPattern = product.getProperty(ConfigurationKey.ApplicationsFolder) + "\\" + app.getRelativeFolderPath() + "\\" + proj.getRelativePath() + "\\";
				File csProjFile = new File(directoryPattern + proj.getAssemblyName() + ".gen.csproj");
				if (!csProjFile.exists())
				{
					ProjectFileTransformer csProj2Text = new ProjectFileTransformer();
					csProj2Text.setProject(proj);
					csProj2Text.generateCode(csProjFile);
				}
			}
			
			
			ArrayList<WindowsServiceProject> windowsServicesProjs = app.getWindowsServiceProjects();			
			
			for(WindowsServiceProject proj : windowsServicesProjs)
			{
				String directoryPattern = product.getProperty(ConfigurationKey.ApplicationsFolder) + "\\" + app.getRelativeFolderPath() + "\\" + proj.getRelativePath() + "\\";
				File csProjFile = new File(directoryPattern + proj.getAssemblyName() + ".gen.csproj");
				if (!csProjFile.exists())
				{
					ProjectFileTransformer csProj2Text = new ProjectFileTransformer();
					csProj2Text.setProject(proj);
					csProj2Text.generateCode(csProjFile);
				}
			}
			
			ArrayList<ConsoleApplicationProject> consoleProjects = app.getConsoleApplicationProjects();
			for(ConsoleApplicationProject proj : consoleProjects) {
				
				String directoryPattern = product.getProperty(ConfigurationKey.ApplicationsFolder) + "\\" + app.getRelativeFolderPath() + "\\" + proj.getRelativePath() + "\\";
				File csProjFile = new File(directoryPattern + proj.getAssemblyName() + ".gen.csproj");
				if (!csProjFile.exists())
				{
					ProjectFileTransformer csProj2Text = new ProjectFileTransformer();
					csProj2Text.setProject(proj);
					csProj2Text.generateCode(csProjFile);
				}
			}
		}
		
	}
	
	/**
	 * Generate UML graphs
	 * @throws Exception 
	 */
	private static void generateUML() throws Exception {

		UMLGraphBuilder graphBuilder = new UMLGraphBuilder(new GraphBuilderOptions());		
		
		for(Application app : product.getApplications())
		{
			String directoryPattern = product.getProperty(ConfigurationKey.ApplicationsFolder) + "\\" + app.getRelativeFolderPath() + "\\UML\\";
			File dir = new File(directoryPattern);
			if (!dir.exists())
				dir.mkdirs();
			
			// Build the UML class diagram from the package contents
			graphBuilder.loadUMLClassGraph(app, app.getUniqueName());
			
			// Export the dot commands to an output file
			graphBuilder.exportGraph(directoryPattern + app.getUniqueName() + ".dot");
			
			// Run the dot tool to produce an image of the UML diagram
			// Syntax: dot -T<output_type> -o"<path_to_image>" -K<layout_type>
			ProcessBuilder dotProcBuilder = new ProcessBuilder("dot", "-Gratio=0.7", "-Eminlen=2", "-Tsvg", "-o" + directoryPattern + app.getUniqueName() + ".svg", "-Kdot", directoryPattern + app.getUniqueName() + ".dot");
			dotProcBuilder.start();
		}
		
	}
	
}
