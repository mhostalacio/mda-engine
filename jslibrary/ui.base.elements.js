﻿


function parseDate(value) {
    if (value != null && value != '') {
        return Date.parseExact(value, Custom.Localization.viewModelDateFormat);
    }
    return value;
}

function parseDateTime(value) {
    if (value != null && value != '') {
        return Date.parseExact(value, Custom.Localization.viewModelDateTimeFormat);
    }
    return value;
}

function parseRadioValue(value) {
    if (value != null && value != '') {
        if (value == "on") {
            return true;
        }
        if (value == "off") {
            return false;
        }
    }
    return value;
}



function reinitPlugins() {
    
    markInvalidFields();
    if (typeof (Metronic) !== "undefined")
        Metronic.init();
    if (typeof (ComponentsPickers) !== "undefined")
        ComponentsPickers.init();
    if (typeof (ComponentsKnobDials) !== "undefined")
        ComponentsKnobDials.init();
}

function markInvalidFields() {
    var summary = $('.MessagesPanel');
    if (summary != undefined) {
        //clear first
        $("div.has-error").each(function () {
            $(this).removeClass("has-error");
        });

        var errors = summary.find('.alert-danger');

        if (errors != undefined) {
            errors.find("label[for!='']").each(function () {
                var elem = $('#' + $(this).attr('for'));
                elem.parent().addClass("has-error");
            });
        }
    }
}

function showToast(msg, shortCutFunction) {

    var title = '';
    var toastIndex = 1;


    toastr.options = {
        closeButton: true,
        debug: false,
        positionClass: "toast-bottom-right",
        onclick: null,
        showDuration: 1000,
        hideDuration: 1000,
        timeOut: 5000,
        extendedTimeOut: 1000,
        showEasing: "swing",
        hideEasing: "linear",
        showMethod: "fadeIn",
        hideMethod: "fadeOut"
    };

    var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
    $toastlast = $toast;

}