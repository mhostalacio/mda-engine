﻿var isDomReady = false;
var shouldCloseWindow = false;
var redirectUrl = null;

$(document).keypress(
    function (event) {

        //Exceptions
        if (event.srcElement != null) {
            //Textarea
            var isTextArea = $("#" + event.srcElement.id).is("textarea")
        }

        if (event.which == '13' && !isTextArea) {
            $form = $(this).closest('form');
            var hasBut = $form.has('#defaultbuttonid');
            if (hasBut) {
                var but = $('#' + $('#defaultbuttonid').val());
                if (but != null)
                    but.trigger("click");
                else
                    alert("Default button not found: " + $('#defaultbuttonid').val());
            }
            event.preventDefault();

        }
    });

$(document).ajaxStart(function () {
    $('#hdnIsOnRequest').val('true');
    //    isDomReady = false;
});

$(document).ajaxStop(function () {

    if (shouldCloseWindow) {
        $('#hdnIsOnRequest').val('false');
        window.close();
    }

    if (redirectUrl != null && redirectUrl != '') {
        window.location.href = redirectUrl;
    }
    else {
        //        isDomReady = true;
        $('#hdnIsOnRequest').val('false');
    }

    //markInvalidFields();
});

$(document).ready(function () {

    isDomReady = true;
    //markInvalidFields();
});

function postAction(url, postData, blockForm) {

    if (blockForm) {
        $.ajax({
            url: url,
            data: postData,
            type: 'POST',
            contentType: "multipart/form-data",
            cache: false,
            beforeSend: function () {
                $.blockUI();
            },
            error: function () {
                $.unblockUI();
            },
            success: function (xml) {
                //if (xml.indexOf('AtLoginPage') == -1) {
                    parseAjaxElementReturn(xml);
                    $.unblockUI();
                //}
                //else {
                //    $.unblockUI();
                //    openTimeoutBlockUI();
                //}
            },
            datatype: 'html'
        });
    }
    else {
        $.ajax({
            url: url,
            data: postData,
            type: 'POST',
            cache: false,
            success: function (xml) {
                //if (xml.indexOf('AtLoginPage') == -1) {
                    parseAjaxElementReturn(xml);
                //}
                //else {
                //    openTimeoutBlockUI();
                //}
            },
            datatype: 'html'
        });
    }
}

function parseAjaxElementReturn(xml) {

    var scriptsContainer = $('#AjaxScriptContainer');

    scriptsContainer.empty();

    if (xml != null && xml != '') {

        $(xml).each(function () {

            var respElem = $(this);

            parseElement(respElem, scriptsContainer);
        });
    }

    reinitPlugins();
}

function openTimeoutBlockUI() {

    $.blockUI({ message: '<div><div style="border-bottom:2px solid #FFFFFF"></div><br/><div><div style="float:left;height:200px;"><img src=""/></div><div style="margin-left: 160px;"><a id="link" href="#" onclick="location.reload()">' });
    $('.blockMsg').attr('class', $('.blockMsg').attr('class') + ' TimeoutBlockUI');
    $('.blockMsg').attr('style', $('.blockMsg').attr('style') + ' background-Image: url(TODO:URL) position:fixed');
    $('.blockOverlay').attr('style', $('.blockOverlay').attr('style') + ' background-color: black');
}


function postForm(elemId, actionName, postArgs, postUrl, postData, blockForm) {

    var tinyCEElement = null;

    if (window.tinyMCE) {
        tinyMCE.triggerSave();

        for (i = 0; i < tinyMCE.editors.length; i++) {

            tinyCEElement = document.getElementById(tinyMCE.editors[i].editorId);

            //Ticket #11361
            if (tinyCEElement != null) {
                v = tinyCEElement.value;
                if (v != null && v != '') {
                    v = v.replace(/&amp;amp;/gi, '&amp;');
                    tinyCEElement.value = v;
                }
            }

        }

    }

    var elem = $('#' + elemId);
    var frm = elem.parents('form:first');
    var nullData = "";

    frm.children('input[name=Post_Back_Action_Name_Hidden]:first').val(actionName);
    frm.children('input[name=Post_Back_Arguments_Hidden]:first').val(postArgs);

    frm.find('select[arl=true]').each(function () {
        $('#' + this.id).selectOptions(/./);
        if ($('#' + this.id).val() == null) {
            nullData += ('&' + this.name + '=');
        }
    });

    var formPostedData = frm.customSerializer();

    if (postData) {

        var postDataParameters = jQuery.param(postData);

        if (postDataParameters !== undefined && postDataParameters !== '') {

            formPostedData += '&' + postDataParameters;
        }
    }

    formPostedData += nullData;

    //Upload timeout setup
    var timeoutValue = 0;

    if (postUrl.indexOf('SendChunk') != -1) {
        timeoutValue = 60000;
    }

    if (blockForm) {
        $.ajax({
            url:  postUrl,
            data: formPostedData,
            type: 'POST',
            cache: false,
            timeout: timeoutValue,
            beforeSend: function () {
                $.blockUI();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.unblockUI();
                //Upload Debug data
                if (postUrl.indexOf('SendChunk') != -1) {
                    if (errorThrown != null) {
                        document.certificateApplet.DebugInfo(['Ajax error textStatus: ', textStatus, ' -- errorThrown.Name: ', errorThrown.name,
                        ' -- errorThrown.Number: ', errorThrown.number, ' -- errorThrown.Description: ', errorThrown.description,
                        ' -- errorThrown.Message: ', errorThrown.message, '\n'].join(''));
                    }
                    else {
                        document.certificateApplet.DebugInfo(['Ajax error textStatus: ', textStatus, '\n'].join(''));
                    }
                }
            },
            success: function (xml) {
                //Upload Debug data
                //if (xml.indexOf('AtLoginPage') == -1) {
                    parseAjaxElementReturn(xml);
                    $.unblockUI();
                //}
                //else {
                //    $.unblockUI();
                //    openTimeoutBlockUI();
                //}
            },
            complete: function () {
                var xhr = arguments[0];
                // fix memory leaks in IEs
                if (this.async === true || typeof this.async == 'undefined') {
                    if (typeof xhr != 'undefined') {
                        if (typeof xhr.onreadystatechange != 'unknown') {
                            xhr.onreadystatechange = null;
                        }
                        if (typeof xhr.abort != 'unknown') {
                            xhr.abort = null;
                        }
                        xhr = null;
                    }
                }
                if (typeof complete == 'function') {
                    complete.apply(this, arguments)
                }
            },
            datatype: 'html'
        });
    }
    else {
        $.ajax({
            url: postUrl,
            data: formPostedData,
            type: 'POST',
            cache: false,
            timeout: timeoutValue,
            error: function (jqXHR, textStatus, errorThrown) {
                //Upload Debug data
                if (postUrl.indexOf('SendChunk') != -1) {
                    if (errorThrown != null) {
                        document.certificateApplet.DebugInfo(['Ajax error textStatus: ', textStatus, ' -- errorThrown.Name: ', errorThrown.name,
                        ' -- errorThrown.Number: ', errorThrown.number, ' -- errorThrown.Description: ', errorThrown.description,
                        ' -- errorThrown.Message: ', errorThrown.message, '\n'].join(''));
                    }
                    else {
                        document.certificateApplet.DebugInfo(['Ajax error textStatus: ', textStatus, '\n'].join(''));
                    }
                }
            },
            success: function (xml) {

                //if (xml.indexOf('AtLoginPage') == -1) {
                    parseAjaxElementReturn(xml);
                //}
                //else {
                //    openTimeoutBlockUI();
                //}
            },
            complete: function () {
                var xhr = arguments[0];
                // fix memory leaks in IEs
                if (this.async === true || typeof this.async == 'undefined') {
                    if (typeof xhr != 'undefined') {
                        if (typeof xhr.onreadystatechange != 'unknown') {
                            xhr.onreadystatechange = null;
                        }
                        if (typeof xhr.abort != 'unknown') {
                            xhr.abort = null;
                        }
                        xhr = null;
                    }
                }
                if (typeof complete == 'function') {
                    complete.apply(this, arguments)
                }
            },
            datatype: 'html'
        });
    }

    frm.find('select[arl=true]').each(function () {
        $('#' + this.id).selectOptions(/zzzzz/, true);
    });
}

jQuery.fn.extend({
    customSerializer: function () {

        return jQuery.param(this.customArraySerializer());
    },
    customArraySerializer: function () {

        var rselectTextarea = /select|textarea/i;
        var rinput = /color|date|datetime|email|hidden|month|number|password|range|search|tel|text|time|url|week/i;

        return this.map(function () {
            return this.elements ? jQuery.makeArray(this.elements) : this;
        })
		    .filter(function () {
		        return this.name && !this.disabled &&
				(this.checked || rselectTextarea.test(this.nodeName) ||
					rinput.test(this.type));
		    })
		    .map(function (i, elem) {

		        var jElem = jQuery(this);
		        var val = jElem.val();

		        if (jQuery.shouldHtmlEscape(jElem)) {

		            val = jQuery.htmlEscape(val);
		        }

		        return val == null ?
				    null :
				    jQuery.isArray(val) ?
					    jQuery.map(val, function (val, i) {
					        return { name: elem.name, value: val };
					    }) :
					    { name: elem.name, value: val };
		    }).get();
    }
});

// Extend jQuery to add html escape Functions
$.extend({
    shouldHtmlEscape: function (jElem) {

        var htmlCtnt = jElem.attr('htmlCtnt');

        //** get elem data. search for key enc and if '1' encode value
        //** TODO : process Arrays to
        return htmlCtnt !== undefined && htmlCtnt === '1';
    },

    htmlEscape: function (str) {
        return String(str)
                .replace(/&/g, '&amp;')
                .replace(/"/g, '&quot;')
                .replace(/</g, '&lt;')
                .replace(/>/g, '&gt;');
    },

    // retrieves the element value
    getVal: function (elemRef, methodName) {

        var elem = undefined;

        if (typeof (elemRef) === 'string') {
            elem = $('#' + elemRef);
        } else {
            elem = elemRef;
        }

        var val = null;

        if (elem !== undefined) {

            val = methodName ? elem[methodName]() : elem.val();
            if (elem.is(":checkbox")) {
                val = !elem.prop('checked');
            }
            if (jQuery.shouldHtmlEscape(elem)) {
                val = jQuery.htmlEscape(val);
            }
        }

        return val;
    },

    // retrieves the URIEncoded element value 
    getURIVal: function (elemId, methodName) {
        //parse radio button groups
        if ($('#' + elemId).hasClass('radio-group')) {
            var radioValue = $('#' + elemId).find("input[name='" + elemId + "Group']:checked").val();
            return radioValue;
        }
        return encodeURIComponent(jQuery.getVal(elemId, methodName));
    },

    getDecimalValue: function (elemId) {

        var elemValue = $.getVal(elemId),
                decimalValue = $.parseBigDecimal(elemValue);

        return decimalValue !== undefined && !isNaN(decimalValue) ? decimalValue.toString() : null;
    }
});


function getAction(url, blockForm, callBack, ajaxCfg) {


    if (blockForm) {
        $.ajax({
            url: url,
            type: 'GET',
            cache: false,
            beforeSend: function () {
                $.blockUI();
            },
            error: function () {

                $.unblockUI();

                if (ajaxCfg) {
                    ajaxCfg.OnError();
                }
            },
            success: function (xml) {
                //if (xml.indexOf('AtLoginPage') == -1) {
                    parseAjaxElementReturn(xml);
                    $.unblockUI();
                    if (callBack != undefined && typeof callBack == "function") {
                        callBack(xml);
                    }

                    if (ajaxCfg) {
                        ajaxCfg.OnSucess();
                    }
                //}
                //else {
                //    $.unblockUI();
                //    openTimeoutBlockUI();
                //}
            },
            datatype: 'html'
        });
    }
    else {
        $.ajax({
            url: url,
            type: 'GET',
            cache: false,
            success: function (xml) {
                //if (xml.indexOf('AtLoginPage') == -1) {
                    parseAjaxElementReturn(xml);
                    if (callBack != undefined && typeof callBack == "function") {
                        callBack(xml);
                    }
                //}
                //else {
                //    openTimeoutBlockUI();
                //}
            },
            datatype: 'html'
        });
    }
}

function getActionAutoRefresh(url, callBack) {

    $.ajax({
        url: url,
        type: 'GET',
        cache: false,
        success: function (xml) {
            //if (xml.indexOf('AtLoginPage') == -1) {
                var result = parseAjaxElementReturnAutoRefresh(xml);
                if (callBack != undefined && typeof callBack == "function" && result !== null && result !== undefined && result != '') {
                    callBack(result);
                }
            //}
            //else {
            //    openTimeoutBlockUI();
            //}
        },
        datatype: 'html'
    });
}

function parseAjaxElementReturnAutoRefresh(xml) {

    var scriptsContainer = $('#AjaxScriptContainer');

    scriptsContainer.empty();

    var elemt = $($(xml)[0]);
    var elemId = elemt.attr('id');

    if (elemt.is('AutoRefresh')) {
        return elemt.attr('result');
    } else {
        $(xml).each(function () {
            var respElem = $(this);
            parseElement(respElem, scriptsContainer);
        });
    }
}



function parseElement(respElem, scriptsContainer) {

    var elemId = respElem.attr('id');

    if (elemId != null && elemId != '') {

        if (respElem.is('elementinstructions')) {

            var elem = $('#' + elemId);

            if (respElem.attr('isvisible') == 'true')
                elem.fadeIn();
            else
                elem.fadeOut();

        } else if (respElem.is('switchelement')) {

            $('#' + elemId).replaceWith(respElem.children().first());

        } else if (respElem.is('addelement')) {

            $('#' + elemId).append(respElem.children().first());

        } else if (respElem.is('removeelement')) {

            $('#' + elemId).remove();

        } else if (respElem.is('script')) {
            $('head').append(respElem);
        }
        else {

            var elem = $('#' + elemId);

            if (elem.length > 0) {
                elem.replaceWith(respElem);
            } else {
                scriptsContainer.append(respElem);
            }
        }
        //|| change to something dynamic? ||\\
    } else if (respElem.is('CallFunction')) {

        eval(respElem[0].firstChild.data);

    } else if (respElem.is('setvalid')) {

        $('#' + respElem.attr('elemId')).parent().removeClass('has-error');

    } else if (respElem.is('setinvalid')) {

        $('#' + respElem.attr('elemId')).parent().addClass('has-error');

    } else if (respElem.is('RedirectToAction')) {

        redirectToAction(respElem.attr('value'));

    } else if (respElem.is('ClosePopup')) {

        if (respElem.attr('value') != '') {
            window.parent.getAction(respElem.attr('value'), true);
        }
        closePopup();

    } else if (respElem.is('CallOpenerAction')) {

        if (respElem.attr('value') != '') {
            window.parent.getAction(respElem.attr('value'), true);
        }

    } else if (respElem.is('SetHtmlEditorContent')) {

        tinyMCE.activeEditor.setContent(unescape(respElem.attr('value')));

    } else if (respElem.is('div')) {

        var addElemAfter = respElem.attr('addelementsafter');

        if (addElemAfter != null && addElemAfter != '') {

            var elems = respElem.children();

            $(addElemAfter).after(elems);

        }

        var addElemBefore = respElem.attr('addelementsbefore');

        if (addElemBefore != null && addElemBefore != '') {

            var elems = respElem.children();

            $(addElemBefore).before(elems);

        }

    } else if (respElem.is('table')) {

        var addRowAfter = respElem.attr('addrowsafter');

        var addRowBefore = respElem.attr('addrowsbefore');

        var switchRows = respElem.attr('switchrows');

        var addTreeRowsAfter = respElem.attr('addtreerowsafter');
        var addTreeRowsBefore = respElem.attr('addtreerowsbefore');

        if (addRowAfter != null && addRowAfter != '') {

            var rows = null;
            var tbody = respElem.find('tbody');

            if (tbody.length > 0)
                rows = tbody.first().children();
            else
                rows = respElem.find('table').first().children();

            $(addRowAfter).after(rows);

        } else if (addRowBefore != null && addRowBefore != '') {

            var rows = null;
            var tbody = respElem.find('tbody');

            if (tbody.length > 0)
                rows = tbody.first().children();
            else
                rows = respElem.find('table').first().children();

            $(addRowBefore).before(rows);

        } else if (switchRows != null && switchRows != '') {

            var rows = null;
            var tbody = respElem.find('tbody');

            if (tbody.length > 0)
                rows = tbody.first().children();
            else
                rows = respElem.find('table').first().children();

            $('#' + switchRows).replaceWith(rows);

        } else if (addTreeRowsAfter != null && addTreeRowsAfter != '') {

            var treeId = respElem.attr('treeId');

            var rows = null;
            var tbody = respElem.find('tbody');

            if (tbody.length > 0)
                rows = tbody.first().children();
            else
                rows = respElem.find('table').first().children();

            var fragment = document.createDocumentFragment();

            for (var e = 0, len = rows.length; e < len; e++) {
                fragment.appendChild(rows[e]);
            }

            addFlatTreeRowsAfter(treeId, addTreeRowsAfter, rows, fragment);


        } else if (addTreeRowsBefore != null && addTreeRowsBefore != '') {

            var treeId = respElem.attr('treeId');

            var rows = null;
            var tbody = respElem.find('tbody');

            if (tbody.length > 0)
                rows = tbody.first().children();
            else
                rows = respElem.find('table').first().children();

            var fragment = document.createDocumentFragment();

            for (var e = 0, len = rows.length; e < len; e++) {
                fragment.appendChild(rows[e]);
            }

            addFlatTreeRowsBefore(treeId, addTreeRowsBefore, rows, fragment);

        } else {

            var tbody = respElem.find('tbody');

            if (tbody.length > 0) {
                var elemId = tbody.attr('id');
                if (elemId != null && elemId != '') {
                    parseElement(tbody, scriptsContainer);
                }
                else {
                    parseElement(tbody.first().children(), scriptsContainer);

                }
            }
            else {
                parseElement(respElem.children(), scriptsContainer);
            }
        }

    } else if (respElem.is('tr')) {

        var tds = respElem.children();

        if (tds != undefined && tds.length > 0) {

            for (var i = 0; i < tds.length; i++) {

                parseElement($(tds[i]), scriptsContainer);
            }
        }
        else
            parseElement(respElem.children(), scriptsContainer);

    } else if (respElem.is('noscript')) {

        var remElm = respElem.attr('removeelement');
        var uncheck = respElem.attr('uncheck');
        var check = respElem.attr('check');
        var setText = respElem.attr('settext');
        var setAtt = respElem.attr('setatt');
        var setClass = respElem.attr('setclass');
        var evalAtt = respElem.attr('eval');

        if (remElm != null && remElm != '') {

            $('#' + remElm).remove();

        } else if (uncheck != null && uncheck != '') {

            $('#' + uncheck).attr('checked', false);

        } else if (check != null && check != '') {

            $('#' + check).attr('checked', true);

        } else if (setText != null && setText != '') {

            $('#' + respElem.attr('elemid')).text(setText);

        } else if (setAtt != null && setAtt != '') {

            $('#' + respElem.attr('elemid')).attr(setAtt, respElem.attr('val'));

        } else if (setClass != null && setClass != '') {

            $('#' + respElem.attr('elemid')).attr('class', setClass);

        } else if (evalAtt != null && evalAtt != '') {

            eval(evalAtt);
        }

    } else if (respElem.is('ClearViewModels')) {

        clearViewModels();

    } else {

        scriptsContainer.append(respElem);
    }
}


function redirectToAction(url) {

    redirectUrl = url;
}



function openPopup(url, height, width, scrollbars, resizable, opener) {
    var openPop = $('#popup_modal');

    openPop.bPopup({
        content: 'iframe', //'ajax', 'iframe' or 'image'
        contentContainer: '.customcontent',
        loadUrl: url, //Uses jQuery.load()
        speed: 650,
        transition: 'fadeIn',
        transitionClose: 'fadeIn',
        follow: [false, false], //x, y
        scrollBar: scrollbars,
        iframeAttr: 'scrolling="yes" frameborder="0"',
        modalColor:'#000',
        onOpen: function() {
            $(this).css('display', 'block');
            $(this).css('visibility', 'visible');
            if (width != "")
                $(this).css('width', width);
            else
                $(this).css('width', '100%');
            if (height != "")
                $(this).css('height', height);
            else
                $(this).css('height', $(window).height());
        },
        onClose: function() {
            $(this).css('display', 'none');
            $(this).css('visibility', 'hidden');
            $(this).css('width', '1');
            $(this).css('height', '1');
            //$(this).dialog('empty');
        }
    });

    if (width != "")
        jQuery('iframe').attr("width", width);
    else
        jQuery('iframe').attr("width", "100%");
    if (height != "")
        jQuery('iframe').attr("height", height);
    else
        jQuery('iframe').attr("height", $(window).height());
}

function closePopup()
{
    if (window.parent.$('#popup_modal_close').get(0) != null)
        window.parent.$('#popup_modal_close').click();
    if ((typeof (window.parent.tinymce) !== "undefined") && window.parent.tinymce != null && window.parent.tinymce.activeEditor != null && window.parent.tinymce.activeEditor.windowManager != null)
        window.parent.tinymce.activeEditor.windowManager.close();
}


$('select').on('show', function (event, dropdownData) {
    console.log(dropdownData);
}).on('hide', function (event, dropdownData) {
    console.log(dropdownData);
});

function addMessage(messageType, message) {

    $("#MessagesPanel").text(message);

}

$(document).bind('drop dragover', function (e) {
    e.preventDefault();
});



function positionDivBellowObj(obj, divContent, xpad, ypad) {
    divContent.css("visibility","visible");
    divContent.css("display", "block");
    var offset = obj.offset();
    var x = offset.left;
    var y = offset.top + obj.height();

    // deal with elements inside tables and such
//    var parent = obj;
//    while (parent.offsetParent) {
//        parent = parent.offsetParent;
//        x += parent.offsetLeft;
//        y += parent.offsetTop;
//    }
    x = x + xpad;
    y = y + ypad;
    divContent.css("left", x + "px");
    divContent.css("top", y + "px");
}




$(document).click(function (event) {
    
});


function getRandom() {
    var min = 0;
    var max = 9007199254740992;
    return Math.floor(Math.random() * (max - min + 1)) + min;
}



function printError(err)
{
    var vDebug = ""; 
    for (var prop in err) 
    {  
       vDebug += "property: "+ prop+ " value: ["+ err[prop]+ "]\n"; 
    } 
    vDebug += "toString(): " + " value: [" + err.toString() + "]"; 
    status.rawValue = vDebug; 
}

function setCheckValue(event, objId)
{
    $('#' + objId).val(event.target.checked);
    $('#' + objId).trigger("change");
    return true;
}

